import { StyleSheet } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
export const colors = {
    black: '#1a1917',
    gray: '#888888',
    background1: '#B721FF',
    background2: '#21D4FD'
};

export default StyleSheet.create({
    safeArea: {
        flex: 1,
        backgroundColor: colors.black
    },
    container: {
        flex: 1,
        backgroundColor: colors.background1
    },
    gradient: {
        ...StyleSheet.absoluteFillObject
    },
    scrollview: {
        flex: 1
    },
    exampleContainer: {
        paddingVertical: 30
    },
    exampleContainerDark: {
        backgroundColor: colors.black
    },
    exampleContainerLight: {
        backgroundColor: 'white'
    },
    title: {
        paddingHorizontal: 30,
        backgroundColor: 'transparent',
        color: 'rgba(255, 255, 255, 0.9)',
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    titleDark: {
        color: colors.black
    },
    subtitle: {
        marginTop: 5,
        paddingHorizontal: 30,
        backgroundColor: 'transparent',
        color: 'rgba(255, 255, 255, 0.75)',
        fontSize: 13,
        fontStyle: 'italic',
        textAlign: 'center'
    },
    slider: {
        marginTop: 15,
        overflow: 'visible' // for custom animations
    },
    sliderContentContainer: {
        paddingVertical: 10 // for custom animation
    },
    paginationContainer: {
        paddingVertical: 8
    },
    paginationDot: {
        width: 8,
        height: 8,
        borderRadius: 4,
        marginHorizontal: 8
    },
    containerStyle: {
        width: '100%',
        height: '100%',
        flex: 1,
      },
      iconsView: {
        flexDirection: 'row',
        alignContent: 'center',
        marginTop: 45,
        justifyContent: 'center',
      },
      back: {
        width: 25, 
        height: 18, 
        marginTop: 45,
        marginLeft: 20,
        position: 'absolute',
        alignSelf: 'flex-start'
      },
      logo: {
        width: 70, 
        height: 38, 
        alignSelf: 'flex-end',
      },
      containericonStyle: {
        flexDirection: 'column',
        alignSelf: 'center',
        justifyContent: 'center',
      },
      containerGender: {
        flexDirection: 'row',
        alignSelf: 'center',
        justifyContent: 'center',
        marginTop: hp('4%'),
      },
      containerMaleStyle: {
        width: wp('40%'),
        height: hp('25%'),
        backgroundColor: '#ffffff',
        flexDirection: 'column',
        borderColor: '#ddd',
        borderRadius: 10,
        position: 'relative',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
      },
      containerFemaleStyle: {
        width: wp('40%'),
        height: hp('25%'),
        left: wp('5%'),
        backgroundColor: '#ffffff',
        flexDirection: 'column',
        borderColor: '#ddd',
        borderRadius: 10,
        position: 'relative',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 2,
      },
      containerSubmitStyle: {
        width: '93%',
        backgroundColor: '#06a283',
        flexDirection: 'row',
        borderRadius: 5,
        justifyContent: 'center',
        alignSelf: 'center',
        padding: 15,
        marginTop: 5,
      },
      deleteImageStyle: {
        width: 20,
        height: 20,
        position: 'relative',
        alignSelf: 'center',
      },
      subTextStyle: {
        alignSelf: 'center',
        fontSize: 12,
        marginLeft: 20,
        marginRight: 20,
        marginTop: 10,
        color: '#ffffff',
        textAlign: 'center',
      },
      subtextOneStyle: {
        fontSize: 24,
        marginTop: hp('12%'),
        marginLeft: 40,
        marginRight: 40,
        fontWeight: '500',
        color: '#2d3142',
        lineHeight: 30,
        letterSpacing: 0.75,
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
      },
      desc: {
        fontSize: 16,
        marginTop: 10,
        marginLeft: 40,
        marginRight: 40,
        fontWeight: '400',
        color: '#9c9eb9',
        lineHeight: 24,
        textAlign: 'center',
        fontFamily: 'Rubik-Regular',  
        marginTop: hp('3%'),
      },
      
      textSubmitStyle: {fontSize: 14, color: '#ffffff', fontWeight: 'bold'},
      mobileImageStyle: {width: 25, height: 25, alignSelf: 'flex-end',marginRight: 20},
      genImg: {height: 40,width: 35, alignSelf: 'center',marginTop: hp('3%'),},
      imageIconStyle: {width: 180, height: 98, alignSelf: 'center'},
    
      linearGradient: {
        width: '95%',
        height: 50,
        borderRadius: 25,
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: 30,
        marginBottom:15,
      },
      
      buttonText: {
        fontSize: 16,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
      },
      countryText: {
        alignSelf: 'center',
        fontSize: 16,
        marginTop: hp('2%'),
        fontWeight: '400',
        color: '#2d3142',
        letterSpacing: 0.26,
        fontFamily: 'Rubik-Regular',
      },
      backImageStyle: {
        width: 19,
        height: 16,
        marginTop: hp('7%'),
        marginLeft:20,
        marginBottom:20,
        alignSelf: 'flex-start',
      },
      appImageStyle: {
        width: 60,
        height: 32,
        marginTop: -45,
        marginBottom:20,
        
      },
      topBarImageStyle: {
        width: wp('10%'),
        height: 8,
        marginBottom:20,
        marginRight:1,
        
      },
      optionInnerContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignSelf: 'center',
      },
      viewBottom: {
        width: wp('93%'),
        alignContent: 'center',
        alignSelf: 'center',
        position:'absolute',
        bottom:10,
      },
});
