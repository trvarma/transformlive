import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import { Alert } from 'react-native';

import {
  INTERNETSTATUS,
  MOBILE_CHANGED,
  GET_OTP,
  GET_OTP_FAIL,
  GET_OTP_SUCCESS,
  USER_LOGIN,
  USER_LOGIN_FAIL,
  USER_LOGIN_SUCCESS,
  BASE_URL,
} from '../actions/types';

settimeout = undefined;

export const setInternetStatus = (connectionInfo) =>{
  return ({
    type: INTERNETSTATUS,
    payLoad: connectionInfo,
   // errors:errorObj
  });
}


/*
* Dispatch the internect connection info like connection type and availabiltity to the reducer
*/

export function CheckInternetStatus(connectionInfo) {
  try {
      return dispatch => {
          //let message = {}
          if(settimeout!=undefined)
          {
              clearTimeout(settimeout);
          }
          if(connectionInfo.isInternetReachable)
          {
              dispatch(setInternetStatus(connectionInfo));
          }
          else
          {
              if(connectionInfo.isConnected)
              {
                  dispatch(setInternetStatus(connectionInfo));
              }
              else
              {
                  dispatch(setInternetStatus(connectionInfo));
              }
          }
          
          settimeout = setTimeout(() => {
              settimeout = undefined
              //dispatch(setError(message))
          }, 3000);
      }
  } catch (error) {
     console.log(error);
  }
}	

export const mobileNumberChanged = text => {
  return {
    type: MOBILE_CHANGED,
    payLoad: text,
  };
};

export const getOtp = (mobileNumber, flag) => {
  return dispatch => {
    dispatch({ type: GET_OTP });
    fetch(`${BASE_URL}/user/sendotp`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        mobileno: mobileNumber,
        resend: flag,
      }),
    })
      .then(processResponse)
      .then(res => {
        const { statusCode, data } = res;
        if (statusCode >= 200 && statusCode <= 300) {
          // if (data.isMgr === 0) {
          getOtpSuccess(dispatch, mobileNumber, flag);
          // } else {
          //   loginUserFail(dispatch, 'You are not authorised :)');
          // }
        } else {
          getOtpFail(dispatch, data.message);
        }
      })
      .catch(function (error) {
        getOtpFail(dispatch, error);
      });
  };
};

export const verifyOtp = ({ mobile, otp, device_key, hashCode }) => {
  return dispatch => {
    dispatch({ type: USER_LOGIN });
    fetch(`${BASE_URL}/user/validateotp`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        mobileno: mobile,
        otp: otp,
        device_key: device_key,
        otpHashCode: hashCode,
      }),
    })
      .then(processResponse)
      .then(res => {
        const { statusCode, data } = res;
        if (statusCode >= 200 && statusCode <= 300) {
          if (data.userId) {
            storeItem(dispatch, 'token', data.token, data.is_trainer.toString(), data.userId.toString(), data.phno.toString(), data.goal_id.toString(), data.goal_name.toString(), data.weight.toString(), data.height.toString(), data.userName.toString(), data.gender_id.toString(), data.prf_img.toString(), data.is_wrkout_subscribe.toString(), data.language_id.toString(), data.language_name.toString(), data.fitbit_token.toString());
          }
          else {
            storeProfileItem(dispatch, 'token', data.token, data.userId.toString(), data.phno.toString(), data.web_url.toString());
          }
        } else {
          loginUserFail(dispatch, data.message);

        }
      })
      .catch(function (error) {
        loginUserFail(dispatch, error);
      });
  };
};

function processResponse(response) {
  const statusCode = response.status;
  const data = response.json();
  return Promise.all([statusCode, data]).then(res => ({
    statusCode: res[0],
    data: res[1],
  }));
}

const storeItem = async (dispatch, key, item, trainer, userId, phno, goalId, goalName, weight, height, userName, gender_id, profImg, is_wrkout_subscribe, language_id, language_name, fitbit_token) => {
  try {
    await AsyncStorage.setItem('token', item);
    await AsyncStorage.setItem('istrainer', trainer);
    await AsyncStorage.setItem('userId', userId);
    await AsyncStorage.setItem('phno', phno);
    await AsyncStorage.setItem('goalId', goalId);
    await AsyncStorage.setItem('goalName', goalName);
    await AsyncStorage.setItem('weight', weight);
    await AsyncStorage.setItem('height', height);
    await AsyncStorage.setItem('userName', userName);
    await AsyncStorage.setItem('genderId', gender_id);
    await AsyncStorage.setItem('profileImg', profImg);
    await AsyncStorage.setItem('is_wrkout_subscribe', is_wrkout_subscribe);
    await AsyncStorage.setItem('isSocialMedia', "0");
    await AsyncStorage.setItem('languageId', language_id);
    await AsyncStorage.setItem('language_name', language_name);
    // await AsyncStorage.setItem('fitbitaccess_token', fitbit_token);
    await AsyncStorage.setItem('diet_id', '0');
    await AsyncStorage.setItem('show_apptour', '0');
    await AsyncStorage.setItem('first_time_home', '0');

    await AsyncStorage.setItem(key, item).then(
      loginUserSuccess(dispatch, item, trainer),
    );
  } catch (error) {
    console.log(error.message);
  }
};

const storeProfileItem = async (dispatch, key, item, userId, phno, regWebUrl) => {
  try {
    await AsyncStorage.setItem('userId', userId);
    await AsyncStorage.setItem('phno', phno);
    await AsyncStorage.setItem('isSocialMedia', "0");
    await AsyncStorage.setItem('diet_id', '0');
    await AsyncStorage.setItem('show_apptour', '0');
    await AsyncStorage.setItem('first_time_home', '0');
    await AsyncStorage.setItem('reg_web_url', regWebUrl);
    //we want to wait for the Promise returned by AsyncStorage.setItem()
    //to be resolved to the actual value before returning the value
    await AsyncStorage.setItem(key, item).then(
      profileReg(dispatch,regWebUrl),
    );
  } catch (error) {
    console.log(error.message);
  }
};

const loginUserFail = (dispatch, error) => {
  dispatch({
    type: USER_LOGIN_FAIL,
    payLoad: error,
  });
  if (error) {
    setTimeout(() => {
      Alert.alert(
        'Alert',
        JSON.stringify(error),
        [
          {
            text: 'OK',
            onPress: () => {
              // Actions.dailyReport();
            },
          },
        ],
        { cancelable: false },
      );
    }, 1000);
  }
};

const getOtpSuccess = async (dispatch, item, flag) => {
  dispatch({
    type: GET_OTP_SUCCESS,
    payLoad: item,
  });
  if (flag === "1") {
    Actions.otp();
  }
};

const getOtpFail = (dispatch, error) => {
  dispatch({
    type: GET_OTP_FAIL,
    payLoad: error,
  });
  if (error) {
    setTimeout(() => {
      Alert.alert(
        'Alert',
        JSON.stringify(error),
        [
          {
            text: 'OK',
            onPress: () => {
              // Actions.dailyReport();
            },
          },
        ],
        { cancelable: false },
      );
    }, 1000);
  }
};

const loginUserSuccess = async (dispatch, item, trainer) => {
  dispatch({
    type: USER_LOGIN_SUCCESS,
    payLoad: item,
  });

  return Actions.thome({ type: 'reset', });

  //  if(trainer != null)
  //   {
  //     if(trainer === '1')
  //     {
  //       return Actions.main({type: 'reset'});
  //     }
  //     else{
  //       return Actions.thome({type: 'reset', });
  //     }

  //   }
};

const profileReg = async (dispatch,regWebUrl) => {
  dispatch({
    type: USER_LOGIN_SUCCESS,
    payLoad: '',
  });
  //return Actions.web({ type: 'reset', url: regWebUrl, flag: 'newreg' });
   return Actions.profile({ type: 'reset', });
};
