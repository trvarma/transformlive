import {
  GET_HEALTH_PROBLEMS,
  GET_FOOD_PREFERENCES,
  GET_FOOD_ALLERGIES,
  GET_ACTIVE_LEVELS,
  GET_EQUIPMENTS,
  GET_SAMPLE_PROFILEPICS,
  MASTER_GET_FAIL,
  BASE_URL,
  GET_TRAINEE_GOALS,
  GET_TRAINEE_GOALS_SUCCESS,
  GET_TRAINEE_GOALS_FAILURE,
  SAVE_TRAINEE_DETAILS,
  SAVE_TRAINEE_DETAILS_SUCCESS,
  SAVE_TRAINEE_DETAILS_FAILURE,
  GET_LANGUAGES,
  GET_PROFESSIONS,
  GET_WORKOUT_LOCATIONS,
  GET_FITNESS_FORM,

} from './types';
import { Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';

export const getFoodPreferences = () => {
  return dispatch => {
    fetch(`${BASE_URL}/master/foodpreference`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        //Authorization: `Bearer ${token}`,
      },
    })
      .then(processResponse)
      .then(res => {
        const { statusCode, data } = res;
        console.log('statusCode', statusCode);
        console.log('data', data);
        if (statusCode >= 200 && statusCode <= 300) {
          if (data.data.length !== 0) {
            data.data.map((item) => {
              item.check = false;
              return data.data;
            })

            dispatch({
              type: GET_FOOD_PREFERENCES,
              payLoad: data.data,
            });
          }
          else {
            dispatch({
              type: MASTER_GET_FAIL,
              payLoad: 'No data available at this moment',
            });
          }
        }
        else {
          dispatch({
            type: MASTER_GET_FAIL,
            payLoad: data.message,
          });
        }
      })
      .catch(function (error) {
        // console.log(error);
        dispatch({
          type: MASTER_GET_FAIL,
          payLoad: error,
        });
      });
  };
};

export const getHealthProblems = () => {
  return dispatch => {
    fetch(`${BASE_URL}/master/medicalcndt`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        //Authorization: `Bearer ${token}`,
      },
    })
      .then(processResponse)
      .then(res => {
        const { statusCode, data } = res;
        console.log('statusCode', statusCode);
        console.log('data', data);
        if (statusCode >= 200 && statusCode <= 300) {
          if (data.data.length !== 0) {
            data.data.map((item) => {
              item.check = false;
              return data.data;
            })

            dispatch({
              type: GET_HEALTH_PROBLEMS,
              payLoad: data.data,
            });
          }
          else {
            dispatch({
              type: MASTER_GET_FAIL,
              payLoad: 'No data available at this moment',
            });
          }
        }
        else {
          dispatch({
            type: MASTER_GET_FAIL,
            payLoad: data.message,
          });
        }
      })
      .catch(function (error) {
        // console.log(error);
        dispatch({
          type: MASTER_GET_FAIL,
          payLoad: error,
        });
      });
  };
};

export const getFoodAllergies = () => {
  return dispatch => {
    fetch(`${BASE_URL}/master/foodallergies`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        //Authorization: `Bearer ${token}`,
      },
    })
      .then(processResponse)
      .then(res => {
        const { statusCode, data } = res;
        console.log('statusCode', statusCode);
        console.log('data', data);
        if (statusCode >= 200 && statusCode <= 300) {
          if (data.data.length !== 0) {
            data.data.map((item) => {
              item.check = false;
              return data.data;
            })

            dispatch({
              type: GET_FOOD_ALLERGIES,
              payLoad: data.data,
            });
          }
          else {
            dispatch({
              type: MASTER_GET_FAIL,
              payLoad: 'No data available at this moment',
            });
          }
        }
        else {
          dispatch({
            type: MASTER_GET_FAIL,
            payLoad: data.message,
          });
        }
      })
      .catch(function (error) {
        // console.log(error);
        dispatch({
          type: MASTER_GET_FAIL,
          payLoad: error,
        });
      });
  };
};

export const getActiveLevels = () => {
  return dispatch => {
    fetch(`${BASE_URL}/master/activelevels`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        //Authorization: `Bearer ${token}`,
      },
    })
      .then(processResponse)
      .then(res => {
        const { statusCode, data } = res;
        console.log('statusCode', statusCode);
        console.log('data', data);
        if (statusCode >= 200 && statusCode <= 300) {
          if (data.data.length !== 0) {
            data.data.map((item) => {
              item.check = false;
              return data.data;
            })

            dispatch({
              type: GET_ACTIVE_LEVELS,
              payLoad: data.data,
            });
          }
          else {
            dispatch({
              type: MASTER_GET_FAIL,
              payLoad: 'No data available at this moment',
            });
          }
        }
        else {
          dispatch({
            type: MASTER_GET_FAIL,
            payLoad: data.message,
          });
        }
      })
      .catch(function (error) {
        // console.log(error);
        dispatch({
          type: MASTER_GET_FAIL,
          payLoad: error,
        });
      });
  };
};

export const getProfessions = () => {
  return dispatch => {
    fetch(`${BASE_URL}/master/profession`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        //Authorization: `Bearer ${token}`,
      },
    })
      .then(processResponse)
      .then(res => {
        const { statusCode, data } = res;
        console.log('statusCode', statusCode);
        console.log('data', data);
        if (statusCode >= 200 && statusCode <= 300) {
          if (data.data.length !== 0) {
            data.data.map((item) => {
              item.check = false;
              return data.data;
            })

            dispatch({
              type: GET_PROFESSIONS,
              payLoad: data.data,
            });
          }
          else {
            dispatch({
              type: MASTER_GET_FAIL,
              payLoad: 'No data available at this moment',
            });
          }
        }
        else {
          dispatch({
            type: MASTER_GET_FAIL,
            payLoad: data.message,
          });
        }
      })
      .catch(function (error) {
        // console.log(error);
        dispatch({
          type: MASTER_GET_FAIL,
          payLoad: error,
        });
      });
  };
};

export const getWorkoutLocations = () => {
  return dispatch => {
    fetch(`${BASE_URL}/master/workoutlocation`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        //Authorization: `Bearer ${token}`,
      },
    })
      .then(processResponse)
      .then(res => {
        const { statusCode, data } = res;
        console.log('statusCode', statusCode);
        console.log('data', data);
        if (statusCode >= 200 && statusCode <= 300) {
          if (data.data.length !== 0) {
            data.data.map((item) => {
              item.check = false;
              return data.data;
            })

            dispatch({
              type: GET_WORKOUT_LOCATIONS,
              payLoad: data.data,
            });
          }
          else {
            dispatch({
              type: MASTER_GET_FAIL,
              payLoad: 'No data available at this moment',
            });
          }
        }
        else {
          dispatch({
            type: MASTER_GET_FAIL,
            payLoad: data.message,
          });
        }
      })
      .catch(function (error) {
        // console.log(error);
        dispatch({
          type: MASTER_GET_FAIL,
          payLoad: error,
        });
      });
  };
};

export const getFitnessForm = () => {
  return dispatch => {
    fetch(`${BASE_URL}/master/fitnessform`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        //Authorization: `Bearer ${token}`,
      },
    })
      .then(processResponse)
      .then(res => {
        const { statusCode, data } = res;
        console.log('statusCode', statusCode);
        console.log('data', data);
        if (statusCode >= 200 && statusCode <= 300) {
          if (data.data.length !== 0) {
            data.data.map((item) => {
              item.check = false;
              return data.data;
            })

            dispatch({
              type: GET_FITNESS_FORM,
              payLoad: data.data,
            });
          }
          else {
            dispatch({
              type: MASTER_GET_FAIL,
              payLoad: 'No data available at this moment',
            });
          }
        }
        else {
          dispatch({
            type: MASTER_GET_FAIL,
            payLoad: data.message,
          });
        }
      })
      .catch(function (error) {
        // console.log(error);
        dispatch({
          type: MASTER_GET_FAIL,
          payLoad: error,
        });
      });
  };
};

export const getEquipments = () => {
  return dispatch => {
    fetch(`${BASE_URL}/master/equipment`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        //Authorization: `Bearer ${token}`,
      },
    })
      .then(processResponse)
      .then(res => {
        const { statusCode, data } = res;
        console.log('statusCode', statusCode);
        console.log('data', data);
        if (statusCode >= 200 && statusCode <= 300) {
          if (data.data.length !== 0) {
            data.data.map((item) => {
              item.check = false;
              return data.data;
            })

            dispatch({
              type: GET_EQUIPMENTS,
              payLoad: data.data,
            });
          }
          else {
            dispatch({
              type: MASTER_GET_FAIL,
              payLoad: 'No data available at this moment',
            });
          }
        }
        else {
          dispatch({
            type: MASTER_GET_FAIL,
            payLoad: data.message,
          });
        }
      })
      .catch(function (error) {
        // console.log(error);
        dispatch({
          type: MASTER_GET_FAIL,
          payLoad: error,
        });
      });
  };
};

export const getLanguages = () => {
  return dispatch => {
    fetch(`${BASE_URL}/master/language`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        //Authorization: `Bearer ${token}`,
      },
    })
      .then(processResponse)
      .then(res => {
        const { statusCode, data } = res;
        console.log('statusCode', statusCode);
        console.log('data', data);
        if (statusCode >= 200 && statusCode <= 300) {
          if (data.data.length !== 0) {
            data.data.map((item) => {
              item.check = false;
              return data.data;
            })

            dispatch({
              type: GET_LANGUAGES,
              payLoad: data.data,
            });
          }
          else {
            dispatch({
              type: MASTER_GET_FAIL,
              payLoad: 'No data available at this moment',
            });
          }
        }
        else {
          dispatch({
            type: MASTER_GET_FAIL,
            payLoad: data.message,
          });
        }
      })
      .catch(function (error) {
        // console.log(error);
        dispatch({
          type: MASTER_GET_FAIL,
          payLoad: error,
        });
      });
  };
};

export const getSampleProfilePics = () => {
  return dispatch => {
    fetch(`${BASE_URL}/master/profile`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        //Authorization: `Bearer ${token}`,
      },
    })
      .then(processResponse)
      .then(res => {
        const { statusCode, data } = res;
        console.log('statusCode', statusCode);
        console.log('data', data);
        if (statusCode >= 200 && statusCode <= 300) {
          if (data.data.length !== 0) {
            data.data.map((item) => {
              item.check = false;
              return data.data;
            })

            dispatch({
              type: GET_SAMPLE_PROFILEPICS,
              payLoad: data.data,
            });
          }
          else {
            dispatch({
              type: MASTER_GET_FAIL,
              payLoad: 'No data available at this moment',
            });
          }
        }
        else {
          dispatch({
            type: MASTER_GET_FAIL,
            payLoad: data.message,
          });
        }
      })
      .catch(function (error) {
        // console.log(error);
        dispatch({
          type: MASTER_GET_FAIL,
          payLoad: error,
        });
      });
  };
};

export const getTraineeGoals = () => {
  // console.log(token);
  return dispatch => {
    dispatch({ type: GET_TRAINEE_GOALS });
    fetch(
      `${BASE_URL}/master/goal`,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      },
    )
      .then(processResponse)
      .then(res => {
        const { statusCode, data } = res;
        console.log('statusCode', statusCode);
        console.log('data', data);
        if (statusCode >= 200 && statusCode <= 300) {
          if (data.data.length !== 0) {
            data.data.map((item) => {
              item.check = false;
              return data.data;
            })

            dispatch({
              type: GET_TRAINEE_GOALS_SUCCESS,
              payload: data.data,
            });
          }
          else {
            getCategoriesFailedFailed(dispatch, 'No data available at this moment');
          }
        } else {
          if (data.message === 'You are not authenticated!') {
            getCatalogueCategoriesFailed(dispatch, data.message);
          } else {
            getCategoriesFailedFailed(dispatch, data.message);
          }
        }
      })

      .catch(function (error) {
        // console.log(error);
        getCategoriesFailedFailed(dispatch, error);

      });
  };
};

export const getTraineeGoalsByGender = ({gender}) => {
  console.log(`${BASE_URL}/master/gendergoal?gender_id=${gender}`);
  return dispatch => {
    dispatch({ type: GET_TRAINEE_GOALS });
    fetch(
      `${BASE_URL}/master/gendergoal?gender_id=${gender}`,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
      },
    )
      .then(processResponse)
      .then(res => {
        const { statusCode, data } = res;
        console.log('statusCode', statusCode);
        console.log('data', data);
        if (statusCode >= 200 && statusCode <= 300) {
          if (data.data.length !== 0) {
            data.data.map((item) => {
              item.check = false;
              return data.data;
            })

            dispatch({
              type: GET_TRAINEE_GOALS_SUCCESS,
              payload: data.data,
            });
          }
          else {
            getCategoriesFailedFailed(dispatch, 'No data available at this moment');
          }
        } else {
          if (data.message === 'You are not authenticated!') {
            getCatalogueCategoriesFailed(dispatch, data.message);
          } else {
            getCategoriesFailedFailed(dispatch, data.message);
          }
        }
      })

      .catch(function (error) {
        // console.log(error);
        getCategoriesFailedFailed(dispatch, error);

      });
  };
};

const getCategoriesFailedFailed = (dispatch, error) => {
  dispatch({
    type: GET_TRAINEE_GOALS_FAILURE,
    payload: error,
  });
  if (error) {
    setTimeout(() => {
      Alert.alert(
        'Alert',
        error,
        [
          {
            text: 'OK',
            onPress: () => {
              // Actions.dailyReport();
            },
          },
        ],
        { cancelable: false },
      );
    }, 1000);
  }
};

const getCatalogueCategoriesFailed = (dispatch, error) => {
  dispatch({
    type: GET_TRAINEE_GOALS_FAILURE,
    payload: error,
  });
  if (error) {
    setTimeout(() => {
      Alert.alert(
        'Alert',
        error,
        [
          {
            text: 'OK',
            onPress: () => {
              // Actions.dailyReport();
            },
          },
        ],
        { cancelable: false },
      );
    }, 1000);
  }
};
export const uploadSubscriptionDetails = (body, token) => {
  return dispatch => {
    console.log("body", body);
    dispatch({ type: SAVE_TRAINEE_DETAILS });
    fetch(`${BASE_URL}/user/userimpdet`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: body,
    })
      .then(processResponse)
      .then(res => {
        const { statusCode, data } = res;
        console.log('statusCode', statusCode);
        console.log('data', data);
        if (statusCode >= 200 && statusCode <= 300) {
          profileUploadSucess(dispatch, data.message)
        } else {
          if (data.message === 'You are not authenticated!') {
            getFeedsFailedFailed(dispatch, data.message);
          } else {
            getFeedsFailed(dispatch, data.message);
          }
        }
      })
      .catch(function (error) {
        //console.log(error);
        getFeedsFailed(dispatch, error);
      });
  };
};

const profileUploadSucess = async (dispatch, text) => {
  await AsyncStorage.setItem('is_wrkout_subscribe', "1");
  dispatch({
    type: SAVE_TRAINEE_DETAILS_SUCCESS,
    payLoad: text,
  });

  if (text) {
    setTimeout(() => {
      Alert.alert(
        'Alert',
        text,
        [
          {
            text: 'OK',
            onPress: () => {
              Actions.traineeWorkoutsHome();
            },
          },
        ],
        { cancelable: false },
      );
    }, 1000);
  }
};

const getFeedsFailedFailed = (dispatch, error) => {
  dispatch({
    type: SAVE_TRAINEE_DETAILS_FAILURE,
    payload: error,
  });
  if (error) {
    setTimeout(() => {
      Alert.alert(
        'Alert',
        error,
        [
          {
            text: 'OK',
            onPress: () => {
              AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
              });
              // Actions.dailyReport();
            },
          },
        ],
        { cancelable: false },
      );
    }, 1000);
  }
};

const getFeedsFailed = (dispatch, error) => {
  dispatch({
    type: SAVE_TRAINEE_DETAILS_FAILURE,
    payload: error,
  });
  if (error) {
    setTimeout(() => {
      Alert.alert(
        'Alert',
        error,
        [
          {
            text: 'OK',
            onPress: () => {

              // Actions.dailyReport();
            },
          },
        ],
        { cancelable: false },
      );
    }, 1000);
  }
};



function processResponse(response) {
  const statusCode = response.status;
  const data = response.json();
  return Promise.all([statusCode, data]).then(res => ({
    statusCode: res[0],
    data: res[1],
  }));
}

