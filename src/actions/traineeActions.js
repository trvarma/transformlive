
import { BASE_URL } from './types';
import TraineeService from '../services/traineeService';

import { 
    TRACKERSDATA, 
    TRAINEE_LASTWK_FOOD_DET, 
    TRAINEE_SELECTED_FOOD_DETAILS, 
    TRAINEE_FOOD_DELETE_STATUS, 
    SET_SLIDERINDEX, 
    SETTRAINEEDETAILS, 
    SETTRENDINGWORKOUTS, 
    ECMRSPRODUCTS,
    GOALSUBCTG,
    CAL_COUNTER_SETUP_STATUS
} from './types';


//Trainee Home page
function setProductsdata(data){
    return {
        type: ECMRSPRODUCTS,
        payLoad: data,
    } 
}

function setTrendingWorkouts(data){
    return {
        type: SETTRENDINGWORKOUTS,
        payLoad: data,
    } 
}

function setTraineeDetails(data){
    return {
        type: SETTRAINEEDETAILS,
        payLoad: data,
    } 
}

export const getProductsAction=(token)=> async dispatch => {
    try{
        const res = await TraineeService.getProducts(BASE_URL,token);
        const { statusCode, data } = res;
        if (statusCode >= 200 && statusCode <= 300) {
            dispatch(setProductsdata(data));
        }
        return res;
    }catch(error){
        console.log(error)
    }
}

export const getHomepTrendingWorkoutsAction=(token)=> async dispatch => {
    try{
        const res = await TraineeService.getTrendingWorkouts(BASE_URL,token);
        const { statusCode, data } = res;
        if (statusCode >= 200 && statusCode <= 300) {
            dispatch(setTrendingWorkouts(data));
        }
        return res;
    }catch(error){
        console.log(error)
    }
}

export const getHomepageDetAction=(token,params)=> async dispatch => {
    try{
        const res = await TraineeService.getHomepageDetails(BASE_URL,token,params);
        const { statusCode, data } = res;
        if (statusCode >= 200 && statusCode <= 300) {
            dispatch(setTraineeDetails(data));
            if(data.data.cc_override == 0){
                dispatch(setCalCounterSetup(false));
            }else{
                dispatch(setCalCounterSetup(true));
            }
        }
        return res;
    }catch(error){
        console.log(error)
    }
}

function setTrackersdata(data){
    return {
        type: TRACKERSDATA,
        payLoad: data,
    }
}


function setGoalsubctg(data){
    return {
        type: GOALSUBCTG,
        payLoad: data,
    }
}

function setTraineeLastWkFoodDetails(data){
    return {
        type: TRAINEE_LASTWK_FOOD_DET,
        payLoad: data,
      };
}

function setSelectedFoodDetails(data){
    return{
        type: TRAINEE_SELECTED_FOOD_DETAILS,
        payLoad: data,
    }
}

function setSelectedFoodDeleteStatus(data){
    return{
        type: TRAINEE_FOOD_DELETE_STATUS,
        payLoad: data,
    }
}

function setCalCounterSetup(data){
    return{
        type: CAL_COUNTER_SETUP_STATUS,
        payLoad: data,
    }
}

export const setSliderIndexAction = (data) => {
    return (dispatch)=> {
        dispatch({
            type: SET_SLIDERINDEX,
            payLoad: data
        })
    }
}

//Trackets Home page
export const getTrackersdataAction=(token)=> async dispatch => {
    try{
        const res = await TraineeService.getTrackersData(BASE_URL,token);
        const { statusCode, data } = res;
        if (statusCode >= 200 && statusCode <= 300) {
            dispatch(setTrackersdata(data));
        }
        return res;
    }catch(error){
        console.log(error)
    }
}

export const saveWaterAction= async (token) => {
    try{
        const res = await TraineeService.saveWaterIntakeDetails(BASE_URL,token);
        const { statusCode, data } = res;
        if (statusCode >= 200 && statusCode <= 300) {
            return res;
        }else{
            return false;
        }
    }catch(error){
        console.log(error)
    }
}


export const GetGoalsubctgAction=  (token,params) => async dispatch => {
    try{
        const res = await TraineeService.GetGoalsubctg(BASE_URL,token,params);
        const { statusCode, data,wg_bad,wg_cals,wg_good ,wl_bad,wl_cals,wl_good } = res;
        if (statusCode >= 200 && statusCode <= 300) {
            dispatch(setGoalsubctg(data));
        }else{
            return false;
        }
    }catch(error){
        console.log("Error in GetGoalsubctgAction",error);
    }
}


//TracketsHomeFoodNew
export const getFoodDetailsAction=(token,selDate)=> async dispatch => {
    try{
        const res = await TraineeService.getTraineeLastWeekFoodDetails(BASE_URL,token,selDate);
        const { statusCode, data } = res;
        if (statusCode >= 200 && statusCode <= 300) {
            dispatch(setTraineeLastWkFoodDetails(data));
        }
        return res;
    }catch(error){
        console.log(error)
    }
}

export const deleteFoodItemAction= (token,params) => {
    try{
        return async dispatch=>{
            const res = await TraineeService.deleteTraineeFoodItem(BASE_URL,token,params);
            const { statusCode, data } = res;
            if (statusCode >= 200 && statusCode <= 300 && data.title== "Confirmed") {
                dispatch(setSelectedFoodDeleteStatus(true));
            }
            return res;
        }
    }catch(error){
        console.log(error)
    }
}

export const getSelFoodItemDetailsAction= (token,params)=> {
    try{
        return async dispatch =>{
            const res = await TraineeService.getSelFoodItemDetails(BASE_URL,token,params);
            const { statusCode, data } = res;
            if (statusCode >= 200 && statusCode <= 300) {
                dispatch(setSelectedFoodDetails(data));
            }
            return res;
        }
    }catch(error){
        console.log("Error in getSelFoodItemDetailsAction",error);
    }
}

export const plansCheckCountAction= async (token,params) => {
    try{
        const res = await TraineeService.plansCheckCount(BASE_URL,token,params);
        const { statusCode, data } = res;
        if (statusCode >= 200 && statusCode <= 300) {
            return true;
        }else{
            return false;
        }
    }catch(error){
        console.log("Error in plansCheckCountAction : ",error)
    }
}


export const saveCalorieCounterSetupAction = (token, params) => async dispatch => {
    try {
        const res = await TraineeService.saveCalorieCounterSetup(BASE_URL, token, params);
        const { statusCode, data } = res;
        if (statusCode >= 200 && statusCode <= 300) {
            dispatch(setCalCounterSetup(true));
            return data;
        } else {
            dispatch(setCalCounterSetup(false));
            return data;
        }

    } catch (error) {
        console.log("Error in saveCalorieCounterSetupAction : ", error)
    }
}
