import {
  GET_HEALTH_PROBLEMS,
  GET_FOOD_PREFERENCES,
  GET_FOOD_ALLERGIES,
  GET_ACTIVE_LEVELS,
  GET_EQUIPMENTS,
  GET_SAMPLE_PROFILEPICS,
  MASTER_GET_FAIL,
  GET_TRAINEE_GOALS,
  GET_TRAINEE_GOALS_SUCCESS,
  GET_TRAINEE_GOALS_FAILURE,
  GET_TRAINEE_SUB_GOALS,
  GET_TRAINEE_SUB_GOALS_SUCCESS,
  SAVE_TRAINEE_DETAILS,
  SAVE_TRAINEE_DETAILS_SUCCESS,
  SAVE_TRAINEE_DETAILS_FAILURE,
  GET_LANGUAGES,
  GET_PROFESSIONS,
  GET_WORKOUT_LOCATIONS,
  GET_FITNESS_FORM,
} from '../actions/types';
const INITIAL_STATE = {
  error: '',
  loading: false,
  token: '',
  foodPreferences: [],
  healthConditions: [],
  foodAllergies: [],
  activeLevels: [],
  equipments: [],
  profilePics: [],
  goals: [],
  subGoals: [],
  languages: [],
  professions: [],
  workoutlocations: [],
  fitnessform: [],
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_HEALTH_PROBLEMS:
      return { ...state, healthConditions: action.payLoad };
    case GET_FOOD_PREFERENCES:
      return { ...state, foodPreferences: action.payLoad };
    case GET_FOOD_ALLERGIES:
      return { ...state, foodAllergies: action.payLoad };
    case GET_ACTIVE_LEVELS:
      return { ...state, activeLevels: action.payLoad };
    case GET_EQUIPMENTS:
      return { ...state, equipments: action.payLoad };
    case GET_SAMPLE_PROFILEPICS:
      return { ...state, profilePics: action.payLoad };
    case GET_LANGUAGES:
      return { ...state, languages: action.payLoad };
    case GET_PROFESSIONS:
      return { ...state, professions: action.payLoad };
    case GET_WORKOUT_LOCATIONS:
      return { ...state, workoutlocations: action.payLoad };
      case GET_FITNESS_FORM:
      return { ...state, fitnessform: action.payLoad };
    case MASTER_GET_FAIL:
      return {
        ...state,
        error: action.payLoad,
        loading: false,
      };
    case GET_TRAINEE_GOALS:
      return {
        ...state,
        loading: true,
        error: '',
      };
    case GET_TRAINEE_GOALS_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case GET_TRAINEE_GOALS_SUCCESS:
      return {
        ...state,
        goals: action.payload,
        loading: false,
        error: '',
      };
    case GET_TRAINEE_SUB_GOALS:
      return {
        ...state,
        loading: true,
        error: '',
      };
    case GET_TRAINEE_SUB_GOALS_SUCCESS:
      return {
        ...state,
        subGoals: action.payload,
        loading: false,
        error: '',
      };
    case SAVE_TRAINEE_DETAILS:
      return {
        ...state,
        loading: true,
        error: '',
      };
    case SAVE_TRAINEE_DETAILS_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case SAVE_TRAINEE_DETAILS_SUCCESS:
      return {
        ...state,
        goals: action.payload,
        loading: false,
        error: '',
      };
    default:
      return state;
  }
};
