import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    BackHandler,
    ScrollView,
    TextInput,
    Platform,
    Modal,
    Keyboard
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { getTraineeGoals, getTrainerCodes, uploadProfile } from '../actions';
import { CustomDialog, Loader } from './common';
import { withNavigationFocus } from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Dropdown } from 'react-native-material-dropdown';
import NetInfo from "@react-native-community/netinfo";
import { BASE_URL } from '../actions/types';
import { PieChart } from 'react-native-svg-charts';
import LogUtils from '../utils/LogUtils.js';
import AppIntroSlider from 'react-native-app-intro-slider';
import FastImage from 'react-native-fast-image';
import { allowFunction } from '../utils/ScreenshotUtils.js';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}
const modules = [
    'highcharts-more',
    'solid-gauge'
];
let tempChartObject;

function getCal(val, defVal) {
    var v1 = parseFloat(val);

    var value = parseInt((v1 * defVal).toFixed());

    if (value < 0) {
        value = 0;
    }
    return value;
}

function getPercentage(val, defVal, cal) {

    var v1 = parseFloat(val);

    var value = parseFloat(v1 * defVal) / cal;

    var value1 = parseFloat(value * 100).toFixed();

    return value1;
}

function getOtherCal(val1, val2, val3, cal) {

    var value1 = parseInt((val1 * 9).toFixed());
    var value2 = parseInt((val2 * 4).toFixed());
    var value3 = parseInt((val3 * 4).toFixed());

    var value = cal - (value1 + value2 + value3);

    if (value < 0) {
        value = 0;
    }
    return value.toFixed();
}

function getOtherPercentage(val1, val2, val3, cal) {

    var value1 = parseFloat(val1 * 9) / cal;
    var per1 = parseInt(value1 * 100).toFixed();
    var value2 = parseFloat(val2 * 4) / cal;
    var per2 = parseInt(value2 * 100).toFixed();
    var value3 = parseFloat(val3 * 4) / cal;
    var per3 = parseInt(value3 * 100).toFixed();

    var newVal = 100 - (parseInt(per1) + parseInt(per2) + parseInt(per3));

    return newVal;
}

function getOtherMG(val1, val2) {

    // var value2 = parseFloat(val2) / 1000;
    // var value3 = parseFloat(val3) / 1000;

    // var value = parseFloat(value2 + value3).toFixed(2);

    // if (value > 0) {
    //     // if (value < 0) {
    //     //     value = 0;
    //     // }

    //     return value;
    // }
    // else {
    //     return 0;
    // }
    var sodium = parseInt(val2.sodium) / 1000;
    var potassium = parseInt(val2.potassium) / 1000;
    var calcium = parseInt(val2.calcium) / 1000;
    var iron = parseInt(val2.iron) / 1000;
    var vitamin_a = parseInt(val2.vitamin_a) / 1000;
    var vitamin_c = parseInt(val2.vitamin_c) / 1000;

    var value = parseFloat(sodium + potassium + calcium + iron + vitamin_a + vitamin_c).toFixed(1);

    if (value > 0) {
        return value;
    }
    else {
        return 0;
    }
}

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

class Trainee_AddFood extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            isAlert: false,
            alertMsg: '',
            resObj: '',
            isSuccess: false,
            sucMsg: '',
            sucTit: '',
            selWeight: 0,
            selReps: 0,
            ufId: 0,
            fsId: 0,
            isUpdate: 0,
            selRepsText: '',
            selWeightText: '',
            selChartObject: {},
            measures: [],
            qty: '',
            selMeg: '',
            flag: '',
            fatPercentage: 10,
            carbPercentage: 20.6,
            proPercentage: 0,
            otherPercentage: 90.0,
            isShowInfoPopup: false,
            foodPhotos: [],
        };
        this.getFoodDetails = this.getFoodDetails.bind(this);
    }

    async componentDidMount() {
        try{
            allowFunction();
            if (this.props.uf_id) {
                this.setState({ ufId: this.props.uf_id, flag: this.props.flag, qty: this.props.qnty.toFixed(2), selMeg: this.props.measurement, isUpdate: this.props.isUpdate });
            }
    
            if (this.props.fsId) {
                this.setState({ fsId: this.props.fsId });
            }
    
            if (this.props.mainObject) {
                this.setState({ resObj: this.props.mainObject });
            }
            
            if (this.state.flag === 'Edit') {
                var values = [];
                var pos = 0;
                this.state.resObj.serving.map((item, index) => {
                    if (item.measurement_description === this.state.selMeg) {
                        pos = index;
                    }
                    values.push({ 'value': item.measurement_description });
                })
                
                this.setState({ measures: values, });
                const result = Object.assign({}, this.state.resObj.serving[pos]);
                tempChartObject = Object.assign({}, this.state.resObj.serving[pos]);
                this.setState({ selChartObject: result, qty: this.props.qnty.toFixed(2), selMeg: this.props.measurement });
    
                this.onSubmitQty();
            }
            else {
                const interval = setInterval(() => {
                    if (!isEmpty(this.state.resObj)) {
                        const result = Object.assign({}, this.state.resObj.serving[0]);
                        tempChartObject = Object.assign({}, this.state.resObj.serving[0]);
    
                        this.setState({ selChartObject: result, selMeg: this.state.resObj.serving[0].measurement_description });
    
                        var values = [];
                        this.state.resObj.serving.map((item) => {
                            values.push({ 'value': item.measurement_description });
                        })
                        
                        this.setState({ measures: values, qty: this.state.selChartObject.number_of_units });
                        clearInterval(interval);
                    }
                }, 100);
            }
            
            this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
                this.onBackPressed();
                return true;
            });
        }catch(error){
            console.log(error);
        }
    }

    onBackPressed() {
        Actions.pop();
        // if (this.state.flag === 'Edit') {
        //     Actions.traHomeFoodNew({ mdate: this.props.mdate });
        // }
        // else {
        //     Actions.traFoodList({ title: this.props.title, foodType: this.props.fType, mdate: this.props.mdate, mFrom: this.props.mFrom });
        // }
    }

    goToFoodHome() {
        if (this.props.mFrom === 'diet') {
            this.setState({ isSuccess: true });
        }
        else {
            Actions.traHomeFoodNew({ mdate: this.props.mdate });
        }
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    async getFoodDetails() {
        try {

            let token = await AsyncStorage.getItem('token');
            LogUtils.infoLog1(token, JSON.stringify({
                food_id: this.props.foodId,
                fs_id: this.props.fsId,
                is_update: this.state.isUpdate,
            }));
            fetch(
                `${BASE_URL}/trainee/getfooditemdetail`,
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${token}`,
                    },
                    body: JSON.stringify({
                        food_id: this.props.foodId,
                        fs_id: this.props.fsId,
                        is_update: this.state.isUpdate,
                    }),
                },
            )
                .then(processResponse)
                .then(res => {
                    const { statusCode, data } = res;
                    LogUtils.infoLog1('statusCode', statusCode);
                    LogUtils.infoLog1('data', data);
                    if (statusCode >= 200 && statusCode <= 300) {

                        if (this.state.flag === 'Edit') {

                            this.setState({ loading: false, resObj: data.data, });
                            var values = [];
                            var pos = 0;

                            this.state.resObj.serving.map((item, index) => {
                                if (item.measurement_description === this.state.selMeg) {
                                    pos = index;

                                }
                                values.push({ 'value': item.measurement_description });

                            })

                            this.setState({ measures: values, });
                            const result = Object.assign({}, data.data.serving[pos]);
                            tempChartObject = Object.assign({}, data.data.serving[pos]);
                            this.setState({ selChartObject: result, qty: this.props.qnty.toFixed(2), selMeg: this.props.measurement });

                            this.onSubmitQty();

                        }
                        else {
                            const result = Object.assign({}, data.data.serving[0]);
                            tempChartObject = Object.assign({}, data.data.serving[0]);

                            this.setState({ loading: false, resObj: data.data, selChartObject: result, selMeg: data.data.serving[0].measurement_description });

                            var values = [];
                            this.state.resObj.serving.map((item) => {
                                values.push({ 'value': item.measurement_description });
                            })
                            this.setState({ measures: values, qty: this.state.selChartObject.number_of_units });

                        }
                    } else {
                        if (data.message === 'You are not authenticated!') {
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                        } else {
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                        }
                    }
                })
                .catch(function (error) {
                    this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
                });
        } catch (error) {
            console.log(error);
        }
    }

    async saveFood() {
        try {
            if (this.state.qty > 0) {
                LogUtils.infoLog1('pwd_id ', JSON.stringify({
                    mt_id: this.props.fType,
                    name: this.state.resObj.food_name,
                    qnty: this.state.qty,
                    measurement: this.state.selMeg,
                    cal: this.state.selChartObject.calories,
                    carb: this.state.selChartObject.carbohydrate,
                    protein: this.state.selChartObject.protein,
                    fat: this.state.selChartObject.fat,
                    food_id: this.state.resObj.food_id,
                    uf_id: this.state.ufId,
                    sel_date: this.props.mdate,
                    fs_id: this.state.fsId,
                }));
                this.setState({ loading: true });
                let token = await AsyncStorage.getItem('token');

                fetch(
                    `${BASE_URL}/trainee/fooddet`,
                    {
                        method: 'POST',
                        headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                            Authorization: `Bearer ${token}`,
                        },
                        body: JSON.stringify({
                            mt_id: this.props.fType,
                            name: this.state.resObj.food_name,
                            qnty: parseFloat(this.state.qty).toFixed(2),
                            measurement: this.state.selMeg,
                            cal: this.state.selChartObject.calories,
                            carb: this.state.selChartObject.carbohydrate,
                            protein: this.state.selChartObject.protein,
                            fat: this.state.selChartObject.fat,
                            food_id: this.state.resObj.food_id,
                            uf_id: this.state.ufId,
                            sel_date: this.props.mdate,
                            fs_id: this.state.fsId,
                        }),
                    },
                )
                    .then(processResponse)
                    .then(res => {
                        const { statusCode, data } = res;
                        LogUtils.infoLog1('statusCode', statusCode);
                        LogUtils.infoLog1('data', data);
                        if (statusCode >= 200 && statusCode <= 300) {
                            this.setState({ loading: false, sucTit: data.title, sucMsg: data.message });
                            this.goToFoodHome();
                        } else {
                            if (data.message === 'You are not authenticated!') {
                                this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                            } else {
                                this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                            }
                        }
                    })
                    .catch(function (error) {
                        this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
                    });
            } else {
                this.setState({
                    isAlert: true, alertMsg: "Please enter valid Quantity."
                })
            }
        } catch (error) {
            console.log(error);
        }
    }

    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
        this.setState({ alertMsg: '' });
        this.setState({ isAlert: false });
    }

    async onSuccess() {
        // const interval = setInterval(() => {
        this.setState({ isSuccess: false, sucMsg: '' });
        // Actions.thome();
        Actions.pop();
        // this.setState({ loading: true });
        // clearInterval(interval);
        // }, 100);
    }

    async onSubmitQty() {
        try{
            if (this.state.qty > 0 ) {

                var v1 = parseFloat(this.state.qty).toFixed(2);
                var v2 = parseFloat(this.state.selChartObject.number_of_units).toFixed(2);
    
                if (v1 !== v2) {
    
                    this.state.selChartObject.calcium = (parseFloat(tempChartObject.calcium) / v2 * v1).toFixed(2);
                    this.state.selChartObject.calories = parseInt(parseFloat(tempChartObject.calories) / v2 * v1);
                    this.state.selChartObject.calories = isNaN(this.state.selChartObject.calories) ? 0 : this.state.selChartObject.calories;
                    this.state.selChartObject.carbohydrate = (parseFloat(tempChartObject.carbohydrate) / v2 * v1).toFixed(2);
                    this.state.selChartObject.cholesterol = (parseFloat(tempChartObject.cholesterol) / v2 * v1).toFixed(2);;
                    this.state.selChartObject.fat = (parseFloat(tempChartObject.fat) / v2 * v1).toFixed(2);
                    this.state.selChartObject.fiber = (parseFloat(tempChartObject.fiber) / v2 * v1).toFixed(2);
                    this.state.selChartObject.iron = (parseFloat(tempChartObject.iron) / v2 * v1).toFixed(2);
                    // this.state.selChartObject.measurement_description = item.measurement_description;
                    // this.state.selChartObject.metric_serving_amount = item.metric_serving_amount;
                    // this.state.selChartObject.metric_serving_unit = item.metric_serving_unit;
                    this.state.selChartObject.monounsaturated_fat = (parseFloat(tempChartObject.monounsaturated_fat) / v2 * v1).toFixed(2);
                    // this.state.selChartObject.number_of_units = item.number_of_units;
                    this.state.selChartObject.polyunsaturated_fat = (parseFloat(tempChartObject.polyunsaturated_fat) / v2 * v1).toFixed(2);
                    this.state.selChartObject.potassium = (parseFloat(tempChartObject.potassium) / v2 * v1).toFixed(2);
                    this.state.selChartObject.protein = (parseFloat(tempChartObject.protein) / v2 * v1).toFixed(2);
                    this.state.selChartObject.saturated_fat = ((tempChartObject.saturated_fat) / v2 * v1).toFixed(2);            
                    // this.state.selChartObject.serving_description = item.serving_description;
                    // this.state.selChartObject.serving_id = item.serving_id;
                    // this.state.selChartObject.serving_url = item.serving_url;
                    this.state.selChartObject.sodium = (parseFloat(tempChartObject.sodium) / v2 * v1).toFixed(2);
                    this.state.selChartObject.sugar = (parseFloat(tempChartObject.sugar) / v2 * v1).toFixed(2);
                    this.state.selChartObject.vitamin_a = (parseFloat(tempChartObject.vitamin_a) / v2 * v1).toFixed(2);
                    this.state.selChartObject.vitamin_c = (parseFloat(tempChartObject.vitamin_c) / v2 * v1).toFixed(2);
    
                    // console.log(v1, v2, this.state.selChartObject.fat);
                    // console.log('Temp Obj', this.state.selChartObject);
                    // console.log('Temp Obj1', tempChartObject);
                    // this.setState({ selChartObject: tempObject });
    
                }
                else {
                    //console.log(v1, v2, this.state.resObj.serving);
                    this.state.resObj.serving.map((item) => {
                        if (item.measurement_description === this.state.selMeg) {
                            // console.log('sel object', item);
                            // console.log(v1, v2, this.state.selChartObject);
    
                            this.state.selChartObject.calcium = item.calcium;
                            this.state.selChartObject.calories = item.calories;
                            this.state.selChartObject.carbohydrate = item.carbohydrate;
                            this.state.selChartObject.cholesterol = item.cholesterol;
                            this.state.selChartObject.fat = item.fat;
                            this.state.selChartObject.fiber = item.fiber;
                            this.state.selChartObject.iron = item.iron;
                            this.state.selChartObject.measurement_description = item.measurement_description;
                            this.state.selChartObject.metric_serving_amount = item.metric_serving_amount;
                            this.state.selChartObject.metric_serving_unit = item.metric_serving_unit;
                            this.state.selChartObject.monounsaturated_fat = item.monounsaturated_fat;
                            this.state.selChartObject.number_of_units = item.number_of_units;
                            this.state.selChartObject.polyunsaturated_fat = item.polyunsaturated_fat;
                            this.state.selChartObject.potassium = item.potassium;
                            this.state.selChartObject.protein = item.protein;
                            this.state.selChartObject.saturated_fat = item.saturated_fat;
                            this.state.selChartObject.serving_description = item.serving_description;
                            this.state.selChartObject.serving_id = item.serving_id;
                            this.state.selChartObject.serving_url = item.serving_url;
                            this.state.selChartObject.sodium = item.sodium;
                            this.state.selChartObject.sugar = item.sugar;
                            this.state.selChartObject.vitamin_a = item.vitamin_a;
                            this.state.selChartObject.vitamin_c = item.vitamin_c;
    
                            // console.log(v1, v2, this.state.selChartObject);
                        }
                    });
                }
    
                // chat refresh
                var conf = {
                    chart: {
                        margin: [0, 0, 0, 10],
                        plotBackgroundColor: null,
                        plotBorderWidth: 0,
                        plotShadow: false,
                        spacingTop: 0,
                    },
                    credits: {
                        enabled: false
                    },
                    title: {
                        text: `<span style="color:#000;font-weight:500;font-size:30px">${this.state.selChartObject.calories}</span> <br>  <span style="color:#6d819c;font-size:14px;">Calories </span> <br> <span style="color:#ffffff;font-size:5px"> s</span> <br> <span style="color:#ff5e3a;font-size:14px"> So Great </span>`,
                        align: 'center',
                        verticalAlign: Platform.OS === 'ios' ? 'top' : 'middle',
                        y: Platform.OS === 'ios' ? 85 : 50,
                        x: Platform.OS === 'ios' ? -70 : -65,
                    },
                    // tooltip: {
                    //     pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    // },
                    colors: ['#5ac8fb', '#8c52ff', '#ff5e3a', '#44db5e'],
                    plotOptions: {
                        pie: {
                            dataLabels: {
                                enabled: false,
                                distance: -10,
                                style: {
                                    fontWeight: 'bold',
                                    color: 'white'
                                }
                            },
                            startAngle: 0,
                            endAngle: 360,
                            center: ['25%', Platform.OS === 'ios' ? '20%' : '60%'],
                            size: Platform.OS === 'ios' ? '45%' : '90%',
                            showInLegend: true,
                            allowPointSelect: false,
                            point: {
                                events: {
                                    legendItemClick: function (e) {
                                        e.preventDefault();
                                    }
                                }
                            }
                        },
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: Platform.OS === 'ios' ? 'top' : 'middle',
                        x: 10,
                        y: 20,
                        floating: false,
                    },
                    series: [{
                        enableMouseTracking: false,
                        type: 'pie',
                        name: 'Browser share',
                        innerSize: '80%',
                        data: [
                            [`<span style="color:#000000;font-weight:500;font-size:14px">Fat</span> <br>  <span style="color:#6d819c;font-weight:400;font-size:12px;">
                            ${getPercentage(this.state.selChartObject.fat, 9, this.state.selChartObject.calories)}% ~ ${getCal(this.state.selChartObject.fat, 9)} cal
                            </span> <br>  <span style="color:#ffffff;font-weight:200;font-size:5px;">c</span>`,
                            getCal(this.state.selChartObject.fat, 9)],
                            [`<span style="color:#000000;font-weight:500;font-size:14px">Carbs</span> <br>  <span style="color:#6d819c;font-weight:400;font-size:12px;">${getPercentage(this.state.selChartObject.carbohydrate, 4, this.state.selChartObject.calories)}% ~ ${getCal(this.state.selChartObject.carbohydrate, 4)} cal</span><br>  <span style="color:#ffffff;font-weight:200;font-size:5px;">c</span>`, getCal(this.state.selChartObject.carbohydrate, 4)],
                            [`<span style="color:#000000;font-weight:500;font-size:14px">Protein</span> <br>  <span style="color:#6d819c;font-weight:400;font-size:12px;">${getPercentage(this.state.selChartObject.protein, 4, this.state.selChartObject.calories)}% ~ ${getCal(this.state.selChartObject.protein, 4)} cal</span><br>  <span style="color:#ffffff;font-weight:200;font-size:5px;">c</span>`, getCal(this.state.selChartObject.protein, 4)],
                            [`<span style="color:#000000;font-weight:500;font-size:14px">Others</span> <br>  <span style="color:#6d819c;font-weight:400;font-size:12px;">${getOtherPercentage(this.state.selChartObject.fat, this.state.selChartObject.carbohydrate, this.state.selChartObject.protein, this.state.selChartObject.calories)}% ~ ${getOtherCal(this.state.selChartObject.fat, this.state.selChartObject.carbohydrate, this.state.selChartObject.protein, this.state.selChartObject.calories)} cal</span><br>  <span style="color:#ffffff;font-weight:200;font-size:5px;">c</span>`, getOtherCal(this.state.selChartObject.fat, this.state.selChartObject.carbohydrate, this.state.selChartObject.protein, this.state.selChartObject.calories)],
                        ]
                    }]
                };
                // console.log(months)
                // console.log(weights)
                this.setState({ chartOptions: conf })
            }else{
                this.setState({
                    isAlert:true,alertMsg:"Please enter valid Quantity."
                })
            }
        }catch(error){
            console.log(error);
        }
    }

    renderChartFood() {
        if (!isEmpty(this.state.resObj)) {

            return (
                <View style={{ flexDirection: 'row', marginTop: 30, }}>
                    <View style={{ flexDirection: 'row', height: 180, width: wp('60%'), justifyContent: 'center', }}>
                        <PieChart
                            style={{ height: 170, width: wp('60%'), }}
                            valueAccessor={({ item }) => item.amount}
                            data={[
                                {
                                    key: 1,
                                    amount: getCal(this.state.selChartObject.fat, 9),
                                    svg: { fill: '#5ac8fb' },
                                },
                                {
                                    key: 2,
                                    amount: getCal(this.state.selChartObject.carbohydrate, 4),
                                    svg: { fill: '#8c52ff' }
                                },
                                {
                                    key: 3,
                                    amount: getCal(this.state.selChartObject.protein, 4),
                                    svg: { fill: '#ff5e3a' }
                                },
                                {
                                    key: 4,
                                    amount: getOtherCal(this.state.selChartObject.fat, this.state.selChartObject.carbohydrate, this.state.selChartObject.protein, this.state.selChartObject.calories),
                                    svg: { fill: '#44db5e' }
                                }
                                // ['#5ac8fb', '#5856d6', '#ff5e3a', '#44db5e'],
                            ]}
                            spacing={0}
                            outerRadius={'95%'}
                            innerRadius={'78%'}
                            // padAngle={'2%'}
                            animate={true}
                            animationDuration={5000}
                        >
                        </PieChart>
                        <View style={{ position: 'absolute', alignItems: 'center', alignSelf: 'center', justifyContent: 'center' }}>
                            <Text style={styles.textCaloriesBold}>{this.state.selChartObject.calories}</Text>
                            <Text style={styles.textCalories}>Calories</Text>
                        </View>
                    </View>
                    <View style={{ height: 180, flexDirection: 'column', alignSelf: 'center', alignContent: 'center', alignItems: 'center', justifyContent: 'center' }}>
                        <View style={styles.nutrition1}>
                            <View style={styles.viewChartFatDot}></View>
                            <View style={{ flexDirection: 'column', marginLeft: 5, }}>
                                <Text style={styles.textChartTit}>Fat</Text>
                                <Text style={styles.textChartDesc}>{getPercentage(this.state.selChartObject.fat, 9, this.state.selChartObject.calories)}% ~ {getCal(this.state.selChartObject.fat, 9)} cal</Text>
                            </View>
                        </View>
                        <View style={{ marginTop: 5, }}></View>
                        <View style={styles.nutrition1}>
                            <View style={styles.viewChartCarbDot}></View>
                            <View style={{ flexDirection: 'column', marginLeft: 5, }}>
                                <Text style={styles.textChartTit}>Carbs</Text>
                                <Text style={styles.textChartDesc}>{getPercentage(this.state.selChartObject.carbohydrate, 4, this.state.selChartObject.calories)}% ~ {getCal(this.state.selChartObject.carbohydrate, 4)} cal</Text>
                            </View>
                        </View>
                        <View style={{ marginTop: 5, }}></View>
                        <View style={styles.nutrition1}>
                            <View style={styles.viewChartProteinDot}></View>
                            <View style={{ flexDirection: 'column', marginLeft: 5, }}>
                                <Text style={styles.textChartTit}>Protein</Text>
                                <Text style={styles.textChartDesc}>{getPercentage(this.state.selChartObject.protein, 4, this.state.selChartObject.calories)}% ~ {getCal(this.state.selChartObject.protein, 4)} cal</Text>
                            </View>
                        </View>
                        <View style={{ marginTop: 5, }}></View>
                        <View style={styles.nutrition1}>
                            <View style={styles.viewChartOtherDot}></View>
                            <View style={{ flexDirection: 'column', marginLeft: 5, }}>
                                <Text style={styles.textChartTit}>Others</Text>
                                <Text style={styles.textChartDesc}>{getOtherPercentage(this.state.selChartObject.fat, this.state.selChartObject.carbohydrate, this.state.selChartObject.protein, this.state.selChartObject.calories)}% ~ {getOtherCal(this.state.selChartObject.fat, this.state.selChartObject.carbohydrate, this.state.selChartObject.protein, this.state.selChartObject.calories)} cal</Text>
                            </View>
                        </View>
                    </View>
                </View>
            );
        }
        else {
            return (
                <View style={{
                    width: wp('80%'),
                    alignItems: 'center',
                    height: 180,
                    alignSelf: 'center',
                }}></View>
            );
        }
    }

    nutritionDetails(data) {
        try {
            //data= '0.00';
            var value = "";
            var value1 = "";
            if(data !=undefined){
                data = parseFloat(data).toFixed(2);
                if (data.includes('.')) {
                    var val = data.split('.');
                    value1 = val[1] == (0 || '00')  ? '' : val[1] > 9 ?  "." + val[1].replace('0','') : "."+ val[1];
                    value = val[0] + value1;
                    return value;
                } else {
                    return data;
                }
            }
        } catch (error) {
            console.log(error);
        }
    }

    render() {
        return (
            <View style={styles.containerStyle}>
                <Loader loading={this.state.loading} />
                <ScrollView
                 //keyboardShouldPersistTaps='always'
                 keyboardShouldPersistTaps='handled'
                    contentContainerStyle={{ paddingBottom: hp('15%') }}>
                    <View style={styles.mainView}>

                        {/* title and cal details */}
                        <View style={styles.viewTitleCal}>
                            <TouchableOpacity
                                style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                                onPress={() => this.onBackPressed()}>
                                <Image
                                    source={require('../res/ic_back.png')}
                                    style={styles.backImageStyle}
                                />
                            </TouchableOpacity>
                            <View style={{ marginTop: 20, }} />

                            {/* title */}
                            <Text style={styles.textStyle}>{this.state.resObj.food_name}</Text>

                            {/* circular menus */}
                            {this.renderChartFood()}

                        </View>
                        <Image
                            source={require('../res/ic_tab_bg.png')}
                            style={{ width: wp('100%'), height: 63, }}
                        />

                        <Text style={styles.textSelQty}>Quantity</Text>

                        <View style={styles.spinnersBg}>
                           
                            <TextInput
                                style={styles.textInputStyle}
                                keyboardType="numeric"
                                placeholder="Qty"
                                placeholderTextColor="grey"
                                maxLength={5}
                                value={this.state.qty}
                                returnKeyType='done'
                                onChangeText={text => {
                                    this.setState({ qty: text.replace(/[^0-9.]/g, ''), })
                                }}
                                onSubmitEditing={() => this.onSubmitQty()}
                                onEndEditing={() => this.onSubmitQty()}
                                />
                               
                        </View>
                        <View style={{ marginTop: 10, }}></View>

                        <View style={{ flexDirection: 'row' }}>
                            <Text style={styles.textSelQty}>Portion Size</Text>
                            {this.state.resObj.serving_img && this.state.resObj.serving_img.length
                                ?
                                (
                                    <TouchableOpacity
                                        onPress={() => {
                                            this.state.resObj.serving_img.map((imgitem) => {
                                                this.state.foodPhotos.push(imgitem);
                                            })
                                            this.setState({ isShowInfoPopup: true, });
                                            // this.chooseImage();
                                        }}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            source={require('../res/ic_eye.png')}
                                            style={{
                                                width: 18,
                                                height: 18,
                                                alignSelf: 'center',
                                                marginLeft: 10,
                                                tintColor: '#8c52ff'
                                            }} />
                                    </TouchableOpacity>
                                )
                                :
                                (
                                    <View />
                                )}
                        </View>

                        <View
                            style={styles.spinnersBg}>
                            <View style={{
                                paddingLeft: 10,
                                paddingRight: 10, width: wp('90%')
                            }}>
                                {this.state.measures.length == 1
                                    ? (
                                        <Text style={styles.textMeasurement}>{this.state.selMeg}</Text>
                                    )
                                    : (
                                        <Dropdown
                                            ref={this.codeRef}
                                            // label='Quantity of Food'
                                            value={this.state.selMeg}
                                            data={this.state.measures}
                                            // labelFontSize={10}
                                            //dropdownOffset={{ top: 15 }}
                                            // dropdownMargins={{min: 5}}
                                            fontSize={14}
                                            // disabled={this.state.measures.length == 1}
                                            onChangeText={(value) => {
                                                this.setState({ selMeg: value });
                                                this.state.resObj.serving.map((item) => {
                                                    if (item.measurement_description === value) {
                                                        tempChartObject = Object.assign({}, item);
                                                        this.setState({ selChartObject: item });
                                                    }
                                                });
                                                this.setState({ qty: this.state.selChartObject.number_of_units });

                                            }}
                                        />
                                    )
                                }


                            </View>
                        </View>

                        <View style={{ marginTop: 15, }}></View>

                        {/* nutrician detail */}
                        <View style={styles.breakfastBg}>
                            <View style={{ flexDirection: 'row', padding: 15, }}>
                                <Text style={styles.textBreakfast}>{'Nutrition detail'.toUpperCase()}</Text>
                                <TouchableOpacity onPress={() => {
                                    if (this.state.resObj) {
                                        Actions.traFoodDet({ title: this.state.resObj.food_name, selChartObject: this.state.selChartObject });
                                    }
                                }}>

                                    <Text style={styles.textAdd}>More Nutrition Facts</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ height: 1, backgroundColor: '#e9e9e9', }}></View>
                            {/* fat detail */}
                            <View style={styles.nutrition}>
                                <View style={styles.nutrition1}>
                                    <View style={styles.viewFatDot}></View>
                                    <View style={styles.nutritionInnerView}>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Text style={styles.textNutTitle}>Fat</Text>
                                            <Text style={styles.textNutTitle1}>{this.state.selChartObject.fat} g</Text>
                                        </View>
                                        { this.state.selChartObject.saturated_fat  > 0
                                            ? (
                                                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, }}>
                                                    <Text style={styles.textNutDesc}>Saturated Fat</Text>
                                                    <Text style={styles.textPro}>{this.state.selChartObject.saturated_fat} g</Text>
                                                </View>
                                            )
                                            : (
                                                <View />
                                            )}

                                        {this.state.selChartObject.polyunsaturated_fat   > 0
                                            ? (
                                                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, }}>
                                                    <Text style={styles.textNutDesc}>Polyunsaturated Fat</Text>
                                                    <Text style={styles.textPro}>{this.state.selChartObject.polyunsaturated_fat} g</Text>
                                                </View>
                                            )
                                            : (
                                                <View />
                                            )}

                                        {this.state.selChartObject.cholesterol  > 0
                                            ? (
                                                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, }}>
                                                    <Text style={styles.textNutDesc}>Cholesterol</Text>
                                                    <Text style={styles.textPro}>{this.state.selChartObject.cholesterol} mg</Text>
                                                </View>
                                            )
                                            : (
                                                <View />
                                            )}
                                    </View>
                                </View>
                            </View>
                            {/* carbs detail */}
                            <View style={styles.nutrition}>
                                <View style={styles.nutrition1}>
                                    <View style={styles.viewCarbsDot}></View>
                                    <View style={styles.nutritionInnerView}>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Text style={styles.textNutTitle}>Carbs</Text>
                                            <Text style={styles.textNutTitle1}>{this.state.selChartObject.carbohydrate} g</Text>
                                        </View>
                                        {this.state.selChartObject.fiber  > 0
                                            ? (
                                                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, }}>
                                                    <Text style={styles.textNutDesc}>Fiber</Text>
                                                    <Text style={styles.textPro}>{parseFloat(this.state.selChartObject.fiber).toFixed(2)} g</Text>
                                                </View>
                                            )
                                            : (
                                                <View />
                                            )}
                                        
                                        {this.state.selChartObject.sugar  > 0
                                            ? (
                                                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, }}>
                                                    <Text style={styles.textNutDesc}>Sugars</Text>
                                                    <Text style={styles.textPro}>{this.state.selChartObject.sugar} g</Text>
                                                </View>
                                            )
                                            : (
                                                <View />
                                            )}
                                    </View>
                                </View>
                            </View>
                            {/* Protein detail */}
                            <View style={styles.nutrition}>
                                <View style={styles.nutrition1}>
                                    <View style={styles.viewProteinDot}></View>
                                    <View style={styles.nutritionInnerView}>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Text style={styles.textNutTitle}>Protein</Text>
                                            <Text style={styles.textNutTitle1}>{this.state.selChartObject.protein} g</Text>
                                        </View>
                                    </View>
                                </View>
                            </View>

                            <View style={styles.nutrition}>
                                <View style={styles.nutrition1}>
                                    <View style={styles.viewOthersDot}></View>
                                    <View style={styles.nutritionInnerView}>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Text style={styles.textNutTitle}>Others</Text>
                                            <Text style={styles.textNutTitle1}>{getOtherMG(0, this.state.selChartObject)} g</Text>
                                        </View>
                                        
                                        {this.state.selChartObject.sodium  > 0
                                            ? (
                                                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, }}>
                                                    <Text style={styles.textNutDesc}>Sodium</Text>
                                                    <Text style={styles.textPro}>{this.state.selChartObject.sodium} mg</Text>
                                                </View>
                                            )
                                            : (
                                                <View />
                                            )}

                                        {this.state.selChartObject.potassium  > 0
                                            ? (
                                                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, }}>
                                                    <Text style={styles.textNutDesc}>Potassium</Text>
                                                    <Text style={styles.textPro}>{this.state.selChartObject.potassium} mg</Text>
                                                </View>
                                            )
                                            : (
                                                <View />
                                            )}

                                    </View>
                                </View>
                            </View>
                        </View>

                    </View>
                </ScrollView>

                <CustomDialog
                    visible={this.state.isAlert}
                    title='Alert'
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    no=''
                    yes='Ok' />

                <CustomDialog
                    visible={this.state.isSuccess}
                    title={this.state.sucTit}
                    desc={this.state.sucMsg}
                    onAccept={this.onSuccess.bind(this)}
                    no=''
                    yes='Ok' />

                <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                    <TouchableOpacity
                        style={styles.buttonTuch}
                        onPress={() => {
                            this.saveFood();
                        }}
                    >
                        <Text style={styles.buttonText}>Add to {this.props.title}</Text>
                    </TouchableOpacity>
                </LinearGradient>


                <Modal
                    transparent={true}
                    animationType={'none'}
                    visible={this.state.isShowInfoPopup}
                    onRequestClose={() => {
                        //console.log('close modal');
                    }}>
                    <View style={{
                        backgroundColor: '#ffffff',
                        // position: 'relative',
                        flex: 1,
                        justifyContent: 'center',
                    }}>

                        <View style={{ flex: 1, backgroundColor: '#000000', justifyContent: 'center' }}>
                            <AppIntroSlider
                                slides={this.state.foodPhotos}
                                // onDone={this._onDone}
                                showSkipButton={false}
                                showDoneButton={false}
                                showNextButton={false}
                                dotStyle={{ backgroundColor: '#ffffff' }}
                                activeDotStyle={{ backgroundColor: '#8c52ff' }}
                                renderItem={({ item }) => {
                                    return <FastImage
                                        style={{ width: wp('90%'), height: hp('80%'), flex: 1, alignSelf: 'center' }}
                                        source={{
                                            uri: item.img,
                                            headers: { Authorization: '12345' },
                                            priority: FastImage.priority.high,
                                        }}
                                        resizeMode={FastImage.resizeMode.contain}
                                    />
                                }}
                            />
                            <TouchableOpacity style={styles.viewTop}
                                onPress={() => this.setState({ isShowInfoPopup: false, foodPhotos: [] })}>
                                <Image
                                    source={require('../res/ic_back.png')}
                                    style={styles.popBackImageStyle}
                                />
                            </TouchableOpacity>
                        </View>

                    </View>
                </Modal>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        height: '100%',
        backgroundColor: '#ffffff',
        flex: 1,
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'center',
    },
    mainView: {
        flexDirection: 'column',
    },
    viewTitleCal: {
        flexDirection: 'column',
        marginLeft: 20,
        marginTop: 20,
        backgroundColor: '#ffffff',
        marginRight: 20,
    },
    textStyle: {
        fontSize: 16,
        marginLeft: 40,
        marginRight: 40,
        fontWeight: '500',
        color: '#2d3142',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    spinnersBg: {
        width: wp('90%'),
        marginLeft: wp('5%'),
        marginRight: wp('5%'),
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        flexDirection: 'column',
        borderRadius: 6,
        position: 'relative',
        alignContent: 'center',
        alignSelf: 'center',
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 6,
    },
    textSelQty: {
        color: '#8c52ff',
        marginLeft: wp('5%'),
        fontFamily: 'Rubik-Medium',
        alignSelf: "flex-start",
        fontSize: 12,
        // flex: 1,
        marginBottom: 5,
        textAlign: 'left',
        fontWeight: '500',
    },
    calViewDet: {
        alignItems: 'center',
        alignSelf: 'center',
    },
    container: {
        width: wp('80%'),
        height: 200,
        borderColor: '#000000',
        borderRadius: 2,
        alignSelf: 'center',
        backgroundColor: '#000000',
        justifyContent: 'center'
    },
    breakfastBg: {
        width: wp('90%'),
        marginLeft: wp('5%'),
        marginRight: wp('5%'),
        marginTop: wp('1%'),
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'column',
        borderRadius: 6,
        position: 'relative',
        alignSelf: 'center',
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 6,
    },
    textBreakfast: {
        color: '#282c37',
        fontFamily: 'Rubik-Medium',
        fontSize: 12,
        flex: 1,
        alignSelf: "flex-start",
        fontWeight: '500',
    },
    textAdd: {
        color: '#8c52ff',
        fontFamily: 'Rubik-Medium',
        alignSelf: "flex-end",
        fontSize: 11,
        flex: 1,
        textAlign: 'right',
        fontWeight: '500',
    },
    nutrition: {
        flexDirection: 'column',
        padding: 15,
    },
    nutrition1: {
        flexDirection: 'row',
        alignSelf: 'flex-start',
    },
    nutritionInnerView: {
        flexDirection: 'column',
        marginLeft: wp('3%'),
        justifyContent: 'flex-start',
        alignSelf: 'flex-start',
        flex: 1,
    },
    textNutTitle: {
        fontSize: 14,
        fontWeight: '500',
        color: '#2d3142',
        flex: 1,
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.23,
    },
    textNutTitle1: {
        fontSize: 14,
        fontWeight: '500',
        color: '#2d3142',
        textAlign: 'right',
        fontFamily: 'Rubik-Medium',
        marginLeft: 8,
        letterSpacing: 0.23,
    },
    textNutDesc: {
        fontSize: 12,
        fontWeight: '400',
        color: '#282c37',
        lineHeight: 18,
        flex: 1,
        fontFamily: 'Rubik-Regular',
        letterSpacing: 0.2,
    },
    textPro: {
        fontSize: 10,
        fontWeight: '500',
        color: '#ffffff',
        borderRadius: 10,
        backgroundColor: '#8c52ff',
        padding: 5,
        textAlign: 'right',
        fontFamily: 'Rubik-Medium',
        marginLeft: 8,
        letterSpacing: 0.23,
        overflow: 'hidden',
    },
    viewFatDot: {
        width: 15,
        height: 15,
        alignSelf: 'flex-start',
        backgroundColor: '#5ac8fb',
        borderRadius: 15 / 2,
    },
    viewCarbsDot: {
        width: 15,
        height: 15,
        alignSelf: 'flex-start',
        backgroundColor: '#8c52ff',
        borderRadius: 15 / 2,
    },
    viewProteinDot: {
        width: 15,
        height: 15,
        alignSelf: 'flex-start',
        backgroundColor: '#ff5e3a',
        borderRadius: 15 / 2,
    },
    viewOthersDot: {
        width: 15,
        height: 15,
        alignSelf: 'flex-start',
        backgroundColor: '#44db5e',
        borderRadius: 15 / 2,
    },
    linearGradient: {
        width: '90%',
        height: 50,
        bottom: 0,
        borderRadius: 25,
        justifyContent: 'center',
        position: 'absolute',
        alignSelf: 'center',
        marginBottom: 15,
        zIndex:0
    },
    buttonTuch:
    {
        backgroundColor: 'transparent',
    },
    buttonText: {
        fontSize: 14,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    textInputStyle: {
        height: 45,
        width: wp('90%'),
        flex: 1,
        alignSelf: 'flex-start',
        fontSize: 14,
        padding: 10,
        fontWeight: '400',
        color: '#2d3142',
        letterSpacing: .5,
        fontFamily: 'Rubik-Regular',
    },
    textMeasurement: {
        width: wp('85%'),
        flex: 1,
        alignSelf: 'flex-start',
        fontSize: 14,
        paddingTop: 14,
        paddingBottom: 14,
        fontWeight: '400',
        color: '#2d3142',
        fontFamily: 'Rubik-Regular',
    },
    textCaloriesBold: {
        alignSelf: 'center',
        fontSize: 25,
        fontWeight: '400',
        color: '#2d3142',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    textCalories: {
        alignSelf: 'center',
        fontSize: 13,
        fontWeight: '400',
        color: '#6d819c',
        textAlign: 'center',
        fontFamily: 'Rubik-Regular',
    },
    viewChartFatDot: {
        width: 12,
        height: 12,
        backgroundColor: '#5ac8fb',
        marginTop: 2,
        alignSelf: 'flex-start',
        borderRadius: 6,
    },
    viewChartCarbDot: {
        width: 12,
        height: 12,
        backgroundColor: '#8c52ff',
        marginTop: 2,
        alignSelf: 'flex-start',
        borderRadius: 6,
    },
    viewChartProteinDot: {
        width: 12,
        height: 12,
        backgroundColor: '#ff5e3a',
        marginTop: 2,
        alignSelf: 'flex-start',
        borderRadius: 6,
    },
    viewChartOtherDot: {
        width: 12,
        height: 12,
        backgroundColor: '#44db5e',
        marginTop: 2,
        alignSelf: 'flex-start',
        borderRadius: 6,
    },
    textChartTit: {
        fontSize: 12,
        fontWeight: '500',
        color: '#2d3142',
        fontFamily: 'Rubik-Medium',
        textAlign: 'left',
    },
    textChartDesc: {
        fontSize: 10,
        fontWeight: '400',
        color: '#6d819c',
        textAlign: 'left',
        fontFamily: 'Rubik-Regular',
    },
    viewTop: {
        flexDirection: 'column',
        position: 'absolute',
        margin: 15,
        alignItems: 'flex-start',
        padding: 5,
        justifyContent: 'center',
        width: 40,
        height: 40,
        borderRadius: 20,
        backgroundColor: 'white',
        // zIndex: 100,
        top: 1,
    },
    popBackImageStyle: {
        width: 19,
        height: 16,
        tintColor: '#8c52ff',
        alignSelf: 'center',
    },

});

const mapStateToProps = state => {
    const { goals, trainerCodes } = state.procre;
    const loading = state.procre.loading;
    const error = state.procre.error;
    return { loading, error, goals, trainerCodes };
};

export default withNavigationFocus(
    connect(
        mapStateToProps,
        { getTraineeGoals, getTrainerCodes, uploadProfile },
    )(Trainee_AddFood),
);

// export default ProfileCreation;
