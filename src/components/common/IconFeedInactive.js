import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import Svg, { Path } from "react-native-svg";


  const IconFeedInactive = props => {
  return (
    <View style={[styles.root, props.style]}>
      <View style={styles.group6}>
        <View style={styles.path37Stack}>
          <Svg viewBox="-0 -0 16.15 11.4" style={styles.path37}>
            <Path
              strokeWidth={0}
              fill="rgba(214,217,224,1)"
              d="M1.21 0.00 L3.04 0.00 L4.25 1.27 L4.25 10.13 L3.04 11.40 L1.21 11.40 L0.00 10.13 L0.00 1.27 L1.21 0.00 Z M7.16 0.00 L8.99 0.00 L10.20 1.27 L10.20 10.13 L8.99 11.40 L7.16 11.40 L5.95 10.13 L5.95 1.27 L7.16 0.00 Z M13.11 0.00 L14.94 0.00 L16.15 1.27 L16.15 10.13 L14.94 11.40 L13.11 11.40 L11.90 10.13 L11.90 1.27 L13.11 0.00 Z"
            ></Path>
          </Svg>
          <View style={styles.rectangle48}></View>
          <View style={styles.rectangleCopy2}></View>
          <View style={styles.rectangleCopy22}></View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1
  },
  group6: {
    width: 17,
    height: 17,
    marginTop: 4,
    marginLeft: 4
  },
  path37: {
    top: 2,
    left: 3,
    width: 16,
    height: 11,
    backgroundColor: "transparent",
    position: "absolute",
    transform: [
      {
        rotate: "90deg"
      }
    ],
    borderColor: "transparent"
  },
  rectangle48: {
    top: 0,
    left: 0,
    width: 4,
    height: 4,
    backgroundColor: "rgba(214,217,224,1)",
    position: "absolute",
    borderRadius: 1
  },
  rectangleCopy2: {
    top: 6,
    left: 0,
    width: 4,
    height: 4,
    backgroundColor: "rgba(214,217,224,1)",
    position: "absolute",
    borderRadius: 1
  },
  rectangleCopy22: {
    top: 12,
    left: 0,
    width: 4,
    height: 4,
    backgroundColor: "rgba(214,217,224,1)",
    position: "absolute",
    borderRadius: 1
  },
  path37Stack: {
    width: 19,
    height: 16
  }
});

export {IconFeedInactive};
