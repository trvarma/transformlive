
import React from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Image,
    StyleSheet,
    Platform,
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const ImagePreview = ({
    style,
    item,
    imageKey,
    onPress,
    index,
    active,
    local,
}) => {
    // const { containerStyle, textStyle, cardSectionStyle } = styles;
    return (
        <TouchableOpacity
            activeOpacity={0.8}
            style={styles.videoContainer}
            onPress={() => onPress(item)}>
            <View style={styles.imageContainer}>
                <Image
                    style={styles.videoPreview}
                    source={{ uri: item.img }}
                />
            </View>
            <View style={{
                flexDirection: 'row',
                alignItems: 'center',
                alignContent: 'flex-start',
                margin: 10,
                bottom: 0,
                left: 10,
                position: 'absolute',
                justifyContent: 'flex-start',
            }}>
                <Text style={styles.desc}>{item.tr_name}</Text>
            </View>
        </TouchableOpacity>
    );
};
const styles = StyleSheet.create({
    videoContainer: {
        width: wp('94%'),
        justifyContent: 'center',
        alignItems: 'center',
    },
    videoPreview: {
        width: wp('92%'),
        height: 230,
        borderRadius: 8,
        marginTop: hp('1%'),
        // marginBottom: hp('0.5%'),
        aspectRatio: 16 / 9,
        resizeMode: 'stretch'
    },
    desc: {
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontSize: 9,
        paddingTop: 1,
        paddingBottom: 1,
        paddingLeft: 10,
        paddingRight: 10,
        borderRadius: 15,
        marginLeft: 2,
        backgroundColor: '#8c52ff',
        alignSelf: 'center',
        textAlign: 'center',
        fontWeight: '500',
        lineHeight: 18,
    },
    imageContainer: {
        width: wp('94%'),
        justifyContent: 'center',
        alignItems: 'center',
    },
    shadow: {
        ...Platform.select({
            ios: {
                shadowColor: 'black',
                shadowOffset: { width: 0, height: 1 },
                shadowOpacity: 0.1,
                shadowRadius: 5,
            },
            android: {
                elevation: 5,
            },
        }),
    },
});
export { ImagePreview };