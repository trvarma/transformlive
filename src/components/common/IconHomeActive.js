import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import Svg, { Path } from "react-native-svg";


  const IconHomeActive = props => {
  return (
    <View style={[styles.root, props.style]}>
      <Svg viewBox="-0 -0 18 18.13308715820314" style={styles.stroke1}>
        <Path
          strokeWidth={0}
          fill="rgba(45,49,66,1)"
          d="M8.31 0.73 L9.10 -0.10 C8.76 0.26 8.41 0.62 8.41 0.62 L0.00 9.94 L0.00 16.85 C0.00 17.56 0.58 18.13 1.29 18.13 L5.14 18.13 C5.85 18.13 6.43 17.56 6.43 16.85 L6.43 11.70 C6.43 10.99 7.00 10.42 7.71 10.42 L10.29 10.42 C11.00 10.42 11.57 10.99 11.57 11.70 L11.57 16.85 C11.57 17.56 12.15 18.13 12.86 18.13 L16.71 18.13 C17.42 18.13 18.00 17.56 18.00 16.85 L18.00 9.94 L9.59 0.62 L9.69 -0.73 Z"
        ></Path>
      </Svg>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1
  },
  stroke1: {
    width: 18,
    height: 18,
    backgroundColor: "transparent",
    borderColor: "transparent",
    marginTop: 3,
    marginLeft: 3
  }
});

export {IconHomeActive};
