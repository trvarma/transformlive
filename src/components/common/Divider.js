import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import Svg, { Path } from "react-native-svg";


  const Divider = props => {
  return (
    <View style={[styles.root, props.style]}>
      <Svg viewBox="-0.5 -0.5 333 3" style={styles.divider2}>
        <Path
          strokeWidth={1}
          fill="transparent"
          stroke="rgba(0,0,0,0.06496263586956522)"
          d="M0.00 0.50 L331.00 0.50 "
        ></Path>
      </Svg>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1
  },
  divider2: {
    width: 333,
    height: 3,
    backgroundColor: "transparent",
    borderColor: "transparent"
  }
});

export {Divider};
