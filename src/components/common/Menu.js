import React, { Component } from "react";
import { StyleSheet, View } from "react-native";
import Svg, { Path, Stop, Defs, LinearGradient } from "react-native-svg";
import IconHomeActive from "./IconHomeActive";
import IconExploreInactive from "./IconExploreInactive";
import IconFeedInactive from "./IconFeedInactive";
import IconUserInactive from "./IconUserInactive";

function Menu(props) {
  return (
    <View style={[styles.root, props.style]}>
      <View style={styles.rectangle49Stack}>
        <View style={styles.rectangle49}>
          <Svg viewBox="-2 -2 108 10" style={styles.path3}>
            <Path
              strokeWidth={4}
              fill="transparent"
              stroke="rgba(211,211,211,1)"
              fillOpacity={0.4000000059604645}
              strokeOpacity={0.4000000059604645}
              d="M2.00 3.00 L102.00 3.00 "
            ></Path>
          </Svg>
          <View style={styles.iconHomeActiveRow}>
            <IconHomeActive style={styles.iconHomeActive}></IconHomeActive>
            <IconExploreInactive
              style={styles.iconExploreInactive}
            ></IconExploreInactive>
            <IconFeedInactive
              style={styles.iconFeedInactive}
            ></IconFeedInactive>
            <IconUserInactive
              style={styles.iconUserInactive}
            ></IconUserInactive>
          </View>
        </View>
        <View style={styles.stroke196Stroke197}>
          <View style={styles.oval2Stack}>
            <Svg viewBox="-0 -0 62 62" style={styles.oval2}>
              <Defs>
                <LinearGradient
                  id="CFZtvz"
                  x1="2%"
                  x2="100%"
                  y1="39%"
                  y2="100%"
                >
                  <Stop offset="0.00" stopColor="rgba(139,99,230,1)" />
                  <Stop offset="1.00" stopColor="rgba(114,101,227,1)" />
                </LinearGradient>
              </Defs>
              <Path
                strokeWidth={0}
                fill="url(#CFZtvz)"
                d="M31.00 62.00 C48.12 62.00 62.00 48.12 62.00 31.00 C62.00 13.88 48.12 0.00 31.00 0.00 C13.88 0.00 0.00 13.88 0.00 31.00 C0.00 48.12 13.88 62.00 31.00 62.00 Z"
              ></Path>
            </Svg>
            <Svg
              viewBox="-1.5 -1.5 6.909090909090908 26"
              style={styles.stroke196198}
            >
              <Path
                strokeWidth={3}
                fill="transparent"
                stroke="rgba(255,255,255,1)"
                d="M1.50 1.50 L1.50 21.50 "
              ></Path>
            </Svg>
            <Svg
              viewBox="-1.5 -1.5 26 6.909090909090908"
              style={styles.stroke197}
            >
              <Path
                strokeWidth={3}
                fill="transparent"
                stroke="rgba(255,255,255,1)"
                d="M21.50 1.50 L1.50 1.50 "
              ></Path>
            </Svg>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1
  },
  rectangle49: {
    top: 22,
    left: 0,
    width: 375,
    height: 80,
    backgroundColor: "rgba(255,255,255,1)",
    position: "absolute",
    shadowOffset: {
      height: -10,
      width: 0
    },
    shadowColor: "rgba(92,108,156,0.1)",
    shadowOpacity: 1,
    shadowRadius: 30,
    flexDirection: "row"
  },
  path3: {
    width: 108,
    height: 10,
    backgroundColor: "transparent",
    borderColor: "transparent",
    marginLeft: 138,
    marginTop: 66
  },
  iconHomeActive: {
    width: 24,
    height: 24,
    backgroundColor: "transparent"
  },
  iconExploreInactive: {
    width: 24,
    height: 24,
    backgroundColor: "transparent",
    marginLeft: 36
  },
  iconFeedInactive: {
    width: 24,
    height: 24,
    backgroundColor: "transparent",
    marginLeft: 142
  },
  iconUserInactive: {
    width: 24,
    height: 24,
    backgroundColor: "transparent",
    marginLeft: 37
  },
  iconHomeActiveRow: {
    height: 24,
    flexDirection: "row",
    flex: 1,
    marginRight: 32,
    marginLeft: -214,
    marginTop: 21
  },
  stroke196Stroke197: {
    top: 0,
    left: 157,
    width: 62,
    height: 62,
    position: "absolute"
  },
  oval2: {
    top: 0,
    left: 0,
    width: 62,
    height: 62,
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  stroke196198: {
    top: 21,
    left: 30,
    width: 7,
    height: 26,
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  stroke197: {
    top: 30,
    left: 20,
    width: 26,
    height: 7,
    backgroundColor: "transparent",
    position: "absolute",
    borderColor: "transparent"
  },
  oval2Stack: {
    width: 62,
    height: 62
  },
  rectangle49Stack: {
    width: 375,
    height: 102
  }
});

export default Menu;
