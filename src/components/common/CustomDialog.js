import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import Modal from "react-native-modal";
import LinearGradient from 'react-native-linear-gradient';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const CustomDialog = ({ visible, title, desc, no, yes, onAccept, onDecline }) => {
    const renderNo = status => {
        if (status) {
            return (
                <TouchableOpacity
                    style={styles.linearGradient2}
                    onPress={onDecline} >
                    <Text style={styles.buttonText}>{no}</Text>
                </TouchableOpacity>
            )
        }
    }

    return (

        <Modal
            isVisible={visible}
            hideModalContentWhileAnimating={true}
            coverScreen={true}
            animationInTiming={200}
            animationOutTiming={100}
            animationIn={'fadeIn'}
            animationOut={'fadeOut'}
        >
            <View style={styles.content}>
                <Text style={styles.contentTitle}>{title}</Text>
                <Text style={styles.contentDesc}>{desc}</Text>

                <View style={styles.viewButtons}>
                    {renderNo(no)}
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient1}>
                        <TouchableOpacity
                            onPress={onAccept}>
                            <Text style={styles.buttonText}>{yes}</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                </View>

            </View>
        </Modal>
    );
};

const styles = {
    content: {
        backgroundColor: 'white',
        padding: 20,
        width: wp('90%'),
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    contentTitle: {
        fontSize: 16,
        textAlign: 'left',
        color: '#1c1c1c',
        alignSelf: 'flex-start',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    contentDesc: {
        fontSize: 13,
        textAlign: 'left',
        color: '#9b9a9f',
        marginTop: 5,
        alignSelf: 'flex-start',
        fontFamily: 'Rubik-Regular',
        fontWeight: '500',
    },
    linearGradient1: {
        width: '25%',
        height: 35,
        borderRadius: 10,
        justifyContent: 'center',
        position: 'relative',
        right: 0,
        marginLeft: 20,
        alignSelf: 'flex-end',
        marginBottom: 5,
        shadowColor: 'rgba(0, 0, 0, 0.08)',
        shadowOffset: { width: 5, height: 0 },
        shadowOpacity: 0.25,
        shadowRadius: 10,
        elevation: 6,
        marginTop: 3.5,
    },
    linearGradient2: {
        width: '25%',
        height: 35,
        borderRadius: 7,
        justifyContent: 'center',
        position: 'relative',
        right: 0,
        backgroundColor: '#767682',
        marginLeft: 20,
        alignSelf: 'flex-end',
        marginBottom: 5,
        shadowColor: 'rgba(0, 0, 0, 0.08)',
        shadowOffset: { width: 5, height: 0 },
        shadowOpacity: 0.25,
        shadowRadius: 10,
        elevation: 6,
        marginTop: 3.5,
    },
    buttonText: {
        fontSize: 13,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    viewButtons: {
        flexDirection: 'row',
        alignSelf: 'flex-end',
        marginTop: 15,
    },
    viewButtons1: {
        flexDirection: 'column',
        alignSelf: 'flex-end',
    },
    fullView: {
        width: wp('80%'),
        height: 40,
        borderRadius: 5,
        justifyContent: 'center',
        position: 'relative',
        right: 0,
        padding: 5,
        backgroundColor: '#8c52ff',
        marginLeft: 20,
        alignSelf: 'flex-end',
        marginTop: 20,
        marginBottom: 5,
    },
    fullView1: {
        width: wp('80%'),
        height: 40,
        borderRadius: 5,
        justifyContent: 'center',
        position: 'relative',
        right: 0,
        padding: 5,
        backgroundColor: '#767682',
        marginLeft: 20,
        alignSelf: 'flex-end',
        marginTop: 5,
        marginBottom: 5,
    },
};

export { CustomDialog };




