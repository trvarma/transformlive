import React from 'react';
import { StyleSheet, View, Modal, Text, TouchableOpacity, Image } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const VersionCheckInfo = ({ visible, title, desc, no, yes, onAccept, onDecline, image }) => {
    // const { containerStyle, textStyle, cardSectionStyle } = styles;
    const renderNo = status => {
        if (status) {
            return (
                <TouchableOpacity
                    style={styles.linearGradient2}
                    onPress={onDecline} >
                    <Text style={styles.buttonText}>{no}</Text>
                </TouchableOpacity>
            )
        }
    }
    return (
        <Modal
            transparent={true}
            animationType={'none'}
            visible={visible}
            onRequestClose={() => {
                //console.log('close modal');
            }}
            >
            <View style={styles.containerStyle}>
                <View style={styles.content}>
                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={image}
                        style={{ width: 60, height: 60, alignSelf: 'center', tintColor: '#ffffff' }}
                    />
                    <Text style={styles.contentTitle}>{title}</Text>
                    <Text style={styles.contentDesc}>{desc}</Text>

                    <View style={styles.viewButtons}>
                      
                        <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient1}>
                            <TouchableOpacity
                                onPress={onAccept}>
                                <Text style={styles.buttonText}>{yes}</Text>
                            </TouchableOpacity>
                        </LinearGradient>

                        {renderNo(no)}
                    </View>
                </View>
            </View>

        </Modal>
    );
};

const styles = StyleSheet.create({
    cardSectionStyle: {
        justifyContent: 'center',
    },
    textStyle: {
        flex: 1,
        fontSize: 18,
        textAlign: 'center',
        lineHeight: 40,
    },
    containerStyle: {
        backgroundColor: 'rgba(0, 0, 0, 0.9)',
        position: 'relative',
        flex: 1,
        justifyContent: 'center',
        zIndex:10
    },
    content: {
        // backgroundColor: 'white',
        padding: 20,
        width: wp('90%'),
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        position: 'relative',
        alignContent: 'center',
        borderRadius: 10,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    contentTitle: {
        fontSize: 18,
        textAlign: 'center',
        color: '#ffffff',
        alignSelf: 'center',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
        marginTop: 25,
        letterSpacing: 0.23,
    },
    contentDesc: {
        fontSize: 12,
        textAlign: 'center',
        color: '#ffffff',
        marginTop: 20,
        textAlign: 'center',
        fontFamily: 'Rubik-Regular',
        fontWeight: '500',
        letterSpacing: 0.23,
    },
    linearGradient1: {
        // width: '25%',
        height: 40,
        borderRadius: 3,
        justifyContent: 'center',
        position: 'relative',
        right: 0,
        marginLeft: 20,
        alignSelf: 'flex-end',
        flex: 1,
        marginBottom: 5,
        shadowColor: 'rgba(0, 0, 0, 0.08)',
        shadowOffset: { width: 5, height: 0 },
        shadowOpacity: 0.25,
        shadowRadius: 10,
        elevation: 6,
        marginTop: 3.5,
    },
    linearGradient2: {
        // width: '25%',
        height: 40,
        borderRadius: 3,
        justifyContent: 'center',
        position: 'relative',
        right: 0,
        backgroundColor: '#767682',
        marginLeft: 20,
        alignSelf: 'flex-end',
        flex: 1,
        marginBottom: 5,
        shadowColor: 'rgba(0, 0, 0, 0.08)',
        shadowOffset: { width: 5, height: 0 },
        shadowOpacity: 0.25,
        shadowRadius: 10,
        elevation: 6,
        marginTop: 3.5,
    },
    buttonText: {
        fontSize: 12,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    viewButtons: {
        flexDirection: 'row',
        alignSelf: 'flex-end',
        marginTop: 30,
    },
    viewButtons1: {
        flexDirection: 'column',
        alignSelf: 'flex-end',
    },
    fullView: {
        width: wp('80%'),
        height: 40,
        borderRadius: 5,
        justifyContent: 'center',
        position: 'relative',
        right: 0,
        padding: 5,
        backgroundColor: '#8c52ff',
        marginLeft: 20,
        alignSelf: 'flex-end',
        marginTop: 20,
        marginBottom: 5,
    },
    fullView1: {
        width: wp('80%'),
        height: 40,
        borderRadius: 5,
        justifyContent: 'center',
        position: 'relative',
        right: 0,
        padding: 5,
        backgroundColor: '#767682',
        marginLeft: 20,
        alignSelf: 'flex-end',
        marginTop: 5,
        marginBottom: 5,
    },
});
export { VersionCheckInfo };
