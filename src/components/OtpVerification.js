import React, { Component } from 'react';
import { connect } from 'react-redux';
import { verifyOtp, getOtp } from '../actions';
// import SmsListener from 'react-native-android-sms-listener';
import LinearGradient from 'react-native-linear-gradient';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  ImageBackground,
  Keyboard,
  Platform,
} from 'react-native';
let finalOtp = '';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { NoInternet, Loader, CustomDialog } from './common';
import NetInfo from "@react-native-community/netinfo";
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';
import LogUtils from '../utils/LogUtils.js';
import { Actions } from 'react-native-router-flux';
import RNOtpVerify from 'react-native-otp-verify';
import appsFlyer from 'react-native-appsflyer';
let fcmToken = '';
import { allowFunction } from '../utils/ScreenshotUtils.js';


class OtpVerification extends Component {
  constructor(props) {
    super(props);
    this.SMSReadSubscription = {};
    this.state = {
      timer: 30,
      otp1: '',
      otp2: '',
      otp3: '',
      otp4: '',
      isCountDown: true,
      isAlert: false,
      alertMsg: '',
      isInternet: false,
      isTcCheck: false,
      otpHashCode: '',
      code: '',
      isShowOTPContinue: true
    };
  }

  async componentDidMount() {
    // requestReadSmsPermission();

    allowFunction();

    this.checkPermission();

    // this.startListeningForOtp();

    if (Platform.OS === 'android') {
      this.getHash();
      RNOtpVerify.getOtp()
        .then(p => RNOtpVerify.addListener(message => {

          LogUtils.infoLog1('Message', message);

          if (message.includes('Transform')) {
            const res = /(\d{4})/g.exec(message)[1];
            LogUtils.infoLog1('received otp ', res);
            this.setState({ code: res });
            this.setState({ otp1: res[0], otp2: res[1], otp3: res[2], otp4: res[3], });
          }

          RNOtpVerify.removeListener();
          Keyboard.dismiss();
        }))
        .catch(p => console.log(p));
    }


    this.interval = setInterval(
      () => this.setState(prevState => ({ timer: prevState.timer - 1 })),
      1000,
    );
    // this.startTimer.bind(this);
  }

  getHash = () => {
    RNOtpVerify.getHash()
      .then(hashCode => {
        this.setState({ otpHashCode: hashCode })
        LogUtils.infoLog1('hashCode : ', hashCode)
      })
      .catch(console.log);
  }

  componentDidUpdate() {
    if (this.state.timer === 0) {
      clearInterval(this.interval);
      // this.setState({ isCountDown: false });
    }
  }

  componentWillUnmount() {
    clearInterval(this.interval);
    if (Platform.OS === 'android') {
      RNOtpVerify.removeListener();
    }
    //remove listener
    // this.SMSReadSubscription.remove();
  }

  //1
  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      LogUtils.infoLog("Permission Enabled");

      this.getToken();
    } else {
      this.requestPermission();
    }
  }

  //3
  async getToken() {
    fcmToken = await AsyncStorage.getItem('fcmToken');
    if (!fcmToken) {
      fcmToken = await firebase.messaging().getToken();
      if (fcmToken) {
        // user has a device token
        await AsyncStorage.setItem('fcmToken', fcmToken);

        appsFlyer.updateServerUninstallToken(fcmToken, (success) => {
          LogUtils.infoLog1('firebaseToken success ', success);
        })
      }
    }
    LogUtils.infoLog1("FCM Token", fcmToken);
    appsFlyer.updateServerUninstallToken(fcmToken, (success) => {
      LogUtils.infoLog1('firebaseToken success ', success);
    })
  }

  //2
  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();
      // User has authorised
      this.getToken();
    } catch (error) {
      // User has rejected permissions
      LogUtils.infoLog('permission rejected');
    }
  }

  renderOtpInput(text) {
    this.onOtpChanged(text);
  }
  onOtpChanged(text) {
    LogUtils.infoLog1('sms', text);
    // this.props.otpChanged(text);
  }



  onDoneCountdown = () => {
    // Alert.alert("Countdown Finish.");
  }

  renderCountDown() {

    if (this.state.timer !== 0) {
      if (this.state.timer.toString().length === 1) {
        return (
          <View style={styles.viewSign}>
            <Text style={styles.sign}>Resend in 00 : 0{this.state.timer} </Text>
          </View>
        );
      }
      else {
        return (
          <View style={styles.viewSign}>
            <Text style={styles.sign}>Resend in 00 : {this.state.timer} </Text>
          </View>
        );
      }
    }
    else {
      return (
        <View style={styles.viewSign}>
          <TouchableOpacity
            onPress={() => this.onResendPressed()}>
            <Text style={styles.sign}>RESEND</Text>
          </TouchableOpacity>
        </View>

      );
    }
  }

  onButtonPressed() {
    try {

      if (this.state.otp1 && this.state.otp2 && this.state.otp3 && this.state.otp4) {
        if (this.state.isTcCheck) {
          LogUtils.firebaseEventLog('click', {
            p_id: 2,
            p_category: 'Login',
            p_name: 'OTP Verified',
          });

          NetInfo.fetch().then(state => {
            if (state.isConnected) {
              finalOtp = this.state.otp1 + this.state.otp2 + this.state.otp3 + this.state.otp4
              this.props.verifyOtp({ mobile: this.props.mobileNumber, otp: finalOtp, device_key: fcmToken, hashCode: this.state.otpHashCode });
            }
            else {
              this.setState({ isInternet: true });
            }
          });
        }
        else {
          this.setState({ isAlert: true, alertMsg: 'Please check our terms of use before continuing' });
        }
      } else if (this.state.code) {

        if (this.state.isTcCheck) {
          LogUtils.firebaseEventLog('click', {
            p_id: 2,
            p_category: 'Login',
            p_name: 'OTP Verified',
          });

          NetInfo.fetch().then(state => {
            if (state.isConnected) {
              this.props.verifyOtp({ mobile: this.props.mobileNumber, otp: this.state.code, device_key: fcmToken, hashCode: this.state.otpHashCode });
            }
            else {
              this.setState({ isInternet: true });
            }
          });
        }
        else {
          this.setState({ isAlert: true, alertMsg: 'Please check our terms of use before continuing' });
        }

      }
      else {
        this.setState({ alertMsg: 'Please enter a valid OTP' });
        this.setState({ isAlert: true });
      }
    } catch (error) {
      console.log(error)
    }
  }

  onResendPressed() {
    try {
      this.props.getOtp(this.props.mobileNumber, '2');
      this.setState({ timer: 30 })
      this.interval = setInterval(
        () => this.setState(prevState => ({ timer: prevState.timer - 1 })),
        1000,
      );
    } catch (error) {
      console.log(error)
    }
  }

  renderResend() {
    return (
      <View style={styles.viewSign}>
        <TouchableOpacity
          onPress={() => this.onButtonPressed()}>
          <Text style={styles.sign}>RESEND</Text>
        </TouchableOpacity>
      </View>

    );
  }

  renderButton() {
    return (
      <View style={styles.containerSubmitStyle}>
        <Text style={styles.textSubmitStyle}>{'login'.toUpperCase()}</Text>
      </View>
    );
  }

  onBackPressed() {
    //Actions.pop();
  }

  async startTimer() {
    try {
      let timer = setInterval(() => {
        var num = (Number(this.state.seconds_Counter) + 1).toString()
        this.setState({
          seconds_Counter: num.length == 1 ? '0' + num : num
        });
      }, 1000);
      this.setState({ timer });
      this.setState({ isCountDown: true })
    } catch (error) {
      console.log(error)
    }
  }

  async onAccept() {
    this.setState({ alertMsg: '' });
    this.setState({ isAlert: false });
  }

  async onRetry() {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.setState({ isInternet: false });
      }
      else {
        // console.log("Is connected?", state.isConnected);
        this.setState({ isInternet: true });
      }
    });
  }

  render() {

    return (
      <KeyboardAwareScrollView
        resetScrollToCoords={{ x: 0, y: 0 }}
        contentContainerStyle={styles.containerStyle}
        enableOnAndroid={false}
        onKeyboardDidShow={(frames) => this.setState({ isShowOTPContinue: false })}
        onKeyboardDidHide={(frames) => this.setState({ isShowOTPContinue: true })}
        scrollEnabled={false}>
        <ImageBackground source={require('../res/app_bg.png')} style={styles.containerStyle}>
          <Loader loading={this.props.loading} />
          <View style={styles.optionInnerContainer}>
            <Image
              source={require('../res/ic_ver_line.png')}
              style={styles.topBarImageStyle}
            />
            <Image
              source={require('../res/ic_ver_line.png')}
              style={styles.topBarImageStyle}
            />

          </View>

          <TouchableOpacity
            onPress={() => this.onBackPressed()}>
            <Image
              source={require('../res/ic_back.png')}
              style={styles.backImageStyle}
            />
          </TouchableOpacity>

          <View style={styles.optionInnerContainer}>
            <Image
              source={require('../res/ic_app.png')}
              style={styles.appImageStyle}
            />
          </View>

          <View style={styles.containericonStyle}>

            <Text style={styles.subtextOneStyle}>We sent you a code to verify your number</Text>
          </View>
          {Platform.OS === 'android'
            ? (
              <View style={styles.optionInnerContainer1}>
                <View style={styles.inputContainerOtp}>
                  <TextInput
                    style={styles.textInputStyle}
                    maxLength={1}
                    keyboardType="numeric"
                    value={this.state.otp1}
                    returnKeyType='done'
                    onChangeText={text => {
                      if (text) {
                        let newText = '';
                        let numbers = '0123456789';
                        for (var i = 0; i < text.length; i++) {
                          if (numbers.indexOf(text[i]) > -1) {
                            newText = newText + text[i];
                          }
                          else {
                            this.setState({ alertMsg: 'Please enter numbers only' });
                            this.setState({ isAlert: true });
                          }
                        }
                        if (newText) {
                          this.secondInput.focus();
                          this.setState({ otp1: newText });
                        }

                      }
                      else {
                        this.setState({ otp1: '' });
                      }

                    }}
                  />
                </View>
                <View style={styles.inputContainerOtp}>
                  <TextInput
                    ref={ref => {
                      this.secondInput = ref;
                    }}
                    style={styles.textInputStyle}
                    keyboardType="numeric"
                    maxLength={1}
                    value={this.state.otp2}
                    returnKeyType='done'
                    onChangeText={text => {
                      if (text) {
                        let newText = '';
                        let numbers = '0123456789';
                        for (var i = 0; i < text.length; i++) {
                          if (numbers.indexOf(text[i]) > -1) {
                            newText = newText + text[i];
                          }
                          else {
                            this.setState({ alertMsg: 'Please enter numbers only' });
                            this.setState({ isAlert: true });
                          }
                        }
                        if (newText) {
                          this.thirdInput.focus();
                          this.setState({ otp2: newText });
                        }
                      }
                      else {
                        this.setState({ otp2: '' });
                      }

                    }}
                  />
                </View>
                <View style={styles.inputContainerOtp}>
                  <TextInput
                    ref={ref => {
                      this.thirdInput = ref;
                    }}
                    style={styles.textInputStyle}
                    keyboardType="numeric"
                    maxLength={1}
                    value={this.state.otp3}
                    returnKeyType='done'
                    onChangeText={text => {
                      if (text) {
                        let newText = '';
                        let numbers = '0123456789';
                        for (var i = 0; i < text.length; i++) {
                          if (numbers.indexOf(text[i]) > -1) {
                            newText = newText + text[i];
                          }
                          else {
                            this.setState({ alertMsg: 'Please enter numbers only' });
                            this.setState({ isAlert: true });
                          }
                        }
                        if (newText) {
                          this.fourthInput.focus();
                          this.setState({ otp3: newText });
                        }
                      }
                      else {
                        this.setState({ otp3: '' });
                      }

                    }}
                  />
                </View>
                <View style={styles.inputContainerOtp}>
                  <TextInput
                    ref={ref => {
                      this.fourthInput = ref;
                    }}
                    style={styles.textInputStyle}
                    keyboardType="numeric"
                    maxLength={1}
                    value={this.state.otp4}
                    returnKeyType='done'
                    onChangeText={text => {
                      if (text) {
                        let newText = '';
                        let numbers = '0123456789';
                        for (var i = 0; i < text.length; i++) {
                          if (numbers.indexOf(text[i]) > -1) {
                            newText = newText + text[i];
                          }
                          else {
                            this.setState({ alertMsg: 'Please enter numbers only' });
                            this.setState({ isAlert: true });
                          }
                        }
                        if (newText) {
                          this.setState({ otp4: text });
                        }
                      }
                      else {
                        this.setState({ otp4: '' });
                      }

                    }}
                  />
                </View>

              </View>

            ) :
            (
              <View style={{
                width: wp('60%'),
                height: 50,
                borderRadius: 10,
                backgroundColor: '#ffffff',
                justifyContent: 'center',
                alignSelf: 'center',
                textAlign: 'center',
                margin: 20,
                shadowColor: "#000",
                shadowOffset: {
                  width: 0,
                  height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 3.84,
                elevation: 5,
                color: '#2d3142',
              }}>
                <TextInput
                  style={{
                    width: wp('100%'),
                    alignSelf: 'center',
                    textAlign: 'center',
                    fontSize: 16,
                    fontWeight: '400',
                    color: '#2d3142',
                    letterSpacing: 30,
                    fontFamily: 'Rubik-Regular',
                    paddingLeft: 30,
                  }}
                  autoFocus
                  maxLength={4}
                  keyboardType="number-pad"
                  value={this.state.code}
                  returnKeyType='done'
                  onChangeText={text => {
                    this.setState({ code: text })
                    // Keyboard.dismiss();
                  }}
                />
              </View>

            )

          }


          <View style={styles.viewSign}>
            <Text style={styles.acc}>Didn't Receive? </Text>
            {this.renderCountDown()}
          </View>

          <View style={styles.viewBottom}>

            <View style={{
              flexDirection: 'row',
              width: wp('80%'),
              alignSelf: 'center',
              alignContent: 'flex-start',
            }}>
              <TouchableOpacity
                onPress={() => this.setState({ isTcCheck: !this.state.isTcCheck })}>
                {this.state.isTcCheck
                  ?
                  (
                    <Image
                      source={require('../res/ic_box_check.png')}
                      style={styles.imgTandC}
                    />
                  )
                  :
                  (
                    <Image
                      source={require('../res/ic_box_uncheck.png')}
                      style={styles.imgTandC}
                    />
                  )
                }

              </TouchableOpacity>

              <TouchableOpacity
                activeOpacity={0.7}
                onPress={() => Actions.webview({ url: 'https://admin.transformfitness.in/html/terms-and-conditions.html' })}>
                <Text style={{
                  textAlign: 'left',
                  alignSelf: 'center',
                  width: wp('70%'),

                }}>
                  <Text style={styles.textTandC}>{`By clicking "Continue" I agree that I have read and accepted the`}</Text>

                  <Text style={styles.textTandCuse}> terms of use</Text>

                </Text>
              </TouchableOpacity>
            </View>

            {
              this.state.isShowOTPContinue &&
              <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                <TouchableOpacity
                  onPress={() => this.onButtonPressed()}>
                  <Text style={styles.buttonText}>Continue</Text>
                </TouchableOpacity>
              </LinearGradient>
            }

          </View>

          <NoInternet
            image={require('../res/img_nointernet.png')}
            loading={this.state.isInternet}
            onRetry={this.onRetry.bind(this)} />


          <CustomDialog
            visible={this.state.isAlert}
            title='Alert'
            desc={this.state.alertMsg}
            onAccept={this.onAccept.bind(this)}
            no=''
            yes='Ok' />
        </ImageBackground>
      </KeyboardAwareScrollView>
    );
  }
}

const mapStateToProps = state => {
  return {
    mobileNumber: state.auth.mobileNumber,
    error: state.auth.error,
    loading: state.auth.loading,
  };
};

const styles = StyleSheet.create({
  viewStyle: {
    backgroundColor: 'white',
  },
  textStyle: {
    fontSize: 18,
    color: 'red',
    alignSelf: 'center',
  },
  imageStyle: {
    width: 400,
    height: 400,
  },
  containerStyle: {
    width: '100%',
    height: '100%',
    flex: 1,
  },
  containericonStyle: {
    flexDirection: 'column',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  containerMobileStyle: {
    backgroundColor: '#ffffff',
    flexDirection: 'row',
    borderColor: '#ddd',
    borderRadius: 15,
    position: 'relative',
    alignItems: 'center',
    marginLeft: 30,
    marginRight: 30,
    marginTop: 30,
    alignSelf: 'center',
    height: 50,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  inputContainerOtp: {
    width: 62,
    height: 62,
    borderRadius: 23,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignSelf: 'center',
    textAlign: 'center',
    margin: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    color: '#2d3142',
  },
  containerSubmitStyle: {
    width: '85%',
    backgroundColor: '#1aba9a',
    flexDirection: 'row',
    borderRadius: 5,
    justifyContent: 'center',
    alignSelf: 'center',
    padding: 15,
    marginTop: 5,
  },
  deleteImageStyle: {
    width: 30,
    height: 30,
    position: 'relative',
    alignSelf: 'center',
  },
  textInputStyle: {
    width: 62,
    height: 62,
    flex: 1,
    alignSelf: 'center',
    textAlign: 'center',
    fontSize: 16,
    fontWeight: '400',
    color: '#2d3142',
    letterSpacing: .5,
    fontFamily: 'Rubik-Regular',
  },
  subTextStyle: {
    alignSelf: 'center',
    fontSize: 14,
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10,
    color: '#8c52ff',
    textAlign: 'center',
  },
  subTextMobileStyle: {
    alignSelf: 'center',
    fontSize: 12,
    color: '#8c52ff',
    marginBottom: 20,
    textAlign: 'center',
  },
  subtextOneStyle: {
    fontSize: 18,
    marginTop: hp('10%'),
    marginLeft: 40,
    marginRight: 40,
    fontWeight: '500',
    color: '#2d3142',
    lineHeight: 24,
    textAlign: 'center',
    fontFamily: 'Rubik-Medium',
  },
  textSubmitStyle: { fontSize: 14, color: '#ffffff', fontWeight: 'bold' },
  mobileImageStyle: { width: 20, height: 20, alignSelf: 'center' },
  imageIconStyle: { width: 180, height: 98, alignSelf: 'center' },
  linearGradient: {
    width: wp('90%'),
    height: 50,
    borderRadius: 25,
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: 20,
    marginBottom: 15,
  },
  buttonText: {
    fontSize: 16,
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    fontFamily: 'Rubik-Medium',
    fontWeight: '500',
  },
  backImageStyle: {
    width: 19,
    height: 16,
    marginTop: hp('7%'),
    marginLeft: 20,
    marginBottom: 20,
    opacity: 0,
    alignSelf: 'flex-start',
  },
  topBarImageStyle: {
    width: wp('50%'),
    height: 8,
    marginBottom: 20,
    marginRight: 1,
  },
  optionInnerContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  optionInnerContainer1: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: hp('2%'),
  },
  pwdBoxesContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'center',
    zIndex: -1,
    marginTop: hp('10%'),
    left: '5%',
  },
  appImageStyle: {
    width: 300,
    height: 90,
    // marginTop: -45,
    marginBottom: 20,
  },
  pwdImageStyle: {
    margin: 10,
  },
  viewSign: {
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: 25,
    marginBottom: 20,
  },
  acc: {
    fontSize: 14,
    fontWeight: '400',
    color: '#9c9eb9',
    lineHeight: 14,
    paddingTop: 10,
    fontFamily: 'Rubik-Medium',
  },
  sign: {
    fontSize: 14,
    fontWeight: '400',
    color: '#8c52ff',
    paddingTop: 3,
    marginLeft: 10,
    fontFamily: 'Rubik-Medium',
  },
  sign1: {
    fontSize: 14,
    fontWeight: '500',
    color: '#8c52ff',
    paddingTop: 5,
    lineHeight: 14,
    fontFamily: 'Rubik-Regular',
  },
  viewBottom: {
    width: wp('93%'),
    alignContent: 'center',
    alignSelf: 'center',
    position: 'absolute',
    bottom: 10,
    flexDirection: 'column',
  },
  textTandC: {
    fontSize: 10,
    fontWeight: '500',
    color: '#2d3142',
    textAlign: 'center',
    fontFamily: 'Rubik-Medium',
  },
  textTandCuse: {
    fontSize: 12,
    fontWeight: '500',
    color: '#8c52ff',
    textAlign: 'center',
    fontFamily: 'Rubik-Medium',
  },
  imgTandC: {
    width: 19,
    height: 19,
    marginLeft: 10,
    marginRight: 10,
    alignSelf: 'flex-start',
  },
});

export default connect(
  mapStateToProps,
  { verifyOtp, getOtp },
)(OtpVerification);
