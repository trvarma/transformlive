/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, StyleSheet, Image, StatusBar } from 'react-native';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Orientation from 'react-native-orientation';
import { allowFunction } from '../utils/ScreenshotUtils.js';
import appsFlyer from 'react-native-appsflyer';
import LogUtils from '../utils/LogUtils.js';
import NetInfo from "@react-native-community/netinfo";
//import {CheckInternetStatus} from '../actions/AuthActions'

class LaunchingScreen extends Component {

  async componentDidMount() {
    try{

      NetInfo.fetch().then(state => {
       // this.handleDeviceConnectivityChange(state);
        if (state.isConnected) {
          // AppsFlyer init setup
          appsFlyer.initSdk(
            {
              devKey: 'dtdYGUcYoD9n3t82of2iUm',
              isDebug: false,
              appId: '1545532803',
              onInstallConversionDataListener: true, //Optional
              onDeepLinkListener: true, //Optional
            },
            (result) => {
              LogUtils.infoLog1("appsFlyer success", result);
            },
            (error) => {
              console.error(error);
              LogUtils.infoLog1("appsFlyer fail", error);
            }
          );
        }
        else {
          //this.setState({ isAlert: true, alertMsg: 'Please enable your internet connection and try again' });
        }
      });
  
      allowFunction();
      StatusBar.setHidden(true);
      Orientation.lockToPortrait();
      setTimeout(() => {
        retrieveItem('token');
      }, 3000);
    }catch(error){
      console.log(error);
    }
  }

  // handleDeviceConnectivityChange=(state)=>{
  //   this.props.CheckInternetStatus(state);
  // }

  render() {
    return (
      <View style={styles.containerStyle}>
        <View style={styles.containeViewStyle}>
          <Image
            source={require('../res/ic_app.png')}
            style={styles.imageIconStyle} />
        </View>
      </View>
    );
  }
}

const retrieveItem = async key => {
  try {
    const token = await AsyncStorage.getItem(key);
    const trainer = await AsyncStorage.getItem('istrainer');
    if (token !== null) {
      return Actions.thome({ type: 'reset', });
    }
    return Actions.splash({ type: 'reset' });
  } catch (error) {
    //(error.message);
  }
};

const styles = StyleSheet.create({
  containerStyle: {
    width: wp('100%'),
    height: hp('100%'),
    alignContent: 'center',
    justifyContent: 'center',
    backgroundColor: '#ffffff',
  },
  containeViewStyle: {
    alignContent: 'center',
    justifyContent: 'center',
    margin: 50,
  },
  imageIconStyle: {
    width: 300,
    height: 90,
    alignSelf: 'center',
    resizeMode: 'contain',
  },
});


// const mapStateToProps = state => {
//   return {};
// };

//export default connect(mapStateToProps, {CheckInternetStatus})(LaunchingScreen);

export default LaunchingScreen;
