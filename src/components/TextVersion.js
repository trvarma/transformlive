import React, { Component } from 'react';
import {
    StyleSheet,
    SafeAreaView,
    Image,
    View,
    Text,
    TouchableOpacity,
    ScrollView,
    BackHandler,
    ImageBackground,
    TextInput, 
} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import { CustomDialog, WorkoutCustomDialog, Loader,HomeDialog } from './common';
import AsyncStorage from '@react-native-community/async-storage';
import { BASE_URL, SWR } from '../actions/types';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { AirbnbRating } from 'react-native-ratings';
import LinearGradient from 'react-native-linear-gradient';
import LogUtils from '../utils/LogUtils.js';

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

class TextVersion extends Component {
    constructor(props) {
        super(props);
        this.state = {
            textVersion: this.props.textVersionObj,
            title: this.props.title,
            name: this.props.name,
            loading:false,
            isBackPressed: false,
            isEndWorkout: false,
            //isEndWithOutFeedback: false,

            post: '',
            rating: 5,
            isFeedbackSubmit: false,

            isAlert: false,
            alertMsg: '',
        }
    }

    componentDidMount() {
        try {
            this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
                this.onBackPressed();
                return true;
            });
        } catch (error) {

        }
    }

    async onBackPressed() {
        //Orientation.lockToPortrait();
        this.setState({ isBackPressed: true, });
        //Actions.pop();
        // Actions.popTo('traineePlanNew');
    }

    showSetsAndReps(item) {
        try {
            var setAndReps = [];

            item.map((ele1, index) => {

                setAndReps.push(
                    <View style={[styles.containerGoalPlan, { height: undefined }]}>
                        <View
                            style={{ backgroundColor: '#8c52ff', height: 30, justifyContent: 'center', borderTopLeftRadius: 5, borderTopRightRadius: 5 }}
                        >
                            <Text style={{ fontFamily: 'Rubik-Regular', marginLeft: 10, fontSize: 15, color: '#ffffff' }}>{ele1.title.toUpperCase()}</Text>
                        </View>
                        {
                            ele1.pdse_arr.map((ele2, index) => {
                                return (
                                    <View style={{ flexDirection: 'column', marginTop: 5 }}>
                                        {
                                            ele2.title != "" ?

                                                <View style={{ justifyContent: 'center', marginLeft: 30, height: 28 }}>
                                                    <Text style={{ fontFamily: 'Rubik-Regular', fontSize: 14 }}>{ele2.title.toUpperCase()} : </Text>
                                                </View> : <View />
                                        }
                                        <View style={{ flexDirection: 'column' }}>
                                            {
                                                ele2.pdses_arr.map((ele3, row) => {
                                                    return (
                                                        <View style={{
                                                            flexDirection: 'row',
                                                            justifyContent: 'space-around',
                                                            //borderBottomColor:'red',
                                                            // borderBottomWidth:1,
                                                            paddingVertical: 4,
                                                            backgroundColor: (row % 2) == 0 ? '#f0ffff' : '#f8f8ff',
                                                            //backgroundColor: '#f0ffff' 
                                                        }}>
                                                            <View>
                                                                <Text style={{ fontFamily: 'Rubik-Regular', fontSize: 13 }}>{`Set ${ele3.set}`}</Text>
                                                            </View>
                                                            <View>
                                                                {
                                                                    ele3.type == "w" ? <Text style={{ fontFamily: 'Rubik-Regular', fontSize: 13 }}>{`Reps: ${ele3.reps}`}</Text> :
                                                                        <Text style={{ fontFamily: 'Rubik-Regular', fontSize: 13 }}>{`Minutes: ${ele3.reps}`}</Text>
                                                                }
                                                            </View>
                                                            {/* {
                                                                ele3.type == "w" ? (
                                                                    <View style={{justifyContent:'center'}}>
                                                                       
                                                                            <Image
                                                                            progressiveRenderingEnabled={true}
                                                                            resizeMethod="resize"
                                                                            source={require('../res/ic_workout_black.png')}
                                                                            style={[styles.backImageStyle, { height: 15, width: 15 }]}
                                                                        />
                                                                    
                                                                    </View>
                                                                ) : (
                                                                    <View style={{justifyContent:'center'}}>
                                                                       
                                                                         <Image
                                                                            progressiveRenderingEnabled={true}
                                                                            resizeMethod="resize"
                                                                            source={require('../res/clock.png')}
                                                                            style={[styles.backImageStyle, { height: 15, width: 15 }]}
                                                                        />
                                                                    </View>
                                                                )
                                                            } */}

                                                        </View>
                                                    )
                                                })
                                            }
                                        </View>
                                    </View>
                                )
                            })
                        }
                    </View>
                )
            })

            return (
                <View style={{ flex: 1 }}>
                    {setAndReps}
                </View>
            )

        } catch (error) {
            console.log(error);
        }
    }

    async onExitWorkout() {
        try {
            this.setState({
                isCompleted: 0,
                isBackPressed: false,
                //isEndWorkout: true
            });
           
            if (this.props.planObj.fb_done === 0) {
                this.setState({ isEndWorkout: true, });
            } else {
                if (this.props.from === 'home') {
                    Actions.popTo('traineeHome');
                } else if (this.props.from === 'workouts') {
                    Actions.WorkoutsHome();
                } else {
                    // Actions.traineeWorkoutsHome();
                    Actions.traineePlanNew({ pId: this.props.p_id, from: 'endWork' });
                }
            }
        } catch (error) {
            console.log(error);
        }
    }

    async onCompleteWorkout() {
        this.setState({ isBackPressed: false, isCompleted: 1, });
        this.saveEndWorkOut();
        ///Actions.traineePlanNew({ pId: this.props.p_id, from: 'endWork' });
    }

    async saveEndWorkOut() {
        try {
            this.setState({ loading: true, });
            let token = await AsyncStorage.getItem('token');
            fetch(
                `${BASE_URL}/trainee/endworkout`,
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${token}`,
                    },
                    body: JSON.stringify({
                        pd_id: this.props.pdObj.pd_id,
                        vrsn: "T"
                    }),
                },
            )
                .then(processResponse)
                .then(res => {
                    const { statusCode, data } = res;
                    LogUtils.infoLog1('statusCode', statusCode);
                    LogUtils.infoLog1('data', data);
                    if (statusCode >= 200 && statusCode <= 300) {

                        if (this.props.planObj.fb_done === 0){

                            this.setState({ loading: false, isEndWorkout: true,});
                        }
                        else {

                            //Actions.traineePlanNew({ pId: this.props.p_id, from: 'endWork' });
                                this.setState({ loading: false,  isAlert: true, alertMsg: data.message, });
                        }

                    } else {
                        if (data.message === 'You are not authenticated!') {
                            //Actions.pop();
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message });

                        } else {
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                        }
                    }
                })
                .catch(function (error) {
                    this.setState({ loading: false, isAlert: true, alertMsg: SWR });
                });
        } catch (error) {
            console.log(error);
        }
    }

    renderProfileImg() {
        try {

            if (this.props.planObj.tr_img) {
                return (
                    <Image
                        source={{ uri: this.props.planObj.tr_img }}
                        style={styles.profileImage1}
                    />
                );
            }
            else {
                return (
                    <Image
                        source={require('../res/ic_profile_gray.png')}
                        style={styles.profileImage1}
                    />
                );
            }
        } catch (error) {
            console.log(error)
        }
    }

    ratingCompleted(rating) {
        try {
            this.setState({ rating: rating })
            switch (rating) {
                case 1:
                    this.setState({ ratingText: 'Bad experience !' })
                    break;
                case 2:
                    this.setState({ ratingText: 'Not that great !' })
                    break;
                case 3:
                    this.setState({ ratingText: 'It was good !' })
                    break;
                case 4:
                    this.setState({ ratingText: 'Had a great experience !' })
                    break;
                case 5:
                    this.setState({ ratingText: 'Absolutely superb !' })
                    break;
                default:
                    this.setState({ ratingText: 'Give rating !' })
                    break
            }
        } catch (error) {
            console.log(error)
        }
    }

    async saveFeebackWorkOut() {
        try {
           
            this.setState({ loading: true });
            let token = await AsyncStorage.getItem('token');
            fetch(
                `${BASE_URL}/trainee/savedayfdback`,
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${token}`,
                    },
                    body: JSON.stringify({
                        pd_id: this.props.pdObj.pd_id,
                        rating: this.state.rating,
                        comments: this.state.post,
                    }),
                },
            )
                .then(processResponse)
                .then(res => {
                    const { statusCode, data } = res;
                    // LogUtils.infoLog1('statusCode', statusCode);
                    // LogUtils.infoLog1('data', data);
                    if (statusCode >= 200 && statusCode <= 300) {
                        this.setState({ loading: false, isFeedbackSubmit: true })
                    } else {
                        if (data.message === 'You are not authenticated!') {
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message });

                        } else {
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                        }
                    }
                })
                .catch(function (error) {
                    this.setState({ loading: false, isAlert: true, alertMsg: SWR });
                });
        } catch (error) {
            console.log(error);
        }
    }

    async onFeedbackSubmit() {
        try {
            if (this.state.alertMsg === 'You are not authenticated!') {
                AsyncStorage.clear().then(() => {
                    Actions.auth({ type: 'reset' });
                });
            }
            this.setState({ isFeedbackSubmit: false, alertMsg: '' });

            Actions.traineePlanNew({ pId: this.props.p_id, from: 'endWork' });

        } catch (error) {
            console.log(error);
        }
    }

    onAccept() {

        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }

        this.setState({
            isAlert: false,
            alertMsg: ''
        })
        Actions.traineePlanNew({ pId: this.props.p_id, from: 'endWork' });
    }

    render() {
        if (!this.state.isEndWorkout) {
            return (
                <SafeAreaView>
                    {/* show sets and reps */}
                    <View style={styles.containerStyle}>

                        <View style={{
                            flexDirection: 'row', margin: 20,
                        }}>
                            <View style={{ position: 'absolute' }}>
                                <TouchableOpacity
                                    style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                                    onPress={() => this.onBackPressed()}>
                                    <Image
                                        progressiveRenderingEnabled={true}
                                        resizeMethod="resize"
                                        source={require('../res/ic_back.png')}
                                        style={styles.backImageStyle}
                                    />
                                </TouchableOpacity>
                            </View>
                        </View>
                        {/* <Text style={styles.textIndicator}>{this.state.name}</Text> */}
                        <Text style={styles.textIndicator}>{this.state.title}</Text>

                        <View style={[styles.viewTitleCal, { height: hp("90%"), marginTop: 5 }]}>
                            <View style={{}}>
                                <ScrollView
                                    //key={index + "gymVersion"}
                                    ref={ref => this.scrollView = ref}
                                    showsVerticalScrollIndicator={false}
                                    contentContainerStyle={{ paddingBottom: hp('1%') }}
                                    keyboardShouldPersistTaps='handled'
                                    keyboardDismissMode={'on-drag'}
                                >
                                    {this.showSetsAndReps(this.state.textVersion)}
                                </ScrollView>
                            </View>
                        </View>

                        <CustomDialog
                            visible={this.state.isAlert}
                            title="Alert"
                            desc={this.state.alertMsg}
                            onAccept={this.onAccept.bind(this)}
                            no=''
                            yes='Ok' />

                        <WorkoutCustomDialog
                            visible={this.state.isBackPressed}
                            title=''
                            desc={'Are you sure you want to exit this workout?'}
                            onAccept={this.onExitWorkout.bind(this)}
                            onDecline={() => { this.setState({ isBackPressed: false }) }}
                            onComplete={this.onCompleteWorkout.bind(this)}
                            no='CANCEL'
                            yes='EXIT WORKOUT'
                            third='MY WORKOUT IS COMPLETED'
                        //third='COMPLETE WORKOUT' 
                        />
                    </View>

                </SafeAreaView>
            )
        } else {
            return (
                <KeyboardAwareScrollView
                    resetScrollToCoords={{ x: 0, y: 0 }}
                    contentContainerStyle={styles.mainContainer}
                    enableOnAndroid={false}
                    scrollEnabled={false}>
                    <ImageBackground source={require('../res/post_back.png')} style={styles.mainImageContainer}>
                        <View style={styles.postContents}>
                            <Text style={styles.createPostStyle}>RATE YOUR WORKOUT</Text>
                            {this.renderProfileImg()}
                            <Text style={styles.feedbackNameStyle}>{this.props.planObj.tr_name}</Text>
                            <Text style={styles.feedbackSubStyle}>{this.props.planObj.title}</Text>
                            <AirbnbRating
                                // showRating
                                type='star'
                                ratingCount={5}
                                reviews={['Bad experience !', 'Not that great !', 'It was good !', 'Had a great experience !', 'Absolutely superb !']}
                                imageSize={30}
                                startingValue={this.state.rating}
                                readonly={false}
                                defaultRating={5}
                                isDisabled={false}
                                reviewSize={15}
                                onFinishRating={this.ratingCompleted.bind(this)}
                                style={{ marginTop: hp('5%'), alignSelf: 'center', paddingVertical: 5 }}
                            />
                            <View style={styles.inputRow}>
                                <TextInput
                                    style={styles.textInputStyle1}
                                    placeholder="Type your review..."
                                    placeholderTextColor="#9c9eb9"
                                    value={this.state.post}
                                    numberOfLines={2}
                                    multiline={true}
                                    onChangeText={text => this.setState({ post: text })}
                                />
                            </View>
                            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearSubmitGradient}>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.saveFeebackWorkOut();
                                    }}
                                    style={styles.buttonTuch}>
                                    <Text style={styles.buttonText}>SUBMIT</Text>
                                </TouchableOpacity>
                            </LinearGradient>
                        </View>
                        
                        <CustomDialog
                            visible={this.state.isFeedbackSubmit}
                            title='Success'
                            desc={'Feedback submitted successfully.'}
                            onAccept={this.onFeedbackSubmit.bind(this)}
                            no=''
                            yes='Ok' />
                        
                    </ImageBackground>
                </KeyboardAwareScrollView>
            );
        }
    }
}

const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        height: '100%',
        flex: 1,
    },
    backImageStyle: {
        width: 19,
        height: 16,
        //marginTop: 10,
        //alignSelf: 'center',
    },
    textIndicator: {
        fontSize: 15,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        //flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#2d3142',
        paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    viewTitleCal: {
        //flex:1,
        //flexDirection: 'column',
        // marginLeft: 20,
        // marginTop: 10,
        // marginRight: 20,
    },
    containerGoalPlan: {
        width: wp('90%'),
        //height: hp('36%'),
        marginTop: 15,
        backgroundColor: '#ffffff',
        borderColor: '#ddd',
        borderRadius: 5,
        marginLeft: 20,
        marginRight: 15,
        position: 'relative',
        // alignItems: 'center',
        // alignSelf: 'center',
        //justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    mainContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    mainImageContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
        justifyContent: 'center',
    },
    postContents: {
        width: '90%',
        alignSelf: 'center',
        backgroundColor: '#fff',
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12,
        alignContent: 'center',
        justifyContent: 'center',
        padding: 23,
        marginTop: 10,
    },
    createPostStyle: {
        height: 30,
        alignSelf: 'center',
        color: '#5d5d5d',
        fontFamily: 'Rubik-Medium',
        fontSize: 14,
        fontWeight: '500',
        lineHeight: 30,
    },
    profileImage1: {
        width: 150,
        height: 150,
        aspectRatio: 1,
        alignSelf: 'center',
        backgroundColor: "#D8D8D8",
        marginTop: hp('2%'),
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: "#979797",
        borderRadius: 150 / 2,
        resizeMode: "cover",
        justifyContent: 'flex-end',
    },
    feedbackNameStyle: {
        height: 30,
        alignSelf: 'center',
        marginTop: hp('2%'),
        color: '#5a5a5a',
        fontFamily: 'Rubik-Medium',
        fontSize: 14,
        fontWeight: '500',
        lineHeight: 30,
    },
    feedbackSubStyle: {
        alignSelf: 'center',
        color: '#5d5d5d',
        fontFamily: 'Rubik-Medium',
        fontSize: 10,
        fontWeight: '500',
    },
    inputRow: {
        flexDirection: 'row',
        alignItems: "flex-start",
        height: '15%',
        marginTop: hp('2%'),
        marginBottom: hp('3%'),
        borderRadius: 10,
        backgroundColor: '#f3f4f6',
    },
    linearSubmitGradient: {
        width: '90%',
        height: 50,
        // bottom: 10,
        borderRadius: 15,
        justifyContent: 'center',
        // position: 'absolute',
        alignSelf: 'center',
    },
    buttonTuch:
    {
        backgroundColor: 'transparent',
    },
    buttonText: {
        fontSize: 14,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    textInputStyle1: {
        width: wp('75%'),
        color: '#2d3142',
        marginLeft: 5,
        fontFamily: 'Rubik-Regular',
        fontSize: 12,
        fontWeight: '400',
        letterSpacing: 0.23,
    },

});

export default TextVersion;