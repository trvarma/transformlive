/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import { View, StyleSheet, Image, Text, ImageBackground, TouchableOpacity, StatusBar } from 'react-native';
import { Actions } from 'react-native-router-flux';
// import AsyncStorage from '@react-native-community/async-storage';
// import LinearGradient from 'react-native-linear-gradient';
// import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import GoogleFit, { Scopes } from 'react-native-google-fit';

// const options = {
//     scopes: [
//         Scopes.FITNESS_ACTIVITY_READ_WRITE,
//         Scopes.FITNESS_BODY_READ_WRITE,
//     ],
// }

class GoogleFitTrack extends Component {

    componentDidMount() {
        StatusBar.setHidden(true);
        //    if(GoogleFit.checkIsAuthorized())
        //    {
        //          const options = {
        //             startDate: "2020-03-01T00:00:17.971Z", // required ISO8601Timestamp
        //             endDate: new Date().toISOString() // required ISO8601Timestamp
        //           };

        //           GoogleFit.getDailyStepCountSamples(options)
        //            .then((res) => {
        //                console.log('Daily steps >>> ', res)
        //            })
        //            .catch((err) => {console.warn(err)})

        //            const opt = {
        //             startDate: "2017-01-01T00:00:17.971Z", // required
        //             endDate: new Date().toISOString(), // required
        //             basalCalculation: true, // optional, to calculate or not basalAVG over the week
        //           };

        //           GoogleFit.getDailyCalorieSamples(opt, (err, res) => {
        //             console.log(res);
        //           });
        //    }
        //    else
        //    {
        const options = {
            scopes: [
                Scopes.FITNESS_ACTIVITY_READ_WRITE,
                Scopes.FITNESS_BODY_READ_WRITE,
            ],
        }
        GoogleFit.authorize(options)
            .then((res) => {
                const options = {
                    startDate: "2020-03-30T00:00:17.971Z", // required ISO8601Timestamp
                    endDate: new Date().toISOString() // required ISO8601Timestamp
                };

                GoogleFit.getDailyStepCountSamples(options)
                    .then((res) => {
                        //console.log('Daily steps >>> ', res)
                        //console.log('Daily steps >>> ', res[0])
                        //console.log('Daily steps >>> ', res[1])
                        //console.log('Daily steps >>> ', res[2])
                    })
                    .catch((err) => { console.warn(err) })
                const opt = {
                    // startDate: "2020-03-01T00:00:17.971Z", // required
                    startDate: "2020-03-30T00:00:17.971Z", // required
                    endDate: new Date().toISOString(), // required
                    basalCalculation: true, // optional, to calculate or not basalAVG over the week
                };

                GoogleFit.getDailyCalorieSamples(opt, (err, res1) => {
                    //console.log("cal res: " ,res1);
                });


                // let options1 = {
                //     startDate: new Date(2020, 1, 1).valueOf(), // simply outputs the number of milliseconds since the Unix Epoch
                //     endDate: new Date(2020, 3, 30).valueOf()
                //   };
                //   GoogleFit.getActivitySamples(options1, (err, res) => {
                //     console.log("All Act res: ", err, res)
                //   });

                const opt1 = {
                    startDate: "2020-03-30T00:00:17.971Z", // required
                    endDate: new Date().toISOString(), // required
                  };
                   
                  GoogleFit.getHeightSamples(opt1, (err, res) => {
                    //console.log("Get height: ", res);
                  });

            })
            .catch((err) => {
                //console.log('err >>> ', err)
            })
        //    }
        // const options = {
        //     startDate: "2017-01-01T00:00:17.971Z", // required ISO8601Timestamp
        //     endDate: new Date().toISOString() // required ISO8601Timestamp
        //   };

        //   GoogleFit.getDailyStepCountSamples(options)
        //    .then((res) => {
        //        console.log('Daily steps >>> ', res)
        //    })
        //    .catch((err) => {console.warn(err)})
    }

    onBackPressed() {
        Actions.pop();
    }

    render() {
        return (
            <ImageBackground source={require('../res/app_bg.png')} style={styles.mainContainer}>
                <View>
                    <View style={{
                        flexDirection: 'row',
                        margin: 20,
                    }}>
                        <View style={{ position: 'absolute', zIndex: 111 }}>
                            <TouchableOpacity
                                onPress={() => this.onBackPressed()}>
                                <Image
                                    source={require('../res/ic_back.png')}
                                    style={styles.backImageStyle}
                                />
                            </TouchableOpacity>
                        </View>
                        <Text style={styles.textHeadTitle}>Google Fit</Text>
                    </View>
                </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    backImageStyle: {
        width: 19,
        height: 16,
        alignSelf: 'flex-start',
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#2d3142',
    },
});

export default GoogleFitTrack;
