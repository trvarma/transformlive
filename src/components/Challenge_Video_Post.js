import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    TextInput,
    Image,
    TouchableOpacity,
    BackHandler,
    ImageBackground,
    Text,
    KeyboardAvoidingView,
    ScrollView,
} from 'react-native';
const profileImageSize = 45;
const padding = 10;
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Video from 'react-native-video';
import ImagePicker from 'react-native-image-picker';
import AsyncStorage from '@react-native-community/async-storage';
import { feedUpload } from '../actions';
import { connect } from 'react-redux';
import { ProgressBar, Loader } from './common';
let token = '', userId = '', profileImg = '';

class Challenge_Video_Post extends Component {
    constructor(props) {
        super(props);
        this.state = {
            post: '',
            fileId: 0,
            filepath: {
                data: '',
                uri: '',
            },
            fileData: '',
            fileUri: '',
            fileType: '',
            currentTime: 0,
            duration: 0,
            isFullScreen: false,
            isLoading: true,
            paused: true,
        };
    }

    async componentDidMount() {
        // this.props.employeesFetchRefresh();
        token = await AsyncStorage.getItem('token');
        userId = await AsyncStorage.getItem('userId');
        profileImg = await AsyncStorage.getItem('profileImg');
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            Actions.pop();
            return true;
        });
    }
    onBackPressed() {
        Actions.pop();
    }

    feedUpload(postText, imageUri, VideoUri) {
        if (!postText) {
            Alert.alert('Alert', 'Please enter some text to post.');
        } else {
            let body = '';
            if (imageUri) {
                body = JSON.stringify({
                    title: postText,
                    desc: '',
                    filetype: '1',
                    article_link: '',
                });
            } else if (VideoUri) {
                body = JSON.stringify({
                    title: postText,
                    desc: '',
                    filetype: '2',
                    article_link: '',
                });
            } else {
                body = JSON.stringify({
                    title: '',
                    desc: postText,
                    filetype: '0',
                    article_link: '',
                });

            }
            this.props.feedUpload(
                token,
                imageUri,
                VideoUri,
                body,
            );
        }
    }

    renderVideoData() {
        
        if (this.props.videoUri) {
            return (
                <View >
                    <Video
                        onEnd={this.onEnd}
                        onLoad={this.onLoad}
                        onLoadStart={this.onLoadStart}
                        onProgress={this.onProgress}
                        paused={this.state.paused}
                        ref={videoPlayer => (this.videoPlayer = videoPlayer)}
                        resizeMode={this.state.screenType}
                        source={{ uri: this.props.videoUri }}
                        style={styles.mediaPlayer}
                        volume={10}
                    />
                    <TouchableOpacity
                        style={styles.playButton}
                        onPress={() => {
                            Actions.play({ video: this.props.videoUri });
                        }}>
                        <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_play_white.png')}
                            style={styles.playImageStyle}
                        />
                    </TouchableOpacity>
                </View>
            );
        }
        else if (this.state.fileUri) {
            return (
                <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center' }}>
                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        resizeMode="contain"
                        style={{
                            width: "100%",
                            height: 215,
                            aspectRatio: 1.8,
                            justifyContent: 'center',
                            alignSelf: 'center',
                            borderWidth: .1,
                            borderRadius: 10,
                            marginTop: '20%',
                        }}
                        // source={require('../res/ic_play.png')}
                        source={{ uri: this.state.fileUri }}
                    />
                </View>
            );
        }
    }

    renderFileData() {
        if (this.state.fileUri) {
            return (
                <Image
                    progressiveRenderingEnabled={true}
                    resizeMethod="resize"
                    resizeMode="contain"
                    style={{
                        width: "100%",
                        height: 215,
                        aspectRatio: 1.8,
                        justifyContent: 'center',
                        alignSelf: 'center',
                        borderWidth: .1,
                        borderRadius: 10,
                        marginTop: '15%',
                    }}
                    // source={require('../res/ic_play.png')}
                    source={{ uri: this.state.fileUri }}
                />
            );
        }
    }

    render() {
        return (
            <KeyboardAvoidingView keyboardVerticalOffset={Platform.select({ ios: 0, android: -70 })} style={styles.mainContainer} behavior="padding" enabled>
                <Loader loading={this.props.loading} />
                <ProgressBar loading={this.props.isProgress} progress={this.props.progress} />
                <ScrollView style={styles.mainContainer}>
                    <ImageBackground source={require('../res/post_back.png')} style={styles.mainContainer}>
                        <TouchableOpacity
                            onPress={() => this.onBackPressed()}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_back.png')}
                                style={styles.backImageStyle}
                            />
                        </TouchableOpacity>
                        <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_path.png')}
                            style={styles.pathImageStyle}
                        />

                        {/* <ImageBackground source={require('../res/ic_post_input.png')} style={styles.containerTop}> */}
                        <View style={styles.postContents}>
                            <Text style={styles.createPostStyle}>Upload your Workout</Text>
                            <View style={styles.inputRow}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    style={styles.avatar}
                                    source={{ uri: profileImg }} />

                                <TextInput
                                    style={styles.textInputStyle}
                                    placeholder="Share your exercises..."
                                    placeholderTextColor="#9c9eb9"
                                    value={this.state.post}
                                    // returnKeyType={ 'next' }
                                    multiline={true}
                                    onChangeText={text => this.setState({ post: text })}
                                />

                            </View>

                            {this.renderVideoData()}

                        </View>
                    </ImageBackground>
                </ScrollView>
                <View style={styles.bottomStyle}>

                    <TouchableOpacity
                        onPress={() => { this.chooseImage() }}>
                        <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_record.png')}
                            style={styles.postSmileStyle}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => {
                            this.feedUpload(
                                this.state.post,
                                this.state.fileUri,
                                this.props.videoUri,
                            );
                        }}
                        style={styles.postButtonStyle}>
                        <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_post_button.png')}
                            style={styles.postPollButtonStyle}
                        />
                    </TouchableOpacity>

                </View>

                {/* </ImageBackground> */}


            </KeyboardAvoidingView>
        );
    }

    chooseImage = () => {
        let options = {
            title: 'Select Video',
            mediaType: 'video',
            takePhotoButtonTitle: 'Take Video',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.showImagePicker(options, response => {
            if (response.didCancel) {
                //console.log('User cancelled image picker');
            } else if (response.error) {
                //console.log('ImagePicker Error: ', response.error);
            } else {
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };
                // alert(JSON.stringify(response));s
                this.props.videoUri = '';
                this.setState({
                    filePath: response,
                    fileData: response.data,
                    fileUri: response.uri,
                    fileType: response.type,
                });
            }
        });
    };

    launchCamera = () => {
        let options = {
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.launchCamera(options, response => {
            if (response.didCancel) {
                //console.log('User cancelled image picker');
            } else if (response.error) {
                //console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                //console.log('User tapped custom button: ', response.customButton);
            } else {
                //console.log('launchCamera fileUri', response.uri);
                this.props.videoUri = '';
                this.setState({
                    filePath: response,
                    fileData: response.data,
                    fileUri: response.uri,
                    fileType: response.type,
                });
            }
        });
    };

    launchImageLibrary = () => {
        let options = {
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.launchImageLibrary(options, response => {
            if (response.didCancel) {
                // console.log('User cancelled image picker');
            } else if (response.error) {
                // console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                // console.log('User tapped custom button: ', response.customButton);
            } else {
                // console.log('launchImageLibrary fileUri', response.uri);
                this.props.videoUri = '';
                this.setState({
                    filePath: response,
                    fileData: response.data,
                    fileUri: response.uri,
                    fileType: response.type,
                });
            }
        });
    };

}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
        backgroundColor: '#fff',
    },
    containerTop: {
        flexDirection: 'column',
        flex: 1,
        marginTop: '3%',
        borderWidth: .3,
        borderRadius: 15,
        backgroundColor: '#000',
    },
    pathImageStyle: {
        width: 150,
        height: 5,
        marginTop: '12%',
        alignSelf: 'center'
    },
    pathStyle: {
        width: 126,
        height: 0,
        borderColor: '#cbd7e1',
        borderStyle: 'solid',
        borderWidth: 4,
        opacity: 0.3,
    },
    postContents: {
        width: '100%',
        height: '100%',
        backgroundColor: '#fff',
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12,
        padding: 23,
        marginTop: 20,
    },
    createPostStyle: {
        width: wp('80%'),
        height: 30,
        color: '#2d3142',
        fontFamily: 'Rubik-Medium',
        fontSize: 24,
        fontWeight: '500',
        lineHeight: 30,
    },
    inputRow: {
        flexDirection: 'row',
        alignItems: "center",
        marginTop: 15,
        marginBottom: 15,

    },
    avatar: {
        width: 40,
        height: 40,
        aspectRatio: 1,
        backgroundColor: "#D8D8D8",
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: "#979797",
        alignSelf: 'flex-start',
        borderRadius: profileImageSize / 2,
        width: profileImageSize,
        height: profileImageSize,
        resizeMode: "cover",
        marginRight: padding
    },
    textInputStyle: {
        width: wp('75%'),
        color: '#2d3142',
        fontFamily: 'Rubik-Regular',
        fontSize: 16,
        fontWeight: '400',
        letterSpacing: 0.23,
        alignSelf: 'center',
    },
    bottomStyle: {
        flexDirection: 'row',
        width: '100%',
        height: 60,
        backgroundColor: '#ffffff',
        borderColor: '#cbd7e1',
        borderStyle: 'solid',
        borderWidth: .5,
        justifyContent: 'flex-start',
        alignSelf: 'center',
        alignItems: 'center',
    },
    postCameraStyle: {
        width: 30,
        height: 25,
        alignSelf: 'center',
        marginLeft: 15,

    },
    postSmileStyle: {
        width: 40,
        height: 35,
        alignSelf: 'center',
        marginLeft: 20,

    },
    postPollStyle: {
        width: 30,
        height: 28,
        alignSelf: 'center',
        marginLeft: 20,

    },
    postPollButtonStyle: {
        width: 90,
        height: 35,
        alignSelf: 'flex-end',
    },
    postButtonStyle: {
        alignSelf: 'center',
        position: 'absolute',
        alignItems: 'flex-end',
        right: 15,

    },
    backImageStyle: {
        width: 19,
        height: 16,
        alignSelf: 'flex-start',
        marginLeft: 20,
        marginTop: 20,
        tintColor: 'white',
    },
    mediaPlayer: {
        width: '100%',
        height: 215,
        position: 'relative',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        borderWidth: .1,
        borderRadius: 10,
        backgroundColor: 'black',
        marginTop: '15%',
    },

    playButton: {
        position: 'absolute',
        alignSelf: 'center',
        bottom: 70,

    },
    playImageStyle: {
        width: 70,
        height: 70,
        justifyContent: 'center',

    },
    container: {
        flex: 1,
        alignSelf: 'center',
    },
});

const mapStateToProps = state => {
    const { feedUpload } = state.feed;
    const loading = state.feed.loading;
    const isProgress = state.catcre.isProgress;
    const progress = state.catcre.progress;
    const error = state.feed.error;
    const videoUri = state.catcre.videoUri;
    return { feedUpload, loading, error, isProgress, progress, videoUri };
};

export default connect(
    mapStateToProps,
    {
        feedUpload,
    },
)(Challenge_Video_Post);