import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  BackHandler,
  ScrollView,
  TextInput,
  ImageBackground,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { Loader, CustomDialog } from '../common';
import { withNavigationFocus } from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import LogUtils from '../../utils/LogUtils.js';
import DeviceInfo from 'react-native-device-info';
import { allowFunction } from '../../utils/ScreenshotUtils.js';
import { GetGoalsubctgAction } from '../../actions/traineeActions'

class AgeConfirm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      age: '',
      isAlert: false,
      alertMsg: '',
      isShowAgeNext:true,
      isOverride:false,
      isOverride_msg:''
    };
  }

  async componentDidMount() {
    try{

      allowFunction();
      if(this.props.trainee.calCounterSetupStatus){
        this.setState({
          isOverride:true,
          isOverride_msg:this.props.trainee.traineeDetails.cc_override_msg
        })
      }

      // let token = await AsyncStorage.getItem('token');
      // let params = { "g_id": "0" };
      // this.props.GetGoalsubctgAction(token, params);

      this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
        this.onBackPressed();
        return true;
      });
    }catch(error){
      console.log(error);
    }
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  onBackPressed() {
    Actions.pop();
  }

  async onAccept() {
    this.setState({ isAlert: false,alertMsg: '' });
  }

  onConfirm(){
    this.setState({
      isOverride:false,
      isOverride_msg:''
    })
  }

  onDeny(){
    this.setState({
      isOverride:false,
      isOverride_msg:''
    })
    Actions.thome();
  }

  render() {
    
    return (
      <KeyboardAwareScrollView
        resetScrollToCoords={{ x: 0, y: 0 }}
        contentContainerStyle={styles.containerStyle}
        enableOnAndroid={false}
        onKeyboardDidShow={(frames)=>this.setState({isShowAgeNext:false})}
        onKeyboardDidHide={(frames)=>this.setState({isShowAgeNext:true})}
        scrollEnabled={false}>
        <ImageBackground source={require('../../res/app_bg.png')} style={{ width: '100%', height: '100%' }}>
          <Loader loading={this.props.loading} />
          <ScrollView contentContainerStyle={{ paddingBottom: hp('15%') }}>
            <View style={{
              flexDirection: 'row',
              margin: 20,
            }}>
              <View style={{ position: 'absolute', zIndex: 111 }}>
                <TouchableOpacity
                  style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                  onPress={() => this.onBackPressed()}>
                  <Image
                    progressiveRenderingEnabled={true}
                    resizeMethod="resize"
                    source={require('../../res/ic_back.png')}
                    style={styles.backImageStyle}
                  />
                </TouchableOpacity>
              </View>

              <Text style={styles.textIndicator}>1 / 4</Text>
            </View>

            <View style={styles.optionInnerContainer}>
              <Image
                progressiveRenderingEnabled={true}
                resizeMethod="resize"
                source={require('../../res/ic_app.png')}
                style={styles.appImageStyle}
              />
            </View>

            <Text style={styles.textStyle}>What is your Age?</Text>

            <Text style={styles.desc}>We need the below information to personalise your experience</Text>

            <View style={styles.containerMobileStyle}>
              <TextInput
                ref={input => { this.ageTextInput = input; }}
                style={styles.textInputStyle}
                placeholder="Enter your age"
                maxLength={3}
                placeholderTextColor="grey"
                keyboardType="numeric"
                returnKeyType='done'
                value={this.state.age}
                onChangeText={text => {
                  if (text) {
                    let newText = '';
                    let numbers = '0123456789';
                    for (var i = 0; i < text.length; i++) {
                      if (numbers.indexOf(text[i]) > -1) {
                        newText = newText + text[i];
                      }
                      else {
                        this.setState({ alertMsg: 'Please enter numbers only' });
                        this.setState({ isAlert: true });
                      }
                    }
                    if (newText) {
                      this.setState({ age: newText });
                    }
                  }
                  else {
                    this.setState({ age: '' });
                  }
                }}
              />
            </View>
          </ScrollView>
          {
            this.state.isShowAgeNext && 
            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
              <TouchableOpacity
                style={styles.buttonTuch}
                onPress={() => {
                  this.profileUpload(
                    this.state.age,
                  );
                }}>
                <Text style={styles.buttonText}>Next</Text>
              </TouchableOpacity>
            </LinearGradient>
          }

          <CustomDialog
            visible={this.state.isAlert}
            title='Alert'
            desc={this.state.alertMsg}
            onAccept={this.onAccept.bind(this)}
            no=''
            yes='Ok' />

          <CustomDialog
            visible={this.state.isOverride}
            title='Alert'
            desc={this.state.isOverride_msg}
            onAccept={this.onConfirm.bind(this)}
            onDecline = {this.onDeny.bind(this)}
            no='No'
            yes='Yes' />

        </ImageBackground>
      </KeyboardAwareScrollView>
    )
  }

  async profileUpload(age) {
    if (!age) {
      this.setState({ isAlert: true, alertMsg: 'Please enter your age' });
    } else {

      if (age >= 15 && age <= 80) {
        LogUtils.firebaseEventLog('click', {
          p_id: 102,
          p_category: 'Registration',
          p_name: 'Age',
        });
        await AsyncStorage.setItem('age', this.state.age.toString());
        Actions.genderCnf();
      } else {
        this.setState({ isAlert: true, alertMsg: 'Persons with age between 15 and 80 can only register', age: '' });
      }
    }
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    width: '100%',
    height: '100%',
    flex: 1,
  },
  backImageStyle: {
    width: 19,
    height: 16,
    marginTop: 10,
    alignSelf: 'center',
  },
  textIndicator: {
    fontSize: 14,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    flex: 1,
    textAlign: 'center',
    alignSelf: 'center',
    color: '#2d3142',
    paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
    paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
  },
  optionInnerContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  appImageStyle: {
    width: 200,
    height: 60,
    marginTop: 40,
  },
  textStyle: {
    fontSize: 20,
    marginTop: hp('3%'),
    marginLeft: 40,
    marginRight: 40,
    //marginBottom: hp('3%'),
    fontWeight: '500',
    color: '#2d3142',
    lineHeight: 30,
    textAlign: 'center',
    fontFamily: 'Rubik-Medium',
  },
  subtextOneStyle: {
    fontSize: 20,
    marginTop: hp('3%'),
    marginLeft: 40,
    marginRight: 40,
    fontWeight: '500',
    color: '#2d3142',
    lineHeight: 30,
    textAlign: 'center',
    fontFamily: 'Rubik-Medium',
  },
  containerMobileStyle: {
    borderBottomWidth: 1,
    backgroundColor: '#ffffff',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: '#ddd',
    borderRadius: 15,
    position: 'relative',
    alignSelf: 'center',
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10,
    padding: 8,
    shadowColor: '#4075cd',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  textInputStyle: {
    height: 40,
    flex: 1,
    fontSize: 13,
    marginLeft: 10,
    fontFamily: 'Rubik-Regular',
    color: '#2d3142',
  },
  linearGradient: {
    width: '90%',
    height: 50,
    bottom: 0,
    borderRadius: 25,
    justifyContent: 'center',
    position: 'absolute',
    alignSelf: 'center',
    marginBottom: 15,
  },
  buttonTuch:
  {
    backgroundColor: 'transparent',
  },
  buttonText: {
    fontSize: 16,
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    fontFamily: 'Rubik-Medium',
    fontWeight: '500',
  },
  desc: {
    fontSize: 14,
    marginTop: 10,
    marginLeft: 40,
    marginRight: 40,
    fontWeight: '400',
    color: '#9c9eb9',
    lineHeight: 24,
    textAlign: 'center',
    fontFamily: 'Rubik-Regular',
    marginTop: hp('3%'),
    marginBottom: hp('3%'),
  },
});

const mapStateToProps = state => {
  const { goals, trainerCodes } = state.procre;
  const loading = state.procre.loading;
  const error = state.procre.error;
  const trainee = state.traineeFood;
  return { loading, error, goals, trainerCodes ,trainee};
};

export default withNavigationFocus(
  connect(
    mapStateToProps,
    {GetGoalsubctgAction },
  )(AgeConfirm),
);

