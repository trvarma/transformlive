import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withNavigationFocus } from 'react-navigation';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    ImageBackground,
    ScrollView,
    Dimensions,
    Image,
    PanResponder,
} from 'react-native';
import times from 'lodash.times';
import PropTypes from 'prop-types';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage';
const GAUGE_WIDTH = Math.floor(Dimensions.get('window').width - 25)
const INTERVAL_WIDTH = 13
import Slider from '@react-native-community/slider';
import LogUtils from '../../utils/LogUtils.js';
import DeviceInfo from 'react-native-device-info';
import { allowFunction } from '../../utils/ScreenshotUtils.js';
const scale1 = (v, inputMin, inputMax, outputMin, outputMax) => {
    return Math.round(((v - inputMin) / (inputMax - inputMin)) * (outputMax - outputMin) + outputMin)
}

function getLbs(val) {
    var value = parseFloat((val * 2.2046).toFixed(2));
    if (value < 0) {
        value = 0;
    }
    return value;
}

function getInches(val) {
    console.log(val)
    var value = parseFloat((val / 2.54).toFixed(2));
    if (value < 0) {
        value = 0;
    }
    return value;
}

const convertedCentoFeet = (values) => {
    var realFeet = ((values * 0.393700) / 12);
    var feet = Math.floor(realFeet);
    var inches = Math.round((realFeet - feet) * 12);
    return feet + "'" + inches;
}

class HeightWeightConfirm extends Component {
    constructor(props) {
        super(props);
        this._handleScroll = this._handleScroll.bind(this)
        this._handleScrollEnd = this._handleScrollEnd.bind(this)
        this._handleContentSizeChange = this._handleContentSizeChange.bind(this)

        this.scrollMin = 0
        this.scrollMax = this._getScrollMax(props)
        this._scrollQueue = null
        this._value = props.value

        this.state = {
            contentOffset: this._scaleValue(this._value),
            value_w: props.value || props.min,
            sliderValue: 50,
        };
    }

    componentWillReceiveProps(nextProps) {
        LogUtils.infoLog1('WillReceiveProps called');
        // this.scrollMax = this._getScrollMax(nextProps)

        // if (nextProps.value !== this._value) {
        //     this._setScrollQueue({
        //         x: this._scaleValue(nextProps.value, nextProps),
        //         animate: true,
        //     })

        //     if (!this._contentSizeWillChange(nextProps)) {
        //         this._resolveScrollQueue()
        //     }
        // }

    }
    
    componentWillMount() {
        allowFunction();
        LogUtils.infoLog1('Will Mount called');
        this._panResponder = PanResponder.create({
            onMoveShouldSetResponderCapture: () => true,
            onMoveShouldSetPanResponderCapture: (evt, gestureState) => {
                return Math.abs(gestureState.dy) > 10;  // can adjust this num
            },
            onPanResponderGrant: (e, gestureState) => {
                this.fScroll.setNativeProps({ scrollEnabled: false })
            },
            onPanResponderMove: () => { },
            onPanResponderTerminationRequest: () => true,
        })
    }

    _contentSizeWillChange(nextProps) {
        let { min, max } = nextProps
        if (min !== this.props.min || max !== this.props.max) {
            return true
        }
        return false
    }
    _getScrollMax(props = this.props) {
        return (props.max - props.min) * INTERVAL_WIDTH
    }

    _scaleScroll(x, props = this.props) {
        let { min, max } = props
        return scale1(x, this.scrollMin, this.scrollMax, min, max)
    }

    _scaleValue(v, props = this.props) {
        let { min, max } = props
        return scale1(v, min, max, this.scrollMin, this.scrollMax)
    }

    _setScrollQueue(scrollTo) {
        this._scrollQueue = scrollTo
    }

    _resolveScrollQueue() {
        if (this._scrollQueue !== null) {
            this._scrollView && this._scrollView.scrollTo(this._scrollQueue)
            this._handleScrollEnd()
        }
    }

    _handleContentSizeChange() {
        this._resolveScrollQueue()
    }

    async _handleScroll(event) {
        if (this._scrollQueue) return

        let offset = event.nativeEvent.contentOffset.x
        let { min, max } = this.props

        let val = this._scaleScroll(offset)

        if (val !== this._value) {
            this._value = val
            this.props.onChange(val)
            this.setState({ value_w: val });
        }
    }

    _handleScrollEnd() {
        //this._value = this.props.value
        this._scrollQueue = null
    }

    _getIntervalSize(val) {
        let { largeInterval, mediumInterval } = this.props

        if (val % largeInterval == 0) return 'large'
        if (val % mediumInterval == 0) return 'medium'
        return 'small'
    }

    _renderIntervals() {
        let { min, max } = this.props
        let range = max - min + 1

        let values = times(range, (i) => i + min)

        return values.map((val, i) => {
            let intervalSize = this._getIntervalSize(val)

            return (
                <View key={`val-${i}`} style={styles.intervalContainer}>
                    {intervalSize === 'large' && (
                        <Text style={[styles.intervalValue, this.props.styles.intervalValue]}>{val}</Text>
                    )}

                    <View style={[styles.interval, styles[intervalSize], this.props.styles.interval, this.props.styles[intervalSize]]} />
                </View>
            )
        })
    }

    onBackPressed() {
        Actions.pop();
    }

    renderWeightValue() {
        return (
            <View style={styles.personViewStyle}>
                <View style={{ width: '90%', alignSelf: 'center', position: 'relative' }}>
                    <Image
                        source={require('../../res/ic_person_height.png')}
                        style={styles.personImageStyle}>

                    </Image>

                </View>
                <View style={[styles.container, this.props.styles.container]}>
                    <View
                        style={styles.triangleshape}>
                    </View>
                    <ScrollView
                        {...this._panResponder.panHandlers}
                        ref={r => this._scrollView = r}
                        onScrollEndDrag={() => this.fScroll.setNativeProps({ scrollEnabled: true })}
                        onScrollBeginDrag={() => this.fScroll.setNativeProps({ scrollEnabled: true })}
                        nestedScrollEnabled={true}
                        automaticallyAdjustInsets={false}
                        horizontal={true}
                        decelerationRate={0}
                        snapToInterval={INTERVAL_WIDTH}
                        snapToAlignment="start"
                        showsHorizontalScrollIndicator={false}
                        onScroll={this._handleScroll}
                        onMomentumScrollEnd={this._handleScrollEnd}
                        onContentSizeChange={this._handleContentSizeChange}
                        scrollEventThrottle={100}
                        contentOffset={{ x: this.state.contentOffset }}>
                        <View style={[styles.intervals, this.props.styles.intervals]}>
                            {this._renderIntervals()}
                        </View>
                    </ScrollView>
                </View>

                <View style={{
                    position: 'absolute',
                    bottom: 0,
                    left: wp('12.5%'),
                }}>
                    <Text style={styles.weighttextStyle}>
                        {this.state.sliderValue} Kg
                    </Text>

                    <Text style={styles.weighttextStyle1}>
                        {getLbs(this.state.sliderValue)} lbs
                    </Text>
                </View>

            </View>
        );

    }

    renderValue() {
        try{
            if (this._value === 130) {
                return (
                    <View style={{
                        position: 'absolute',
                        top: 0,
                        right: wp('12%'),
                    }}>
                        <Text style={styles.HeighttextStyle}>
                            {this.props.value} cm
                        </Text>
                        <Text style={styles.HeighttextStyle1}>
                            {/* {getInches(this.props.value)} inch */}
                            {convertedCentoFeet(this.props.value)}"
                        </Text>
    
                    </View>
                    // <View
                    // >
                    //     <Text style={styles.HeighttextStyle}>
                    //         {this.props.value} cm
                    // </Text>
    
                    // </View>
                );
            } else {
                return (
                    <View style={{
                        position: 'absolute',
                        top: 0,
                        right: wp('12%'),
                    }}>
                        <Text style={styles.HeighttextStyle}>
                            {this._value} cm
                        </Text>
                        <Text style={styles.HeighttextStyle1}>
                            {/* {getInches(this._value)} inch */}
                            {convertedCentoFeet(this._value)}"
                        </Text>
    
                    </View>
                    // <View>
                    //     <Text style={styles.HeighttextStyle}>
                    //         {this._value} cm
                    // </Text>
    
                    // </View>
                );
    
            }
        }catch(error){
            console.log(error);
        }
    }

    async onButtonPressed() {
        try{
        await AsyncStorage.setItem('height_value', this.state.value_w.toString());
        await AsyncStorage.setItem('weight_value', this.state.sliderValue.toString());
        LogUtils.firebaseEventLog('click', {
            p_id: 104,
            p_category: 'Registration',
            p_name: 'Height',
        });
        Actions.actLevel();
    }catch(error){
        console.log(error);
    }
    }

    render() {

        return (
            <ImageBackground source={require('../../res/app_bg.png')} style={styles.containerStyle}>

                <ScrollView
                    ref={(e) => { this.fScroll = e }}
                    nestedScrollEnabled={true}
                    contentContainerStyle={{ paddingBottom: hp('5%') }}>

                    <View style={{
                        flexDirection: 'row',
                        margin: 20,
                    }}>
                        <View style={{ position: 'absolute', zIndex: 111 }}>
                            <TouchableOpacity
                                style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                                onPress={() => this.onBackPressed()}>
                                <Image
                                    source={require('../../res/ic_back.png')}
                                    style={styles.backImageStyle}
                                />
                            </TouchableOpacity>
                        </View>

                        <Text style={styles.textIndicator}>3 / 4</Text>
                    </View>

                    <View style={styles.optionInnerContainer}>
                        <Image
                            source={require('../../res/ic_app.png')}
                            style={styles.appImageStyle}
                        />
                    </View>
                    <Text style={styles.subtextOneStyle}>What is your weight and height?</Text>

                    <Text style={styles.desc}>We need the below information to personalise your experience</Text>

                    <View style={{ flexDirection: 'column', marginTop: 20 }}>
                        {this.renderValue()}
                        {this.renderWeightValue()}
                    </View>

                    <Slider
                        style={styles.sliderImageStyle}
                        minimumValue={0}
                        maximumValue={150}
                        minimumTrackTintColor="#8c52ff"
                        maximumTrackTintColor="#000000"
                        scrollEventThrottle={100}
                        value={this.state.sliderValue}
                        onValueChange={(sliderValue) => this.setState({ sliderValue: Math.floor(sliderValue) })}
                    />

                </ScrollView>

                <View style={styles.viewBottom}>
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                        <TouchableOpacity
                            onPress={() => this.onButtonPressed()}>
                            <Text style={styles.buttonText}>Next</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                </View>

            </ImageBackground>
        );
    }
}

HeightWeightConfirm.propTypes = {
    min: PropTypes.number,
    max: PropTypes.number,
    largeInterval: PropTypes.number,
    mediumInterval: PropTypes.number,
    value: PropTypes.number,
    onChange: PropTypes.func,
    styles: PropTypes.object,
}

HeightWeightConfirm.defaultProps = {
    min: 130,
    max: 210,
    mediumInterval: 5,
    largeInterval: 10,
    value: 130,
    onChange: () => { },
    styles: {},
}

const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        height: '100%',
        flex: 1,
    },
    linearGradient: {
        width: '95%',
        height: 50,
        borderRadius: 25,
        justifyContent: 'center',
        backgroundColor: 'transparent',
        alignSelf: 'center',
        marginBottom: 15
    },
    buttonText: {
        fontSize: 16,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    // Scale Styles
    container: {
        height: 40,
        width: GAUGE_WIDTH,
        marginVertical: 8,
        alignSelf: 'center',
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        transform: [{ rotate: '90deg' }],
        top: 0,
    },
    triangleshape: {
        width: 0,
        height: 0,
        borderTopWidth: 15,
        borderWidth: 10,
        borderBottomWidth: 0,
        borderColor: 'transparent',
        borderTopColor: '#8c52ff',
        alignSelf: 'center',
        marginBottom: -12,

    },
    intervals: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        paddingHorizontal: GAUGE_WIDTH / 2,
        marginHorizontal: -INTERVAL_WIDTH / 2,
    },
    intervalContainer: {
        width: INTERVAL_WIDTH,
        alignItems: 'center',

    },
    interval: {
        width: 1,
        backgroundColor: '#8c52ff',
    },
    intervalValue: {
        width: 35,
        fontSize: 16,
        fontWeight: 'bold',
        color: '#8c52ff',
        position: 'absolute',
        bottom: -65,
    },
    small: {
        height: 20,
    },
    medium: {
        height: 25,
    },
    large: {
        backgroundColor: '#8c52ff',
        width: 3,
        height: 35,
        position: 'absolute',
    },
    optionInnerContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignSelf: 'center',
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'center',
    },
    appImageStyle: {
        width: 200,
        height: 60,
        marginTop: 40,
    },
    personImageStyle: {
        alignSelf: 'flex-end',
        width: 65,
        height: 220,
        marginTop: 20,
        marginRight: -40,
    },
    textStyle: {
        fontSize: 18,
        marginTop: hp('2%'),
        marginLeft: 40,
        marginRight: 40,
        fontWeight: '500',
        color: '#2d3142',
        lineHeight: 30,
        textAlign: 'center',
        fontFamily: 'Rubik-Regular',
    },
    datePicker: {
        backgroundColor: 'transparent',
        width: '100%',
        alignSelf: 'flex-start',
        alignContent: 'flex-start',
        justifyContent: 'flex-start',
    },
    viewBottom: {
        width: wp('93%'),
        alignContent: 'center',
        alignSelf: 'center',
        position: 'absolute',
        bottom: 0,
        backgroundColor: '#fff',
    },
    subtextOneStyle: {
        fontSize: 20,
        marginTop: hp('2%'),
        marginLeft: 40,
        marginRight: 40,
        fontWeight: '500',
        color: '#2d3142',
        lineHeight: 30,
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    sliderImageStyle: {
        width: wp('75%'),
        height: hp('15%'),
        resizeMode: 'contain',
        alignSelf: 'center',
    },
    personViewStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        //backgroundColor:'red'
    },
    weighttextStyle: {
        fontSize: 24,
        fontWeight: '500',
        color: '#2d3142',
        letterSpacing: 0.72,
        fontFamily: 'Rubik-Medium',
        // position: 'absolute',
        // bottom: 0,
        // left: wp('12.5%'),
    },
    weighttextStyle1: {
        fontSize: 10,
        fontWeight: '500',
        color: '#9c9eb9',
        letterSpacing: 0.72,
        fontFamily: 'Rubik-Medium',
        alignSelf: 'center',
        marginTop: 5,
        // position: 'absolute',
        // bottom: 0,
        // left: wp('12.5%'),
    },
    HeighttextStyle: {
        fontSize: 24,
        fontWeight: '500',
        color: '#2d3142',
        letterSpacing: 0.72,
        fontFamily: 'Rubik-Medium',

    },
    HeighttextStyle1: {
        fontSize: 10,
        fontWeight: '500',
        color: '#9c9eb9',
        letterSpacing: 0.72,
        alignSelf: 'center',
        fontFamily: 'Rubik-Medium',
        marginTop: 5,
    },
    textIndicator: {
        fontSize: 14,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#2d3142',
        // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    desc: {
        fontSize: 14,
        marginTop: 10,
        marginLeft: 40,
        marginRight: 40,
        fontWeight: '400',
        color: '#9c9eb9',
        lineHeight: 24,
        textAlign: 'center',
        fontFamily: 'Rubik-Regular',
        marginTop: hp('1%'),
        //marginBottom: hp('1%'),
      },
});

const mapStateToProps = state => {

    return {};
};

export default withNavigationFocus(
    connect(
        mapStateToProps,
        {},
    )(HeightWeightConfirm),
);
