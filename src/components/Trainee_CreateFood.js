import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    BackHandler,
    ScrollView,
    TextInput,
    ImageBackground,
    FlatList,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Loader, CustomDialog, NoInternet } from './common';
import LinearGradient from 'react-native-linear-gradient';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import LogUtils from '../utils/LogUtils.js';
import DeviceInfo from 'react-native-device-info';
import { BASE_URL, SWR } from '../actions/types';
import NetInfo from "@react-native-community/netinfo";
import moment from "moment";
import AsyncStorage from '@react-native-community/async-storage';
import { Dropdown } from 'react-native-material-dropdown';
import { allowFunction } from '../utils/ScreenshotUtils.js'; 

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
};
let subCatIds = '', subcatVals = '';

function validateArray(arrTestArray) {
    var valid = true;
    arrTestArray.map((item) => {
        if (item.is_mandatory === 1 && !item.value) {
            valid = false;
        }
    })
    return valid;
}

class Trainee_CreateFood extends Component {

    constructor(props) {
        super(props);
        this.state = {
            foodName: '',
            brandName: '',
            isVeg: 'V',
            serSize: '',
            serUnits: 'Grams',
            serPerContainer: '',
            isAlert: false,
            loading: false,
            alertMsg: '',
            saveText: 'Next',
            arrNutritionFacts: [],
            isNoIntenet: false,
            measures: [{ value: 'Grams' }, { value: 'Milliliters' }, { value: 'Units' }],
        };
    }

    async componentDidMount() {
        allowFunction();
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.getNutritionFacts();
            }
            else {
                this.setState({ isNoIntenet: true });
            }
        });

        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }


    async onBackPressed() {
        if (this.state.saveText === 'Save') {
            this.setState({ saveText: 'Next' });
        }
        else {
            Actions.pop();
        }
    }

    async getNutritionFacts() {
        fetch(
            `${BASE_URL}/master/foodattrmst`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('NutritionFacts data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    if (data.data.length > 0) {
                        data.data.map((item) => {
                            item.value = '';
                        })
                        this.setState({ arrNutritionFacts: data.data });
                    }

                } else {
                }
            })
            .catch(function (error) {
            });
    }


    async onAccept() {
        this.setState({ alertMsg: '' });
        this.setState({ isAlert: false });
    }

    async onRetry() {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.getNutritionFacts();
            }
            else {
                this.setState({ isNoIntenet: true });
            }
        });
    }

    renderVegNonveg() {
        if (this.state.isVeg === 'V') {
            return (
                <View style={styles.viewServingSize}>
                    <TouchableOpacity style={styles.viewVeg}
                        onPress={() => this.setState({ isVeg: 'V' })}>
                        <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/check.png')}
                            style={styles.imgVegSelect}
                        />
                        <Text style={styles.textVegSelect}>Veg</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.viewVeg}
                        onPress={() => this.setState({ isVeg: 'N' })}>
                        <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_uncheck_tick.png')}
                            style={styles.imgVegSelect1}
                        />
                        <Text style={styles.textVegSelect1}>Non-Veg</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.viewVeg}
                        onPress={() => this.setState({ isVeg: 'E' })}>
                        <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_uncheck_tick.png')}
                            style={styles.imgVegSelect1}
                        />
                        <Text style={styles.textVegSelect1}>Egg</Text>
                    </TouchableOpacity>
                </View>
            );
        }
        else if (this.state.isVeg === 'N') {
            return (
                <View style={styles.viewServingSize}>
                    <TouchableOpacity style={styles.viewVeg}
                        onPress={() => this.setState({ isVeg: 'V' })}>
                        <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_uncheck_tick.png')}
                            style={styles.imgVegSelect1}
                        />
                        <Text style={styles.textVegSelect1}>Veg</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.viewVeg}
                        onPress={() => this.setState({ isVeg: 'N' })}>
                        <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/check.png')}
                            style={styles.imgVegSelect}
                        />
                        <Text style={styles.textVegSelect}>Non-Veg</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.viewVeg}
                        onPress={() => this.setState({ isVeg: 'E' })}>
                        <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_uncheck_tick.png')}
                            style={styles.imgVegSelect1}
                        />
                        <Text style={styles.textVegSelect1}>Egg</Text>
                    </TouchableOpacity>
                </View>
            );
        }
        else if (this.state.isVeg === 'E') {
            return (
                <View style={styles.viewServingSize}>
                    <TouchableOpacity style={styles.viewVeg}
                        onPress={() => this.setState({ isVeg: 'V' })}>
                        <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_uncheck_tick.png')}
                            style={styles.imgVegSelect1}
                        />
                        <Text style={styles.textVegSelect1}>Veg</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.viewVeg}
                        onPress={() => this.setState({ isVeg: 'N' })}>
                        <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_uncheck_tick.png')}
                            style={styles.imgVegSelect1}
                        />
                        <Text style={styles.textVegSelect1}>Non-Veg</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.viewVeg}
                        onPress={() => this.setState({ isVeg: 'E' })}>
                        <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/check.png')}
                            style={styles.imgVegSelect}
                        />
                        <Text style={styles.textVegSelect}>Egg</Text>
                    </TouchableOpacity>
                </View>
            );
        }
    }

    FlatListItemSeparator = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: "100%",
                    backgroundColor: "transparent",
                }}
            />
        );
    }

    changeValue = (hey, value) => {
        const tempArray = [...this.state.arrNutritionFacts];

        tempArray.map((item) => {
            if (item.id === hey.id) {
                item.value = value;
            }
        });

        this.setState(() => {
            return {
                arrNutritionFacts: tempArray,
            };
        });

    }

    renderAllData() {
        if (this.state.saveText === 'Next') {
            return (
                <ScrollView contentContainerStyle={{ paddingBottom: hp('15%') }}>
                    <View style={styles.mainContainerStyle}>
                        <View style={{ marginTop: 15, }} />
                        {/* <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.textFoodHeaderBack}>
                            <Text style={styles.textFoodHeader}>New Food</Text>
                        </LinearGradient> */}
                        <Text style={styles.textTitle}>Food Name ( Required)</Text>
                        <View style={styles.spinnersBg}>
                            <TextInput
                                style={styles.textInputStyle}
                                keyboardType='default'
                                placeholder='Ex.Chicken Curry'
                                placeholderTextColor='grey'
                                maxLength={100}
                                value={this.state.foodName}
                                returnKeyType='next'
                                onChangeText={text => {
                                    this.setState({ foodName: text });
                                }}
                                onSubmitEditing={() => { this.homeMadeInput.focus(); }}
                            />
                        </View>
                        <Text style={styles.textTitle}>Brand Name ( Optional )</Text>
                        <View style={styles.spinnersBg}>
                            <TextInput
                                ref={input => { this.homeMadeInput = input; }}
                                style={styles.textInputStyle}
                                keyboardType='default'
                                placeholder='Ex.Homemade'
                                placeholderTextColor='grey'
                                maxLength={100}
                                value={this.state.brandName}
                                returnKeyType='next'
                                onChangeText={text => {
                                    this.setState({ brandName: text });
                                }}
                                onSubmitEditing={() => { this.inputSerSize.focus(); }}
                            />
                        </View>

                        <Text style={styles.textTitle}>Food Type</Text>
                        {this.renderVegNonveg()}
                        <View style={{ marginTop: 15, }} />
                        {/* <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.textFoodHeaderBack}>
                            <Text style={styles.textFoodHeader}>Servings</Text>
                        </LinearGradient> */}
                        <Text style={styles.textTitle}>Portion Size( Required )</Text>

                        <View style={styles.viewServingSize}>
                            <View style={{ flex: 1 }}>
                                <View style={styles.spinnersBg1}>
                                    <TextInput
                                        ref={input => { this.inputSerSize = input; }}
                                        style={styles.textInputStyle}
                                        keyboardType="numeric"
                                        placeholder="100"
                                        placeholderTextColor="grey"
                                        maxLength={3}
                                        value={this.state.serSize}
                                        returnKeyType='next'
                                        onChangeText={text => {
                                            this.setState({ serSize: text });
                                        }}
                                        onSubmitEditing={() => { this.inputSerUnits.focus(); }}
                                    />
                                </View>
                            </View>
                            {/* <View style={{ flex: 1 }}></View> */}
                            <View style={{
                                width: wp('43%'),
                                backgroundColor: '#ffffff',
                                justifyContent: 'center',
                                flexDirection: 'column',
                                borderRadius: 10,
                                position: 'relative',
                                alignContent: 'center',
                                alignSelf: 'center',
                                marginTop: 3,
                                height: 45,
                                marginLeft: 10,
                                padding: 10,
                                shadowColor: '#4075cd',
                                shadowOffset: { width: 0, height: 2 },
                                shadowOpacity: 0.25,
                                shadowRadius: 3.84,
                                elevation: 6,
                            }}>
                                <Dropdown
                                    // style={{ paddingBottom: 5, alignSelf: 'center',  marginTop: -10, }}
                                    // DropDownInsets={{  top: -10 }}
                                    ref={this.codeRef}
                                    value={this.state.serUnits}
                                    onChangeText={(value) => {
                                        this.setState({ serUnits: value });
                                    }}
                                    fontSize={14}
                                    dropdownOffset={{ left: 5, top: 20 }}
                                    // label='Color code'
                                    data={this.state.measures}
                                    propsExtractor={({ props }, index) => props}
                                />
                            </View>

                        </View>

                        <Text style={styles.textTitle}>Serving(Ex: 1 Glass/2 Pieces/1 Bowl)</Text>
                        <View style={styles.spinnersBg}>
                            <TextInput
                                ref={input => { this.inputSerperCont = input; }}
                                style={styles.textInputStyle}
                                keyboardType="default"
                                placeholder="2 Cups"
                                placeholderTextColor="grey"
                                maxLength={100}
                                value={this.state.serPerContainer}
                                returnKeyType='done'
                                onChangeText={text => {
                                    this.setState({ serPerContainer: text });
                                }}
                            />
                        </View>

                        <Text style={styles.textTitle}>Hints</Text>
                        <Text style={styles.textHint}>{`1. Rice/Curry bowls --> Portion Size : 200 gm, Serving : 1 Bowl`}</Text>
                        <Text style={styles.textHint}>{`2. Rolls/Breads --> Portion Size : 100 gm, Serving : 2 Pieces`}</Text>
                        <Text style={styles.textHint}>{`3. Biscuits --> Portion Size : 100 gm, Serving : 4 Pieces`}</Text>
                        <Text style={styles.textHint}>{`4. Juices --> Portion Size : 100 ml, Serving : 1 Glass`}</Text>
                        <Text style={styles.textHint}>{`5. Fruits --> Portion Size : 100 gm, Serving : 1 Bowl(Optional)`}</Text>
                        <Text style={styles.textHint}>{`6. Pickles --> Portion Size : 5 gm, Serving : 1 Teaspoon`}</Text>

                    </View>

                </ScrollView>
            );
        }
        else {
            return (
                <View style={styles.mainContainerStyle}>
                    {/* <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.textFoodHeaderBack}>
                        <Text style={styles.textFoodHeader}>Nutrition Facts</Text>
                    </LinearGradient> */}
                    <FlatList
                        contentContainerStyle={{ paddingBottom: hp('25%') }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.arrNutritionFacts}
                        keyExtractor={item => item.id}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({ item }) => {
                            return <View key={item.id} style={styles.containerMobileStyle}>
                                {item.mesurment
                                    ? (
                                        <Text style={styles.textListName}>{`${item.name} ( ${item.mesurment} )`}</Text>
                                    )
                                    : (
                                        <Text style={styles.textListName}>{`${item.name}`}</Text>
                                    )}

                                {/* <View style={styles.spinnersBg}> */}
                                <TextInput
                                    style={styles.textListInput}
                                    keyboardType='numeric'
                                    placeholder={item.is_mandatory === 1 ? 'Required' : 'Optional'}
                                    placeholderTextColor='grey'
                                    maxLength={6}
                                    value={item.value}
                                    returnKeyType='next'
                                    onChangeText={text => {
                                        this.changeValue(item, text)
                                    }}
                                />
                                {/* </View> */}

                            </View>
                        }} />
                </View >
            );
        }
    }

    async saveFood() {

        if (this.state.saveText === 'Next') {
            if (!this.state.foodName) {
                this.setState({ isAlert: true, alertMsg: 'Please enter a food name' });
            }
            else if (!this.state.serSize) {
                this.setState({ isAlert: true, alertMsg: 'Please enter portion size' });
            }
            else if (!this.state.serUnits) {
                this.setState({ isAlert: true, alertMsg: 'Please enter portion units' });
            }
            else if (!this.state.serPerContainer) {
                this.setState({ isAlert: true, alertMsg: 'Please enter the number of serving per container' });
            }
            else {
                this.setState({ saveText: 'Save' });
            }
        }
        else {

            if (validateArray(this.state.arrNutritionFacts)) {
                subCatIds = '';
                subcatVals = '';
                this.state.arrNutritionFacts.map((item) => {

                    if (item.value) {
                        if (subCatIds.length === 0) {
                            subCatIds = subCatIds + item.code;
                        }
                        else {
                            subCatIds = subCatIds + ',' + item.code;
                        }

                        if (subcatVals.length === 0) {
                            subcatVals = subcatVals + item.value;
                        }
                        else {
                            subcatVals = subcatVals + ',' + item.value;
                        }
                    }

                })
                //LogUtils.infoLog1('subCatIds', subCatIds);
                //LogUtils.infoLog1('subcatVals', subcatVals);

                this.createFooditem();
            }
            else {
                this.setState({ isAlert: true, alertMsg: 'Please enter a value in all required fields' });
            }

        }

    }

    async createFooditem() {
        this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');
        LogUtils.infoLog(JSON.stringify({
            mt_id: this.props.foodType,
            pname: this.state.foodName,
            bname: this.state.brandName,
            qnty: this.state.serSize,
            serving: this.state.serUnits,
            servingper: this.state.serPerContainer,
            foodtype: this.state.isVeg,
            attr_code: subCatIds,
            attr_val: subcatVals,
        }));
        fetch(
            `${BASE_URL}/trainee/saveusernewfood`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    mt_id: this.props.foodType,
                    pname: this.state.foodName,
                    bname: this.state.brandName,
                    qnty: this.state.serSize,
                    serving: this.state.serUnits,
                    servingper: this.state.serPerContainer,
                    foodtype: this.state.isVeg,
                    attr_code: subCatIds,
                    attr_val: subcatVals,
                }),

            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {

                    if (data) {
                        this.setState({ loading: false });
                        Actions.traHomeFoodNew({ mdate: moment(new Date()).format("YYYY-MM-DD") });
                    }
                    else {
                        this.setState({ loading: false, noDataMsg: 'No data available' });
                    }
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
            });
    }

    render() {
        return (
            <KeyboardAwareScrollView
                resetScrollToCoords={{ x: 0, y: 0 }}
                contentContainerStyle={styles.containerStyle}
                enableOnAndroid={false}
                scrollEnabled={false}>
                <ImageBackground source={require('../res/app_bg.png')} style={{ width: '100%', height: '100%' }}>
                    <Loader loading={this.state.loading} />
                    <NoInternet
                        image={require('../res/img_nointernet.png')}
                        loading={this.state.isNoIntenet}
                        onRetry={this.onRetry.bind(this)} />
                    <View style={{
                        flexDirection: 'row',
                        margin: 20,
                    }}>
                        <View style={{ position: 'absolute', zIndex: 111 }}>
                            <TouchableOpacity
                                style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                                onPress={() => this.onBackPressed()}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_back.png')}
                                    style={styles.backImageStyle}
                                />
                            </TouchableOpacity>
                        </View>

                        <Text style={styles.textIndicator}>Create Food</Text>
                    </View>

                    {this.renderAllData()}

                    <View style={styles.viewBottom}>
                        <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                            <TouchableOpacity
                                style={styles.buttonTuch}
                                onPress={() => {
                                    this.saveFood();
                                }}>
                                <Text style={styles.buttonText}>{this.state.saveText}</Text>
                            </TouchableOpacity>
                        </LinearGradient>
                    </View>



                    <CustomDialog
                        visible={this.state.isAlert}
                        title='Alert'
                        desc={this.state.alertMsg}
                        onAccept={this.onAccept.bind(this)}
                        no=''
                        yes='Ok' />

                </ImageBackground>
            </KeyboardAwareScrollView>
        )
    }

}

const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        height: '100%',
        flex: 1,
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'center',
    },
    textIndicator: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#2d3142',
        paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    mainContainerStyle: {
        flexDirection: 'column',
        marginLeft: wp('5%'),
        marginRight: wp('5%'),
    },
    textFoodHeaderBack: {
        borderRadius: 15,
        justifyContent: 'center',
        alignSelf: "flex-start",
        alignItems: 'center',
    },
    textFoodHeader: {
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontSize: 13,
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 15,
        paddingRight: 15,
        alignSelf: 'center',
        textAlign: 'center',
        fontWeight: '500',
        lineHeight: 18,
    },
    textTitle: {
        color: '#2d3142',
        fontFamily: 'Rubik-Medium',
        alignSelf: "flex-start",
        fontSize: 13,
        flex: 1,
        marginTop: 10,
        textAlign: 'left',
        fontWeight: '500',
    },
    spinnersBg: {
        width: wp('90%'),
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        flexDirection: 'column',
        borderRadius: 10,
        position: 'relative',
        alignContent: 'center',
        alignSelf: 'center',
        marginTop: 3,
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 6,
    },
    viewServingSize: {
        flexDirection: 'row',
    },
    spinnersBg1: {
        width: wp('44%'),
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        flexDirection: 'column',
        borderRadius: 10,
        position: 'relative',
        alignContent: 'center',
        alignSelf: 'center',
        marginTop: 3,
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 6,
    },
    textInputStyle: {
        height: 45,
        width: wp('90%'),
        flex: 1,
        alignSelf: 'flex-start',
        fontSize: 14,
        padding: 10,
        fontWeight: '400',
        color: '#2d3142',
        fontFamily: 'Rubik-Regular',
        letterSpacing: .5,
    },
    viewVeg: {
        width: wp('30%'),
        justifyContent: 'flex-start',
        flexDirection: 'row',
        alignContent: 'center',
        alignSelf: 'center',
        marginTop: 15,
    },
    textVegSelect: {
        color: '#2d3142',
        fontFamily: 'Rubik-Medium',
        alignSelf: "flex-start",
        fontSize: 12,
        marginLeft: 5,
        color: '#2d3142',
        textAlign: 'left',
        fontWeight: '500',
    },
    imgVegSelect: {
        width: 17,
        height: 17,
        alignSelf: 'center',
    },
    imgVegSelect1: {
        width: 17,
        height: 17,
        alignSelf: 'center',
        tintColor: '#6d819c',
    },
    textVegSelect1: {
        color: '#2d3142',
        fontFamily: 'Rubik-Regular',
        alignSelf: "flex-start",
        fontSize: 12,
        marginLeft: 5,
        color: '#6d819c',
        textAlign: 'left',
        fontWeight: '500',
    },
    containerMobileStyle: {
        width: wp('88%'),
        borderBottomWidth: 1,
        // backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: '#ddd',
        // borderRadius: 10,
        position: 'relative',
        alignSelf: 'center',
        // marginTop: 1,
        flex: 1,
        // shadowColor: '#4075cd',
        // shadowOffset: { width: 0, height: 2 },
        // shadowOpacity: 0.25,
        // shadowRadius: 3.84,
        // elevation: 5
    },
    textListName: {
        width: wp('65%'),
        color: '#2d3142',
        fontFamily: 'Rubik-Medium',
        alignSelf: "flex-start",
        fontSize: 14,
        color: '#2d3142',
        padding: 10,
        textAlign: 'left',
        fontWeight: '500',
    },
    textListInput: {
        height: 45,
        width: wp('25%'),
        alignSelf: 'flex-start',
        fontSize: 14,
        padding: 10,
        fontWeight: '400',
        color: '#2d3142',
        letterSpacing: .5,
        fontFamily: 'Rubik-Regular',
    },
    viewBottom: {
        width: wp('100%'),
        alignContent: 'center',
        alignSelf: 'center',
        position: 'absolute',
        bottom: 0,
        backgroundColor: '#fff',
    },
    linearGradient: {
        width: '90%',
        height: 50,
        // bottom: 0,
        borderRadius: 25,
        justifyContent: 'center',
        // position: 'absolute',
        alignSelf: 'center',
        marginBottom: 15,
        backgroundColor: '#ffffff',
    },
    buttonTuch:
    {
        backgroundColor: 'transparent',
    },
    buttonText: {
        fontSize: 16,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    textHint: {
        color: '#2d3142',
        fontFamily: 'Rubik-Regular',
        alignSelf: "flex-start",
        fontSize: 11,
        marginTop: 8,
        color: '#6d819c',
        textAlign: 'left',
        // letterSpacing: 0.1,
        fontWeight: '500',
    },

});

export default Trainee_CreateFood;

