import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    FlatList,
    Image,
    TouchableOpacity,
    BackHandler,
    ImageBackground,
    Text,
    SafeAreaView
} from 'react-native';
import { connect } from 'react-redux';
import { BASE_URL } from '../actions/types';
import { Loader, CustomDialog } from './common';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import NetInfo from "@react-native-community/netinfo";
import LinearGradient from 'react-native-linear-gradient';
import LogUtils from '../utils/LogUtils.js';

import DeviceInfo from 'react-native-device-info';
import { allowFunction } from '../utils/ScreenshotUtils.js';
function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

const formatData = (data, numColumns) => {
    const numberOfFullRows = Math.floor(data.length / numColumns);

    let numberOfElementsLastRow = data.length - (numberOfFullRows * numColumns);
    while (numberOfElementsLastRow !== numColumns && numberOfElementsLastRow !== 0) {
        data.push({ key: `blank-${numberOfElementsLastRow}`, empty: true });
        numberOfElementsLastRow++;
    }

    return data;
};

const numColumns = 2;

class TrainersList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchText: '',
            isAlert: false,
            loading: false,
            alertMsg: '',
            arrTrainers: [],
            noData: '',
            pageNo: 1,
            refreshing: false,
            loadmore: false,
            isData: '',
        };
        this.getAllTrainers = this.getAllTrainers.bind(this);
    }

    async componentDidMount() {
        allowFunction();
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.getAllTrainers();
            }
            else {
                this.setState({ isAlert: true, alertMsg: 'Please enable your internet connection and try again' });
            }
        });

        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    FlatListItemSeparator = () => {
        return (
            <View
                style={{
                    height: 5,
                    width: "100%",
                    backgroundColor: "transparent",
                }}
            />
        );
    }
    //handling onPress action
    getListViewItem = item => {
        // Alert.alert(item.hours);
    };

    onBackPressed() {
        Actions.pop();
    }

    async getAllTrainers() {
        this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/trainerlist`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    pagesize: 20,
                    page: this.state.pageNo,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('statusmessage', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false, isData: '0' });
                    if (data.data.length > 0) {
                        this.setState({
                            arrTrainers: [...this.state.arrTrainers, ...data.data],
                            loadMore: true,
                            isData: '1',
                            //adding the new data with old one available in Data Source of the List
                        });
                    } else {
                        this.setState({ loadmore: false })
                    }

                    if (this.state.programsArray.length === 0) {
                        this.setState({ noData: 'No data available at this moment' });
                    }
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ isAlert: true, alertMsg: data.message, refreshing: false });
                    } else {
                        this.setState({ isAlert: true, alertMsg: data.message, refreshing: false });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ isAlert: true, alertMsg: JSON.stringify(error), refreshing: false });
            });
    }

    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
        this.setState({ alertMsg: '' });
        this.setState({ isAlert: false });
    }

    async onSuccess() {
        this.setState({ isSuccess: false, sucMsg: '' });
    }

    handleLoadMore = () => {
        LogUtils.infoLog1('Load more', this.state.loadmore)
        if (!this.state.loading && this.state.loadmore) {
            this.setState({ loading: false, loadmore: false, pageNo: this.state.pageNo + 1 });// increase page by 1

            this.getAllTrainers();

        }
    };

    renderTrainersList() {
        if (this.state.isData) {
            if (Array.isArray(this.state.arrTrainers) && this.state.arrTrainers.length) {
                return (
                    <FlatList
                        contentContainerStyle={{
                            paddingBottom: hp('10%'),
                            alignItems: 'flex-start',
                            justifyContent: 'center',
                        }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.arrTrainers}
                        keyExtractor={item => item.tr_id}
                        style={styles.disFlatList}
                        numColumns={numColumns}
                        // initialNumToRender={5}
                        onEndReached={this.handleLoadMore.bind(this)}
                        onEndReachedThreshold={0.5}
                        renderItem={({ item }) => {
                            return <TouchableOpacity key={item.tr_id} style={styles.vBestTrainer} onPress={() => {

                                // Actions.traDiscover({ mFrom: 'trainer', title: item.name, wType: item.tr_id, ffId: 0 });
                                Actions.disHomePage({ title: item.name, ffId: 0, tr_id: item.tr_id});
                            }}>
                                <View style={{ flexDirection: 'column', }}>
                                    {item.image
                                        ?
                                        (
                                            <Image
                                                progressiveRenderingEnabled={true}
                                                resizeMethod="resize"
                                                source={{ uri: item.image }}
                                                // source={`${item.image}`}
                                                style={styles.imgTrainer}
                                            />
                                        )
                                        :
                                        (
                                            <Image
                                                progressiveRenderingEnabled={true}
                                                resizeMethod="resize"
                                                source={require('../res/ic_noimage.png')}
                                                style={styles.imgTrainer}
                                            />
                                        )}

                                    <View style={{ paddingTop: 3, paddingBottom: 3, alignItems: 'center', alignSelf: 'flex-start', justifyContent: 'center' }}>
                                        <Text numberOfLines={1} style={styles.textTrName}>{`${item.name}`}</Text>
                                        <Text numberOfLines={1} style={styles.textSpecial}>{`${item.special_in}`}</Text>
                                        <Text numberOfLines={1} style={styles.textSpecial}>{`${item.fitness_form}`}</Text>

                                    </View>


                                </View>

                            </TouchableOpacity>

                            //<TouchableOpacity style={styles.containerOurTrainers} onPress={() => {
                            //     Actions.traDiscover({ mFrom: 'trainer', title: item.name, wType: item.tr_id, ffId: 0 });
                            // }}>
                            //     <View>
                            //         {item.image
                            //             ? (
                            //                 <View>
                            //                     <Image
                            //                         progressiveRenderingEnabled={true}
                            //                         resizeMethod="resize"
                            //                         source={{ uri: item.image }}
                            //                         style={{
                            //                             height: 200,
                            //                             width: wp('46%'),
                            //                             borderRadius: 7,
                            //                             resizeMode: 'cover',
                            //                         }}
                            //                     />
                            //                     <View style={{
                            //                         width: wp('46%'),
                            //                         height: 200,
                            //                         borderRadius: 7,
                            //                         position: 'absolute',
                            //                         backgroundColor: '#0000008a',
                            //                     }}>

                            //                     </View>
                            //                 </View>

                            //             )
                            //             : (
                            //                 <View>
                            //                     <Image
                            //                         progressiveRenderingEnabled={true}
                            //                         resizeMethod="resize"
                            //                         source={require('../res/ic_noimage.png')}
                            //                         style={{
                            //                             width: wp('46%'),
                            //                             height: 200,
                            //                             borderRadius: 7,
                            //                             opacity: 0.9,
                            //                             resizeMode: 'cover',
                            //                         }}
                            //                     />
                            //                     <View style={{
                            //                         width: wp('46%'),
                            //                         height: 200,
                            //                         borderRadius: 7,
                            //                         position: 'absolute',
                            //                         backgroundColor: '#0000008a',
                            //                     }}>

                            //                     </View>
                            //                 </View>

                            //             )
                            //         }
                            //         <View style={{
                            //             flexDirection: 'column',
                            //             alignItems: 'center',
                            //             alignContent: 'flex-start',
                            //             margin: 10,
                            //             bottom: 0,
                            //             position: 'absolute',
                            //             justifyContent: 'flex-start',
                            //         }}>
                            //             <Text style={styles.textOurTrainer}>{item.name}</Text>
                            //             <LinearGradient colors={['#8b63e6', '#8c52ff']} style={{ borderRadius: 15, marginTop: 3, alignSelf: 'flex-start' }}>
                            //                 <Text style={styles.textTrainerPrograms}>{`${item.fitness_form}`}</Text>
                            //             </LinearGradient>
                            //             {/* <Text style={styles.textTrainerPrograms}>{item.fitness_form}</Text> */}
                            //         </View>
                            //     </View>

                            // </TouchableOpacity>

                        }} />

                );
            }
            else {
                return (

                    <View style={{ flexDirection: 'row', flex: 1, }}>
                        <Text style={styles.textNodata}>{this.state.noData}</Text>
                    </View>
                );
            }
        }
    }

    render() {
        return (
            // <SafeAreaView style={styles.mainContainer}>
            <ImageBackground source={require('../res/app_bg.png')} style={styles.mainContainer}>
                <View style={styles.mainContainer}>
                    <Loader loading={this.state.loading} />
                    <View style={{
                        flexDirection: 'row',
                        margin: 20,
                    }}>
                        <View style={{ position: 'absolute', zIndex: 111 }}>
                            <TouchableOpacity
                                style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                                onPress={() => this.onBackPressed()}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_back.png')}
                                    style={styles.backImageStyle}
                                />
                            </TouchableOpacity>
                        </View>

                        <Text style={styles.textHeadTitle}>Our Trainers</Text>
                    </View>


                    {this.renderTrainersList()}

                    <CustomDialog
                        visible={this.state.isAlert}
                        title='Alert'
                        desc={this.state.alertMsg}
                        onAccept={this.onAccept.bind(this)}
                        no=''
                        yes='Ok' />
                </View>

            </ImageBackground>
            // </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        width: wp('100%'),
        height: hp('100%'),
        backgroundColor: '#ffffff',
    },
    image: {
        width: 60,
        height: 60,
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'center',
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    textNodata: {
        width: wp('90%'),
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        padding: 10,
        marginTop: 15,
        marginBottom: 15,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
    },
    containerOurTrainers: {
        backgroundColor: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center',
        flex: .5,
        margin: 5,
        borderRadius: 7,
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    textOurTrainer: {
        fontSize: 14,
        fontWeight: '500',
        flexShrink: 2,
        textAlign: 'left',
        fontFamily: 'Rubik-Medium',
        color: '#ffffff',
        alignSelf: 'flex-start',
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    textTrainerPrograms: {
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontSize: 8,
        paddingTop: 3,
        paddingBottom: 3,
        paddingLeft: 10,
        paddingRight: 10,
        alignSelf: 'center',
        textAlign: 'center',
        fontWeight: '500',
    },
    disFlatList: {
        marginVertical: 5,
        marginHorizontal: 5,
        alignSelf: 'center',
    },
    vBestTrainer: {
        width: wp('45%'),
        // backgroundColor: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center',
        // flex: 1, // approximate a square
        margin: 5,
        borderRadius: 10,

        // width: wp('45%'),
        // backgroundColor: '#ffffff',
        // flexDirection: 'row',
        // marginTop: hp('0.1%'),
        // marginBottom: hp('1.1%'),
        // marginRight: wp('1%'),
        // marginLeft: wp('1.2%'),
        // borderColor: '#ddd',
        // borderRadius: 5,
        // position: 'relative',
        // alignSelf: 'center',
        // shadowColor: "#fff",
        // shadowOffset: {
        //     width: 0,
        //     height: 2,
        // },
        // shadowOpacity: 0.25,
        // shadowRadius: 3.84,
        // elevation: 5,
    },
    imgTrainer: {
        width: wp('45%'),
        // height: 130,
        // alignSelf: 'center',
        // position: 'relative',
        borderRadius: 5,
        height: undefined,
        resizeMode: 'contain',
        aspectRatio: 5 / 6,
    },
    textTrName: {
        /// height: 25,
        fontSize: 13,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        color: '#2d3142',
        textAlign: 'left',
        alignSelf: 'flex-start',
        // alignContent: 'center',
        // flex: 1,
    },
    textSpecial: {
        fontSize: 10,
        fontWeight: '500',
        color: '#2d3142',
        letterSpacing: 0.23,
        // marginTop: 2,
        textAlign: 'left',
        fontFamily: 'Rubik-Regular',
        alignSelf: 'flex-start',
    },
});

const mapStateToProps = state => {

    return {};
};

export default connect(
    mapStateToProps,
    {
    },
)(TrainersList);
