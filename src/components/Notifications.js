import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    BackHandler,
    FlatList,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { withNavigationFocus } from 'react-navigation';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import LogUtils from '../utils/LogUtils.js';
import Database from '../Database';
import { BASE_URL } from '../actions/types';
import { NoInternet, CustomDialog, Loader } from './common';
import NetInfo from "@react-native-community/netinfo";
import { allowFunction } from '../utils/ScreenshotUtils.js';

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

const db1 = new Database();


class Notifications extends Component {

    constructor(props) {
        super(props);
        this.state = {
            notifications: [],

            isAlert: false,
            loading: false,
            alertMsg: '',
            isInternet: false,
            isSuccess: false,
            sucMsg: '',
            titMsg: '',

        };

    }
    async componentDidMount() {
        allowFunction();
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isInternet: false, loading: true });
                this.getNotifications();
            }
            else {
                this.setState({ isInternet: true });
            }
        });

        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        Actions.pop();

    }

    async saveNotification(body) {
        let token = await AsyncStorage.getItem('token');
        fetch(`${BASE_URL}/trainee/notificationseen`, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: `Bearer ${token}`,
            },
            body: body,
        })
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('statusmessage', data);
                if (statusCode >= 200 && statusCode <= 300) {

                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        // this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
            });
    }


    async getNotifications() {
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/notifications`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },

            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('statusmessage', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    if (data.data.length > 0) {
                        this.setState({ loading: false, notifications: data.data });
                    }
                    else {
                        this.setState({ loading: false, notifications: [] });
                    }
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, });
                    } else {
                        this.setState({ loading: false, noData: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: SWR, });
            });
    }

    FlatListItemSeparator1 = () => {
        return (
            <View
                style={{
                    height: 1.6,
                    backgroundColor: "#F4F6FA",
                }}
            />
        );
    }

    renderBody(item) {
        if (item.body && item.body != null) {
            return (
                <View style={{ width: '96%', }}>
                    <Text style={styles.textTime}>{`${item.body}`}</Text>
                </View>

            );
        } else {
            return (
                <View>

                </View>
            );
        }
    }

    renderFlatList() {
        if (!this.state.loading) {

            if (Array.isArray(this.state.notifications) && this.state.notifications.length) {

                return (
                    <View>
                        <FlatList
                            contentContainerStyle={{ paddingBottom: hp('20%') }}
                            showsVerticalScrollIndicator={false}
                            data={this.state.notifications}
                            keyExtractor={item => item.id}
                            ItemSeparatorComponent={this.FlatListItemSeparator1}
                            renderItem={({ item }) => {
                                return <TouchableOpacity onPress={() => {
                                    // this.setState({ isPopVisible: true })
                                    let data = JSON.stringify({
                                        nsl_id: item.nsl_id,
                                        ns_id: item.ns_id,
                                        id: item.id,
                                        type: item.type,
                                    });
                                    LogUtils.infoLog(data)
                                    this.saveNotification(data);
                                    if (item.type === 'c') {
                                        if (item.id && item.id > 0) {
                                            Actions.traChallenges({ cid: item.id });
                                            // Actions.accChallenge({ cid: item.id, uc_id: 0 })
                                            // Actions.challengedetails({ cid: item.id });

                                        } else {
                                            //Challenge Home Page
                                            Actions.traChallenges()
                                        }
                                    } else if (item.type === 'p') {
                                        if (item.id && item.id > 0) {
                                            Actions.traineePlanNew({ pId: item.id });

                                        } else {
                                            //Challenge Home Page
                                            Actions.traineeWorkoutsHome();
                                        }
                                    } else if (item.type === 'f') {
                                        if (item.id && item.id > 0) {
                                            Actions.feeddetails({ feedId: item.id });

                                        } else {
                                            Actions.wall();
                                        }
                                    } else if (item.type === 'cvu') {
                                        if (item.id && item.id > 0) {
                                            // Actions.accChallenge({ cid: item.id, uc_id: item.uc_id }):
                                            Actions.traChallenges({ cid: item.id });


                                        } else {
                                            //Challenge Home Page
                                            Actions.traChallenges()
                                        }
                                    } else if (item.type === 'dr') {
                                        if (item.id && item.id > 0) {


                                        } else {
                                            //Diet Home Page
                                            Actions.dietHome1()
                                        }
                                    } else if (item.type === 'cwd') {
                                        if (item.id && item.id > 0) {
                                            Actions.traChallenges({ cid: item.id });
                                            // Actions.accChallenge({ cid: item.id, uc_id: 0, flag: 'Leaderboard' })


                                        } else {
                                            //Challenge Home Page
                                            Actions.traChallenges()
                                        }
                                    }
                                }}>

                                    <View style={styles.aroundListStyle} >
                                        <View >
                                            <Image
                                                progressiveRenderingEnabled={true}
                                                resizeMethod="resize"
                                                source={{ uri: item.image }}
                                                style={styles.profileImage}
                                            />

                                        </View>
                                        <View style={{ width: '85%', flexDirection: 'column', paddingLeft: 15, alignItems: 'flex-start', alignContent: 'center', justifyContent: 'center', }}>
                                            <View style={{ flexDirection: 'row', }}>
                                                <Text style={{
                                                    fontSize: 14,
                                                    fontWeight: '500',
                                                    lineHeight: 18,
                                                    // marginRight: 40,
                                                    fontFamily: 'Rubik-Regular',
                                                    color: '#282c37',
                                                }}>{`${item.title}`}</Text>
                                                {/* {item.seen === 0
                                                    ? (

                                                        <View style={styles.viewOraDot}>
                                                        </View>
                                                    )
                                                    : (
                                                        <View>
                                                        </View>
                                                    )
                                                } */}

                                            </View>
                                            <Text style={{
                                                fontSize: 9,
                                                fontWeight: '400',
                                                fontFamily: 'Rubik-Regular',
                                                color: '#9c9ab9',
                                                alignSelf: 'flex-start',
                                                textAlign: 'left',
                                            }}>{`${item.sent_format}`}</Text>

                                            {this.renderBody(item)}
                                        </View>
                                        {item.seen === 0
                                            ? (
                                                <View style={{ flex: 0.5, flexDirection: 'column', width: '30%', marginTop: 5, }}>
                                                    {/* <Text style={{
                                                        fontSize: 9,
                                                        fontWeight: '400',
                                                        fontFamily: 'Rubik-Regular',
                                                        color: '#9c9ab9',
                                                        alignSelf: 'center',
                                                        textAlign: 'center',
                                                    }}>{`${item.sent_format}`}</Text> */}
                                                    <View style={styles.viewOraDot}>
                                                    </View>
                                                </View>
                                            )
                                            : (
                                                <View>
                                                </View>
                                            )
                                        }


                                    </View>
                                </TouchableOpacity>
                            }} />
                    </View>
                );

            }
            else {
                return (
                    <View style={styles.viewTitleCal}>
                        <Image
                            source={require('../res/ic_notification_nodata.png')}
                            style={styles.imgUnSuscribe}
                        />
                        <Text style={styles.textCreditCard}>No Notifications Received</Text>

                        <Text style={styles.textDesc}>Once the notifications are received will be displyed here.</Text>

                    </View>
                );

            }
        }
    }

    async onAccept() {
        this.setState({ isAlert: false, alertMsg: '' });
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
    }

    async onRetry() {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isInternet: false, loading: true });
                this.getNotifications();
            }
            else {
                LogUtils.infoLog1("Is connected?", state.isConnected);
                this.setState({ isAlert: true, alertMsg: 'Please enable your internet connection and try again' });
            }
        });
    }

    render() {
        return (
            <View style={styles.containerStyle}>
                <Loader loading={this.state.loading} />
                {/* top */}
                <View style={styles.viewTop}>

                    <TouchableOpacity
                        onPress={() => this.onBackPressed()}>
                        <Image
                            source={require('../res/ic_back.png')}
                            style={styles.backImageStyle}
                        />
                    </TouchableOpacity>
                    <Text style={styles.notyTextStyle}>Notifications</Text>
                </View>
                {this.renderFlatList()}

                <NoInternet
                    image={require('../res/img_nointernet.png')}
                    loading={this.state.isInternet}
                    onRetry={this.onRetry.bind(this)} />

                <CustomDialog
                    visible={this.state.isAlert}
                    title='Alert'
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    no=''
                    yes='Ok' />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        height: '100%',
        flex: 1,
        backgroundColor: '#ffffff',
    },
    viewTop: {
        width: '100%',
        flexDirection: 'column',
        padding: 20,
        backgroundColor: '#F4F6FA',
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'flex-start',
    },
    notyTextStyle: {
        color: '#2d3142',
        fontFamily: 'Rubik-Medium',
        fontSize: 18,
        marginTop: 15,
        fontWeight: '500',
        letterSpacing: 0.8,
    },
    aroundListStyle: {
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        position: 'relative',
        padding: 15,
    },
    profileImage: {
        width: 45,
        height: 45,
        aspectRatio: 1,
        backgroundColor: "#D8D8D8",
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: "#979797",
        borderRadius: 45 / 2,
        resizeMode: "cover",
        alignSelf: 'flex-start',
    },
    textWorkName: {
        fontSize: 14,
        fontWeight: '500',
        lineHeight: 18,
        marginRight: 40,
        fontFamily: 'Rubik-Regular',
        color: '#282c37',
    },
    textTime: {
        width: '100%',
        fontSize: 11,
        fontWeight: '400',
        fontFamily: 'Rubik-Regular',
        marginTop: 5,
        color: '#9c9ab9',
    },
    viewOraDot: {
        width: 10,
        height: 10,
        backgroundColor: '#e1ddf5',
        position: 'absolute',
        alignSelf: 'center',
        borderRadius: 10,
        right: 3,

    },
    viewTitleCal: {
        height: '85%',
        flexDirection: 'column',
        padding: 30,
        justifyContent: 'center',
        alignContent: 'center',
    },
    textCreditCard: {
        width: wp('85%'),
        fontSize: 16,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
    },
    textDesc: {
        width: wp('85%'),
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
    },
    imgUnSuscribe: {
        width: 250,
        height: 250,
        alignSelf: 'center',
    },
});

const mapStateToProps = state => {
    //   const { goals, trainerCodes } = state.procre;
    const loading = state.procre.loading;
    const error = state.procre.error;
    return { loading, error, };

};

export default withNavigationFocus(
    connect(
        mapStateToProps,
        {},
    )(Notifications),
);

// export default ProfileCreation;