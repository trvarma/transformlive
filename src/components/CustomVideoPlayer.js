
import React, {
    Component
} from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    BackHandler,
} from 'react-native';
import Video from 'react-native-video';
const initial = Orientation.getInitialOrientation();
let orientationchange = 'PORTRAIT';
import Orientation from 'react-native-orientation';
import { Actions } from 'react-native-router-flux';
import { Loader } from './common';
import { PLAYER_STATES } from 'react-native-media-controls';
import LogUtils from '../utils/LogUtils.js';
import {forbidFunction} from '../utils/ScreenshotUtils.js'; 

let url = '';

class CustomVideoPlayer extends Component {
    constructor(props) {
        super(props);
        this.downloadAttempts = 3;
        this.state = {
            rate: 1,
            volume: 1,
            muted: false,
            resizeMode: 'contain',
            duration: 0.0,
            currentTime: 0.0,
            paused: false,
            loading: false,
            isLoading: true,
            playerState: PLAYER_STATES.PLAYING,
            fullscreen: false,
            play: false,
            duration: 0,
            showControls: true,
            isBuffering: true,
        };
    }

    async onBackPressed() {
        Orientation.lockToPortrait();
        //console.log('Orientation', orientationchange);
        if (orientationchange === 'PORTRAIT') {
            Actions.pop();
        } else {
            Orientation.lockToPortrait();
        }
    }

    async componentDidMount() {
        forbidFunction();
        Orientation.unlockAllOrientations();
        Orientation.addOrientationListener(this._orientationDidChange);
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillMount() {
        if (initial === 'PORTRAIT') {
            // do something
            //console.log("orientation", initial.toString())
        } else {
            // do something else
        }
    }

    _orientationDidChange = (orientation) => {
        initial === orientation;
        orientationchange === orientation;
        if (orientation === 'LANDSCAPE') {
            // do something with landscape layout
            Orientation.lockToLandscape();
        } else {
            // do something with portrait layout
            Orientation.lockToPortrait();
        }
    }

    _orientationRemove = (orientation) => {
        orientation.lockToPortrait();
    }

    componentWillUnmount() {
        this.backHandler.remove();
        Orientation.getOrientation((err, orientation) => {
            //console.log(`Current Device Orientation: ${orientation}`);
        });
        // Remember to remove listener
        Orientation.removeOrientationListener(this._orientationRemove);
    }

    onLoadStart = (data) => {
        this.setState({ loading: true })
        LogUtils.infoLog1("onLoadStart")
    };

    onLoad = (data) => {
        LogUtils.infoLog1("onLoad")
        this.setState({ duration: data.duration, loading: false });
    };

    // onProgress = (data) => {
    //     this.setState({ currentTime: data.currentTime });
    // };

    onProgress = (data) => {
        // Video Player will continue progress even if the video already ended
        if (this.state.loading) {
            this.setState({ currentTime: data.currentTime });
        }
    };

    onEnd = () => {
        this.video.seek(0)
        setTimeout(() => {
            this.setState({ paused: true })
            LogUtils.infoLog1("this.state.paused", this.state.paused);
            if (this.props.label) {
                LogUtils.firebaseEventLog('video', {
                    p_id: 1245,
                    p_category: 'Programs',
                    p_name: this.props.label,
                });
            }
        }, 100);

        // const interval = setInterval(() => {
        //     this.setState({ paused: true });
        //     // this.video.seek(0);
        //     // this.setState({ paused: true });

        //     if (this.props.label) {
        //         LogUtils.firebaseEventLog('video', {
        //             p_id: 1245,
        //             p_category: 'Programs',
        //             p_name: this.props.label,
        //         });
        //     }

        //     clearInterval(interval);
        // }, 1000);
    };

    onAudioBecomingNoisy = () => {
        this.setState({ paused: true })
    };

    getCurrentTimePercentage() {
        if (this.state.currentTime > 0) {
            return parseFloat(this.state.currentTime) / parseFloat(this.state.duration);
        }
        return 0;
    };

    renderRateControl(rate) {
        const isSelected = (this.state.rate === rate);
        return (
            <TouchableOpacity onPress={() => { this.setState({ rate }) }}>
                <Text style={[styles.controlOption, { fontWeight: isSelected ? 'bold' : 'normal' }]}>
                    {rate}x
            </Text>
            </TouchableOpacity>
        );
    }

    renderResizeModeControl(resizeMode) {
        const isSelected = (this.state.resizeMode === resizeMode);
        return (
            <TouchableOpacity onPress={() => { this.setState({ resizeMode }) }}>
                <Text style={[styles.controlOption, { fontWeight: isSelected ? 'bold' : 'normal' }]}>
                    {resizeMode}
                </Text>
            </TouchableOpacity>
        )
    }

    renderVolumeControl(volume) {
        const isSelected = (this.state.volume === volume);
        return (
            <TouchableOpacity onPress={() => { this.setState({ volume }) }}>
                <Text style={[styles.controlOption, { fontWeight: isSelected ? 'bold' : 'normal' }]}>
                    {volume * 100}%
            </Text>
            </TouchableOpacity>
        )
    }


    // handlePlayPause() {
    //     // If playing, pause and show controls immediately.
    //     if (this.state.play) {
    //         this.setState({ ...state, play: false, showControls: true });
    //         return;
    //     }

    //     this.setState({ ...state, play: true });
    //     setTimeout(() => this.setState(s => ({ ...s, showControls: false })), 2000);
    // }

    // skipBackward() {
    //     this.video.current.seek(this.state.currentTime - 15);
    //     this.setState({ ...state, currentTime: this.state.currentTime - 15 });
    // }

    // skipForward() {
    //     this.video.current.seek(this.state.currentTime + 15);
    //     this.setState({ ...state, currentTime: this.state.currentTime + 15 });
    // }

    onSeek(data) {
        this.video.current.seek(data.seekTime);
        this.setState({ ...state, currentTime: data.seekTime });
    }

    onBuffer = () => {
        this.setState({ isBuffering: true });
    }

    // videoError = () => {
    //     alert('video Error');
    //     console.log('video Error called');
    // }

    videoError = async () => {
        if (this.downloadAttempts > 0) {
            this.downloadAttempts--;
            //   try {
            //     await CacheService.remove(this.props.uri);
            //   } catch (error) {}
            //   await CacheService.download(this.props.uri);
        } else {
            this.setState({ error: 'Error :(' });
        }
    }

    render() {
        const flexCompleted = this.getCurrentTimePercentage() * 100;
        const flexRemaining = (1 - this.getCurrentTimePercentage()) * 100;
        if (this.props.item) {
            if (this.props.from === 'challengedetails') {
                url = this.props.item.video;
                LogUtils.infoLog1('Url', url);
            } else if (this.props.from === 'feed') {
                url = this.props.item.file_url;
            }
        } else if (this.props.videoURL) {
            url = this.props.videoURL;
            // if (this.props.from === 'feedpost') {
            //     url = this.props.videoURL;
            // }
        }

        return (
            <View style={styles.container}>
                <Loader loading={this.state.loading} />
                <Video
                    ref={(ref) => { this.video = ref }}
                    /* For ExoPlayer */
                    source={{ uri: url }}
                    // source={{ uri: "https://www.learningcontainer.com/wp-content/uploads/2020/05/sample-mp4-file.mp4" }}
                    style={styles.fullScreen}
                    rate={this.state.rate}
                    paused={this.state.paused}
                    volume={this.state.volume}
                    muted={this.state.muted}
                    resizeMode={this.state.resizeMode}
                    onLoadStart={this.onLoadStart}
                    onLoad={this.onLoad}
                    onProgress={this.onProgress}
                    onEnd={this.onEnd}
                    onAudioBecomingNoisy={this.onAudioBecomingNoisy}
                    onBuffer={this.onBuffer}
                    isLoading={this.state.isLoading}
                    // onAudioFocusChanged={this.onAudioFocusChanged}
                    repeat={false}
                    controls={true}
                // onError={this.videoError}   
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black',
    },
    fullScreen: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
    controls: {
        backgroundColor: 'transparent',
        borderRadius: 5,
        position: 'absolute',
        bottom: 20,
        left: 20,
        right: 20,
    },
    progress: {
        flex: 1,
        flexDirection: 'row',
        borderRadius: 3,
        overflow: 'hidden',
    },
    rateControl: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    volumeControl: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    resizeModeControl: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    controlOption: {
        alignSelf: 'center',
        fontSize: 11,
        color: 'white',
        paddingLeft: 2,
        paddingRight: 2,
        lineHeight: 12,
    },
});

export default CustomVideoPlayer;