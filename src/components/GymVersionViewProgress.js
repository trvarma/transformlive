import React, { Component } from 'react';
import {
    StyleSheet,
    SafeAreaView,
    Image,
    View,
    Text,
    TouchableOpacity,
    ScrollView,
    BackHandler,
    ImageBackground,
    TextInput, 
} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';

class GymVersionViewProgress extends Component {
    constructor(props) {
        super(props);
        this.state = {
            viewReport: this.props.viewReport,
            title: this.props.title,
            name: this.props.name,
            day_image:this.props.url,
            loading:false,
            isBackPressed: false,
            
        }
    }

    componentDidMount() {
        try {
            this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
                this.onBackPressed();
                return true;
            });
        } catch (error) {

        }
    }

    async onBackPressed() {
        //Orientation.lockToPortrait();
        this.setState({ isBackPressed: true, });
        Actions.pop();
        // Actions.popTo('traineePlanNew');
    }

    showSetsAndReps(item) {
        try {
            var setAndReps = [];

            item.map((ele1, index) => {

                setAndReps.push(
                    <View style={[styles.containerGoalPlan, { height: undefined }]}>
                        <View
                            style={{ backgroundColor: '#8c52ff', height: 30, justifyContent: 'center', borderTopLeftRadius: 5, borderTopRightRadius: 5 }}
                        >
                            <Text style={{ fontFamily: 'Rubik-Regular', marginLeft: 10, fontSize: 15, color: '#ffffff' }}>{ele1.title.toUpperCase()}</Text>
                        </View>
                        {
                            ele1.pdse_arr.map((ele2, index) => {
                                return (
                                    <View style={{ flexDirection: 'column', marginTop: 5 }}>
                                        {
                                            ele2.title != "" ?

                                                <View style={{ justifyContent: 'center', marginLeft: 30, height: 28 }}>
                                                    <Text style={{ fontFamily: 'Rubik-Regular', fontSize: 14 }}>{ele2.title.toUpperCase()} : </Text>
                                                </View> : <View />
                                        }
                                        <View style={{ flexDirection: 'column' }}>
                                            {
                                                ele2.pdses_arr.map((ele3, row) => {
                                                    return (
                                                        <View style={{
                                                            flexDirection: 'row',
                                                            justifyContent: 'space-around',
                                                            //borderBottomColor:'red',
                                                            // borderBottomWidth:1,
                                                            paddingVertical: 4,
                                                            backgroundColor: (row % 2) == 0 ? '#f0ffff' : '#f8f8ff',
                                                            //backgroundColor: '#f0ffff' 
                                                        }}>
                                                            <View style={{flex:0.3,justifyContent:'center'}}>
                                                                <Text style={{ fontFamily: 'Rubik-Regular', fontSize: 13,marginLeft:12 }}>{`Set ${ele3.set}`}</Text>
                                                            </View>
                                                           
                                                            {
                                                                ele3.type == "w" ? (
                                                                    <View style={{flex:0.3,justifyContent:'center'}}>
                                                                       <Text style={{ fontFamily: 'Rubik-Regular', fontSize: 13 }}>{`Weight : ${ele3.set_val} Kg`}</Text>
                                                                    </View>
                                                                ) : (
                                                                    <View style={{flex:0.3,justifyContent:'center'}}>
                                                                       <Text style={{ fontFamily: 'Rubik-Regular', fontSize: 13 }}>{`Time : ${ele3.set_val} Sec`}</Text>
                                                                    </View>
                                                                )
                                                            }
                                                             {
                                                                ele3.type == "w" ? (
                                                                    <View style={{flex:0.3,justifyContent:'center'}}>
                                                                       <Text style={{ fontFamily: 'Rubik-Regular', fontSize: 13 }}>{`Reps : ${ele3.reps}`}</Text>
                                                                    </View>
                                                                ) : (
                                                                    <View style={{flex:0.3,justifyContent:'center'}}>
                                                                       <Text style={{ fontFamily: 'Rubik-Regular', fontSize: 13 }}>{`Minutes : ${ele3.reps}`}</Text>
                                                                    </View>
                                                                )
                                                            }

                                                        </View>
                                                    )
                                                })
                                            }
                                        </View>
                                    </View>
                                )
                            })
                        }
                    </View>
                )
            })

            return (
                <View style={{ flex: 1 }}>
                    {setAndReps}
                </View>
            )

        } catch (error) {
            console.log(error);
        }
    }

    render() {
        return (
            <SafeAreaView>
                {/* show sets and reps */}
                <View style={styles.containerStyle}>

                    <View style={{
                        flexDirection: 'row', margin: 20,
                    }}>
                        <View style={{ position: 'absolute' }}>
                            <TouchableOpacity
                                style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                                onPress={() => this.onBackPressed()}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_back.png')}
                                    style={styles.backImageStyle}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                    {/* <Text style={styles.textIndicator}>{this.state.name}</Text> */}
                    <Text style={styles.textIndicator}>{this.state.title.toUpperCase()}</Text>

                    <View style={styles.mediaPlayer}>

                        {this.state.day_image
                            ? (
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={{ uri: this.state.day_image }}
                                    style={styles.thumbNail}
                                />
                            )
                            : (
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_noimage.png')}
                                    style={styles.thumbNail}
                                />
                            )
                        }

                    </View>

                    <View style={[styles.viewTitleCal, { height: hp("90%"), marginTop: 5 }]}>
                        <View style={{}}>
                            <ScrollView
                                //key={index + "gymVersion"}
                                ref={ref => this.scrollView = ref}
                                showsVerticalScrollIndicator={false}
                                contentContainerStyle={{ paddingBottom: hp('1%') }}
                                keyboardShouldPersistTaps='handled'
                                keyboardDismissMode={'on-drag'}
                            >
                                {this.showSetsAndReps(this.state.viewReport)}
                            </ScrollView>
                        </View>
                    </View>

                </View>

            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        height: '100%',
        flex: 1,
    },
    backImageStyle: {
        width: 19,
        height: 16,
        //marginTop: 10,
        //alignSelf: 'center',
    },
    textIndicator: {
        fontSize: 15,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        //flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#2d3142',
        paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    viewTitleCal: {
        //flex:1,
        //flexDirection: 'column',
        // marginLeft: 20,
        // marginTop: 10,
        // marginRight: 20,
    },
    containerGoalPlan: {
        width: wp('90%'),
        //height: hp('36%'),
        marginTop: 15,
        backgroundColor: '#ffffff',
        borderColor: '#ddd',
        borderRadius: 5,
        marginLeft: 20,
        marginRight: 15,
        position: 'relative',
        // alignItems: 'center',
        // alignSelf: 'center',
        //justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    mediaPlayer: {
        width: wp('100%'),
        height: hp('30%'),
        position: 'relative',
        justifyContent: 'center',
        backgroundColor: 'black',
        marginTop:10
      },
      thumbNail: {
        width: wp('100%'),
        height: hp('30%'),
        backgroundColor: 'black',
      },
    

});

export default GymVersionViewProgress;