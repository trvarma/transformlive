import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    FlatList,
    Image,
    TouchableOpacity,
    BackHandler,
    Text,
} from 'react-native';
import { connect } from 'react-redux';
import { CustomDialog, Loader } from './common';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import NetInfo from "@react-native-community/netinfo";
import { BASE_URL } from '../actions/types';
import DeviceInfo from 'react-native-device-info';
import LogUtils from '../utils/LogUtils.js';
import { allowFunction } from '../utils/ScreenshotUtils.js';



function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

const formatData = (data, numColumns) => {
    const numberOfFullRows = Math.floor(data.length / numColumns);
    let numberOfElementsLastRow = data.length - (numberOfFullRows * numColumns);
    while (numberOfElementsLastRow !== numColumns && numberOfElementsLastRow !== 0) {
        data.push({ key: `blank-${numberOfElementsLastRow}`, empty: true });
        numberOfElementsLastRow++;
    }
    return data;
};
const numColumns = 2;

class TraineefitnessformsList extends Component {
    constructor(props) {
        super(props);
        LogUtils.showSlowLog(this);
        // this.trace = null;
        this.state = {
            searchText: '',
            isAlert: false,
            loading: false,
            alertMsg: '',
            resObj: '',
            arrFitFormList: [],
            isPopupVisible: false,
            isData: false,
        };
    }

    async componentDidMount() {
        allowFunction();
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.getAllFitnessForms();
            }
            else {
                this.setState({ isAlert: true, alertMsg: 'Please enable your internet connection and try again' });
            }
        });

        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    async componentWillUnmount() {
        this.backHandler.remove();
    }

    async getAllFitnessForms() {
        // let token = await AsyncStorage.getItem('token');
        this.setState({ loading: true });
        fetch(
            `${BASE_URL}/master/fitnessform`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    // Authorization: `Bearer ${token}`,
                },
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false, isData: true, arrFitFormList: data.data });
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
            });
    }

    onBackPressed() {
        Actions.pop();
    }

    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
        this.setState({ isAlert: false, alertMsg: '' });
    }

    renderFitnessForms() {
        if (this.state.isData) {
            if (this.state.arrFitFormList) {
                return (
                    <FlatList
                        contentContainerStyle={{
                            paddingBottom: hp('10%'),
                            alignItems: 'flex-start',
                            justifyContent: 'center',
                        }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.arrFitFormList}
                        // data={formatData(this.state.arrFitFormList, numColumns)}
                        style={styles.disFlatList}
                        numColumns={numColumns}
                        keyExtractor={item => item.ff_id.toString()}
                        renderItem={({ item }) => {
                            return <TouchableOpacity key={item.ff_id} style={styles.item} onPress={() => {
                                Actions.traDiscover({ mFrom: 'discover', title: item.name, wType: 0, ffId: item.ff_id });
                            }}><View>
                                    {item.img
                                        ? (
                                            <Image
                                                progressiveRenderingEnabled={true}
                                                resizeMethod="resize"
                                                source={{ uri: item.img }}
                                                style={{
                                                    height: 200,
                                                    width: wp('45%'),
                                                    borderRadius: 7,
                                                    resizeMode: 'cover',
                                                }}
                                            />
                                        )
                                        : (
                                            <Image
                                                progressiveRenderingEnabled={true}
                                                resizeMethod="resize"
                                                source={require('../res/ic_noimage.png')}
                                                style={{
                                                    height: 200,
                                                    width: wp('45%'),
                                                    borderRadius: 7,
                                                    opacity: 0.9,
                                                    resizeMode: 'cover',
                                                }}
                                            />
                                        )
                                    }
                                    <View style={{
                                        flexDirection: 'column',
                                        alignItems: 'center',
                                        width: 150,
                                        alignContent: 'flex-start',
                                        marginBottom: 20,
                                        bottom: 0,
                                        position: 'absolute',
                                        justifyContent: 'flex-start',
                                    }}>
                                        <LinearGradient colors={['#8c52ff', '#8c52ff']}
                                            style={{ borderBottomRightRadius: 5, borderTopRightRadius: 5, alignSelf: 'flex-start' }}>
                                            <Text style={styles.textForm}>{`${item.name}`}</Text>
                                        </LinearGradient>
                                    </View>
                                </View>

                            </TouchableOpacity>
                        }} />
                );
            }
        }
    }

    render() {
        return (
            <View style={styles.mainContainer}>
                <Loader loading={this.state.loading} />

                <View style={{
                    flexDirection: 'row',
                    margin: 20,
                }}>
                    <View style={{ position: 'absolute', zIndex: 111 }}>
                        <TouchableOpacity
                            style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                            onPress={() => this.onBackPressed()}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_back.png')}
                                style={styles.backImageStyle}
                            />
                        </TouchableOpacity>
                    </View>

                    <Text style={styles.textHeadTitle}>Choose Your Form</Text>
                </View>

                {this.renderFitnessForms()}

                <CustomDialog
                    visible={this.state.isAlert}
                    title='Alert'
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    no=''
                    yes='Ok' />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: '#ffffff',
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'center',
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    disFlatList: {
        marginVertical: 5,
        alignSelf: 'center',
    },
    item: {
        width: wp('45%'),
        // backgroundColor: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center',
        // flex: 1, // approximate a square
        margin: 5,
        borderRadius: 10,
        // shadowColor: '#4075cd',
        // shadowOffset: { width: 0, height: 2 },
        // shadowOpacity: 0.25,
        // shadowRadius: 3.84,
        // elevation: 5
    },
    textForm: {
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontSize: 10,
        paddingTop: 3,
        paddingBottom: 3,
        paddingLeft: 10,
        paddingRight: 10,
        alignSelf: 'center',
        textAlign: 'center',
        fontWeight: '500',
    },
});

const mapStateToProps = state => {
    return {};
};

export default connect(
    mapStateToProps,
    {
    },
)(TraineefitnessformsList);
