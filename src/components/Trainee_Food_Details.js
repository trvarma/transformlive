import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    BackHandler,
    ScrollView,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { getTraineeGoals } from '../actions';
import { CustomDialog, Loader } from './common';
import { withNavigationFocus } from 'react-navigation';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import NetInfo from "@react-native-community/netinfo";
import { PieChart } from 'react-native-svg-charts';
import { allowFunction } from '../utils/ScreenshotUtils.js';

function getCal(val, defVal) {

    var v1 = parseFloat(val);

    var value = parseInt((v1 * defVal).toFixed());

    if (value < 0) {
        value = 0;
    }

    return value;
}

function getPercentage(val, defVal, cal) {

    var v1 = parseFloat(val);

    var value = parseFloat(v1 * defVal) / cal;

    var value1 = parseFloat(value * 100).toFixed();

    return value1;
}

function getOtherCal(val1, val2, val3, cal) {

    var value1 = parseInt((val1 * 9).toFixed());
    var value2 = parseInt((val2 * 4).toFixed());
    var value3 = parseInt((val3 * 4).toFixed());

    var value = cal - (value1 + value2 + value3);

    if (value < 0) {
        value = 0;
    }

    return value.toFixed();
}

function getOtherPercentage(val1, val2, val3, cal) {


    var value1 = parseFloat(val1 * 9) / cal;
    var per1 = parseInt(value1 * 100).toFixed();
    var value2 = parseFloat(val2 * 4) / cal;
    var per2 = parseInt(value2 * 100).toFixed();
    var value3 = parseFloat(val3 * 4) / cal;
    var per3 = parseInt(value3 * 100).toFixed();

    var newVal = 100 - (parseInt(per1) + parseInt(per2) + parseInt(per3));

    return newVal;
}

function getOtherMG(val1, val2) {
     try{
         var sodium = parseInt(val2.sodium) / 1000;
         var potassium = parseInt(val2.potassium) / 1000;
         var calcium = parseInt(val2.calcium) / 1000;
         var iron = parseInt(val2.iron) / 1000;
         var vitamin_a = parseInt(val2.vitamin_a) / 1000;
         var vitamin_c = parseInt(val2.vitamin_c) / 1000;
     
         var value = parseFloat(sodium + potassium + calcium + iron + vitamin_a + vitamin_c).toFixed(1);
     
         if (value > 0) {
             return value;
         }
         else {
             return 0;
         }
     }catch(error){
         console.log(error)
     }
}

class Trainee_Food_Details extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            isAlert: false,
            alertMsg: '',
            resObj: '',
            isSuccess: false,
            sucMsg: '',
            selWeight: 0,
            selReps: 0,
            selRepsText: '',
            selWeightText: '',
            measures: [],
            qty: '',
            selMeg: '',

        };
    }
    async componentDidMount() {
        allowFunction();
        NetInfo.fetch().then(state => {
            if (state.isConnected) {

            }
            else {
                // this.setState({ isAlert: true, alertMsg: 'Please enable your internet connection and try again' });
            }
        });
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        Actions.pop();
    }

    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
        this.setState({ alertMsg: '' });
        this.setState({ isAlert: false });
    }

    async onSuccess() {
        // const interval = setInterval(() => {
        this.setState({ isSuccess: false, sucMsg: '' });
        // Actions.thome();
        Actions.popTo('traineeHome');
        // this.setState({ loading: true });
        // clearInterval(interval);
        // }, 100);
    }

    nutritionDetails(data) {
        try {
            //data= '0.00';
            var value = "";
            var value1 = "";
            if (data != undefined) {
                data = parseFloat(data).toFixed(2);
                if (data.includes('.')) {
                    var val = data.split('.');
                    value1 = val[1] == (0 || '00') ? '' : val[1] > 9 ? "." + val[1].replace('0', '') : "." + val[1];
                    value = val[0] + value1;
                    return value;
                } else {
                    return data;
                }
            }
        } catch (error) {
            console.log(error);
        }
    }

    render() {
        return (
            <View style={styles.containerStyle}>
                <Loader loading={this.props.loading} />
                <ScrollView
                    contentContainerStyle={{ paddingBottom: hp('15%') }}>
                    <View style={styles.mainView}>

                        {/* title and cal details */}
                        <View style={styles.viewTitleCal}>
                            <TouchableOpacity
                                onPress={() => this.onBackPressed()}>
                                <Image
                                    source={require('../res/ic_back.png')}
                                    style={styles.backImageStyle}
                                />
                            </TouchableOpacity>
                            <View style={{ marginTop: 20, }} />

                            {/* title */}
                            <Text style={styles.textStyle}>{this.props.title}</Text>

                            {/* circular menus */}
                            <View style={{ flexDirection: 'row', marginTop: 30, }}>
                                <View style={{ flexDirection: 'row', height: 180, width: wp('60%'), justifyContent: 'center', }}>
                                    <PieChart
                                        style={{ height: 170, width: wp('60%'), }}
                                        valueAccessor={({ item }) => item.amount}
                                        data={[
                                            {
                                                key: 1,
                                                amount: getCal(this.props.selChartObject.fat, 9),
                                                svg: { fill: '#5ac8fb' },
                                            },
                                            {
                                                key: 2,
                                                amount: getCal(this.props.selChartObject.carbohydrate, 4),
                                                svg: { fill: '#8c52ff' }
                                            },
                                            {
                                                key: 3,
                                                amount: getCal(this.props.selChartObject.protein, 4),
                                                svg: { fill: '#ff5e3a' }
                                            },
                                            {
                                                key: 4,
                                                amount: getOtherCal(this.props.selChartObject.fat, this.props.selChartObject.carbohydrate, this.props.selChartObject.protein, this.props.selChartObject.calories),
                                                svg: { fill: '#44db5e' }
                                            }
                                            // ['#5ac8fb', '#5856d6', '#ff5e3a', '#44db5e'],
                                        ]}
                                        spacing={0}
                                        outerRadius={'95%'}
                                        innerRadius={'78%'}
                                        animate={true}
                                        animationDuration={5000}
                                    >
                                    </PieChart>
                                    <View style={{ position: 'absolute', alignItems: 'center', alignSelf: 'center', justifyContent: 'center' }}>
                                        <Text style={styles.textCaloriesBold}>{this.props.selChartObject.calories}</Text>
                                        <Text style={styles.textCalories}>Calories</Text>
                                    </View>
                                </View>
                                <View style={{ height: 180, flexDirection: 'column', alignSelf: 'center', alignContent: 'center', alignItems: 'center', justifyContent: 'center' }}>
                                    <View style={styles.nutrition1}>
                                        <View style={styles.viewChartFatDot}></View>
                                        <View style={{ flexDirection: 'column', marginLeft: 5, }}>
                                            <Text style={styles.textChartTit}>Fat</Text>
                                            <Text style={styles.textChartDesc}>{getPercentage(this.props.selChartObject.fat, 9, this.props.selChartObject.calories)}% ~ {getCal(this.props.selChartObject.fat, 9)} cal</Text>
                                        </View>
                                    </View>
                                    <View style={{ marginTop: 5, }}></View>
                                    <View style={styles.nutrition1}>
                                        <View style={styles.viewChartCarbDot}></View>
                                        <View style={{ flexDirection: 'column', marginLeft: 5, }}>
                                            <Text style={styles.textChartTit}>Carbs</Text>
                                            <Text style={styles.textChartDesc}>{getPercentage(this.props.selChartObject.carbohydrate, 4, this.props.selChartObject.calories)}% ~ {getCal(this.props.selChartObject.carbohydrate, 4)} cal</Text>
                                        </View>
                                    </View>
                                    <View style={{ marginTop: 5, }}></View>
                                    <View style={styles.nutrition1}>
                                        <View style={styles.viewChartProteinDot}></View>
                                        <View style={{ flexDirection: 'column', marginLeft: 5, }}>
                                            <Text style={styles.textChartTit}>Protein</Text>
                                            <Text style={styles.textChartDesc}>{getPercentage(this.props.selChartObject.protein, 4, this.props.selChartObject.calories)}% ~ {getCal(this.props.selChartObject.protein, 4)} cal</Text>
                                        </View>
                                    </View>
                                    <View style={{ marginTop: 5, }}></View>
                                    <View style={styles.nutrition1}>
                                        <View style={styles.viewChartOtherDot}></View>
                                        <View style={{ flexDirection: 'column', marginLeft: 5, }}>
                                            <Text style={styles.textChartTit}>Others</Text>
                                            <Text style={styles.textChartDesc}>{getOtherPercentage(this.props.selChartObject.fat, this.props.selChartObject.carbohydrate, this.props.selChartObject.protein, this.props.selChartObject.calories)}% ~ {getOtherCal(this.props.selChartObject.fat, this.props.selChartObject.carbohydrate, this.props.selChartObject.protein, this.props.selChartObject.calories)} cal</Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <Image
                            source={require('../res/ic_tab_bg.png')}
                            style={{ width: wp('100%'), height: 63, }}
                        />

                        <View style={{ marginTop: 15, }}></View>

                        {/* nutrician detail */}
                        <View style={styles.breakfastBg}>
                            <View style={{ flexDirection: 'row', padding: 15, }}>
                                <Text style={styles.textBreakfast}>{'Nutrition detail'.toUpperCase()}</Text>

                            </View>
                            <View style={{ height: 1, backgroundColor: '#e9e9e9', }}></View>
                            {/* fat detail */}
                            <View style={styles.nutrition}>
                                <View style={styles.nutrition1}>
                                    <View style={styles.viewFatDot}></View>
                                    <View style={styles.nutritionInnerView}>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Text style={styles.textNutTitle}>Fat</Text>
                                            <Text style={styles.textNutTitle1}>{this.props.selChartObject.fat} g</Text>
                                        </View>

                                        {this.props.selChartObject.saturated_fat > 0
                                            ? (
                                                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, }}>
                                                    <Text style={styles.textNutDesc}>Saturated Fat</Text>
                                                    <Text style={styles.textPro}>{this.props.selChartObject.saturated_fat} g</Text>
                                                </View>
                                            )
                                            : (
                                                <View />
                                            )}

                                        {this.props.selChartObject.polyunsaturated_fat > 0
                                            ? (
                                                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, }}>
                                                    <Text style={styles.textNutDesc}>Polyunsaturated Fat</Text>
                                                    <Text style={styles.textPro}>{this.props.selChartObject.polyunsaturated_fat} g</Text>
                                                </View>
                                            )
                                            : (
                                                <View />
                                            )}

                                        {this.props.selChartObject.monounsaturated_fat > 0
                                            ? (
                                                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, }}>
                                                    <Text style={styles.textNutDesc}>Monounsaturated Fat</Text>
                                                    <Text style={styles.textPro}>{this.props.selChartObject.monounsaturated_fat} g</Text>
                                                </View>
                                            )
                                            : (
                                                <View />
                                            )}

                                        {this.props.selChartObject.cholesterol > 0
                                            ? (
                                                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, }}>
                                                    <Text style={styles.textNutDesc}>Cholesterol</Text>
                                                    <Text style={styles.textPro}>{this.props.selChartObject.cholesterol} mg</Text>
                                                </View>
                                            )
                                            : (
                                                <View />
                                            )}

                                    </View>
                                </View>
                            </View>
                            {/* carbs detail */}
                            <View style={styles.nutrition}>
                                <View style={styles.nutrition1}>
                                    <View style={styles.viewCarbsDot}></View>
                                    <View style={styles.nutritionInnerView}>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Text style={styles.textNutTitle}>Carbs</Text>
                                            <Text style={styles.textNutTitle1}>{this.props.selChartObject.carbohydrate} g</Text>
                                        </View>
                                        {this.props.selChartObject.fiber > 0
                                            ? (
                                                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, }}>
                                                    <Text style={styles.textNutDesc}>Fiber</Text>
                                                    <Text style={styles.textPro}>{parseFloat(this.props.selChartObject.fiber).toFixed(2)} g</Text>
                                                </View>
                                            )
                                            : (
                                                <View />
                                            )}

                                        {this.props.selChartObject.sugar > 0
                                            ? (
                                                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, }}>
                                                    <Text style={styles.textNutDesc}>Sugars</Text>
                                                    <Text style={styles.textPro}>{parseFloat(this.props.selChartObject.sugar).toFixed(2)} g</Text>
                                                </View>
                                            )
                                            : (
                                                <View />
                                            )}

                                    </View>
                                </View>
                            </View>
                            {/* Protein detail */}
                            <View style={styles.nutrition}>
                                <View style={styles.nutrition1}>
                                    <View style={styles.viewProteinDot}></View>
                                    <View style={styles.nutritionInnerView}>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Text style={styles.textNutTitle}>Protein</Text>
                                            <Text style={styles.textNutTitle1}>{parseFloat(this.props.selChartObject.protein).toFixed(2)} g</Text>
                                        </View>
                                    </View>
                                </View>
                            </View>

                            <View style={styles.nutrition}>
                                <View style={styles.nutrition1}>
                                    <View style={styles.viewOthersDot}></View>
                                    <View style={styles.nutritionInnerView}>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Text style={styles.textNutTitle}>Others</Text>
                                            <Text style={styles.textNutTitle1}>{getOtherMG(0, this.props.selChartObject)} g</Text>
                                        </View>
                
                                        {this.props.selChartObject.sodium > 0
                                            ? (
                                                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, }}>
                                                    <Text style={styles.textNutDesc}>Sodium</Text>
                                                    <Text style={styles.textPro}> {parseFloat(this.props.selChartObject.sodium).toFixed(2)} mg</Text>
                                                </View>
                                            )
                                            : (
                                                <View />
                                            )}

                                        {this.props.selChartObject.potassium > 0
                                            ? (
                                                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, }}>
                                                    <Text style={styles.textNutDesc}>Potassium</Text>
                                                    <Text style={styles.textPro}> {parseFloat(this.props.selChartObject.potassium).toFixed(2)} mg</Text>
                                                </View>
                                            )
                                            : (
                                                <View />
                                            )}

                                        {this.props.selChartObject.calcium > 0
                                            ? (
                                                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, }}>
                                                    <Text style={styles.textNutDesc}>Calcium</Text>
                                                    <Text style={styles.textPro}> {parseFloat(this.props.selChartObject.calcium).toFixed(2)} mg</Text>
                                                </View>
                                            )
                                            : (
                                                <View />
                                            )}

                                        {this.props.selChartObject.iron > 0
                                            ? (
                                                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, }}>
                                                    <Text style={styles.textNutDesc}>Iron</Text>
                                                    <Text style={styles.textPro}> {parseFloat(this.props.selChartObject.iron).toFixed(2)} mg</Text>
                                                </View>
                                            )
                                            : (
                                                <View />
                                            )}

                                        {this.props.selChartObject.vitamin_a > 0
                                            ? (
                                                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, }}>
                                                    <Text style={styles.textNutDesc}>Vitamin A</Text>
                                                    <Text style={styles.textPro}> {parseFloat(this.props.selChartObject.vitamin_a).toFixed(2)} mg</Text>
                                                </View>
                                            )
                                            : (
                                                <View />
                                            )}

                                        {this.props.selChartObject.vitamin_c > 0
                                            ? (
                                                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, }}>
                                                    <Text style={styles.textNutDesc}>Vitamin C</Text>
                                                    <Text style={styles.textPro}> {parseFloat(this.props.selChartObject.vitamin_c).toFixed(2)} mg</Text>
                                                </View>
                                            )
                                            : (
                                                <View />
                                            )}

                                    </View>
                                </View>

                            </View>

                        </View>

                    </View>
                </ScrollView>

                <CustomDialog
                    visible={this.state.isAlert}
                    title='Alert'
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    no=''
                    yes='Ok' />

                <CustomDialog
                    visible={this.state.isSuccess}
                    title='Success '
                    desc={this.state.sucMsg}
                    onAccept={this.onSuccess.bind(this)}
                    no=''
                    yes='Ok' />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        height: '100%',
        backgroundColor: '#ffffff',
        flex: 1,
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'flex-start',
    },
    mainView: {
        flexDirection: 'column',
    },
    viewTitleCal: {
        flexDirection: 'column',
        marginLeft: 20,
        marginTop: 20,
        backgroundColor: '#ffffff',
        marginRight: 20,
    },
    textStyle: {
        fontSize: 16,
        marginLeft: 40,
        marginRight: 40,
        fontWeight: '500',
        color: '#2d3142',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    container: {
        width: wp('80%'),
        height: 200,
        borderColor: '#000000',
        borderRadius: 2,
        alignSelf: 'center',
        backgroundColor: '#000000',
        justifyContent: 'center'
    },
    breakfastBg: {
        width: wp('90%'),
        marginLeft: wp('5%'),
        marginRight: wp('5%'),
        marginTop: wp('1%'),
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'column',
        borderRadius: 6,
        position: 'relative',
        alignSelf: 'center',
        shadowColor: 'rgba(0, 0, 0, 0.08)',
        shadowOffset: { width: 5, height: 0 },
        shadowOpacity: 0.25,
        shadowRadius: 10,
        elevation: 6,
    },
    textBreakfast: {
        color: '#282c37',
        fontFamily: 'Rubik-Medium',
        fontSize: 12,
        flex: 1,
        alignSelf: "flex-start",
        fontWeight: '500',
    },
    nutrition: {
        flexDirection: 'column',
        padding: 15,
    },
    nutrition1: {
        flexDirection: 'row',
        alignSelf: 'flex-start',
    },
    nutritionInnerView: {
        flexDirection: 'column',
        marginLeft: wp('3%'),
        justifyContent: 'flex-start',
        alignSelf: 'flex-start',
        flex: 1,
    },
    textNutTitle: {
        fontSize: 14,
        fontWeight: '500',
        color: '#2d3142',
        flex: 1,
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.23,
    },
    textNutTitle1: {
        fontSize: 14,
        fontWeight: '500',
        color: '#2d3142',
        textAlign: 'right',
        fontFamily: 'Rubik-Medium',
        marginLeft: 8,
        letterSpacing: 0.23,
    },
    textNutDesc: {
        fontSize: 12,
        fontWeight: '400',
        color: '#282c37',
        lineHeight: 18,
        flex: 1,
        fontFamily: 'Rubik-Regular',
        letterSpacing: 0.2,
    },
    textPro: {

        fontSize: 10,
        fontWeight: '500',
        color: '#ffffff',
        borderRadius: 10,
        padding: 5,
        textAlign: 'right',
        fontFamily: 'Rubik-Medium',
        marginLeft: 8,
        letterSpacing: 0.23,
        backgroundColor: '#8c52ff',
        overflow: 'hidden',
    },
    viewFatDot: {
        width: 11,
        height: 11,
        backgroundColor: '#ffa66d',
        alignSelf: 'center',
        borderRadius: 11 / 2,
    },
    viewFatDot: {
        width: 15,
        height: 15,
        alignSelf: 'flex-start',
        backgroundColor: '#5ac8fb',
        borderRadius: 15 / 2,
    },
    viewCarbsDot: {
        width: 15,
        height: 15,
        alignSelf: 'flex-start',
        backgroundColor: '#8c52ff',
        borderRadius: 15 / 2,
    },
    viewProteinDot: {
        width: 15,
        height: 15,
        alignSelf: 'flex-start',
        backgroundColor: '#ff5e3a',
        borderRadius: 15 / 2,
    },
    viewOthersDot: {
        width: 15,
        height: 15,
        alignSelf: 'flex-start',
        backgroundColor: '#44db5e',
        borderRadius: 15 / 2,
    },
    textCaloriesBold: {
        alignSelf: 'center',
        fontSize: 25,
        fontWeight: '400',
        color: '#2d3142',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    textCalories: {
        alignSelf: 'center',
        fontSize: 13,
        fontWeight: '400',
        color: '#6d819c',
        textAlign: 'center',
        fontFamily: 'Rubik-Regular',
    },
    viewChartFatDot: {
        width: 12,
        height: 12,
        backgroundColor: '#5ac8fb',
        marginTop: 2,
        alignSelf: 'flex-start',
        borderRadius: 6,
    },
    viewChartCarbDot: {
        width: 12,
        height: 12,
        backgroundColor: '#8c52ff',
        marginTop: 2,
        alignSelf: 'flex-start',
        borderRadius: 6,
    },
    viewChartProteinDot: {
        width: 12,
        height: 12,
        backgroundColor: '#ff5e3a',
        marginTop: 2,
        alignSelf: 'flex-start',
        borderRadius: 6,
    },
    viewChartOtherDot: {
        width: 12,
        height: 12,
        backgroundColor: '#44db5e',
        marginTop: 2,
        alignSelf: 'flex-start',
        borderRadius: 6,
    },
    textChartTit: {
        fontSize: 12,
        fontWeight: '500',
        color: '#2d3142',
        fontFamily: 'Rubik-Medium',
        textAlign: 'left',
    },
    textChartDesc: {
        fontSize: 10,
        fontWeight: '400',
        color: '#6d819c',
        textAlign: 'left',
        fontFamily: 'Rubik-Regular',
    },

});

const mapStateToProps = state => {
    const { goals, trainerCodes } = state.procre;
    const loading = state.procre.loading;
    const error = state.procre.error;
    return { loading, error, goals, trainerCodes };
};

export default withNavigationFocus(
    connect(
        mapStateToProps,
        { getTraineeGoals },
    )(Trainee_Food_Details),
);
