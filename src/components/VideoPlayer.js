import React, { Component } from 'react';
import {
    AlertIOS, AppRegistry, Platform, StyleSheet, Text, TouchableOpacity, Button, Slider, PanResponder,
    StatusBar, Dimensions, BackHandler, View, Image, KeyboardAvoidingView, TouchableWithoutFeedback, ImageBackground, TextInput
} from 'react-native';
import Video from 'react-native-video';
// import Slider from '@react-native-community/slider';

function getMinutesFromSeconds(time) {
    const minutes = time >= 60 ? Math.floor(time / 60) : 0;
    const seconds = Math.floor(time - minutes * 60);

    return `${minutes >= 10 ? minutes : '0' + minutes}:${
        seconds >= 10 ? seconds : '0' + seconds
        }`;
}

import { CustomDialog, Loader } from './common';
import Orientation from 'react-native-orientation';
const initial = Orientation.getInitialOrientation();
let orientationchange = 'PORTRAIT';
import { BASE_URL, SWR } from '../actions/types';
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import LogUtils from '../utils/LogUtils.js';
import { Actions } from 'react-native-router-flux';
import { AirbnbRating } from 'react-native-ratings';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
// import VideoPlayer1 from 'react-native-video-controls';
let url = '';
import {forbidFunction} from '../utils/ScreenshotUtils.js'; 
function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

class VideoPlayer extends Component {

    constructor(props) {
        super(props);
        this.onLoad = this.onLoad.bind(this);
        this.onProgress = this.onProgress.bind(this);
        this.onBuffer = this.onBuffer.bind(this);
        this.state = {
            rate: 1,
            volume: 50,
            muted: false,
            resizeMode: 'contain',
            duration: 0.0,
            currentTime: 0.0,
            controls: false,
            paused: false,
            skin: 'custom',
            ignoreSilentSwitch: 'ignore',
            isBuffering: false,

            isBackPressed: false,
            isEndWorkout: false,
            isEndWithOutFeedback: false,
            post: '',
            rating: 3,
            isFeedbackSubmit: false,
            isBackPressed: false,
            fullscreen: false,
            loading: false,
            isAlert: false,
            alertMsg: '',
            resObj: '',
            isSuccess: false,
        };
    }

    componentDidMount() {
        forbidFunction();
        Orientation.unlockAllOrientations();
        // Orientation.lockToLandscape();

        Orientation.addOrientationListener(this._orientationDidChange);
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
        StatusBar.setHidden(true);


    }

    componentWillMount() {
        // The getOrientation method is async. It happens sometimes that
        // you need the orientation at the moment the JS runtime starts running on device.
        // `getInitialOrientation` returns directly because its a constant set at the
        // beginning of the JS runtime.


        if (initial === 'PORTRAIT') {
            // do something
            //console.log("orientation", initial.toString())
        } else {
            // do something else
        }

        this._panResponder = PanResponder.create({
            // Ask to be the responder:
            onStartShouldSetPanResponder: (evt, gestureState) => true,
            onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
            onMoveShouldSetPanResponder: (evt, gestureState) => true,
            onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,

            onPanResponderGrant: (evt, gestureState) => {
                // The guesture has started. Show visual feedback so the user knows
                // what is happening!

                // gestureState.{x,y}0 will be set to zero now

                let state = this.state;
                state.seeking = true;
                state.scrubbing = false;
                this.setState(state);
            },
            onPanResponderMove: (evt, gestureState) => {
                // The most recent move distance is gestureState.move{X,Y}

                // The accumulated gesture distance since becoming responder is
                // gestureState.d{x,y}
                let state = this.state;
                state.scrubbing = true;
                this.setState(state);
            },
            onPanResponderTerminationRequest: (evt, gestureState) => true,
            onPanResponderRelease: (evt, gestureState) => {
                // The user has released all touches while this view is the
                // responder. This typically means a gesture has succeeded
                let state = this.state;
                state.seeking = false;
                this.setState(state);
            },
            onPanResponderTerminate: (evt, gestureState) => {
                // Another component has become the responder, so this gesture
                // should be cancelled
            },
            onShouldBlockNativeResponder: (evt, gestureState) => {
                // Returns whether this component should block native components from becoming the JS
                // responder. Returns true by default. Is currently only supported on android.
                return true;
            },
        });
    }

    _orientationDidChange = (orientation) => {
        initial === orientation;
        orientationchange === orientation;
        ///console.log(`Changed Device Orientation: ${orientation}`);
        if (orientation === 'LANDSCAPE') {
            //console.log(`Changed Device Orientation: ${orientation}`);
            // do something with landscape layout
            this.setState({ fullscreen: true });
            // Orientation.lockToLandscape();
        } else {
            // do something with portrait layout
            //console.log(`Changed Device Orientation: ${orientation}`);
            // Orientation.lockToPortrait();
            this.setState({ fullscreen: false });
        }
    }
    _orientationRemove = (orientation) => {
        // orientation.lockToPortrait();
    }

    componentWillUnmount() {
        this.backHandler.remove();

        Orientation.getOrientation((err, orientation) => {
            //console.log(`Current Device Orientation: ${orientation}`);
        });


        // Remember to remove listener
        Orientation.removeOrientationListener(this._orientationRemove);
    }

    onLoad(data) {
        //console.log('On load fired!');
        this.setState({ duration: data.duration, loading: false, controls: true });
        setTimeout(() => this.setState(s => ({ controls: false })), 5000);
    }
    onProgress(data) {
        if (!this.state.scrubbing) {
            this.setState({ currentTime: data.currentTime });
        }

        // this.player.seek(this.state.currentTime)

        // console.log(this.state.currentTime);
    }
    onBuffer(isBuffering) {
        this.setState({ isBuffering: isBuffering.isBuffering });
        //console.log('isBuffering', isBuffering);
        // if (isBuffering.isBuffering) {

        //     this.setState({loading : true });
        // }else  {

        //     this.setState({ loading : false });
        // }

    }

    async onBackPressed() {
        if (this.props.item || this.props.videoURL){ 
            Orientation.lockToPortrait();
            //console.log('Orientation', orientationchange);
            if (orientationchange === 'PORTRAIT') {
                Actions.pop();
            } else {
                Orientation.lockToPortrait();
            }

        }
        else {
            if (!this.state.isEndWorkout) {
                Orientation.lockToPortrait();
                // Actions.pop();
                this.setState({ isBackPressed: true, paused: true });
            }
        }
    }

    getCurrentTimePercentage() {
        if (this.state.currentTime > 0) { return parseFloat(this.state.currentTime) / parseFloat(this.state.duration); } else { return 0; }
    }

    renderSkinControl(skin) {
        const isSelected = this.state.skin == skin;
        const selectControls = skin == 'native' || skin == 'embed';
        return (
            <TouchableOpacity onPress={() => {
                this.setState({
                    controls: selectControls,
                    skin: skin
                })
            }}>
                <Text style={[styles.controlOption, { fontWeight: isSelected ? "bold" : "normal" }]}>
                    {skin}
                </Text>
            </TouchableOpacity>
        );
    }

    renderRateControl(rate) {
        const isSelected = (this.state.rate == rate);
        return (
            <TouchableOpacity onPress={() => { this.setState({ rate: rate }) }}><Text style={[styles.controlOption, { fontWeight: isSelected ? "bold" : "normal" }]}>
                {rate}x
        </Text>
            </TouchableOpacity>
        )
    }
    renderResizeModeControl(resizeMode) {
        const isSelected = (this.state.resizeMode == resizeMode);
        return (
            <TouchableOpacity onPress={() => { this.setState({ resizeMode: resizeMode }) }}>
                <Text style={[styles.controlOption, { fontWeight: isSelected ? "bold" : "normal" }]}>
                    {resizeMode}
                </Text>
            </TouchableOpacity>
        )
    }

    renderVolumeControl(volume) {
        const isSelected = (this.state.volume == volume);
        return (
            <TouchableOpacity onPress={() => { this.setState({ volume: volume }) }}>
                <Text style={[styles.controlOption, { fontWeight: isSelected ? "bold" : "normal" }]}>
                    {volume * 100}%
        </Text>
            </TouchableOpacity>
        )
    }
    renderIgnoreSilentSwitchControl(ignoreSilentSwitch) {
        const isSelected = (this.state.ignoreSilentSwitch == ignoreSilentSwitch);
        return (
            <TouchableOpacity onPress={() => { this.setState({ ignoreSilentSwitch: ignoreSilentSwitch }) }}>
                <Text style={[styles.controlOption, { fontWeight: isSelected ? "bold" : "normal" }]}>
                    {ignoreSilentSwitch}
                </Text>
            </TouchableOpacity>
        )
    }

    renderPlayPause() {
        if (this.state.paused) {
            return (
                <View>
                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={require('../res/play.png')}
                        style={styles.playIcon} />
                </View>

            );
        } else {
            return (
                <View>
                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={require('../res/pause.png')}
                        style={styles.playIcon} />
                </View>

            )
        }
    }

    renderSkipBackward() {
        return (
            <View>
                <Image
                    progressiveRenderingEnabled={true}
                    resizeMethod="resize"
                    source={require('../res/ic_skip_back.png')}
                    style={styles.playIcon} />
            </View>

        );
    }

    renderSkipForward() {
        return (
            <View>
                <Image
                    progressiveRenderingEnabled={true}
                    resizeMethod="resize"
                    source={require('../res/ic_skip_forward.png')}
                    style={styles.playIcon} />
            </View>

        );
    }

    onEndWorkout() {
        this.setState({ isBackPressed: false, paused: true });
        this.saveEndWorkOut();
    }

    async saveEndWorkOut() {
        this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/endworkout`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    pd_id: this.props.pdObj.pd_id,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {

                    if (this.state.currentTime >= this.props.planObj.feedback_time) {

                        if (this.props.planObj.fb_done === 0)
                            this.setState({ loading: false, isEndWorkout: true });
                        else
                            this.setState({ loading: false, isEndWithOutFeedback: true, isAlert: true, alertMsg: data.message, });

                    } else {
                        this.setState({ loading: false, isEndWithOutFeedback: true, isAlert: true, alertMsg: data.message, });
                    }

                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, isEndWorkout: true });

                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, isEndWorkout: true });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: SWR, isEndWorkout: true });
            });
    }


    async saveFeebackWorkOut() {
        this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/savedayfdback`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    pd_id: this.props.pdObj.pd_id,
                    rating: this.state.rating,
                    comments: this.state.post,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false, isFeedbackSubmit: true })
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });

                    } else {
                        this.setState({ loading: false, isFeedBackError: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isFeedBackError: true, alertMsg: SWR });
            });
    }

    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }

        this.setState({ isAlert: false, alertMsg: '' });

        if (this.state.isEndWithOutFeedback) {
            this.setState({ isEndWithOutFeedback: false });

            if (this.props.from === 'home')
                // Actions.thome();
                Actions.popTo('traineeHome');
            else
                // Actions.traineeWorkoutsHome();
                Actions.traineePlanNew({ pId: this.props.p_id, from: 'endWork' });
        }
    }


    async onFeedbackSubmit() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
        this.setState({ isFeedbackSubmit: false, alertMsg: '' });

        if (this.props.from === 'home')
            Actions.popTo('traineeHome');
        else
            Actions.traineePlanNew({ pId: this.props.p_id, from: 'endWork' });
    }

    renderProfileImg() {
        if (this.props.planObj.tr_img) {
            return (
                <Image
                    source={{ uri: this.props.planObj.tr_img }}
                    style={styles.profileImage}
                />
            );
        }
        else {
            return (
                <Image
                    source={require('../res/ic_profile_gray.png')}
                    style={styles.profileImage}
                />
            );
        }
    }

    ratingCompleted(rating) {
        this.setState({ rating: rating })
        switch (rating) {
            case 1:
                this.setState({ ratingText: 'Bad experience !' })
                break;
            case 2:
                this.setState({ ratingText: 'Not that great !' })
                break;
            case 3:
                this.setState({ ratingText: 'It was good !' })
                break;
            case 4:
                this.setState({ ratingText: 'Had a great experience !' })
                break;
            case 5:
                this.setState({ ratingText: 'Absolutely superb !' })
                break;
            default:
                this.setState({ ratingText: 'Give rating !' })
                break
        }
    }

    onEnd = () => {

        if (this.props.item || this.props.videoURL){ 

            this.player.seek(0);
            this.setState({ isBackPressed: false, paused: true, controls: true });
            
        } else {

            this.saveEndWorkOut();
            // if (this.props.planObj.fb_done === 0)
            //     this.setState({ loading: false, isEndWorkout: true });
            // else
            //     this.setState({ loading: false, isEndWithOutFeedback: true, isAlert: true, alertMsg: 'Workout Completed Successfully', });
            if (this.props.label) {
                LogUtils.firebaseEventLog('video', {
                    p_id: this.props.planObj.p_id,
                    p_category: 'Programs',
                    p_name: this.props.label,
                });
            }
        }
    }

    handleSliderChange(data) {
        this.player.seek(data);
        this.setState({ currentTime: data });
    }

    handleFullScreen() {
        this.setState({ fullscreen: !this.state.fullscreen })
        if (this.state.fullscreen) {
            Orientation.lockToPortrait()
            Orientation.unlockAllOrientations();
        } else {
            Orientation.lockToLandscapeLeft()
            Orientation.unlockAllOrientations();
        }
    }

    renderCustomSkin() {
        const flexCompleted = this.getCurrentTimePercentage() * 100;
        const flexRemaining = (1 - this.getCurrentTimePercentage()) * 100;
        if (this.props.item) {
            if (this.props.from === 'challengedetails') {
                url = this.props.item.video;
                LogUtils.infoLog1('Url', url);
            } else if (this.props.from === 'feed') {
                url = this.props.item.file_url;
                LogUtils.infoLog1('feed Url', url);
            }
        } else if (this.props.videoURL) {
            url = this.props.videoURL;
            // if (this.props.from === 'feedpost') {
            //     url = this.props.videoURL;
            // }
        } else {
            url = this.props.planObj.video_url;
        }
        if (!this.state.isEndWorkout) {
            return (
                <View style={styles.container}>
                    <Loader loading={this.state.loading} />
                    {this.state.controls
                        ? (

                            <View style={{ position: 'absolute', zIndex: 111, top: 20, left: 25, }}>
                                <TouchableOpacity
                                    onPress={() => this.onBackPressed()}>
                                    <Image
                                        resizeMethod="resize"
                                        source={require('../res/ic_back.png')}
                                        style={styles.backImageStyle}
                                    />
                                </TouchableOpacity>
                            </View>

                        ) :
                        (
                            <View></View>
                        )}

                    {this.state.controls
                        ? (


                            <View style={{ position: 'absolute', zIndex: 111, top: 20, right: 25, }}>
                                <TouchableOpacity
                                    onPress={() =>
                                        this.handleFullScreen()
                                    }>
                                    {this.state.fullscreen
                                        ? (
                                            <Image
                                                resizeMethod="resize"
                                                source={require('../res/ic_fullscreen_exit.png')}
                                                style={{ width: 20, height: 20, tintColor: '#ffffff' }}
                                            />
                                        ) :
                                        (
                                            <Image
                                                resizeMethod="resize"
                                                source={require('../res/ic_fullscreen.png')}
                                                style={{ width: 20, height: 20, tintColor: '#ffffff' }}
                                            />
                                        )


                                    }

                                </TouchableOpacity>
                            </View>

                        ) :
                        (
                            <View></View>
                        )}


                    <TouchableWithoutFeedback style={styles.fullScreen} onPress={() => {
                        this.setState({ controls: !this.state.controls })
                        // setTimeout(() => this.setState(s => ({ controls: false })), 5000);
                    }}>
                        <Video
                            ref={ref => this.player = ref}
                            // source={{ uri: "https://www.learningcontainer.com/wp-content/uploads/2020/05/sample-mp4-file.mp4" }}
                            source={{ uri: url }}
                            style={styles.fullScreen}
                            // style={this.state.fullscreen ? styles.fullscreenVideo : styles.fullScreen}
                            rate={this.state.rate}
                            paused={this.state.paused}
                            volume={this.state.volume}
                            muted={this.state.muted}
                            ignoreSilentSwitch={this.state.ignoreSilentSwitch}
                            // resizeMode={this.state.resizeMode}
                            onLoad={this.onLoad}
                            onBuffer={this.onBuffer}
                            onProgress={this.onProgress}
                            onEnd={this.onEnd}
                            preferredForwardBufferDuration={10}
                        // onEnd={() => { AlertIOS.alert('Done!') }}
                        // repeat={true}
                        // controls={true}
                        // fullscreen={true}
                        // bufferConfig={{
                        //     minBufferMs: 15000,
                        //     maxBufferMs: 50000,
                        //     bufferForPlaybackMs: 2500,
                        //     bufferForPlaybackAfterRebufferMs: 5000
                        // }}
                        />

                        {/* <VideoPlayer1
                            ref={ref => this.player = ref}
                            source={{ uri: this.props.planObj.video_url }}
                            // disableFullscreen
                            // disablePlayPause
                            disableVolume
                            // disableBack
                            // onShowControls={() => {  this.setState({ controls: true }) }}
                            // onHideControls={() => {  this.setState({ controls: false }) }}
                            onEnd={this.onEnd}

                        /> */}
                    </TouchableWithoutFeedback>
                    {this.state.controls
                        ? (
                            <View style={{ position: 'absolute', backgroundColor: 'transparent', flexDirection: 'row' }}>
                                <TouchableOpacity style={{ marginRight: 50 }} onPress={() => {
                                    if (this.state.currentTime > 15) {
                                        this.player.seek(this.state.currentTime - 15)
                                        this.setState({ currentTime: this.state.currentTime - 15 });
                                    }

                                }}>
                                    {this.renderSkipBackward()}
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => { this.setState({ paused: !this.state.paused }) }}>
                                    {this.renderPlayPause()}
                                </TouchableOpacity>
                                <TouchableOpacity style={{ marginLeft: 50 }} onPress={() => {
                                    this.player.seek(this.state.currentTime + 15)
                                    this.setState({ currentTime: this.state.currentTime + 15 });
                                }}>
                                    {this.renderSkipForward()}
                                </TouchableOpacity>
                            </View>
                        ) :
                        (
                            <View></View>
                        )
                    }
                    {this.state.controls
                        ? (
                            < View style={styles.controls}>


                                <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'center', }}
                                    {...this._panResponder.panHandlers}>

                                    <Text style={styles.timeText}>{getMinutesFromSeconds(this.state.currentTime)}</Text>

                                    <Slider
                                        style={{ width: '75%', justifyContent: 'center', alignSelf: 'center' }}
                                        value={this.state.currentTime}
                                        minimumValue={0}
                                        maximumValue={this.state.duration}
                                        step={1}

                                        // onSlidingStart={(currentTime) =>
                                        //     this.setState({ paused: true })}
                                        onValueChange={(currentTime) =>
                                            this.handleSliderChange(currentTime)}
                                        // onSlidingComplete={(currentTime) =>
                                        //     this.handleSliderChange(currentTime)}
                                        minimumTrackTintColor={'#F44336'}
                                        maximumTrackTintColor={'#FFFFFF'}
                                        thumbTintColor={'#F44336'}
                                        thumbStyle={styles.thumb}
                                    />
                                    <Text style={styles.timeText}>{getMinutesFromSeconds(this.state.duration)}</Text>
                                </View>
                            </View>
                        ) :
                        (
                            <View></View>
                        )
                    }

                    <CustomDialog
                        visible={this.state.isAlert}
                        title='Alert'
                        desc={this.state.alertMsg}
                        onAccept={this.onAccept.bind(this)}
                        no=''
                        yes='Ok' />
                    < CustomDialog
                        visible={this.state.isBackPressed}
                        title='Alert'
                        desc={'Do you want to end this workout?'}
                        onAccept={this.onEndWorkout.bind(this)}
                        onDecline={() => {

                            this.setState({ isBackPressed: false, paused: false })

                        }
                        }
                        no='No'
                        yes='Yes' />
                </View >
            );
        } else {

            return (
                <KeyboardAvoidingView keyboardVerticalOffset={Platform.select({ ios: 0, android: -70 })} style={styles.mainContainer} behavior="padding" enabled>
                    <ImageBackground source={require('../res/post_back.png')} style={styles.mainImageContainer}>
                        <View style={styles.postContents}>
                            <Text style={styles.createPostStyle}>RATE YOUR WORKOUT</Text>

                            {this.renderProfileImg()}

                            <Text style={styles.feedbackNameStyle}>{this.props.planObj.tr_name}</Text>
                            <Text style={styles.feedbackSubStyle}>{this.props.planObj.title}</Text>

                            <AirbnbRating
                                // showRating
                                type='star'
                                ratingCount={5}
                                reviews={['Bad experience !', 'Not that great !', 'It was good !', 'Had a great experience !', 'Absolutely superb !']}
                                imageSize={30}
                                startingValue={this.state.rating}
                                readonly={false}
                                isDisabled={false}
                                reviewSize={15}
                                onFinishRating={this.ratingCompleted.bind(this)}
                                style={{ marginTop: hp('5%'), alignSelf: 'center', paddingVertical: 5 }}
                            />

                            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearSubmitGradient}>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.saveFeebackWorkOut();
                                    }}
                                    style={styles.buttonTuch}>
                                    <Text style={styles.buttonText}>SUBMIT</Text>
                                </TouchableOpacity>
                            </LinearGradient>

                            <View style={styles.inputRow}>
                                <TextInput
                                    style={styles.textInputStyle}
                                    placeholder="Type your review..."
                                    placeholderTextColor="#9c9eb9"
                                    value={this.state.post}
                                    numberOfLines={2}
                                    multiline={true}
                                    onChangeText={text => this.setState({ post: text })}
                                />
                            </View>
                        </View>

                        <CustomDialog
                            visible={this.state.isFeedbackSubmit}
                            title='Success'
                            desc={'Feedback submitted successfully.'}
                            onAccept={this.onFeedbackSubmit.bind(this)}
                            no=''
                            yes='Ok' />

                    </ImageBackground>
                </KeyboardAvoidingView>
            );

        }
    }

    render() {
        return this.renderCustomSkin();
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black'
    },
    fullScreen: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
    fullscreenVideo: {
        height: Dimensions.get('window').width,
        width: Dimensions.get('window').height,
        backgroundColor: 'black',
    },
    controls: {
        backgroundColor: "transparent",
        borderRadius: 5,
        position: 'absolute',
        bottom: 44,
        left: 4,
        right: 4,
    },
    progress: {
        flex: 1,
        flexDirection: 'row',
        borderRadius: 3,
        overflow: 'hidden',
    },
    innerProgressCompleted: {
        height: 20,
        backgroundColor: '#cccccc'
    },
    innerProgressRemaining: {
        height: 20,
        backgroundColor: '#2C2C2C',
    },
    generalControls: {
        flex: 1,
        flexDirection: 'row',
        overflow: 'hidden',
        paddingBottom: 10,
    },
    skinControl: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    rateControl: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    volumeControl: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    resizeModeControl: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    ignoreSilentSwitchControl: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    controlOption: {
        alignSelf: 'center',
        fontSize: 11,
        color: "white",
        paddingLeft: 2,
        paddingRight: 2,
        lineHeight: 12,
    },
    nativeVideoControls: {
        top: 184,
        height: 300
    },
    timeText: {
        width: '12%',
        fontSize: 14,
        textAlign: 'center',
        color: '#ffffff',
        marginTop: (Platform.OS === 'ios') ? 12 : 0,
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    thumb: {
        width: 30,
        height: 30,
        borderRadius: 30 / 2,
        backgroundColor: 'white',
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 2,
        shadowOpacity: 0.35,
    },
    playIcon: {
        width: 35,
        height: 35,
        alignSelf: 'center',
        tintColor: '#ffffff',
    },
    backImageStyle: {
        width: 19,
        height: 16,
        alignSelf: 'flex-start',
        tintColor: '#ffffff',
    },

    mainContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    postContents: {
        width: '90%',
        alignSelf: 'center',
        backgroundColor: '#fff',
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12,
        alignContent: 'center',
        justifyContent: 'center',
        padding: 23,
        marginTop: 10,
    },
    createPostStyle: {
        height: 30,
        alignSelf: 'center',
        color: '#5d5d5d',
        fontFamily: 'Rubik-Medium',
        fontSize: 14,
        fontWeight: '500',
        lineHeight: 30,
    },
    profileImage: {
        width: 150,
        height: 150,
        aspectRatio: 1,
        alignSelf: 'center',
        backgroundColor: "#D8D8D8",
        marginTop: hp('2%'),
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: "#979797",
        borderRadius: 150 / 2,
        resizeMode: "cover",
        justifyContent: 'flex-end',
    },
    feedbackNameStyle: {
        height: 30,
        alignSelf: 'center',
        marginTop: hp('2%'),
        color: '#5a5a5a',
        fontFamily: 'Rubik-Medium',
        fontSize: 14,
        fontWeight: '500',
        lineHeight: 30,
    },
    feedbackSubStyle: {
        alignSelf: 'center',
        color: '#5d5d5d',
        fontFamily: 'Rubik-Medium',
        fontSize: 10,
        fontWeight: '500',
    },
    inputRow: {
        flexDirection: 'row',
        alignItems: "flex-start",
        height: '18%',
        marginTop: hp('2%'),
        marginBottom: hp('3%'),
        borderRadius: 10,
        backgroundColor: '#f3f4f6',
    },
    linearSubmitGradient: {
        width: '90%',
        height: 50,
        bottom: 10,
        borderRadius: 15,
        justifyContent: 'center',
        position: 'absolute',
        alignSelf: 'center',
    },
    buttonTuch:
    {
        backgroundColor: 'transparent',
    },
    buttonText: {
        fontSize: 14,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    textInputStyle: {
        width: wp('75%'),
        color: '#2d3142',
        marginLeft: 5,
        fontFamily: 'Rubik-Regular',
        fontSize: 12,
        fontWeight: '400',
        letterSpacing: 0.23,
    },
    mainImageContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
        justifyContent: 'center',
    },
});

export default VideoPlayer;