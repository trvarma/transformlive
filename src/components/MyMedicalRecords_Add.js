import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    FlatList,
    Image,
    TouchableOpacity,
    BackHandler,
    Text,
    ImageBackground,
    TextInput,
    Platform
} from 'react-native';
import { CustomDialog, Loader, NoInternet, ProgressBar, ProgressBar1 } from './common';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import LinearGradient from 'react-native-linear-gradient';
import NetInfo from "@react-native-community/netinfo";
import { BASE_URL, SWR } from '../actions/types';
import DocumentPicker from 'react-native-document-picker';
import DeviceInfo from 'react-native-device-info';
import PDFView from 'react-native-view-pdf';
import FastImage from 'react-native-fast-image';
import ImagePicker from 'react-native-image-picker';
import LogUtils from '../utils/LogUtils.js';
import _ from 'lodash';
import { allowFunction } from '../utils/ScreenshotUtils.js';
const futch = (url, opts = {}, onProgress) => {
    LogUtils.infoLog1(url, opts)
    return new Promise((res, rej) => {
        var xhr = new XMLHttpRequest();
        xhr.open(opts.method || 'get', url);
        for (var k in opts.headers || {})
            xhr.setRequestHeader(k, opts.headers[k]);
        xhr.onload = e => res(e.target);
        xhr.onerror = rej;
        if (xhr.upload && onProgress)
            xhr.upload.onprogress = onProgress; // event.loaded / event.total * 100 ; //event.lengthComputable
        xhr.send(opts.body);
    });
}

class MyMedicalRecords_Add extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isAlert: false,
            loading: false,
            alertTit: '',
            alertMsg: '',
            reportName: '',
            isNoInternet: false,
            isSuccess: false,
            type: 0,
            attFileType: 0,
            filePath: '',
            fileName: '',
            fileData: '',
            fileUri: '',
            fileType: '',
            isProgress: false,
            progress: 10,
            arrSelImages: [],
        };

    }

    async componentDidMount() {
        allowFunction();
        plan_cnt = await AsyncStorage.getItem('plan_cnt');
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isNoInternet: false });
            }
            else {
                this.setState({ isNoInternet: true });
            }
        });
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
        if (this.props.from) {
            if (this.props.from === 'home') {
                this.setState({ buttonText: 'Modify' });
            }
        }
    }
    componentWillUnmount() {
        this.backHandler.remove();
    }


    async onAccept() {
        this.setState({ isAlert: false, alertMsg: '' });
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }

        if (this.state.isSuccess) {
            this.setState({ isSuccess: false });
            Actions.myMdlReds();
        }

    }

    FlatListItemSeparator1 = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: 1,
                    backgroundColor: "transparent",
                }}
            />
        );
    }

    onBackPressed() {
        Actions.pop();
    }



    renderBottomButton() {
        if (this.state.loading === false) {
            return (
                <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient1}>
                    <TouchableOpacity onPress={() => {
                        this.onButtonClicked();

                    }}
                        style={styles.buttonTuch}>
                        <Text style={styles.buttonText}>Submit</Text>
                    </TouchableOpacity>
                </LinearGradient>
            );
        }
    }



    async onRetry() {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isNoInternet: false });
            }
            else {
                this.setState({ isNoInternet: true });
            }
        });
    }

    async pickPdfDocument() {

        // Pick a single file
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.pdf],
            });
            LogUtils.infoLog1('PDF sel response', res);

            this.setState({
                filePath: res,
                fileName: res.name,
                fileUri: res.uri,
                fileType: res.type,
            });
            // console.log(
            //     this.state.fileUri,
            //     this.state.fileType, // mime type
            //     this.state.fileData,
            //     this.state.filePath,
            // );



        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
                //console.log('user cancelled>>');
            } else {
                throw err;
            }
        }

    }

    async pickImagesFromGallery() {
        // Pick a single file
        // try {
        //     const res = await DocumentPicker.pick({
        //         type: [DocumentPicker.types.images],
        //     });
        //     console.log(
        //         res.uri,
        //         res.type, // mime type
        //         res.name,
        //         res.size
        //     );
        // } catch (err) {
        //     if (DocumentPicker.isCancel(err)) {
        //         // User cancelled the picker, exit any dialogs or menus and move on
        //     } else {
        //         throw err;
        //     }
        // }

        // Pick multiple files
        try {
            const results = await DocumentPicker.pickMultiple({
                type: [DocumentPicker.types.images],
            });
            //console.log('sel images array>>',results);
            if (results.length > 10) {
                this.setState({ fileUri: '', arrSelImages: [] });
                this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'You can select max 10 images' });
            }
            else {
                this.setState({ fileUri: 'Gallery Images', arrSelImages: results });
            }
            // for (const res of results) {
            //     console.log(
            //         res.uri,
            //         res.type, // mime type
            //         res.name,
            //         res.size
            //     );
            // }
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
                //console.log('user cancelled>>');
            } else {
                throw err;
            }
        }
    }

    async pickImagesFromGalleryIOS() {
        // Pick a single file
        let options = {
            title: 'Select Image',

            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.launchImageLibrary(options, response => {
            if (response.didCancel) {
                LogUtils.infoLog('User cancelled image picker');
            } else if (response.error) {
                LogUtils.infoLog1('ImagePicker Error: ', response.error);
            } else {
                // LogUtils.infoLog1('response', response);
                LogUtils.infoLog1('fileUri', response.uri);
                // let selectedImages =[];
                // selectedImages = selectedImages.push({uri:response.uri});
                // LogUtils.infoLog1('selectedImages', selectedImages);
                // this.setState({ fileUri: 'Gallery Images', arrSelImages: [{ 'uri': response.uri }] });
                this.updateCaptureList(response.uri);
                // this.setState({
                //   filePath: response,
                //   fileData: response.data,
                //   fileUri: response.uri,
                //   fileType: response.type,
                // });
                // this.callPhotoUpload();
            }
        });
    }

    captureImage = () => {
        let options = {
            title: 'Select Image',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.launchCamera(options, response => {
            if (response.didCancel) {
                LogUtils.infoLog('User cancelled image picker');
            } else if (response.error) {
                LogUtils.infoLog1('ImagePicker Error: ', response.error);
            } else {
                // LogUtils.infoLog1('response', response);
                LogUtils.infoLog1('fileUri', response.uri);

                this.setState({
                    filePath: response,
                    fileData: response.data,
                    fileUri: response.uri,
                    fileType: response.type,
                });
                this.updateCaptureList(response.uri);
                // this.setState({ fileUri: 'Gallery Images', arrSelImages: results });
                //this.callPhotoUpload();
            }
        });
    };

    updateCaptureList(selFileUri) {
        if (Array.isArray(this.state.arrSelImages) && this.state.arrSelImages.length) {

            this.state.arrSelImages.map((item) => {
                if (item.uri) {
                }
                else {
                    item.uri = selFileUri;
                }
            });

            if (this.state.arrSelImages.length < 10) {
                const obj = { 'uri': '' };
                this.setState({ arrSelImages: [...this.state.arrSelImages, obj] });
            }
            else {
                this.setState({ arrSelImages: this.state.arrSelImages });
            }
        }
        else {
            const obj = { 'uri': selFileUri };
            const obj1 = { 'uri': '' };
            this.setState({ arrSelImages: [...this.state.arrSelImages, obj, obj1] });
        }
    }

    createFormData = (fileUri, body) => {
        const data = new FormData();

        if (this.state.type === 1) {
            if (fileUri) {
                data.append('file', {
                    uri: fileUri,
                    name: this.state.fileName,
                    type: 'application/pdf', // or photo.type
                });
            }
            data.append('data', body);
        }
        else if (this.state.type === 2 || this.state.type === 3) {
            this.state.arrSelImages.forEach((photo) => {
                if (photo.uri) {
                    data.append('file', {
                        uri: photo.uri,
                        name: 'Image',
                        type: 'image/jpeg', // or photo.type
                    });
                }
            });
            data.append('data', body);
        }
        return data;
    };

    async uploadAttachment() {
        this.setState({ isProgress: true });
        let token = await AsyncStorage.getItem('token');
        let body = JSON.stringify({
            file_type: this.state.attFileType,
            report_name: this.state.reportName,
        });
        LogUtils.infoLog1('Body', body);

        futch(`${BASE_URL}/trainee/savemedicalreport`, {
            method: 'POST',
            body: this.createFormData(this.state.fileUri, body),
            headers: {
                'Content-Type': 'multipart/form-data',
                Authorization: `Bearer ${token}`,
            },

        }, (progressEvent) => {
            const progress = Math.floor((progressEvent.loaded / progressEvent.total) * 100);
            this.setState({ progress: progress });
        })
            .then(res => {
                LogUtils.infoLog1('res', res);
                this.setState({ isProgress: false })
                const statusCode = res.status;
                LogUtils.infoLog1('statusCode', statusCode);
                var data = JSON.parse(res.response);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {

                    this.setState({ loading: false, isAlert: true, isSuccess: true, alertTit: data.title, alertMsg: data.message });
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertTit: data.title, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertTit: data.title, alertMsg: data.message });
                    }
                }
                this.setState({ isProgress: false })
            })
            .catch(function (error) {
                LogUtils.infoLog1('upload error', error);
                this.setState({ isProgress: false, loading: false, isAlert: true, alertTit: 'Alert', alertMsg: JSON.stringify(error) });
            });
    }

    async onButtonClicked() {
        if (this.state.reportName) {

            if (this.state.type === 1) {
                if (this.state.fileUri) {
                    this.uploadAttachment();
                }
                else {
                    this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please select your PDF document' });
                }
            }
            else if (this.state.type === 2) {
                if (Array.isArray(this.state.arrSelImages) && this.state.arrSelImages.length) {
                    this.uploadAttachment();
                }
                else {
                    this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please select your images' });
                }
            }
            else if (this.state.type === 3) {
                if (Array.isArray(this.state.arrSelImages) && this.state.arrSelImages.length) {
                    this.uploadAttachment();
                }
                else {
                    this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please capture images' });
                }
            }
            else {
                this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please select attachment' });
            }

        }
        else {
            this.setState({ isAlert: true, alertTit: 'Alert', alertMsg: 'Please enter report name' });
        }
    }

    renderSelectedDocument() {
        if (this.state.type === 1) {
            if (this.state.fileUri) {
                return (
                    <View style={{ flex: 1, marginLeft: 10, marginRight: 10, marginBottom: 70, borderWidth: 0.5, borderColor: '#9c9eb9' }}>

                        {/* <PDFView
                                    style={{ flex: 1 }}
                                    onError={(error) => console.log('onError', error)}
                                    onLoad={() => console.log('PDF rendered from file')}
                                    resource={this.state.fileUri}
                                    resourceType="file"
                                /> */}

                        {/*  Some Controls to change PDF resource  */}
                        <PDFView
                            style={{ flex: 1 }}
                            onError={(error) => {
                                // console.log('onError', error);
                            }}
                            onLoad={() => {
                                // console.log('PDF rendered from file');
                            }}
                            // resource={Platform.OS === 'ios' ? 'sample3.pdf' : '/sdcard/Download/sample3.pdf'}
                            resource={this.state.fileUri}
                            resourceType="url"
                        />
                    </View>
                );
            }
            else {
                return (
                    <View></View>
                )
            }
        }
        else if (this.state.type === 2) {
            if (Array.isArray(this.state.arrSelImages) && this.state.arrSelImages.length) {
                if (Platform.OS === 'android') {
                    return (
                        <View style={{
                            flex: 1,
                            // backgroundColor: 'red',
                            marginLeft: wp('1%'),
                            marginRight: wp('1%'),
                            marginBottom: 70,
                        }}>
                            <FlatList
                                data={this.state.arrSelImages}
                                renderItem={({ item }) => (
                                    <View style={styles.imageContainerStyle}>
                                        <TouchableOpacity
                                            key={item.id}
                                            style={{ flex: 1 }}
                                            onPress={() => {
                                                //showModalFunction(true, item.src);
                                            }}>
                                            <FastImage
                                                style={styles.imageStyle}
                                                source={{
                                                    uri: item.uri,
                                                }}
                                            />
                                        </TouchableOpacity>
                                    </View>
                                )}
                                //Setting the number of column
                                numColumns={3}
                                keyExtractor={(item, index) => item.id}
                            />
                        </View>
                    )
                }
                else {
                    return (
                        <View style={{
                            flex: 1,
                            // backgroundColor: 'red',
                            marginLeft: wp('1%'),
                            marginRight: wp('1%'),
                            marginBottom: 70,
                        }}>
                            <FlatList
                                data={this.state.arrSelImages}
                                renderItem={({ item }) => (
                                    <View style={styles.imageContainerStyle}>
                                        <TouchableOpacity
                                            key={item.id}
                                            style={{ flex: 1 }}
                                            onPress={() => {
                                                //showModalFunction(true, item.src);
                                            }}>
                                            {item.uri
                                                ?
                                                (
                                                    <FastImage
                                                        style={styles.imageStyle}
                                                        source={{
                                                            uri: item.uri,
                                                        }}
                                                    />
                                                )

                                                :
                                                (
                                                    <TouchableOpacity
                                                        style={{
                                                            height: 120,
                                                            backgroundColor: '#ffffff',
                                                            width: wp('31%'),
                                                            justifyContent: 'center',
                                                        }}
                                                        onPress={() => {
                                                            this.pickImagesFromGalleryIOS();
                                                        }}>
                                                        <Image
                                                            progressiveRenderingEnabled={true}
                                                            resizeMethod="resize"
                                                            source={require('../res/ic_add_foodimg.png')}
                                                            style={{
                                                                width: wp('10%'),
                                                                height: wp('10%'),
                                                                // borderRadius: 8,
                                                                tintColor: '#d6d5dd',
                                                                alignSelf: 'center',
                                                                resizeMode: 'cover',
                                                            }}
                                                        />
                                                    </TouchableOpacity>

                                                )
                                            }

                                        </TouchableOpacity>
                                    </View>
                                )}
                                //Setting the number of column
                                numColumns={3}
                                keyExtractor={(item, index) => item.id}
                            />
                        </View>
                    )
                }
            }
            else {
                return (
                    <View></View>
                )
            }
        }
        else if (this.state.type === 3) {
            if (Array.isArray(this.state.arrSelImages) && this.state.arrSelImages.length) {
                return (
                    <View style={{
                        flex: 1,
                        // backgroundColor: 'red',
                        marginLeft: wp('1%'),
                        marginRight: wp('1%'),
                        marginBottom: 70,
                    }}>
                        <FlatList
                            data={this.state.arrSelImages}
                            renderItem={({ item }) => (
                                <View style={styles.imageContainerStyle}>
                                    <TouchableOpacity
                                        key={item.id}
                                        style={{ flex: 1 }}
                                        onPress={() => {
                                            //showModalFunction(true, item.src);
                                        }}>
                                        {item.uri
                                            ?
                                            (
                                                <FastImage
                                                    style={styles.imageStyle}
                                                    source={{
                                                        uri: item.uri,
                                                    }}
                                                />
                                            )

                                            :
                                            (
                                                <TouchableOpacity
                                                    style={{
                                                        height: 120,
                                                        backgroundColor: '#ffffff',
                                                        width: wp('31%'),
                                                        justifyContent: 'center',
                                                    }}
                                                    onPress={() => {
                                                        this.captureImage();
                                                    }}>
                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        source={require('../res/ic_add_foodimg.png')}
                                                        style={{
                                                            width: wp('10%'),
                                                            height: wp('10%'),
                                                            // borderRadius: 8,
                                                            tintColor: '#d6d5dd',
                                                            alignSelf: 'center',
                                                            resizeMode: 'cover',
                                                        }}
                                                    />

                                                    {/* <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        style={styles.imageStyle1}
                                                        source={require('../res/ic_camera_transform.png')}>
                                                    </Image> */}
                                                </TouchableOpacity>

                                            )
                                        }

                                    </TouchableOpacity>
                                </View>
                            )}
                            //Setting the number of column
                            numColumns={3}
                            keyExtractor={(item, index) => item.id}
                        />
                    </View>
                )
            }
            else {
                return (
                    <View></View>
                )
            }
        }
    }

    renderProgressBar() {
        if (Platform.OS === 'android') {
            return (
                <ProgressBar loading={this.state.isProgress} progress={this.state.progress} />
            );
        } else {
            return (
                <ProgressBar1 loading={this.state.isProgress} progress={this.state.progress} />
            );
        }
    }

    render() {
        return (
            <ImageBackground source={require('../res/app_bg.png')} style={styles.mainContainer}>
                <View style={styles.mainContainer}>
                    <Loader loading={this.state.loading} />
                    {this.renderProgressBar()}
                    <NoInternet
                        image={require('../res/img_nointernet.png')}
                        loading={this.state.isNoInternet}
                        onRetry={this.onRetry.bind(this)} />

                    <View style={{
                        flexDirection: 'row',
                        margin: 20,
                    }}>
                        <View style={{ position: 'absolute', zIndex: 111 }}>
                            <TouchableOpacity
                                style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                                onPress={() => this.onBackPressed()}>
                                <Image
                                    source={require('../res/ic_back.png')}
                                    style={styles.backImageStyle}
                                />
                            </TouchableOpacity>
                        </View>

                        <Text style={styles.textHeadTitle}>Add Medical Record</Text>
                    </View>

                    <View style={styles.containerMobileStyle}>
                        <TextInput
                            style={styles.textInputStyle}
                            placeholder="Enter Report Name*"
                            maxLength={200}
                            placeholderTextColor="grey"
                            value={this.state.reportName}
                            autoCapitalize='sentences'
                            returnKeyType={'next'}
                            // onSubmitEditing={() => { this.secondTextInput.focus(); }}
                            onChangeText={text => this.setState({ reportName: text })}
                        />
                    </View>
                    <View style={{ flexDirection: 'column', marginBottom: 10, }}>
                        <Text style={styles.subtextOneStyle}>Add Attachment</Text>

                        <View style={{ flexDirection: 'row', alignSelf: 'center', alignItems: 'center' }}>
                            <View style={{ flexDirection: 'column', alignSelf: 'center', width: '31%' }}>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.setState({ attFileType: 1, type: 1, fileUri: '', arrSelImages: [] })
                                        this.pickPdfDocument()
                                    }}>
                                    <View style={styles.containerMaleStyle}>

                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            style={{ width: 35, height: 35, }}
                                            source={require('../res/ic_pdf.png')}>
                                        </Image>

                                    </View>
                                </TouchableOpacity>
                                {this.state.type === 1
                                    ?
                                    (
                                        <Text style={styles.tvSeldesc}>PDF</Text>
                                    )
                                    :
                                    (
                                        <Text style={styles.desc}>PDF</Text>
                                    )
                                }

                            </View>

                            <View style={{ flexDirection: 'column', alignSelf: 'center', width: '31%' }}>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.setState({ attFileType: 2, type: 2, fileUri: '', arrSelImages: [] })
                                        if (Platform.OS === 'android') {
                                            this.pickImagesFromGallery();
                                        } else {
                                            this.pickImagesFromGalleryIOS();
                                        }
                                        // this.pickImagesFromGallery()
                                    }}>
                                    <View style={styles.containerMaleStyle1}>

                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            style={{ width: 35, height: 35, tintColor: '#ffffff' }}
                                            source={require('../res/transformation.png')}>
                                        </Image>

                                    </View>
                                </TouchableOpacity>
                                {this.state.type === 2
                                    ?
                                    (
                                        <Text style={styles.tvSeldesc}>Gallery</Text>
                                    )
                                    :
                                    (
                                        <Text style={styles.desc}>Gallery</Text>
                                    )
                                }
                            </View>
                            <View style={{ flexDirection: 'column', alignSelf: 'center', width: '37%' }}>
                                <TouchableOpacity
                                    onPress={() => {
                                        if (this.state.arrSelImages.length && this.state.type === 3) {
                                            if (this.state.arrSelImages.length < 10) {
                                                this.captureImage();
                                            }
                                        }
                                        else {
                                            this.setState({ attFileType: 2, type: 3, fileUri: '', arrSelImages: [] });
                                            this.captureImage();
                                        }

                                    }}>
                                    <View style={styles.containerMaleStyle2}>

                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            style={{ width: 35, height: 35, tintColor: '#ffffff' }}
                                            source={require('../res/photo.png')}>
                                        </Image>

                                    </View>
                                </TouchableOpacity>
                                {this.state.type === 3
                                    ?
                                    (
                                        <Text style={styles.tvSeldesc}>Camera</Text>
                                    )
                                    :
                                    (
                                        <Text style={styles.desc}>Camera</Text>
                                    )
                                }
                            </View>
                        </View>
                    </View>

                    {this.renderSelectedDocument()}


                    {this.renderBottomButton()}


                    <CustomDialog
                        visible={this.state.isAlert}
                        title={this.state.alertTit}
                        desc={this.state.alertMsg}
                        onAccept={this.onAccept.bind(this)}
                        no=''
                        yes='Ok' />

                </View>

            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        width: wp('100%'),
        height: hp('100%'),
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'center',
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    containerMobileStyle: {
        borderBottomWidth: 1,
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 15,
        position: 'relative',
        alignSelf: 'center',
        marginLeft: 20,
        marginRight: 20,
        marginTop: 10,
        padding: 8,
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    textInputStyle: {
        height: 40,
        flex: 1,
        fontSize: 13,
        marginLeft: 10,
        fontFamily: 'Rubik-Regular',
        color: '#2d3142',
    },
    linearGradient1: {
        width: '90%',
        height: 50,
        bottom: 0,
        borderRadius: 25,
        justifyContent: 'center',
        position: 'absolute',
        alignSelf: 'center',
        marginBottom: 15,
    },
    buttonTuch:
    {
        backgroundColor: 'transparent',
    },
    buttonText: {
        fontSize: 14,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    subtextOneStyle: {
        fontSize: 16,
        marginLeft: 20,
        fontWeight: '500',
        color: '#2d3142',
        lineHeight: 30,
        fontFamily: 'Rubik-Medium',
        marginTop: 20,
        marginBottom: 10,
    },
    containerMaleStyle: {
        width: 80,
        height: 80,
        // margin: hp('2%'),
        backgroundColor: '#83e4ef',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 20,
        position: 'relative',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    containerMaleStyle1: {
        width: 80,
        height: 80,
        // margin: hp('2%'),
        backgroundColor: '#fd9c93',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 20,
        position: 'relative',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    containerMaleStyle2: {
        width: 80,
        height: 80,
        // margin: hp('2%'),
        backgroundColor: '#b79ef7',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 20,
        position: 'relative',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    desc: {
        fontSize: 12,
        // marginLeft: 20,
        // marginRight: 20,
        fontWeight: '400',
        color: '#9c9eb9',
        lineHeight: 24,
        marginTop: 5,
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
        // marginBottom: hp('2%'),
    },
    tvSeldesc: {
        fontSize: 12,
        // marginLeft: 20,
        // marginRight: 20,
        fontWeight: '400',
        color: '#8c52ff',
        lineHeight: 24,
        marginTop: 5,
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
        // marginBottom: hp('2%'),
    },
    imageContainerStyle: {
        // flex: 1,
        flexDirection: 'column',
        margin: 3,
    },
    imageStyle: {
        height: 120,
        width: wp('31%'),
    },
    imageStyle1: {
        height: 70,
        width: 70,
        alignSelf: 'center',
    },

});

export default MyMedicalRecords_Add;
