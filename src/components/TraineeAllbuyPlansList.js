/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Image,
    Text,
    ImageBackground,
    TouchableOpacity,
    BackHandler,
    FlatList,
    ActivityIndicator,
    SectionList,
    Modal,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { NoInternet, HomeDialog, Loader } from './common';
import NetInfo from "@react-native-community/netinfo";
import { BASE_URL } from '../actions/types';
import DeviceInfo from 'react-native-device-info';
import LogUtils from '../utils/LogUtils.js';
import _ from 'lodash';
import { allowFunction } from '../utils/ScreenshotUtils.js';
import ImageSlider from 'react-native-image-slider';
import { SliderBox } from "react-native-image-slider-box";
import Dialog, {
    DialogContent,
    SlideAnimation
} from 'react-native-popup-dialog';
import Video from 'react-native-video';
import { WebView } from 'react-native-webview';
import { Platform } from 'react-native';
import { plansCheckCountAction } from '../actions/traineeActions'

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}
let imagesArray = [], modalHeight = 0, bottomHeight = 0;

class TraineeAllbuyPlansList extends Component {

    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            isAlert: false,
            alertTitle: '',
            alertMsg: '',
            isInternet: false,
            arrAllPlans: [],
            // arrAllSubcPlans: [],
            // arrAllOneTimePlans: [],
            noDataMsg: '',
            freeTrial: 0,
            isTabClicked: 1,
            active_plan: 0,
            active_plan_msg: '',
            direct_iap: 0,
            media: [],
            user_reviews: '',
            modalPopup: false,
            resObj: {},
            indicatorVisible: true
        };
        this.getAllPlans = this.getAllPlans.bind(this);
    }

    async componentDidMount() {
        try {
            allowFunction();
            if (this.props.isFreeTrial) {
                this.setState({ freeTrial: this.props.isFreeTrial });
            }
            if (Platform.OS === 'android') {
                modalHeight = 0.33;
                bottomHeight = -22;
            } else {
                modalHeight = 0.33;
                bottomHeight = -10;
            }
            NetInfo.fetch().then(state => {
                if (state.isConnected) {
                    this.getAllPlans();
                }
                else {
                    this.setState({ isInternet: true });
                }
            });
            this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
                this.onBackPressed();
                return true;
            });
        } catch (error) {
            console.log(error);
        }
    }

    componentWillUnmount() {
        this.setState({ modalPopup: false });
        this.backHandler.remove();
    }

    onBackPressed() {
        this.setState({ modalPopup: false });
        Actions.pop();
    }

    async getAllPlans() {
        try{
            this.setState({ loading: true });
            let token = await AsyncStorage.getItem('token');
            let params = {"p_id":0,"pp_id":0,"screen":this.props.screen};
            plansCheckCountAction(token,params);
            // `${BASE_URL}/trainee/plans2?trial=${this.state.freeTrial}`,
            fetch(
                `${BASE_URL}/trainee/plans3`,
                {
                    method: 'GET',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${token}`,
                    },
                },
            )
                .then(processResponse)
                .then(res => {
                    const { statusCode, data } = res;
                    LogUtils.infoLog1('statusCode', statusCode);
                    LogUtils.infoLog1('plan data', data);
                    if (statusCode >= 200 && statusCode <= 300) {
    
                        this.setState({ loading: false, media: data.media, arrAllPlans: data.data, resObj: data });
                        LogUtils.infoLog1('this.state.media', this.state.media);
                        this.setState({ active_plan: data.active_plan, active_plan_msg: data.active_plan_msg, direct_iap: data.direct_iap, user_reviews: data.user_reviews });
                        imagesArray = [];
                        data.media.map((item) => {
                            imagesArray.push(item.media_url);
                        })
                        // if (data.data.length) {
                        //     this.setState({ loading: false, arrAllPlans: data.data });
                        //     data.data.map((item) => {
                        //         item.isExpanded = false;
                        //         // LogUtils.infoLog1('Plans', item.data);
                        //         return data.data;
                        //     })
    
                        // }
                        // else {
                        //     this.setState({ loading: false, noDataMsg: 'No data available at this moment' });
                        // }
                        setTimeout(() => { this.setState({ modalPopup: true }) }, 200);
    
                    } else {
                        if (data.message === 'You are not authenticated!') {
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                        } else {
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                        }
                    }
                })
                .catch(function (error) {
                    this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error), alertTitle: 'Alert' });
                });
        }catch(error){
            console.log(error);
        }
    }


    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
        this.setState({ isAlert: false, alertMsg: '', alertTitle: '' });
    }

    async onRetry() {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isInternet: false });
                this.getAllPlans();
            }
            else {
                this.setState({ isInternet: true });
            }
        });
    }

    FlatListItemSeparator1 = () => {
        return (
            <View
                style={{
                    height: 15,
                    width: 1,
                    backgroundColor: "transparent",
                }}
            />
        );
    }

    renderSectionHeader = ({ section }) => {
        return (
            <View>
                <Text style={{
                    fontSize: 16,
                    fontWeight: '500',
                    lineHeight: 16,
                    fontFamily: 'Rubik-Medium',
                    color: '#282c37',
                    alignSelf: 'center',
                    padding: 10,
                    marginTop: 25,
                }}>{section.name}</Text>

                <FlatList
                    style={{ justifyContent: 'center', alignSelf: 'center' }}
                    data={section.data}
                    // numColumns={2}
                    renderItem={this.renderListItem}
                    showsVerticalScrollIndicator={false}
                    ItemSeparatorComponent={this.FlatListItemSeparator1}
                />
            </View>

        );
    }

    renderListItem = ({ item, index }) => {
        return (
            <View>
                <TouchableOpacity key={item.plan_id} style={styles.aroundListStyle}
                    onPress={() => {
                        LogUtils.firebaseEventLog('click', {
                            p_id: item.plan_id,
                            p_category: 'Plans',
                            p_name: `Plan - ${item.name}`,
                        });

                        var mySelItem = _.cloneDeep(item);

                        Actions.traBuyPlanDetNew({ selPlan: mySelItem, active_plan: this.state.active_plan, active_plan_msg: this.state.active_plan_msg, direct_iap: this.state.direct_iap, user_reviews: this.state.user_reviews });
                    }}>
                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={{ uri: item.image2 }}
                        style={{
                            width: wp('92%'),
                            height: hp('22%'),
                            borderRadius: 8,
                            // borderTopLeftRadius: 8,
                            // borderTopRightRadius: 8,
                            resizeMode: 'cover',
                        }}
                    />
                    {/* <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={{ uri: item.image }}
                        style={{
                            width: wp('45%'),
                            height: undefined,
                            // height: hp('26%'),
                            borderTopLeftRadius: 8,
                            borderTopRightRadius: 8,
                            resizeMode: 'contain',
                            aspectRatio: 1 / 1,
                        }}
                    /> */}



                    {/* <View style={{ width: wp('92%'), flexDirection: 'column', alignItems: 'flex-start', alignContent: 'center', justifyContent: 'center', }}>
                        <LinearGradient colors={['#8c52ff', '#8c52ff']} style={{ borderBottomLeftRadius: 8, borderBottomRightRadius: 8, padding: 5, width: '100%', height: 60, justifyContent: 'center', }}>
                            <Text style={styles.txtPlanName2} numberOfLines={5}>{`${item.name}`}</Text>
                            {item.starts_from
                                ?
                                (
                                    <Text style={styles.txtAmount}>{`${item.starts_from}`}</Text>
                                )
                                :
                                (
                                    <View></View>
                                )

                            }
                        </LinearGradient>
                    </View> */}

                    {index % 2 == 0
                        ? (
                            <View style={{
                                flexDirection: 'row',
                                padding: 15,
                                position: 'absolute',
                                width: wp('92%'),
                                height: hp('23%'),
                            }}>

                                <View style={{ flexDirection: 'column', alignItems: 'flex-start', alignContent: 'center', justifyContent: 'center', }}>

                                    <Text style={styles.txtPlanName} numberOfLines={5}>{`${item.name}`}</Text>

                                    {item.starts_from
                                        ?
                                        (
                                            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={{ borderRadius: 25, marginTop: 20, padding: 7, }}>
                                                <Text style={styles.txtAmount}>{`${item.starts_from}`}</Text>
                                            </LinearGradient>
                                        )
                                        :
                                        (
                                            <View></View>
                                        )
                                    }


                                </View>
                            </View>
                        )
                        : (
                            <View style={{
                                flexDirection: 'row',
                                padding: 15,
                                position: 'absolute',
                                width: wp('92%'),
                                height: hp('23%'),
                            }}>
                                <View style={{ marginLeft: wp('25%'), flexDirection: 'column', alignItems: 'flex-end', alignContent: 'center', justifyContent: 'center', alignSelf: 'center' }}>
                                    <Text style={styles.txtPlanName1} numberOfLines={5}>{`${item.name}`}</Text>
                                    {item.starts_from
                                        ?
                                        (
                                            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={{ borderRadius: 25, marginTop: 20, padding: 7, }}>
                                                <Text style={styles.txtAmount}>{`${item.starts_from}`}</Text>
                                            </LinearGradient>
                                        )
                                        :
                                        (
                                            <View></View>
                                        )

                                    }


                                </View>
                            </View>
                        )
                    }


                </TouchableOpacity >

            </View>

        );
    }

    renderSection = ({ item }) => {
        return (
            <View>

            </View>
            // <FlatList
            //     style={{ justifyContent: 'center', alignSelf: 'center' }}
            //     data={item.data}
            //     // numColumns={2}
            //     renderItem={this.renderListItem}
            //     showsVerticalScrollIndicator={false}
            //     ItemSeparatorComponent={this.FlatListItemSeparator1}
            // />
        );
    }

     renderAllPlanListItem = ({ item, index }) => {
        
        return (
            <View>
                <TouchableOpacity key={item.plan_id} style={styles.aroundListStyle}
                    onPress={async () => {
                        LogUtils.firebaseEventLog('click', {
                            p_id: item.plan_id,
                            p_category: 'Plans',
                            p_name: `Plan - ${item.name}`,
                        });
                        var mySelItem = _.cloneDeep(item);
                        LogUtils.infoLog1('this.state.media', this.state.media);
                        Actions.traBuyPlanDetNew({ selPlan: mySelItem, active_plan: this.state.active_plan, active_plan_msg: this.state.active_plan_msg, direct_iap: this.state.direct_iap, media: this.state.media, user_reviews: this.state.user_reviews, obj: this.state.resObj });
                    }}>
                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={{ uri: item.image2 }}
                        style={{
                            width: wp('92%'),
                            height: hp('22%'),
                            borderRadius: 8,
                            resizeMode: 'cover',
                        }}
                    />

                    <View style={{
                        flexDirection: 'row',
                        padding: 15,
                        position: 'absolute',
                        width: wp('92%'),
                        height: hp('23%'),
                    }}>

                        <View style={{ flexDirection: 'column', alignItems: 'flex-start', alignContent: 'center', justifyContent: 'center', }}>

                            <Text style={styles.txtPlanName} numberOfLines={2}>{`${item.name}`}</Text>
                            <Text style={styles.txtPlanDesc} numberOfLines={3}>{`${item.description}`}</Text>

                            {item.starts_from
                                ?
                                (
                                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={{ borderRadius: 25, marginTop: 15, padding: 7, }}>
                                        <Text style={styles.txtAmount}>{`${item.starts_from}`}</Text>
                                    </LinearGradient>
                                )
                                :
                                (
                                    <View></View>
                                )
                            }

                        </View>
                    </View>
                </TouchableOpacity >

            </View>

        );
    }


    renderAllPlansData() {
        if (this.state.loading === false) {

            // if (this.state.isTabClicked === 1) {
            return (
                <View>

                    <FlatList
                        contentContainerStyle={{ paddingBottom: hp('20%'), justifyContent: 'center', alignSelf: 'center' }}
                        data={this.state.arrAllPlans}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={this.renderAllPlanListItem}
                        showsVerticalScrollIndicator={false}
                        ItemSeparatorComponent={this.FlatListItemSeparator1}
                    />
                </View>
            );
            // }
            // else if (this.state.isTabClicked === 2) {
            //     return (
            //         <View>
            //             <View style={{ flexDirection: 'row' }}>
            //                 <TouchableOpacity onPress={() => this.setState({ isTabClicked: 1 })} style={styles.viewTabDeSelect}>
            //                     <Text style={styles.textTabDeSelect}>Subscription</Text>
            //                 </TouchableOpacity>
            //                 <View style={styles.viewTabSelect}>
            //                     <Text style={styles.textTabSelect}>One Time</Text>
            //                 </View>
            //             </View>

            //             <FlatList
            //                 contentContainerStyle={{ justifyContent: 'center', alignSelf: 'center' }}
            //                 data={this.state.arrAllOneTimePlans}
            //                 // numColumns={2}
            //                 renderItem={this.renderAllPlanListItem}
            //                 showsVerticalScrollIndicator={false}
            //                 ItemSeparatorComponent={this.FlatListItemSeparator1}
            //             />
            //         </View>
            //     );
            // }
        }

    }

    render() {
        return (
            <ImageBackground source={require('../res/app_bg.png')} style={styles.containerStyle}>
                <View style={styles.containerStyle}>
                    <Loader loading={this.state.loading} />

                    <NoInternet
                        image={require('../res/img_nointernet.png')}
                        loading={this.state.isInternet}
                        onRetry={this.onRetry.bind(this)} />

                    <View style={{
                        flexDirection: 'row',
                        margin: 20,
                    }}>
                        <View style={{ position: 'absolute', zIndex: 111 }}>
                            <TouchableOpacity
                                onPress={() => this.onBackPressed()}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_back.png')}
                                    style={styles.backImageStyle}
                                />
                            </TouchableOpacity>
                        </View>

                        <Text style={styles.textHeadTitle}>Select a Plan</Text>

                    </View>

                    {this.renderAllPlansData()}

                    {/* <SectionList
                        contentContainerStyle={{ paddingBottom: hp('5%') }}
                        sections={this.state.arrAllPlans}
                        style={styles.secList}
                        showsVerticalScrollIndicator={false}
                        renderSectionHeader={this.renderSectionHeader}
                        renderItem={this.renderSection}
                    /> */}


                </View>

                <HomeDialog
                    visible={this.state.isAlert}
                    title={this.state.alertTitle}
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    no=''
                    yes='Ok' />

                <Dialog
                    onDismiss={() => {
                        this.setState({ modalPopup: false });
                    }}
                    onTouchOutside={() => {
                        this.setState({ modalPopup: false });
                    }}
                    width={.92}
                    height={modalHeight}
                    visible={this.state.modalPopup}
                    dialogAnimation={
                        new SlideAnimation({ slideFrom: 'right' })
                    }>

                    <DialogContent
                        style={{
                            width: wp('100%'),
                            backgroundColor: '#ffffff',
                            flexDirection: 'column',
                            justifyContent: 'center',
                            // alignItems: 'center',
                            alignSelf: 'center',
                        }}>

                        <View >


                            <Text style={{
                                fontSize: 13,
                                fontWeight: '500',
                                color: '#282c37',
                                fontFamily: 'Rubik-Medium',
                                letterSpacing: 0.2,
                                paddingTop: 15,
                                paddingLeft: 10,
                                paddingBottom: 10,
                                // justifyContent:'center',
                                // alignSelf:'center',
                            }}>{this.state.user_reviews}</Text>

                            <TouchableOpacity style={{
                                position: 'absolute',
                                right: 10,
                                top: 15,
                                // padding: 10
                            }}
                                onPress={() => {
                                    this.setState({ modalPopup: false });
                                }}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    style={{
                                        width: 15,
                                        height: 15,
                                        alignSelf: 'center',
                                        tintColor: 'black',
                                    }}
                                    source={require('../res/ic_cross_black.png')}
                                />
                            </TouchableOpacity>


                            <View
                            >

                                {Array.isArray(imagesArray) && imagesArray.length
                                    ?
                                    (
                                        <View style={{
                                            height: hp('25%'),
                                            justifyContent: 'center',
                                            alignItems: "center",
                                        }}>
                                            <SliderBox
                                                images={imagesArray}
                                                sliderBoxHeight={200}
                                                onCurrentImagePressed={index => console.warn(`image ${index} pressed`)}
                                                dotColor="#8c52ff"
                                                inactiveDotColor="#90A4AE"
                                                // paginationBoxVerticalPadding={20}
                                                autoplay
                                                circleLoop
                                                resizeMethod={'resize'}
                                                resizeMode={'cover'}
                                                paginationBoxStyle={{
                                                    position: "absolute",
                                                    bottom: bottomHeight,
                                                    padding: 0,
                                                    alignItems: "center",
                                                    alignSelf: "center",
                                                    justifyContent: "center",
                                                }}
                                                dotStyle={{
                                                    width: 0,
                                                    height: 0,
                                                    borderRadius: 5,
                                                    marginHorizontal: 0,
                                                    padding: 0,
                                                    margin: 0,
                                                    backgroundColor: "rgba(128, 128, 128, 0.92)"
                                                }}
                                                ImageComponentStyle={{
                                                    width: wp('90%'),
                                                    height: undefined,
                                                    justifyContent: 'center',
                                                    alignSelf: 'center',
                                                    borderRadius: 10,
                                                    aspectRatio: 16 / 9,
                                                }}
                                                imageLoadingColor="#2196F3"
                                            />
                                        </View>
                                    ) :
                                    (
                                        <View style={{ justifyContent: 'center', alignSelf: 'center', alignItems: 'center' }}>
                                            {/* <Image
                                                progressiveRenderingEnabled={true}
                                                resizeMethod="resize"
                                                source={{ uri: this.state.resObj.thumbnail_url }}
                                                style={{
                                                    width: wp('90%'),
                                                    height: undefined,
                                                    justifyContent: 'center',
                                                    alignSelf: 'center',
                                                    aspectRatio: 16 / 9,
                                                    // transform: [{rotate: spin}] 
                                                }}
                                            /> */}



                                            <View style={{
                                                width: wp('90%'),
                                                height: hp('26%'),

                                            }}>
                                                {Platform.OS === 'android'
                                                    ? (
                                                        <WebView
                                                            javaScriptEnabled={true}
                                                            domStorageEnabled={true}
                                                            style={{
                                                                flex: 1,
                                                                width: wp('90%'),
                                                                // position: 'absolute', top: 0, bottom: 0, right: 0, left: 0,
                                                                backgroundColor: 'black',
                                                            }}
                                                            source={{
                                                                uri: this.state.resObj.video_url
                                                            }}
                                                        />

                                                    ) : (
                                                        <Video
                                                            ref={videoPlayer => { this.videoPlayer = videoPlayer }}
                                                            source={{ uri: this.state.resObj.video_url }}
                                                            controls={true}
                                                            style={{
                                                                flex: 1,
                                                                width: wp('90%'),
                                                                // position: 'absolute', top: 0, bottom: 0, right: 0, left: 0,
                                                                backgroundColor: 'black',
                                                            }}

                                                        />

                                                    )

                                                }
                                                {/* {this.state.indicatorVisible && (
                                                    <ActivityIndicator
                                                        color="#ffffff"
                                                        size="large"
                                                        style={styles.ActivityIndicatorStyle}
                                                    />
                                                )} */}


                                            </View>

                                            {/* <View style={styles.playContainer1}>


                                                <TouchableOpacity
                                                    activeOpacity={0.7}
                                                    style={{ flexDirection: 'row', justifyContent: 'center' }}
                                                    onPress={() => {
                                                        this.setState({ modalPopup: false });
                                                        setTimeout(() => { Actions.newvideoplay({ videoURL: this.state.resObj.video_url }) }, 100);

                                                        // Actions.newvideoplay({ videoURL: this.state.resObj.video_url });

                                                    }}>
                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        source={require('../res/ic_play_purple.png')}
                                                        style={styles.playIcon} />
                                                </TouchableOpacity>


                                            </View> */}
                                        </View>

                                    )}

                            </View>

                        </View>


                    </DialogContent>
                </Dialog>

                {/* <Modal
                    transparent={true}
                    animationType={'none'}
                    visible={this.state.modalPopup}
                    onRequestClose={() => {
                        console.log('close modal');
                        this.setState({ modalPopup: false });
                    }}>
                    <View style={{
                        backgroundColor: '#ffffff',
                        // position: 'relative',
                        flex: 1,
                        justifyContent: 'center',
                    }}>
                        <View style={{ flex: 1, backgroundColor: 'rgba(0, 0, 0, 0.75)', justifyContent: 'center' }}>


                            <Text style={{
                                fontSize: 15,
                                fontWeight: '500',
                                color: '#ffffff',
                                fontFamily: 'Rubik-Medium',
                                letterSpacing: 0.2,
                                padding: 10,
                                // justifyContent:'center',
                                // alignSelf:'center',
                            }}>{this.state.user_reviews}</Text>


                            <View
                                style={{ width: '100%', height: hp('21%'), marginBottom: 20, justifyContent: 'center' }}>

                                {Array.isArray(this.state.media) && this.state.media.length
                                    ?
                                    (
                                        <ImageSlider
                                            loopBothSides
                                            loop
                                            autoPlayWithInterval={2000}
                                            images={this.state.media}
                                            // titleStyle={styles.sliderDate}
                                            indicatorSize={0}
                                            customSlide={({ index, item, style, width }) => (
                                                <View key={index} style={[style, styles.customSlide]}>
                                                    {item.media_type === 1
                                                        ?
                                                        (
                                                            <Image
                                                                progressiveRenderingEnabled={true}
                                                                resizeMethod="resize"
                                                                source={{ uri: item.media_url }}
                                                                style={{
                                                                    width: wp('100%'),
                                                                    height: undefined,
                                                                    justifyContent: 'center',
                                                                    alignSelf: 'center',
                                                                    aspectRatio: 21 / 10,
                                                                    // transform: [{rotate: spin}] 
                                                                }}
                                                            />
                                                        )
                                                        : (
                                                            <View style={{ justifyContent: 'center' }}>
                                                                <Image
                                                                    progressiveRenderingEnabled={true}
                                                                    resizeMethod="resize"
                                                                    source={{ uri: item.thumbnail_url }}
                                                                    style={{
                                                                        width: wp('100%'),
                                                                        height: undefined,
                                                                        justifyContent: 'center',
                                                                        alignSelf: 'center',
                                                                        aspectRatio: 40 / 17,
                                                                        // transform: [{rotate: spin}] 
                                                                    }}
                                                                />

                                                                <View style={styles.playContainer1}>


                                                                    <TouchableOpacity
                                                                        activeOpacity={0.7}
                                                                        style={{ flexDirection: 'row', justifyContent: 'center' }}
                                                                        onPress={() => {

                                                                            Actions.newvideoplay({ videoURL: item.media_url });
                                                                            // this.renderProgramVideoPlay(this.state.resObj.current_day[0]);

                                                                        }}>
                                                                        <Image
                                                                            progressiveRenderingEnabled={true}
                                                                            resizeMethod="resize"
                                                                            source={require('../res/ic_play_purple.png')}
                                                                            style={styles.playIcon} />
                                                                    </TouchableOpacity>


                                                                </View>
                                                            </View>


                                                        )}



                                                </View>
                                            )}
                                        />
                                    ) :
                                    (
                                        <View></View>

                                    )}

                            </View>

                        </View>
                    </View>
                </Modal> */}

            </ImageBackground >
        );
    }
    ActivityIndicatorLoadingView() {
        //making a view to show to while loading the webpage
        // if (this.state.indicatorVisible === true) {
        return (
            <ActivityIndicator
                color="#ffffff"
                size="large"
                style={styles.ActivityIndicatorStyle}
            />
        );
        // }

    }

    hideSpinner() {

        this.setState({ indicatorVisible: false });
        LogUtils.infoLog1('Loaded', this.state.indicatorVisible);
    }

}


const styles = StyleSheet.create({
    containerStyle: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    backImageStyle: {
        width: 19,
        height: 16,
        alignSelf: 'flex-start',
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    textSubHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        margin: 10
    },
    textNodata: {
        width: wp('90%'),
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        padding: 10,
        margin: 20,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
    },
    aroundListStyle: {
        width: wp('92%'),
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        alignContent: 'center',
        alignSelf: 'center',
        flexDirection: 'column',
        borderColor: '#ddd',
        // borderRadius: 15,
        borderRadius: 8,
        position: 'relative',
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 1,
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },

    aroundListStyle1: {
        width: wp('45%'),
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        alignContent: 'center',
        alignSelf: 'flex-start',
        flexDirection: 'column',
        borderColor: '#ddd',
        borderRadius: 8,
        position: 'relative',
        marginLeft: 5,
        marginRight: 5,
        marginBottom: 1,
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    txtPlanName: {
        // width: wp('100%'),
        // fontSize: 15,
        // fontWeight: '500',
        // color: '#282c37',
        // fontFamily: 'Rubik-Medium',
        // letterSpacing: 0.2,
        // textAlign: 'center',

        width: wp('60%'),
        fontSize: 15,
        fontWeight: '500',
        color: '#282c37',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
    },
    txtPlanDesc: {
        width: wp('60%'),
        fontSize: 11,
        fontWeight: '400',
        color: '#282c37',
        marginTop: 8,
        fontFamily: 'Rubik-Regular',
        letterSpacing: 0.2,
    },
    txtPlanName1: {
        width: wp('60%'),
        fontSize: 15,
        fontWeight: '500',
        color: '#282c37',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
        textAlign: 'right',
    },
    txtPlanName2: {
        fontSize: 11,
        fontWeight: '500',
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
        textAlign: 'center',


    },
    txtAmount: {
        fontSize: 11,
        fontWeight: '500',
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
        padding: 3,
        textAlign: 'center',
        // backgroundColor: '#8c52ff9c',
        // borderTopLeftRadius: 5, borderBottomLeftRadius: 5,
    },
    viewTabSelect: {
        flex: 1,
        backgroundColor: '#8c52ff',
        margin: 15,
        alignSelf: 'center',
        justifyContent: 'center',
        alignContent: 'center',
        flexDirection: 'column',
        borderColor: '#ddd',
        borderRadius: 20,
        position: 'relative',
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    viewTabDeSelect: {
        flex: 1,
        backgroundColor: '#F0EFF6',
        margin: 15,
        alignSelf: 'center',
        justifyContent: 'center',
        alignContent: 'center',
        flexDirection: 'column',
        borderColor: '#ddd',
        borderRadius: 20,
        position: 'relative',
        // shadowColor: '#4075cd',
        // shadowOffset: { width: 0, height: 2 },
        // shadowOpacity: 0.25,
        // shadowRadius: 3.84,
        // elevation: 5
    },
    textTabSelect: {
        fontSize: 12,
        fontWeight: '500',
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
        padding: 11,
        alignSelf: 'center',
        textAlign: 'center',
    },
    textTabDeSelect: {
        fontSize: 12,
        fontWeight: '500',
        color: '#8c52ff',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
        padding: 10,
        alignSelf: 'center',
        textAlign: 'center',
    },
    customSlide: {
        backgroundColor: '#1f1f1f',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
    },
    playContainer1: {
        flexDirection: 'row',
        alignContent: 'center',
        position: 'absolute',
        alignSelf: 'center',
        justifyContent: 'center',
    },
    playIcon: {
        width: 35,
        height: 35,
        alignSelf: 'center',
        justifyContent: 'center',
    },
    ActivityIndicatorStyle: {
        flex: 1,
        justifyContent: 'center',
        position: 'absolute', top: 0, bottom: 0, right: 0, left: 0,
    },
});

export default TraineeAllbuyPlansList;
