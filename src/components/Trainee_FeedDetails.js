import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  SafeAreaView,
  Image,
  TextInput,
  TouchableOpacity,
  BackHandler,
  ImageBackground,
  Text,
  KeyboardAvoidingView,
  ScrollView,
  Alert,
  Modal,
  Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import {
  uploadComment,
  uploadLike,
  uploadPoll,
} from '../actions';
import { Loader, CustomDialog, NoInternet } from './common';
import { Comments_list_item } from './Comments_list_item';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
const profileImageSize = 38;
const padding = 10;
let username = "", userId = '', token;
import NetInfo from "@react-native-community/netinfo";
import Image1 from 'react-native-scalable-image';
import { BASE_URL, SWR } from '../actions/types';
import { allowFunction } from '../utils/ScreenshotUtils.js';
function processResponse(response) {
  const statusCode = response.status;
  const data = response.json();
  return Promise.all([statusCode, data]).then(res => ({
    statusCode: res[0],
    data: res[1],
  }));
}
const screenWidth = Math.round(Dimensions.get('window').width);

class Trainee_FeedDetails extends Component {

  constructor(props) {
    super(props);
    this.state = {
      comment: '',
      commentsArray: [],
      commentsCount: 0,
      likesCount: 0,
      is_poll: 0,
      is_like: 0,
      modalVisible: false,
      imageUri: '',
      feedItem: {},

      isAlert: false,
      alertMsg: '',
      isInternet: false,
      loading: false,
      isSuccess: false,
      noData: '',
    };
  }

  async componentDidMount() {
    allowFunction();
    //this.props.exercisesFetchRefresh();
    username = await AsyncStorage.getItem('userName');
    userId = await AsyncStorage.getItem('userId');
    token = await AsyncStorage.getItem('token');

    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.setState({ loading: true });
        this.getTraineeFeedDetails(this.props.feedId);

      }
      else {
        this.setState({ isInternet: true });
      }
    });

    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      Actions.wall();
      return true;
    });
  }
  componentWillUnmount() {
    this.backHandler.remove();
  }
  async getTraineeFeedDetails(feedId) {
    fetch(
      `${BASE_URL}/trainee/feed?f_id=${feedId}&w=` + screenWidth,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },

      },
    )
      .then(processResponse)
      .then(res => {
        const { statusCode, data } = res;
        if (statusCode >= 200 && statusCode <= 300) {
          this.setState({ loading: false, isAlert: false, isSuccess: true, alertMsg: data.message, feedItem: data.data });
          if (this.state.feedItem.length === 0) {
            this.setState({ noData: 'Unable to retrieve the data.Please try again later.' });
          } else {
            this.setState({ commentsArray: [...this.state.feedItem.comments] })
            this.setState({ commentsCount: this.state.feedItem.comment_cnt })
            this.setState({ likesCount: this.state.feedItem.like_cnt })
            this.setState({ is_poll: this.state.feedItem.is_poll })
            this.setState({ is_like: this.state.feedItem.is_like })
          }

        } else {
          if (data.message === 'You are not authenticated!') {
            this.setState({ loading: false, isAlert: true, alertMsg: data.message });
          } else {
            this.setState({ loading: false, isAlert: true, alertMsg: data.message });
          }
        }
      })
      .catch(function (error) {
        this.setState({ loading: false, isAlert: true, alertMsg: SWR });
      });
  }

  async uploadLike(body, token) {
    fetch(`${BASE_URL}/trainee/like`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: body,
    })
      .then(processResponse)
      .then(res => {
        const { statusCode, data } = res;
        if (statusCode >= 200 && statusCode <= 300) {

        } else {
          if (data.message === 'You are not authenticated!') {
            this.setState({ loading: false, isAlert: true, alertMsg: data.message });
          } else {
            this.setState({ loading: false, isAlert: true, alertMsg: data.message });
          }
        }
      })
      .catch(function (error) {
        this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
      });
  }

  async uploadPoll(body, token) {
    fetch(`${BASE_URL}/trainee/Feedpolloptn`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: body,
    })
      .then(processResponse)
      .then(res => {
        const { statusCode, data } = res;
        if (statusCode >= 200 && statusCode <= 300) {

        } else {
          if (data.message === 'You are not authenticated!') {
            this.setState({ loading: false, isAlert: true, alertMsg: data.message });
          } else {
            this.setState({ loading: false, isAlert: true, alertMsg: data.message });
          }
        }
      })
      .catch(function (error) {
        this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
      });
  }


  Item({ title }) {
    return (
      <View style={styles.item}>
        <Text style={styles.title}>{title}</Text>
      </View>
    );
  }
  onBackPressed() {
    Actions.wall();
  }

  onPollPressed = (feedItem, item) => {
    this.setState({ is_poll: 1 })
    let data = JSON.stringify({
      fId: feedItem.feed_id,
      foId: item.option_id,
    });
    this.props.uploadPoll(data, token);
  }

  renderfileType = feeditem => {
    switch (feeditem.file_type) {
      case 1:
        return (
          <View>
            {feeditem.title
              ? (
                <Text style={styles.titleTextStyle}>{feeditem.title}</Text>
              )
              : (
                <View>
                </View>
              )}

            <View>

              <View style={{ borderRadius: 10, justifyContent: 'center', alignItems: 'center', backgroundColor: '#e1e4e8', }}>
                <Image1
                  style={{ borderRadius: 10 }}
                  width={Dimensions.get('window').width - 20} // height will be calculated automatically
                  source={{ uri: feeditem.file_url }}
                  onPress={() => {
                    this.toggleModal(!this.state.modalVisible);
                    this.setState({ imageUri: feeditem.file_url });
                  }}
                />

              </View>
            </View>

          </View>
        );
      case 2:
        return (
          <View style={styles.container}>
            {/* <Text style={styles.titleTextStyle}>{feeditem.title}</Text> */}
            {feeditem.title
              ? (
                <Text style={styles.titleTextStyle}>{feeditem.title}</Text>
              )
              : (
                <View>
                </View>
              )}

            <View style={styles.containerVideo}>

              <Image1
                style={{ borderRadius: 10, }}
                height={215}
                width={Dimensions.get('window').width - 20} // height will be calculated automatically
                source={{ uri: feeditem.thumbnail_img }}

              />

              <TouchableOpacity
                style={styles.playButton}
                onPress={() => {

                  if (Platform.OS === 'android') {
                    Actions.newvideoplay({ item: feeditem, from: 'feed' });
                    // Actions.customPlayer({ item: feeditem, from: 'feed' });
                  } else {
                    Actions.afPlayer({ item: feeditem, from: 'feed' });
                  }

                }}>
                <Image
                  source={require('../res/ic_play.png')}
                  style={styles.playImageStyle}
                />
              </TouchableOpacity>
            </View>
          </View>


        );
      case 0:
        return (
          <View >
            <Text style={styles.titleTextStyle}>{feeditem.description}</Text>
          </View>
        );
      case 3:
        return (
          <View >
            <Text style={styles.titleTextStyle}>{feeditem.description}</Text>

            <FlatList
              data={feeditem.poll_arr}
              keyExtractor={item => item.option_id}
              extraData={this.state}
              ItemSeparatorComponent={this.FlatListItemSeparator}
              renderItem={({ item }) => {
                return (
                  <View>
                    {this.state.is_poll === 1
                      ? (
                        <View style={styles.containerPercentageStyle}>
                          <View style={{
                            width: `${item.poll_result} %`, height: 50, position: 'absolute', backgroundColor: '#D6D9E5', borderTopLeftRadius: 8,
                            borderBottomLeftRadius: 8,
                          }}>
                          </View>
                          <View >
                            <Text style={styles.optnText}>{`${item.option_name}`}</Text>
                          </View>
                          <Text style={styles.percentageText}>{`${item.poll_result} %`}</Text>
                        </View>
                      )
                      : (
                        <TouchableOpacity style={styles.containerMaleStyle} onPress={() => { this.onPollPressed(feeditem, item) }}>
                          <View >
                            <Text style={styles.countryText}>{`${item.option_name}`}</Text>
                          </View>
                          <View style={styles.mobileImageStyle}>
                            <Image
                              progressiveRenderingEnabled={true}
                              resizeMethod="resize"
                              source={require('../res/ic_right_arrow.png')}
                              style={styles.mobileImageStyle} />
                          </View>
                        </TouchableOpacity>
                      )}
                  </View>
                );

              }} />

          </View>
        );
      case 4:
        return (
          <View>
            <Text style={styles.titleTextStyle}>{feeditem.title}</Text>
            <Text style={styles.titleTextStyle}>{feeditem.description}</Text>
            <Text style={styles.articleTextStyle}
              onPress={() => Actions.webview({ url: feeditem.aricle_link })}>
              {feeditem.aricle_link}
            </Text>

            {feeditem.file_url
              ? (
                <View style={{ borderRadius: 10, justifyContent: 'center', alignItems: 'center', backgroundColor: '#e1e4e8', }}>

                  <Image1
                    style={{ borderRadius: 10, }}
                    width={Dimensions.get('window').width - 20} // height will be calculated automatically
                    source={{ uri: feeditem.file_url }}
                  />

                </View>
              )
              : (
                <View>
                </View>
              )}
          </View>
        );

      case 5:
        return (
          <View>
            <Text style={styles.titleTextStyle}>{feeditem.title}</Text>

            <View style={{ borderRadius: 10, justifyContent: 'center', alignItems: 'center', backgroundColor: '#e1e4e8', marginTop: 10, flexDirection: 'row', alignSelf: 'center' }}>

              <View
                style={{ width: '50%', alignSelf: 'center', alignItems: 'center', borderTopLeftRadius: 6, borderBottomLeftRadius: 6, }}>

                <Image1
                  style={{ borderTopLeftRadius: 6, borderBottomLeftRadius: 6, marginLeft: 3, }}
                  width={(Dimensions.get('window').width / 2) - 10} // height will be calculated automatically
                  source={{ uri: feeditem.file_url }}
                />

              </View>

              <View style={{ width: '50%', borderTopRightRadius: 6, borderBottomRightRadius: 6, }}>


                <Image1
                  style={{ borderTopRightRadius: 6, borderBottomRightRadius: 6, }}
                  width={(Dimensions.get('window').width / 2) - 10} // height will be calculated automatically
                  source={{ uri: feeditem.filename_url1 }}
                />

              </View>

              <View style={{ position: 'absolute', top: 0, width: '100%', flexDirection: 'row' }}>
                <View style={{ width: '50%', alignSelf: 'center' }}>
                  <Text style={styles.desc1}>Before</Text>
                </View>
                <View style={{ width: '50%', alignSelf: 'center' }}>
                  <Text style={styles.desc1}>After</Text>
                </View>
              </View>
            </View>
          </View>

        );

      default:
        break;
    }
  };

  onSendButtonPressed() {
    if (!this.state.comment) {
      Alert.alert('Alert', 'Please write your comment to post');
    }
    else {
      this.setState({ commentsCount: this.state.feedItem.comment_cnt + 1 })
      this.state.feedItem.comment_cnt = this.state.commentsCount;
      this.state.feedItem.comments.push({ comment: this.state.comment, user_name: username, cdate: '1 min ago' });
      this.setState({ commentsArray: [...this.state.feedItem.comments] })
      let data = JSON.stringify({
        fId: this.state.feedItem.feed_id,
        userId: userId,
        comment: this.state.comment,
      });
      this.props.uploadComment(data, token);
      this.setState({ comment: '' })
    }
  }

  onLikePressed() {
    this.setState({ likesCount: this.state.feedItem.like_cnt + 1, is_like: 1 })
    let data = JSON.stringify({
      fId: this.state.feedItem.feed_id,
      userId: userId,
    });
    this.props.uploadLike(data, token);
  }

  async onRetry() {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.setState({ isInternet: false, loading: true });
        this.getTraineeFeedDetails(this.props.feedId);
      }
      else {
        this.setState({ isAlert: true, alertMsg: 'Please enable your internet connection and try again' });
      }
    });
  }

  async onAccept() {
    if (this.state.alertMsg === 'You are not authenticated!') {
      AsyncStorage.clear().then(() => {
        Actions.auth({ type: 'reset' });
      });
    }
    this.setState({ alertMsg: '' });
    this.setState({ isAlert: false });
  }

  render() {
    return (
      <KeyboardAvoidingView style={styles.mainContainer} behavior={Platform.OS === "ios" ? "padding" : null}>

        <ImageBackground style={styles.mainContainer}>
          <ScrollView>
            <View style={styles.containerTop}>
              <TouchableOpacity
                onPress={() => this.onBackPressed()}>
                <Image
                  progressiveRenderingEnabled={true}
                  resizeMethod="resize"
                  source={require('../res/ic_back.png')}
                  style={styles.backImageStyle}
                />
              </TouchableOpacity>


              <View style={styles.container}>
                <View style={[styles.row]}>
                  <View style={styles.row}>
                    <Image
                      progressiveRenderingEnabled={true}
                      resizeMethod="resize"
                      style={styles.avatar}
                      source={{ uri: this.state.feedItem.profile_url }} />
                    <View style={styles.column}>
                      <Text style={styles.textStyle}>{this.state.feedItem.user_name}</Text>
                      <Text style={styles.text}>{this.state.feedItem.cdate}</Text>
                    </View>
                  </View>
                </View>
                <View style={{ marginTop: 10 }}>

                </View>
                {this.renderfileType(this.state.feedItem)}

                <View style={styles.likeRow}>

                  {this.state.is_like === 1
                    ? (
                      <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={require('../res/ic_liked.png')}
                        style={styles.likeImageStyle}
                      />
                    )
                    : (
                      <TouchableOpacity
                        onPress={() => this.onLikePressed()}>
                        <Image
                          progressiveRenderingEnabled={true}
                          resizeMethod="resize"
                          source={require('../res/ic_like.png')}
                          style={styles.likeImageStyle}
                        />
                      </TouchableOpacity>
                    )}


                  <Text style={styles.likeTextStyle}>{this.state.likesCount}</Text>
                  <Image
                    progressiveRenderingEnabled={true}
                    resizeMethod="resize"
                    source={require('../res/ic_comment.png')}
                    style={styles.commentImageStyle}
                  />
                  <Text style={styles.commentTextStyle}>{this.state.commentsCount}</Text>
                </View>

                <Text style={styles.commentTextTextStyle}>Comments</Text>
                <View style={styles.flatStyle}>
                  <SafeAreaView >
                    <Loader loading={this.state.loading} />
                    <FlatList
                      data={this.state.feedItem.comments}
                      renderItem={({ item }) => (
                        <Comments_list_item item={item} />
                      )}
                      keyExtractor={item => item.comment_id}
                      initialNumToRender={5}
                      showsVerticalScrollIndicator={false}
                      refreshing={this.props.refreshing}
                    />
                  </SafeAreaView>

                </View>

              </View>
            </View>

          </ScrollView>
          <View style={styles.mainContainer} >
            <Modal animationType={"slide"} transparent={false}
              visible={this.state.modalVisible}
              onRequestClose={() => { console.log("Modal has been closed.") }}>

              <View style={styles.modal}>
                <Image
                  progressiveRenderingEnabled={true}
                  resizeMethod="resize"
                  style={{ width: '100%', height: '100%', resizeMode: 'contain' }}
                  source={{ uri: this.state.imageUri }}
                />


              </View>
              <TouchableOpacity style={{ position: 'absolute', top: 0, marginTop: hp('2%'), marginLeft: '2%', padding: 10 }}
                onPress={() => { this.toggleModal(!this.state.modalVisible) }}>
                <Image
                  progressiveRenderingEnabled={true}
                  resizeMethod="resize"
                  style={{
                    width: 19,
                    height: 16,
                    alignSelf: 'flex-start'
                  }}
                  source={require('../res/ic_back.png')}
                />
              </TouchableOpacity>
            </Modal>
          </View>
          <View style={styles.containerMobileStyle}>
            <TextInput
              style={styles.textInputStyle}
              placeholder="Write comment..."
              placeholderTextColor="grey"
              multiline={true}
              keyboardType="name-phone-pad"
              value={this.state.comment}
              onChangeText={text => this.setState({ comment: text })}
            />

            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
              <TouchableOpacity
                onPress={() => this.onSendButtonPressed()}>
                <Text style={styles.buttonText}>Send</Text>
              </TouchableOpacity>
            </LinearGradient>
          </View>

          <NoInternet
            image={require('../res/img_nointernet.png')}
            loading={this.state.isInternet}
            onRetry={this.onRetry.bind(this)} />

          <CustomDialog
            visible={this.state.isAlert}
            title='Alert'
            desc={this.state.alertMsg}
            onAccept={this.onAccept.bind(this)}
            no=''
            yes='Ok' />

        </ImageBackground>

      </KeyboardAvoidingView>
    );
  }
  toggleModal(visible) {
    this.setState({ modalVisible: visible });
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    width: '100%',
    height: '100%',
    backgroundColor: '#ffffff',
  },
  container: {
    flex: 1,
    width: '100%',
    height: '100%',
    backgroundColor: '#ffffff',
    marginTop: '6%',
  },
  containerTop: {
    flexDirection: 'column',
    flex: 1,
    margin: '4%'
  },
  backImageStyle: {
    width: 19,
    height: 16,
    marginTop: hp('1%'),
    alignSelf: 'flex-start',
  },
  commentTextTextStyle: {
    height: 30,
    color: '#2d3142',
    fontFamily: 'Rubik-Medium',
    fontSize: 18,
    fontWeight: '500',
    marginTop: 15,
  },
  item: {
    width: 375,
    height: 355,
    backgroundColor: '#ffffff',
    margin: 15,
  },
  title: {
    fontSize: 32,
  },
  row: {
    flexDirection: 'row',
    alignItems: "center"

  },
  column: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
  },
  padding: {
    padding: 5,
  },
  avatar: {
    width: 40,
    height: 40,
    aspectRatio: 1,
    backgroundColor: "#D8D8D8",
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: "#979797",
    borderRadius: profileImageSize / 2,
    width: profileImageSize,
    height: profileImageSize,
    resizeMode: "cover",
    marginRight: padding
  },
  text: {
    width: 230,
    height: 14,
    color: '#9c9eb9',
    fontFamily: 'Rubik-Regular',
    fontSize: 12,
    fontWeight: '400',
    letterSpacing: 0.17,
  },
  textStyle: {
    color: '#2d3142',
    fontFamily: 'Rubik-Medium',
    fontSize: 16,
    fontWeight: '500',
    letterSpacing: 0.23,
  },
  titleTextStyle: {
    color: '#2d3142',
    fontFamily: 'Rubik-Regular',
    fontSize: 16,
    fontWeight: '400',
    letterSpacing: 0.29,
    marginTop: 10,
    marginBottom: 10,
  },
  likeImageStyle: {
    width: 21,
    height: 19,
    alignSelf: 'center'
  },
  commentImageStyle: {
    width: 20,
    height: 19,
    marginLeft: 10,
  },
  likeTextStyle: {
    width: 18,
    height: 14,
    color: '#9c9eb9',
    fontFamily: 'Rubik-Regular',
    fontSize: 14,
    fontWeight: '400',
    letterSpacing: 0.2,
    lineHeight: 14,
    marginLeft: 10,
    textAlign: 'center',
    alignSelf: 'center'
  },
  commentTextStyle: {
    height: 14,
    color: '#9c9eb9',
    fontFamily: 'Rubik-Regular',
    fontSize: 14,
    fontWeight: '400',
    letterSpacing: 0.2,
    lineHeight: 14,
    marginLeft: 10,
  },
  likeRow: {
    flexDirection: 'row',
    alignItems: "center",
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#eff2f8ff',
    marginTop: 15,
    paddingTop: 15,
    paddingBottom: 15,
  },
  flatStyle: {
  },
  textInputStyle: {
    height: 40,
    flex: 1,
    color: '#2d3142',
    fontFamily: 'Rubik-Regular',
    fontSize: 14,
    fontWeight: '400',
    letterSpacing: 0.23,
    marginLeft: 10,
  },
  containerMobileStyle: {
    borderTopWidth: 1,
    backgroundColor: '#fff',
    color: 'white',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: '#ddd',
    position: 'relative',
    alignSelf: 'center',
    padding: 8,
    shadowColor: '#4075cd',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  linearGradient: {
    width: '25%',
    height: 30,
    borderRadius: 20,
    justifyContent: 'center',
    alignSelf: 'center',
    backgroundColor: 'transparent',
  },
  buttonText: {
    fontSize: 14,
    textAlign: 'center',
    margin: 8,
    color: '#ffffff',
    fontFamily: 'Rubik-Medium',
    fontWeight: '500',
  },
  containerMaleStyle: {
    width: wp('90%'),
    height: hp('7%'),
    marginTop: hp('1%'),
    backgroundColor: '#ffffff',
    flexDirection: 'row',
    borderColor: '#ddd',
    borderRadius: 10,
    position: 'relative',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    borderWidth: 1,
  },
  countryText: {
    width: wp('75%'),
    alignSelf: 'center',
    fontSize: 14,
    fontWeight: '400',
    marginLeft: wp('2%'),
    color: '#2d3142',
    letterSpacing: 0.23,
    fontFamily: 'Rubik-Regular',
  },
  mobileImageStyle: { width: 25, height: 25, position: 'relative', alignSelf: 'center', marginRight: 10 },
  articleTextStyle: {
    color: 'blue',
    fontFamily: 'Rubik-Regular',
    fontSize: 16,
    fontWeight: '400',
    letterSpacing: 0.23,
    marginLeft: 10,
    marginBottom: 10,
  },
  containerPercentageStyle: {
    width: wp('90%'),
    height: 50,
    borderColor: '#ddd',
    borderRadius: 10,
    borderWidth: .5,
    marginLeft: 10,
    justifyContent: 'center',
    backgroundColor: '#EFF2F8',
    marginBottom: 10,
  },
  percentageText: {
    color: 'black',
    fontFamily: 'Rubik-Regular',
    fontSize: 18,
    fontWeight: '500',
    letterSpacing: 0.23,
    margin: 10,
    textAlign: 'right',
  },
  optnText: {
    color: '#686C7B',
    fontFamily: 'Rubik-Regular',
    fontSize: 16,
    fontWeight: '400',
    letterSpacing: 0.23,
    margin: 10,
    textAlign: 'left',
    position: 'absolute',
  },
  playButton: {
    padding: 10,
    position: 'absolute',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    bottom: 0,
    marginBottom: 80
  },
  playImageStyle: {
    width: 40,
    height: 40,
    alignSelf: 'center',
    justifyContent: 'center',
  },
  desc1: {
    fontSize: 16,
    fontWeight: '500',
    color: '#ffffff',
    textAlign: 'center',
    fontFamily: 'Rubik-Medium',
    marginTop: 15,
  },
});

const mapStateToProps = state => {
  const { uploadComment, uploadLike, poll } = state.feed;
  const loading = state.feed.loading;
  const refreshing = state.feed.refreshing;
  const error = state.feed.error;
  return { uploadComment, uploadLike, loading, refreshing, error, poll };
};

export default connect(
  mapStateToProps,
  {
    uploadComment,
    uploadLike,
    uploadPoll,
  },
)(Trainee_FeedDetails);
