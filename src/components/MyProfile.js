import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Image,
    TouchableOpacity,
    BackHandler,
    ScrollView,
    Text,
    ImageBackground,
} from 'react-native';
import { connect } from 'react-redux';
import {
    getProfileDet,
    updateProfilePic,
} from '../actions';
import { Loader, NoInternet } from './common';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import ImagePicker from 'react-native-image-picker';
import DocumentPicker from 'react-native-document-picker';
import DeviceInfo from 'react-native-device-info';
import LogUtils from '../utils/LogUtils.js';
import NetInfo from "@react-native-community/netinfo";
import Dialog, {
    DialogContent,
} from 'react-native-popup-dialog';
import { allowFunction } from '../utils/ScreenshotUtils.js';

class MyProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchText: '',
            userName: '',
            goalName: '',
            weight: '',
            profImg: '',

            imagefileId: 0,
            imagefilepath: {
                data: '',
                uri: '',
            },
            imagefileData: '',
            imagefileUri: '',
            imagefileType: '',
            isNoInternet: false,
            isAddImgPop: false,
        };
    }

    async componentDidMount() {
        allowFunction();
        this.setState({
            userName: await AsyncStorage.getItem('userName'),
            goalName: await AsyncStorage.getItem('goalName'),
            weight: await AsyncStorage.getItem('weight'),
            profImg: await AsyncStorage.getItem('profileImg')
        });

        let token = await AsyncStorage.getItem('token');
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isNoInternet: false });
                this.props.getProfileDet({ token });
            }
            else {
                this.setState({ isNoInternet: true });
            }
        });

        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }
    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        if (this.state.isAddImgPop) {
            this.setState({ isAddImgPop: false });
        }
        else {
            Actions.popTo('traineeHome');
        }
    }

    async updateUserName() {
        if (this.props.profdet) {
            if (this.props.profdet.Username) {
                await AsyncStorage.setItem('userName', this.props.profdet.Username);
            }
        }
    }

    async callService() {
        if (this.state.imagefileUri) {
            let token = await AsyncStorage.getItem('token');
            this.props.updateProfilePic(this.state.imagefileUri, token);
        }

    }

    renderProfileImg() {
        this.updateUserName()
        if (this.state.imagefileUri) {
            return (
                <TouchableOpacity
                    style={{
                        width: 100,
                        height: 100,
                        marginTop: 60,
                        alignSelf: 'center',
                        justifyContent: 'center',
                    }}
                    onPress={() => this.setState({ isAddImgPop: true })}>
                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={{ uri: this.state.imagefileUri }}
                        style={styles.profileImage}
                    />
                </TouchableOpacity>

            );
        }
        else if (this.props.profdet.profile) {
            return (
                <TouchableOpacity
                    style={{
                        width: 100,
                        height: 100,
                        marginTop: 60,
                        alignSelf: 'center',
                        justifyContent: 'center',
                    }}
                    onPress={() => this.setState({ isAddImgPop: true })}>
                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={{ uri: this.props.profdet.profile }}
                        style={styles.profileImage}
                    />
                </TouchableOpacity>

            );
        }
        else {
            return (
                <TouchableOpacity
                    style={{
                        width: 100,
                        height: 100,
                        marginTop: 60,
                        alignSelf: 'center',
                        justifyContent: 'center',
                    }}
                    onPress={() => this.setState({ isAddImgPop: true })}>
                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={require('../res/ic_profile_gray.png')}
                        style={styles.profileImage}
                    />
                </TouchableOpacity>
            );
        }
    }

    chooseImage = (hey) => {
        let options = {
            title: 'Select Image',

            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.showImagePicker(options, response => {
            if (response.didCancel) {
                LogUtils.infoLog('User cancelled image picker');
            } else if (response.error) {
                LogUtils.infoLog1('ImagePicker Error: ', response.error);
            }
            else {

                LogUtils.infoLog1('fileUri', response.uri);
                this.setState({
                    imagefilePath: response,
                    imagefileData: response.data,
                    imagefileUri: response.uri,
                    imagefileType: response.type,
                });
                if (response.uri) {
                    this.callService();
                }


            }
        });
    };

    captureImage = () => {
        let options = {
            title: 'Select Image',

            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.launchCamera(options, response => {
            if (response.didCancel) {
                LogUtils.infoLog('User cancelled image picker');
            } else if (response.error) {
                LogUtils.infoLog1('ImagePicker Error: ', response.error);
            } else {
                // LogUtils.infoLog1('response', response);
                LogUtils.infoLog1('fileUri', response.uri);

                this.setState({
                    imagefilePath: response,
                    imagefileData: response.data,
                    imagefileUri: response.uri,
                    imagefileType: response.type,
                });
                if (response.uri) {
                    this.callService();
                }
            }
        });
    };

    async pickImagesFromGallery() {
        // Pick a single file
        try {
            const response = await DocumentPicker.pick({
                type: [DocumentPicker.types.images],
            });

            this.setState({
                imagefilePath: response,
                imagefileData: response.data,
                imagefileUri: response.uri,
                imagefileType: response.type,
            });
            if (response.uri) {
                this.callService();
            }

        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }
    }

    async pickImagesFromGalleryIOS() {
        // Pick a single file
        let options = {
            title: 'Select Image',

            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.launchImageLibrary(options, response => {
            if (response.didCancel) {
                LogUtils.infoLog('User cancelled image picker');
            } else if (response.error) {
                LogUtils.infoLog1('ImagePicker Error: ', response.error);
            } else {
                // LogUtils.infoLog1('response', response);
                LogUtils.infoLog1('fileUri', response.uri);

                this.setState({
                    imagefilePath: response,
                    imagefileData: response.data,
                    imagefileUri: response.uri,
                    imagefileType: response.type,
                });
                if (response.uri) {
                    this.callService();
                }
            }
        });
    }

    renderFoodPref() {
        if (this.props.profdet.food_preference) {
            if (this.props.profdet.food_preference.length === 0) {
                return (
                    <Text style={styles.waterdranktextStyle}>
                        NA
                    </Text>
                );
            }
            else {
                let names = '';
                this.props.profdet.food_preference.map((item) => {
                    if (names.length === 0) {
                        names = names + item.name;
                    }
                    else {
                        names = names + ', ' + item.name;
                    }
                })
                return (
                    <Text style={styles.waterdranktextStyle}>
                        {names}
                    </Text>
                );
            }
        }

    }

    renderAllergies() {
        if (this.props.profdet.food_allergies) {
            if (this.props.profdet.food_allergies.length === 0) {
                return (
                    <Text style={styles.waterdranktextStyle}>
                        NA
                    </Text>
                );
            }
            else {
                let names = '';
                this.props.profdet.food_allergies.map((item) => {
                    if (names.length === 0) {
                        names = names + item.name;
                    }
                    else {
                        names = names + ', ' + item.name;
                    }
                })

                if (this.props.profdet.allergies_other) {
                    return (
                        <Text style={styles.waterdranktextStyle}>
                            {names} - {this.props.profdet.allergies_other}
                        </Text>
                    );
                }
                else {
                    return (
                        <Text style={styles.waterdranktextStyle}>
                            {names}
                        </Text>
                    );
                }

            }
        }

    }

    renderHealthProbs() {
        if (this.props.profdet.health_problems) {
            if (this.props.profdet.health_problems.length === 0) {
                return (
                    <Text style={styles.waterdranktextStyle}>
                        NA
                    </Text>
                );
            }
            else {
                let names = '';
                this.props.profdet.health_problems.map((item) => {
                    if (names.length === 0) {
                        names = names + item.name;
                    }
                    else {
                        names = names + ', ' + item.name;
                    }
                })

                if (this.props.profdet.hlthprblm_other) {
                    return (
                        <Text style={styles.waterdranktextStyle}>
                            {names} - {this.props.profdet.hlthprblm_other}
                        </Text>
                    );
                }
                else {
                    return (
                        <Text style={styles.waterdranktextStyle}>
                            {names}
                        </Text>
                    );
                }
            }
        }

    }

    renderEqpAcc() {
        if (this.props.profdet.equipment_access) {
            if (this.props.profdet.equipment_access.length === 0) {
                return (
                    <Text style={styles.waterdranktextStyle}>
                        NA
                    </Text>
                );
            }
            else {
                let names = '';
                this.props.profdet.equipment_access.map((item) => {
                    if (names.length === 0) {
                        names = names + item.name;
                    }
                    else {
                        names = names + ', ' + item.name;
                    }
                })
                return (
                    <Text style={styles.waterdranktextStyle}>
                        {names}
                    </Text>
                );
            }
        }

    }

    renderWorkingoutLoc() {
        if (this.props.profdet.workout_location) {
            if (this.props.profdet.workout_location.length === 0) {
                return (
                    <Text style={styles.waterdranktextStyle}>
                        NA
                    </Text>
                );
            }
            else {
                let names = '';
                this.props.profdet.workout_location.map((item) => {
                    if (names.length === 0) {
                        names = names + item.name;
                    }
                    else {
                        names = names + ', ' + item.name;
                    }
                })
                return (
                    <Text style={styles.waterdranktextStyle}>
                        {names}
                    </Text>
                );
            }
        }


    }

    renderProfession() {
        if (this.props.profdet.pf_name) {
            if (this.props.profdet.profesion_other) {
                return (
                    <Text style={styles.waterdranktextStyle}>
                        {this.props.profdet.pf_name} - {this.props.profdet.profesion_other}
                    </Text>
                );
            }
            else {
                return (
                    <Text style={styles.waterdranktextStyle}>
                        {this.props.profdet.pf_name}
                    </Text>
                );
            }
        }
        else {
            return (
                <Text style={styles.waterdranktextStyle}>
                    NA
                </Text>
            );
        }

    }

    renderLanguage() {
        if (this.props.profdet.language_name) {
            return (
                <Text style={styles.waterdranktextStyle}>
                    {this.props.profdet.language_name}
                </Text>
            );
        }
        else {
            return (
                <Text style={styles.waterdranktextStyle}>
                    NA
                </Text>
            );
        }
    }

    renderGoalTitle() {
        // if (this.props.profdet.goal_code === 'WL' || this.props.profdet.goal_code === 'PP') {
        //     return (
        //         <Text style={styles.performancetextStyle}>
        //             Goal/Target Weight
        //         </Text>
        //     );
        // }
        // else if (this.props.profdet.goal_code === 'WG') {
        //     return (
        //         <Text style={styles.performancetextStyle}>
        //             Goal/Desired Weight
        //         </Text>
        //     );
        // }
        // else {
        //     return (
        //         <Text style={styles.performancetextStyle}>
        //             Goal
        //         </Text>
        //     );
        // }
        return (
            <Text style={styles.performancetextStyle}>
                Goal
            </Text>
        );
    }

    renderTargetWeight() {
        if (this.props.profdet.weight_target && this.props.profdet.weight_target !== 0) {
            return (
                <Text style={styles.waterdranktextStyle}>
                    {/* {this.props.profdet.goal} / {this.props.profdet.weight_target} */}
                    {this.props.profdet.goal}
                </Text>
            );
        }
        else {
            return (
                <Text style={styles.waterdranktextStyle}>
                    {this.props.profdet.goal}
                </Text>
            );
        }
    }

    renderFitnessForm() {
        if (this.props.profdet.fitness_form) {
            if (this.props.profdet.fitness_form.length === 0) {
                return (
                    <Text style={styles.waterdranktextStyle}>
                        NA
                    </Text>
                );
            }
            else {
                let names = '';
                this.props.profdet.fitness_form.map((item) => {
                    if (names.length === 0) {
                        names = names + item.name;
                    }
                    else {
                        names = names + ', ' + item.name;
                    }
                })
                return (
                    <Text style={styles.waterdranktextStyle}>
                        {names}
                    </Text>
                );
            }
        }
    }

    renderBodyMeasurements() {
        if (this.props.profdet.body_mesurments) {
            if (this.props.profdet.body_mesurments.length === 0) {
                return (
                    <Text style={styles.waterdranktextStyle}>
                        NA
                    </Text>
                );
            }
            else {
                let names = '';
                this.props.profdet.body_mesurments.map((item) => {
                    if (names.length === 0) {
                        names = names + item.name;
                    }
                    else {
                        names = names + ', ' + item.name;
                    }
                })

                return (
                    <Text style={styles.waterdranktextStyle}>
                        {names}
                    </Text>
                );
            }
        }

    }

    renderFoodLikes() {
        if (this.props.profdet.liked_food) {
            if (this.props.profdet.liked_food.length === 0) {
                return (
                    <Text style={styles.waterdranktextStyle}>
                        NA
                    </Text>
                );
            }
            else {
                let likes = '';
                this.props.profdet.liked_food.map((item) => {
                    if (likes.length === 0) {
                        likes = likes + item.name;
                    }
                    else {
                        likes = likes + ', ' + item.name;
                    }
                })

                return (
                    <Text
                        numberOfLines={3}
                        style={styles.waterdranktextStyle}>
                        {likes}
                    </Text>

                );
            }
        }

    }

    async onRetry() {
        let token = await AsyncStorage.getItem('token');
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isNoInternet: false });
                this.props.getProfileDet({ token });
            }
            else {
                this.setState({ isNoInternet: true });
            }
        });
    }

    render() {
        return (
            <ImageBackground source={require('../res/app_bg.png')} style={styles.mainContainer}>
                <Image
                    source={require('../res/ic_bg_homeblue.png')}
                    progressiveRenderingEnabled={true}
                    resizeMethod="resize"
                    style={{
                        width: wp('100%'),
                        height: hp('30%'),
                        position: 'absolute',
                        zIndex: 1,
                    }}
                />

                <View style={styles.containerStyle}>
                    <ScrollView contentContainerStyle={{ paddingBottom: hp('5%') }}>
                        <Loader loading={this.props.loading} />
                        <NoInternet
                            image={require('../res/img_nointernet.png')}
                            loading={this.state.isNoInternet}
                            onRetry={this.onRetry.bind(this)} />
                        <View style={{
                            flexDirection: 'row',
                            margin: 20,
                        }}>
                            <View style={{ position: 'absolute', zIndex: 111 }}>
                                <TouchableOpacity
                                    style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                                    onPress={() => this.onBackPressed()}>
                                    <Image
                                        progressiveRenderingEnabled={true}
                                        resizeMethod="resize"
                                        source={require('../res/ic_back.png')}
                                        style={styles.backImageStyle}
                                    />
                                </TouchableOpacity>
                            </View>

                            <Text style={styles.textHeadTitle}>{this.props.profdet.firstname} {this.props.profdet.lastname}</Text>
                        </View>

                        {this.renderProfileImg()}
                        {/* goal and weight details */}
                        <View style={{ marginTop: 15, }}></View>
                        <View style={styles.goalBg}>
                            <View
                                style={styles.goalDet}>
                                <Text style={styles.textTitle}>Your goal</Text>
                                <Text style={styles.textValue}>{this.props.profdet.goal}</Text>
                            </View>
                            <View style={{ width: 1, backgroundColor: '#e9e9e9' }}></View>
                            <View style={styles.goalDet}>
                                <Text style={styles.textTitle}>Latest weight</Text>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={styles.textWeight}>{this.props.profdet.weight}</Text>
                                    <Text style={styles.textKgs}> kg </Text>
                                    <Image
                                        progressiveRenderingEnabled={true}
                                        resizeMethod="resize"
                                        source={require('../res/img_goal_level.png')}
                                        style={styles.imgGoal}
                                    />
                                </View>

                            </View>
                        </View>
                        <View style={{ marginTop: 25 }}></View>
                        {/* information details */}
                        {this.props.profdet.is_paiduser === 0 && this.props.profdet.show_free_call === 0
                            ? (
                                <View style={styles.goalBg1}>
                                    <TouchableOpacity
                                        onPress={() => Actions.traAllBuyPlans1({screen:"My Profile"})}
                                        style={styles.performanceRow}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_EPP.png')}
                                            style={{
                                                width: 17,
                                                height: 17,
                                                marginRight: 15,
                                                tintColor: '#ffffff',
                                                alignSelf: 'flex-start',
                                            }}
                                        />

                                        <View style={styles.performancecolumn}>
                                            <Text style={styles.performancetextStyle1}>
                                                {this.props.profdet.epf_text}
                                            </Text>
                                            <Text style={styles.waterdranktextStyle1}>
                                                {this.props.profdet.epf_sub_text}
                                            </Text>
                                        </View>

                                    </TouchableOpacity>
                                </View>

                            ) : (
                                <View></View>

                            )}
                        <View style={{ height: 10, }}></View>
                        <View style={styles.goalBg}>
                            <TouchableOpacity
                                onPress={() => Actions.myProfBasicInfo({
                                    firstName: this.props.profdet.firstname,
                                    lastName: this.props.profdet.lastname,
                                    email: this.props.profdet.email,
                                    age: this.props.profdet.age,
                                    height: this.props.profdet.height,
                                    weight: this.props.profdet.weight,
                                    gender: this.props.profdet.gender_id
                                })}
                                style={styles.performanceRow}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_profile_gray.png')}
                                    style={{
                                        width: 17,
                                        height: 17,
                                        marginRight: 15,
                                        tintColor: '#2d3142',
                                        alignSelf: 'flex-start',
                                    }}
                                />

                                <View style={styles.performancecolumn}>
                                    <Text style={styles.performancetextStyle}>
                                        Basic Information
                                    </Text>
                                    <Text style={styles.waterdranktextStyle}>
                                        Name, Age, Gender, Height, Weight
                                    </Text>
                                </View>

                            </TouchableOpacity>
                        </View>

                        <View style={{ height: 10, }}></View>
                        {/* Goal Details */}
                        <View style={styles.goalBg}>
                            <TouchableOpacity
                                onPress={() => Actions.myProfUpdate({ from: 'activity', actLvlId: this.props.profdet.active_level_id })}
                                style={styles.performanceRow}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_info_goal.png')}
                                    style={{
                                        width: 20,
                                        height: 20,
                                        marginRight: 15,
                                        tintColor: '#2d3142',
                                        alignSelf: 'flex-start',
                                    }}
                                />

                                <View style={styles.performancecolumn}>
                                    <Text style={styles.performancetextStyle}>
                                        Active level
                                    </Text>
                                    <Text style={styles.waterdranktextStyle}>
                                        {this.props.profdet.active_level}
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={{ height: 10, }}></View>

                        {/* Food  Pregerences and Allergies */}
                        {this.props.profdet.show_diet === 1
                            ? (<View>
                                <View style={styles.goalBg}>
                                    <TouchableOpacity
                                        onPress={() => Actions.myProfUpdate({ from: 'food', isVeg: this.props.profdet.is_veg, allgIds: this.props.profdet.food_preference })}
                                        style={styles.performanceRow}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_info_food.png')}
                                            style={{
                                                width: 20,
                                                height: 20,
                                                marginRight: 15,
                                                tintColor: '#2d3142',
                                                alignSelf: 'flex-start',
                                            }}
                                        />

                                        <View style={styles.performancecolumn}>
                                            <Text style={styles.performancetextStyle}>
                                                Food Preferences
                                            </Text>
                                            {this.renderFoodPref()}
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ height: 10, }}></View>
                                <View style={styles.goalBg}>
                                    <TouchableOpacity
                                        onPress={() => Actions.myProfUpdate({ from: 'alleries', allgIds: this.props.profdet.food_allergies, otherText: this.props.profdet.allergies_other })}
                                        style={styles.performanceRow}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_allergies.png')}
                                            style={{
                                                width: 20,
                                                height: 18,
                                                marginRight: 15,
                                                tintColor: '#2d3142',
                                                alignSelf: 'flex-start',
                                            }}
                                        />

                                        <View style={styles.performancecolumn}>
                                            <Text style={styles.performancetextStyle}>
                                                Allergies
                                            </Text>
                                            {this.renderAllergies()}
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ height: 10, }}></View>
                            </View>)
                            : (
                                <View>
                                </View>
                            )}


                        <View style={styles.goalBg}>
                            <TouchableOpacity
                                onPress={() => Actions.myProfUpdate({ from: 'healthpro', allgIds: this.props.profdet.health_problems, otherText: this.props.profdet.hlthprblm_other })}
                                style={styles.performanceRow}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_medical.png')}
                                    style={{
                                        width: 20,
                                        height: 20,
                                        marginRight: 15,
                                        tintColor: '#2d3142',
                                        alignSelf: 'flex-start',
                                    }}
                                />

                                <View style={styles.performancecolumn}>
                                    <Text style={styles.performancetextStyle}>
                                        Health Problems
                                    </Text>
                                    {this.renderHealthProbs()}
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={{ height: 10, }}></View>
                        <View style={styles.goalBg}>
                            <TouchableOpacity
                                onPress={() => Actions.myProfUpdate({ from: 'eqpacc', allgIds: this.props.profdet.equipment_access })}
                                style={styles.performanceRow}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_workout_black.png')}
                                    style={{
                                        width: 20,
                                        height: 20,
                                        marginRight: 15,
                                        tintColor: '#2d3142',
                                        alignSelf: 'flex-start',
                                    }}
                                />

                                <View style={styles.performancecolumn}>
                                    <Text style={styles.performancetextStyle}>
                                        Equipment Access
                                    </Text>
                                    {this.renderEqpAcc()}
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={{ height: 10, }}></View>
                        <View style={styles.goalBg}>
                            <TouchableOpacity
                                onPress={() => Actions.myProfUpdate({ from: 'fitnessform', allgIds: this.props.profdet.fitness_form })}
                                style={styles.performanceRow}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_fitness_form.png')}
                                    style={{
                                        width: 20,
                                        height: 20,
                                        marginRight: 15,
                                        tintColor: '#2d3142',
                                        alignSelf: 'flex-start',
                                    }}
                                />

                                <View style={styles.performancecolumn}>
                                    <Text style={styles.performancetextStyle}>
                                        Fitness Form
                                    </Text>
                                    {this.renderFitnessForm()}
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={{ height: 10, }}></View>
                        <View style={styles.goalBg}>
                            <TouchableOpacity
                                onPress={() => Actions.myProfUpdate({ from: 'workloc', allgIds: this.props.profdet.workout_location })}
                                style={styles.performanceRow}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_work_location.png')}
                                    style={{
                                        width: 20,
                                        height: 20,
                                        marginRight: 15,
                                        tintColor: '#2d3142',
                                        alignSelf: 'flex-start',
                                    }}
                                />

                                <View style={styles.performancecolumn}>
                                    <Text style={styles.performancetextStyle}>
                                        Working Out Location
                                    </Text>
                                    {this.renderWorkingoutLoc()}
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={{ height: 10, }}></View>
                        <View style={styles.goalBg}>
                            <TouchableOpacity
                                onPress={() => Actions.myProfUpdate({ from: 'profession', profeId: this.props.profdet.pf_id, otherText: this.props.profdet.profesion_other })}
                                style={styles.performanceRow}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_profession.png')}
                                    style={{
                                        width: 20,
                                        height: 20,
                                        marginRight: 15,
                                        tintColor: '#2d3142',
                                        alignSelf: 'flex-start',
                                    }}
                                />

                                <View style={styles.performancecolumn}>
                                    <Text style={styles.performancetextStyle}>
                                        Profession
                                    </Text>
                                    {this.renderProfession()}
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={{ height: 10, }}></View>
                        <View style={styles.goalBg}>
                            <TouchableOpacity
                                onPress={() => Actions.myProfUpdate({
                                    from: 'goal', goalId: this.props.profdet.goal_id,
                                    goalSubId: this.props.profdet.gt_id, goalCode: this.props.profdet.goal_code, goalName: this.props.profdet.goal,
                                    weight_target: this.props.profdet.weight_target, weight: this.props.profdet.weight, genderId: this.props.profdet.gender_id
                                })}
                                style={styles.performanceRow}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_targetwt.png')}
                                    style={{
                                        width: 20,
                                        height: 20,
                                        marginRight: 15,
                                        tintColor: '#2d3142',
                                        alignSelf: 'flex-start',
                                    }}
                                />

                                <View style={styles.performancecolumn}>
                                    {this.renderGoalTitle()}

                                    {this.renderTargetWeight()}
                                </View>
                            </TouchableOpacity>
                        </View>

                        <View style={{ height: 10, }}></View>
                        <View style={styles.goalBg}>
                            <TouchableOpacity
                                onPress={() => Actions.bodymeasure()}
                                style={styles.performanceRow}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_body_measurements.png')}
                                    style={{
                                        width: 20,
                                        height: 20,
                                        marginRight: 15,
                                        tintColor: '#2d3142',
                                        alignSelf: 'flex-start',
                                    }}
                                />

                                <View style={styles.performancecolumn}>
                                    <Text style={styles.performancetextStyle}>
                                        Body Measurements
                                    </Text>
                                    {this.renderBodyMeasurements()}
                                </View>
                            </TouchableOpacity>
                        </View>

                        {this.props.profdet.show_diet === 1
                            ?
                            (
                                <View>
                                    {/* food like and dislikes */}
                                    <View style={{ height: 10, }}></View>
                                    <View style={styles.goalBg}>
                                        <TouchableOpacity
                                            onPress={() => Actions.myProfUpdate({ from: 'likedislikes', isVeg: this.props.profdet.is_veg, likeDislikeIds: this.props.profdet.liked_food })}
                                            style={styles.performanceRow}>
                                            <Image
                                                progressiveRenderingEnabled={true}
                                                resizeMethod="resize"
                                                source={require('../res/ic_like_grey.png')}
                                                style={{
                                                    width: 20,
                                                    height: 20,
                                                    marginRight: 15,
                                                    tintColor: '#2d3142',
                                                    alignSelf: 'flex-start',
                                                }}
                                            />

                                            <View style={styles.performancecolumn}>
                                                <Text style={styles.performancetextStyle}>
                                                    Food Likes
                                                </Text>
                                                {this.renderFoodLikes()}
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            )
                            :
                            (
                                <View>
                                </View>
                            )}



                    </ScrollView>
                </View>

                <Dialog
                    onDismiss={() => {
                        this.setState({ isAddImgPop: false });
                    }}
                    onTouchOutside={() => {
                        this.setState({ isAddImgPop: false });
                    }}
                    width={0.7}
                    // height={0.5}
                    visible={this.state.isAddImgPop}>
                    <DialogContent
                        style={{
                            backgroundColor: '#ffffff'
                        }}>
                        <View style={{ flexDirection: 'column', padding: 15 }}>
                            <View style={{ alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'column' }}>
                                <View style={{ marginTop: 10, }}></View>
                                <Text style={styles.textAddImgTit}>Add Photo</Text>

                                <View style={{ flexDirection: 'row', alignSelf: 'center', alignItems: 'center', marginTop: 25, }}>
                                    <View style={{ flexDirection: 'column', alignSelf: 'center' }}>
                                        <TouchableOpacity
                                            onPress={() => {
                                                this.setState({ isAddImgPop: false });
                                                this.captureImage();
                                            }}>
                                            <View style={styles.containerMaleStyle2}>

                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    style={{ width: 35, height: 35, tintColor: '#ffffff' }}
                                                    source={require('../res/photo.png')}>
                                                </Image>

                                            </View>
                                        </TouchableOpacity>
                                        <Text style={styles.desc}>Camera</Text>
                                    </View>
                                    <View style={{ flexDirection: 'column', alignSelf: 'center', marginLeft: 25, }}>
                                        <TouchableOpacity
                                            onPress={() => {
                                                this.setState({ isAddImgPop: false });
                                                if (Platform.OS === 'android') {
                                                    this.pickImagesFromGallery();
                                                } else {
                                                    this.pickImagesFromGalleryIOS();
                                                }
                                            }}>
                                            <View style={styles.containerMaleStyle1}>

                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    style={{ width: 35, height: 35, tintColor: '#ffffff' }}
                                                    source={require('../res/transformation.png')}>
                                                </Image>

                                            </View>
                                        </TouchableOpacity>
                                        <Text style={styles.desc}>Gallery</Text>
                                    </View>

                                </View>
                            </View>
                        </View>
                    </DialogContent>
                </Dialog>

            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    containerStyle: {
        flexDirection: 'column',
        flex: 1,
        zIndex: 10,
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'center',
        tintColor: '#ffffff',
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#ffffff',
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    profileImage: {
        width: 100,
        height: 100,
        aspectRatio: 1,
        backgroundColor: "#D8D8D8",
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: "#979797",
        borderRadius: 50,
        // marginTop: 60,
        resizeMode: "cover",
        alignSelf: 'center',
        justifyContent: 'center',
    },
    goalBg: {
        width: wp('90%'),
        marginLeft: wp('5%'),
        marginRight: wp('5%'),
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderRadius: 6,
        position: 'relative',
        alignSelf: 'center',
        shadowColor: "#000",
        shadowOffset: { width: 5, height: 0 },
        shadowOpacity: 0.25,
        shadowRadius: 10,
        elevation: 5,
        marginTop: 3.5,
    },
    goalBg1: {
        width: wp('90%'),
        marginLeft: wp('5%'),
        marginRight: wp('5%'),
        backgroundColor: '#8c52ff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderRadius: 6,
        position: 'relative',
        alignSelf: 'center',
        shadowColor: "#000",
        shadowOffset: { width: 5, height: 0 },
        shadowOpacity: 0.25,
        shadowRadius: 10,
        elevation: 5,
        marginTop: 3.5,
    },

    goalDet: {
        width: wp('45%'),
        padding: 15,
        justifyContent: 'flex-start',
        flexDirection: 'column',
        alignContent: 'center',
        alignItems: 'flex-start',
    },
    textTitle: {
        fontSize: 12,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        lineHeight: 18,
    },
    imgGoal: {
        width: 50,
        height: 26,
        marginLeft: 10,
        alignSelf: 'flex-end',
    },
    textValue: {
        fontSize: 20,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        marginTop: 10,
        color: '#282c37',
    },
    textWeight: {
        fontSize: 26,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        marginTop: 10,
        color: '#282c37',
    },
    textKgs: {
        fontFamily: 'Rubik-Medium',
        marginTop: 10,
        color: '#282c37',
        fontSize: 16,
        alignSelf: 'flex-end',
        fontWeight: '400',
        lineHeight: 24,
    },
    performanceRow: {
        flexDirection: 'row',
        flex: 1,
        padding: 15,
        justifyContent: 'flex-start',
        marginLeft: 5,
    },
    performancecolumn: {
        flexDirection: 'column',
        flex: 1,
        alignItems: 'flex-start',
        marginLeft: 10,
    },
    performancetextStyle: {
        fontSize: 14,
        fontWeight: '500',
        color: '#2d3142',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    waterdranktextStyle: {
        fontSize: 12,
        fontWeight: '500',
        color: '#C3C1CE',
        textAlign: 'left',
        lineHeight: 16,
        marginTop: 5,
        fontFamily: 'Rubik-Regular',
    },
    performancetextStyle1: {
        fontSize: 14,
        fontWeight: '500',
        color: '#ffffff',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    waterdranktextStyle1: {
        fontSize: 12,
        fontWeight: '500',
        color: '#ffffff',
        textAlign: 'left',
        lineHeight: 16,
        marginTop: 5,
        fontFamily: 'Rubik-Regular',
    },
    textAddImgTit: {
        fontSize: 16,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
    },
    containerMaleStyle1: {
        width: 80,
        height: 80,
        // margin: hp('2%'),
        backgroundColor: '#fd9c93',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 20,
        position: 'relative',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    containerMaleStyle2: {
        width: 80,
        height: 80,
        // margin: hp('2%'),
        backgroundColor: '#b79ef7',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 20,
        position: 'relative',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    desc: {
        fontSize: 12,
        fontWeight: '400',
        color: '#9c9eb9',
        lineHeight: 24,
        marginTop: 5,
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
});

const mapStateToProps = state => {
    const { profdet } = state.procre;
    const loading = state.procre.loading;
    const error = state.procre.error;
    return { profdet, loading, error };
};

export default connect(
    mapStateToProps,
    {
        getProfileDet,
        updateProfilePic,
    },
)(MyProfile);
