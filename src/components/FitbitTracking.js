import React, { Component } from 'react';
import { StyleSheet, Text, View, Linking, TouchableOpacity } from 'react-native';
import qs from 'qs';
import config from '../config.js';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { withNavigationFocus } from 'react-navigation';
function OAuth(client_id, cb) {
    Linking.addEventListener('url', handleUrl);
    function handleUrl(event) {
        Linking.removeEventListener('url', handleUrl);
        const [, query_string] = event.url.match(/\#(.*)/);
        const query = qs.parse(query_string);
        cb(query.access_token);
    }
    const oauthurl = `https://www.fitbit.com/oauth2/authorize?${qs.stringify({
        client_id,
        response_type: 'token',
        scope: 'heartrate activity activity profile sleep',
        redirect_uri: 'fitness://fitbittracking',
        expires_in: '31536000',
    })}`;
    Linking.openURL(oauthurl).catch(err => console.error('Error processing linking', err));
}


function getData(access_token) {
    fetch('https://api.fitbit.com/1/user/-/activities/steps/date/today/1d.json', {
        method: 'GET',
        headers: {
            Authorization: `Bearer ${access_token}`,
        },
        // body: `root=auto&path=${Math.random()}`
    })
        .then(res => res.json())
        .then(res => {
            //console.log(`res: ${JSON.stringify(res)}`);
        })
        .catch(err => {
            console.error('Error: ', err);
        });
}

class FitbitTracking extends Component {
    componentDidMount() {
        OAuth(config.client_id, getData);
    }

    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={() => OAuth(config.client_id, getData)}>
                    <Text style={styles.welcome}>
                        Welcome to Fitbit Integration
                    </Text>
                </TouchableOpacity>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#00a8b5',
    },
    welcome: {
        fontSize: 25,
        textAlign: 'center',
        color: '#fff',
        margin: 10,
    },
});

const mapStateToProps = state => {
    return {};
};

export default withNavigationFocus(
    connect(
        mapStateToProps,
        {},
    )(FitbitTracking),
);