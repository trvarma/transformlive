import React from 'react';
import { Text, View, Image, StyleSheet, TouchableHighlight } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

const profileImageSize = 38;
const padding = 10;
let profImg;

const Comments_list_item = props => {
  const {
    viewStyle,
    textStyle,
    viewDateStyle,
    textDateStyle,
    textDateStyle1,
  } = styles;
  //   const { text, name, imageWidth, imageHeight, uid, image } = this.props;

  //     // Reduce the name to something
  //     const imgW = imageWidth || this.state.width;
  //     const imgH = imageHeight || this.state.height;
  //     const aspect = imgW / imgH || 1;
  
  return (
    <View style={viewStyle}>
      <View style={[styles.row]}>
        <View style={styles.row}>
          <Image style={styles.avatar} source={{ uri: props.item.profile_url }} />
          <View style={styles.column}>
            <Text style={styles.textStyle}>{props.item.user_name}</Text>
            <Text style={styles.text}>{props.item.cdate}</Text>
          </View>
          {/* <Icon name="ios-more" /> */}
        </View>
      </View>
      <Text style={styles.titleTextStyle}>{props.item.comment}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: "center"
  },
  column: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignSelf: 'center',
  },
  padding: {
    padding: 5,
  },
  avatar: {
    width: 40,
    height: 40,
    aspectRatio: 1,
    backgroundColor: "#D8D8D8",
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: "#979797",
    borderRadius: profileImageSize / 2,
    width: profileImageSize,
    height: profileImageSize,
    resizeMode: "cover",
    marginRight: padding
  },
  text: {
    height: 24,
    color: '#9c9eb9',
    fontFamily: 'Rubik-Regular',
    fontSize: 13,
    fontWeight: '400',
    letterSpacing: 0.2,
  },
  textStyle: {
    color: '#2d3142',
    fontFamily: 'Rubik-Medium',
    fontSize: 14,
    fontWeight: '500',
    letterSpacing: 0.23,
    marginTop: 10,
  },
  titleTextStyle: {
    color: '#2d3142',
    fontFamily: 'Rubik-Regular',
    fontSize: 14,
    fontWeight: '400',
    lineHeight: 22,
    marginLeft: 50,
    marginTop: 5,
  },
  likeImageStyle: {
    width: 21,
    height: 19,
    marginLeft: 10,
    alignSelf: 'center'
  },
  commentImageStyle: {
    width: 20,
    height: 19,
    marginLeft: 10,
  },
  likeTextStyle: {
    width: 18,
    height: 14,
    color: '#9c9eb9',
    fontFamily: 'Rubik-Regular',
    fontSize: 14,
    fontWeight: '400',
    letterSpacing: 0.2,
    lineHeight: 14,
    marginLeft: 10,
    textAlign: 'center',
    alignSelf: 'center'
  },
  commentTextStyle: {
    width: 9,
    height: 14,
    color: '#9c9eb9',
    fontFamily: 'Rubik-Regular',
    fontSize: 14,
    fontWeight: '400',
    letterSpacing: 0.2,
    lineHeight: 14,
    marginLeft: 10,
  },
  likeRow: {
    flexDirection: 'row',
    alignItems: "center",
    marginTop: 15,
    marginBottom: 15,

  },
  BorderClass:
  {
    borderWidth: 1,
    borderColor: '#F44336',
    borderRadius: 10
  },

  viewStyle: {
    paddingLeft: 3,
    paddingTop: 5,
    paddingBottom: 5,
    color: '#d6d9e0',
    backgroundColor: 'white',
  },
  viewDateStyle: {
    flexDirection: 'row',
  },
  textDateStyle: {
    fontSize: 11,
    color: '#858C94',
  },
  textDateStyle1: {
    fontSize: 11,
    color: '#2c2c2c',
  },
});

export { Comments_list_item };
