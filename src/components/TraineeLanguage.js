import React, { Component } from 'react';
import { connect } from 'react-redux';
import { CustomDialog, Loader, NoInternet } from './common';
import { withNavigationFocus } from 'react-navigation';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import {
    View,
    Text,
    Image,
    StyleSheet,
    TouchableOpacity,
    BackHandler,
    ImageBackground,
    KeyboardAvoidingView,
    StatusBar,
    FlatList,
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { getTraineeGoals } from '../actions';
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from "@react-native-community/netinfo";
import { BASE_URL } from '../actions/types';

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}


class TraineeLanguage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            mobile: '',
            languages: [],
            languageId: 0,
            fakeContact: [],
            SelectedFakeContactList: [],
            isAlert: false,
            loading: false,
            alertMsg: '',
            isInternet: false,
            noData: '',

        };
    }

    async componentDidMount() {
        StatusBar.setHidden(true);
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.getLanguageMasters();
            }
            else {
                this.setState({ isInternet: true });
            }
        });

        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            if (this.props.isFocused) {
                Actions.pop();
            } else {
                this.props.navigation.goBack(null);
            }
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onMobileNumberChanged(text) {
        this.props.mobileNumberChanged(text);
    }

    onBackPressed() {
        Actions.pop();
    }

    async getLanguageMasters() {
        this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/master/language`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },

            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false, languages: data.data });
                    if (this.state.languages.length === 0) {
                        this.setState({ noData: 'Cureently no languages are available to select.' });
                    }
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ isAlert: true, alertMsg: data.message, });
                    } else {
                        this.setState({ isAlert: true, alertMsg: data.message, });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ isAlert: true, alertMsg: JSON.stringify(error), });
            });
    }

    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
        this.setState({ alertMsg: '' });
        this.setState({ isAlert: false });
    }

    async onSuccess() {
        this.setState({ isSuccess: false, sucMsg: '' });
    }

    async onButtonPressed() {
        if (this.state.languageId !== 0) {
            await AsyncStorage.setItem('languageId', this.state.languageId.toString());
            Actions.profilePic();
        }
        else {
            this.setState({ isAlert: true, alertMsg: 'Please select your preferred language' });
        }
    }

    renderButton() {
        return (
            <View style={styles.containerSubmitStyle}>
                <Text style={styles.textSubmitStyle}>
                    {'request otp'.toUpperCase()}
                </Text>
            </View>
        );
    }

    FlatListItemSeparator = () => {
        return (
            <View
                style={{
                    height: 2,
                    width: "100%",
                    backgroundColor: "transparent",
                }}
            />
        );
    }

    async onAccept() {
        this.setState({ alertMsg: '' });
        this.setState({ isAlert: false });
    }

    renderFlatList() {
        if (this.state.languages) {
            if (Array.isArray(this.state.languages) && this.state.languages.length) {
                return (
                    <View>
                        <FlatList
                            contentContainerStyle={{ paddingBottom: hp('40%') }}
                            data={this.state.languages}
                            keyExtractor={item => item.id}
                            extraData={this.state}
                            showsVerticalScrollIndicator={false}
                            ItemSeparatorComponent={this.FlatListItemSeparator}
                            renderItem={({ item }) => {
                                return <TouchableOpacity key={item.id} style={styles.containerMaleStyle} onPress={() => {
                                    this.press(item)
                                }}>
                                    <View >
                                        <Text style={styles.countryText}>{`${item.name}`}</Text>
                                    </View>
                                    <View style={styles.mobileImageStyle}>
                                        {item.check
                                            ? (
                                                <TouchableOpacity onPress={() => {
                                                    this.press(item)
                                                }}>
                                                    <Image source={require('../res/check.png')} style={styles.mobileImageStyle} />
                                                </TouchableOpacity>
                                            )
                                            : (
                                                <TouchableOpacity onPress={() => {
                                                    this.press(item)
                                                }}>
                                                    <Image source={require('../res/uncheck.png')} style={styles.mobileImageStyle} />
                                                </TouchableOpacity>
                                            )}
                                    </View>
                                </TouchableOpacity>
                            }} />
                    </View>

                );
            }
            else {
                return (

                    <View style={{ flexDirection: 'row', flex: 1, }}>
                        <Text style={styles.textNodata}>{this.state.noData}</Text>
                    </View>
                );
            }
        }
    }

    async onRetry() {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isInternet: false });
                this.getLanguageMasters();
            }
            else {
                this.setState({ isInternet: true });
            }
        });
    }

    render() {
        return (
            <KeyboardAvoidingView keyboardVerticalOffset={Platform.select({ ios: 0, android: -20 })} style={styles.containerStyle} behavior="padding" enabled>
                <ImageBackground source={require('../res/app_bg.png')} style={styles.img}>
                    <Loader loading={this.state.loading} />
                    <NoInternet
                        image={require('../res/img_nointernet.png')}
                        loading={this.state.isInternet}
                        onRetry={this.onRetry.bind(this)} />
                    <View style={styles.optionInnerContainer}>
                        <Image
                            source={require('../res/ic_ver_line.png')}
                            style={styles.topBarImageStyle}
                        />
                        <Image
                            source={require('../res/ic_ver_line.png')}
                            style={styles.topBarImageStyle}
                        />
                        <Image
                            source={require('../res/ic_ver_line.png')}
                            style={styles.topBarImageStyle}
                        />
                        <Image
                            source={require('../res/ic_ver_line.png')}
                            style={styles.topBarImageStyle}
                        />
                        <Image
                            source={require('../res/ic_ver_line.png')}
                            style={styles.topBarImageStyle}
                        />
                        <Image
                            source={require('../res/ic_ver_line.png')}
                            style={styles.topBarImageStyle}
                        />
                        <Image
                            source={require('../res/ic_ver_line.png')}
                            style={styles.topBarImageStyle}
                        />
                        <Image
                            source={require('../res/ic_ver_line.png')}
                            style={styles.topBarImageStyle}
                        />
                        <Image
                            source={require('../res/ic_ver_line.png')}
                            style={styles.topBarImageStyle}
                        />
                        <Image
                            source={require('../res/ic_ver_line.png')}
                            style={styles.topBarImageStyle}
                        />
                        <Image
                            source={require('../res/ic_ver_shade.png')}
                            style={styles.topBarImageStyle}
                        />
                    </View>

                    <TouchableOpacity
                        onPress={() => this.onBackPressed()}>
                        <Image
                            source={require('../res/ic_back.png')}
                            style={styles.backImageStyle}
                        />
                    </TouchableOpacity>

                    <View style={styles.optionInnerContainer}>
                        <Image
                            source={require('../res/ic_app.png')}
                            style={styles.appImageStyle}
                        />
                    </View>

                    <View style={styles.containericonStyle}>

                        <Text style={styles.subtextOneStyle}>What is your preferred language</Text>
                        <Text style={styles.desc}>You always can change this later</Text>

                        {this.renderFlatList()}


                    </View>

                    <View style={styles.viewBottom}>
                        <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                            <TouchableOpacity
                                onPress={() => this.onButtonPressed()}>
                                <Text style={styles.buttonText}>Next</Text>
                            </TouchableOpacity>
                        </LinearGradient>
                    </View>

                    <CustomDialog
                        visible={this.state.isAlert}
                        title='Alert'
                        desc={this.state.alertMsg}
                        onAccept={this.onAccept.bind(this)}
                        no=''
                        yes='Ok' />
                </ImageBackground>
            </KeyboardAvoidingView>
        );
    }

    press = (hey) => {
        this.state.languages.map((item) => {
            if (item.id === hey.id) {
                item.check = true;
                this.setState({ languageId: item.id });
            }
            else {
                item.check = false;
            }
        })

    }
}

const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        height: '100%',
        flex: 1,
    },
    img: {
        width: '100%',
        height: '100%',
        flex: 1,
    },
    iconsView: {
        flexDirection: 'row',
        alignContent: 'center',
        marginTop: 45,
        justifyContent: 'center',
    },
    back: {
        width: 25,
        height: 18,
        marginTop: 45,
        marginLeft: 20,
        position: 'absolute',
        alignSelf: 'flex-start'
    },
    containericonStyle: {
        flexDirection: 'column',
        alignSelf: 'center',
        justifyContent: 'center',
    },
    containerMaleStyle: {
        width: wp('90%'),
        height: hp('9%'),
        marginTop: hp('1%'),
        backgroundColor: '#ffffff',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 10,
        position: 'relative',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    containerSubmitStyle: {
        width: '93%',
        backgroundColor: '#06a283',
        flexDirection: 'row',
        borderRadius: 5,
        justifyContent: 'center',
        alignSelf: 'center',
        padding: 15,
        marginTop: 5,
    },
    subtextOneStyle: {
        fontSize: 20,
        marginTop: hp('22%'),
        marginLeft: 40,
        marginRight: 40,
        fontWeight: '500',
        color: '#2d3142',
        lineHeight: 30,
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    desc: {
        fontSize: 14,
        marginTop: 10,
        marginLeft: 20,
        marginRight: 20,
        fontWeight: '400',
        color: '#9c9eb9',
        lineHeight: 24,
        textAlign: 'center',
        fontFamily: 'Rubik-Regular',
        marginTop: hp('2%'),
        marginBottom: hp('2%'),
    },
    textSubmitStyle: { fontSize: 14, color: '#ffffff', fontWeight: 'bold' },
    mobileImageStyle: { width: 25, height: 25, position: 'relative', alignSelf: 'center', marginRight: 10 },
    linearGradient: {
        width: '95%',
        height: 50,
        borderRadius: 25,
        justifyContent: 'center',
        alignSelf: 'center',
        backgroundColor: 'transparent',
        marginBottom: 15,
    },
    buttonText: {
        fontSize: 16,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    countryText: {
        width: wp('75%'),
        alignSelf: 'center',
        fontSize: 14,
        fontWeight: '400',
        marginLeft: wp('2%'),
        color: '#2d3142',
        letterSpacing: 0.23,
        fontFamily: 'Rubik-Regular',
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: hp('7%'),
        marginLeft: 20,
        marginBottom: 20,
        alignSelf: 'flex-start',
    },
    appImageStyle: {
        width: 200,
        height: 60,
        marginTop: -45,
    },
    topBarImageStyle: {
        width: wp('9%'),
        height: 8,
        marginBottom: 20,
        marginRight: 1,
    },
    optionInnerContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignSelf: 'center',
    },
    viewBottom: {
        width: wp('93%'),
        alignContent: 'center',
        alignSelf: 'center',
        position: 'absolute',
        bottom: 0,
        backgroundColor: '#fff',
    },
    textNodata: {
        width: wp('100%'),
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        padding: 20,
        marginTop: 15,
        marginBottom: 15,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
    },
});

const mapStateToProps = state => {

    const { goals } = state.procre;
    const loading = state.procre.loading;
    const error = state.procre.error;
    return { loading, error, goals };

};

export default withNavigationFocus(
    connect(
        mapStateToProps,
        { getTraineeGoals },
    )(TraineeLanguage),
);
