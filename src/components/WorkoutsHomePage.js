import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    FlatList,
    Image,
    TouchableOpacity,
    BackHandler,
    ScrollView,
    Text,
    Dimensions,
    Animated,
    SafeAreaView,
    TouchableWithoutFeedback
} from 'react-native';
import { connect } from 'react-redux';
import { CustomDialog, Loader } from './common';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import NetInfo from "@react-native-community/netinfo";
import { BASE_URL } from '../actions/types';
import DeviceInfo from 'react-native-device-info';
import LogUtils from '../utils/LogUtils.js';
import { allowFunction } from '../utils/ScreenshotUtils.js';

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}
let screenHeight = Math.round(Dimensions.get('window').height);
let popupHeight = screenHeight / 2;
let isIphoneX = false;

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}


class Trainee_WorkoutsHome extends Component {
    constructor(props) {
        super(props);
        LogUtils.showSlowLog(this);

        // this.trace = null;
        this.state = {
            searchText: '',
            isAlert: false,
            loading: false,
            popuploading: false,
            alertMsg: '',
            resObj: '',
            isPopupVisible: false,
            bounceValue: new Animated.Value(1000),  //This is the initial position of the subview
            confirmSuccess: false,
            freeTrialObj: {},
            buttonState: '',
            freeTrialSuccessTitle: '',
            freeTrialSuccessMessage: '',
            onConfirmClick: false,
            email: '',
            freeTrailProcess: 1,
            emailOtp: '',
            emailSuccessTitle: '',
            emailSuccessDesc: '',
        };
    }

    async componentDidMount() {
        allowFunction();
        isIphoneX = DeviceInfo.hasNotch();
        LogUtils.infoLog1('isIphoneX', isIphoneX);
        LogUtils.infoLog1('popupHeight', popupHeight);

        if (popupHeight < 370) {
            popupHeight = 370;
        }

        NetInfo.fetch().then(state => {
            if (state.isConnected) {

                this.setState({ loading: true });
                this.getAllPrograms();
            }
            else {

                this.setState({ isAlert: true, alertMsg: 'Please enable your internet connection and try again' });
            }
        });

        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    async componentWillUnmount() {
        // Stop the trace
        //await this.trace.stop();

        this.backHandler.remove();
    }




    async getAllPrograms() {
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/getworkoutshome`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false, resObj: data.data, });

                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
            });
    }



    FlatListItemSeparator = () => {
        return (
            <View
                style={{
                    height: 2,
                    width: -5,
                    backgroundColor: "transparent",
                }}
            />
        );
    }


    onBackPressed() {

        if (this.state.isPopupVisible) {
            this.setState({ isPopupVisible: false });
        } else {
            // Actions.thome();
            // Actions.popTo('traineeHome');
            if (this.props.isRefresh === 'yes') {
                Actions.traineeHome();
            }
            else if (this.props.from === 'rewards' || this.props.from === 'feed') {
                Actions.pop();
            }
            else {
                Actions.popTo('traineeHome');
            }
        }
    }

    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
        this.setState({ isAlert: false, alertMsg: '' });
    }

    onWorkoutClick(item) {
        LogUtils.firebaseEventLog('click', {
            p_id: item.pd_id,
            p_category: 'Workouts',
            p_name: `Workouts - ${item.title}`,
        });
        Actions.traPlanIntro({ pdObj: item, from: 'workouts', pId: item.p_id });
    }

    renderFreeorPremium(item) {
        if (item.pd_type === 1) {
            return (
                <View style={{
                    flexDirection: 'column',
                    alignItems: 'center',
                    alignContent: 'flex-end',
                    margin: 10,
                    right: 0,
                    position: 'absolute',
                    justifyContent: 'center',
                }}>
                    {/* <LinearGradient
                colors={['#ffffff', '#ffffff']}
                style={{
                  width: 30,
                  height: 30,
                  borderRadius: 15,
                  alignSelf: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}> */}
                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={require('../res/ic_free.png')}
                        style={{
                            width: 30,
                            height: 30,
                            alignSelf: 'center',
                            // tintColor: '#ffffff'
                        }}
                    />
                    {/* </LinearGradient> */}
                </View>
            );
        }
        else if (item.pd_type === 2) {
            return (
                <View style={{
                    flexDirection: 'column',
                    alignItems: 'center',
                    alignContent: 'flex-end',
                    margin: 10,
                    right: 0,
                    position: 'absolute',
                    justifyContent: 'center',
                }}>
                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={require('../res/ic_premium.png')}
                        style={{
                            width: 30,
                            height: 30,
                            alignSelf: 'center',
                            // tintColor: '#ffffff'
                        }}
                    />
                </View>
            );
        }
        else {
            return (
                <View>
                </View>
            );
        }
    }

    renderFlatlist() {
        if (!isEmpty(this.state.resObj)) {
            if (Array.isArray(this.state.resObj) && this.state.resObj.length) {
                return (
                    <View>


                        <FlatList
                            showsHorizontalScrollIndicator={false}
                            data={this.state.resObj}
                            keyExtractor={item => "resObj"+item.type_id}
                            ItemSeparatorComponent={this.FlatListItemSeparator}
                            renderItem={({ item }) => {
                                return <View>

                                    {this.renderNewWorkouts(item)}
                                </View>
                            }} />

                    </View>
                );
            }
            else {
                return (
                    <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                    </View>
                );
            }
        }
    }

    renderInnerListItem(innerItem, item) {
        // LogUtils.infoLog1('Item', item);
        if (innerItem.type_id === 1) {

            return <TouchableWithoutFeedback onPress={() => {

                this.onWorkoutClick(item);
            }}>
                <View style={styles.containerOurTrainers}>
                    {item.pd_img
                        ? (
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={{ uri: item.pd_img }}
                                style={{
                                    width: wp('87%'),
                                    height: undefined,
                                    borderRadius: 7,
                                    aspectRatio: 17 / 10,
                                }}
                            />
                        )
                        : (
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_noimage.png')}
                                style={{
                                    width: wp('87%'),
                                    height: 100,
                                    borderRadius: 7,
                                    opacity: 0.9,
                                    resizeMode: 'cover',
                                }}
                            />

                        )
                    }
                    {this.renderFreeorPremium(item)}
                    <View style={{
                        flexDirection: 'column',
                        alignContent: 'flex-start',
                        marginTop: 10,
                        justifyContent: 'flex-start',
                    }}>

                        <Text style={styles.textWorkNamePop} numberOfLines={1}>{item.title}</Text>
                        <Text style={styles.textTitle} numberOfLines={1}>{`${item.tr_name}`}</Text>
                        <Text numberOfLines={1} style={styles.textEqpNeed}>{`${item.fitness_form} - ${item.duration}`}</Text>
                    </View>
                </View>
            </TouchableWithoutFeedback>

        } else if (innerItem.type_id === 2) {

            return <TouchableWithoutFeedback onPress={() => {

                this.onWorkoutClick(item);
            }}>
                <View style={styles.containerOurTrainers}>
                    {item.home_img
                        ? (
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={{ uri: item.home_img }}
                                style={{
                                    width: wp('30%'),
                                    height: undefined,
                                    borderRadius: 7,
                                    aspectRatio: 5 / 9,
                                }}
                            />
                        )
                        : (
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_noimage.png')}
                                style={{
                                    width: wp('30%'),
                                    height: 100,
                                    borderRadius: 7,
                                    opacity: 0.9,
                                    resizeMode: 'cover',
                                }}
                            />

                        )
                    }
                    {this.renderFreeorPremium(item)}
                    <View style={{
                        width: 100,
                        flexDirection: 'column',
                        alignContent: 'flex-start',
                        marginTop: 10,
                        justifyContent: 'flex-start',
                    }}>

                        <Text style={styles.textWorkNamePop} numberOfLines={1}>{item.fitness_form}</Text>
                        <Text style={styles.textTitle} numberOfLines={1}>{`${item.tr_name}`}</Text>
                        <Text numberOfLines={1} style={styles.textEqpNeed}>{`${item.duration}`}</Text>
                    </View>
                </View>

            </TouchableWithoutFeedback>

        } else if (innerItem.type_id === 3) {

            return <TouchableWithoutFeedback onPress={() => {

                this.onWorkoutClick(item);
            }}>
                <View style={styles.containerOurTrainers}>
                    {item.pd_img
                        ? (
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={{ uri: item.pd_img }}
                                style={{
                                    width: wp('60%'),
                                    height: undefined,
                                    borderRadius: 7,
                                    aspectRatio: 17 / 10,
                                }}
                            />
                        )
                        : (
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_noimage.png')}
                                style={{
                                    width: wp('60%'),
                                    height: 100,
                                    borderRadius: 7,
                                    opacity: 0.9,
                                    resizeMode: 'cover',
                                }}
                            />

                        )
                    }
                    {this.renderFreeorPremium(item)}
                    <View style={{
                        width: wp('60%'),
                        flexDirection: 'column',
                        alignContent: 'flex-start',
                        marginTop: 10,
                        justifyContent: 'flex-start',
                    }}>

                        <Text style={styles.textWorkNamePop} numberOfLines={1}>{item.title}</Text>
                        <Text style={styles.textTitle} numberOfLines={1}>{`${item.tr_name}`}</Text>
                        <Text numberOfLines={1} style={styles.textEqpNeed}>{`${item.fitness_form} - ${item.duration}`}</Text>
                    </View>
                </View>

            </TouchableWithoutFeedback>

        } else if (innerItem.type_id === 4) {

            return <TouchableWithoutFeedback onPress={() => {

                this.onWorkoutClick(item);
            }}>
                <View style={styles.containerOurTrainers}>
                    {item.home_img
                        ? (
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={{ uri: item.home_img }}
                                style={{
                                    width: wp('31%'),
                                    height: 130,
                                    borderRadius: 7,
                                }}
                            />
                        )
                        : (
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_noimage.png')}
                                style={{
                                    width: wp('31%'),
                                    height: 130,
                                    borderRadius: 7,
                                    opacity: 0.9,
                                    resizeMode: 'cover',
                                }}
                            />

                        )
                    }
                    <View style={{
                        width: wp('30%'),
                        flexDirection: 'column',
                        alignContent: 'flex-start',
                        marginTop: 10,
                        justifyContent: 'flex-start',
                    }}>

                        <Text style={styles.textWorkNamePop} numberOfLines={1}>{item.fitness_form}</Text>
                        <Text style={styles.textTitle} numberOfLines={1}>{`${item.tr_name}`}</Text>
                        <Text numberOfLines={1} style={styles.textEqpNeed}>{`${item.duration}`}</Text>
                    </View>

                </View>

            </TouchableWithoutFeedback>

        } else if (innerItem.type_id === 5) {

            return <TouchableWithoutFeedback onPress={() => {

                this.onWorkoutClick(item);
            }}>
                <View style={styles.containerOurTrainers}>
                    {item.pd_img
                        ? (
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={{ uri: item.pd_img }}
                                style={{
                                    width: wp('60%'),
                                    height: undefined,
                                    borderRadius: 7,
                                    aspectRatio: 17 / 10,
                                }}
                            />
                        )
                        : (
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_noimage.png')}
                                style={{
                                    width: wp('60%'),
                                    height: 100,
                                    borderRadius: 7,
                                    opacity: 0.9,
                                    resizeMode: 'cover',
                                }}
                            />

                        )
                    }
                    {this.renderFreeorPremium(item)}
                    <View style={{
                        width: wp('60%'),
                        flexDirection: 'column',
                        alignContent: 'flex-start',
                        marginTop: 10,
                        justifyContent: 'flex-start',
                    }}>

                        <Text style={styles.textWorkNamePop} numberOfLines={1}>{item.title}</Text>
                        <Text style={styles.textTitle} numberOfLines={1}>{`${item.tr_name}`}</Text>
                        <Text numberOfLines={1} style={styles.textEqpNeed}>{`${item.fitness_form} - ${item.duration}`}</Text>
                    </View>
                </View>

            </TouchableWithoutFeedback>

        } else if (innerItem.type_id === 6 || innerItem.type_id === 7) {

            return <TouchableWithoutFeedback onPress={() => {

                this.onWorkoutClick(item);
            }}>
                <View style={styles.containerOurTrainers}>
                    {item.pd_img
                        ? (
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={{ uri: item.pd_img }}
                                style={{
                                    width: wp('60%'),
                                    height: undefined,
                                    borderRadius: 7,
                                    aspectRatio: 17 / 10,
                                }}
                            />
                        )
                        : (
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_noimage.png')}
                                style={{
                                    width: wp('60%'),
                                    height: 100,
                                    borderRadius: 7,
                                    opacity: 0.9,
                                    resizeMode: 'cover',
                                }}
                            />

                        )
                    }
                    {this.renderFreeorPremium(item)}
                    <View style={{
                        width: wp('60%'),
                        flexDirection: 'column',
                        alignContent: 'flex-start',
                        marginTop: 10,
                        justifyContent: 'flex-start',
                    }}>

                        <Text style={styles.textWorkNamePop} numberOfLines={1}>{item.title}</Text>
                        <Text style={styles.textTitle} numberOfLines={1}>{`${item.tr_name}`}</Text>
                        <Text numberOfLines={1} style={styles.textEqpNeed}>{`${item.fitness_form} - ${item.duration}`}</Text>
                    </View>
                </View>

            </TouchableWithoutFeedback>
        }
    }


    renderTrendingWorkouts() {
        if (!isEmpty(this.state.resObj)) {
            if (Array.isArray(this.state.resObj.workouts) && this.state.resObj.workouts.length) {
                return (
                    <View>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                            <Text style={styles.textValue}>Trending workouts</Text>
                            <TouchableOpacity
                                onPress={() => {
                                    Actions.workoutsmore({ mFrom: 'Trending Workouts', wType: 2 });
                                }}>
                                <Text style={styles.textViewAll}>{'MORE'.toUpperCase()}</Text>
                            </TouchableOpacity>

                        </View>
                        <View style={{ width: wp('90%'), height: 10, }}></View>

                        <FlatList
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            data={this.state.resObj.workouts}
                            keyExtractor={item => "workouts"+item.pd_id}
                            ItemSeparatorComponent={this.FlatListItemSeparator}
                            renderItem={({ item }) => {
                                return <TouchableOpacity style={styles.containerOurTrainers} onPress={() => {
                                    this.onWorkoutClick(item);

                                }}>
                                    <View>
                                        {item.pd_img
                                            ? (
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={{ uri: item.pd_img }}
                                                    style={{
                                                        width: wp('87%'),
                                                        height: undefined,
                                                        borderRadius: 7,
                                                        aspectRatio: 17 / 10,
                                                    }}
                                                />
                                            )
                                            : (
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_noimage.png')}
                                                    style={{
                                                        width: wp('87%'),
                                                        height: 100,
                                                        borderRadius: 7,
                                                        opacity: 0.9,
                                                        resizeMode: 'cover',
                                                    }}
                                                />

                                            )
                                        }
                                        {this.renderFreeorPremium(item)}
                                        <View style={{
                                            flexDirection: 'column',
                                            alignContent: 'flex-start',
                                            marginTop: 10,
                                            justifyContent: 'flex-start',
                                        }}>

                                            <Text style={styles.textWorkNamePop} numberOfLines={1}>{item.title}</Text>
                                            <Text style={styles.textTitle} numberOfLines={1}>{`${item.level}`}</Text>
                                            <Text numberOfLines={1} style={styles.textEqpNeed}>{`${item.duration}`}</Text>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            }} />

                    </View>
                );
            }
            else {
                return (
                    <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                    </View>
                );
            }
        }
    }

    renderNewWorkouts(innertItem) {
        // if (!isEmpty(this.state.resObj)) {
        if (Array.isArray(innertItem.workouts) && innertItem.workouts.length) {
            return (
                <View>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                        <Text style={styles.textValue}>{`${innertItem.type_name}`}</Text>
                        <TouchableOpacity
                            onPress={() => {
                                Actions.workoutsmore({ mFrom: innertItem.type_name, wType: innertItem.type_id });
                            }}>
                            <Text style={styles.textViewAll}>{'MORE'.toUpperCase()}</Text>
                        </TouchableOpacity>

                    </View>
                    <View style={{ width: wp('90%'), height: 10, }}></View>

                    <FlatList
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                        data={innertItem.workouts}
                        keyExtractor={(item,index) => index+"workouts"}
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        renderItem={({ item }) =>
                            this.renderInnerListItem(innertItem, item)
                        } />
                </View>
            );
        }
        else {
            return (
                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                </View>
            );
        }
        // }
    }

    renderFullBodyWorkouts() {
        if (!isEmpty(this.state.resObj)) {
            if (Array.isArray(this.state.resObj.fullbody_workouts) && this.state.resObj.fullbody_workouts.length) {
                return (
                    <View>
                        <View style={{ width: wp('90%'), height: 10, }}></View>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                            <Text style={styles.textValue}>Full Body Workouts</Text>
                            <TouchableOpacity
                                onPress={() => {
                                    Actions.workoutsmore({ mFrom: 'Full Body Workouts', wType: 3 });
                                }}>
                                <Text style={styles.textViewAll}>{'MORE'.toUpperCase()}</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ width: wp('90%'), height: 10, }}></View>
                        <FlatList
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            data={this.state.resObj.fullbody_workouts}
                            keyExtractor={item => "fullbody_workouts"+item.pd_id}
                            ItemSeparatorComponent={this.FlatListItemSeparator}
                            renderItem={({ item }) => {
                                return <TouchableOpacity style={styles.containerOurTrainers} onPress={() => {

                                    this.onWorkoutClick(item);
                                }}>
                                    <View>
                                        {item.pd_img
                                            ? (
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={{ uri: item.pd_img }}
                                                    style={{
                                                        width: wp('60%'),
                                                        height: undefined,
                                                        borderRadius: 7,
                                                        aspectRatio: 17 / 10,
                                                    }}
                                                />
                                            )
                                            : (
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_noimage.png')}
                                                    style={{
                                                        width: wp('60%'),
                                                        height: 100,
                                                        borderRadius: 7,
                                                        opacity: 0.9,
                                                        resizeMode: 'cover',
                                                    }}
                                                />

                                            )
                                        }
                                        {this.renderFreeorPremium(item)}
                                        <View style={{
                                            width: wp('60%'),
                                            flexDirection: 'column',
                                            alignContent: 'flex-start',
                                            marginTop: 10,
                                            justifyContent: 'flex-start',
                                        }}>

                                            <Text style={styles.textWorkNamePop} numberOfLines={1}>{item.title}</Text>
                                            <Text style={styles.textTitle} numberOfLines={1}>{`${item.level}`}</Text>
                                            <Text numberOfLines={1} style={styles.textEqpNeed}>{`${item.duration}`}</Text>
                                        </View>
                                    </View>


                                </TouchableOpacity>
                            }} />


                    </View>
                );
            }
            else {
                return (
                    <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                    </View>
                );
            }
        }
    }

    renderBeginnerWorkouts() {
        if (!isEmpty(this.state.resObj)) {
            if (Array.isArray(this.state.resObj.beginner_workouts) && this.state.resObj.beginner_workouts.length) {
                return (
                    <View>
                        <View style={{ width: wp('90%'), height: 10, }}></View>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                            <Text style={styles.textValue}>Free Workouts</Text>
                            <TouchableOpacity
                                onPress={() => {
                                    Actions.workoutsmore({ mFrom: 'Free Workouts', wType: 4 });
                                }}>
                                <Text style={styles.textViewAll}>{'MORE'.toUpperCase()}</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ width: wp('90%'), height: 10, }}></View>

                        <FlatList
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            data={this.state.resObj.beginner_workouts}
                            keyExtractor={item => "beginner_workouts"+item.pd_id}
                            ItemSeparatorComponent={this.FlatListItemSeparator}
                            renderItem={({ item }) => {
                                return <TouchableOpacity style={styles.containerOurTrainers} onPress={() => {

                                    this.onWorkoutClick(item);
                                }}>
                                    <View>
                                        {item.home_img
                                            ? (
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={{ uri: item.home_img }}
                                                    style={{
                                                        width: wp('31%'),
                                                        height: 130,
                                                        borderRadius: 7,
                                                    }}
                                                />
                                            )
                                            : (
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_noimage.png')}
                                                    style={{
                                                        width: wp('31%'),
                                                        height: 130,
                                                        borderRadius: 7,
                                                        opacity: 0.9,
                                                        resizeMode: 'cover',
                                                    }}
                                                />

                                            )
                                        }
                                        <View style={{
                                            width: wp('30%'),
                                            flexDirection: 'column',
                                            alignContent: 'flex-start',
                                            marginTop: 10,
                                            justifyContent: 'flex-start',
                                        }}>

                                            <Text style={styles.textWorkNamePop} numberOfLines={1}>{item.title}</Text>
                                            <Text style={styles.textTitle} numberOfLines={1}>{`${item.level}`}</Text>
                                            <Text numberOfLines={1} style={styles.textEqpNeed}>{`${item.duration}`}</Text>
                                        </View>

                                    </View>


                                </TouchableOpacity>
                            }} />



                    </View>
                );
            }
            else {
                return (
                    <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                    </View>
                );
            }
        }
    }



    renderIntermediateAdvancedWorkouts() {
        if (!isEmpty(this.state.resObj)) {
            if (Array.isArray(this.state.resObj.advanced_workouts) && this.state.resObj.advanced_workouts.length) {
                return (
                    <View>
                        <View style={{ width: wp('90%'), height: 10, }}></View>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                            <Text style={styles.textValue}>Premium Workouts</Text>
                            <TouchableOpacity
                                onPress={() => {
                                    Actions.workoutsmore({ mFrom: 'Premium Workouts', wType: 5 });
                                }}>
                                <Text style={styles.textViewAll}>{'MORE'.toUpperCase()}</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ width: wp('90%'), height: 10, }}></View>


                        <FlatList
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            data={this.state.resObj.advanced_workouts}
                            keyExtractor={item => "advanced_workouts"+item.pd_id}
                            ItemSeparatorComponent={this.FlatListItemSeparator}
                            renderItem={({ item }) => {
                                return <TouchableOpacity style={styles.containerOurTrainers} onPress={() => {

                                    this.onWorkoutClick(item);
                                }}>
                                    <View>
                                        {item.pd_img
                                            ? (
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={{ uri: item.pd_img }}
                                                    style={{
                                                        width: wp('60%'),
                                                        height: undefined,
                                                        borderRadius: 7,
                                                        aspectRatio: 16 / 9,
                                                    }}
                                                />
                                            )
                                            : (
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_noimage.png')}
                                                    style={{
                                                        width: wp('60%'),
                                                        height: 100,
                                                        borderRadius: 7,
                                                        opacity: 0.9,
                                                        resizeMode: 'cover',
                                                    }}
                                                />

                                            )
                                        }
                                        <View style={{
                                            width: wp('45%'),
                                            flexDirection: 'column',
                                            alignContent: 'flex-start',
                                            marginTop: 10,
                                            justifyContent: 'flex-start',
                                        }}>

                                            <Text style={styles.textWorkNamePop} numberOfLines={1}>{item.title}</Text>
                                            <Text style={styles.textTitle} numberOfLines={1}>{`${item.level}`}</Text>
                                            <Text numberOfLines={1} style={styles.textEqpNeed}>{`${item.duration}`}</Text>
                                        </View>
                                    </View>


                                </TouchableOpacity>
                            }} />

                    </View>
                );
            } else {
                return (
                    <View></View>
                );
            }
        }
    }





    render() {
        return (
            <SafeAreaView style={styles.mainContainer}>
                {/* <View style={styles.mainContainer}> */}
                <Loader loading={this.state.loading} />

                <View style={{
                    flexDirection: 'row',
                    margin: 20,
                }}>
                    <View style={{ position: 'absolute', zIndex: 111 }}>
                        <TouchableOpacity
                            style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                            onPress={() => this.onBackPressed()}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_back.png')}
                                style={styles.backImageStyle}
                            />
                        </TouchableOpacity>
                    </View>

                    <Text style={styles.textHeadTitle}>Workouts</Text>
                </View>

                <ScrollView contentContainerStyle={{ paddingBottom: hp('10%') }}>

                    {this.renderFlatlist()}

                    {/* {this.renderTrendingWorkouts()}
                    {this.renderNewWorkouts()}
                    {this.renderFullBodyWorkouts()}
                    {this.renderBeginnerWorkouts()}
                    {this.renderIntermediateAdvancedWorkouts()} */}




                </ScrollView>

                <CustomDialog
                    visible={this.state.isAlert}
                    title='Alert'
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    no=''
                    yes='Ok' />
                {/* </View> */}
            </SafeAreaView>
        );
    }


}

const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: '#ffffff',
        flex: 1,
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'center',
    },
    textValue: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        marginLeft: 10,
        color: '#282c37',
    },
    textWorkName: {
        fontSize: 13,
        fontWeight: '500',
        flexShrink: 2,
        fontFamily: 'Rubik-Medium',
        color: '#000000',
        alignSelf: 'flex-start',
        marginRight: 3,
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    textOurTrainer: {
        fontSize: 14,
        fontWeight: '500',
        flexShrink: 2,
        textAlign: 'left',
        fontFamily: 'Rubik-Medium',
        color: '#ffffff',
        alignSelf: 'flex-start',
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    textMore: {
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontSize: 10,
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 5,
        paddingRight: 5,
        alignSelf: 'center',
        textAlign: 'center',
        fontWeight: '500',
        lineHeight: 18,
    },
    textViewAll: {
        color: '#8c52ff',
        fontFamily: 'Rubik-Medium',
        fontSize: 12,
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 5,
        paddingRight: 5,
        alignSelf: 'center',
        textAlign: 'center',
        fontWeight: '500',
        lineHeight: 18,
        marginRight: 10
    },
    textMainTrainer: {
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontSize: 8,
        paddingTop: 3,
        paddingBottom: 3,
        paddingLeft: 10,
        paddingRight: 10,
        alignSelf: 'center',
        textAlign: 'center',
        fontWeight: '500',
    },
    textRecTrainer: {
        fontSize: 9,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        backgroundColor: '#8c52ff',
        borderRadius: 5,
        paddingTop: 2,
        paddingBottom: 2,
        paddingLeft: 6,
        paddingRight: 6,
        margin: 5,
        textAlign: 'left',
        alignSelf: 'flex-start',
        color: '#ffffff',
    },
    textWorkNamePop: {
        fontSize: 12,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        color: '#000000',
        flexDirection: 'row',
        lineHeight: 18
    },
    textTitle: {
        fontSize: 10,
        fontWeight: '400',
        fontFamily: 'Rubik-Regular',
        // lineHeight: 14,
        color: '#000000',
    },
    textLevel: {
        fontSize: 12,
        fontWeight: '500',
        fontFamily: 'Rubik-Regular',
        lineHeight: 18,
        color: '#000000',
        marginTop: 2,
        justifyContent: 'flex-start',
        textAlign: 'left',
    },
    textFocusArea: {
        fontSize: 12,
        fontWeight: '500',
        fontFamily: 'Rubik-Regular',
        lineHeight: 18,
        color: '#000000',
        marginTop: 2,
        justifyContent: 'flex-start',
        textAlign: 'left',
    },
    textTimeDuration: {
        fontSize: 12,
        fontWeight: '500',
        fontFamily: 'Rubik-Regular',
        lineHeight: 18,
        color: '#000000',
        marginTop: 2,
        justifyContent: 'flex-start',
        textAlign: 'left',
    },
    textEqpNeed: {
        fontSize: 10,
        fontWeight: '400',
        fontFamily: 'Rubik-Regular',
        // lineHeight: 14,
        color: '#000000',
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        // paddingTop: (DeviceInfo.getModel() === ('iPhone 11 Pro Max' || 'iPhone 11' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR')) ? 30 : 0,
        // paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    viewFollow: {
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        marginTop: 3,
        alignContent: 'center',
        justifyContent: 'flex-start',
    },
    textSub: {
        fontSize: 10,
        fontWeight: '500',
        fontFamily: 'Rubik-Regular',
        backgroundColor: '#8c52ff',
        borderRadius: 5,
        paddingTop: 1,
        paddingBottom: 1,
        paddingLeft: 6,
        paddingRight: 6,
        bottom: 5,
        textAlign: 'left',
        alignSelf: 'flex-start',
        color: '#ffffff',
    },
    image: {
        width: 60,
        height: 60,
    },
    containerListStyle: {
        width: 220,
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'column',
        borderColor: '#ddd',
        // borderRadius: 15,
        borderRadius: 10,
        position: 'relative',
        margin: 10,
        justifyContent: 'flex-start',
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    containerOurTrainers: {
        // backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'column',
        borderColor: '#ddd',
        // borderRadius: 15,
        borderRadius: 10,
        position: 'relative',
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 10,
        // shadowColor: '#4075cd',
        // shadowOffset: { width: 0, height: 2 },
        // shadowOpacity: 0.25,
        // shadowRadius: 3.84,
        // elevation: 5
    },
    containerFitnessform: {
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'column',
        borderColor: '#ddd',
        // borderRadius: 15,
        borderRadius: 10,
        position: 'relative',
        marginLeft: 5,
        marginRight: 5,
        marginBottom: 5,
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    linearGradient: {
        width: '18%',
        borderRadius: 15,
        marginRight: 10,
        justifyContent: 'center',
        alignSelf: 'center',
        alignSelf: "flex-end",
        alignItems: 'center',
    },
    linearGradientViewPlans: {
        width: '90%',
        height: 50,
        bottom: 0,
        borderRadius: 25,
        justifyContent: 'center',
        position: 'absolute',
        alignSelf: 'center',
        marginBottom: 15,
    },
    disFlatList: {
        marginVertical: 5,
    },
    item: {
        backgroundColor: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1, // approximate a square
        margin: 5,
        borderRadius: 7,
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    textTraDiscover: {
        fontSize: 14,
        fontWeight: '500',
        flexShrink: 2,
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
        color: '#ffffff',
        alignSelf: 'center',
        flexWrap: 'wrap',
    },
    imgpopup: {
        width: 200,
        height: 200,
        alignSelf: 'center',
    },
    textNodata: {
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
    },
    textNodataTitle: {
        fontSize: 16,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
    },
    textSave: {
        width: wp('40%'),
        fontSize: 14,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        borderRadius: 15,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 8,
        paddingBottom: 8,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#ffffff',
    },
    linearGradientEnroll: {
        borderRadius: 15,
        justifyContent: 'center',
        alignSelf: 'center',
        alignSelf: "center",
        alignItems: 'center',
        marginTop: 20,
        // position:'absolute',
        // bottom:0,
    },
    linearGradientBottom: {
        borderRadius: 15,
        justifyContent: 'center',
        alignSelf: 'center',
        alignSelf: "center",
        alignItems: 'center',
        marginTop: 20,
        position: 'absolute',
        bottom: 0,
    },
    chaProfile: {
        width: 23,
        height: 23,
        aspectRatio: 1,
        backgroundColor: "#D8D8D8",
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: "#979797",
        borderRadius: 23 / 2,
        marginRight: 2,
        resizeMode: "cover",
        justifyContent: 'center',
    },
    chaMore: {
        flexDirection: 'row',
        backgroundColor: '#979797',
        borderRadius: 15,
        paddingLeft: 8,
        paddingTop: 4,
        marginTop: 5,
        alignSelf: 'center',
        paddingBottom: 4,
        paddingRight: 8,
    },
    chaMoreText: {
        color: '#ffffff',
        fontSize: 12,
        textAlign: 'center',
        alignSelf: 'center',
        fontFamily: 'Rubik-Regular',
        backgroundColor: '#979797',
        borderRadius: 10,
        paddingLeft: 4,
        paddingTop: 2,
        alignSelf: 'center',
        paddingBottom: 2,
        paddingRight: 4,
        overflow: 'hidden',
    },
    textForm: {
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontSize: 10,
        paddingTop: 3,
        paddingBottom: 3,
        paddingLeft: 5,
        paddingRight: 10,
        alignSelf: 'center',
        textAlign: 'left',
        fontWeight: '500',
    },
    buttonText: {
        fontSize: 14,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    buttonTuch:
    {
        backgroundColor: 'transparent',
    },
    vBestTrainer: {
        width: wp('30%'),
        backgroundColor: '#ffffff',
        flexDirection: 'row',
        marginTop: hp('0.1%'),
        marginBottom: hp('1.1%'),
        marginRight: wp('1%'),
        marginLeft: wp('1%'),
        borderColor: '#ddd',
        borderRadius: 5,
        position: 'relative',
        alignSelf: 'center',
        shadowColor: "#fff",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    imgTrainer: {
        width: wp('30%'),
        // height: 130,
        // alignSelf: 'center',
        // position: 'relative',
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        height: undefined,
        resizeMode: 'contain',
        aspectRatio: 5 / 6,
    },
    textTrName: {
        // height: 25,
        fontSize: 9,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        color: '#2d3142',
        textAlign: 'center',
        alignSelf: 'center',
        alignContent: 'center',
        // flex: 1,
    },
    textSpecial: {
        fontSize: 7,
        fontWeight: '500',
        color: '#8c52ff',
        letterSpacing: 0.23,
        marginTop: 4,
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
        alignSelf: 'center',
    },
    subView: {
        position: "absolute",
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: "#FFFFFF",
        height: 200,
    },
    textPopTitle: {
        fontSize: 14,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
        marginTop: 5,
    },
    textPopDesc: {
        fontSize: 12,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 16,
        marginLeft: 8,
        padding: 20,
    },
    freeTrialPopupTitle: {
        width: wp('85%'),
        fontSize: 14,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
        marginTop: 10
    },
    freeTrialPopupText: {
        fontSize: 12,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
        marginTop: 5,
        marginLeft: 5,
        marginRight: 5,
        marginBottom: 15,
    },
    containerMobileStyle: {
        borderBottomWidth: 1,
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 15,
        position: 'relative',
        alignSelf: 'center',
        marginLeft: 20,
        marginRight: 20,
        marginTop: 10,
        marginBottom: 20,
        padding: 8,
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    textInputStyle: {
        height: 40,
        flex: 1,
        fontSize: 13,
        marginLeft: 10,
        fontFamily: 'Rubik-Regular',
        color: '#2d3142',
    },
});

const mapStateToProps = state => {
    return {};
};

export default connect(
    mapStateToProps,
    {
    },
)(Trainee_WorkoutsHome);
