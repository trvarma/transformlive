import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    BackHandler,
    FlatList,
    ImageBackground,
    Linking,
    Platform,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { NoInternet, CustomDialog, Loader } from './common';
import { withNavigationFocus } from 'react-navigation';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { BASE_URL } from '../actions/types';
import GoogleFit, { Scopes } from 'react-native-google-fit';
import moment from "moment";
import AppleHealthKit from 'rn-apple-healthkit';
import DeviceInfo from 'react-native-device-info';
import LogUtils from '../utils/LogUtils.js';
import { allowFunction } from '../utils/ScreenshotUtils.js';

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}
let data, fitbit_disconnected = '0';
let stepsArray = [], caloriesArray = [], distanceArray = [];
var context;
import qs from 'qs';
let fitbitToken = "", trackId = '', token, fitbitUserId = '', fitbitUserWeight = '';
let isServiceCalled = false;
import config from '../config.js';
function OAuth(client_id) {

    const oauthurl = `https://www.fitbit.com/oauth2/authorize?${qs.stringify({
        client_id,
        response_type: 'token',
        scope: 'heartrate activity activity profile sleep weight',
        redirect_uri: 'fitness://trackdevices',
        expires_in: '31536000',
    })}`;
    LogUtils.infoLog(oauthurl);
    Linking.openURL(oauthurl).catch(err => console.error('Error processing linking', err));
}

function getData(access_token, user_id) {

    // fetch('https://api.fitbit.com/1/user/-/activities/list.json?afterDate=2020-10-13&sort=asc&offset=0&limit=10', {
    // fetch('https://api.fitbit.com/1/user/-/activities/date/2020-09-07.json', {
    fetch('https://api.fitbit.com/1/user/-/activities/tracker/steps/date/today/7d.json', {
        // fetch('https://api.fitbit.com/1/user/-/activities/list.json?afterDate=2020-09-11&sort=desc&offset=3&limit=10', {
        method: 'GET',
        headers: {
            Authorization: `Bearer ${access_token}`,
        },
        // body: `root=auto&path=${Math.random()}`
    })
        .then(res => res.json())
        .then(res => {

            LogUtils.infoLog1(`res:`, res);
            stepsArray = [];
            caloriesArray = [];
            res["activities-tracker-steps"].map((item) => {

                stepsArray.push({ 'act': 'summary', 'from': item.dateTime, 'to': item.dateTime, 'steps': item.value, 'cals': 0, 'dist': 0 });
                // stepsArray.push({ 'act': 'summary', 'c_date': item.dateTime, 'steps': item.value,'cals' : 0 });

            })
            LogUtils.infoLog1("Steps Array", stepsArray);
            if (Array.isArray(stepsArray) && stepsArray.length) {



                LogUtils.infoLog1("body", stepsArray);
                // sendStepsData(data);

                getFitbitCaloriesData(fitbitToken, stepsArray);

            }

        })
        .catch(err => {
            console.error('Error: ', err);
        });
}

function getFitbitCaloriesData(access_token, data) {

    fetch('https://api.fitbit.com/1/user/-/activities/tracker/activityCalories/date/today/7d.json', {
        method: 'GET',
        headers: {
            Authorization: `Bearer ${access_token}`,
        },
        // body: `root=auto&path=${Math.random()}`
    })
        .then(res => res.json())
        .then(res => {

            LogUtils.infoLog1(`res:`, res);

            if (data) {
                data.map((dataItem) => {
                    res["activities-tracker-activityCalories"].map((item) => {
                        if (dataItem.from === item.dateTime) {
                            dataItem.cals = item.value
                        }
                    })
                })
            }

            LogUtils.infoLog1(" Calories data", data);
            // if (Array.isArray(caloriesArray) && caloriesArray.length) {

            //     data = JSON.stringify({
            //         td_id: 1,
            //         step_arr: stepsArray,
            //         cal_arr: caloriesArray,
            //         fitbit_token: access_token,
            //     });
            //     LogUtils.infoLog1("body", data);
            //     sendStepsData(data);

            // }

            getFitbitDistanceData(fitbitToken, data);

        })
        .catch(err => {
            console.error('Error: ', err);
        });
}


function getFitbitDistanceData(access_token, data) {

    fetch('https://api.fitbit.com/1/user/-/activities/tracker/distance/date/today/7d.json', {
        method: 'GET',
        headers: {
            Authorization: `Bearer ${access_token}`,
        },
        // body: `root=auto&path=${Math.random()}`
    })
        .then(res => res.json())
        .then(res => {

            LogUtils.infoLog1(`res:`, res);

            if (data) {
                data.map((dataItem) => {
                    res["activities-tracker-distance"].map((item) => {
                        if (dataItem.from === item.dateTime) {
                            dataItem.dist = item.value * 1000
                            dataItem.from = item.dateTime + ' 00:00:00'
                            dataItem.to = item.dateTime + ' 23:59:59'
                        }
                    })
                })
            }

            LogUtils.infoLog1("Final data", data);




            // if (Array.isArray(data) && data.length) {

            //     let body = JSON.stringify({
            //         td_id: 1,
            //         td_unq_id: fitbitUserId,
            //         fitbit_token: access_token,
            //         act_arr: data,
            //         // cal_arr: caloriesArray,

            //     });
            //     LogUtils.infoLog1("body", body);
            //     saveuseractivitytran(body);

            // }
            getFitbitActivitiesData(fitbitToken, data);

        })
        .catch(err => {
            console.error('Error: ', err);
        });
}

function getFitbitActivitiesData(access_token, data) {

    fetch('https://api.fitbit.com/1/user/-/activities/list.json?afterDate=' + moment().subtract(7, 'days').format("YYYY-MM-DD") + '&sort=asc&offset=0&limit=100', {
        method: 'GET',
        headers: {
            Authorization: `Bearer ${access_token}`,
        },
        // body: `root=auto&path=${Math.random()}`
    })
        .then(res => res.json())
        .then(res => {

            LogUtils.infoLog1(`res:`, res);

            if (res) {
                if (res.activities.length > 0) {

                    res.activities.map((item) => {

                        if (item.logType != 'manual') {
                            var fromDate = item.originalStartTime.split('T')[0] + ' ' + item.originalStartTime.split('T')[1].split('.')[0];
                            LogUtils.infoLog1("fromDate", fromDate);
                            var milliseconds = Date.parse(item.originalStartTime); // some mock date
                            LogUtils.infoLog1("milliseconds", milliseconds);

                            var totalMillis = 0;
                            if (milliseconds) {
                                totalMillis = milliseconds + item.originalDuration;
                                LogUtils.infoLog1("totalMillis", totalMillis);
                            }

                            data.push({ 'act': item.activityName, 'from': fromDate, 'to': moment(totalMillis).format("YYYY-MM-DD HH:mm:ss"), 'steps': item.steps, 'cals': item.calories, 'dist': item.distance * 1000 });
                        }

                    })

                }
            }

            LogUtils.infoLog1("Final With Activity Data", data);

            if (Array.isArray(data) && data.length) {

                let body = JSON.stringify({
                    td_id: 1,
                    td_unq_id: fitbitUserId,
                    weight: fitbitUserWeight,
                    fitbit_token: access_token,
                    act_arr: data,
                    // cal_arr: caloriesArray,
                });
                LogUtils.infoLog1("body", body);
                saveuseractivitytran(body);
            }
        })
        .catch(err => {
            console.error('Error: ', err);
        });
}

function getUserProfile(access_token) {


    fetch('https://api.fitbit.com/1/user/-/profile.json', {
        // fetch('https://api.fitbit.com/1/user/-/body/log/weight/date/2020-11-24.json', {

        method: 'GET',
        headers: {
            Authorization: `Bearer ${access_token}`,
        },
        // body: `root=auto&path=${Math.random()}`
    })
        .then(res => res.json())
        .then(res => {
            LogUtils.infoLog1(`res:`, res);
            if (res.user) {
                fitbitUserWeight = res.user.weight;
            }
        })
        .catch(err => {
            console.error('Error: ', err);
        });
}

// function sendStepsData(body) {
//     fetch(
//         `${BASE_URL}/trainee/saveuserstepsnew`,
//         {
//             method: 'POST',
//             headers: {
//                 Accept: 'application/json',
//                 'Content-Type': 'application/json',
//                 Authorization: `Bearer ${token}`,
//             },
//             body: body,
//         },
//     )
//         .then(processResponse)
//         .then(res => {
//             const { statusCode, data } = res;
//             LogUtils.infoLog1('statusCode', statusCode);
//             LogUtils.infoLog1('data', data);
//             if (statusCode >= 200 && statusCode <= 300) {
//                 context.setState({ text: 50 });
//                 if (!isServiceCalled && trackId === '1') {
//                     isServiceCalled = true;
//                     getFitbitCaloriesData(fitbitToken);
//                 }
//             } else {
//                 if (data.message === 'You are not authenticated!') {
//                     // this.setState({ isAlert: true, alertMsg: data.message });
//                 } else {
//                     // this.setState({ isAlert: true, alertMsg: data.message });
//                 }
//             }
//         })
//         .catch(function (error) {
//             // this.setState({ isAlert: true, alertMsg: JSON.stringify(error) });
//         });

// }


function saveuseractivitytran(body) {
    LogUtils.infoLog1('Url', `${BASE_URL}/trainee/saveuseractivitytran`);
    LogUtils.infoLog1("body", body);
    fetch(
        `${BASE_URL}/trainee/saveuseractivitytran`,
        {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: `Bearer ${token}`,
            },
            body: body,
        },
    )
        .then(processResponse)
        .then(res => {
            const { statusCode, data } = res;
            LogUtils.infoLog1('statusCode', statusCode);
            LogUtils.infoLog1('data', data);
            if (statusCode >= 200 && statusCode <= 300) {

            } else {
                if (data.message === 'You are not authenticated!') {
                    // this.setState({ isAlert: true, alertMsg: data.message });
                } else {
                    // this.setState({ isAlert: true, alertMsg: data.message });
                }
            }
        })
        .catch(function (error) {
            // this.setState({ isAlert: true, alertMsg: JSON.stringify(error) });
        });

}

const HKPERMS = AppleHealthKit.Constants.Permissions;
const HKOPTIONS = {
    permissions: {
        read: [
            HKPERMS.StepCount,
            HKPERMS.ActiveEnergyBurned,
            HKPERMS.DistanceWalkingRunning,
            HKPERMS.Workout,
        ],
    }
};

class TrakingScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            text: 50,
            fakeContact: [],
            SelectedFakeContactList: [],
            isAlert: false,
            alertTitle: '',
            alertMsg: '',
            nolabel: '',
            yeslabel: '',
            isSuccess: false,
            sucMsg: '',
            titMsg: '',
            DATA: [
                {
                    id: '1',
                    name: 'Fitbit',
                    caption: 'Fitbit, Inc',
                    check: false,
                    image: require('../res/fitbit.png'),
                },
                {
                    id: '2',
                    name: 'Google Fit',
                    caption: 'Google LLC',
                    check: false,
                    image: require('../res/google-fit.png'),
                },

            ],
            isPermissionAlert: false,
            permissionAlertTitle: 'Permission',
            permissionAlertMsg: 'Permission Denied. If you deny the permission your steps and activity data will not get synced. Would you like to allow it.',
            permissionAlertnolabel: 'No',
            permissionAlertyeslabel: 'Allow',
        };
    }

    async componentDidMount() {
        allowFunction();
        context = this;
        token = await AsyncStorage.getItem('token');
        trackId = await AsyncStorage.getItem('trackId');
        // LogUtils.infoLog1( moment.unix(1600324550697 / 1000).format("YYYY-MM-DD HH:mm:ss"));
        isServiceCalled = false;
        if (!fitbitToken) {
            fitbitToken = await AsyncStorage.getItem('fitbitaccess_token');
        }
        fitbit_disconnected = await AsyncStorage.getItem('fitbit_disconnected');
        this.ShowCurrentDate();
        if (Platform.OS === 'ios') {
            this.state.DATA.map((item) => {
                if (item.id === '2') {
                    item.name = 'HealthKit';
                    item.caption = 'Apple Inc'
                    item.image = require('../res/ic_healthkit.png')
                }
            });
        }
        this.state.DATA.map((item) => {
            if (item.id === trackId) {
                item.check = true;
            } else {
                item.check = false;
            }
        });
        this.setState({ DATA: this.state.DATA });
        this.setState({ text: 50 });
        Linking
            .getInitialURL()
            .then(url => {
                LogUtils.infoLog('url', url);
                this._handleOpenURL({ url });
            })
            .catch(error => console.error(error));
        Linking.addEventListener('url', (url) => {
            this._handleOpenURL(url);
        });
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
        LogUtils.infoLog("Remove Event Listener is called");
        Linking.removeEventListener('url', this._handleOpenURL);
    }

    _handleOpenURL = (url) => {
        LogUtils.infoLog1("URL", url);
        if (url.url != null) {
            if ((trackId === null || trackId === '0') && fitbit_disconnected === '0') {
                this.handleUrl(url);
            }

        }

    };

    async handleUrl(event) {
        let q = 'error=access_denied';
        LogUtils.infoLog(event.url);
        if (event.url.indexOf(q.toLowerCase()) > -1) {
            this.setState({ isPermissionAlert: true, });
        } else {
            const [, query_string] = event.url.match(/\#(.*)/);

            LogUtils.infoLog(query_string);
            const query = qs.parse(query_string);
            LogUtils.infoLog(`query: ${JSON.stringify(query)}`);
            fitbitToken = query.access_token;
            fitbitUserId = query.user_id;
            await AsyncStorage.setItem('fitbitaccess_token', query.access_token);
            await AsyncStorage.setItem('td_unq_id', query.user_id);
            await AsyncStorage.setItem('trackId', '1');
            trackId = await AsyncStorage.getItem('trackId');
            isServiceCalled = false;
            this.state.DATA.map((item) => {
                if (item.id === '1') {
                    item.check = true;
                } else {
                    item.check = false;
                }
            });
            this.setState({ DATA: this.state.DATA });
            getUserProfile(query.access_token);
            getData(query.access_token, query.user_id);
        }


    }



    onBackPressed() {
        if (this.props.from === 'trackhome') {
            Actions.trackershome();
        } else {
            Actions.pop();
        }

    }
    ShowCurrentDate = () => {

        var date = new Date().getDate();
        var month = new Date().getMonth() + 1;
        var year = new Date().getFullYear();

        var currentDate = year + '-' + month + '-' + date;
    }

    async savegFitData() {
        LogUtils.infoLog('saving tracjid >>>');
        await AsyncStorage.setItem('fitbitaccess_token', '');
        await AsyncStorage.setItem('trackId', '2');
        trackId = await AsyncStorage.getItem('trackId');
    }

    async googleFitAuth() {
        let token = await AsyncStorage.getItem('token');
        const options = {
            scopes: [
                Scopes.FITNESS_ACTIVITY_READ,
                // Scopes.FITNESS_ACTIVITY_READ_WRITE,
                Scopes.FITNESS_BODY_READ_WRITE,
                Scopes.FITNESS_LOCATION_READ,
            ],
        }

        LogUtils.infoLog1("GoogleFit.checkIsAuthorized()", GoogleFit.isAuthorized);


        GoogleFit.authorize(options)
            .then((res) => {
                LogUtils.infoLog1('authorized >>>', res)
                if (res.success) {

                    this.savegFitData();
                    LogUtils.infoLog1("TrackId", trackId);

                    this.state.DATA.map((item) => {
                        if (item.id === '2') {
                            item.check = true;
                        } else {
                            item.check = false;
                        }
                    });
                    this.setState({ DATA: this.state.DATA });

                    let options = {
                        startDate: new Date(moment().subtract(7, 'days').format("YYYY-MM-DD")).valueOf(), // simply outputs the number of milliseconds since the Unix Epoch
                        endDate: new Date().valueOf()
                    };


                    GoogleFit.getActivitySamples(options, (err, res) => {
                        LogUtils.infoLog1("err", err);
                        LogUtils.infoLog1("Googlefit res", res);
                        if (res && Array.isArray(res) && res.length) {
                            stepsArray = [];

                            res.map((item) => {

                                if (item.tracked === true) {
                                    var element = { 'act': item.activityName, 'from': moment.unix(item.start / 1000).format("YYYY-MM-DD HH:mm:ss"), 'to': moment.unix(item.end / 1000).format("YYYY-MM-DD HH:mm:ss"), 'steps': 0, 'cals': 0, 'dist': 0 };

                                    if (item.quantity) {

                                        element["steps"] = item.quantity;

                                    } else {
                                        element["steps"] = 0;
                                    }

                                    if (item.calories) {

                                        element["cals"] = item.calories;

                                    } else {
                                        element["cals"] = 0;
                                    }

                                    if (item.distance) {

                                        element["dist"] = item.distance;

                                    } else {
                                        element["dist"] = 0;
                                    }

                                    stepsArray.push(element);

                                }

                            });

                            LogUtils.infoLog1("Googlefit data", stepsArray);

                            let body = JSON.stringify({
                                td_id: 2,
                                td_unq_id: '',
                                fitbit_token: '',
                                act_arr: stepsArray,

                            });

                            saveuseractivitytran(body);

                        }

                    });


                    // const options = {
                    //     // startDate: "2020-03-01T00:00:17.971Z", // required
                    //     startDate: moment().subtract(7, 'days').format("YYYY-MM-DD") + 'T00:00:17.971Z', // required ISO8601Timestamp
                    //     endDate: new Date().toISOString() // required ISO8601Timestamp
                    // };


                    // GoogleFit.getDailyStepCountSamples(options)
                    //     .then((res) => {
                    //         LogUtils.infoLog1('Steps Response >>> ', res)
                    //         if (Array.isArray(res) && res.length) {
                    //             res.map((item) => {
                    //                 if (item.source === 'com.google.android.gms:estimated_steps') {
                    //                     if (Array.isArray(item.steps) && item.steps.length) {
                    //                         stepsArray = [];
                    //                         item.steps.map((stepitem) => {

                    //                             stepsArray.push({ 'c_date': stepitem.date, 'step_cnt': stepitem.value });

                    //                         })
                    //                         LogUtils.infoLog1("Steps Array", stepsArray);
                    //                         if (Array.isArray(stepsArray) && stepsArray.length) {

                    //                             data = JSON.stringify({
                    //                                 td_id: 2,
                    //                                 step_arr: stepsArray,
                    //                                 cal_arr: caloriesArray,
                    //                                 fitbit_token: '',
                    //                             });
                    //                             LogUtils.infoLog1("body", data);
                    //                             sendStepsData(data);

                    //                         }
                    //                     }
                    //                 }
                    //             });

                    //         }

                    //     })
                    //     .catch((err) => { console.warn(err) })

                    // const opt = {
                    //     // startDate: "2020-03-01T00:00:17.971Z", // required
                    //     startDate: moment().subtract(7, 'days').format("YYYY-MM-DD") + 'T00:00:17.971Z', // required
                    //     endDate: new Date().toISOString(), // required
                    //     basalCalculation: true, // optional, to calculate or not basalAVG over the week
                    // };


                    // GoogleFit.getDailyCalorieSamples(opt, (err, res1) => {
                    //     LogUtils.infoLog1("cal res: ", res1);
                    //     if (Array.isArray(res1) && res1.length) {
                    //         caloriesArray = [];
                    //         res1.map((calorieitem) => {

                    //             caloriesArray.push({ 'c_date': calorieitem.startDate.split('T')[0], 'cal_burn': calorieitem.calorie });

                    //         })
                    //         LogUtils.infoLog1("Calorie Array", caloriesArray);
                    //         if (Array.isArray(caloriesArray) && caloriesArray.length) {

                    //             data = JSON.stringify({
                    //                 td_id: 2,
                    //                 step_arr: stepsArray,
                    //                 cal_arr: caloriesArray,
                    //                 fitbit_token: '',
                    //             });
                    //             LogUtils.infoLog1("body", data);
                    //             sendStepsData(data);

                    //         }

                    //     }
                    // });

                }
                else {
                    this.setState({ isAlert: true, alertMsg: res.message, alertTitle: 'Alert', yeslabel: 'Ok', nolabel: '' });
                }

            })
            .catch((err) => {
                LogUtils.infoLog1('err >>> ', err)
            })
    }
    async appleAuth() {

        AppleHealthKit.isAvailable((err, available) => {
            if (available) {
                AppleHealthKit.initHealthKit(HKOPTIONS, (err, results) => {
                    LogUtils.infoLog1("results: ", results);
                    if (err) {
                        LogUtils.infoLog1("error initializing Healthkit: ", err);
                        return;
                    }
                    this.savegFitData();
                    LogUtils.infoLog1("TrackId", trackId);

                    this.state.DATA.map((item) => {
                        if (item.id === '2') {
                            item.check = true;
                        } else {
                            item.check = false;
                        }
                    });
                    this.setState({ DATA: this.state.DATA });

                    stepsArray = [];
                    caloriesArray = [];

                    // this._fetchApple_Workouts();

                    this._fetchStepsToday_Apple();
                    // this._fetchCalories_Apple();

                });
            }
        });
    }


    _fetchStepsToday_Apple() {

        const options = {
            startDate: moment().subtract(7, 'days').format("YYYY-MM-DD") + 'T07:39:49.472Z', // required ISO8601Timestamp
            endDate: new Date().toISOString(), // required ISO8601Timestamp
            // type: 'Workout',
        }

        // LogUtils.infoLog(options.startDate);
        // LogUtils.infoLog(options.endDate);

        // AppleHealthKit.getDailyDistanceWalkingRunningSamples(options, (err, res) => {
        AppleHealthKit.getDailyStepCountSamples(options, (err, res) => {

            if (err) {
                LogUtils.infoLog(err);
                return;
            }
            // LogUtils.infoLog(res);

            if (res && Array.isArray(res) && res.length) {
                let dummyStepsArray = [];

                res.map((item) => {


                    dummyStepsArray.push({ 'act': 'summary', 'from': item.startDate.split('T')[0], 'to': item.endDate.split('T')[0], 'steps': item.value, 'cals': 0, 'dist': 0 });


                    // stepsArray.push(item);




                });

                if (Array.isArray(dummyStepsArray) && dummyStepsArray.length) {
                    let cDate = '';
                    let stepCount = 0;
                    let prevDate = '';
                    for (let i = 0; i < dummyStepsArray.length; ++i) {
                        prevDate = dummyStepsArray[i].from;

                        if (cDate != '' && cDate != prevDate) {
                            // LogUtils.infoLog1("CDate", cDate);
                            // LogUtils.infoLog1("to", dummyStepsArray[i].to);
                            stepsArray.push({ 'act': 'summary', 'from': moment(cDate).format('YYYY-MM-DD'), 'to': moment(cDate).format('YYYY-MM-DD'), 'steps': stepCount, 'cals': 0, 'dist': 0, });

                            cDate = '';
                            stepCount = 0;

                        }
                        cDate = prevDate;
                        prevDate = '';
                        stepCount = stepCount + dummyStepsArray[i].steps;
                        if (i === dummyStepsArray.length - 1) {
                            stepsArray.push({ 'act': 'summary', 'from': moment(cDate).format('YYYY-MM-DD'), 'to': moment(cDate).format('YYYY-MM-DD'), 'steps': stepCount, 'cals': 0, 'dist': 0, });
                        }


                    }

                }

                LogUtils.infoLog1("Health Steps data", stepsArray);

            }





            this._fetchDistance_Apple(stepsArray);
        });
    }

    _fetchDistance_Apple(data) {

        const options = {
            startDate: moment().subtract(7, 'days').format("YYYY-MM-DD") + 'T07:39:49.472Z', // required ISO8601Timestamp
            endDate: new Date().toISOString(), // required ISO8601Timestamp
            // type: 'Workout',
        }

        AppleHealthKit.getDailyDistanceWalkingRunningSamples(options, (err, res) => {
            if (err) {
                LogUtils.infoLog(err);
                return;
            }
            // LogUtils.infoLog(res);

            if (res && Array.isArray(res) && res.length) {
                let dummyDistanceArray = [];

                res.map((item) => {
                    dummyDistanceArray.push({ 'act': 'summary', 'from': item.startDate.split('T')[0], 'to': item.endDate.split('T')[0], 'steps': 0, 'cals': 0, 'dist': item.value });
                    // dummyDistanceArray.push(item);
                });

                if (Array.isArray(dummyDistanceArray) && dummyDistanceArray.length) {
                    let cDate = '';
                    let distance = 0;
                    let prevDate = '';
                    for (let i = 0; i < dummyDistanceArray.length; ++i) {
                        prevDate = dummyDistanceArray[i].from;

                        if (cDate != '' && cDate != prevDate) {
                            // LogUtils.infoLog1("CDate", cDate);
                            // LogUtils.infoLog1("to", dummyDistanceArray[i].to);
                            distanceArray.push({ 'act': 'summary', 'from': moment(cDate).format('YYYY-MM-DD'), 'to': moment(cDate).format('YYYY-MM-DD'), 'steps': 0, 'cals': 0, 'dist': distance, });

                            cDate = '';
                            distance = 0;

                        }
                        cDate = prevDate;
                        prevDate = '';
                        distance = distance + dummyDistanceArray[i].dist;
                        if (i === dummyDistanceArray.length - 1) {
                            distanceArray.push({ 'act': 'summary', 'from': moment(cDate).format('YYYY-MM-DD'), 'to': moment(cDate).format('YYYY-MM-DD'), 'steps': 0, 'cals': 0, 'dist': distance, });
                        }
                    }
                }
                LogUtils.infoLog1("Health Distance data", distanceArray);

                // let HealthArray = [];

                data.map((dataItem) => {
                    distanceArray.map((item) => {
                        if (dataItem.from === item.from) {
                            dataItem.dist = item.dist
                        }
                    })
                })

                LogUtils.infoLog1("Health data", data);
                this._fetchCalories_Apple(data);

                // let body = JSON.stringify({
                //     td_id: 3,
                //     td_unq_id: '',
                //     fitbit_token: '',
                //     act_arr: HealthArray,

                // });

                // saveuseractivitytran(body);
            }
        });
    }

    _fetchCalories_Apple(data) {
        const opt = {
            // startDate: "2020-03-01T00:00:17.971Z", // required
            startDate: moment().subtract(7, 'days').format("YYYY-MM-DD") + 'T00:00:17.971Z', // required ISO8601Timestamp
            endDate: new Date().toISOString(), // required
            // type: 'walking',
        };
        AppleHealthKit.getActiveEnergyBurned(opt, (err, res) => {
            if (err) {
                LogUtils.infoLog(err);
                return;
            }
            LogUtils.infoLog1(res);

            if (res && Array.isArray(res) && res.length) {
                let dummyCaloriesArray = [];

                res.map((item) => {
                    if (!item.sourceName.includes('Health')) {
                        dummyCaloriesArray.push({ 'act': 'summary', 'from': item.startDate.split('T')[0], 'to': item.endDate.split('T')[0], 'steps': 0, 'cals': item.value, 'dist': 0 });
                    }
                    // dummyDistanceArray.push(item);
                });

                if (Array.isArray(dummyCaloriesArray) && dummyCaloriesArray.length) {
                    let cDate = '';
                    let calories = 0;
                    let prevDate = '';
                    for (let i = 0; i < dummyCaloriesArray.length; ++i) {
                        prevDate = dummyCaloriesArray[i].from;

                        if (cDate != '' && cDate != prevDate) {
                            // LogUtils.infoLog1("CDate", cDate);
                            // LogUtils.infoLog1("to", dummyDistanceArray[i].to);
                            caloriesArray.push({ 'act': 'summary', 'from': moment(cDate).format('YYYY-MM-DD'), 'to': moment(cDate).format('YYYY-MM-DD'), 'steps': 0, 'cals': calories, 'dist': 0, });

                            cDate = '';
                            calories = 0;

                        }
                        cDate = prevDate;
                        prevDate = '';
                        calories = calories + dummyCaloriesArray[i].cals;
                        if (i === dummyCaloriesArray.length - 1) {
                            caloriesArray.push({ 'act': 'summary', 'from': moment(cDate).format('YYYY-MM-DD'), 'to': moment(cDate).format('YYYY-MM-DD'), 'steps': 0, 'cals': calories, 'dist': 0, });
                        }
                    }
                }
                LogUtils.infoLog1("Health Calories data", caloriesArray);

                // let HealthArray = [];
                data.map((dataItem) => {
                    caloriesArray.map((item) => {
                        if (dataItem.from === item.from) {
                            dataItem.cals = item.cals
                        }
                    })
                })
            }

            LogUtils.infoLog1("Health data", data);
            this._fetchApple_Workouts(data);
        });
    }

    _fetchApple_Workouts(data) {

        const options = {
            startDate: moment().subtract(7, 'days').format("YYYY-MM-DD") + 'T07:39:49.472Z', // required ISO8601Timestamp
            endDate: new Date().toISOString(), // required ISO8601Timestamp
            type: 'Workout',
        }

        AppleHealthKit.getSamples(options, (err, res) => {

            if (err) {
                LogUtils.infoLog1(err);
                return;
            }
            LogUtils.infoLog1(res);

            if (res && Array.isArray(res) && res.length) {

                res.map((item) => {

                    if (item.tracked === true) {

                        data.push({ 'act': item.activityName, 'from': item.start.split('T')[0] + ' ' + item.start.split('T')[1].split('.')[0], 'to': item.end.split('T')[0] + ' ' + item.end.split('T')[1].split('.')[0], 'steps': 0, 'cals': item.calories, 'dist': item.distance });

                    }
                });
            }

            LogUtils.infoLog1("Health Final data", data);
            let body = JSON.stringify({
                td_id: 3,
                td_unq_id: '',
                fitbit_token: '',
                act_arr: data,

            });
            saveuseractivitytran(body);
        });
    }

    FlatListItemSeparator = () => {
        return (
            <View
                style={{
                    height: 5,
                    width: "100%",
                    backgroundColor: "transparent",
                }}
            />
        );
    }

    async onAccept() {
        if (this.state.alertTitle === 'Device Connection') {
            if (trackId === '1') {
                await AsyncStorage.setItem('fitbit_disconnected', "1");
                fitbit_disconnected = await AsyncStorage.getItem('fitbit_disconnected');
                await AsyncStorage.setItem('trackId', '0');
                trackId = await AsyncStorage.getItem('trackId');
            }
            else if (trackId === '2' && Platform.OS === 'android') {
                // GoogleFit.disconnect();
                await AsyncStorage.setItem('trackId', '0');
                trackId = await AsyncStorage.getItem('trackId');
            }
            else if (trackId === '2' && Platform.OS === 'ios') {
                await AsyncStorage.setItem('trackId', '0');
                trackId = await AsyncStorage.getItem('trackId');
            }

            this.state.DATA.map((item) => {
                if (item.id === trackId) {
                    item.check = true;
                } else {
                    item.check = false;
                }
            });
            this.setState({ DATA: this.state.DATA });

        }
        this.setState({ isAlert: false, alertMsg: '', alertTitle: '', yeslabel: '', nolabel: '' });

    }

    async onDeny() {
        this.setState({ isAlert: false, alertMsg: '', nolabel: '', yeslabel: '' });
    }
    async onPermissionAccept() {
        OAuth(config.client_id);
    }

    async onPermissionDeny() {
        this.setState({ isPermissionAlert: false, });
    }

    async fitbitStatusUpdate() {
        await AsyncStorage.setItem('fitbit_disconnected', '0');
        fitbit_disconnected = await AsyncStorage.getItem('fitbit_disconnected');
    }
    renderDisconnection = (item) => {
        if (item.check) {
            return (
                <View>
                    <Image
                        source={require('../res/ic_disconnect.png')}
                        style={{ width: 25, height: 25, alignSelf: 'center', marginRight: 50 }}
                    />
                </View>
            );
        }
        else {
            return (
                <View>
                </View>
            );
        }
    }

    render() {
        return (
            <ImageBackground source={require('../res/app_bg.png')} style={styles.mainContainer}>
                <View style={styles.containerStyle}>
                    {/* <Loader loading={this.props.loading} /> */}
                    <View style={styles.mainView}>

                        <View style={{
                            flexDirection: 'row',
                            margin: 20,
                        }}>
                            <View style={{ position: 'absolute', zIndex: 111, }}>
                                <TouchableOpacity
                                    style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                                    onPress={() => this.onBackPressed()}>
                                    <Image
                                        source={require('../res/ic_back.png')}
                                        style={styles.backImageStyle}
                                    />
                                </TouchableOpacity>
                            </View>

                            <Text style={styles.textHeadTitle}>Link My Tracker</Text>
                        </View>

                        <FlatList
                            style={{ marginTop: 10 }}
                            contentContainerStyle={{ paddingBottom: hp('20%') }}
                            showsVerticalScrollIndicator={false}
                            data={this.state.DATA}
                            extraData={this.state}
                            keyExtractor={item => item.id}
                            ItemSeparatorComponent={this.FlatListItemSeparator}
                            renderItem={({ item }) => {
                                return <TouchableOpacity style={styles.containerListStyle} onPress={() => {
                                    if (item.id === '1') {
                                        if (trackId === '1') {
                                            this.setState({ isAlert: true, alertMsg: 'Are you sure?\nDo You want to disconnect?', alertTitle: 'Device Connection', yeslabel: 'Yes', nolabel: 'No' });
                                        }
                                        else if (trackId === '2') {
                                            this.setState({ isAlert: true, alertMsg: 'You have an existing connection.\nDisconnect that before trying to connect here', alertTitle: 'Existing Connection', yeslabel: 'Ok', nolabel: '' });
                                        } else {
                                            this.fitbitStatusUpdate();
                                            OAuth(config.client_id)
                                        }

                                    } else if (item.id === '2') {
                                        LogUtils.infoLog1('trackId', trackId);
                                        if (Platform.OS === 'ios') {
                                            if (trackId === '1') {
                                                this.setState({ isAlert: true, alertMsg: 'You have an existing connection.\n Disconnect that before trying to connect here', alertTitle: 'Existing Connection', yeslabel: 'Ok', nolabel: '' });
                                            } else if (trackId === '2') {
                                                this.setState({ isAlert: true, alertMsg: 'Are you sure?\nDo You want to disconnect?', alertTitle: 'Device Connection', yeslabel: 'Yes', nolabel: 'No' });
                                            } else {
                                                this.appleAuth();

                                            }

                                        } else {
                                            if (trackId === '1') {
                                                this.setState({ isAlert: true, alertMsg: 'You have an existing connection.\n Disconnect that before trying to connect here', alertTitle: 'Existing Connection', yeslabel: 'Ok', nolabel: '' });
                                            } else if (trackId === '2') {
                                                this.setState({ isAlert: true, alertMsg: 'Are you sure?\nDo You want to disconnect?', alertTitle: 'Device Connection', yeslabel: 'Yes', nolabel: 'No' });
                                            } else {
                                                this.googleFitAuth();
                                            }
                                        }
                                    }

                                }}>
                                    <View style={{ flexDirection: 'row', alignItems: 'flex-start', alignContent: 'flex-start', }}>
                                        <View style={{ flexDirection: 'row', alignItems: 'center', flex: 1 }}>
                                            <Image
                                                source={item.image}
                                                style={{ width: 60, height: 60, marginRight: 15, alignSelf: 'center', marginLeft: 10, marginRight: 20 }}
                                            />

                                            <View style={{ flexDirection: 'column' }}>

                                                <Text style={{
                                                    fontSize: 18,
                                                    width: wp('50%'),
                                                    fontWeight: '500',
                                                    paddingRight: 10,
                                                    fontFamily: 'Rubik-Medium',
                                                    color: '#282c37',
                                                    letterSpacing: 1,
                                                }}>{`${item.name}`}</Text>

                                                <Text style={styles.textTitle}>
                                                    {`${item.caption}`}
                                                </Text>
                                                {item.check
                                                    ? (
                                                        <View style={{ flexDirection: 'row' }}>
                                                            <Image
                                                                source={require('../res/ic_connected.png')}
                                                                style={{ width: 10, height: 10, alignSelf: 'center', marginRight: 5 }}
                                                            />
                                                            <Text style={styles.textConnected}>
                                                                Connected
                                                            </Text>
                                                        </View>
                                                    )
                                                    : (
                                                        <View style={{ flexDirection: 'row' }}></View>

                                                    )}

                                            </View>

                                        </View>

                                    </View>

                                </TouchableOpacity>
                            }} />
                    </View>

                </View>

                <CustomDialog
                    visible={this.state.isAlert}
                    title={this.state.alertTitle}
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    onDecline={this.onDeny.bind(this)}
                    no={this.state.nolabel}
                    yes={this.state.yeslabel}
                />
                <CustomDialog
                    visible={this.state.isPermissionAlert}
                    title={this.state.permissionAlertTitle}
                    desc={this.state.permissionAlertMsg}
                    onAccept={this.onPermissionAccept.bind(this)}
                    onDecline={this.onPermissionDeny.bind(this)}
                    no={this.state.permissionAlertnolabel}
                    yes={this.state.permissionAlertyeslabel}
                />
            </ImageBackground >
        )
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    containerStyle: {
        width: '100%',
        height: '100%',
        flex: 1,
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'center',
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    mainView: {
        flexDirection: 'column',
    },
    containerListStyle: {
        width: wp('90%'),
        marginTop: hp('1%'),
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: '#ddd',
        // borderRadius: 15,
        borderRadius: 6,
        padding: 15,
        position: 'relative',
        marginLeft: wp('5%'),
        marginRight: wp('5%'),
        alignItems: 'center',
        justifyContent: 'flex-start',
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    textTitle: {
        fontSize: 14,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        lineHeight: 18,
    },
    textConnected: {
        fontSize: 12,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        color: '#FF5E3A',
        lineHeight: 18,
    },
});

const mapStateToProps = state => {

    return {};
};

export default withNavigationFocus(
    connect(
        mapStateToProps,
        {},
    )(TrakingScreen),
);

// export default ProfileCreation;
