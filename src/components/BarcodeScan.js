import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    BackHandler,
    Dimensions,
    Text,
    ImageBackground,
} from 'react-native';
import { NoInternet, CustomDialog, Loader } from './common';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import NetInfo from "@react-native-community/netinfo";
import { BASE_URL } from '../actions/types';
import { RNCamera } from 'react-native-camera';
import { allowFunction } from '../utils/ScreenshotUtils.js'; 
function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

class BarcodeScan extends Component {
    constructor(props) {
        super(props);
        this.camera = null;
        this.barcodeCodes = [];
        this.state = {
            searchText: '',
            loading: false,
            isAlert: false,
            isInternet: false,
            isRunning: false,
            alertMsg: '',
            noDataMsg: '',
            barcode: '',
            arrAppointments: [],
            resObj: '',
            camera: {
                type: RNCamera.Constants.Type.back,
                flashMode: RNCamera.Constants.FlashMode.auto,
            }
        };
    }

    async componentDidMount() {
        allowFunction();
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
            }
            else {
                this.setState({ isInternet: true });
            }
        });

        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }
    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        Actions.pop();
    }

    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
        this.setState({ alertMsg: '' });
        this.setState({ isAlert: false });
    }

    async onRetry() {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isInternet: false });
                this.getAllAppointments();
            }
            else {
                this.setState({ isInternet: true });
            }
        });
    }

    async callBarcode() {
        this.setState({ loading: true, isRunning: true });
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/foodbybarcode`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    barcode: this.state.barcode,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false, isRunning: false, resObj: data.data });

                    if (data.data !== '0') {
                        Actions.traAddFood({ title: this.props.title, foodId: data.data, fType: this.props.foodType, mdate: this.props.mdate });
                    }
                    else {
                        this.setState({ loading: false, isAlert: true, alertMsg: 'Barcode not found' });
                    }

                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }



            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
            });
    }

    onBarCodeRead(scanResult) {
        console.warn(scanResult.type);
        console.warn(scanResult.data);

        if (scanResult.data) {
            if (scanResult.data !== this.state.barcode) {
                if (!this.state.isRunning) {
                    this.setState({ barcode: scanResult.data });
                    this.callBarcode();
                }
            }
        }

        if (scanResult.data != null) {
            if (!this.barcodeCodes.includes(scanResult.data)) {
                this.barcodeCodes.push(scanResult.data);
                console.warn('onBarCodeRead call');
            }
        }
        return;
    }

    render() {
        return (
            <ImageBackground source={require('../res/app_bg.png')} style={styles.mainContainer}>
                <View style={styles.mainContainer}>
                    <Loader loading={this.state.loading} />

                    <NoInternet
                        image={require('../res/img_nointernet.png')}
                        loading={this.state.isInternet}
                        onRetry={this.onRetry.bind(this)} />

                    <RNCamera
                        ref={ref => {
                            this.camera = ref;
                        }}
                        defaultTouchToFocus
                        flashMode={this.state.camera.flashMode}
                        mirrorImage={false}
                        onBarCodeRead={this.onBarCodeRead.bind(this)}
                        onFocusChanged={() => { }}
                        onZoomChanged={() => { }}
                        permissionDialogTitle={'Permission to use camera'}
                        permissionDialogMessage={'We need your permission to use your camera phone'}
                        style={styles.preview}
                        type={this.state.camera.type}
                    >

                    </RNCamera>
                    <View style={[styles.overlay, styles.topOverlay]}>
                        <Text style={styles.scanScreenMessage}>Please scan the barcode.</Text>
                        <View style={{ marginTop: hp('35%'), justifyContent: 'center', alignSelf: 'center', width: wp('90%'), height: hp('30%'), borderWidth: 2, borderColor: 'red' }}>

                        </View>
                    </View>


                </View>
                <CustomDialog
                    visible={this.state.isAlert}
                    title='Alert'
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    no=''
                    yes='Ok' />
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    overlay: {
        position: 'absolute',
        padding: 16,
        right: 0,
        left: 0,
        alignItems: 'center',
        flexDirection: 'column',
    },
    topOverlay: {
        top: 0,
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    scanScreenMessage: {
        fontSize: 16,
        color: 'white',
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        color: '#ffffff',
        fontFamily: 'Rubik-Regular',
    },
});

export default BarcodeScan;
