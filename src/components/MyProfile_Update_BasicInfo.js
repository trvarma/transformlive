import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withNavigationFocus } from 'react-navigation';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    ImageBackground,
    ScrollView,
    Dimensions,
    Image,
    TextInput,
    BackHandler,
} from 'react-native';
import { CustomDialog, Loader } from './common';
import times from 'lodash.times';
import PropTypes from 'prop-types';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage';
const GAUGE_WIDTH = Math.floor(Dimensions.get('window').width - 25)
const INTERVAL_WIDTH = 13
import Slider from '@react-native-community/slider';
import LogUtils from '../utils/LogUtils.js';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
const scale1 = (v, inputMin, inputMax, outputMin, outputMax) => {
    if (v != 'undefined') {
        return Math.round(((v - inputMin) / (inputMax - inputMin)) * (outputMax - outputMin) + outputMin)
    }
}
import { updateProfileBasicInfo, profileRefresh, } from '../actions';
import { allowFunction } from '../utils/ScreenshotUtils.js';

function getLbs(val) {
    var value = parseFloat((val * 2.2046).toFixed(2));
    if (value < 0) {
        value = 0;
    }
    return value;
}

function getInches(val) {
    var value = parseFloat((val / 2.54).toFixed(2));
    if (value < 0) {
        value = 0;
    }
    return value;
}

const convertedCentoFeet = (values) => {
    var realFeet = ((values * 0.393700) / 12);
    var feet = Math.floor(realFeet);
    var inches = Math.round((realFeet - feet) * 12);
    return feet + "'" + inches;
}

class MyProfile_Update_BasicInfo extends Component {
    constructor(props) {
        super(props);
        this._handleScroll = this._handleScroll.bind(this)
        this._handleScrollEnd = this._handleScrollEnd.bind(this)
        this._handleContentSizeChange = this._handleContentSizeChange.bind(this)

        this.scrollMin = 0
        this.scrollMax = this._getScrollMax(props)
        this._scrollQueue = null
        this._value = Math.floor(this.props.height)

        this._setScrollQueue({
            x: this._scaleValue(this._value),
            animate: true,
        });

        this.state = {
            contentOffset: this._scaleValue(this._value),
            value_w: Math.floor(this.props.height),
            sliderValue: 0,
            firstName: '',
            lastName: '',
            email: '',
            age: '',
            height: '',
            gender: 0,
            curIndex: 1,
            subText: 'Next',
            isAlert: false,
            alertMsg: '',

            isShowAgeNext:true,

        };
    }

    async componentDidMount() {
        allowFunction();
        this.setState({ firstName: this.props.firstName });
        this.setState({ lastName: this.props.lastName });
        this.setState({ email: this.props.email });
        this.setState({ age: this.props.age.toString() });
        this.setState({ gender: this.props.gender });
        this.setState({ sliderValue: this.props.weight });
        this.setState({ value_w: Math.floor(this.props.height) });
        this._value = Math.floor(this.props.height);

        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        if (this.state.curIndex === 1) {
            Actions.pop();
        }
        else if (this.state.curIndex === 2) {
            this.setState({ curIndex: 1 });
            this.setState({ subText: 'Next' });
        }
        else if (this.state.curIndex === 3) {
            this.setState({ subText: 'Next' });
            this.setState({ curIndex: 2 });
        }
    }

    componentWillReceiveProps(nextProps) {
        // this.scrollMax = this._getScrollMax(nextProps)

        // if (nextProps.value !== this._value) {
        //     this._setScrollQueue({
        //         x: this._scaleValue(nextProps.value, nextProps),
        //         animate: true,
        //     })

        //     if (!this._contentSizeWillChange(nextProps)) {
        //         this._resolveScrollQueue()
        //     }
        // }

    }

    _contentSizeWillChange(nextProps) {
        let { min, max } = nextProps
        if (min !== this.props.min || max !== this.props.max) {
            return true
        }
        return false
    }
    _getScrollMax(props = this.props) {
        return (props.max - props.min) * INTERVAL_WIDTH
    }

    _scaleScroll(x, props = this.props) {
        LogUtils.infoLog('Got Here');
        let { min, max } = props
        return scale1(x, this.scrollMin, this.scrollMax, min, max)
    }

    _scaleValue(v, props = this.props) {
        LogUtils.infoLog('Got Here...');
        let { min, max } = props
        return scale1(v, min, max, this.scrollMin, this.scrollMax)
    }

    _setScrollQueue(scrollTo) {
        this._scrollQueue = scrollTo
    }

    _resolveScrollQueue() {
        if (this._scrollQueue !== null) {
            this._scrollView && this._scrollView.scrollTo(this._scrollQueue)
            this._handleScrollEnd()
        }
    }

    _handleContentSizeChange() {
        this._resolveScrollQueue()
    }

    async _handleScroll(event) {
        if (this._scrollQueue) return
        // let offset = event.nativeEvent.contentOffset.x;
        let offset = 0;
        if (event.nativeEvent.contentOffset.x === 0) {
            offset = this.state.contentOffset
        } else {
            offset = event.nativeEvent.contentOffset.x
            this.setState({ contentOffset: offset })
        }

        let { min, max } = this.props

        let val = this._scaleScroll(offset)
        if (val !== this._value) {
            this._value = val
            LogUtils.infoLog1("this._value", this._value);
            this.props.onChange(val)
            this.setState({ value_w: val });
        }
    }

    _handleScrollEnd() {
        this._value = this.props.value
        this._scrollQueue = null
    }

    _getIntervalSize(val) {
        let { largeInterval, mediumInterval } = this.props

        if (val % largeInterval == 0) return 'large'
        if (val % mediumInterval == 0) return 'medium'
        return 'small'
    }

    _renderIntervals() {
        let { min, max } = this.props
        let range = max - min + 1

        let values = times(range, (i) => i + min)

        return values.map((val, i) => {
            let intervalSize = this._getIntervalSize(val)

            return (
                <View key={`val-${i}`} style={styles.intervalContainer}>
                    {intervalSize === 'large' && (
                        <Text style={[styles.intervalValue, this.props.styles.intervalValue]}>{val}</Text>
                    )}

                    <View style={[styles.interval, styles[intervalSize], this.props.styles.interval, this.props.styles[intervalSize]]} />
                </View>
            )
        })
    }


    renderWeightValue() {
        return (
            <View style={styles.personViewStyle}>
                <View style={{ width: '90%', alignSelf: 'center', position: 'relative' }}>
                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={require('../res/ic_person_height.png')}
                        style={styles.personImageStyle}>

                    </Image>

                </View>
                <View style={[styles.container, this.props.styles.container]}>
                    <View
                        style={styles.triangleshape}>
                    </View>
                    <ScrollView
                        ref={r => this._scrollView = r}
                        automaticallyAdjustInsets={false}
                        horizontal={true}
                        decelerationRate={0}
                        snapToInterval={INTERVAL_WIDTH}
                        snapToAlignment="start"
                        showsHorizontalScrollIndicator={false}
                        onScroll={this._handleScroll}
                        onMomentumScrollEnd={this._handleScrollEnd}
                        onContentSizeChange={this._handleContentSizeChange}
                        scrollEventThrottle={100}
                        contentOffset={{ x: this.state.contentOffset }}>

                        <View style={[styles.intervals, this.props.styles.intervals]}>
                            {this._renderIntervals()}
                        </View>
                    </ScrollView>
                </View>

                <View style={{
                    position: 'absolute',
                    bottom: 0,
                    left: wp('12.5%'),
                }}>
                    <Text style={styles.weighttextStyle}>
                        {this.state.sliderValue} Kg
                    </Text>

                    <Text style={styles.weighttextStyle1}>
                        {getLbs(this.state.sliderValue)} lbs
                    </Text>
                </View>
            </View>
        );
    }

    renderValue() {
        if (this._value === 130) {
            return (
                <View>
                    <Text style={styles.HeighttextStyle}>
                        {this.props.value} cm
                    </Text>

                </View>
            );
        } else {
            return (
                <View>
                    <Text style={styles.HeighttextStyle}>
                        {this._value} cm
                    </Text>

                </View>
            );
        }
    }

    renderProfileDet() {
        if (this.state.curIndex === 1) {
            return (

                <View style={styles.containericonStyle}>
                    <Text style={styles.textStyle}>Update Basic Info</Text>
                    <View style={styles.containerMobileStyle}>
                        <TextInput
                            style={styles.textInputStyle}
                            placeholder="Enter First Name"
                            maxLength={50}
                            placeholderTextColor="grey"
                            value={this.state.firstName}
                            autoCapitalize='words'
                            returnKeyType={'next'}
                            onSubmitEditing={() => { this.secondTextInput.focus(); }}
                            onChangeText={text => this.setState({ firstName: text })}
                        />
                    </View>

                    <View style={styles.containerMobileStyle}>
                        <TextInput
                            ref={input => { this.secondTextInput = input; }}
                            style={styles.textInputStyle}
                            placeholder="Enter Last Name"
                            placeholderTextColor="grey"
                            maxLength={50}
                            autoCapitalize='words'
                            value={this.state.lastName}
                            returnKeyType={'next'}
                            onSubmitEditing={() => { this.emailTextInput.focus(); }}
                            onChangeText={text => this.setState({ lastName: text })}
                        />
                    </View>

                    <View style={styles.containerMobileStyle}>
                        <TextInput
                            ref={input => { this.emailTextInput = input; }}
                            style={styles.textInputStyle}
                            placeholder="Enter Email"
                            placeholderTextColor="grey"
                            keyboardType="email-address"
                            maxLength={100}
                            value={this.state.email}
                            returnKeyType={'next'}
                            onSubmitEditing={() => { this.ageTextInput.focus(); }}
                            onChangeText={text => this.setState({ email: text })}
                        />
                    </View>
                    <View style={styles.containerMobileStyle}>
                        <TextInput
                            ref={input => { this.ageTextInput = input; }}
                            style={styles.textInputStyle}
                            placeholder="Enter your age"
                            maxLength={3}
                            placeholderTextColor="grey"
                            keyboardType="numeric"
                            value={this.state.age}
                            onChangeText={text => {
                                if (text) {
                                    let newText = '';
                                    let numbers = '0123456789';
                                    for (var i = 0; i < text.length; i++) {
                                        if (numbers.indexOf(text[i]) > -1) {
                                            newText = newText + text[i];
                                        }
                                        else {
                                            this.setState({ alertMsg: 'Please enter numbers only' });
                                            this.setState({ isAlert: true });
                                        }
                                    }
                                    if (newText) {
                                        this.setState({ age: newText });
                                    }
                                }
                                else {
                                    this.setState({ age: '' });
                                }
                            }}
                        />
                    </View>
                </View>
            );
        }
        else if (this.state.curIndex === 2) {
            return (
                <View style={styles.containericonStyle}>

                    <Text style={styles.textStyle}>Which one are you?</Text>

                    {this.state.gender === 1
                        ? (
                            <View style={styles.containerGender}>
                                <TouchableOpacity
                                    onPress={() => this.setState({ gender: 1 })}>
                                    <View style={styles.containerMaleStyle}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/check.png')}
                                            style={styles.mobileImageStyle}
                                        />
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/male.png')}
                                            style={styles.genImg}
                                        />
                                        <Text style={styles.countryText}>Male</Text>
                                    </View>
                                </TouchableOpacity>

                                <TouchableOpacity
                                    onPress={() => this.setState({ gender: 2 })}>
                                    <View style={styles.containerFemaleStyle}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/uncheck.png')}
                                            style={styles.mobileImageStyle}
                                        />
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/female.png')}
                                            style={styles.genImg}
                                        />
                                        <Text style={styles.countryText}>Female</Text>
                                    </View>
                                </TouchableOpacity>

                            </View>
                        )
                        : (
                            <View style={styles.containerGender}>
                                <TouchableOpacity
                                    onPress={() => this.setState({ gender: 1 })}>
                                    <View style={styles.containerMaleStyle}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/uncheck.png')}
                                            style={styles.mobileImageStyle}
                                        />
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/male.png')}
                                            style={styles.genImg}
                                        />
                                        <Text style={styles.countryText}>Male</Text>
                                    </View>
                                </TouchableOpacity>

                                <TouchableOpacity
                                    onPress={() => this.setState({ gender: 2 })}>

                                    <View style={styles.containerFemaleStyle}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/check.png')}
                                            style={styles.mobileImageStyle}
                                        />
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/female.png')}
                                            style={styles.genImg}
                                        />
                                        <Text style={styles.countryText}>Female</Text>
                                    </View>
                                </TouchableOpacity>

                            </View>
                        )
                    }
                    <Text style={styles.desc}>To give you a better experience we need to know your gender</Text>
                </View>
            );
        }
        else if (this.state.curIndex === 3) {
            return (
                <View>
                    <Text style={styles.textStyle}>
                        What is your weight and height?
                    </Text>
                    <View style={{
                        position: 'absolute',
                        top: 80,
                        right: wp('12%'),
                    }}>
                        <Text style={styles.HeighttextStyle}>
                            {this.state.value_w} cm
                        </Text>
                        <Text style={styles.HeighttextStyle1}>
                            {/* {getInches(this.state.value_w)} inch */}
                            {convertedCentoFeet(this.state.value_w)}"
                        </Text>

                    </View>
                    {this.renderWeightValue()}
                    <Slider
                        style={styles.sliderImageStyle}
                        minimumValue={0}
                        maximumValue={150}
                        minimumTrackTintColor="#8c52ff"
                        maximumTrackTintColor="#000000"
                        scrollEventThrottle={100}
                        value={this.state.sliderValue}
                        onValueChange={(sliderValue) => this.setState({ sliderValue: Math.floor(sliderValue) })}
                    />
                </View>
            )
        }
    }

    async profileUpload(firstName, lastName, email, age) {
        if (this.state.curIndex === 1) {
            let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

            if (!firstName) {
                this.setState({ isAlert: true, alertMsg: 'Please enter first name' });
            }
            else if (!lastName) {
                this.setState({ isAlert: true, alertMsg: 'Please enter last name' });
            } else if (!email) {
                this.setState({ isAlert: true, alertMsg: 'Please enter an email ID' });
            }
            else if (reg.test(email.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '')) === false) {
                this.setState({ isAlert: true, alertMsg: 'Please enter a valid email ID' });
            } else if (!age) {
                this.setState({ isAlert: true, alertMsg: 'Please enter your age' });
            } else {

                if (age >= 15 && age <= 80) {
                    this.setState({ curIndex: 2 });
                    this.setState({ subText: 'Next' });
                } else {
                    this.setState({ alertMsg: 'Persons with age between 15 and 80 can only register' });
                    this.setState({ isAlert: true });
                    this.setState({ age: '' });
                }
            }
        }
        else if (this.state.curIndex === 2) {
            setTimeout(() => {
                this._scrollView && this._scrollView.scrollTo({ x: this.state.contentOffset, y: 0, animated: true })
            }, 100);
            this.setState({ curIndex: 3 });
            this.setState({ subText: 'Submit' });
        }
        else if (this.state.curIndex === 3) {
            let token = await AsyncStorage.getItem('token');
            let data = JSON.stringify({
                fname: this.state.firstName,
                lname: this.state.lastName,
                email: this.state.email,
                age: this.state.age,
                gender_id: this.state.gender,
                height: this.state.value_w.toString(),
                weight: this.state.sliderValue.toString(),
            });
            this.props.updateProfileBasicInfo({ token, data });
        }
    }

    async onAccept() {
        this.setState({ alertMsg: '' });
        this.setState({ isAlert: false });
    }

    async onSuccess() {
        this.props.profileRefresh();
        Actions.myProfile();
    }

    render() {

        return (
            <KeyboardAwareScrollView
                resetScrollToCoords={{ x: 0, y: 0 }}
                contentContainerStyle={styles.containerStyle}
                enableOnAndroid={false}
                scrollEnabled={false}
                onKeyboardDidShow={(frames)=>this.setState({isShowAgeNext:false})}
                onKeyboardDidHide={(frames)=>this.setState({isShowAgeNext:true})}
                >
                <ImageBackground source={require('../res/app_bg.png')} style={styles.containerStyle}>
                    <Loader loading={this.props.loading} />
                    <TouchableOpacity
                        onPress={() => this.onBackPressed()}>
                        <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_back.png')}
                            style={styles.backImageStyle}
                        />
                    </TouchableOpacity>

                    <View style={styles.optionInnerContainer}>
                        <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_app.png')}
                            style={styles.appImageStyle}
                        />
                    </View>

                    {this.renderProfileDet()}

                    <View style={styles.viewBottom}>
                        {
                            this.state.isShowAgeNext &&
                            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.profileUpload(
                                            this.state.firstName,
                                            this.state.lastName,
                                            this.state.email,
                                            this.state.age,
                                        );
                                    }}>
                                    <Text style={styles.buttonText}>{this.state.subText}</Text>
                                </TouchableOpacity>
                            </LinearGradient>
                        }
                    </View>

                    <CustomDialog
                        visible={this.state.isAlert}
                        title='Alert'
                        desc={this.state.alertMsg}
                        onAccept={this.onAccept.bind(this)}
                        no=''
                        yes='Ok' />

                    <CustomDialog
                        visible={this.props.isSuccess}
                        title='Congratulations !'
                        desc={this.props.sucMsg}
                        onAccept={this.onSuccess.bind(this)}
                        no=''
                        yes='Ok' />
                </ImageBackground>
            </KeyboardAwareScrollView>
        );
    }
}

MyProfile_Update_BasicInfo.propTypes = {
    min: PropTypes.number,
    max: PropTypes.number,
    largeInterval: PropTypes.number,
    mediumInterval: PropTypes.number,
    value: PropTypes.number,
    onChange: PropTypes.func,
    styles: PropTypes.object,
}

MyProfile_Update_BasicInfo.defaultProps = {
    min: 130,
    max: 210,
    mediumInterval: 5,
    largeInterval: 10,
    onChange: () => { },
    styles: {},
}

const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        height: '100%',
        flex: 1,
    },
    containerMobileStyle: {
        borderBottomWidth: 1,
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 15,
        position: 'relative',
        alignSelf: 'center',
        marginLeft: 20,
        marginRight: 20,
        marginTop: 10,
        padding: 8,
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    textInputStyle: {
        height: 40,
        flex: 1,
        fontSize: 13,
        marginLeft: 10,
        fontFamily: 'Rubik-Regular',
        color: '#2d3142',
    },
    linearGradient: {
        width: '95%',
        height: 50,
        borderRadius: 25,
        justifyContent: 'center',
        backgroundColor: 'transparent',
        alignSelf: 'center',
        marginBottom: 15
    },
    buttonText: {
        fontSize: 16,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    container: {
        height: 40,
        width: GAUGE_WIDTH,
        marginVertical: 8,
        alignSelf: 'center',
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        transform: [{ rotate: '90deg' }],
        top: 0,
    },
    triangleshape: {
        width: 0,
        height: 0,
        borderTopWidth: 15,
        borderWidth: 10,
        borderBottomWidth: 0,
        borderColor: 'transparent',
        borderTopColor: '#8c52ff',
        alignSelf: 'center',
        marginBottom: -12,
    },
    intervals: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        paddingHorizontal: GAUGE_WIDTH / 2,
        marginHorizontal: -INTERVAL_WIDTH / 2,
    },
    intervalContainer: {
        width: INTERVAL_WIDTH,
        alignItems: 'center',
    },
    interval: {
        width: 1,
        backgroundColor: '#8c52ff',
    },
    intervalValue: {
        width: 35,
        fontSize: 16,
        fontWeight: 'bold',
        color: '#8c52ff',
        position: 'absolute',
        bottom: -65,
    },
    small: {
        height: 20,
    },
    medium: {
        height: 25,
    },
    large: {
        backgroundColor: '#8c52ff',
        width: 3,
        height: 35,
        position: 'absolute',
    },
    optionInnerContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignSelf: 'center',
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: hp('7%'),
        marginLeft: 20,
        marginBottom: 20,
        alignSelf: 'flex-start',
    },
    appImageStyle: {
        width: 200,
        height: 60,
        marginTop: -40,
    },
    personImageStyle: {
        alignSelf: 'flex-end',
        width: 65,
        height: 220,
        marginTop: 20,
        marginRight: -40,
    },
    textStyle: {
        fontSize: 20,
        marginTop: hp('3%'),
        fontWeight: '500',
        color: '#2d3142',
        marginLeft: 40,
        marginRight: 40,
        marginBottom: hp('3%'),
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
        alignSelf: 'center',
        justifyContent: 'center',
    },
    viewBottom: {
        width: wp('93%'),
        alignContent: 'center',
        alignSelf: 'center',
        position: 'absolute',
        bottom: 0,
        backgroundColor: '#fff',
    },
    sliderImageStyle: {
        width: wp('75%'),
        height: hp('15%'),
        resizeMode: 'contain',
        alignSelf: 'center',
    },
    personViewStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
    },
    weighttextStyle: {
        fontSize: 24,
        fontWeight: '500',
        color: '#2d3142',
        letterSpacing: 0.72,
        fontFamily: 'Rubik-Medium',
        // position: 'absolute',
        // bottom: 0,
        // left: wp('12.5%'),
    },
    weighttextStyle1: {
        fontSize: 10,
        fontWeight: '500',
        color: '#9c9eb9',
        letterSpacing: 0.72,
        fontFamily: 'Rubik-Medium',
        alignSelf: 'center',
        marginTop: 5,
        // position: 'absolute',
        // bottom: 0,
        // left: wp('12.5%'),
    },
    HeighttextStyle: {
        fontSize: 24,
        fontWeight: '500',
        color: '#2d3142',
        letterSpacing: 0.72,
        fontFamily: 'Rubik-Medium',

    },
    HeighttextStyle1: {
        fontSize: 10,
        fontWeight: '500',
        color: '#9c9eb9',
        letterSpacing: 0.72,
        alignSelf: 'center',
        fontFamily: 'Rubik-Medium',
        marginTop: 5,
    },
    containericonStyle: {
        flexDirection: 'column',
        alignSelf: 'center',
        justifyContent: 'center',
    },
    containerGender: {
        flexDirection: 'row',
        alignSelf: 'center',
        justifyContent: 'center',
        marginTop: hp('2%'),
    },
    containerMaleStyle: {
        width: wp('40%'),
        height: hp('25%'),
        backgroundColor: '#ffffff',
        flexDirection: 'column',
        borderColor: '#ddd',
        borderRadius: 10,
        position: 'relative',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    containerFemaleStyle: {
        width: wp('40%'),
        height: hp('25%'),
        left: wp('5%'),
        backgroundColor: '#ffffff',
        flexDirection: 'column',
        borderColor: '#ddd',
        borderRadius: 10,
        position: 'relative',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 2,
    },
    mobileImageStyle: { width: 25, height: 25, position: 'absolute', top: 20, right: 0, marginRight: 20 },
    genImg: { height: 40, width: 35, alignSelf: 'center', marginTop: hp('3%'), },
    countryText: {
        alignSelf: 'center',
        fontSize: 16,
        marginTop: hp('2%'),
        fontWeight: '400',
        color: '#2d3142',
        letterSpacing: 0.26,
        fontFamily: 'Rubik-Regular',
    },
    desc: {
        fontSize: 14,
        marginTop: 10,
        marginLeft: 40,
        marginRight: 40,
        fontWeight: '400',
        color: '#9c9eb9',
        lineHeight: 24,
        textAlign: 'center',
        fontFamily: 'Rubik-Regular',
        marginTop: hp('3%'),
    },

});


const mapStateToProps = state => {

    const loading = state.procre.loading;
    const error = state.procre.error;
    const sucMsg = state.procre.sucMsg;
    const isSuccess = state.procre.isSuccess;
    return {
        loading,
        error,
        sucMsg,
        isSuccess
    };
};

export default withNavigationFocus(
    connect(
        mapStateToProps,
        { updateProfileBasicInfo, profileRefresh, },
    )(MyProfile_Update_BasicInfo),
);
