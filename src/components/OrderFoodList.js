import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Image,
    Text,
    ImageBackground,
    TouchableOpacity,
    BackHandler,
    FlatList,
    Alert
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { NoInternet, HomeDialog, Loader } from './common';
import NetInfo from "@react-native-community/netinfo";
import { BASE_URL, SWR } from '../actions/types';
import DeviceInfo from 'react-native-device-info';
import LogUtils from '../utils/LogUtils.js';
import _ from 'lodash';
import { allowFunction } from '../utils/ScreenshotUtils.js';
import GetLocation from 'react-native-get-location';

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

class OrderFoodList extends Component {

    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            isAlert: false,
            alertTitle: '',
            alertMsg: '',
            isInternet: false,
            arrOrderFood: [],
            noDataMsg: '',
            freeTrial: 0,
            isTabClicked: 1,
            active_plan: 0,
            active_plan_msg: '',
            direct_iap: 0,
            pageNo: 1,
            lat: 0,
            lng: 0,
            getData: false,
        };
        this.getOrderFoodList = this.getOrderFoodList.bind(this);
    }

    async componentDidMount() {

        allowFunction();

        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ loading: true });
                this.getCurrentLocation();
            }
            else {
                this.setState({ isInternet: true });
            }
        });
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        Actions.pop();
    }

    getCurrentLocation() {
        GetLocation.getCurrentPosition({
          enableHighAccuracy: true,
          timeout: 15000,
        })
          .then(location => {
            LogUtils.infoLog(location);
            if (location) {
                
              this.setState({ lat: location.latitude, lng: location.longitude });
             
              this.getOrderFoodList();
            }
          })
          .catch(error => {
            const { code, message } = error;
            LogUtils.infoLog1('error '+code, message);
            // GetLocation.openGpsSettings();
            this.setState({ loading: false, isAlert: true, alertMsg: 'Please enable your location settings', alertTitle: 'Alert' });
            // if (code === 'CANCELLED') {
            //     Alert.alert('Location cancelled by user or by another request');
            // }
            // if (code === 'UNAVAILABLE') {
            //     Alert.alert('Location service is disabled or unavailable');
            // }
            // if (code === 'TIMEOUT') {
            //     Alert.alert('Location request timed out');
            // }
            // if (code === 'UNAUTHORIZED') {
            //     Alert.alert('Authorization denied');
            // }
          })
      }

    async getOrderFoodList() {
       
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/fooddlvrybrands`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    lat: this.state.lat,
                    lng: this.state.lng,
                    // pagesize: 10,
                    // page: this.state.pageNo,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);

                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ getData: true});
                    if (data.data.length > 0) {
                        this.setState({ loading: false, arrOrderFood: data.data });
                    }
                    else {
                        this.setState({ loading: false, alertMsg: data.message, alertTitle: data.title });
                    }

                } else {
                    this.setState({ getData: true});
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                    } else {
                        this.setState({ loading: false, isAlert: true,alertMsg: data.message, alertTitle: data.title });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: SWR, alertTitle: 'Alert' });
            });
    }


    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
        else if(this.state.alertMsg === 'Please enable your location settings')
        {
            GetLocation.openGpsSettings();
        }
        this.setState({ isAlert: false, alertMsg: '', alertTitle: '' });
    }

    async onRetry() {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isInternet: false });
                this.getOrderFoodList();
            }
            else {
                this.setState({ isInternet: true });
            }
        });
    }

    FlatListItemSeparator = () => {
        return (
            <View
              style={{
                height: 5,
                width: "100%",
                backgroundColor: "transparent",
              }}
            />
          );
    }

    renderAllPlansData() {
        if (this.state.getData) {
            if (Array.isArray(this.state.arrOrderFood) && this.state.arrOrderFood.length) {
                return (
                    <View>
                        <FlatList
                            contentContainerStyle={{ paddingBottom: hp('20%') }}
                            showsVerticalScrollIndicator={false}
                            data={this.state.arrOrderFood}
                            keyExtractor={item => item.id}
                            ItemSeparatorComponent={this.FlatListItemSeparator}
                            extraData={this.state}
                            // initialNumToRender={5}
                            // onEndReached={this.handleLoadMore.bind(this)}
                            // onEndReachedThreshold={0.5}
                            bounces={false}
                            // refreshing={this.state.refreshing}
                            // onMomentumScrollBegin={() => {  this.setState({ onEndReachedCalledDuringMomentum: false }); }}
                            renderItem={({ item }) => {
                                return <TouchableOpacity style={styles.containerListStyle} onPress={() => Actions.webview({ url: item.web_url })}>
                                    <View style={{ flexDirection: 'column', alignItems: 'flex-start', alignContent: 'flex-start', }}>
                                        <View>
                                            {item.img
                                                ? (
                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        source={{ uri: item.img }}
                                                        style={{
                                                            width: wp('90%'),
                                                            height: undefined,
                                                            borderRadius: 6,
                                                            resizeMode: 'cover',
                                                            aspectRatio: 2 / 1,
                                                        }}
                                                    />
                                                )
                                                : (
                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        source={require('../res/ic_noimage.png')}
                                                        style={{
                                                            width: wp('90%'),
                                                            height: hp('25%'),
                                                            borderRadius: 6,
                                                            resizeMode: 'cover',
                                                        }}
                                                    />
                                                )
                                            }

                                            {/* <View style={{
                                                flexDirection: 'row',
                                                alignItems: 'center',
                                                alignContent: 'flex-start',
                                                margin: 10,
                                                bottom: 0,
                                                position: 'absolute',
                                                justifyContent: 'flex-start',
                                            }}>
                                                <Text style={styles.textPRO}>{item.name}</Text>
                                            </View> */}
                                        </View>

                                        {/* <View style={{ width: wp('90%'), flexDirection: 'column', padding: 10, alignItems: 'flex-start', alignContent: 'flex-start', borderBottomLeftRadius: 6, borderBottomRightRadius: 6 }}>
                                            <Text style={styles.textName} numberOfLines={2}>{`${item.name}`}</Text>
                                            <Text style={styles.textTitle}>{`${item.durtnlvl}`}</Text>
                                            <View style={{ marginTop: 2, justifyContent: 'center', flexDirection: 'row', alignSelf: 'flex-start', }}>
                                                <Text style={styles.textEqpNeed} numberOfLines={1}>{`${item.equipment}`}</Text>
                                                {this.renderChallengesProfiles(item)}
                                            </View>
                                        </View> */}
                                    </View>
                                </TouchableOpacity>
                            }} />
                    </View>

                );

            }
            else {
                return (
                    <View style={{ flexDirection: 'column', flex: 1, alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>
                        <Image
                            source={require('../res/img_hungry_info.png')}
                            style={styles.imgNoSuscribe}
                        />
                        <Text style={styles.textNodataTitle}>{this.state.alertTitle}</Text>

                        <Text style={styles.textNodata}>{this.state.alertMsg}</Text>
                    </View>
                );
            }
        }
    }

    render() {
        return (
            <ImageBackground source={require('../res/app_bg.png')} style={styles.containerStyle}>
                <View style={styles.containerStyle}>
                    <Loader loading={this.state.loading} />

                    <NoInternet
                        image={require('../res/img_nointernet.png')}
                        loading={this.state.isInternet}
                        onRetry={this.onRetry.bind(this)} />

                    <View style={{
                        flexDirection: 'row',
                        margin: 20,
                    }}>
                        <View style={{ position: 'absolute', zIndex: 111 }}>
                            <TouchableOpacity
                                onPress={() => this.onBackPressed()}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_back.png')}
                                    style={styles.backImageStyle}
                                />
                            </TouchableOpacity>
                        </View>

                        <Text style={styles.textHeadTitle}>Order Food</Text>
                    </View>

                    {this.renderAllPlansData()}

                </View>

                <HomeDialog
                    visible={this.state.isAlert}
                    title={this.state.alertTitle}
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    no=''
                    yes='Ok' />

            </ImageBackground>
        );
    }
}


const styles = StyleSheet.create({
    containerStyle: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    backImageStyle: {
        width: 19,
        height: 16,
        alignSelf: 'flex-start',
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    textNodata: {
        width: wp('85%'),
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
    },
    imgNoSuscribe: {
        width: 270,
        height: 270,
        alignSelf: 'center',
      },
      textNodataTitle: {
        width: wp('85%'),
        fontSize: 16,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
      },
      containerListStyle: {
        width: wp('90%'),
        marginTop: hp('1%'),
        backgroundColor: '#ffffff',
        flexDirection: 'column',
        borderColor: '#ddd',
        borderRadius: 6,
        position: 'relative',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
      },
      textPRO: {
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontSize: 9,
        paddingTop: 1,
        paddingBottom: 1,
        paddingLeft: 10,
        paddingRight: 10,
        borderRadius: 15,
        marginLeft: 2,
        backgroundColor: '#8c52ff',
        alignSelf: 'center',
        textAlign: 'center',
        fontWeight: '500',
        lineHeight: 18,
      },
});

export default OrderFoodList;
