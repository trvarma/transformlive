/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Image,
    Text,
    ImageBackground,
    TouchableOpacity,
    BackHandler,
    FlatList,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { NoInternet, HomeDialog, Loader } from './common';
import NetInfo from "@react-native-community/netinfo";
import { BASE_URL } from '../actions/types';
import DeviceInfo from 'react-native-device-info';
import LogUtils from '../utils/LogUtils.js';
import { allowFunction } from '../utils/ScreenshotUtils.js'; 



function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}



class Trainee_AllBuyPlansLIst extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            isAlert: false,
            alertTitle: '',
            alertMsg: '',
            isInternet: false,
            arrAllPlans: [],
            noDataMsg: '',
        };
        this.getAllPlans = this.getAllPlans.bind(this);
    }

    async componentDidMount() {
        allowFunction();
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.getAllPlans();
            }
            else {
                this.setState({ isInternet: true });
            }
        });
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        Actions.pop();
    }

    async getAllPlans() {
        this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');

        fetch(
            `${BASE_URL}/trainee/plans`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);

                if (statusCode >= 200 && statusCode <= 300) {
                    if (data.data.length) {
                        this.setState({ loading: false, arrAllPlans: data.data });
                    }
                    else {
                        this.setState({ loading: false, noDataMsg: 'No data available at this moment' });
                    }

                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error), alertTitle: 'Alert' });
            });
    }


    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
        this.setState({ isAlert: false, alertMsg: '', alertTitle: '' });
    }

    async onRetry() {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isInternet: false });
                this.getAllPlans();
            }
            else {
                this.setState({ isInternet: true });
            }
        });
    }

    FlatListItemSeparator1 = () => {
        return (
            <View
                style={{
                    height: 10,
                    width: 1,
                    backgroundColor: "transparent",
                }}
            />
        );
    }

    renderAllPlansList() {
        if (Array.isArray(this.state.arrAllPlans) && this.state.arrAllPlans.length) {
            return (
                <FlatList
                    contentContainerStyle={{ paddingBottom: hp('10%') }}
                    showsVerticalScrollIndicator={false}
                    data={this.state.arrAllPlans}
                    keyExtractor={item => item.plan_id}
                    ItemSeparatorComponent={this.FlatListItemSeparator1}
                    // ListEmptyComponent={this.ListEmptyView}
                    renderItem={({ item, index }) => {
                        return <TouchableOpacity key={item.plan_id} style={styles.aroundListStyle}
                            onPress={() => {
                                LogUtils.firebaseEventLog('click', {
                                    p_id: item.plan_id,
                                    p_category: 'Plans',
                                    p_name: `Plan - ${item.name}`,
                                  });
                                Actions.traBuyPlanDet({ selPlan: item });
                            }}>
                            <View>

                                {item.image
                                    ? (
                                        <View>
                                            <Image
                                                progressiveRenderingEnabled={true}
                                                resizeMethod="resize"
                                                source={{ uri: item.image }}
                                                style={{
                                                    width: wp('92%'),
                                                    height: hp('22%'),
                                                    borderRadius: 10,
                                                    resizeMode: 'cover',
                                                }}
                                            />
                                        </View>

                                    )
                                    : (
                                        <View style={{
                                            width: wp('92%'),
                                            height: hp('23%'),
                                            borderRadius: 10,
                                            resizeMode: 'cover',
                                        }}>
                                        </View>
                                    )
                                }
                                {index % 2 == 0
                                    ? (
                                        <View style={{
                                            flexDirection: 'row',
                                            padding: 15,
                                            position: 'absolute',
                                            width: wp('92%'),
                                            height: hp('23%'),
                                        }}>

                                            <View style={{ flexDirection: 'column', alignItems: 'flex-start', alignContent: 'center', justifyContent: 'center', }}>

                                                <Text style={styles.txtPlanName} numberOfLines={5}>{`${item.title}`}</Text>

                                                {/* <Text style={styles.txtPlanDesc} numberOfLines={3}>{`${item.description}`}</Text> */}

                                                <LinearGradient colors={['#8c52ff', '#8c52ff']} style={{ borderRadius: 25, marginTop: 20, padding: 7, }}>
                                                    <Text style={styles.txtAmount}>{`${item.starts_from}`}</Text>
                                                </LinearGradient>
                                            </View>
                                        </View>
                                    )
                                    : (
                                        <View style={{
                                            flexDirection: 'row',
                                            padding: 15,
                                            position: 'absolute',
                                            width: wp('92%'),
                                            height: hp('23%'),
                                        }}>
                                            <View style={{ marginLeft: wp('25%'), flexDirection: 'column', alignItems: 'flex-end', alignContent: 'center', justifyContent: 'center', alignSelf: 'center' }}>
                                                <Text style={styles.txtPlanName1} numberOfLines={5}>{`${item.title}`}</Text>
                                                <LinearGradient colors={['#8c52ff', '#8c52ff']} style={{ borderRadius: 25, marginTop: 20, padding: 7, }}>
                                                    <Text style={styles.txtAmount}>{`${item.starts_from}`}</Text>
                                                </LinearGradient>
                                            </View>
                                        </View>
                                    )
                                }

                            </View>
                        </TouchableOpacity >
                    }} />
            );
        }
        else {
            return (

                <View style={{ flexDirection: 'row', flex: 1, }}>
                    <Text style={styles.textNodata}>{this.state.noDataMsg}</Text>
                </View>
            );
        }

    }

    render() {
        return (
            <ImageBackground source={require('../res/app_bg.png')} style={styles.containerStyle}>
                <View style={styles.containerStyle}>
                    <Loader loading={this.state.loading} />

                    <NoInternet
                        image={require('../res/img_nointernet.png')}
                        loading={this.state.isInternet}
                        onRetry={this.onRetry.bind(this)} />

                    <View style={{
                        flexDirection: 'row',
                        margin: 20,
                    }}>
                        <View style={{ position: 'absolute', zIndex: 111 }}>
                            <TouchableOpacity
                                onPress={() => this.onBackPressed()}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_back.png')}
                                    style={styles.backImageStyle}
                                />
                            </TouchableOpacity>
                        </View>

                        <Text style={styles.textHeadTitle}>All Plans</Text>
                    </View>

                    {this.renderAllPlansList()}

                </View>

                <HomeDialog
                    visible={this.state.isAlert}
                    title={this.state.alertTitle}
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    no=''
                    yes='Ok' />

            </ImageBackground>
        );
    }
}


const styles = StyleSheet.create({
    containerStyle: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    backImageStyle: {
        width: 19,
        height: 16,
        alignSelf: 'flex-start',
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    textNodata: {
        width: wp('90%'),
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        padding: 10,
        margin: 20,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
    },
    aroundListStyle: {
        width: wp('92%'),
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        alignContent: 'center',
        alignSelf: 'center',
        flexDirection: 'column',
        borderColor: '#ddd',
        // borderRadius: 15,
        borderRadius: 8,
        position: 'relative',
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 1,
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    txtPlanName: {
        width: wp('60%'),
        fontSize: 15,
        fontWeight: '500',
        color: '#282c37',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
    },
    txtPlanDesc: {
        width: wp('60%'),
        fontSize: 11,
        fontWeight: '400',
        color: '#282c37',
        marginTop: 8,
        fontFamily: 'Rubik-Regular',
        letterSpacing: 0.2,
    },
    txtPlanName1: {
        width: wp('60%'),
        fontSize: 15,
        fontWeight: '500',
        color: '#282c37',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
        textAlign: 'right',
    },
    txtAmount: {
        fontSize: 12,
        fontWeight: '500',
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
    },
});

export default Trainee_AllBuyPlansLIst;
