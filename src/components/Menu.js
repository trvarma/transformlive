import React from 'react';
import PropTypes from 'prop-types';
import {
    Dimensions,
    StyleSheet,
    ScrollView,
    View,
    Image,
    Text,
    TouchableOpacity,
} from 'react-native';

const window = Dimensions.get('window');
const uri = 'https://pickaface.net/gallery/avatar/Opi51c74d0125fd4.png';

const styles = StyleSheet.create({
    menu: {
        flex: 1,
        width: window.width,
        height: window.height,
        backgroundColor: '#ffffff',
        alignContent: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        padding: 20,
    },
    avatarContainer: {
        marginBottom: 20,
        marginTop: 20,
    },
    avatar: {
        width: 48,
        height: 48,
        borderRadius: 24,
        flex: 1,
    },
    name: {
        position: 'absolute',
        left: 70,
        top: 20,
    },
    item: {
        fontSize: 11,
        fontWeight: '500',
        paddingTop: 15,
        fontWeight: '500',
        color: '#6d819c',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    item1: {
        fontSize: 11,
        fontWeight: '500',
        paddingTop: 15,
        fontWeight: '500',
        color: '#8c52ff',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
});

export default function Menu({ onItemSelected }) {
    return (
        // <ScrollView scrollsToTop={false} style={styles.menu}>
        <View style={styles.menu}>
            {/* <View style={styles.avatarContainer}>
                <Image
                    style={styles.avatar}
                    source={{ uri }}
                />
                <Text style={styles.name}>Your name</Text>
            </View> */}

            <TouchableOpacity onPress={() => onItemSelected('Home')}>
                <Text style={styles.item1}>HOME</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => onItemSelected('My Orders')}>
                <Text style={styles.item}>MY ORDERS</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => onItemSelected('Appointments')}>
                <Text style={styles.item}>APPOINTMENTS</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => onItemSelected('Photos')}>
                <Text style={styles.item}>PHOTOS</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => onItemSelected('Notifications')}>
                <Text style={styles.item}>NOTIFICATIONS</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => onItemSelected('Terms')}>
                <Text style={styles.item}>TERMS AND CONDITIONS</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => onItemSelected('Refer')}>
                <Text style={styles.item}>REFER A FRIEND</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => onItemSelected('Settings')}>
                <Text style={styles.item}>SETTINGS</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => onItemSelected('Logout')}>
                <Text style={styles.item}>LOGOUT</Text>
            </TouchableOpacity>
        </View>
        // </ScrollView> 
    );
}

Menu.propTypes = {
    onItemSelected: PropTypes.func.isRequired,
};