import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  Alert,
  Image,
  TouchableOpacity,
  BackHandler,
  ImageBackground,
  Text,
  Modal,
  ScrollView,
} from 'react-native';
import { connect } from 'react-redux';
import { BASE_URL } from '../actions/types';
import { Loader, CustomDialog } from './common';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import NetInfo from "@react-native-community/netinfo";
import LogUtils from '../utils/LogUtils.js';
import { allowFunction } from '../utils/ScreenshotUtils.js'; 

function processResponse(response) {
  const statusCode = response.status;
  const data = response.json();
  return Promise.all([statusCode, data]).then(res => ({
    statusCode: res[0],
    data: res[1],
  }));
}
let LocationId = 0, fitnessLevelIds = '', trTypeIds = '', eqpIds = '', goalIds = '', fitnessFormIds = '', healthProblemIds = '';

class Trainee_AllWorkoutList extends Component {
  constructor(props) {
    super(props);
    LogUtils.showSlowLog(this);

    this.state = {
      searchText: '',
      isAlert: false,
      loading: false,
      alertMsg: '',
      resObj: '',
      dataToRender: [],
      pageNo: 1,
      refreshing: false,
      callLoadMore: false,
      onEndReachedCalledDuringMomentum: true,
      isFilterVisible: false,
      filterObj: {},
      isBeginner: 0,
      isIntermediate: 0,
      isAdvanced: 0,

      LocationId: 0,

      fitnesslevelTemp: [],
      fitnesslevelSelected: [],


      trainingTypeTemp: [],
      trainingTypeSelected: [],

      fitnessFormTemp: [],
      fitnessFormSelected: [],

      equipmentTemp: [],
      equipmentSelected: [],

      goalTemp: [],
      goalSelected: [],

      healthProblemsTemp: [],
      healthProblemsSelected: [],
    };
  }


  async componentDidMount() {
    allowFunction();
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.callFiltersService();
        this.setState({ loading: true });
        LocationId = 0, fitnessLevelIds = '', trTypeIds = '', eqpIds = '', goalIds = '', fitnessFormIds = '', healthProblemIds = '';
        this.callService();
      }
      else {
        this.setState({ isAlert: true, alertMsg: 'Please enable your internet connection and try again' });
      }
    });


    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      this.onBackPressed();
      return true;
    });
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 5,
          width: "100%",
          backgroundColor: "transparent",
        }}
      />
    );
  }
  //handling onPress action
  getListViewItem = item => {
    Alert.alert(item.hours);
  };

  onBackPressed() {
    Actions.pop();
  }

  async callService() {
    this.setState({ visible: false, loading: true });
    let token = await AsyncStorage.getItem('token');
    LogUtils.infoLog('Body', JSON.stringify({
      wtype: this.props.wType,
      pagesize: 10,
      page: this.state.pageNo,
      gid: goalIds,
      eid: eqpIds,
      ttid: trTypeIds,
      flid: fitnessLevelIds,
      ltid: this.state.LocationId,
      ffid: fitnessFormIds,
    }));

    fetch(
      `${BASE_URL}/trainee/getmoreworkouts`,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
          wtype: this.props.wType,
          pagesize: 10,
          page: this.state.pageNo,
          gid: goalIds,
          eid: eqpIds,
          ttid: trTypeIds,
          flid: fitnessLevelIds,
          ltid: this.state.LocationId,
          ffid: fitnessFormIds,
        }),
      },
    )
      .then(processResponse)
      .then(res => {
        const { statusCode, data } = res;
        LogUtils.infoLog1('Fiter statusCode', statusCode);
        LogUtils.infoLog1('Fiter statusmessage', data);
        if (statusCode >= 200 && statusCode <= 300) {


          this.setState({ loading: false, resObj: data.data, refreshing: false, });
          switch (this.props.wType) {
            case 1:
              // this.setState({ dataToRender: this.state.resObj.recmworkouts });
              if (data.data.recmworkouts.length > 0) {
                this.setState({
                  dataToRender: [...this.state.dataToRender, ...this.state.resObj.recmworkouts],
                  callLoadMore: true
                  //adding the new data with old one available in Data Source of the List
                });
              } else {
                this.setState({ callLoadMore: false });
              }

              break;
            case 2:
              if (data.data.recmworkouts.length > 0) {
                this.setState({
                  dataToRender: [...this.state.dataToRender, ...this.state.resObj.recmworkouts],
                  callLoadMore: true
                  //adding the new data with old one available in Data Source of the List
                });
              } else {
                this.setState({ callLoadMore: false });
              }

              break;
            // case 3:
            //   this.setState({ dataToRender: this.state.resObj.nearbyworkouts });
            //   break;
            // case 4:
            //   this.setState({ dataToRender: this.state.resObj.trainer });
            //   break;
            case 5:
              // this.setState({ dataToRender: this.state.resObj.recmworkouts });
              if (data.data.recmworkouts.length > 0) {
                this.setState({
                  dataToRender: [...this.state.dataToRender, ...this.state.resObj.recmworkouts],
                  callLoadMore: true
                  //adding the new data with old one available in Data Source of the List
                });
              } else {
                this.setState({ callLoadMore: false });
              }

              break;
          }


        } else {
          if (data.message === 'You are not authenticated!') {
            this.setState({ isAlert: true, alertMsg: data.message, refreshing: false });
          } else {
            this.setState({ isAlert: true, alertMsg: data.message, refreshing: false });
          }
        }
      })
      .catch(function (error) {
        this.setState({ isAlert: true, alertMsg: JSON.stringify(error), refreshing: false });
      });
  }

  handleLoadMore = () => {
    LogUtils.infoLog1('Call Load more', this.state.callLoadMore);
    if (!this.state.loading && this.state.callLoadMore) {
      this.setState({ pageNo: this.state.pageNo + 1, loading: false, callLoadMore: false }) // increase page by 1
      this.callService(); // method for API call 
    }
  };


  async callFiltersService() {
    let token = await AsyncStorage.getItem('token');

    fetch(
      `${BASE_URL}/master/homefilters`,
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },

      },
    )
      .then(processResponse)
      .then(res => {
        const { statusCode, data } = res;
        LogUtils.infoLog1('statusCode', statusCode);
        LogUtils.infoLog1('statusmessage', data);
        if (statusCode >= 200 && statusCode <= 300) {

          this.setState({ filterObj: data.data, });

        } else {
          if (data.message === 'You are not authenticated!') {
            // this.setState({ isAlert: true, alertMsg: data.message, refreshing: false });
          } else {
            // this.setState({ isAlert: true, alertMsg: data.message, refreshing: false });
          }
        }
      })
      .catch(function (error) {
        // this.setState({ isAlert: true, alertMsg: JSON.stringify(error), refreshing: false });
      });
  }



  async callServiceLoadMore() {
    if (this.state.callLoadMore) {

      this.setState({ visible: false, refreshing: true });
      let token = await AsyncStorage.getItem('token');
      LogUtils.infoLog1('BASE_URL', `${BASE_URL}/trainee/getmoreworkouts`);
      LogUtils.infoLog1('Load more Body', JSON.stringify({
        wtype: this.props.wType,
        pagesize: 10,
        page: this.state.pageNo,
        gid: goalIds,
        eid: eqpIds,
        ttid: trTypeIds,
        flid: fitnessLevelIds,
        ltid: LocationId,
      }));
      fetch(
        `${BASE_URL}/trainee/getmoreworkouts`,
        {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify({
            wtype: this.props.wType,
            pagesize: 10,
            page: this.state.pageNo,
            gid: goalIds,
            eid: eqpIds,
            ttid: trTypeIds,
            flid: fitnessLevelIds,
            ltid: LocationId,
          }),
        },
      )
        .then(processResponse)
        .then(res => {
          const { statusCode, data } = res;
          LogUtils.infoLog1('statusCode', statusCode);
          LogUtils.infoLog1('statusmessage', data);
          if (statusCode >= 200 && statusCode <= 300) {
            this.setState({ loading: false, refreshing: false });
            switch (this.props.wType) {
              case 1:
                // this.setState({ dataToRender: this.state.resObj.recmworkouts });
                if (data.data.recmworkouts.length > 0) {
                 
                  this.setState({
                    dataToRender: [...this.state.dataToRender, ...data.data.recmworkouts],
                    //adding the new data with old one available in Data Source of the List
                  });
                }
                else {
                  this.setState({ callLoadMore: false, });
                }

                break;
              case 2:
                // this.setState({ dataToRender: this.state.resObj.populworkouts });
                if (data.data.populworkouts.length > 0) {
                  this.setState({
                    dataToRender: [...this.state.dataToRender, ...data.data.populworkouts],
                    //adding the new data with old one available in Data Source of the List
                  });
                }
                else {
                  this.setState({ callLoadMore: false, });
                }

                break;
              // case 3:
              //   this.setState({ dataToRender: this.state.resObj.nearbyworkouts });
              //   break;
              // case 4:
              //   this.setState({ dataToRender: this.state.resObj.trainer });
              //   break;
            }
          } else {
            if (data.message === 'You are not authenticated!') {
              this.setState({ isAlert: true, alertMsg: data.message, refreshing: false });
            } else {
              this.setState({ isAlert: true, alertMsg: data.message, refreshing: false });
            }
          }
        })
        .catch(function (error) {
          this.setState({ isAlert: true, alertMsg: JSON.stringify(error), refreshing: false });
        });
    }
  }

  async onAccept() {
    if (this.state.alertMsg === 'You are not authenticated!') {
      AsyncStorage.clear().then(() => {
        Actions.auth({ type: 'reset' });
      });
    }
    this.setState({ alertMsg: '' });
    this.setState({ isAlert: false });
  }

  async onSuccess() {
    this.setState({ isSuccess: false, sucMsg: '' });
  }

  toggleModal(visible) {
    this.setState({ isFilterVisible: visible });
  }

  pressLocation = (hey) => {
    this.state.filterObj.woroutlocation.map((item) => {
      if (item.id === hey.id) {
        item.check = true;
        this.setState({ LocationId: item.id });
      }
      else {
        item.check = false;
      }
    })

  }


  pressfitnessLevel = (hey) => {

    if (hey.name.toLowerCase() === 'all levels') {
      this.onClearArrayLevel(hey);
    }
    else {
      this.state.filterObj.fitnesslevel.map((item) => {
        if (item.id === hey.id) {
          item.check = !item.check
          if (item.check === true) {
            this.state.fitnesslevelSelected.push(item);
          } else if (item.check === false) {
            const i = this.state.fitnesslevelSelected.indexOf(item)
            if (1 != -1) {
              this.state.fitnesslevelSelected.splice(i, 1)
              return this.state.fitnesslevelSelected
            }
          }
        }
        else {
          if (item.name.toLowerCase() === 'all levels') {
            item.check = false
          }
        }
      })

      this.setState({ fitnesslevelTemp: this.state.filterObj.fitnesslevel })
    }
  }


  presstrainingType = (hey) => {
    this.state.filterObj.trainingtype.map((item) => {
      if (item.id === hey.id) {
        item.check = !item.check
        if (item.check === true) {
          this.state.trainingTypeSelected.push(item);
        } else if (item.check === false) {
          const i = this.state.trainingTypeSelected.indexOf(item)
          if (1 != -1) {
            this.state.trainingTypeSelected.splice(i, 1)
            return this.state.trainingTypeSelected
          }
        }
      }
    })
    this.setState({ trainingTypeTemp: this.state.filterObj.trainingtype })
  }


  pressFitnessForm = (hey) => {
    this.state.filterObj.fitness_form.map((item) => {
      if (item.id === hey.id) {
        item.check = !item.check
        if (item.check === true) {
          this.state.fitnessFormSelected.push(item);
        } else if (item.check === false) {
          const i = this.state.fitnessFormSelected.indexOf(item)
          if (1 != -1) {
            this.state.fitnessFormSelected.splice(i, 1)
            return this.state.fitnessFormSelected
          }
        }
      }
    })
    this.setState({ fitnessFormTemp: this.state.filterObj.fitness_form })
  }

  pressHealthProblems = (hey) => {

    if (hey.name.toLowerCase() === 'none') {
      this.onClearArray(hey);
    }
    else {
      this.state.filterObj.health_problm.map((item) => {
        if (item.id === hey.id) {
          item.check = !item.check
          if (item.check === true) {
            this.state.healthProblemsSelected.push(item);
          } else if (item.check === false) {
            const i = this.state.healthProblemsSelected.indexOf(item)
            if (1 != -1) {
              this.state.healthProblemsSelected.splice(i, 1)
              return this.state.healthProblemsSelected
            }
          }
        }
        else {
          if (item.name.toLowerCase() === 'none') {
            item.check = false
          }
        }
      })

      // this.removeNone();
      this.setState({ healthProblemsTemp: this.state.filterObj.health_problm })
    }

  }

  removeNone() {
    this.state.filterObj.health_problm.map((item) => {
      if (item.check) {
      }
      else {
        const i = this.state.filterObj.health_problm.indexOf(item)
        if (1 != -1) {
          this.state.filterObj.health_problm.splice(i, 1)
          return this.state.filterObj.health_problm
        }
      }
    })
  }

  pressEquipment = (hey) => {
    this.state.filterObj.equipment.map((item) => {
      if (item.id === hey.id) {
        item.check = !item.check
        if (item.check === true) {
          this.state.equipmentSelected.push(item);
        } else if (item.check === false) {
          const i = this.state.equipmentSelected.indexOf(item)
          if (1 != -1) {
            this.state.equipmentSelected.splice(i, 1)
            return this.state.equipmentSelected
          }
        }
      }
    })
    this.setState({ equipmentTemp: this.state.filterObj.equipment })
  }

  pressGoal = (hey) => {
    this.state.filterObj.goal.map((item) => {
      if (item.id === hey.id) {
        item.check = !item.check
        if (item.check === true) {
          this.state.goalSelected.push(item);
        } else if (item.check === false) {
          const i = this.state.goalSelected.indexOf(item)
          if (1 != -1) {
            this.state.goalSelected.splice(i, 1)
            return this.state.goalSelected
          }
        }
      }
    })
    this.setState({ goalTemp: this.state.filterObj.goal })
  }

  onClearArray(hey) {
    const interval = setInterval(() => {
      this.setState({ healthProblemsSelected: [] });
      this.state.filterObj.health_problm.map((item) => {
        if (item.id === hey.id) {
          item.check = !item.check;
          if (item.check === true) {
            this.state.healthProblemsSelected.push(item);
          }
        }
        else {
          item.check = false;
        }
      });
      this.setState({ healthProblemsTemp: this.state.filterObj.health_problm });
      clearInterval(interval);
    }, 100);
  };

  onClearArrayLevel(hey) {
    const interval = setInterval(() => {
      this.setState({ fitnesslevelSelected: [] });
      this.state.filterObj.fitnesslevel.map((item) => {
        if (item.id === hey.id) {
          item.check = !item.check;
          if (item.check === true) {
            this.state.fitnesslevelSelected.push(item);
          }
        }
        else {
          item.check = false;
        }
      });
      this.setState({ fitnesslevelTemp: this.state.filterObj.fitnesslevel });
      clearInterval(interval);
    }, 100);
  };

  reset() {

    if (this.state.LocationId > 0) {
      this.state.filterObj.woroutlocation.map((item) => {
        if (item.check === true) {
          item.check = false;

        }
        this.setState({ LocationId: 0 });
        LocationId = this.state.LocationId;
      })

    }

    if (this.state.fitnesslevelSelected) {
      fitnessLevelIds = '';
      this.state.filterObj.fitnesslevel.map((item) => {
        if (item.check === true) {
          item.check = false;

        }
      })
      this.setState({ fitnesslevelSelected: [] });

    }

    if (this.state.trainingTypeSelected) {
      trTypeIds = '';
      this.state.filterObj.trainingtype.map((item) => {
        if (item.check === true) {
          item.check = false;
        }
      })
      this.setState({ trainingTypeSelected: [] });

    }

    if (this.state.equipmentSelected) {
      eqpIds = '';
      this.state.filterObj.equipment.map((item) => {
        if (item.check === true) {
          item.check = false;
        }
      })
      this.setState({ equipmentSelected: [] });

    }

    if (this.state.goalSelected) {
      goalIds = '';
      this.state.filterObj.goal.map((item) => {
        if (item.check === true) {
          item.check = false;
        }
      })
      this.setState({ goalSelected: [] });

    }

    if (this.state.fitnessFormSelected) {
      fitnessFormIds = '';
      this.state.filterObj.fitness_form.map((item) => {
        if (item.check === true) {
          item.check = false;
        }
      })
      this.setState({ fitnessFormSelected: [] });

    }

    if (this.state.healthProblemsSelected) {
      healthProblemIds = '';
      this.state.filterObj.health_problm.map((item) => {
        if (item.check === true) {
          item.check = false;
        }
      })
      this.setState({ healthProblemsSelected: [] });

    }

    this.setState({ LocationId: 0 });

  }

  onDoneClicked() {
    if (this.state.LocationId > 0) {
      LocationId = this.state.LocationId;
    }

    if (this.state.fitnesslevelSelected) {
      fitnessLevelIds = '';
      this.state.fitnesslevelSelected.map((item) => {
        if (item.check) {
          if (fitnessLevelIds.length === 0) {
            fitnessLevelIds = fitnessLevelIds + item.id;
          }
          else {
            fitnessLevelIds = fitnessLevelIds + ',' + item.id;
          }
        }
      })

    }

    if (this.state.trainingTypeSelected) {
      trTypeIds = '';
      this.state.trainingTypeSelected.map((item) => {
        if (item.check) {
          if (trTypeIds.length === 0) {
            trTypeIds = trTypeIds + item.id;
          }
          else {
            trTypeIds = trTypeIds + ',' + item.id;
          }
        }
      })
    }

    if (this.state.equipmentSelected) {
      eqpIds = '';
      this.state.equipmentSelected.map((item) => {
        if (item.check) {
          if (eqpIds.length === 0) {
            eqpIds = eqpIds + item.id;
          }
          else {
            eqpIds = eqpIds + ',' + item.id;
          }
        }
      })
    }
    if (this.state.goalSelected) {
      goalIds = '';
      this.state.goalSelected.map((item) => {
        if (item.check) {
          if (goalIds.length === 0) {
            goalIds = goalIds + item.id;
          }
          else {
            goalIds = goalIds + ',' + item.id;
          }
        }
      })
    }

    if (this.state.fitnessFormSelected) {
      fitnessFormIds = '';
      this.state.fitnessFormSelected.map((item) => {
        if (item.check) {
          if (fitnessFormIds.length === 0) {
            fitnessFormIds = fitnessFormIds + item.id;
          }
          else {
            fitnessFormIds = fitnessFormIds + ',' + item.id;
          }
        }
      })
    }

    if (this.state.healthProblemsSelected) {
      healthProblemIds = '';
      this.state.healthProblemsSelected.map((item) => {
        if (item.check) {
          if (healthProblemIds.length === 0) {
            healthProblemIds = healthProblemIds + item.id;
          }
          else {
            healthProblemIds = healthProblemIds + ',' + item.id;
          }
        }
      })
    }
    this.setState({ pageNo: 1, dataToRender: [], });
    this.toggleModal(!this.state.isFilterVisible);
    this.callService();
  }

  renderChallengesProfiles(item) {
    if (item.user_images.length) {
      if (item.user_images.length === 1) {
        let arrProfiles = item.user_images.map((item1, i) => {
          return <View key={i} style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Image
              source={{ uri: item1.profile_img }}
              style={styles.chaProfile} />
          </View>
        });
        return (
          <View>
            {arrProfiles}
          </View>
        );
      }
      else if (item.user_images.length === 2) {
        let arrProfiles = item.user_images.map((item1, i) => {
          return <View key={i} style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Image
              source={{ uri: item1.profile_img }}
              style={styles.chaProfile} />
          </View>
        });
        return (
          <View style={{ flexDirection: 'row', marginTop: 7, alignItems: 'center' }}>
            {arrProfiles}
            
            {item.pgm_usr_cnt !== 0
              ?
              (
                <Text style={styles.chaMoreText}>+{`${item.pgm_usr_cnt}`}</Text>
              )
              :
              (
                <View></View>
              )
            }
          </View>
        );
      }
      // else {
      //   let arrProfiles = item.user_images.map((item1, i) => {
      //     if (i < 2) {
      //       return <View key={i} style={{ flexDirection: 'row', alignItems: 'center' }}>
      //         <Image
      //           source={{ uri: item1.profile_img }}
      //           style={styles.chaProfile} />
      //       </View>
      //     }
      //   });
      //   return (
      //     <View style={{ flexDirection: 'row', alignItems: 'center', }}>
      //       {arrProfiles}
      //       <Text style={styles.chaMoreText}>+{item.enrolled_cnt - 2}</Text>
      //     </View>
      //   );
      // }
    }
    else {
      return (
        <View>
        </View>
      );
    }
  }

  renderFlatlist() {
    if (this.state.resObj) {
      LogUtils.infoLog1('dataToRender ', this.state.dataToRender);
      if (Array.isArray(this.state.dataToRender) && this.state.dataToRender.length) {
        return (
          <View>
            <Text style={styles.notyTextStyle}>{this.props.mFrom.toUpperCase()}</Text>
            <FlatList
              contentContainerStyle={{ paddingBottom: hp('20%') }}
              showsVerticalScrollIndicator={false}
              data={this.state.dataToRender}
              keyExtractor={item => item.id}
              ItemSeparatorComponent={this.FlatListItemSeparator}
              extraData={this.state}
              // initialNumToRender={5}
              onEndReached={this.handleLoadMore.bind(this)}
              onEndReachedThreshold={0.5}
              bounces={false}
              // refreshing={this.state.refreshing}
              // onMomentumScrollBegin={() => {  this.setState({ onEndReachedCalledDuringMomentum: false }); }}
              renderItem={({ item }) => {
                return <TouchableOpacity style={styles.containerListStyle} onPress={() => {
                  LogUtils.firebaseEventLog('click', {
                    p_id: item.p_id,
                    p_category: 'programs',
                    p_name: item.name,
                  });
                  Actions.traineePlanNew({ pId: item.p_id });
                }}>
                  <View style={{ flexDirection: 'column', alignItems: 'flex-start', alignContent: 'flex-start', }}>
                    <View>
                      {item.workoutimg
                        ? (
                          <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={{ uri: item.workoutimg }}
                            style={{
                              width: wp('90%'),
                              height: undefined,
                              borderTopLeftRadius: 6,
                              borderTopRightRadius: 6,
                              resizeMode: 'cover',
                              aspectRatio: 16/9,
                            }}
                          />
                        )
                        : (
                          <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_noimage.png')}
                            style={{
                              width: wp('90%'),
                              height: hp('25%'),
                              borderTopLeftRadius: 6,
                              borderTopRightRadius: 6,
                              resizeMode: 'cover',
                            }}
                          />
                        )
                      }

                      <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        alignContent: 'flex-start',
                        margin: 10,
                        bottom: 0,
                        position: 'absolute',
                        justifyContent: 'flex-start',
                      }}>
                        <Text style={styles.textPRO}>{item.trainername}</Text>
                      </View>
                    </View>

                    <View style={{ width: wp('90%'), flexDirection: 'column', padding: 10, alignItems: 'flex-start', alignContent: 'flex-start', borderBottomLeftRadius: 6, borderBottomRightRadius: 6 }}>
                      <Text style={styles.textName} numberOfLines={2}>{`${item.name}`}</Text>
                      <Text style={styles.textTitle}>{`${item.durtnlvl}`}</Text>
                      <View style={{ marginTop: 2, justifyContent: 'center', flexDirection: 'row', alignSelf: 'flex-start', }}>
                        <Text style={styles.textEqpNeed} numberOfLines={1}>{`${item.equipment}`}</Text>
                        {this.renderChallengesProfiles(item)}
                      </View>
                    </View>
                  </View>
                </TouchableOpacity>
              }} />
          </View>

        );

      }
      else {
        return (
          <View style={{ flexDirection: 'column', flex: 1, alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>
            <Image
              source={require('../res/img_no_workprograms.png')}
              style={styles.imgNoSuscribe}
            />
            <Text style={styles.textNodataTitle}>No Programs</Text>

            <Text style={styles.textNodata}>There are no programs listed for your selection. Check back later</Text>
          </View>
        );
      }
    }
  }

  render() {
    return (
      <ImageBackground source={require('../res/app_bg.png')} style={styles.mainContainer}>
        <View style={styles.mainContainer}>
          <Loader loading={this.state.loading} />
          <View style={{
            flexDirection: 'row',
            margin: 20,
          }}>
            <View style={{ position: 'absolute', zIndex: 111 }}>
              <TouchableOpacity
                style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                onPress={() => this.onBackPressed()}>
                <Image
                  source={require('../res/ic_back.png')}
                  style={styles.backImageStyle}
                />
              </TouchableOpacity>
            </View>

            <View style={{ position: 'absolute', zIndex: 111, right: 0 }}>
              <TouchableOpacity
                style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                onPress={() => { this.setState({ isFilterVisible: true }) }}>
                <Image
                  source={require('../res/ic_filter.png')}
                  style={styles.filterImageStyle}
                />
              </TouchableOpacity>
            </View>
          </View>

          {this.renderFlatlist()}

          <Modal
            backdropColor="#B4B3DB"
            backdropOpacity={0.8}
            animationIn="zoomInDown"
            animationOut="zoomOutUp"
            animationInTiming={600}
            animationOutTiming={600}
            backdropTransitionInTiming={600}
            backdropTransitionOutTiming={600}
            visible={this.state.isFilterVisible}
            onRequestClose={() => { this.toggleModal(!this.state.isFilterVisible) }}>

            <View style={styles.modal}>
              <View style={styles.workOutBg}>

                <View style={{ flexDirection: "row", width: '100%', padding: 10, marginTop: 10 }}>

                  <TouchableOpacity
                    style={{ flex: 1 }}
                    onPress={() => { this.reset() }}>
                    <Text style={styles.textAdd1}>{'Reset all'.toUpperCase()}</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{ flex: 1 }}
                    onPress={() => { }}>
                    <Text style={styles.textFilters}>{'Filters'}</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{ flex: 1 }}
                    onPress={() => this.onDoneClicked()}>
                    <Text style={styles.textAdd}>{'Done'.toUpperCase()}</Text>
                  </TouchableOpacity>

                </View>

              </View>
              <ScrollView contentContainerStyle={{ paddingBottom: hp('15%') }}>
                <View style={{ width: '100%', height: '100%', backgroundColor: '#ffffff', flexDirection: 'column' }}>
                  <View style={{ flexDirection: 'column', backgroundColor: '#ffffff', borderColor: '#f2f2f2', }}>
                    <Text style={styles.textStyleSetsHeader}>
                      Goal
                        </Text>

                    <FlatList
                      style={{ margin: 10, alignSelf: 'center' }}
                      // columnWrapperStyle={{ flexWrap: 'wrap', flex: 1, marginTop: 5 }}
                      contentContainerStyle={{ paddingBottom: hp('1%') }}
                      showsVerticalScrollIndicator={false}
                      data={this.state.filterObj.goal}
                      keyExtractor={item => item.w_id}
                      numColumns={2}
                      ItemSeparatorComponent={this.FlatListItemSeparator}
                      renderItem={({ item }) => {
                        return <TouchableOpacity style={{
                          width: '45%',

                          marginRight: 5,
                          marginLeft: 5,
                          marginBottom: 7,

                        }} onPress={() => { this.pressGoal(item) }}>
                          {item.check
                            ? (
                              <View style={{
                                borderColor: '#8c52ff', borderRadius: 5,
                                borderWidth: 1,
                                height: 40,
                                justifyContent: 'center',
                                backgroundColor: '#8c52ff'
                              }}>
                                <Text style={styles.textWorkNameChecked}>{`${item.name}`}</Text>

                              </View>

                            )
                            : (

                              <View style={{
                                borderColor: '#ddd', borderRadius: 5,
                                borderWidth: 1,
                                height: 40,
                                justifyContent: 'center',
                                backgroundColor: '#ffffff'

                              }}>
                                <Text style={styles.textWorkName}>{`${item.name}`}</Text>

                              </View>

                            )}
                        </TouchableOpacity>
                      }} />
                  </View>

                  <View style={{ flexDirection: 'column', backgroundColor: '#ffffff', borderWidth: 1, borderColor: '#f2f2f2', }}>
                    <Text style={styles.textStyleSetsHeader}>
                      Location
                        </Text>

                    <FlatList
                      style={{ margin: 10, alignSelf: 'center' }}
                      columnWrapperStyle={{ flexWrap: 'wrap', flex: 1, }}
                      contentContainerStyle={{ paddingBottom: hp('1%') }}
                      showsVerticalScrollIndicator={false}
                      data={this.state.filterObj.woroutlocation}
                      keyExtractor={item => item.id}
                      numColumns={3}
                      ItemSeparatorComponent={this.FlatListItemSeparator}
                      renderItem={({ item }) => {
                        return <TouchableOpacity style={{
                          width: '45%',
                          marginRight: 5,
                          marginLeft: 5,
                          marginBottom: 7,


                        }} onPress={() => { this.pressLocation(item) }}>
                          {item.check
                            ? (
                              <View style={{
                                borderColor: '#8c52ff', borderRadius: 5,
                                borderWidth: 1,
                                height: 40,
                                justifyContent: 'center',
                                backgroundColor: '#8c52ff'
                              }}>
                                <Text style={styles.textWorkNameChecked}>{`${item.name}`}</Text>

                              </View>

                            )
                            : (

                              <View style={{
                                borderColor: '#ddd', borderRadius: 5,
                                borderWidth: 1,
                                height: 40,
                                justifyContent: 'center',
                                backgroundColor: '#ffffff'

                              }}>
                                <Text style={styles.textWorkName}>{`${item.name}`}</Text>

                              </View>

                            )}
                        </TouchableOpacity>
                      }} />
                  </View>

                  <View style={{ flexDirection: 'column', backgroundColor: '#ffffff', borderWidth: 1, borderColor: '#f2f2f2', }}>
                    <Text style={styles.textStyleSetsHeader}>
                      Fitness Level
                        </Text>

                    <FlatList
                      style={{ margin: 10, alignSelf: 'center' }}
                      // columnWrapperStyle={{ flexWrap: 'wrap', flex: 1, }}
                      contentContainerStyle={{ paddingBottom: hp('1%') }}
                      showsVerticalScrollIndicator={false}
                      data={this.state.filterObj.fitnesslevel}
                      keyExtractor={item => item.id}
                      numColumns={2}
                      ItemSeparatorComponent={this.FlatListItemSeparator}
                      renderItem={({ item }) => {
                        return <TouchableOpacity style={{
                          width: '45%',
                          marginRight: 5,
                          marginLeft: 5,
                          marginBottom: 7,

                        }} onPress={() => { this.pressfitnessLevel(item) }}>
                          {item.check
                            ? (
                              <View style={{
                                borderColor: '#8c52ff', borderRadius: 5,
                                borderWidth: 1,
                                height: 40,
                                justifyContent: 'center',
                                backgroundColor: '#8c52ff'
                              }}>
                                <Text style={styles.textWorkNameChecked}>{`${item.name}`}</Text>

                              </View>

                            )
                            : (

                              <View style={{
                                borderColor: '#ddd', borderRadius: 5,
                                borderWidth: 1,
                                height: 40,
                                justifyContent: 'center',
                                backgroundColor: '#ffffff'

                              }}>
                                <Text style={styles.textWorkName}>{`${item.name}`}</Text>

                              </View>

                            )}
                        </TouchableOpacity>
                      }} />
                  </View>

                  <View style={{ flexDirection: 'column', backgroundColor: '#ffffff', borderWidth: 1, borderColor: '#f2f2f2', }}>
                    <Text style={styles.textStyleSetsHeader}>
                      Training Type
                        </Text>

                    <FlatList
                      style={{ margin: 10, alignSelf: 'center' }}
                      // columnWrapperStyle={{ flexWrap: 'wrap', flex: 1, }}
                      contentContainerStyle={{ paddingBottom: hp('1%') }}
                      showsVerticalScrollIndicator={false}
                      data={this.state.filterObj.trainingtype}
                      keyExtractor={item => item.id}
                      numColumns={2}
                      ItemSeparatorComponent={this.FlatListItemSeparator}
                      renderItem={({ item }) => {
                        return <TouchableOpacity style={{
                          width: '45%',
                          marginRight: 5,
                          marginLeft: 5,
                          marginBottom: 7,

                        }} onPress={() => { this.presstrainingType(item) }}>
                          {item.check
                            ? (
                              <View style={{
                                borderColor: '#8c52ff', borderRadius: 5,
                                borderWidth: 1,
                                height: 40,
                                justifyContent: 'center',
                                backgroundColor: '#8c52ff'
                              }}>
                                <Text style={styles.textWorkNameChecked}>{`${item.name}`}</Text>

                              </View>

                            )
                            : (

                              <View style={{
                                borderColor: '#ddd', borderRadius: 5,
                                borderWidth: 1,
                                height: 40,
                                justifyContent: 'center',
                                backgroundColor: '#ffffff'

                              }}>
                                <Text style={styles.textWorkName}>{`${item.name}`}</Text>

                              </View>

                            )}
                        </TouchableOpacity>
                      }} />
                  </View>

                  <View style={{ flexDirection: 'column', backgroundColor: '#ffffff', borderWidth: 1, borderColor: '#f2f2f2', }}>
                    <Text style={styles.textStyleSetsHeader}>
                      Fitness Form
                        </Text>

                    <FlatList
                      style={{ margin: 10, alignSelf: 'center' }}
                      // columnWrapperStyle={{ flexWrap: 'wrap', flex: 1, }}
                      contentContainerStyle={{ paddingBottom: hp('1%') }}
                      showsVerticalScrollIndicator={false}
                      data={this.state.filterObj.fitness_form}
                      keyExtractor={item => item.id}
                      numColumns={2}
                      ItemSeparatorComponent={this.FlatListItemSeparator}
                      renderItem={({ item }) => {
                        return <TouchableOpacity style={{
                          width: '45%',
                          marginRight: 5,
                          marginLeft: 5,
                          marginBottom: 7,

                        }} onPress={() => { this.pressFitnessForm(item) }}>
                          {item.check
                            ? (
                              <View style={{
                                borderColor: '#8c52ff', borderRadius: 5,
                                borderWidth: 1,
                                height: 40,
                                justifyContent: 'center',
                                backgroundColor: '#8c52ff'
                              }}>
                                <Text style={styles.textWorkNameChecked}>{`${item.name}`}</Text>

                              </View>

                            )
                            : (

                              <View style={{
                                borderColor: '#ddd', borderRadius: 5,
                                borderWidth: 1,
                                height: 40,
                                justifyContent: 'center',
                                backgroundColor: '#ffffff'

                              }}>
                                <Text style={styles.textWorkName}>{`${item.name}`}</Text>

                              </View>

                            )}
                        </TouchableOpacity>
                      }} />
                  </View>

                  <View style={{ flexDirection: 'column', backgroundColor: '#ffffff', borderWidth: 1, borderColor: '#f2f2f2', }}>
                    <Text style={styles.textStyleSetsHeader}>
                      Equipment Required
                        </Text>

                    <FlatList
                      style={{ margin: 10, alignSelf: 'center' }}
                      // columnWrapperStyle={{ flexWrap: 'wrap', flex: 1, marginTop: 5 }}
                      contentContainerStyle={{ paddingBottom: hp('1%') }}
                      showsVerticalScrollIndicator={false}
                      data={this.state.filterObj.equipment}
                      keyExtractor={item => item.w_id}
                      numColumns={2}
                      ItemSeparatorComponent={this.FlatListItemSeparator}
                      renderItem={({ item }) => {
                        return <TouchableOpacity style={{
                          width: '45%',
                          marginRight: 5,
                          marginLeft: 5,
                          marginBottom: 7,

                        }} onPress={() => { this.pressEquipment(item) }}>
                          {item.check
                            ? (
                              <View style={{
                                borderColor: '#8c52ff', borderRadius: 5,
                                borderWidth: 1,
                                height: 40,
                                justifyContent: 'center',
                                backgroundColor: '#8c52ff'
                              }}>
                                <Text style={styles.textWorkNameChecked}>{`${item.name}`}</Text>

                              </View>

                            )
                            : (

                              <View style={{
                                borderColor: '#ddd', borderRadius: 5,
                                borderWidth: 1,
                                height: 40,
                                justifyContent: 'center',
                                backgroundColor: '#ffffff'

                              }}>
                                <Text style={styles.textWorkName}>{`${item.name}`}</Text>

                              </View>

                            )}
                        </TouchableOpacity>
                      }} />
                  </View>
                </View>
              </ScrollView>
            </View>
          </Modal>
          <CustomDialog
            visible={this.state.isAlert}
            title='Alert'
            desc={this.state.alertMsg}
            onAccept={this.onAccept.bind(this)}
            no=''
            yes='Ok' />
        </View>

      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    width: wp('100%'),
    height: hp('100%'),
  },
  notyTextStyle: {
    color: '#2d3142',
    fontFamily: 'Rubik-Medium',
    fontSize: 18,
    marginLeft: 20,
    marginBottom: 10,
    marginTop: 10,
    fontWeight: '500',
  },
  image: {
    width: 60,
    height: 60,
  },
  containerListStyle: {
    width: wp('90%'),
    marginTop: hp('1%'),
    backgroundColor: '#ffffff',
    flexDirection: 'column',
    borderColor: '#ddd',
    borderRadius: 6,
    position: 'relative',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    shadowColor: '#4075cd',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  textName: {
    fontSize: 14,
    width: wp('80%'),
    fontWeight: '500',
    paddingRight: 10,
    marginRight: 10,
    fontFamily: 'Rubik-Medium',
    color: '#282c37',
  },
  textPRO: {
    color: '#ffffff',
    fontFamily: 'Rubik-Medium',
    fontSize: 9,
    paddingTop: 1,
    paddingBottom: 1,
    paddingLeft: 10,
    paddingRight: 10,
    borderRadius: 15,
    marginLeft: 2,
    backgroundColor: '#8c52ff',
    alignSelf: 'center',
    textAlign: 'center',
    fontWeight: '500',
    lineHeight: 18,
  },
  textTitle: {
    fontSize: 12,
    fontWeight: '400',
    fontFamily: 'Rubik-Regular',
    color: '#282c37',
    marginTop: 3,
    lineHeight: 18,
  },
  backImageStyle: {
    width: 19,
    height: 16,
    marginTop: 10,
    alignSelf: 'center',
  },
  filterImageStyle: {
    width: 20,
    height: 23,
    marginTop: 10,
    alignSelf: 'center',
  },
  modal: {
    width: '100%',
    flex: 1,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
  },
  textAdd: {
    color: '#8c52ff',
    fontFamily: 'Rubik-Medium',
    alignSelf: "flex-end",
    fontSize: 14,
    fontWeight: '500',
    lineHeight: 18,
    textAlign: 'center'
  },
  textAdd1: {
    color: '#8c52ff',
    fontFamily: 'Rubik-Medium',
    alignSelf: "flex-start",
    fontSize: 14,
    fontWeight: '500',
    lineHeight: 18,
    textAlign: 'center'
  },
  textFilters: {
    color: '#282c37',
    fontFamily: 'Rubik-Medium',
    alignSelf: "center",
    fontSize: 14,
    fontWeight: '500',
    lineHeight: 18,
    textAlign: 'center'
  },
  workOutBg: {
    width: wp('100%'),
    backgroundColor: '#ffffff',
    justifyContent: 'flex-start',
    flexDirection: 'column',
    position: 'relative',
    alignSelf: 'center',
    shadowColor: 'rgba(0, 0, 0, 0.08)',
    shadowOffset: { width: 5, height: 0 },
    shadowOpacity: 0.25,
    shadowRadius: 10,
    elevation: 6,
  },
  textStyleSetsHeader: {
    fontSize: 16,
    fontWeight: '400',
    color: '#2d3142',
    fontFamily: 'Rubik-Medium',
    marginLeft: 20,
    marginBottom: 10,
    marginTop: 10,
  },
  ContainerGender: {
    width: wp('26%'),
    height: 80,
    borderRadius: 1,
    backgroundColor: '#ffffff',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    textAlign: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    flexDirection: 'column',
    margin: 10,
  },
  textWorkNameChecked: {
    fontSize: 12,
    fontWeight: '400',
    textAlign: 'center',
    alignSelf: 'center',
    fontFamily: 'Rubik-Regular',
    color: '#ffffff',
    padding: 10,
  },
  textWorkName: {
    fontSize: 12,
    fontWeight: '400',
    textAlign: 'center',
    alignSelf: 'center',
    fontFamily: 'Rubik-Regular',
    color: '#282c37',
    padding: 10,
  },
  textNodata: {
    width: wp('85%'),
    fontSize: 13,
    fontWeight: '400',
    letterSpacing: 0.2,
    fontFamily: 'Rubik-Regular',
    padding: 10,
    textAlign: 'center',
    alignSelf: 'center',
    color: '#6d819c',
    lineHeight: 18,
  },
  imgNoSuscribe: {
    width: 270,
    height: 270,
    alignSelf: 'center',
  },
  textNodataTitle: {
    width: wp('85%'),
    fontSize: 16,
    fontWeight: '500',
    letterSpacing: 0.2,
    fontFamily: 'Rubik-Medium',
    padding: 10,
    textAlign: 'center',
    alignSelf: 'center',
    color: '#282c37',
    lineHeight: 18,
  },
  textEqpNeed: {
    fontSize: 12,
    fontWeight: '400',
    fontFamily: 'Rubik-Regular',
    lineHeight: 18,
    flex: 1,
    color: '#282c37',
  },
  chaProfile: {
    width: 28,
    height: 28,
    aspectRatio: 1,
    backgroundColor: "#D8D8D8",
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: "#979797",
    borderRadius: 28 / 2,
    marginRight: 3,
    resizeMode: "cover",
    justifyContent: 'center',
  },
  chaMore: {
    flexDirection: 'row',
    backgroundColor: '#979797',
    borderRadius: 15,
    paddingLeft: 8,
    paddingTop: 4,
    marginTop: 5,
    alignSelf: 'center',
    paddingBottom: 4,
    paddingRight: 8,
  },
  chaMoreText: {
    color: '#ffffff',
    fontSize: 12,
    textAlign: 'center',
    alignSelf: 'center',
    fontFamily: 'Rubik-Regular',
    backgroundColor: '#979797',
    borderRadius: 12,
    paddingLeft: 6,
    paddingTop: 3,
    alignSelf: 'center',
    marginLeft: 5,
    paddingBottom: 3,
    paddingRight: 6,
  },
});

const mapStateToProps = state => {

  return {};
};

export default connect(
  mapStateToProps,
  {
  },
)(Trainee_AllWorkoutList);
