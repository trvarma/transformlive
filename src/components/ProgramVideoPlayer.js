/*Example of React Native Video*/
import React, { Component } from 'react';
//Import React
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image, BackHandler,
    ImageBackground,
    KeyboardAvoidingView,
    TextInput,
} from 'react-native';
import Video from 'react-native-video';
import { PLAYER_STATES } from 'react-native-media-controls';
import Orientation from 'react-native-orientation';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import { BASE_URL, SWR } from '../actions/types';
import { CustomDialog, Loader } from './common';
import AsyncStorage from '@react-native-community/async-storage';
import { AirbnbRating } from 'react-native-ratings';
import LinearGradient from 'react-native-linear-gradient';
import LogUtils from '../utils/LogUtils.js';
import {forbidFunction} from '../utils/ScreenshotUtils.js'; 

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

class ProgramVideoPlayer extends Component {
    videoPlayer;

    constructor(props) {
        super(props);
        LogUtils.showSlowLog(this);
        this.state = {
            currentTime: 0,
            duration: 0,
            isFullScreen: false,
            isLoading: true,
            paused: false,
            playerState: PLAYER_STATES.PLAYING,
            screenType: 'contain',
            onBuffer: false,

            isAlert: false,
            loading: false,
            alertMsg: '',
            resObj: '',
            isSuccess: false,
            isEndWorkout: false,
            isEndWithOutFeedback: false,
            post: '',
            rating: 3,
            isFeedbackSubmit: false,
            isBackPressed: false,

            volume: 1,
            rate: 1,
            muted: false,
        };
    }



    componentWillMount() {
        forbidFunction();
        
        // The getOrientation method is async. It happens sometimes that
        // you need the orientation at the moment the JS runtime starts running on device.
        // `getInitialOrientation` returns directly because its a constant set at the
        // beginning of the JS runtime.

        const initial = Orientation.getInitialOrientation();
        if (initial === 'PORTRAIT') {
            // do something
            LogUtils.infoLog1("orientation", initial.toString())
        } else {
            // do something else
        }
    }

    async componentDidMount() {
        // this locks the view to Portrait Mode
        // Orientation.lockToPortrait();

        // this locks the view to Landscape Mode
        // Orientation.lockToLandscapeRight();

        // this unlocks any previous locks to all Orientations
        Orientation.unlockAllOrientations();

        Orientation.addOrientationListener(this._orientationDidChange);
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
        LogUtils.infoLog1('this.props.planObj',this.props.planObj);



    }

    async onBackPressed() {
        if (!this.state.isEndWorkout) {
            Orientation.lockToPortrait();
            this.setState({ isBackPressed: true });
        }
    }

    _orientationDidChange = (orientation) => {
        if (orientation === 'LANDSCAPE') {
            LogUtils.infoLog(`Changed Device Orientation: ${orientation}`);
            // do something with landscape layout
            Orientation.lockToLandscape();
        } else {
            // do something with portrait layout
            LogUtils.infoLog(`Changed Device Orientation: ${orientation}`);
            Orientation.lockToPortrait();
        }
    }

    _orientationRemove = (orientation) => {
        orientation.lockToPortrait();
    }

    componentWillUnmount() {
        this.backHandler.remove();

        Orientation.getOrientation((err, orientation) => {
            LogUtils.infoLog(`Current Device Orientation: ${orientation}`);
        });


        // Remember to remove listener
        Orientation.removeOrientationListener(this._orientationRemove);
    }

    onSeek = seek => {
        //Handler for change in seekbar
        this.videoPlayer.seek(seek);
    };

    onPaused = playerState => {
        //Handler for Video Pause
        this.setState({
            paused: !this.state.paused,
            playerState,
        });
    };

    onReplay = () => {
        //Handler for Replay
        this.setState({ playerState: PLAYER_STATES.PLAYING });
        this.videoPlayer.seek(0);
    };

    onProgress = data => {
        const { isLoading, playerState } = this.state;
        // Video Player will continue progress even if the video already ended
        if (!isLoading && playerState !== PLAYER_STATES.ENDED) {
            this.setState({ currentTime: data.currentTime });
        }
    };

    onAudioBecomingNoisy = () => {
        this.setState({ paused: true })
    };

    onLoad = data => { LogUtils.infoLog(data.duration); this.setState({ duration: data.duration, isLoading: false, loading: false }); }

    onLoadStart = data => {
        this.videoPlayer.presentFullscreenPlayer();
        this.setState({ isLoading: true, loading: true })
    };

    onEnd = () => {
        this.setState({ playerState: PLAYER_STATES.ENDED });
        this.saveEndWorkOut();
       
        if(this.props.label)
        {
            LogUtils.firebaseEventLog('video', {
                p_id:  this.props.planObj.p_id,
                p_category: 'Programs',
                p_name: this.props.label,
              });
        }

    }

    onError = () => alert('Oh! ', error);

    exitFullScreen = () => {
        alert('Exit full screen');
    };

    enterFullScreen = () => { };

    onFullScreen = () => {
        if (this.state.screenType == 'content')
            this.setState({ screenType: 'cover' });
        else this.setState({ screenType: 'content' });
    };
    // onBuffer(data) {
    //     this.setState({ isBuffering: data.isBuffering });
    // }

    renderToolbar = () => (
        <View>
            <Text> toolbar </Text>
        </View>
    );
    onSeeking = currentTime => this.setState({ currentTime });

    async saveEndWorkOut() {
        this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/endworkout`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    pd_id: this.props.pdObj.pd_id,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {

                    if (this.props.planObj.fb_done === 0)
                        this.setState({ loading: false, isEndWorkout: true });
                    else
                        this.setState({ loading: false, isEndWithOutFeedback: true, isAlert: true, alertMsg: data.message, });
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, isEndWorkout: true });

                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, isEndWorkout: true });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: SWR, isEndWorkout: true });
            });
    }

    async saveFeebackWorkOut() {
        this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/savedayfdback`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    pd_id: this.props.pdObj.pd_id,
                    rating: this.state.rating,
                    comments: this.state.post,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false, isFeedbackSubmit: true })
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });

                    } else {
                        this.setState({ loading: false, isFeedBackError: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isFeedBackError: true, alertMsg: SWR });
            });
    }

    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }

        this.setState({ isAlert: false, alertMsg: '' });

        if (this.state.isEndWithOutFeedback) {
            this.setState({ isEndWithOutFeedback: false });
            LogUtils.infoLog1('pId', this.props.pdObj.pg_id);
            if (this.props.from === 'home')
                Actions.traineePlanNew({ pId: this.props.p_id, from: 'home' });
            else
                Actions.traineePlanNew({ pId: this.props.p_id, from: 'subscribe' });
        }

    }

    async onFeedbackSubmit() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
        this.setState({ isFeedbackSubmit: false, alertMsg: '' });
        LogUtils.infoLog1('pId', this.props.pdObj.pg_id);
        if (this.props.from === 'home')
            Actions.traineePlanNew({ pId: this.props.p_id, from: 'home' });
        else
            Actions.traineePlanNew({ pId: this.props.p_id, from: 'subscribe' });
    }

    onEndWorkout() {
        this.setState({ isBackPressed: false, paused: true });
        this.saveEndWorkOut();
    }

    renderProfileImg() {
        if (this.props.planObj.tr_img) {
            return (
                <Image
                    source={{ uri: this.props.planObj.tr_img }}
                    style={styles.profileImage}
                />
            );
        }
        else {
            return (
                <Image
                    source={require('../res/ic_profile_gray.png')}
                    style={styles.profileImage}
                />
            );
        }
    }

    ratingCompleted(rating) {
        this.setState({ rating: rating })
        switch (rating) {
            case 1:
                this.setState({ ratingText: 'Bad experience !' })
                break;
            case 2:
                this.setState({ ratingText: 'Not that great !' })
                break;
            case 3:
                this.setState({ ratingText: 'It was good !' })
                break;
            case 4:
                this.setState({ ratingText: 'Had a great experience !' })
                break;
            case 5:
                this.setState({ ratingText: 'Absolutely superb !' })
                break;
            default:
                this.setState({ ratingText: 'Give rating !' })
                break
        }
    }

    render() {
        if (!this.state.isEndWorkout) {
            return (
                <View style={{ flex: 1 }}>
                    <Loader loading={this.state.loading} />
                    <View style={styles.container}>

                        <Video
                            ref={videoPlayer => (this.videoPlayer = videoPlayer)}
                            /* For ExoPlayer */
                            /* source={{ uri: 'http://www.youtube.com/api/manifest/dash/id/bf5bb2419360daf1/source/youtube?as=fmp4_audio_clear,fmp4_sd_hd_clear&sparams=ip,ipbits,expire,source,id,as&ip=0.0.0.0&ipbits=0&expire=19000000000&signature=51AF5F39AB0CEC3E5497CD9C900EBFEAECCCB5C7.8506521BFC350652163895D4C26DEE124209AA9E&key=ik0', type: 'mpd' }} */
                            source={{ uri: this.props.planObj.video_url }}
                            style={styles.fullScreen}
                            rate={this.state.rate}
                            paused={this.state.paused}
                            volume={this.state.volume}
                            muted={this.state.muted}
                            resizeMode={this.state.screenType}
                            onLoad={this.onLoad}
                            onLoadStart={this.onLoadStart}
                            onProgress={this.onProgress}
                            onEnd={this.onEnd}
                            onAudioBecomingNoisy={this.onAudioBecomingNoisy}
                            // onBuffer={this.onBuffer}
                            // onAudioFocusChanged={this.onAudioFocusChanged}
                            repeat={false}
                            controls={true}

                        />

                        {/* <Video
                            onEnd={this.onEnd}
                            onLoad={this.onLoad}
                            onLoadStart={this.onLoadStart}
                            onProgress={this.onProgress}
                            paused={this.state.paused}
                            ref={videoPlayer => (this.videoPlayer = videoPlayer)}
                            resizeMode={this.state.screenType}
                            onFullScreen={this.state.isFullScreen}
                            source={{ uri: this.props.planObj.video_url }}
                            style={styles.mediaPlayer}
                            volume={10}
                            hls={true}
                        /> */}
                        {/* <MediaControls
                            duration={this.state.duration}
                            isLoading={this.state.isLoading}
                            mainColor="#333"
                            //   onFullScreen={this.onFullScreen}
                            onPaused={this.onPaused}
                            onReplay={this.onReplay}
                            onSeek={this.onSeek}
                            onSeeking={this.onSeeking}
                            playerState={this.state.playerState}
                            progress={this.state.currentTime}
                        //   toolbar={this.renderToolbar()}
                        />
                        <View style={styles.containerTop}>
                            <TouchableOpacity
                                onPress={() => this.onBackPressed()}>
                                <Image
                                    source={require('../res/ic_back.png')}
                                    style={styles.backImageStyle}
                                />
                            </TouchableOpacity>
                        </View> */}

                    </View>

                    <CustomDialog
                        visible={this.state.isAlert}
                        title='Alert'
                        desc={this.state.alertMsg}
                        onAccept={this.onAccept.bind(this)}
                        no=''
                        yes='Ok' />

                    <CustomDialog
                        visible={this.state.isBackPressed}
                        title='Alert'
                        desc={'Do you want End Workout ?'}
                        onAccept={this.onEndWorkout.bind(this)}
                        onDecline={() => this.setState({ isBackPressed: false })}
                        no='No'
                        yes='Yes' />
                </View>

            );
        }
        else {
            return (
                <KeyboardAvoidingView keyboardVerticalOffset={Platform.select({ ios: 0, android: -70 })} style={styles.mainContainer} behavior="padding" enabled>
                    <ImageBackground source={require('../res/post_back.png')} style={styles.mainImageContainer}>
                        <View style={styles.postContents}>
                            <Text style={styles.createPostStyle}>RATE YOUR WORKOUT</Text>

                            {this.renderProfileImg()}

                            <Text style={styles.feedbackNameStyle}>{this.props.planObj.tr_name}</Text>
                            <Text style={styles.feedbackSubStyle}>{this.props.planObj.title}</Text>

                            <AirbnbRating
                                // showRating
                                type='star'
                                ratingCount={5}
                                reviews={['Bad experience !', 'Not that great !', 'It was good !', 'Had a great experience !', 'Absolutely superb !']}
                                imageSize={30}
                                startingValue={this.state.rating}
                                readonly={false}
                                isDisabled={false}
                                reviewSize={15}
                                onFinishRating={this.ratingCompleted.bind(this)}
                                style={{ marginTop: hp('5%'), alignSelf: 'center', paddingVertical: 5 }}
                            />

                            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearSubmitGradient}>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.saveFeebackWorkOut();
                                    }}
                                    style={styles.buttonTuch}>
                                    <Text style={styles.buttonText}>SUBMIT</Text>
                                </TouchableOpacity>
                            </LinearGradient>

                            <View style={styles.inputRow}>
                                <TextInput
                                    style={styles.textInputStyle}
                                    placeholder="Type your review..."
                                    placeholderTextColor="#9c9eb9"
                                    value={this.state.post}
                                    numberOfLines={2}
                                    multiline={true}
                                    onChangeText={text => this.setState({ post: text })}
                                />
                            </View>
                        </View>

                        <CustomDialog
                            visible={this.state.isFeedbackSubmit}
                            title='Success'
                            desc={'Feedback submitted successfully.'}
                            onAccept={this.onFeedbackSubmit.bind(this)}
                            no=''
                            yes='Ok' />

                    </ImageBackground>
                </KeyboardAvoidingView>
            );
        }
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'black',
    },
    toolbar: {
        marginTop: 30,
        backgroundColor: 'white',
        padding: 10,
        borderRadius: 5,
    },
    mediaPlayer: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        backgroundColor: 'black',
    },
    containerTop: {
        flexDirection: 'column',
        position: 'absolute',
        top: 0,
    },
    backImageStyle: {
        width: 20,
        height: 16,
        margin: hp('3%'),
        alignSelf: 'flex-start',
        tintColor: 'white',
    },
    mainImageContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
        justifyContent: 'center',
    },
    mainContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    postContents: {
        width: '90%',
        alignSelf: 'center',
        backgroundColor: '#fff',
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12,
        alignContent: 'center',
        justifyContent: 'center',
        padding: 23,
        marginBottom: 10
    },
    createPostStyle: {
        height: 30,
        alignSelf: 'center',
        color: '#5d5d5d',
        fontFamily: 'Rubik-Medium',
        fontSize: 14,
        fontWeight: '500',
        lineHeight: 30,
    },
    profileImage: {
        width: 150,
        height: 150,
        aspectRatio: 1,
        alignSelf: 'center',
        backgroundColor: "#D8D8D8",
        marginTop: hp('2%'),
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: "#979797",
        borderRadius: 150 / 2,
        resizeMode: "cover",
        justifyContent: 'flex-end',
    },
    feedbackNameStyle: {
        alignSelf: 'center',
        marginTop: hp('2%'),
        color: '#5a5a5a',
        fontFamily: 'Rubik-Medium',
        fontSize: 14,
        fontWeight: '500',
        lineHeight: 30,
    },
    feedbackSubStyle: {
        alignSelf: 'center',
        color: '#5d5d5d',
        fontFamily: 'Rubik-Medium',
        fontSize: 10,
        fontWeight: '500',
    },
    inputRow: {
        flexDirection: 'row',
        alignItems: "flex-start",
        height: '18%',
        marginTop: hp('2%'),
        marginBottom: hp('3%'),
        borderRadius: 10,
        backgroundColor: '#f3f4f6',
    },
    linearSubmitGradient: {
        width: '90%',
        height: 50,
        bottom: 10,
        borderRadius: 15,
        justifyContent: 'center',
        position: 'absolute',
        alignSelf: 'center',
    },
    buttonTuch:
    {
        backgroundColor: 'transparent',
    },
    buttonText: {
        fontSize: 14,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    textInputStyle: {
        width: wp('75%'),
        color: '#2d3142',
        marginLeft: 5,
        fontFamily: 'Rubik-Regular',
        fontSize: 12,
        fontWeight: '400',
        letterSpacing: 0.23,
    },
    fullScreen: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
});
export default ProgramVideoPlayer;