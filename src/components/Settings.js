import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    BackHandler,
    FlatList,
    Platform
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { NoInternet, CustomDialog, Loader } from './common';
import { withNavigationFocus } from 'react-navigation';
import LogUtils from '../utils/LogUtils.js';
import {allowFunction} from '../utils/ScreenshotUtils.js';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import DeviceInfo from 'react-native-device-info';
import NetInfo from "@react-native-community/netinfo";
import { BASE_URL } from '../actions/types';
function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

class Settings extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            isInternet: false,
            arrAllSettings: [],
            isAlert: false,
            alertMsg: '',
            appVersion: '',
        };
    }

    async componentDidMount() {

        let version = DeviceInfo.getVersion();
        this.setState({ appVersion: version });

        // forbidFunction();

        allowFunction();

        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.getAllSettings();


            }
            else {
                this.setState({ isInternet: true });
            }
        });


        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    async getAllSettings() {
        this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/settings`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    if (data.data.length > 0) {
                        this.setState({ loading: false, arrAllSettings: data.data });
                    }
                    else {
                        this.setState({ loading: false, });
                    }

                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
            });
    }


    onBackPressed() {
        Actions.popTo('traineeHome');
    }

    async onDeny() {
        this.setState({ isSuccess: false, sucMsg: '', titMsg: '' });
    }

    async onSuccess() {
        this.setState({ isSuccess: false, sucMsg: '', titMsg: '' });
        try {

            await AsyncStorage.clear().then(() =>
                Actions.splash({ type: 'reset' }),
            );
        } catch (error) {
            //console.log(error.message);
        }
    }

    renderVersion() {
        // if (Platform.OS === 'android') {
        //     if (this.props.android) {
                return (
                    <View style={{ position: 'absolute', right: 10, justifyContent: 'center', alignSelf: 'center' }}>
                        <Text style={styles.setText1}>
                            {this.state.appVersion}
                        </Text>
                    </View>

                );
        //     }

        // } else if (Platform.OS === 'ios') {
        //     if (this.props.ios) {
        //         return (
        //             <View style={{ position: 'absolute', right: 10, justifyContent: 'center', alignSelf: 'center' }}>
        //                 <Text style={styles.setText1}>
        //                     {this.props.ios}
        //                 </Text>
        //             </View>

        //         );
        //     }
        // }
    }

    async onRetry() {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isInternet: false, });
                this.getAllSettings();
            }
            else {
                this.setState({ isInternet: true, });
            }
        });
    }

    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
        this.setState({ isAlert: false, alertMsg: '' });
    }

    footer = () => {
        return (
            <View style={styles.performanceRow}>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.setText}>
                        Version
            </Text>
                    {this.renderVersion()}

                </View>
                <View style={{ height: 1, backgroundColor: '#e9e9e9' }}></View>
            </View>
        );
    };

    renderSettingsData() {
        if (Array.isArray(this.state.arrAllSettings) && this.state.arrAllSettings.length) {
            return (
                <View>
                    <FlatList
                        contentContainerStyle={{ paddingBottom: hp('13%') }}
                        showsVerticalScrollIndicator={false}
                        data={this.state.arrAllSettings}
                        keyExtractor={(item,index) => "arrAllSettings"+index}
                        ListFooterComponent={this.footer}
                        ItemSeparatorComponent={this.FlatListItemSeparator1}
                        ListEmptyComponent={this.ListEmptyView}
                        renderItem={({ item }) => {
                            return <TouchableOpacity style={styles.performanceRow} onPress={() => Actions.setDetail({ from: '', url: item.url })}>
                                <View style={styles.performancecolumn}>
                                    <Text style={styles.setText}>
                                        {item.title}
                                    </Text>
                                    <View style={{ height: 1, backgroundColor: '#e9e9e9' }}></View>
                                </View>
                            </TouchableOpacity>
                        }} />
                </View>
            );
        }
        else {
            return (
                <View style={styles.performanceRow}>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.setText}>
                            Version
            </Text>
                        {this.renderVersion()}

                    </View>
                    <View style={{ height: 1, backgroundColor: '#e9e9e9' }}></View>
                </View>
            );
        }
    }

    render() {
        return (
            <View style={styles.containerStyle}>

                {/* <ScrollView> */}
                <Loader loading={this.state.loading} />

                <View style={styles.viewTop}>

                    <TouchableOpacity
                        style={{ width: 40, height: 40 }}
                        onPress={() => this.onBackPressed()}>
                        <Image
                            source={require('../res/ic_back.png')}
                            style={styles.backImageStyle}
                        />
                    </TouchableOpacity>
                    <Text style={styles.notyTextStyle}>Settings</Text>

                    <View style={{ marginTop: 15, }}></View>


                    {this.renderSettingsData()}

                    {/* <TouchableOpacity style={styles.performanceRow} onPress={() => Actions.setDetail({ from: 'about' })}>
                        <View style={styles.performancecolumn}>
                            <Text style={styles.setText}>
                                About Us
                                </Text>
                            <View style={{ height: 1, backgroundColor: '#e9e9e9' }}></View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.performanceRow} onPress={() => Actions.setDetail({ from: 'tc' })}>
                        <View style={styles.performancecolumn}>
                            <Text style={styles.setText}>
                                Terms and Conditions
                                </Text>
                            <View style={{ height: 1, backgroundColor: '#e9e9e9' }}></View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.performanceRow}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={styles.setText}>
                                Version
                                </Text>
                            {this.renderVersion()}

                        </View>
                        <View style={{ height: 1, backgroundColor: '#e9e9e9' }}></View>
                    </TouchableOpacity> */}

                </View>
                {/* </ScrollView> */}

                <NoInternet
                    image={require('../res/img_nointernet.png')}
                    loading={this.state.isInternet}
                    onRetry={this.onRetry.bind(this)} />

                <CustomDialog
                    visible={this.state.isAlert}
                    title='Alert'
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    no=''
                    yes='Ok' />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    containerStyle: {
        width: wp('100%'),
        height: hp('100%'),
        flex: 1,
        backgroundColor: '#ffffff',
    },
    viewTop: {
        width: '100%',
        flexDirection: 'column',
        padding: 20,
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'flex-start',
    },
    notyTextStyle: {
        color: '#2d3142',
        fontFamily: 'Rubik-Medium',
        fontSize: 24,
        marginTop: 15,
        letterSpacing: 0.8,
        fontWeight: '500',
    },
    performanceRow: {
        flexDirection: 'column',
        justifyContent: 'flex-start',
    },
    setText: {
        fontSize: 13,
        fontWeight: '500',
        color: '#2d3142',
        marginTop: 15,
        marginBottom: 15,
        textAlign: 'left',
        fontFamily: 'Rubik-Medium',
    },
    setText1: {
        fontSize: 13,
        fontWeight: '500',
        color: '#8c52ff',
        marginTop: 15,
        marginBottom: 15,
        textAlign: 'left',
        fontFamily: 'Rubik-Medium',
    },

});

const mapStateToProps = state => {

    //   const { goals, trainerCodes } = state.procre;
    const loading = state.procre.loading;
    const error = state.procre.error;
    return { loading, error, };

};

export default withNavigationFocus(
    connect(
        mapStateToProps,
        {},
    )(Settings),
);

// export default ProfileCreation;