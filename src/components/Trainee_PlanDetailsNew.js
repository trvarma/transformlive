import React, { Component } from 'react';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  BackHandler,
  ScrollView,
  FlatList,
  Dimensions,
  KeyboardAvoidingView, StatusBar,
  Animated,
  ActivityIndicator,
  TextInput
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import { CustomDialog, Loader } from './common';
import LinearGradient from 'react-native-linear-gradient';
import { Rating } from 'react-native-ratings';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import NetInfo from "@react-native-community/netinfo";
import { BASE_URL, SWR } from '../actions/types';
const sliderheight = Dimensions.get('window').height;
import DeviceInfo from 'react-native-device-info';
import LogUtils from '../utils/LogUtils.js';
import _ from 'lodash';
import Dialog, {
  DialogContent,
} from 'react-native-popup-dialog';

import RBSheet from "react-native-raw-bottom-sheet";
let screenHeight = Math.round(Dimensions.get('window').height);
let popupHeight = screenHeight / 2;
import { allowFunction } from '../utils/ScreenshotUtils.js';

function processResponse(response) {
  const statusCode = response.status;
  const data = response.json();
  return Promise.all([statusCode, data]).then(res => ({
    statusCode: res[0],
    data: res[1],
  }));
}
function isEmpty(obj) {
  for (var key in obj) {
    if (obj.hasOwnProperty(key))
      return false;
  }
  return true;
}

const initialLayout = { width: Dimensions.get('window').width };
let colors = ['#CFD7E2', '#E0CBC8', '#DEE2D3', '#D4D3DB'];//, '#EADACB'

class Trainee_PlanDetailsNew extends Component {
  constructor(props) {
    super(props);
    LogUtils.showSlowLog(this);
    this.state = {
      index: 0,
      setIndex: 0,
      routes: [
        { key: 'first', title: 'TRAINER' },
        { key: 'second', title: 'INTRODUCTION' },
        { key: 'third', title: 'PLAN DETAIL' },
      ],
      tab: 1,
      isAlert: false,
      loading: false,
      isSubTrue: false,
      alertMsg: '',
      alertTitle: '',
      resObj: '',
      width: Dimensions.get('window').width,
      height: Dimensions.get('window').height,
      arrAllPlans: [],
      isWeekPopup: false,
      selSubWeekObj: {},

      bounceValue: new Animated.Value(1000),  //This is the initial position of the subview
      confirmSuccess: false,
      freeTrialObj: {},
      buttonState: '',
      freeTrialSuccessTitle: '',
      freeTrialSuccessMessage: '',
      onConfirmClick: false,
      freeTrialConfirmImg: '',
      freeTrialConfirmTitle: '',
      freeTrialConfirmText: '',
      popuploading: false,

      email: '',
      freeTrailProcess: 1,
      emailOtp: '',
      emailSuccessTitle: '',
      emailSuccessDesc: '',


    };

    this.onLayout = this.onLayout.bind(this);
    this.getTraineeAndPlanDetails = this.getTraineeAndPlanDetails.bind(this);
  }

  onLayout(e) {
    this.setState({
      width: Dimensions.get('window').width,
      height: Dimensions.get('window').height,
    });
  }


  async subscribeUserWorkOut() {
    this.setState({ loading: true });
    let token = await AsyncStorage.getItem('token');
    LogUtils.infoLog1('Body', JSON.stringify({
      pg_id: this.props.pId
    }))
    fetch(
      `${BASE_URL}/trainee/programsubscr`,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
          pg_id: this.props.pId
        }),
      },
    )
      .then(processResponse)
      .then(res => {
        const { statusCode, data } = res;
        LogUtils.infoLog(data);
        if (statusCode >= 200 && statusCode <= 300) {
          this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title, isSubTrue: true });
        } else {
          if (data.message === 'You are not authenticated!') {
            this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
          } else {
            this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
          }
        }
      })
      .catch(function (error) {
        this.setState({ loading: false, isAlert: true, alertMsg: SWR, alertTitle: 'Alert' });
      });
  }

  async componentDidMount() {

    allowFunction();

    if (popupHeight < 370) {
      popupHeight = 370;
    }

    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.setState({ loading: true });
        LogUtils.infoLog1('pId', this.props.pId)

        this.getTraineeAndPlanDetails();
      }
      else {
        this.setState({ isAlert: true, alertMsg: 'Please enable your internet connection and try again' });
      }
    });

    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      this.onBackPressed();
      return true;
    });
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  async getTraineeAndPlanDetails() {
    let token = await AsyncStorage.getItem('token');
    LogUtils.infoLog1('URL', `${BASE_URL}/trainee/workoutschedule`);
    LogUtils.infoLog1('Body', JSON.stringify({
      pid: this.props.pId,
    }))
    fetch(
      `${BASE_URL}/trainee/workoutschedule`,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
          pid: this.props.pId,
        }),
      },
    )
      .then(processResponse)
      .then(res => {
        const { statusCode, data } = res;
        LogUtils.infoLog1('statusCode', statusCode);
        LogUtils.infoLog1('ProgramDetails data', data);
        LogUtils.infoLog1('Curr Day ', data.data.current_day);

        if (statusCode >= 200 && statusCode <= 300) {
          this.setState({ loading: false, resObj: data.data, freeTrialConfirmImg: data.data.ftc_image, freeTrialConfirmTitle: data.data.ftc_title, freeTrialConfirmText: data.data.ftc_text });
          this.setState({ email: data.data.email });
          // this.setState({ selSubWeekObj: this.state.resObj.week_arr[0] });
          this.state.resObj.week_arr.map((item) => {
            if (item.isExpanded) {
              this.setState({ selSubWeekObj: item });
            }
          })

          if (this.props.from) {
            if (this.props.from === 'endWork') {
              this.setState({ index: 2 });
            }
          }

        } else {
          if (data.message === 'You are not authenticated!') {
            this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title, });
          } else {
            this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title, });
          }
        }
      })
      .catch(function (error) {
        this.setState({ loading: false, isAlert: true, alertMsg: SWR, alertTitle: data.title, });
      });
  }

  async saveFreeTrial() {
    let token = await AsyncStorage.getItem('token');
    let urlType = '';
    if (this.state.resObj.show_free_call === 1) {
      urlType = 'save_freetrial2';
    }
    else {
      urlType = 'save_freetrial';
    }

    LogUtils.infoLog(`Url : ${BASE_URL}/trainee/${urlType}`);

    fetch(
      `${BASE_URL}/trainee/${urlType}`,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
          otp: this.state.emailOtp,
        }),
      },
    )
      .then(processResponse)
      .then(res => {
        const { statusCode, data } = res;
        LogUtils.infoLog1('statusCode', statusCode);
        LogUtils.infoLog1('data', data);
        if (statusCode >= 200 && statusCode <= 300) {
          this._toggleSubview();
          this.setState({ popuploading: false, freeTrialObj: data, confirmSuccess: true, freeTrialSuccessTitle: data.title, freeTrialSuccessMessage: data.message, freeTrailProcess: 4 });
          if (this.state.resObj.show_free_call === 1) {
            LogUtils.appsFlyerEventLog('freeconsultationstarted', {
              desc: 'Free Consultation Started',
            });
          }
          else {
            LogUtils.appsFlyerEventLog('freetrialstarted', {
              desc: 'Free Trial Started',
            });
          }
        } else {
          if (data.message === 'You are not authenticated!') {
            this.setState({ popuploading: false, isAlert: true, alertMsg: data.message, onConfirmClick: false });
          } else {
            this.setState({ popuploading: false, isAlert: true, alertMsg: data.message, onConfirmClick: false });
          }
        }
      })
      .catch(function (error) {
        this.setState({ popuploading: false, isAlert: true, alertMsg: JSON.stringify(error), onConfirmClick: false });
      });
  }

  async verifyEmailOtp() {
    let token = await AsyncStorage.getItem('token');
    fetch(
      `${BASE_URL}/trainee/verifyemailotp`,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
          email: this.state.email,
        }),
      },
    )
      .then(processResponse)
      .then(res => {
        const { statusCode, data } = res;
        LogUtils.infoLog1('statusCode', statusCode);
        LogUtils.infoLog1('data', data);
        if (statusCode >= 200 && statusCode <= 300) {
          this._toggleSubview();
          this.setState({ popuploading: false, confirmSuccess: true, emailSuccessTitle: data.title, emailSuccessDesc: data.message, freeTrailProcess: 3, onConfirmClick: false });
          // this.setState({ popuploading: false, freeTrialObj: data.data,confirmSuccess: true, freeTrialSuccessTitle:  data.data.title, freeTrialSuccessMessage:  data.data.message, });

        } else {
          if (data.message === 'You are not authenticated!') {
            this.setState({ popuploading: false, isAlert: true, alertMsg: data.message, onConfirmClick: false });
          } else {
            this.setState({ popuploading: false, isAlert: true, alertMsg: data.message, onConfirmClick: false });
          }
        }
      })
      .catch(function (error) {
        this.setState({ popuploading: false, isAlert: true, alertMsg: JSON.stringify(error), onConfirmClick: false });
      });
  }


  ratingCompleted(rating) {
    LogUtils.infoLog1("Rating is: ", rating)
  }


  async onBackPressed() {
    this.setState({ isWeekPopup: false, });
    if (this.props.from === 'subscribe' || this.props.from === 'endWork') {
      Actions.traineeWorkoutsHome();
    }
    else {
      Actions.pop();
    }
  }

  FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 2,
          width: "100%",
          backgroundColor: "transparent",
        }}
      />
    );
  }

  weeklistSuperator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "100%",
          backgroundColor: '#b0b0b0',
        }}
      />
    );
  }

  async onAccept() {
    if (this.state.alertMsg === 'You are not authenticated!') {
      AsyncStorage.clear().then(() => {
        Actions.auth({ type: 'reset' });
      });
    }

    this.setState({ isAlert: false, alertMsg: '' });
    if (this.state.isSubTrue) {
      this.setState({ isSubTrue: false, loading: true });
      this.getTraineeAndPlanDetails();
    }
  }

  FirstRoute = () => (
    <ScrollView contentContainerStyle={{ paddingBottom: hp('12%') }}>
      {this.state.resObj && !isEmpty(this.state.resObj)
        ?
        (
          <View>
            {this.state.resObj.trainer_video
              ?
              (
                <TouchableOpacity
                  onPress={() => {
                    LogUtils.firebaseEventLog('video', {
                      p_id: this.props.pId,
                      p_category: 'Programs',
                      p_name: `Start - ${this.state.resObj.trainername} Intro Video`,
                    });

                    if (Platform.OS === 'android') {
                      Actions.newvideoplay({ videoURL: this.state.resObj.trainer_video, from: 'intro', label: `End - ${this.state.resObj.trainername} Intro Video` });
                      // Actions.customPlayer({ videoURL: this.state.resObj.trainer_video, from: 'intro', label: `End - ${this.state.resObj.trainername} Intro Video` });
                    } else {
                      Actions.afPlayer({ videoURL: this.state.resObj.trainer_video, from: 'intro', label: `End - ${this.state.resObj.trainername} Intro Video` });
                    }
                  }}>
                  {this.state.resObj.profile_img
                    ? (
                      <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={{ uri: this.state.resObj.profile_img }}
                        style={styles.imgTrainer}
                      />
                    )
                    : (
                      <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={require('../res/ic_noimage.png')}
                        style={[styles.imgTrainer, { resizeMode: 'contain', }]}
                      />
                    )
                  }


                  <View style={styles.playContainer}>
                    <TouchableOpacity
                      style={{ flexDirection: 'row' }}
                      onPress={() => {

                        LogUtils.firebaseEventLog('video', {
                          p_id: this.props.pId,
                          p_category: 'Programs',
                          p_name: `Start - ${this.state.resObj.trainername} Intro Video`,
                        });

                        if (Platform.OS === 'android') {
                          Actions.newvideoplay({ videoURL: this.state.resObj.trainer_video, from: 'intro', label: `End - ${this.state.resObj.trainername} Intro Video` });
                          // Actions.customPlayer({ videoURL: this.state.resObj.trainer_video, from: 'intro', label: `End - ${this.state.resObj.trainername} Intro Video` });
                        } else {
                          Actions.afPlayer({ videoURL: this.state.resObj.trainer_video, from: 'intro', label: `End - ${this.state.resObj.trainername} Intro Video` });
                        }
                      }}>
                      <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={require('../res/ic_play_purple.png')}
                        style={styles.playIcon} />
                    </TouchableOpacity>
                  </View>

                </TouchableOpacity>
              )
              :
              (
                <View>
                  {this.state.resObj.profile_img
                    ? (
                      <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={{ uri: this.state.resObj.profile_img }}
                        style={styles.imgTrainer}
                      />
                    )
                    : (
                      <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={require('../res/ic_noimage.png')}
                        style={[styles.imgTrainer, { resizeMode: 'contain', }]}
                      />
                    )
                  }

                </View>
              )

            }

            <View style={styles.trainerInfoContainer}>
              <View style={styles.viewFollow}>
                <View style={{ flexDirection: 'column', alignSelf: 'flex-start', width: wp('73%') }}>
                  <Text style={styles.trainerName}>{this.state.resObj.trainername}</Text>
                  <Text style={styles.textSpecial}>{this.state.resObj.special_in}</Text>
                  <Text style={styles.textSpecial}>{this.state.resObj.tr_fitness_form}</Text>
                </View>

                <View style={styles.calViewDet}>
                  <View style={styles.viewHeight}>
                    <Text style={styles.textHeight}>FOLLOWERS</Text>
                    <Text style={styles.textHeightValue}>{this.state.resObj.follow}</Text>
                  </View>
                  <View style={{ marginTop: 10 }}></View>
                  <View style={styles.viewHeight}>
                    <Text style={styles.textHeight}>RATING</Text>
                    <View style={{
                      flexDirection: 'row',
                      justifyContent: 'flex-start'
                    }}>
                      <Rating
                        // showRating
                        type='star'
                        ratingCount={5}
                        reviews={[]}
                        startingValue={this.state.resObj.rating}
                        imageSize={13}
                        readonly={true}
                        isDisabled={true}
                        // onFinishRating={this.ratingCompleted}
                        style={{ marginTop: 0, alignSelf: 'center', paddingVertical: 2 }}
                      />
                    </View>
                  </View>

                </View>
              </View>

              {/* <View style={styles.viewHeightRatings}>
          <View style={styles.viewHeight}>
            <Text style={styles.textHeight}>EXPERIENCE</Text>
            <Text style={styles.textHeightValue}>{this.state.resObj.experience} Years</Text>
          </View>
          <View style={styles.viewHeight}>
            <Text style={styles.textHeight}>AGE</Text>
            <Text style={styles.textHeightValue}>{this.state.resObj.age}</Text>
          </View>
          <View style={styles.viewHeight}>
            <Text style={styles.textHeight}>RATING</Text>
            <View style={{
              flexDirection: 'row',
              justifyContent: 'flex-start'
            }}>
              <Rating
                // showRating
                type='star'
                ratingCount={5}
                reviews={[]}
                startingValue={this.state.resObj.rating}
                imageSize={13}
                readonly={true}
                isDisabled={true}
                // onFinishRating={this.ratingCompleted}
                style={{ marginTop: 0, alignSelf: 'center', paddingVertical: 2 }}
              />
            </View>
          </View>
        </View> */}

              <View style={{ width: wp('90%'), height: 15, }}></View>
              <View style={styles.viewDesc}>
                <Text style={styles.textHeight}>DESCRIPTION</Text>
                <Text style={styles.textDescValue}>{this.state.resObj.about}</Text>
                <View style={{ marginTop: 10 }}></View>

                {this.state.resObj.certification
                  ?
                  (
                    <View>
                      <Text style={styles.textHeight}>CERTIFICATIONS</Text>
                      <Text style={styles.textDescValue}>{this.state.resObj.certification}</Text>
                    </View>
                  )
                  :
                  (
                    <View></View>
                  )
                }

              </View>

            </View>
          </View>
        )
        :
        (
          <View></View>
        )
      }
    </ScrollView>
  );

  renderDayItem = ({ item, index }) => {
    return (
      <View key={item.p_id} style={{ width: '20%', justifyContent: 'flex-start' }}>
        <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
          <Text style={styles.countryText}>{`${item.name.toUpperCase()}`}</Text>
        </View>
        <Text style={styles.textWorkName}>{`${item.title}`}</Text>
      </View>
    );
  }

  SecondRoute = () => (
    <ScrollView contentContainerStyle={{ paddingBottom: hp('12%') }}>

      {this.state.resObj.intro_video
        ?
        (
          <TouchableOpacity style={styles.mediaPlayer}
            onPress={() => {
              LogUtils.firebaseEventLog('video', {
                p_id: this.props.pId,
                p_category: 'Programs',
                p_name: `Start - ${this.state.resObj.programname} Intro Video`,
              });

              if (Platform.OS === 'android') {
                Actions.newvideoplay({ videoURL: this.state.resObj.intro_video, from: 'plandetails', label: `End - ${this.state.resObj.programname} Intro Video` });
                // Actions.customPlayer({ videoURL: this.state.resObj.intro_video, from: 'plandetails', label: `End - ${this.state.resObj.programname} Intro Video` });
              } else {
                Actions.afPlayer({ videoURL: this.state.resObj.intro_video, from: 'plandetails', label: `End - ${this.state.resObj.programname} Intro Video` });
              }
            }}>
            {this.state.resObj.workoutimg
              ? (
                <Image
                  progressiveRenderingEnabled={true}
                  resizeMethod="resize"
                  source={{ uri: this.state.resObj.workoutimg }}
                  style={styles.thumbNail}
                />
              )
              : (
                <Image
                  progressiveRenderingEnabled={true}
                  resizeMethod="resize"
                  source={require('../res/ic_noimage.png')}
                  style={[styles.thumbNail, { resizeMode: 'contain', }]}
                />
              )
            }
            <View style={styles.playContainerProgram}>
              <TouchableOpacity
                style={{ flexDirection: 'row' }}
                onPress={() => {

                  LogUtils.firebaseEventLog('video', {
                    p_id: this.props.pId,
                    p_category: 'Programs',
                    p_name: `Start - ${this.state.resObj.programname} Intro Video`,
                  });

                  if (Platform.OS === 'android') {
                    Actions.newvideoplay({ videoURL: this.state.resObj.intro_video, from: 'plandetails', label: `End - ${this.state.resObj.programname} Intro Video` });
                    // Actions.customPlayer({ videoURL: this.state.resObj.intro_video, from: 'plandetails', label: `End - ${this.state.resObj.programname} Intro Video` });
                  } else {
                    Actions.afPlayer({ videoURL: this.state.resObj.intro_video, from: 'plandetails', label: `End - ${this.state.resObj.programname} Intro Video` });
                  }
                }}>
                <Image
                  progressiveRenderingEnabled={true}
                  resizeMethod="resize"
                  source={require('../res/ic_play_purple.png')}
                  style={styles.playIcon} />
              </TouchableOpacity>
            </View>
          </TouchableOpacity>
        )
        :
        (
          <View>
            {this.state.resObj.workoutimg
              ? (
                <Image
                  progressiveRenderingEnabled={true}
                  resizeMethod="resize"
                  source={{ uri: this.state.resObj.workoutimg }}
                  style={styles.thumbNail}
                />
              )
              : (
                <Image
                  progressiveRenderingEnabled={true}
                  resizeMethod="resize"
                  source={require('../res/ic_noimage.png')}
                  style={[styles.thumbNail, { resizeMode: 'contain', }]}
                />
              )
            }
          </View>
        )

      }

      <View style={styles.introDerails}>
        <Text style={styles.textValue}>{this.state.resObj.programname}</Text>
        <View style={{ flexDirection: 'row', marginTop: 2, marginLeft: wp('5%'), }}>
          <Image
            progressiveRenderingEnabled={true}
            resizeMethod="resize"
            source={require('../res/ic_alarm.png')}
            style={{ width: 12, height: 12, marginRight: 7, alignSelf: 'center' }}
          />
          <Text style={styles.textTitle}>{this.state.resObj.durtnlvl} ({this.state.resObj.execdays_week})</Text>
        </View>
      </View>
      <View style={{ justifyContent: 'center', marginLeft: wp('5%'), }}>

        <View style={styles.viewHeightRatings}>
          <View style={{
            width: wp('70%'),
            flexDirection: 'column',
            justifyContent: 'flex-start'
          }}>
            <Text style={styles.textHeight}>FITNESS FORM</Text>
            <Text style={styles.textHeightValue}>{this.state.resObj.fitness_form}</Text>
          </View>
          <View style={{
            width: wp('30%'),
            flexDirection: 'column',
            justifyContent: 'flex-start'
          }}>
            <Text style={styles.textHeight}>RATING</Text>
            <View style={{
              flexDirection: 'row',
              justifyContent: 'flex-start'
            }}>
              <Rating
                // showRating
                type='star'
                ratingCount={5}
                reviews={[]}
                startingValue={this.state.resObj.pgm_rating}
                imageSize={13}
                readonly={true}
                isDisabled={true}
                // onFinishRating={this.ratingCompleted}
                style={{ marginTop: 0, alignSelf: 'center', paddingVertical: 2 }}
              />
            </View>
          </View>
        </View>
        <View style={styles.viewHeightRatings}>
          <View style={{
            width: wp('100%'),
            flexDirection: 'column',
            justifyContent: 'flex-start'
          }}>
            <Text style={styles.textHeight}>EQUIPMENT</Text>
            <Text style={styles.textHeightValue}>{this.state.resObj.equipment}</Text>

          </View>

        </View>

        <View style={styles.viewHeightRatings}>
          <View style={styles.viewHeight}>
            <Text style={styles.textHeight}>DESCRIPTION</Text>

          </View>

        </View>

      </View>
      <Text style={styles.textDesc}>{this.state.resObj.description}</Text>

      <View style={{ height: 10 }}></View>
      <View style={{ marginLeft: wp('5%'), marginBottom: 3 }}>
        <Text style={styles.textHeight}>PROGRAM SCHEDULE</Text>
      </View>
      <FlatList
        style={{ marginLeft: wp('1.2%') }}
        extraData={this.state}
        contentContainerStyle={{ paddingBottom: hp('5%') }}
        data={this.state.resObj.program}
        keyExtractor={(item, index) => index.toString()}
        showsVerticalScrollIndicator={false}
        numColumns={5}
        // renderItem={(this.renderDayItem)}
        renderItem={({ item, index }) => (
          <View key={index} style={{ width: wp('19.5%'), height: wp('19.5%'), justifyContent: 'center', }}>
            <View style={{ padding: 5, width: wp('18%'), height: wp('18%'), flexDirection: 'column', alignSelf: 'center', backgroundColor: colors[index % colors.length] }}>
              <Text style={styles.testDayTit}>{`${item.name.toUpperCase()}`}</Text>
              <Text numberOfLines={4} style={styles.textDayName}>{`${item.title}`}</Text>
            </View>

          </View>
        )}
      />

    </ScrollView>
  );

  renderProgramVideoPlay(selItem) {
    if (selItem.is_restday === 0) {
      if (this.state.resObj.is_paiduser === 1) {
        if (this.state.resObj.is_subscribe === 1) {
          if (selItem.can_play === 1) {
            LogUtils.firebaseEventLog('video', {
              p_id: this.props.pId,
              p_category: 'Programs',
              p_name: `Start - ${this.state.resObj.programname}-${this.state.resObj.current_day[0].title}`,
            });
            Actions.newvideoplay({ pdObj: this.state.resObj.current_day[0], planObj: this.state.resObj.current_day[0], from: 'details', p_id: this.state.resObj.p_id, label: `End - ${this.state.resObj.programname}-${this.state.resObj.current_day[0].title}` });
          }
          else {
            Actions.myOrders({ from: 'premiumday' });
          }
        }
        else {
          this.setState({ isAlert: true, alertMsg: 'Please enroll the fitness program to start', alertTitle: 'Alert' });
        }
      }
      else {
        if (selItem.can_play === 1) {
          LogUtils.firebaseEventLog('video', {
            p_id: this.props.pId,
            p_category: 'Programs',
            p_name: `Start - ${this.state.resObj.programname}-${this.state.resObj.current_day[0].title}`,
          });
          Actions.newvideoplay({ pdObj: this.state.resObj.current_day[0], planObj: this.state.resObj.current_day[0], from: 'details', p_id: this.state.resObj.p_id, label: `End - ${this.state.resObj.programname}-${this.state.resObj.current_day[0].title}` });
        }
        else {
          Actions.myOrders({ from: 'premiumday' });
        }
      }
    }
  }

  callPlanDayIntro(selItem) {
    try {
      if (selItem.is_restday === 0) {
        if (this.state.resObj.is_paiduser === 1) {
          if (this.state.resObj.is_subscribe === 1) {

            if (selItem.can_play === 1) {
              if (selItem.is_technique === 1) {
                if (Platform.OS === 'android') {
                    Actions.newvideoplay({ videoURL: selItem.day_video, from: 'plandetails', label: `End - ${selItem.title}` });
                  // Actions.customPlayer({ videoURL: this.state.resObj.intro_video, from: 'plandetails', label: `End - ${this.state.resObj.programname} Intro Video` });
                } else {
                  Actions.afPlayer({ videoURL: selItem.day_video, from: 'plandetails', label: `End - ${selItem.title}` });
                }
              }
              else {
                Actions.traPlanIntro({ pdObj: selItem, from: 'details', p_id: this.state.resObj.p_id });
              }
            }
            else {
              Actions.myOrders({ from: 'premiumday' });
            }
          }
          else {
            this.setState({ isAlert: true, alertMsg: 'Please enroll the fitness program to start', alertTitle: 'Alert' });

          }

        }
        else {
          // if (selItem.is_buy.toLowerCase() === 'free') {
          //   Actions.traPlanIntro({ pdObj: selItem, from: 'details', p_id: this.state.resObj.p_id });
          // }
          if (selItem.can_play === 1) {
            if (selItem.is_technique === 1) {
              if (Platform.OS === 'android') {
                Actions.newvideoplay({ videoURL: selItem.day_video, from: 'plandetails', label: `End - ${selItem.title}` });
              } else {
                Actions.afPlayer({ videoURL: selItem.day_video, from: 'plandetails', label: `End - ${selItem.title}` });
              }
            }
            else {
              Actions.traPlanIntro({ pdObj: selItem, from: 'details', p_id: this.state.resObj.p_id });
            }
          }
          else {
            Actions.myOrders({ from: 'premiumday' });
          }
        }
      }
    } catch (error) {
      console.log("Error in callPlanDayIntro :", error);
    }
  }

  renderTodayWorkout() {
    try{
      if (this.state.resObj && !isEmpty(this.state.resObj)) {
        if (Array.isArray(this.state.resObj.current_day) && this.state.resObj.current_day.length) {
          return (
            <TouchableOpacity activeOpacity={0.7} style={styles.containerListStyle}
              onPress={() => {
                this.renderProgramVideoPlay(this.state.resObj.current_day[0]);
  
                // if (this.state.resObj.current_day[0].is_restday === 0) {
                //   {
                //     this.state.resObj.is_paiduser === 1 ?
                //       (this.renderProgramVideoPlay(this.state.resObj.current_day[0]))
                //       :
                //       (Actions.myOrders({ from: 'premiumday' }))
                //   }
                // }
  
              }}>
  
              {this.state.resObj.current_day[0].image
                ? (
                  <Image
                    progressiveRenderingEnabled={true}
                    resizeMethod="resize"
                    source={{ uri: this.state.resObj.current_day[0].image }}
                    style={styles.todayImage}
                  />
                )
                : (
                  <Image
                    progressiveRenderingEnabled={true}
                    resizeMethod="resize"
                    source={require('../res/ic_noimage.png')}
                    style={styles.todayImage}
                  />
                )
              }
  
              <Text style={styles.todayText}>{this.state.resObj.current_day[0].title}</Text>
  
              {this.state.resObj.current_day[0].is_restday === 0 ?
                (
                  <View style={styles.playContainer1}>
                    <TouchableOpacity
                      activeOpacity={0.7}
                      style={{ flexDirection: 'row' }}
                      onPress={() => {
                        this.renderProgramVideoPlay(this.state.resObj.current_day[0]);
                        // {
                        //   this.state.resObj.is_paiduser === 1 ?
                        //     (this.renderProgramVideoPlay(this.state.resObj.current_day[0]))
                        //     :
                        //     (Actions.myOrders({ from: 'premiumday' }))
                        // }
                      }}>
                      <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={require('../res/ic_play_purple.png')}
                        style={styles.playIcon} />
                    </TouchableOpacity>
                  </View>
                )
                :
                (
                  <View></View>
                )}
  
            </TouchableOpacity>
          );
        }
        else {
          return (
            <View>
            </View>
          );
        }
      }
    }catch(error){
      console.log("Error in renderTodayWorkout :",error);
    }
  }

  renderWeekTitle(item, index) {
    if (item.week_title) {
      if (index > 0) {
        if (this.state.resObj.program[index].week_title === this.state.resObj.program[index - 1].week_title) {
          return (
            <View />
          );
        }
        else {
          return (
            <Text style={styles.textWeekTit}>{item.week_title}</Text>
          );
        }
      }
      else {
        return (
          <Text style={styles.textWeekTit}>{item.week_title}</Text>
        );
      }
    }

  }

  renderWeekDropDown() {
    if (this.state.resObj && !isEmpty(this.state.resObj)) {
      if (Array.isArray(this.state.resObj.week_arr)) {
        if (this.state.resObj.week_arr.length === 1) {
          return (
            <View style={styles.viewSelectWeek}
              onPress={() => {
                this.setState({ isWeekPopup: true, });
              }}>
              <View style={styles.viewSelectWeek1}>
                <Text style={styles.textWeekName}>{this.state.selSubWeekObj.name}</Text>
                {/* <Image
                  source={require('../res/ic_top_arrow.png')}
                  style={styles.imgDropdown}
                /> */}
              </View>
            </View>
          );
        }
        else {
          return (
            <TouchableOpacity style={styles.viewSelectWeek}
              onPress={() => {
                this.setState({ isWeekPopup: true, });
              }}>
              <View style={styles.viewSelectWeek1}>
                <Text style={styles.textWeekName}>{this.state.selSubWeekObj.name}</Text>
                <Image
                  source={require('../res/ic_top_arrow.png')}
                  style={styles.imgDropdown}
                />
              </View>
            </TouchableOpacity>
          );
        }

      }

    }
  }


  ThirdRoute = () => (
    <ScrollView contentContainerStyle={{ paddingBottom: hp('10%') }}>
      <View style={styles.planDuration}>
        <Text style={styles.textWorkPlan}>{this.state.resObj.programname}</Text>
        <View style={{ flexDirection: 'row', marginTop: 2, }}>
          <Image
            progressiveRenderingEnabled={true}
            resizeMethod="resize"
            source={require('../res/ic_alarm.png')}
            style={{ width: 12, height: 12, marginRight: 7, alignSelf: 'center', tintColor: '#ffffff' }}
          />
          <Text style={styles.textTitleWhite}>{this.state.resObj.durtnlvl}</Text>
        </View>
      </View>

      {this.renderTodayWorkout()}
      {this.renderWeekDropDown()}

      <FlatList
        style={{ marginTop: 15 }}
        contentContainerStyle={{ paddingBottom: hp('5%') }}
        data={this.state.selSubWeekObj.pgm_arr}
        keyExtractor={(item, index) => index.toString()}
        showsVerticalScrollIndicator={false}
        scrollEnabled={false}
        ItemSeparatorComponent={this.FlatListItemSeparator}
        renderItem={({ item, index }) => {
          return <View>

            {/* {this.renderWeekTitle(item, index)} */}

            <TouchableOpacity activeOpacity={0.7} style={styles.containerListStyle} onPress={() => {

              this.callPlanDayIntro(item);

              // if (item.is_restday === 0) {
              //   {
              //     this.state.resObj.is_paiduser === 1 ?
              //     (this.callPlanDayIntro(item))
              //     :
              //     (Actions.myOrders({ from: 'premiumday' }))
              //   }
              // }

            }}>
              <View style={{ flexDirection: 'row', alignItems: 'flex-start', }}>
                {item.home_img
                  ? (
                    <Image
                      progressiveRenderingEnabled={true}
                      resizeMethod="resize"
                      source={{ uri: item.home_img }}
                      style={styles.profileImage}
                    />
                  )
                  : (
                    <Image
                      progressiveRenderingEnabled={true}
                      resizeMethod="resize"
                      source={require('../res/ic_noimage.png')}
                      style={styles.thumbNail}
                    />
                  )
                }
                <View style={{ flexDirection: 'column', flex: 1, alignItems: 'flex-start', alignContent: 'flex-start', justifyContent: 'center' }}>
                  <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
                    <Text style={styles.countryText}>{`${item.name.toUpperCase()}`}</Text>
                    <Text style={styles.isfree}>{`${item.is_buy.toUpperCase()}`}</Text>
                  </View>
                  <Text style={styles.textWorkName}>{`${item.title}`}</Text>

                  {item.is_restday === 0 && item.is_technique === 0
                    ? (
                      <View style={{ flexDirection: 'row', paddingLeft: 10, paddingTop: 2, paddingBottom: 10, }}>
                        <Image
                          progressiveRenderingEnabled={true}
                          resizeMethod="resize"
                          source={require('../res/ic_alarm.png')}
                          style={{ width: 10, height: 10, marginRight: 4, alignSelf: 'center' }}
                        />
                        <Text style={styles.textTitle}>{`${item.duration}`} mins</Text>
                        <Image
                          progressiveRenderingEnabled={true}
                          resizeMethod="resize"
                          source={require('../res/ic_cal.png')}
                          style={{ width: 12, height: 12, marginRight: 4, marginLeft: 10, alignSelf: 'center' }}
                        />
                        <Text style={styles.textTitle}>{`${item.cal_burn}`} cal</Text>
                      </View>
                    )
                    : (
                      <View style={{ flexDirection: 'row', paddingLeft: 10, paddingTop: 2, paddingBottom: 10, }}>
                        <Image
                          progressiveRenderingEnabled={true}
                          resizeMethod="resize"
                          style={{ width: 10, height: 10, marginRight: 4, alignSelf: 'center' }}
                        />
                        <Text style={styles.textTitle}></Text>
                        <Image
                          progressiveRenderingEnabled={true}
                          resizeMethod="resize"
                          style={{ width: 12, height: 12, marginRight: 4, marginLeft: 10, alignSelf: 'center' }}
                        />
                        <Text style={styles.textTitle}></Text>
                      </View>
                    )
                  }


                </View>
              </View>
            </TouchableOpacity>
          </View>
        }} />
    </ScrollView>
  );

  _renderTabBar = props => {
    const inputRange = props.navigationState.routes.map((x, i) => i);
    return (
      <TabBar
        style={{ backgroundColor: '#FFFFFF', elevation: 0, borderColor: '#fff', borderBottomWidth: 1, }}
        labelStyle={styles.textPlan}
        {...props}
        indicatorStyle={{ backgroundColor: '#8c52ff', height: 3 }}

        renderLabel={({ route }) => (
          <View>
            <Text style={{
              fontFamily: 'Rubik-Medium',
              alignSelf: "center",
              fontSize: 12,
              fontWeight: '500',
              lineHeight: 18,
              color: route.key === props.navigationState.routes[props.navigationState.index].key ?
                '#8c52ff' : '#6D819C'
            }}>
              {route.title}
            </Text>
          </View>
        )}
      />
    );
  };

  _renderLabel = props => ({ route, index }) => {
    const inputRange = props.navigationState.routes.map((x, i) => i);
    const outputRange = inputRange.map(
      inputIndex => (inputIndex === index ? '#D6356C' : '#222')
    );
    const color = props.position.interpolate({
      inputRange,
      outputRange,
    });

    return (
      <Animated.Text style={[styles.label, { color }]}>
        {route.title}
      </Animated.Text>
    );
  };

  // async getAllPlans() {
  //   this.setState({ loading: true });
  //   let token = await AsyncStorage.getItem('token');

  //   fetch(
  //     `${BASE_URL}/trainee/plans2`,
  //     {
  //       method: 'GET',
  //       headers: {
  //         Accept: 'application/json',
  //         'Content-Type': 'application/json',
  //         Authorization: `Bearer ${token}`,
  //       },
  //     },
  //   )
  //     .then(processResponse)
  //     .then(res => {
  //       const { statusCode, data } = res;
  //       LogUtils.infoLog1('statusCode', statusCode);
  //       LogUtils.infoLog1('data', data);

  //       if (statusCode >= 200 && statusCode <= 300) {
  //         if (data.data.length) {
  //           this.setState({ loading: false, arrAllPlans: data.data });
  //           var mySelItem = _.cloneDeep(this.state.arrAllPlans[0].data[0]);
  //           Actions.traBuyPlanDetNew({ selPlan: mySelItem });
  //         }
  //         else {
  //           this.setState({ loading: false, noDataMsg: 'No data available at this moment' });
  //         }

  //       } else {
  //         if (data.message === 'You are not authenticated!') {
  //           this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
  //         } else {
  //           this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
  //         }
  //       }
  //     })
  //     .catch(function (error) {
  //       this.setState({ loading: false, isAlert: true, alertMsg: SWR, alertTitle: 'Alert' });
  //     });
  // }

  async onViewPlansClicked() {
    // let plan_cnt = await AsyncStorage.getItem('plan_cnt');
    // if (plan_cnt === '1') {
    //   this.getAllPlans();
    // }
    // else {
    Actions.traAllBuyPlans1();
    // }
  }

  renderBottom() {
    if (this.state.resObj) {
      if (this.state.resObj.show_free_call === 1) {
        return (
          <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
            <TouchableOpacity
              onPress={() => {
                this.bottompopup.open();
              }}
              style={styles.buttonTuch}>
              <Text style={styles.buttonText}>{this.state.resObj.fcc_btn}</Text>
            </TouchableOpacity>
          </LinearGradient>
        );
      }
      else if (this.state.resObj.show_free_trial === 1) {
        return (
          <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
            <TouchableOpacity
              onPress={() => {
                this.bottompopup.open();
              }}
              style={styles.buttonTuch}>
              <Text style={styles.buttonText}>{this.state.resObj.ftc_btn}</Text>
            </TouchableOpacity>
          </LinearGradient>
        );
      }
      else {
        if (this.state.resObj.is_paiduser === 1) {
          if (this.state.resObj.is_subscribe !== 1) {
            return (
              <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                <TouchableOpacity
                  onPress={() => {
                    this.subscribeUserWorkOut();
                  }}
                  style={styles.buttonTuch}>
                  <Text style={styles.buttonText}>ENROLL</Text>
                </TouchableOpacity>
              </LinearGradient>
            );
          }
        }
      }
    }
  }

  renderWeeksData() {
    if (this.state.resObj) {
      let arrBreakfastInner = this.state.resObj.week_arr.map((item, j) => {
        return <LinearGradient key={j} colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientPopup}>
          <TouchableOpacity onPress={() => {
            this.setState({ selSubWeekObj: item, isWeekPopup: false });
          }}>
            <Text style={styles.textSelWeek}>{item.name}</Text>
          </TouchableOpacity>
        </LinearGradient>
      });
      return (
        <View>
          {arrBreakfastInner}
        </View>
      );
    }
  }

  _toggleSubview() {
    Animated.timing(
      this.state.bounceValue,
      {
        toValue: 0,
        duration: 1000
      }
    ).start();
  }


  renderFreeTrailProcess() {

    if (this.state.freeTrailProcess === 1) {

      return (
        <View style={{ flexDirection: 'column', margin: 15, flex: 1, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>

          {this.state.resObj.show_free_call === 1 ?
            (
              <View>
                <Image
                  progressiveRenderingEnabled={true}
                  resizeMethod="resize"
                  source={{ uri: this.state.resObj.fcc_image }}
                  style={{
                    width: 150,
                    height: 150,
                    alignSelf: 'center',
                  }}
                />
                <Text style={styles.freeTrialPopupTitle}>{this.state.resObj.fcc_title}</Text>
                <Text style={styles.freeTrialPopupText}>{this.state.resObj.fcc_text}</Text>
              </View>
            )
            :
            (
              <View>
                <Image
                  progressiveRenderingEnabled={true}
                  resizeMethod="resize"
                  source={{ uri: this.state.resObj.ftc_image }}
                  style={{
                    width: 150,
                    height: 150,
                    alignSelf: 'center',
                  }}
                />
                <Text style={styles.freeTrialPopupTitle}>{this.state.resObj.ftc_title}</Text>
                <Text style={styles.freeTrialPopupText}>{this.state.resObj.ftc_text}</Text>
              </View>
            )
          }

          {this.state.popuploading
            ?
            (
              <ActivityIndicator size="large" color="#000000" animating={this.state.popuploading} />
            )
            :
            (

              <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                <TouchableOpacity onPress={() => {

                  if (this.state.resObj.email_verified === 1) {
                    this.setState({ popuploading: true, onConfirmClick: true });
                    this.saveFreeTrial();

                  } else {
                    this._toggleSubview();
                    this.setState({ freeTrailProcess: 2 });
                  }


                }}>
                  <Text style={styles.textSave}>Confirm</Text>
                </TouchableOpacity>
              </LinearGradient>

            )
          }


        </View>
      );

    } else if (this.state.freeTrailProcess === 2) {

      return (

        <View style={{ flexDirection: 'column', margin: 15, flex: 1, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>

          <Animated.View style={{ transform: [{ translateX: this.state.bounceValue }] }}>

            <Text style={{
              width: wp('85%'),
              fontSize: 14,
              fontWeight: '500',
              letterSpacing: 0.2,
              fontFamily: 'Rubik-Medium',
              textAlign: 'center',
              alignSelf: 'center',
              color: '#282c37',
              lineHeight: 18,
              marginBottom: 10
            }}>{this.state.resObj.ev_title}</Text>

            <Text style={{
              fontSize: 12,
              fontWeight: '400',
              letterSpacing: 0.2,
              fontFamily: 'Rubik-Regular',
              textAlign: 'center',
              alignSelf: 'center',
              color: '#6d819c',
              lineHeight: 18,
              marginTop: 5,
              marginLeft: 5,
              marginRight: 5,
              marginBottom: 25,
            }}> {this.state.resObj.ev_descp} </Text>
          </Animated.View>

          <View style={styles.containerMobileStyle}>
            <TextInput
              ref={input => { this.emailTextInput = input; }}
              style={styles.textInputStyle}
              placeholder="Enter Email"
              placeholderTextColor="grey"
              keyboardType="email-address"
              maxLength={100}
              value={this.state.email}
              returnKeyType='done'
              onChangeText={text => this.setState({ email: text })}
            />
          </View>

          {this.state.popuploading
            ?
            (
              <ActivityIndicator size="large" color="#000000" animating={this.state.popuploading} style={{ marginTop: 40 }} />
            )
            :
            (

              <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                <TouchableOpacity onPress={() => {

                  this.validateEmail(this.state.email);
                  // setTimeout(() => {
                  //   this._toggleSubview();
                  //   this.setState({ popuploading: false, confirmSuccess: true, freeTrialSuccessTitle: 'Congratulations', freeTrialSuccessMessage: 'Dear User, as part of the free trial you shall have access to all the fitness programs and a sample diet plan for the next 7 days. Hope you have fun.', });
                  // }, 5000)
                }}>
                  <Text style={styles.textSave}>Continue</Text>
                </TouchableOpacity>
              </LinearGradient>

            )
          }

        </View>

      );

    } else if (this.state.freeTrailProcess === 3) {

      return (

        <View style={{ flexDirection: 'column', margin: 15, flex: 1, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>

          <Animated.View style={{ transform: [{ translateX: this.state.bounceValue }] }}>

            <Text style={{
              width: wp('85%'),
              fontSize: 14,
              fontWeight: '500',
              letterSpacing: 0.2,
              fontFamily: 'Rubik-Medium',
              textAlign: 'center',
              alignSelf: 'center',
              color: '#282c37',
              lineHeight: 18,
              marginBottom: 10
            }}> {this.state.emailSuccessTitle} </Text>

            <Text style={{
              fontSize: 12,
              fontWeight: '400',
              letterSpacing: 0.2,
              fontFamily: 'Rubik-Regular',
              textAlign: 'center',
              alignSelf: 'center',
              color: '#6d819c',
              lineHeight: 18,
              marginTop: 5,
              marginLeft: 20,
              marginRight: 20,
              marginBottom: 25,
            }}> {this.state.emailSuccessDesc} </Text>
          </Animated.View>



          <View style={styles.containerMobileStyle}>
            <TextInput
              ref={input => { this.emailTextInput = input; }}
              style={styles.textInputStyle}
              placeholder="Enter code"
              placeholderTextColor="grey"
              keyboardType="number-pad"
              maxLength={4}
              value={this.state.emailOtp}
              returnKeyType='done'
              onChangeText={text => this.setState({ emailOtp: text })}
            />
          </View>



          {this.state.popuploading
            ?
            (
              <ActivityIndicator size="large" color="#000000" animating={this.state.popuploading} style={{ marginTop: 40 }} />
            )
            :
            (

              <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                <TouchableOpacity onPress={() => {

                  if (this.state.emailOtp) {
                    this.setState({ popuploading: true, onConfirmClick: true });
                    this.saveFreeTrial();
                  } else {
                    this.setState({ isAlert: true, alertMsg: 'Please enter otp sent to your email id' });
                  }


                  // setTimeout(() => {
                  //   this._toggleSubview();
                  //   this.setState({ popuploading: false, confirmSuccess: true, freeTrialSuccessTitle: 'Congratulations', freeTrialSuccessMessage: 'Dear User, as part of the free trial you shall have access to all the fitness programs and a sample diet plan for the next 7 days. Hope you have fun.', });
                  // }, 5000)



                }}>
                  <Text style={styles.textSave}>Continue</Text>
                </TouchableOpacity>
              </LinearGradient>

            )
          }

        </View>

      );

    } else if (this.state.freeTrailProcess === 4) {

      return (

        <View style={{ flexDirection: 'column', margin: 15, flex: 1, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>

          <Image
            progressiveRenderingEnabled={true}
            resizeMethod="resize"
            source={{ uri: this.state.freeTrialObj.image }}
            style={{
              width: 150,
              height: 150,
              alignSelf: 'center',
            }}
          />
          <Animated.View style={{ transform: [{ translateX: this.state.bounceValue }] }}>

            <Text style={styles.freeTrialPopupTitle}>{this.state.freeTrialSuccessTitle}</Text>

            <Text style={styles.freeTrialPopupText}>{this.state.freeTrialSuccessMessage}</Text>
          </Animated.View>
          <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
            <TouchableOpacity onPress={() => {
              this.bottompopup.close();
              // Actions.traBuySuccess({ from: 'payment', sucResponse: this.state.freeTrialObj });

              if (this.state.freeTrialObj.qst_type !== 0) {
                if (this.state.freeTrialObj.qst_type === 1) {
                  if (this.state.freeTrialObj.diet_imp_confirm === 0) {
                    Actions.aidietques();
                  } else {
                    Actions.dietHome1({ isRefresh: 'yes' });
                  }
                } else if (this.state.freeTrialObj.qst_type === 2 || this.state.freeTrialObj.qst_type === 3) {
                  if (this.state.freeTrialObj.workout_imp_confirm === 0) {
                    Actions.traDietWorkQues({ qst_type: this.state.freeTrialObj.qst_type });
                  } else if (this.state.freeTrialObj.diet_imp_confirm === 0) {
                    Actions.traDietWorkQues({ qst_type: this.state.freeTrialObj.qst_type });
                  } else {
                    Actions.traineeWorkoutsHome({ isRefresh: 'yes' });
                  }
                }
              }
            }}>
              <Text style={styles.textSave}>Ok. Thanks</Text>
            </TouchableOpacity>
          </LinearGradient>
        </View>

      );
    }
  }


  async validateEmail(email) {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if (!email) {
      this.setState({ isAlert: true, alertMsg: 'Please enter an email ID' });
    }
    else if (reg.test(email.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '')) === false) {
      this.setState({ isAlert: true, alertMsg: 'Please enter a valid email ID' });
    }
    else {

      this.setState({ popuploading: true, onConfirmClick: true });
      this.verifyEmailOtp();

    }
  }

  render() {
    return (
      <KeyboardAvoidingView onLayout={this.onLayout} keyboardVerticalOffset={Platform.select({ ios: 0, android: -20 })} style={styles.containerStyle} behavior="padding" enabled>
        <Loader loading={this.state.loading} />

        <View style={{
          flexDirection: 'row',
          margin: 20,
        }}>
          <View style={{ position: 'absolute', zIndex: 111 }}>
            <TouchableOpacity
              style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
              onPress={() => this.onBackPressed()}>
              <Image
                progressiveRenderingEnabled={true}
                resizeMethod="resize"
                source={require('../res/ic_back.png')}
                style={styles.backImageStyle}
              />
            </TouchableOpacity>
          </View>

          <Text style={styles.textHeadTitle}>Program Details</Text>
        </View>

        <TabView
          navigationState={this.state}
          renderScene={({ route }) => {
            switch (route.key) {
              case 'first':
                return this.FirstRoute();
              case 'second':
                return this.SecondRoute();
              case 'third':
                return this.ThirdRoute();
              default:
                return null;
            }
          }}
          // renderScene={SceneMap({
          //   first: this.FirstRoute.bind(this),
          //   second: this.SecondRoute.bind(this),
          //   third: this.ThirdRoute.bind(this),
          // })}
          onIndexChange={index => this.setState({ index })}
          initialLayout={initialLayout}
          style={styles.container}
          renderTabBar={this._renderTabBar}
        />

        {this.renderBottom()}

        <CustomDialog
          visible={this.state.isAlert}
          title={this.state.alertTitle}
          desc={this.state.alertMsg}
          onAccept={this.onAccept.bind(this)}
          no=''
          yes='Ok' />

        <Dialog
          onDismiss={() => {
            this.setState({ isWeekPopup: false });
          }}
          onTouchOutside={() => {
            this.setState({ isWeekPopup: false });
          }}
          width={0.5}
          // height={0.2}
          visible={this.state.isWeekPopup}
        >
          <DialogContent
            style={{
              backgroundColor: '#ffffff'
            }}>
            <View style={{
              alignItems: 'center',
              alignContent: 'center',
              alignSelf: 'center',
              flexDirection: 'column'
            }}>

              <Text style={styles.textPopTitle}>Select Week</Text>

              {this.renderWeeksData()}

              {/* <FlatList
                style={{ marginTop: 20, }}
                // contentContainerStyle={{ paddingBottom: hp('5%') }}
                data={this.state.resObj.week_arr}
                keyExtractor={(item, index) => index.toString()}
                showsVerticalScrollIndicator={false}
                // scrollEnabled={false}
                ItemSeparatorComponent={this.FlatListItemSeparator}
                renderItem={({ item, index }) => {
                  return <TouchableOpacity onPress={() => {
                    this.setState({ selSubWeekObj: item, isWeekPopup: false });
                  }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', alignSelf: 'center', }}>
                      <Text style={styles.textPopWeekName}>{item.name}</Text>
                    </View>
                  </TouchableOpacity>
                }} /> */}

            </View>

          </DialogContent>
        </Dialog>

        <RBSheet
          ref={ref => {
            this.bottompopup = ref;
          }}
          height={popupHeight}
          closeOnDragDown={false}
          closeOnPressMask={false}
          closeOnPressBack={!this.state.onConfirmClick}
          onClose={() => {
            this.setState({ freeTrailProcess: 1 })
          }}
          customStyles={{
            container: {
              borderTopLeftRadius: 10,
              borderTopRightRadius: 10
            }
          }}
        >


          {this.state.onConfirmClick
            ?
            (
              <View></View>
            )
            :
            (
              <TouchableOpacity style={{ position: 'absolute', right: 10, top: 10, padding: 10, zIndex: 1 }}
                onPress={() => { this.bottompopup.close() }}>
                <Image
                  progressiveRenderingEnabled={true}
                  resizeMethod="resize"
                  style={{
                    width: 19,
                    height: 19,
                    alignSelf: 'flex-start',
                    tintColor: 'black',
                  }}
                  source={require('../res/ic_cross_black.png')}
                />
              </TouchableOpacity>

            )}

          {this.renderFreeTrailProcess()}

          <CustomDialog
            visible={this.state.isAlert}
            title='Alert'
            desc={this.state.alertMsg}
            onAccept={this.onAccept.bind(this)}
            no=''
            yes='Ok' />

        </RBSheet>

      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: StatusBar.currentHeight,
    backgroundColor: '#fff'
  },
  scene: {
  },
  playIcon: {
    width: 35,
    height: 35,
    alignSelf: 'center',
  },
  textPlayTitle: {
    fontSize: 10,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    textAlign: 'center',
    backgroundColor: '#ffffff',
    alignSelf: 'center',
    padding: 5,
    marginLeft: 5,
    borderRadius: 5,
    color: '#282c37',
  },
  playContainer: {
    flexDirection: 'row',
    alignContent: 'center',
    position: 'absolute',
    right: 20,
    bottom: 30,
    alignSelf: 'center',
    alignContent: 'center',
    justifyContent: 'center',
  },
  containerStyle: {
    width: '100%',
    height: sliderheight,
    backgroundColor: '#ffffff',
    flex: 1,
  },
  container: {
    flex: 1,
    justifyContent: "flex-start"
  },
  backImageStyle: {
    width: 19,
    height: 16,
    marginTop: 10,
    alignSelf: 'center',
  },
  textHeadTitle: {
    fontSize: 16,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    flex: 1,
    textAlign: 'center',
    alignSelf: 'center',
    color: '#282c37',
    // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
    paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
  },
  introDerails: {
    justifyContent: 'flex-start',
    flexDirection: 'column',
  },
  textPlan: {
    color: '#6D819C',
    fontFamily: 'Rubik-Medium',
    alignSelf: 'center',
    textAlign: 'center',
    fontSize: 11,
    fontWeight: '500',
  },
  textTitle: {
    fontSize: 11,
    fontWeight: '400',
    letterSpacing: 0.2,
    fontFamily: 'Rubik-Regular',
    color: '#6d819c',
    lineHeight: 18,
  },
  textTitleWhite: {
    fontSize: 10,
    fontWeight: '400',
    letterSpacing: 0.2,
    fontFamily: 'Rubik-Medium',
    color: '#ffffff',
    lineHeight: 18,
  },
  textValue: {
    fontSize: 18,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    marginLeft: wp('5%'),
    marginTop: hp('2%'),
    marginRight: wp('3%'),
    color: '#282c37',
  },
  mediaPlayer: {
    width: wp('100%'),
    position: 'relative',
    // marginTop: hp('3%'),
    justifyContent: 'center',
    marginBottom: hp('3%'),
    backgroundColor: 'black',
  },
  thumbNail: {
    width: wp('100%'),
    height: undefined,
    backgroundColor: 'black',
    resizeMode: 'contain',
    aspectRatio: 16 / 9,

  },
  thumbNail1: {
    width: wp('100%'),
    height: undefined,
    backgroundColor: 'black',
    resizeMode: 'contain',
  },
  imgTrainer: {
    width: wp('100%'),
    height: undefined,
    resizeMode: 'contain',
    aspectRatio: 5 / 6,
  },
  play: {
    width: 60,
    height: 60,
    alignSelf: "center",
    position: 'absolute',
  },
  textDesc: {
    fontSize: 13,
    fontWeight: '400',
    fontFamily: 'Rubik-Regular',
    marginLeft: wp('5%'),
    marginRight: wp('1%'),
    lineHeight: 18,
    color: '#282c37',
  },
  planDuration: {
    justifyContent: 'flex-start',
    flexDirection: 'column',
    backgroundColor: '#8c52ff',
    padding: 15,
  },
  textWorkPlan: {
    fontSize: 16,
    lineHeight: 18,
    color: '#ffffff',
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
  },
  containerListStyle: {
    width: wp('92%'),
    marginTop: hp('1%'),
    backgroundColor: '#ffffff',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: '#ddd',
    borderRadius: 6,
    position: 'relative',
    marginLeft: 15,
    alignSelf: 'center',
    marginRight: 15,
    alignItems: 'center',
    justifyContent: 'flex-start',
    shadowColor: '#4075cd',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  countryText: {
    color: '#282c37',
    fontFamily: 'Rubik-Medium',
    fontSize: 10,
    padding: 10,
    flex: 1,
    fontWeight: '500',
  },
  isfree: {
    color: '#8c52ff',
    fontFamily: 'Rubik-Medium',
    fontSize: 10,
    padding: 10,
    fontWeight: '500',
  },
  textWorkName: {
    fontSize: 14,
    width: wp('73%'),
    fontWeight: '500',
    paddingLeft: 10,
    // paddingTop: 10,
    paddingRight: 10,
    fontFamily: 'Rubik-Medium',
    color: '#282c37',
  },
  buttonText: {
    fontSize: 14,
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    fontFamily: 'Rubik-Medium',
    fontWeight: '500',
  },
  linearGradient: {
    width: '90%',
    height: 50,
    bottom: 0,
    borderRadius: 25,
    justifyContent: 'center',
    position: 'absolute',
    alignSelf: 'center',
    marginBottom: 15,
  },
  buttonTuch:
  {
    backgroundColor: 'transparent',
  },
  trainerInfoContainer: {
    width: wp('100%'),
    marginTop: -20,
    backgroundColor: '#ffffff',
    flexDirection: 'column',
    borderColor: '#ddd',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    position: 'relative',
    justifyContent: 'flex-start',
    padding: 20,
  },
  viewFollow: {
    flexDirection: 'row',
    width: wp('100%'),
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'flex-start',
  },
  calViewDet: {
    flexDirection: 'column',
    alignItems: 'center',
    position: 'absolute',
    right: 0,
    justifyContent: 'flex-end'
  },
  calTitle: {
    fontSize: 12,
    fontWeight: '500',
    fontFamily: 'Rubik-Regular',
    textAlign: 'center',
    color: '#6d819c',
  },
  calDesc: {
    fontSize: 9,
    fontWeight: '500',
    fontFamily: 'Rubik-Regular',
    color: '#6d819c',
    textAlign: 'center',
  },
  viewHeightRatings: {
    flexDirection: 'row',
    marginTop: 15,
    width: '100%',
    justifyContent: 'flex-start'
  },
  viewHeight: {
    width: wp('25%'),
    flexDirection: 'column',
    justifyContent: 'flex-start'
  },
  trainerName: {
    fontSize: 20,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    color: '#282c37',
  },
  textHeight: {
    fontSize: 10,
    fontWeight: '500',
    fontFamily: 'Rubik-Regular',
    textAlign: 'left',
    color: '#6d819c',
  },
  textHeightValue: {
    fontSize: 13,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    marginTop: 2,
    color: '#282c37',
    textAlign: 'left',
  },
  viewDesc: {
    flexDirection: 'column',
    justifyContent: 'flex-start'
  },
  textDescValue: {
    fontSize: 13,
    fontWeight: '500',
    fontFamily: 'Rubik-Regular',
    marginTop: 2,
    lineHeight: 18,
    color: '#282c37',
    textAlign: 'left',
  },
  tabBar: {
    flexDirection: 'row',
  },
  wrapper: { flexDirection: 'row' },
  title: { flex: 1, backgroundColor: '#f6f8fa' },
  row: { height: 60 },
  text: { textAlign: 'center', fontSize: 8, padding: 5 },
  profileImage: {
    width: 40,
    height: 70,
    backgroundColor: "#D8D8D8",
    borderColor: "#979797",
    borderRadius: 5,
    marginLeft: 10,
    alignSelf: 'center',
    resizeMode: "contain",
    justifyContent: 'center',
  },
  todayImage: {
    width: wp('92%'),
    height: hp('28%'),
    alignSelf: 'center',
    resizeMode: 'cover',
    borderRadius: 6,
  },
  playContainer1: {
    flexDirection: 'row',
    alignContent: 'center',
    position: 'absolute',
    right: 20,
    bottom: 30,
    alignSelf: 'center',
    alignContent: 'center',
    justifyContent: 'center',
  },
  todayText: {
    position: 'absolute',
    left: 10,
    top: 5,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    textAlign: 'left',
    alignSelf: 'center',
    backgroundColor: '#8c52ff',
    padding: 5,
    borderRadius: 15,
    color: '#ffffff',
    fontSize: 10,
  },
  playContainerProgram: {
    flexDirection: 'row',
    alignContent: 'center',
    position: 'absolute',
    right: 20,
    bottom: 20,
    alignSelf: 'center',
    alignContent: 'center',
    justifyContent: 'center',
  },
  textWeekTit: {
    fontSize: 16,
    fontWeight: '500',
    marginLeft: 20,
    marginTop: 10,
    fontFamily: 'Rubik-Medium',
    color: '#282c37',
  },
  testDayTit: {
    color: '#282c37',
    fontFamily: 'Rubik-Medium',
    fontSize: 11,
    alignSelf: 'center',
    textAlign: 'center',
    fontWeight: '500',
  },
  textDayName: {
    fontSize: 8,
    fontWeight: '500',
    marginTop: 5,
    fontFamily: 'Rubik-Regular',
    color: '#282c37',
    alignSelf: 'center',
    textAlign: 'center',
  },
  viewSelectWeek: {
    width: wp('92%'),
    borderRadius: 25,
    borderColor: '#8c52ff',
    // // borderStyle: 'solid',
    borderWidth: 0.5,
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: 15,
    // backgroundColor: '#ffffff',
  },
  viewSelectWeek1: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    alignContent: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
  },
  textWeekName: {
    color: '#282c37',
    fontFamily: 'Rubik-Medium',
    fontSize: 13,
    alignSelf: 'center',
    fontWeight: '500',
  },
  imgDropdown: {
    width: 16,
    height: 8,
    marginTop: 5,
    marginLeft: 10,
    alignSelf: 'center',
    tintColor: '#282c37',
    transform: [{ rotate: '180deg' }]
  },
  textPopWeekName: {
    color: '#282c37',
    fontFamily: 'Rubik-Regular',
    fontSize: 14,
    paddingTop: 5,
    paddingBottom: 5,
    alignSelf: 'center',
    textAlign: 'center',
    fontWeight: '500',
  },
  textPopTitle: {
    fontSize: 16,
    fontWeight: '500',
    letterSpacing: 0.2,
    fontFamily: 'Rubik-Medium',
    padding: 10,
    textAlign: 'center',
    alignSelf: 'center',
    color: '#282c37',
    lineHeight: 18,
    marginTop: 5,
  },
  linearGradientPopup: {
    borderRadius: 15,
    justifyContent: 'center',
    alignSelf: 'center',
    alignSelf: "center",
    alignItems: 'center',
    marginTop: 12,
  },
  textSelWeek: {
    width: wp('25%'),
    fontSize: 12,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    borderRadius: 15,
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 7,
    paddingBottom: 7,
    textAlign: 'center',
    alignSelf: 'center',
    color: '#ffffff',
  },
  textSpecial: {
    fontSize: 9,
    fontWeight: '500',
    color: '#8c52ff',
    letterSpacing: 0.23,
    marginTop: 4,
    textAlign: 'left',
    fontFamily: 'Rubik-Medium',
    alignSelf: 'flex-start',
  },
  linearGradientEnroll: {
    borderRadius: 15,
    justifyContent: 'center',
    alignSelf: 'center',
    alignSelf: "center",
    alignItems: 'center',
    marginTop: 20,
    // position:'absolute',
    // bottom:0,
  },
  textSave: {
    width: wp('40%'),
    fontSize: 14,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    borderRadius: 15,
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 8,
    paddingBottom: 8,
    textAlign: 'center',
    alignSelf: 'center',
    color: '#ffffff',
  },

  textNodataTitle: {
    width: wp('85%'),
    fontSize: 16,
    fontWeight: '500',
    letterSpacing: 0.2,
    fontFamily: 'Rubik-Medium',
    padding: 10,
    textAlign: 'center',
    alignSelf: 'center',
    color: '#282c37',
    lineHeight: 18,
  },
  textNodata: {
    width: wp('85%'),
    fontSize: 13,
    fontWeight: '400',
    letterSpacing: 0.2,
    fontFamily: 'Rubik-Regular',
    padding: 10,
    textAlign: 'center',
    alignSelf: 'center',
    color: '#6d819c',
    lineHeight: 18,
  },
  freeTrialPopupTitle: {
    width: wp('85%'),
    fontSize: 14,
    fontWeight: '500',
    letterSpacing: 0.2,
    fontFamily: 'Rubik-Medium',
    textAlign: 'center',
    alignSelf: 'center',
    color: '#282c37',
    lineHeight: 18,
    marginTop: 10
  },
  freeTrialPopupText: {
    fontSize: 12,
    fontWeight: '400',
    letterSpacing: 0.2,
    fontFamily: 'Rubik-Regular',
    textAlign: 'center',
    alignSelf: 'center',
    color: '#6d819c',
    lineHeight: 18,
    marginTop: 5,
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 15,
  },
  containerMobileStyle: {
    borderBottomWidth: 1,
    backgroundColor: '#ffffff',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: '#ddd',
    borderRadius: 15,
    position: 'relative',
    alignSelf: 'center',
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10,
    marginBottom: 20,
    padding: 8,
    shadowColor: '#4075cd',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  textInputStyle: {
    height: 40,
    flex: 1,
    fontSize: 13,
    marginLeft: 10,
    fontFamily: 'Rubik-Regular',
    color: '#2d3142',
  },
});

export default (Trainee_PlanDetailsNew);
