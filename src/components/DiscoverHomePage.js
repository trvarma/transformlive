import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    FlatList,
    Image,
    TouchableOpacity,
    BackHandler,
    ImageBackground,
    Text,
    ScrollView,
} from 'react-native';
import { BASE_URL, SWR } from '../actions/types';
import { Loader, CustomDialog } from './common';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import NetInfo from "@react-native-community/netinfo";
import LogUtils from '../utils/LogUtils.js';
import DeviceInfo from 'react-native-device-info';
import { allowFunction } from '../utils/ScreenshotUtils.js';
import StarRating from './Starrating';

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

class DiscoverHomePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchText: '',
            isAlert: false,
            loading: false,
            alertMsg: '',
            noData: '',
        };
        this.getDiscoverDetails = this.getDiscoverDetails.bind(this);
    }

    async componentDidMount() {
        allowFunction();
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.getDiscoverDetails();
            }
            else {
                this.setState({ isAlert: true, alertMsg: 'Please enable your internet connection and try again' });
            }
        });

        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        Actions.pop();
    }

    async getDiscoverDetails() {
        this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');

        fetch(
            `${BASE_URL}/trainee/discoverdata?ff_id=${this.props.ffId}&tr_id=${this.props.tr_id}`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data ', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false, resObj: data });
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ isAlert: true, alertMsg: SWR });
            });
    }

    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
        this.setState({ isAlert: false });
        this.setState({ alertMsg: '' });
    }

    FlatListItemSeparatorWorkouts = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: 10,
                    backgroundColor: "transparent",
                }}
            />
        );
    }

    renderPrograms() {
        if (!isEmpty(this.state.resObj)) {
            LogUtils.infoLog1('Programs : ', this.state.resObj.programs);
            if (Array.isArray(this.state.resObj.programs) && this.state.resObj.programs.length) {
                return (
                    <View style={styles.vHeaderData}>
                        <Text style={styles.textTitle}>Programs</Text>
                        <FlatList
                            horizontal
                            showsHorizontalScrollIndicator={false}
                            data={this.state.resObj.programs}
                            //keyExtractor={(item, index) => item.pg_id}
                            keyExtractor={(item, index) => "programs"+index}
                            contentContainerStyle={{ paddingLeft: 0, paddingRight: 20, }}
                            ItemSeparatorComponent={this.FlatListItemSeparatorWorkouts}
                            renderItem={({ item }) => {
                                return <View>
                                    <TouchableOpacity key={item.pg_id} activeOpacity={0.5} style={{ flexDirection: 'column', }} onPress={() => {
                                        Actions.traineePlanNew({ pId: item.pg_id });
                                    }}>
                                        <View style={{ flexDirection: 'column', }}>
                                            {item.img
                                                ? (
                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        source={{ uri: item.img }}
                                                        style={{
                                                            width: wp('80%'),
                                                            height: undefined,
                                                            borderRadius: 8,
                                                            aspectRatio: 16 / 9,
                                                        }}

                                                    />
                                                )
                                                : (
                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        source={require('../res/ic_noimage.png')}
                                                        style={{
                                                            width: wp('80%'),
                                                            height: undefined,
                                                            borderRadius: 8,
                                                            aspectRatio: 16 / 9,
                                                            resizeMode: 'cover',
                                                        }}
                                                    />
                                                )
                                            }

                                            <View style={styles.viewTrendDetails}>
                                                <Text numberOfLines={1} style={styles.txtRecProName}>{`${item.name}`}</Text>
                                                <Text numberOfLines={1} style={styles.txtRecProDuration}>{`${item.duration}`}</Text>
                                                <Text numberOfLines={1} style={styles.txtRecProfitForm}>{`${item.fitnessform}`}</Text>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            }} />
                    </View>
                );
            }
            else {
                return (
                    <View></View>
                );
            }
        }
    }


    renderFreeorPremium(item) {
        if (item.pd_type === 1) {
            return (
                <View style={{
                    flexDirection: 'column',
                    alignItems: 'center',
                    alignContent: 'flex-end',
                    margin: 10,
                    right: 0,
                    position: 'absolute',
                    justifyContent: 'center',
                }}>
                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={require('../res/ic_free.png')}
                        style={{
                            width: 30,
                            height: 30,
                            alignSelf: 'center',
                            // tintColor: '#ffffff'
                        }}
                    />
                </View>
            );
        }
        else if (item.pd_type === 2) {
            return (
                <View style={{
                    flexDirection: 'column',
                    alignItems: 'center',
                    alignContent: 'flex-end',
                    margin: 10,
                    right: 0,
                    position: 'absolute',
                    justifyContent: 'center',
                }}>
                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={require('../res/ic_premium.png')}
                        style={{
                            width: 30,
                            height: 30,
                            alignSelf: 'center',
                            // tintColor: '#ffffff'
                        }}
                    />
                </View>
            );
        }
        else {
            return (
                <View>
                </View>
            );
        }
    }

    renderWorkouts() {
        if (!isEmpty(this.state.resObj)) {
            LogUtils.infoLog1(' Workouts: ', this.state.resObj.trending_workouts);
            if (Array.isArray(this.state.resObj.workouts) && this.state.resObj.workouts.length) {
                return (
                    <View style={styles.vHeaderData}>
                        <Text style={styles.textTitle}>Workouts</Text>
                        <FlatList
                            horizontal
                            showsHorizontalScrollIndicator={false}
                            data={this.state.resObj.workouts}
                            // keyExtractor={item => item.pg_id}
                            keyExtractor={(item, index) => "workouts"+index}
                            contentContainerStyle={{ paddingLeft: 0, paddingRight: 20, paddingBottom: 10, }}
                            ItemSeparatorComponent={this.FlatListItemSeparatorWorkouts}
                            renderItem={({ item }) => {
                                return <View>
                                    <TouchableOpacity key={item.pg_id} activeOpacity={0.5} style={{ flexDirection: 'column', }} onPress={() => {
                                        Actions.traPlanIntro({ pdObj: item, from: 'workouts', pId: item.p_id });
                                    }}>
                                        <View style={{ flexDirection: 'column', }}>
                                            <View>
                                                {item.home_img
                                                    ? (
                                                        <Image
                                                            progressiveRenderingEnabled={true}
                                                            resizeMethod="resize"
                                                            source={{ uri: item.home_img }}
                                                            style={{
                                                                width: 150,
                                                                height: 268,
                                                                borderRadius: 8,
                                                                alignSelf: 'center',
                                                                resizeMode: 'cover',
                                                            }}
                                                        />
                                                    )
                                                    : (
                                                        <Image
                                                            progressiveRenderingEnabled={true}
                                                            resizeMethod="resize"
                                                            source={require('../res/ic_noimage.png')}
                                                            style={{
                                                                width: 150,
                                                                height: 268,
                                                                borderRadius: 7,
                                                                resizeMode: 'cover',
                                                            }}
                                                        />

                                                    )
                                                }
                                                {this.renderFreeorPremium(item)}
                                            </View>
                                            <View style={styles.viewRecDetails}>
                                                <Text numberOfLines={1} style={styles.txtRecProName}>{`${item.title}`}</Text>
                                                <Text numberOfLines={1} style={styles.txtRecProDuration}>{`${item.level}`}</Text>
                                                <Text numberOfLines={1} style={styles.txtRecProfitForm}>{`${item.duration}`}</Text>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            }} />
                    </View>
                );
            }
            else {
                return (
                    <View></View>
                );
            }
        }
    }

    renderTrainersList() {
        if (!isEmpty(this.state.resObj)) {
            if (Array.isArray(this.state.resObj.trainers) && this.state.resObj.trainers.length) {
                return (
                    <View style={styles.vHeaderData}>
                        <Text style={styles.textTitle}>Trainers</Text>

                        <FlatList
                            // horizontal
                            showsHorizontalScrollIndicator={false}
                            data={this.state.resObj.trainers}
                            style={{ alignSelf: 'flex-start' }}
                            // numColumns={3}
                            // keyExtractor={item => item.pg_id}
                            keyExtractor={(item, index) => "trainers"+index}
                            // contentContainerStyle={{ margin: 10, paddingRight: 30 }}
                            ItemSeparatorComponent={this.FlatListItemSeparatorWorkouts}
                            renderItem={({ item }) => {
                                return <TouchableOpacity key={item.id} style={styles.vBestTrainer} onPress={() => {
                                    Actions.traDiscover({ mFrom: 'trainer', title: item.tr_name, wType: item.tr_id, ffId: 0 });
                                }}>
                                    <View style={{ flexDirection: 'row', }}>
                                        {item.img
                                            ? (
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={{ uri: item.img }}
                                                    // source={`${item.image}`}
                                                    style={styles.imgTrainer}
                                                />
                                            )
                                            : (
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_noimage.png')}
                                                    style={styles.imgTrainer}
                                                />

                                            )
                                        }

                                        <View style={{
                                            margin: 10,
                                            alignItems: 'center',
                                            alignSelf: 'center',
                                            justifyContent: 'center',
                                            width: wp('69%'),
                                        }}>
                                            <Text numberOfLines={1} style={styles.textTrName}>{`${item.tr_name}`}</Text>
                                            <View style={{ height: 2 }}></View>
                                            <Text numberOfLines={1} style={styles.textSpecial}>{`${item.special_in}`}</Text>
                                            <Text numberOfLines={1} style={styles.textSpecial}>{`${item.fitness_form}`}</Text>

                                            <View style={{ flexDirection: 'column', alignSelf: 'flex-end', }}>
                                                <StarRating ratings={item.rating} />
                                            </View>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            }} />
                    </View >
                );
            }

        }
    }

    render() {
        return (
            <ImageBackground source={require('../res/app_bg.png')} style={styles.mainContainer}>
                <View style={styles.mainContainer}>
                    <Loader loading={this.state.loading} />
                    <View style={{
                        flexDirection: 'row',
                        margin: 20,
                    }}>
                        <View style={{ position: 'absolute', zIndex: 111 }}>
                            <TouchableOpacity
                                style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                                onPress={() => this.onBackPressed()}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_back.png')}
                                    style={styles.backImageStyle}
                                />
                            </TouchableOpacity>
                        </View>
                        <Text style={styles.textHeadTitle}>{this.props.title}</Text>
                    </View>

                    <ScrollView contentContainerStyle={{ paddingBottom: hp('15%') }}>
                        {this.renderPrograms()}
                        {this.renderWorkouts()}
                        {this.renderTrainersList()}
                    </ScrollView>

                    <CustomDialog
                        visible={this.state.isAlert}
                        title='Alert'
                        desc={this.state.alertMsg}
                        onAccept={this.onAccept.bind(this)}
                        no=''
                        yes='Ok' />
                </View>

            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        width: wp('100%'),
        height: hp('100%'),
        backgroundColor: '#ffffff',
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'center',
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    textNodata: {
        width: wp('90%'),
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        padding: 10,
        marginTop: 15,
        marginBottom: 15,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
    },
    vHeaderData: {
        width: wp('94%'),
        marginLeft: wp('3%'),
        marginRight: wp('3%'),
        justifyContent: 'flex-start',
        flexDirection: 'column',
        alignSelf: 'center',
    },
    textTitle: {
        lineHeight: 18,
        color: '#282c37',
        fontFamily: 'Rubik-Medium',
        fontSize: 16,
        // flex: 1,
        paddingTop: 15,
        paddingBottom: 15,
        alignSelf: 'flex-start',
        fontWeight: '500',
    },
    viewRecDetails: {
        marginTop: 5,
        marginRight: 5,
        marginBottom: 5,
        width: 150,
        alignSelf: 'flex-start',
        flexDirection: 'column',
    },
    txtRecProName: {
        fontSize: 14,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        textAlign: 'left',
        alignSelf: 'flex-start',
        color: '#2d3142',
    },
    txtRecProDuration: {
        fontSize: 11,
        fontWeight: '400',
        fontFamily: 'Rubik-Regular',
        color: '#2d3142',
        textAlign: 'left',
        // marginTop: 2
    },
    txtRecProfitForm: {
        fontSize: 11,
        fontWeight: '400',
        fontFamily: 'Rubik-Regular',
        color: '#2d3142',
        textAlign: 'left',
    },
    viewTrendDetails: {
        marginRight: 5,
        marginTop: 5,
        marginBottom: 5,
        width: wp('80%'),
        alignSelf: 'flex-start',
        flexDirection: 'column',
    },
    vBestTrainer: {
        // width: wp('40%'),
        flexDirection: 'row',
        marginTop: hp('0.1%'),
        marginBottom: hp('1.1%'),
        marginRight: wp('1.3%'),
        borderColor: '#ddd',
        // backgroundColor: '#b0b0b0',
        borderRadius: 5,
        position: 'relative',
        alignSelf: 'center',
    },
    imgTrainer: {
        width: wp('21%'),
        // borderTopLeftRadius: 5,
        // borderTopRightRadius: 5,
        borderRadius: 8,
        height: undefined,
        resizeMode: 'contain',
        aspectRatio: 5 / 6,
    },
    textTrName: {
        fontSize: 15,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        color: '#2d3142',
        textAlign: 'left',
        alignSelf: 'flex-start',
    },
    textSpecial: {
        fontSize: 11,
        fontWeight: '500',
        color: '#2d3142',
        letterSpacing: 0.23,
        textAlign: 'left',
        fontFamily: 'Rubik-Regular',
        alignSelf: 'flex-start',
        marginTop: 2,
    },
});

export default DiscoverHomePage;
