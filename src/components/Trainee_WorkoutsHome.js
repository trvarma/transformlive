import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  Alert,
  Image,
  TouchableOpacity,
  BackHandler,
  ScrollView,
  Text,
  Dimensions,
  Animated,
  ActivityIndicator,
  TextInput,
} from 'react-native';
import { connect } from 'react-redux';
import { CustomDialog, Loader ,ImagePreview} from './common';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Carousel from 'react-native-anchor-carousel';
const sliderWidth = Dimensions.get('window').width - 70;
const itemWidth = 250;
import NetInfo from "@react-native-community/netinfo";
import { BASE_URL } from '../actions/types';
import StarRating from './Starrating';
import Dialog, {
  DialogContent,
} from 'react-native-popup-dialog';
import DeviceInfo from 'react-native-device-info';
import LogUtils from '../utils/LogUtils.js';
import RBSheet from "react-native-raw-bottom-sheet";
import { allowFunction } from '../utils/ScreenshotUtils.js';
import { FlatListSlider } from 'react-native-flatlist-slider';

function processResponse(response) {
  const statusCode = response.status;
  const data = response.json();
  return Promise.all([statusCode, data]).then(res => ({
    statusCode: res[0],
    data: res[1],
  }));
}
let screenHeight = Math.round(Dimensions.get('window').height);
let popupHeight = screenHeight / 2;


const formatData = (data, numColumns) => {
  const numberOfFullRows = Math.floor(data.length / numColumns);
  let numberOfElementsLastRow = data.length - (numberOfFullRows * numColumns);
  while (numberOfElementsLastRow !== numColumns && numberOfElementsLastRow !== 0) {
    data.push({ key: `blank-${numberOfElementsLastRow}`, empty: true });
    numberOfElementsLastRow++;
  }
  return data;
};

const numColumns = 3;
let isIphoneX = false;

function isEmpty(obj) {
  try {
    for (var key in obj) {
      if (obj.hasOwnProperty(key))
        return false;
    }
    return true;
  } catch (error) {
    console.log(error)
  }
}

class Trainee_WorkoutsHome extends Component {
  constructor(props) {
    super(props);
    LogUtils.showSlowLog(this);

    // this.trace = null;
    this.state = {
      searchText: '',
      isAlert: false,
      loading: false,
      popuploading: false,
      alertMsg: '',
      resObj: '',
      isPopupVisible: false,
      bounceValue: new Animated.Value(1000),  //This is the initial position of the subview
      confirmSuccess: false,
      freeTrialObj: {},
      buttonState: '',
      freeTrialSuccessTitle: '',
      freeTrialSuccessMessage: '',
      onConfirmClick: false,
      email: '',
      freeTrailProcess: 1,
      emailOtp: '',
      emailSuccessTitle: '',
      emailSuccessDesc: '',
    };
  }

  async componentDidMount() {
    try {

      allowFunction();
      isIphoneX = DeviceInfo.hasNotch();
      LogUtils.infoLog1('isIphoneX', isIphoneX);
      LogUtils.infoLog1('popupHeight', popupHeight);
      if (popupHeight < 370) {
        popupHeight = 370;
      }

      NetInfo.fetch().then(state => {
        if (state.isConnected) {

          this.setState({ loading: true });
          this.getAllPrograms();
        }
        else {
          this.setState({ isAlert: true, alertMsg: 'Please enable your internet connection and try again' });
        }
      });
      this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
        this.onBackPressed();
        return true;
      });
    } catch (error) {
      console.log("Error occured at componentDidMount Trainee_WorkoutsHome : ", error)
    }
  }

  async componentWillUnmount() {
    // Stop the trace
    //await this.trace.stop();
    this.backHandler.remove();
  }

  async getAllPrograms() {
    try {
      let token = await AsyncStorage.getItem('token');
      fetch(
        `${BASE_URL}/trainee/getworkouts`,
        {
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
        },
      )
        .then(processResponse)
        .then(res => {
          const { statusCode, data } = res;
          LogUtils.infoLog1('statusCode', statusCode);
          LogUtils.infoLog1('data', data);
          if (statusCode >= 200 && statusCode <= 300) {
            this.setState({ loading: false, resObj: data.data, });
            this.setState({ email: data.data.email });

            if (this.props.mFrom === 'dwques') {
              this.setState({ isPopupVisible: true });
            }
          } else {
            if (data.message === 'You are not authenticated!') {
              this.setState({ loading: false, isAlert: true, alertMsg: data.message });
            } else {
              this.setState({ loading: false, isAlert: true, alertMsg: data.message });
            }
          }
        })
        .catch(function (error) {
          this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
        });
    } catch (error) {
      console.log("Error occured at getAllPrograms Trainee_WorkoutsHome : ", error)
    }
  }

  async saveFreeTrial() {
    try {
      let token = await AsyncStorage.getItem('token');
      let urlType = '';
      if (this.state.resObj.show_free_call === 1) {
        urlType = 'save_freetrial2';
      }
      else {
        urlType = 'save_freetrial';
      }

      LogUtils.infoLog(`Url : ${BASE_URL}/trainee/${urlType}`);

      fetch(
        `${BASE_URL}/trainee/${urlType}`,
        {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify({
            otp: this.state.emailOtp,
          }),
        },
      )
        .then(processResponse)
        .then(res => {
          const { statusCode, data } = res;
          LogUtils.infoLog1('statusCode', statusCode);
          LogUtils.infoLog1('data', data);
          if (statusCode >= 200 && statusCode <= 300) {
            this._toggleSubview();
            this.setState({ popuploading: false, freeTrialObj: data, confirmSuccess: true, freeTrialSuccessTitle: data.title, freeTrialSuccessMessage: data.message, freeTrailProcess: 4 });
            if (this.state.resObj.show_free_call === 1) {
              LogUtils.appsFlyerEventLog('freeconsultationstarted', {
                desc: 'Free Consultation Started',
              });
            }
            else {
              LogUtils.appsFlyerEventLog('freetrialstarted', {
                desc: 'Free Trial Started',
              });
            }
          } else {
            if (data.message === 'You are not authenticated!') {
              this.setState({ popuploading: false, isAlert: true, alertMsg: data.message, onConfirmClick: false });
            } else {
              this.setState({ popuploading: false, isAlert: true, alertMsg: data.message, onConfirmClick: false });
            }
          }
        })
        .catch(function (error) {
          this.setState({ popuploading: false, isAlert: true, alertMsg: JSON.stringify(error), onConfirmClick: false });
        });
    } catch (error) {
      console.log("Error occured at saveFreeTrial Trainee_WorkoutsHome : ", error)
    }
  }

  async verifyEmailOtp() {
    try {
      let token = await AsyncStorage.getItem('token');
      fetch(
        `${BASE_URL}/trainee/verifyemailotp`,
        {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify({
            email: this.state.email,
          }),
        },
      )
        .then(processResponse)
        .then(res => {
          const { statusCode, data } = res;
          LogUtils.infoLog1('statusCode', statusCode);
          LogUtils.infoLog1('data', data);
          if (statusCode >= 200 && statusCode <= 300) {
            this._toggleSubview();
            this.setState({ popuploading: false, confirmSuccess: true, emailSuccessTitle: data.title, emailSuccessDesc: data.message, freeTrailProcess: 3, onConfirmClick: false });
            // this.setState({ popuploading: false, freeTrialObj: data.data,confirmSuccess: true, freeTrialSuccessTitle:  data.data.title, freeTrialSuccessMessage:  data.data.message, });

          } else {
            if (data.message === 'You are not authenticated!') {
              this.setState({ popuploading: false, isAlert: true, alertMsg: data.message, onConfirmClick: false });
            } else {
              this.setState({ popuploading: false, isAlert: true, alertMsg: data.message, onConfirmClick: false });
            }
          }
        })
        .catch(function (error) {
          this.setState({ popuploading: false, isAlert: true, alertMsg: JSON.stringify(error), onConfirmClick: false });
        });
    } catch (error) {
      console.log("Error occured at verifyEmailOtp Trainee_WorkoutsHome : ", error)
    }
  }

  FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 2,
          width: -5,
          backgroundColor: "transparent",
        }}
      />
    );
  }

  FlatListItemSeparator1 = () => {
    return (
      <View
        style={{
          height: 1,
          width: 1,
          backgroundColor: "transparent",
        }}
      />
    );
  }
  //handling onPress action
  getListViewItem = item => {
    Alert.alert(item.hours);
  };

  onBackPressed() {
    try {
      if (this.state.isPopupVisible) {
        this.setState({ isPopupVisible: false });
      } else {
        // Actions.thome();
        // Actions.popTo('traineeHome');
        if (this.props.isRefresh === 'yes') {
          Actions.traineeHome();
        }
        else if (this.props.from === 'rewards' || this.props.from === 'feed') {
          Actions.pop();
        }
        else {
          Actions.popTo('traineeHome');
        }
      }
    } catch (error) {
      console.log("Error occured at onBackPressed Trainee_WorkoutsHome : ", error)
    }
  }

  ratingCompleted(rating) {
    LogUtils.infoLog1("Rating is: ", rating)
  }

  renderItem = ({ item, index }) => {
    const { backgroundColor } = item;
    return (
      <TouchableOpacity style={[styles.item, { backgroundColor }]}
        onPress={() => {
          this._carousel.scrollToIndex(index);
        }}>

      </TouchableOpacity>)
  };

  async onAccept() {
    if (this.state.alertMsg === 'You are not authenticated!') {
      AsyncStorage.clear().then(() => {
        Actions.auth({ type: 'reset' });
      });
    }
    this.setState({ isAlert: false, alertMsg: '' });
  }

  renderChallengesProfiles(item) {
    try {
      if (item.user_images.length) {
        if (item.user_images.length === 1) {
          let arrProfiles = item.user_images.map((item1, i) => {
            return <View key={i + "images"} style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Image
                progressiveRenderingEnabled={true}
                resizeMethod="resize"
                source={{ uri: item1.profile_img }}
                style={styles.chaProfile} />
            </View>
          });
          return (
            <View>
              {arrProfiles}
            </View>
          );
        }
        else if (item.user_images.length === 2) {
          let arrProfiles = item.user_images.map((item1, i) => {
            return <View key={i + "userImages"} style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Image
                progressiveRenderingEnabled={true}
                resizeMethod="resize"
                source={{ uri: item1.profile_img }}
                style={styles.chaProfile} />
            </View>
          });
          return (
            <View style={{ flexDirection: 'row', marginTop: 7, alignItems: 'center' }}>
              {arrProfiles}

              {item.pgm_usr_cnt !== 0
                ?
                (
                  <Text style={styles.chaMoreText}>+{`${item.pgm_usr_cnt}`}</Text>
                )
                :
                (
                  <View></View>
                )
              }

            </View>
          );
        }
      }
      else {
        return (
          <View>
          </View>
        );
      }
    } catch (error) {
      console.log("Error occured at renderChallengesProfiles Trainee_WorkoutsHome : ", error)
    }
  }

  renderRecomendedWorkouts() {
    try {
      if (this.state.resObj) {
        if (this.state.resObj.recmworkouts) {
          return (
            <View>
              <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                <Text style={styles.textValue}>Recommended For You</Text>
                <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                  <TouchableOpacity
                    onPress={() => {
                      Actions.traineeWorkouts({ mFrom: 'Recommended For You', wType: 1 });
                    }}>
                    <Text style={styles.textMore}>{'MORE'.toUpperCase()}</Text>
                  </TouchableOpacity>
                </LinearGradient>
              </View>
              <View style={{ width: wp('90%'), height: 10, }}></View>
              <Carousel style={{ paddingLeft: 20, paddingRight: 20, width: wp('100%') }}
                data={this.state.resObj.recmworkouts}
                renderItem={({ item }) => {
                  return <View>
                    {item.is_comingsoon === 1
                      ?
                      (
                        <View style={styles.containerListStyle}>
                          <View>
                            {item.home_img
                              ? (
                                <View style={{ flexDirection: 'column' }}>
                                  <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={{ uri: item.home_img }}
                                    style={{
                                      width: 220,
                                      height: 396,
                                      borderRadius: 9,
                                      // resizeMode: 'contain',
                                      aspectRatio: 5 / 9,
                                    }}
                                  />
                                  <View style={{ flexDirection: 'column', top: 0, right: 0, position: 'absolute' }}>
                                    <Text style={styles.textRecTrainer}>{`${item.trainername}`}</Text>
                                  </View>
                                </View>
                              )
                              : (
                                <Image
                                  progressiveRenderingEnabled={true}
                                  resizeMethod="resize"
                                  source={require('../res/ic_noimage.png')}
                                  style={{
                                    width: 220,
                                    height: 400,
                                    borderRadius: 9,
                                    resizeMode: 'stretch',
                                  }}
                                />
                              )
                            }
                            {/* <View style={{ flexDirection: 'column', bottom: 0, position: 'absolute' }}>
                            {item.user_images.length
                              ?
                              (
                                <View style={{ flexDirection: 'row', padding: 5, alignContent: 'center', alignItems: 'center' }}>
                                  {this.renderChallengesProfiles(item)}
                                  <View style={{ flexDirection: 'column', paddingRight: 3, paddingTop: 10, right: 0, position: 'absolute' }}>
                                    <StarRating ratings={item.pgm_rating} />
                                  </View>
                                </View>
                              )
                              :
                              (
                                <View style={{ flexDirection: 'row', padding: 5, alignContent: 'center', alignItems: 'center' }}>
                                 
                                  <View style={{ flexDirection: 'column', paddingRight: 3, paddingBottom: 10, right: 0, position: 'absolute' }}>
                                    <StarRating ratings={item.pgm_rating} />
                                  </View>
                                </View>
                              )
                            }

                            <View style={{ width: 220, flexDirection: 'column', padding: 0, alignItems: 'flex-start', alignContent: 'flex-start', borderBottomLeftRadius: 9, borderBottomRightRadius: 9 }}>

                            </View>
                          </View> */}

                          </View>
                        </View>
                      )
                      :
                      (
                        <TouchableOpacity key={"Program" + item.name} style={styles.containerListStyle} onPress={() => {

                          LogUtils.firebaseEventLog('click', {
                            p_id: item.p_id,
                            p_category: 'Programs',
                            p_name: `Program - ${item.name}`,
                          });

                          Actions.traineePlanNew({ pId: item.p_id });
                        }}>
                          <View>
                            {item.home_img
                              ? (
                                <View style={{ flexDirection: 'column' }}>
                                  <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={{ uri: item.home_img }}
                                    style={{
                                      width: 220,
                                      height: 400,
                                      borderRadius: 9,
                                      resizeMode: 'cover',
                                    }}
                                  />
                                  <View style={{ flexDirection: 'column', top: 0, right: 0, position: 'absolute' }}>
                                    <Text style={styles.textRecTrainer}>{`${item.trainername}`}</Text>
                                  </View>
                                </View>
                              )
                              : (
                                <Image
                                  progressiveRenderingEnabled={true}
                                  resizeMethod="resize"
                                  source={require('../res/ic_noimage.png')}
                                  style={{
                                    width: 220,
                                    height: 400,
                                    borderRadius: 9,
                                    resizeMode: 'stretch',
                                  }}
                                />
                              )
                            }
                            <View style={{ flexDirection: 'column', bottom: 0, position: 'absolute' }}>
                              {item.user_images.length
                                ?
                                (
                                  <View style={{ flexDirection: 'row', padding: 5, alignContent: 'center', alignItems: 'center' }}>
                                    {this.renderChallengesProfiles(item)}
                                    <View style={{ flexDirection: 'column', paddingRight: 3, paddingTop: 10, right: 0, position: 'absolute' }}>
                                      <StarRating key={item.pgm_rating.toString()} ratings={item.pgm_rating} />
                                    </View>
                                  </View>
                                )
                                :
                                (
                                  <View style={{ flexDirection: 'row', padding: 5, alignContent: 'center', alignItems: 'center' }}>

                                    <View style={{ flexDirection: 'column', paddingRight: 3, paddingBottom: 10, right: 0, position: 'absolute' }}>
                                      <StarRating key={item.pgm_rating.toString()} ratings={item.pgm_rating} />
                                    </View>
                                  </View>
                                )
                              }

                              <View style={{ width: 220, flexDirection: 'column', padding: 0, alignItems: 'flex-start', alignContent: 'flex-start', borderBottomLeftRadius: 9, borderBottomRightRadius: 9 }}>
                                {/* <View style={{ justifyContent: 'center', flexDirection: 'row', alignSelf: 'flex-start', }}>
        <Text style={styles.textWorkName} numberOfLines={1}>{`${item.name}`}</Text>
      </View>
      <Text style={styles.textTitle}>{`${item.durtnlvl}`}</Text>
      <View style={styles.viewFollow}>
        <Text numberOfLines={2} style={styles.textEqpNeed}>{`${item.fitness_form}`}</Text>
      </View> */}
                              </View>

                              {/* <View style={{ width: 220, backgroundColor: '#f9f5f5eb', flexDirection: 'column', padding: 10, alignItems: 'flex-start', alignContent: 'flex-start', borderBottomLeftRadius: 9, borderBottomRightRadius: 9 }}>
      <View style={{ justifyContent: 'center', flexDirection: 'row', alignSelf: 'flex-start', }}>
        <Text style={styles.textWorkName} numberOfLines={1}>{`${item.name}`}</Text>
      </View>
      <Text style={styles.textTitle}>{`${item.durtnlvl}`}</Text>
      <View style={styles.viewFollow}>
        <Text numberOfLines={2} style={styles.textEqpNeed}>{`${item.fitness_form}`}</Text>
      </View>
    </View> */}
                            </View>

                          </View>
                        </TouchableOpacity>
                      )

                    }
                  </View>


                }}
                itemWidth={itemWidth}
                containerWidth={sliderWidth}
                separatorWidth={10}
                inActiveScale={0.9}
                ref={(c) => {
                  this._carousel = c;
                }}
              //pagingEnable={false}
              />
            </View>
          );
        }
        else {
          return (
            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
              {/* <Text style={{ width: wp('80%'), alignSelf: 'center', height: hp('20%') }}>Nodata</Text> */}
            </View>
          );
        }
      }
    } catch (error) {
      console.log("Error occured at renderRecomendedWorkouts Trainee_WorkoutsHome : ", error)
    }
  }

  renderPopularWorkouts() {
    try {
      if (this.state.resObj) {
        if (Array.isArray(this.state.resObj.populworkouts) && this.state.resObj.populworkouts.length) {
          return (
            <View>
              <View style={{ width: wp('90%'), height: 10, }}></View>
              <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                <Text style={styles.textValue}>Most Popular</Text>
                <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                  <TouchableOpacity
                    onPress={() => {
                      Actions.traineeWorkouts({ mFrom: 'Most Popular', wType: 2 });
                    }}>
                    <Text style={styles.textMore}>{'MORE'.toUpperCase()}</Text>
                  </TouchableOpacity>
                </LinearGradient>
              </View>
              <View style={{ width: wp('90%'), height: 10, }}></View>

              <FlatList
                contentContainerStyle={{ paddingBottom: hp('2%') }}
                data={this.state.resObj.populworkouts}
                keyExtractor={(item, index) => "populworkouts" + item.p_id}
                showsVerticalScrollIndicator={false}
                scrollEnabled={false}
                ItemSeparatorComponent={this.FlatListItemSeparator1}
                renderItem={({ item }) => {
                  return <TouchableOpacity key={item.p_id + "Workouts"} style={{ flexDirection: 'column', height: 110, paddingLeft: 5, paddingBottom: 10, }}
                    onPress={() => {
                      LogUtils.firebaseEventLog('click', {
                        p_id: item.p_id,
                        p_category: 'Programs',
                        p_name: `Program - ${item.name}`,
                      });

                      Actions.traineePlanNew({ pId: item.p_id });
                    }}>
                    <View style={{ flexDirection: 'row', flex: 1, }}>
                      <View style={{ flexDirection: 'column' }}>
                        <Image
                          progressiveRenderingEnabled={true}
                          // resizeMethod="resize"
                          source={{ uri: item.workoutimg }}
                          style={{
                            width: 160,
                            height: 100,
                            borderRadius: 6,
                            alignSelf: 'center',
                            alignContent: 'center',
                            resizeMode: 'contain',
                            aspectRatio: 16 / 9,
                          }} />
                        <View style={{ flexDirection: 'column', bottom: 0, position: 'absolute' }}>
                          <Text style={styles.textRecTrainer}>{`${item.trainername}`}</Text>
                        </View>
                      </View>

                      <View style={{ flexDirection: 'column', flex: 1, justifyContent: 'center', padding: 15 }}>
                        <View style={{ flexDirection: 'column', justifyContent: 'flex-start', alignItems: 'flex-start', }}>

                          <Text style={styles.textWorkNamePop} numberOfLines={1}>{item.name}</Text>
                          <Text style={styles.textTitle}>{`${item.durtnlvl}`}</Text>
                          <Text numberOfLines={1} style={styles.textEqpNeed}>{`${item.fitness_form}`}</Text>

                          {item.user_images.length
                            ? (
                              <View style={{ marginTop: 5, }}>
                                {this.renderChallengesProfiles(item)}
                              </View>
                            )
                            : (
                              <View style={{ marginTop: 10, height: 23 }}>
                              </View>
                            )
                          }

                          <View style={{ flexDirection: 'column', alignSelf: 'flex-end', paddingRight: 3, bottom: 0, right: 0, position: 'absolute' }}>
                            <StarRating key={item.pgm_rating.toString()} ratings={item.pgm_rating} />
                          </View>

                        </View>

                      </View>
                    </View>

                  </TouchableOpacity>
                }
                } />
            </View>
          );
        }
        else {
          return (
            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
              {/* <Text style={{ width: wp('80%'), alignSelf: 'center', height: hp('20%') }}>Nodata</Text> */}
            </View>
          );
        }
      }
    } catch (error) {
      console.log(error)
    }
  }

  renderNewArrivals() {
    try {
      if (this.state.resObj) {
        if (this.state.resObj.newarrival_arr) {
          // LogUtils.infoLog1('newarrivals', this.state.resObj.newarrival_arr);
          return (
            <View>
              <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                <Text style={styles.textValue}>New Arrivals</Text>
                <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                  <TouchableOpacity
                    onPress={() => {
                      Actions.traineeWorkouts({ mFrom: 'New Arrivals', wType: 5 });
                    }}>
                    <Text style={styles.textMore}>{'MORE'.toUpperCase()}</Text>
                  </TouchableOpacity>
                </LinearGradient>
              </View>
              <View style={{ width: wp('90%'), height: 10, }}></View>

              <FlatList
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                data={this.state.resObj.newarrival_arr}
                // keyExtractor={item => item.p_id}
                keyExtractor={item => "newarrival_arr" + item.p_id}
                ItemSeparatorComponent={this.FlatListItemSeparator}
                renderItem={({ item }) => {
                  return <TouchableOpacity style={styles.containerOurTrainers} onPress={() => {

                    LogUtils.firebaseEventLog('click', {
                      p_id: item.p_id,
                      p_category: 'Programs',
                      p_name: `Program - ${item.name}`,
                    });

                    Actions.traineePlanNew({ pId: item.p_id });
                  }}>
                    <View>
                      {item.workoutimg
                        ? (
                          <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={{ uri: item.workoutimg }}
                            style={{
                              width: 250,
                              height: 150,
                              borderRadius: 7,
                              // borderTopLeftRadius: 7,
                              // borderTopRightRadius: 7,
                              // resizeMode: 'cover',
                              aspectRatio: 16 / 9,
                            }}
                          />
                        )
                        : (
                          <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_noimage.png')}
                            style={{
                              width: 250,
                              height: 150,
                              borderRadius: 7,
                              // borderTopLeftRadius: 7,
                              // borderTopRightRadius: 7,
                              opacity: 0.9,
                              resizeMode: 'cover',
                            }}
                          />

                        )
                      }
                      <View style={{
                        flexDirection: 'column',
                        alignItems: 'center',
                        alignContent: 'flex-start',
                        margin: 10,
                        bottom: 0,
                        position: 'absolute',
                        justifyContent: 'flex-start',
                      }}>
                        <LinearGradient colors={['#8c52ff', '#8c52ff']} style={{ borderRadius: 5, marginTop: 3, alignSelf: 'flex-start' }}>
                          <Text style={styles.textMainTrainer}>{`${item.trainername}`}</Text>
                        </LinearGradient>
                      </View>
                    </View>
                    {/* <View style={{ padding: 10, width: 250, flexDirection: 'column', justifyContent: 'flex-start', alignItems: 'flex-start', }}>
                    <Text style={styles.textWorkNamePop} numberOfLines={1}>{item.name}</Text>
                    <Text style={styles.textTitle}>{`${item.durtnlvl}`}</Text>
                    <Text numberOfLines={1} style={styles.textEqpNeed}>{`${item.fitness_form}`}</Text>
                  </View> */}

                  </TouchableOpacity>
                }} />
            </View>
          );
        }
        else {
          return (
            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
              {/* <Text style={{ width: wp('80%'), alignSelf: 'center', height: hp('20%') }}>Nodata</Text> */}
            </View>
          );
        }
      }
    } catch (error) {
      console.log(error)
    }
  }

  renderFitnessForm() {
    try {
      if (this.state.resObj) {
        if (this.state.resObj.fitnessform_arr) {
          return (
            <View>
              <View style={{ width: wp('90%'), height: 10, }}></View>
              <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                <Text style={styles.textValue}>Choose Your Form</Text>
                <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                  <TouchableOpacity
                    onPress={() => {
                      Actions.traFormlist();
                    }}>
                    <Text style={styles.textMore}>{'MORE'.toUpperCase()}</Text>
                  </TouchableOpacity>
                </LinearGradient>
              </View>
              <View style={{ width: wp('90%'), height: 10, }}></View>
              <FlatList
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                data={this.state.resObj.fitnessform_arr}
                keyExtractor={item => "fitnessform_arr" + item.ff_id}
                // ItemSeparatorComponent={this.FlatListItemSeparator}
                style={{ paddingRight: 10 }}
                renderItem={({ item }) => {
                  return <TouchableOpacity style={styles.containerFitnessform} onPress={() => {
                    Actions.traDiscover({ mFrom: 'discover', title: item.name, wType: 0, ffId: item.ff_id });
                  }}>
                    <View>
                      {item.image
                        ? (
                          <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={{ uri: item.image }}
                            style={{
                              width: wp('29%'),
                              height: wp('29%'),
                              borderRadius: 7,
                              resizeMode: 'cover',
                              aspectRatio: 1 / 1,
                            }}
                          />
                        )
                        : (
                          <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_noimage.png')}
                            style={{
                              width: wp('29%'),
                              height: wp('29%'),
                              borderRadius: 7,
                              resizeMode: 'cover',
                            }}
                          />
                        )
                      }
                      <View style={{
                        flexDirection: 'column',
                        alignItems: 'center',
                        // width: wp('28%'),
                        alignContent: 'flex-start',
                        marginBottom: 20,
                        bottom: 0,
                        // right: 20,
                        position: 'absolute',
                        justifyContent: 'flex-start',
                      }}>
                        <LinearGradient colors={['#8c52ff', '#8c52ff']}
                          style={{ borderBottomRightRadius: 5, borderTopRightRadius: 5, alignSelf: 'flex-start' }}>
                          <Text style={styles.textForm}>{`${item.name}`}</Text>
                        </LinearGradient>
                      </View>
                    </View>
                  </TouchableOpacity>
                }} />
            </View>
          );
        }
        else {
          return (
            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
              {/* <Text style={{ width: wp('80%'), alignSelf: 'center', height: hp('20%') }}>Nodata</Text> */}
            </View>
          );
        }
      }
    } catch (error) {
      console.log(error)
    }
  }

  renderTrainers() {
    try {
      if (this.state.resObj) {
        if (this.state.resObj.trainer) {
          return (
            <View>
              <View style={{ width: wp('90%'), height: 10, }}></View>
              <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                <Text style={styles.textValue}>Our Trainers</Text>
                <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                  <TouchableOpacity
                    onPress={() => {
                      Actions.trainersList();
                    }}>
                    <Text style={styles.textMore}>{'MORE'.toUpperCase()}</Text>
                  </TouchableOpacity>
                </LinearGradient>
              </View>
              <View style={{ width: wp('90%'), height: 10, }}></View>

              <FlatList
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                data={this.state.resObj.trainer}
                keyExtractor={item => "trainer" + item.tr_id}
                ItemSeparatorComponent={this.FlatListItemSeparator}
                renderItem={({ item }) => {
                  return <TouchableOpacity key={item.tr_id + "Trainer"} style={styles.vBestTrainer} onPress={() => {

                    // Actions.traDiscover({ mFrom: 'trainer', title: item.name, wType: item.tr_id, ffId: 0 });
                    Actions.disHomePage({ title: item.tr_name, ffId: 0, tr_id: item.tr_id });
                  }}>
                    <View style={{ flexDirection: 'column', }}>
                      {item.image
                        ?
                        (
                          <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={{ uri: item.image }}
                            // source={`${item.image}`}
                            style={styles.imgTrainer}
                          />
                        )
                        :
                        (
                          <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_noimage.png')}
                            style={styles.imgTrainer}
                          />
                        )}

                      <View style={{ paddingTop: 3, paddingBottom: 3, alignItems: 'center', alignSelf: 'flex-start', justifyContent: 'center' }}>
                        <Text numberOfLines={1} style={styles.textTrName}>{`${item.name}`}</Text>
                        <Text numberOfLines={1} style={styles.textSpecial}>{`${item.special_in}`}</Text>
                        <Text numberOfLines={1} style={styles.textSpecial}>{`${item.fitness_form}`}</Text>

                      </View>

                    </View>

                  </TouchableOpacity>
                }} />
            </View>
          );
        }
        else {
          return (
            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
              {/* <Text style={{ width: wp('80%'), alignSelf: 'center', height: hp('20%') }}>Nodata</Text> */}
            </View>
          );
        }
      }
    } catch (error) {
      console.log(error)
    }
  }

  renderDiscover() {
    try {
      if (this.state.resObj) {
        if (this.state.resObj.workout_type) {
          return (
            <View>
              <View style={{ width: wp('90%'), height: 10, }}></View>
              <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', }}>
                <Text style={styles.textValue}>Discover</Text>

              </View>
              <FlatList
                contentContainerStyle={{ paddingBottom: hp('2%') }}
                data={formatData(this.state.resObj.workout_type, numColumns)}
                style={styles.disFlatList}
                numColumns={numColumns}
                keyExtractor={item => "workout_type" + item.tt_id}
                renderItem={({ item }) => {
                  return <TouchableOpacity style={styles.item} onPress={() => {
                    Actions.traDiscover({ mFrom: 'discover', title: item.name, wType: item.tt_id, ffId: 0 });
                  }}><View>
                      {item.image
                        ? (
                          <View>
                            <Image
                              progressiveRenderingEnabled={true}
                              resizeMethod="resize"
                              source={{ uri: item.image }}
                              style={{
                                width: wp('31%'),
                                height: 130,
                                borderRadius: 7,
                                resizeMode: 'cover',
                              }}
                            />
                            <View style={{
                              width: wp('31%'),
                              height: 130,
                              borderRadius: 7,
                              position: 'absolute',
                              backgroundColor: '#0000008a',
                            }}>

                            </View>
                          </View>

                        )
                        : (
                          <View>
                            <Image
                              progressiveRenderingEnabled={true}
                              resizeMethod="resize"
                              source={require('../res/ic_noimage.png')}
                              style={{
                                width: wp('31%'),
                                height: 130,
                                borderRadius: 7,
                                opacity: 0.9,
                                resizeMode: 'cover',
                              }}
                            />
                            <View style={{
                              width: wp('31%'),
                              height: 130,
                              borderRadius: 7,
                              position: 'absolute',
                              backgroundColor: '#0000008a',
                            }}>

                            </View>
                          </View>

                        )
                      }
                      <View style={{
                        flexDirection: 'column',
                        alignItems: 'center',
                        alignContent: 'center',
                        margin: 10,
                        height: 130,
                        position: 'absolute',
                        justifyContent: 'center',
                      }}>
                        <Text style={styles.textTraDiscover}>{item.name}</Text>
                      </View>
                    </View>

                  </TouchableOpacity>
                }} />
            </View>
          );
        }
      }
    } catch (error) {
      console.log(error)
    }
  }

  renderBottom() {
    try {
      if (this.state.resObj) {
        if (this.state.resObj.show_free_call === 1) {
          return (
            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientViewPlans}>
              <TouchableOpacity
                onPress={() => {
                  this.bottompopup.open()
                }}
                style={styles.buttonTuch}>
                <Text style={styles.buttonText}>{this.state.resObj.fcc_btn}</Text>
              </TouchableOpacity>
            </LinearGradient>
          );

        }
        else if (this.state.resObj.show_free_trial === 1) {
          return (
            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientViewPlans}>
              <TouchableOpacity
                onPress={() => {
                  this.bottompopup.open()
                }}
                style={styles.buttonTuch}>
                <Text style={styles.buttonText}>{this.state.resObj.ftc_btn}</Text>
              </TouchableOpacity>
            </LinearGradient>
          );
        }
      }
    } catch (error) {
      console.log(error)
    }
  }

  _toggleSubview() {
    Animated.timing(
      this.state.bounceValue,
      {
        toValue: 0,
        duration: 1000
      }
    ).start();
  }

  renderFreeTrailProcess() {
    try {
      if (this.state.freeTrailProcess === 1) {

        return (
          <View style={{ flexDirection: 'column', margin: 15, flex: 1, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>

            {this.state.resObj.show_free_call === 1 ?
              (
                <View>
                  <Image
                    progressiveRenderingEnabled={true}
                    resizeMethod="resize"
                    source={{ uri: this.state.resObj.fcc_image }}
                    style={{
                      width: 150,
                      height: 150,
                      alignSelf: 'center',
                    }}
                  />
                  <Text style={styles.freeTrialPopupTitle}>{this.state.resObj.fcc_title}</Text>
                  <Text style={styles.freeTrialPopupText}>{this.state.resObj.fcc_text}</Text>
                </View>
              )
              :
              (
                <View>
                  <Image
                    progressiveRenderingEnabled={true}
                    resizeMethod="resize"
                    source={{ uri: this.state.resObj.ftc_image }}
                    style={{
                      width: 150,
                      height: 150,
                      alignSelf: 'center',
                    }}
                  />
                  <Text style={styles.freeTrialPopupTitle}>{this.state.resObj.ftc_title}</Text>
                  <Text style={styles.freeTrialPopupText}>{this.state.resObj.ftc_text}</Text>
                </View>
              )
            }


            {this.state.popuploading
              ?
              (
                <ActivityIndicator size="large" color="#000000" animating={this.state.popuploading} />
              )
              :
              (

                <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                  <TouchableOpacity onPress={() => {

                    if (this.state.resObj.email_verified === 1) {
                      this.setState({ popuploading: true, onConfirmClick: true });

                      this.saveFreeTrial();

                    } else {
                      this._toggleSubview();
                      this.setState({ freeTrailProcess: 2 });
                    }

                  }}>
                    <Text style={styles.textSave}>Confirm</Text>
                  </TouchableOpacity>
                </LinearGradient>

              )
            }


          </View>
        );

      } else if (this.state.freeTrailProcess === 2) {

        return (

          <View style={{ flexDirection: 'column', margin: 15, flex: 1, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>

            <Animated.View style={{ transform: [{ translateX: this.state.bounceValue }] }}>

              <Text style={{
                width: wp('85%'),
                fontSize: 14,
                fontWeight: '500',
                letterSpacing: 0.2,
                fontFamily: 'Rubik-Medium',
                textAlign: 'center',
                alignSelf: 'center',
                color: '#282c37',
                lineHeight: 18,
                marginBottom: 10
              }}>{this.state.resObj.ev_title}</Text>

              <Text style={{
                fontSize: 12,
                fontWeight: '400',
                letterSpacing: 0.2,
                fontFamily: 'Rubik-Regular',
                textAlign: 'center',
                alignSelf: 'center',
                color: '#6d819c',
                lineHeight: 18,
                marginTop: 5,
                marginLeft: 5,
                marginRight: 5,
                marginBottom: 25,
              }}> {this.state.resObj.ev_descp} </Text>
            </Animated.View>



            <View style={styles.containerMobileStyle}>
              <TextInput
                ref={input => { this.emailTextInput = input; }}
                style={styles.textInputStyle}
                placeholder="Enter Email"
                placeholderTextColor="grey"
                keyboardType="email-address"
                maxLength={100}
                value={this.state.email}
                returnKeyType='done'
                onChangeText={text => this.setState({ email: text })}
              />
            </View>



            {this.state.popuploading
              ?
              (
                <ActivityIndicator size="large" color="#000000" animating={this.state.popuploading} style={{ marginTop: 40 }} />
              )
              :
              (

                <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                  <TouchableOpacity onPress={() => {

                    this.validateEmail(this.state.email);
                  }}>
                    <Text style={styles.textSave}>Continue</Text>
                  </TouchableOpacity>
                </LinearGradient>

              )
            }

          </View>

        );

      } else if (this.state.freeTrailProcess === 3) {

        return (

          <View style={{ flexDirection: 'column', margin: 15, flex: 1, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>

            <Animated.View style={{ transform: [{ translateX: this.state.bounceValue }] }}>

              <Text style={{
                width: wp('85%'),
                fontSize: 14,
                fontWeight: '500',
                letterSpacing: 0.2,
                fontFamily: 'Rubik-Medium',
                textAlign: 'center',
                alignSelf: 'center',
                color: '#282c37',
                lineHeight: 18,
                marginBottom: 10
              }}> {this.state.emailSuccessTitle} </Text>

              <Text style={{
                fontSize: 12,
                fontWeight: '400',
                letterSpacing: 0.2,
                fontFamily: 'Rubik-Regular',
                textAlign: 'center',
                alignSelf: 'center',
                color: '#6d819c',
                lineHeight: 18,
                marginTop: 5,
                marginLeft: 20,
                marginRight: 20,
                marginBottom: 25,
              }}> {this.state.emailSuccessDesc} </Text>
            </Animated.View>



            <View style={styles.containerMobileStyle}>
              <TextInput
                ref={input => { this.emailTextInput = input; }}
                style={styles.textInputStyle}
                placeholder="Enter code"
                placeholderTextColor="grey"
                keyboardType="number-pad"
                maxLength={4}
                value={this.state.emailOtp}
                returnKeyType='done'
                onChangeText={text => this.setState({ emailOtp: text })}
              />
            </View>



            {this.state.popuploading
              ?
              (
                <ActivityIndicator size="large" color="#000000" animating={this.state.popuploading} style={{ marginTop: 40 }} />
              )
              :
              (

                <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                  <TouchableOpacity onPress={() => {

                    if (this.state.emailOtp) {
                      this.setState({ popuploading: true, onConfirmClick: true });
                      this.saveFreeTrial();
                    } else {
                      this.setState({ isAlert: true, alertMsg: 'Please enter otp sent to your email id' });
                    }

                  }}>
                    <Text style={styles.textSave}>Continue</Text>
                  </TouchableOpacity>
                </LinearGradient>

              )
            }

          </View>

        );

      } else if (this.state.freeTrailProcess === 4) {

        return (

          <View style={{ flexDirection: 'column', margin: 15, flex: 1, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>

            <Image
              progressiveRenderingEnabled={true}
              resizeMethod="resize"
              source={{ uri: this.state.freeTrialObj.image }}
              style={{
                width: 150,
                height: 150,
                alignSelf: 'center',
              }}
            />
            <Animated.View style={{ transform: [{ translateX: this.state.bounceValue }] }}>

              <Text style={styles.freeTrialPopupTitle}>{this.state.freeTrialSuccessTitle}</Text>

              <Text style={styles.freeTrialPopupText}>{this.state.freeTrialSuccessMessage}</Text>
            </Animated.View>
            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
              <TouchableOpacity onPress={() => {
                this.bottompopup.close();

                if (this.state.freeTrialObj.qst_type !== 0) {
                  if (this.state.freeTrialObj.qst_type === 1) {
                    if (this.state.freeTrialObj.diet_imp_confirm === 0) {
                      Actions.aidietques();
                    } else {
                      Actions.dietHome1({ isRefresh: 'yes' });
                    }

                  } else if (this.state.freeTrialObj.qst_type === 2 || this.state.freeTrialObj.qst_type === 3) {
                    if (this.state.freeTrialObj.workout_imp_confirm === 0) {
                      Actions.traDietWorkQues({ qst_type: this.state.freeTrialObj.qst_type });
                    } else if (this.state.freeTrialObj.diet_imp_confirm === 0) {
                      Actions.traDietWorkQues({ qst_type: this.state.freeTrialObj.qst_type });
                    } else {
                      Actions.traineeWorkoutsHome({ isRefresh: 'yes' });
                    }

                  }
                }

              }}>
                <Text style={styles.textSave}>Ok. Thanks</Text>
              </TouchableOpacity>
            </LinearGradient>


          </View>

        );
      }
    } catch (error) {
      console.log(error)
    }
  }

  async validateEmail(email) {
    try {
      let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

      if (!email) {
        this.setState({ isAlert: true, alertMsg: 'Please enter an email ID' });
      }
      else if (reg.test(email.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '')) === false) {
        this.setState({ isAlert: true, alertMsg: 'Please enter a valid email ID' });
      }
      else {

        this.setState({ popuploading: true, onConfirmClick: true });
        this.verifyEmailOtp();
      }
    } catch (error) {
      console.log(error)
    }
  }

  render() {
    return (
      // <SafeAreaView style={styles.mainContainer}>
      <View style={styles.mainContainer}>
        <Loader loading={this.state.loading} />

        <View style={{
          flexDirection: 'row',
          margin: 20,
        }}>
          <View style={{ position: 'absolute', zIndex: 111 }}>
            <TouchableOpacity
              style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
              onPress={() => this.onBackPressed()}>
              <Image
                progressiveRenderingEnabled={true}
                resizeMethod="resize"
                source={require('../res/ic_back.png')}
                style={styles.backImageStyle}
              />
            </TouchableOpacity>
          </View>

          <Text style={styles.textHeadTitle}>Fitness Programs</Text>
        </View>

        <ScrollView contentContainerStyle={{ paddingBottom: hp('10%') }}>

          {this.renderRecomendedWorkouts()}
          {this.renderPopularWorkouts()}
          {this.renderNewArrivals()}
          {this.renderFitnessForm()}
          {this.renderTrainers()}
          {this.renderDiscover()}

        </ScrollView>

        {this.renderBottom()}
        <Dialog
          onDismiss={() => {
            this.setState({ isPopupVisible: false });
          }}
          onTouchOutside={() => {
            this.setState({ isPopupVisible: false });
          }}
          width={0.7}
          visible={this.state.isPopupVisible}
        >
          <DialogContent
            style={{
              backgroundColor: '#ffffff'
            }}>
            <View style={{ flexDirection: 'column', }}>
              <View style={{ flexDirection: 'column', padding: 15 }}>

                <View style={{ alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'column' }}>

                  <Image
                    progressiveRenderingEnabled={true}
                    resizeMethod="resize"
                    source={require('../res/img_program_enroll.png')}
                    style={styles.imgpopup}
                  />

                  <Text style={styles.textNodataTitle}>Choose a Program</Text>

                  <Text style={styles.textNodata}>Enroll into one of the fitness programs to start</Text>

                  <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                    <TouchableOpacity onPress={() => {
                      this.setState({ isPopupVisible: false });
                    }}>
                      <Text style={styles.textSave}>ENROLL</Text>
                    </TouchableOpacity>
                  </LinearGradient>

                </View>

              </View>

            </View>

          </DialogContent>
        </Dialog>

        <RBSheet
          ref={ref => {
            this.bottompopup = ref;
          }}
          height={popupHeight}
          closeOnDragDown={false}
          closeOnPressMask={false}
          closeOnPressBack={!this.state.onConfirmClick}
          onClose={() => {
            this.setState({ freeTrailProcess: 1 })
          }}
          customStyles={{
            container: {
              borderTopLeftRadius: 10,
              borderTopRightRadius: 10
            }
          }}
        >
          {this.state.onConfirmClick
            ?
            (
              <View></View>
            )
            :
            (
              <TouchableOpacity style={{ position: 'absolute', right: 10, top: 10, padding: 10, zIndex: 1 }}
                onPress={() => {
                  this.setState({ freeTrailProcess: 1 })
                  this.bottompopup.close()
                }}>
                <Image
                  progressiveRenderingEnabled={true}
                  resizeMethod="resize"
                  style={{
                    width: 19,
                    height: 19,
                    alignSelf: 'flex-start',
                    tintColor: 'black',
                  }}
                  source={require('../res/ic_cross_black.png')}
                />
              </TouchableOpacity>

            )}

          {this.renderFreeTrailProcess()}

          <CustomDialog
            visible={this.state.isAlert}
            title='Alert'
            desc={this.state.alertMsg}
            onAccept={this.onAccept.bind(this)}
            no=''
            yes='Ok' />


        </RBSheet>

        <CustomDialog
          visible={this.state.isAlert}
          title='Alert'
          desc={this.state.alertMsg}
          onAccept={this.onAccept.bind(this)}
          no=''
          yes='Ok' />
      </View>
      // </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#ffffff',
    flex: 1,
  },
  backImageStyle: {
    width: 19,
    height: 16,
    marginTop: 10,
    alignSelf: 'center',
  },
  textValue: {
    fontSize: 16,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    flex: 1,
    marginLeft: 10,
    color: '#282c37',
  },
  textWorkName: {
    fontSize: 13,
    fontWeight: '500',
    flexShrink: 2,
    fontFamily: 'Rubik-Medium',
    color: '#000000',
    alignSelf: 'flex-start',
    marginRight: 3,
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  textOurTrainer: {
    fontSize: 14,
    fontWeight: '500',
    flexShrink: 2,
    textAlign: 'left',
    fontFamily: 'Rubik-Medium',
    color: '#ffffff',
    alignSelf: 'flex-start',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  textMore: {
    color: '#ffffff',
    fontFamily: 'Rubik-Medium',
    fontSize: 10,
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 5,
    paddingRight: 5,
    alignSelf: 'center',
    textAlign: 'center',
    fontWeight: '500',
    lineHeight: 18,
  },
  textMainTrainer: {
    color: '#ffffff',
    fontFamily: 'Rubik-Medium',
    fontSize: 8,
    paddingTop: 3,
    paddingBottom: 3,
    paddingLeft: 10,
    paddingRight: 10,
    alignSelf: 'center',
    textAlign: 'center',
    fontWeight: '500',
  },
  textRecTrainer: {
    fontSize: 9,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    backgroundColor: '#8c52ff',
    borderRadius: 5,
    paddingTop: 2,
    paddingBottom: 2,
    paddingLeft: 6,
    paddingRight: 6,
    margin: 5,
    textAlign: 'left',
    alignSelf: 'flex-start',
    color: '#ffffff',
  },
  textWorkNamePop: {
    fontSize: 15,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    color: '#000000',
    flexDirection: 'row',
    lineHeight: 18
  },
  textTitle: {
    fontSize: 12,
    fontWeight: '400',
    fontFamily: 'Rubik-Regular',
    lineHeight: 18,
    color: '#000000',
    marginTop: 2
  },
  textEqpNeed: {
    fontSize: 12,
    fontWeight: '400',
    fontFamily: 'Rubik-Regular',
    lineHeight: 18,
    color: '#000000',
  },
  textHeadTitle: {
    fontSize: 16,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    flex: 1,
    textAlign: 'center',
    alignSelf: 'center',
    color: '#282c37',
    // paddingTop: (DeviceInfo.getModel() === ('iPhone 11 Pro Max' || 'iPhone 11' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR')) ? 30 : 0,
    paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
  },
  viewFollow: {
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
    marginTop: 3,
    alignContent: 'center',
    justifyContent: 'flex-start',
  },
  textSub: {
    fontSize: 10,
    fontWeight: '500',
    fontFamily: 'Rubik-Regular',
    backgroundColor: '#8c52ff',
    borderRadius: 5,
    paddingTop: 1,
    paddingBottom: 1,
    paddingLeft: 6,
    paddingRight: 6,
    bottom: 5,
    textAlign: 'left',
    alignSelf: 'flex-start',
    color: '#ffffff',
  },
  image: {
    width: 60,
    height: 60,
  },
  containerListStyle: {
    width: 220,
    backgroundColor: '#ffffff',
    justifyContent: 'flex-start',
    flexDirection: 'column',
    borderColor: '#ddd',
    // borderRadius: 15,
    borderRadius: 10,
    position: 'relative',
    margin: 10,
    justifyContent: 'flex-start',
    shadowColor: '#4075cd',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  containerOurTrainers: {
    backgroundColor: '#ffffff',
    justifyContent: 'flex-start',
    flexDirection: 'column',
    borderColor: '#ddd',
    // borderRadius: 15,
    borderRadius: 10,
    position: 'relative',
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    shadowColor: '#4075cd',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  containerFitnessform: {
    backgroundColor: '#ffffff',
    justifyContent: 'flex-start',
    flexDirection: 'column',
    borderColor: '#ddd',
    // borderRadius: 15,
    borderRadius: 10,
    position: 'relative',
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 5,
    shadowColor: '#4075cd',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  linearGradient: {
    width: '18%',
    borderRadius: 15,
    marginRight: 10,
    justifyContent: 'center',
    alignSelf: 'center',
    alignSelf: "flex-end",
    alignItems: 'center',
  },
  linearGradientViewPlans: {
    width: '90%',
    height: 50,
    bottom: 0,
    borderRadius: 25,
    justifyContent: 'center',
    position: 'absolute',
    alignSelf: 'center',
    marginBottom: 15,
  },
  disFlatList: {
    marginVertical: 5,
  },
  item: {
    backgroundColor: '#ffffff',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1, // approximate a square
    margin: 5,
    borderRadius: 7,
    shadowColor: '#4075cd',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  textTraDiscover: {
    fontSize: 14,
    fontWeight: '500',
    flexShrink: 2,
    textAlign: 'center',
    fontFamily: 'Rubik-Medium',
    color: '#ffffff',
    alignSelf: 'center',
    flexWrap: 'wrap',
  },
  imgpopup: {
    width: 200,
    height: 200,
    alignSelf: 'center',
  },
  textNodata: {
    fontSize: 13,
    fontWeight: '400',
    letterSpacing: 0.2,
    fontFamily: 'Rubik-Regular',
    padding: 10,
    textAlign: 'center',
    alignSelf: 'center',
    color: '#6d819c',
    lineHeight: 18,
  },
  textNodataTitle: {
    fontSize: 16,
    fontWeight: '500',
    letterSpacing: 0.2,
    fontFamily: 'Rubik-Medium',
    padding: 10,
    textAlign: 'center',
    alignSelf: 'center',
    color: '#282c37',
    lineHeight: 18,
  },
  textSave: {
    width: wp('40%'),
    fontSize: 14,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    borderRadius: 15,
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 8,
    paddingBottom: 8,
    textAlign: 'center',
    alignSelf: 'center',
    color: '#ffffff',
  },
  linearGradientEnroll: {
    borderRadius: 15,
    justifyContent: 'center',
    alignSelf: 'center',
    alignSelf: "center",
    alignItems: 'center',
    marginTop: 20,
    // position:'absolute',
    // bottom:0,
  },
  linearGradientBottom: {
    borderRadius: 15,
    justifyContent: 'center',
    alignSelf: 'center',
    alignSelf: "center",
    alignItems: 'center',
    marginTop: 20,
    position: 'absolute',
    bottom: 0,
  },
  chaProfile: {
    width: 23,
    height: 23,
    aspectRatio: 1,
    backgroundColor: "#D8D8D8",
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: "#979797",
    borderRadius: 23 / 2,
    marginRight: 2,
    resizeMode: "cover",
    justifyContent: 'center',
  },
  chaMore: {
    flexDirection: 'row',
    backgroundColor: '#979797',
    borderRadius: 15,
    paddingLeft: 8,
    paddingTop: 4,
    marginTop: 5,
    alignSelf: 'center',
    paddingBottom: 4,
    paddingRight: 8,
  },
  chaMoreText: {
    color: '#ffffff',
    fontSize: 12,
    textAlign: 'center',
    alignSelf: 'center',
    fontFamily: 'Rubik-Regular',
    backgroundColor: '#979797',
    borderRadius: 10,
    paddingLeft: 4,
    paddingTop: 2,
    alignSelf: 'center',
    paddingBottom: 2,
    paddingRight: 4,
    overflow: 'hidden',
  },
  textForm: {
    color: '#ffffff',
    fontFamily: 'Rubik-Medium',
    fontSize: 10,
    paddingTop: 3,
    paddingBottom: 3,
    paddingLeft: 5,
    paddingRight: 10,
    alignSelf: 'center',
    textAlign: 'left',
    fontWeight: '500',
  },
  buttonText: {
    fontSize: 14,
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    fontFamily: 'Rubik-Medium',
    fontWeight: '500',
  },
  buttonTuch:
  {
    backgroundColor: 'transparent',
  },
  vBestTrainer: {
    width: wp('40%'),
    // backgroundColor: '#ffffff',
    flexDirection: 'row',
    marginTop: hp('0.1%'),
    marginBottom: hp('1.1%'),
    marginRight: wp('1.3%'),
    borderColor: '#ddd',
    borderRadius: 5,
    position: 'relative',
    alignSelf: 'center',
    // shadowColor: "#fff",
    // shadowOffset: {
    //   width: 0,
    //   height: 2,
    // },
    // shadowOpacity: 0.25,
    // shadowRadius: 3.84,
    // elevation: 5,
    marginLeft: 10
  },
  imgTrainer: {
    width: wp('40%'),
    // height: 130,
    // alignSelf: 'center',
    // position: 'relative',
    borderRadius: 5,
    height: undefined,
    resizeMode: 'contain',
    aspectRatio: 5 / 6,
  },
  textTrName: {
    /// height: 25,
    fontSize: 13,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    color: '#2d3142',
    textAlign: 'left',
    alignSelf: 'flex-start',
    // alignContent: 'center',
    // flex: 1,
  },
  textSpecial: {
    fontSize: 10,
    fontWeight: '500',
    color: '#2d3142',
    letterSpacing: 0.23,
    // marginTop: 2,
    textAlign: 'left',
    fontFamily: 'Rubik-Regular',
    alignSelf: 'flex-start',
  },
  subView: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: "#FFFFFF",
    height: 200,
  },
  textPopTitle: {
    fontSize: 14,
    fontWeight: '500',
    letterSpacing: 0.2,
    fontFamily: 'Rubik-Medium',
    padding: 10,
    textAlign: 'center',
    alignSelf: 'center',
    color: '#282c37',
    lineHeight: 18,
    marginTop: 5,
  },
  textPopDesc: {
    fontSize: 12,
    fontWeight: '400',
    letterSpacing: 0.2,
    fontFamily: 'Rubik-Regular',
    alignSelf: 'center',
    color: '#6d819c',
    lineHeight: 16,
    marginLeft: 8,
    padding: 20,
  },
  freeTrialPopupTitle: {
    width: wp('85%'),
    fontSize: 14,
    fontWeight: '500',
    letterSpacing: 0.2,
    fontFamily: 'Rubik-Medium',
    textAlign: 'center',
    alignSelf: 'center',
    color: '#282c37',
    lineHeight: 18,
    marginTop: 10
  },
  freeTrialPopupText: {
    fontSize: 12,
    fontWeight: '400',
    letterSpacing: 0.2,
    fontFamily: 'Rubik-Regular',
    textAlign: 'center',
    alignSelf: 'center',
    color: '#6d819c',
    lineHeight: 18,
    marginTop: 5,
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 15,
  },
  containerMobileStyle: {
    borderBottomWidth: 1,
    backgroundColor: '#ffffff',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: '#ddd',
    borderRadius: 15,
    position: 'relative',
    alignSelf: 'center',
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10,
    marginBottom: 20,
    padding: 8,
    shadowColor: '#4075cd',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  textInputStyle: {
    height: 40,
    flex: 1,
    fontSize: 13,
    marginLeft: 10,
    fontFamily: 'Rubik-Regular',
    color: '#2d3142',
  },
  workOutBgNew: {
    width: wp('94%'),
    marginLeft: wp('3%'),
    marginRight: wp('3%'),
    // backgroundColor: '#ffffff',
    justifyContent: 'flex-start',
    flexDirection: 'column',
    //position: 'relative',
    alignSelf: 'center',
  },
  vHeaderTitMore: {
    width: wp('94%'),
    flexDirection: 'row',
    paddingTop: 15,
    paddingBottom: 15,
  },
  textPlan: {
    lineHeight: 18,
    color: '#282c37',
    fontFamily: 'Rubik-Medium',
    fontSize: 14,
    flex: 1,
    alignSelf: "center",
    fontWeight: '500',
  },
  textAdd: {
    color: '#8c52ff',
    fontFamily: 'Rubik-Medium',
    alignSelf: "flex-end",
    fontSize: 11,
    fontWeight: '500',
    lineHeight: 18,
  },
});

const mapStateToProps = state => {
  const trainee = state.traineeFood
  return { trainee };
};

export default connect(mapStateToProps, {},)(Trainee_WorkoutsHome);
