import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    FlatList,
    Image,
    TouchableOpacity,
    BackHandler,
    Text,
    ImageBackground,
    Modal,
} from 'react-native';
import { CustomDialog, Loader, NoInternet } from './common';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import LinearGradient from 'react-native-linear-gradient';
import NetInfo from "@react-native-community/netinfo";
import { BASE_URL, SWR } from '../actions/types';
import DeviceInfo from 'react-native-device-info';
import LogUtils from '../utils/LogUtils.js';
import PDFView from 'react-native-view-pdf';
import AppIntroSlider from 'react-native-app-intro-slider';
import FastImage from 'react-native-fast-image';
import _ from 'lodash';
import { allowFunction } from '../utils/ScreenshotUtils.js'; 
function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

class MyMedicalRecords extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isAlert: false,
            loading: false,
            alertMsg: '',
            programsArray: '',
            isNoInternet: false,
            sucMsg: '',
            titMsg: '',
            selUpId: 0,
            isPopupVisible: false,
            pageNo: 1,
            pageSize: 20,
            isShowInfoPopup: false,
            selItem: '',
        };
        // this.getMyMedicalReports = this.getMyMedicalReports.bind(this);
    }

    async componentDidMount() {
        allowFunction();
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isNoInternet: false });
                this.getMyMedicalReports();
            }
            else {
                this.setState({ isNoInternet: true });
            }
        });
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    async onAccept() {
        this.setState({ isAlert: false, alertMsg: '' });
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
    }

    async getMyMedicalReports() {
        this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/getmedicalreport`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    page: this.state.pageNo,
                    pagesize: this.state.pageSize,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('statusmessage', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    if (data.data.length > 0) {
                        
                        this.setState({  loading: false, programsArray: data.data });
                    }
                    else {
                        this.setState({ loading: false,  programsArray: []});
                    }
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, programsArray: [], isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, programsArray: [] });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: SWR, });
            });
    }

    FlatListItemSeparator1 = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: 1,
                    backgroundColor: "transparent",
                }}
            />
        );
    }

    onBackPressed() {
        Actions.popTo('traineeHome');
    }

    renderAllSuscriprionsList() {

        if (this.state.programsArray) {
            if (Array.isArray(this.state.programsArray) && this.state.programsArray.length) {
                return (
                    <View>
                        <FlatList
                            contentContainerStyle={{ paddingBottom: hp('20%') }}
                            showsVerticalScrollIndicator={false}
                            data={this.state.programsArray}
                            keyExtractor={item => item.umd_id}
                            ItemSeparatorComponent={this.FlatListItemSeparator1}
                            renderItem={({ item }) => {
                                // console.log(item.report_img)
                                return <TouchableOpacity
                                    key={item.umd_id}
                                    style={styles.aroundListStyle}
                                    onPress={() => {
                                        this.setState({ isShowInfoPopup: true, selItem: item });
                                    }
                                    }>
                                    {item.file_type === 1
                                        ?
                                        (
                                            <PDFView
                                            style={{ width: 60, height: 60 }}
                                            onError={(error) => {
                                                // console.log('onError', error);
                                            }}
                                            onLoad={() => {
                                               // console.log('PDF rendered from file');
                                            }}
                                            // resource={Platform.OS === 'ios' ? 'sample3.pdf' : '/sdcard/Download/sample3.pdf'}
                                            resource={item.pdf_url}
                                            resourceType="url"
                                        />

                                            // <Image
                                            //     source={require('../res/ic_pdf_red.png')}
                                            //     style={{ width: 60, height: 60 }}
                                            // />
                                        )
                                        :
                                        (
                                            <Image
                                                // source={require('../res/ic_pdf_red.png')}
                                                source={{ uri: item.report_img[0].img }}
                                                style={{ width: 60, height: 60 }}
                                            />
                                        )
                                    }

                                    <View style={{ flexDirection: 'column', marginLeft: 10, justifyContent: 'center' }}>
                                        <Text
                                            numberOfLines={2}
                                            style={styles.textWorkName}>{`${item.name}`}</Text>
                                        <View style={{ flexDirection: 'row', marginTop: 5 }}>

                                            {item.file_type === 1
                                                ?
                                                (
                                                    <Image
                                                        source={require('../res/ic_pdf.png')}
                                                        style={{
                                                            width: 15,
                                                            height: 15,
                                                            alignSelf: 'center',
                                                            marginRight: 5,
                                                            tintColor: '#6d819c',
                                                        }}
                                                    />
                                                )
                                                :
                                                (
                                                    <Image
                                                        source={require('../res/photo.png')}
                                                        style={{
                                                            width: 15,
                                                            height: 12,
                                                            alignSelf: 'center',
                                                            marginRight: 5,
                                                            tintColor: '#6d819c',
                                                        }}
                                                    />
                                                )
                                            }

                                            {item.file_type === 1
                                                ?
                                                (
                                                    <Text style={styles.textTitle}>PDF</Text>
                                                )
                                                :
                                                (
                                                    <Text style={styles.textTitle}>Images({`${item.report_img.length}`})</Text>
                                                )
                                            }

                                            <View style={styles.viewBlueDot}></View>
                                            <Text style={styles.textTitle}>{`${item.c_date}`}</Text>
                                            <View style={styles.viewBlueDot}></View>
                                            <Text style={styles.textTitle}>{`${item.file_sze}`}</Text>
                                        </View>

                                    </View>

                                </TouchableOpacity>
                            }} />
                    </View>

                );

            }
            else {
                return (
                    <View style={{ flexDirection: 'column', flex: 1, alignSelf: 'center', justifyContent: 'center' }}>
                        <View style={{ flexDirection: 'column', flex: 1, alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>
                            <Image
                                source={require('../res/img_no_mdlreds.png')}
                                style={styles.imgNoSuscribe}
                            />
                            <Text style={styles.textNodataTitle}>No Medical Records.</Text>
                            <Text style={styles.textNodata}>Please add your medical records for reference, so that our trainers/dieticians to give you more personalized guidance.</Text>
                        </View>
                    </View>
                );
            }
        }
    }

    async onButtonClicked() {
        Actions.myMdlRedsAdd();
    }

    renderBottomButton() {
        if (this.state.loading === false) {
            return (
                <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient1}>
                    <TouchableOpacity onPress={() => {
                        this.onButtonClicked();

                    }}
                        style={styles.buttonTuch}>
                        <Text style={styles.buttonText}>Add</Text>
                    </TouchableOpacity>
                </LinearGradient>
            );
        }
    }

    async onRetry() {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isNoInternet: false });
                this.getMyMedicalReports();
            }
            else {
                this.setState({ isNoInternet: true });
            }
        });
    }

    render() {
        return (
            <ImageBackground source={require('../res/app_bg.png')} style={styles.mainContainer}>
                <View style={styles.mainContainer}>
                    <Loader loading={this.state.loading} />
                    <NoInternet
                        image={require('../res/img_nointernet.png')}
                        loading={this.state.isNoInternet}
                        onRetry={this.onRetry.bind(this)} />
                    <View style={{
                        flexDirection: 'row',
                        margin: 20,
                    }}>
                        <View style={{ position: 'absolute', zIndex: 111 }}>
                            <TouchableOpacity
                                style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                                onPress={() => this.onBackPressed()}>
                                <Image
                                    source={require('../res/ic_back.png')}
                                    style={styles.backImageStyle}
                                />
                            </TouchableOpacity>
                        </View>

                        <Text style={styles.textHeadTitle}>My Medical Records</Text>
                    </View>

                    {this.renderAllSuscriprionsList()}

                    {this.renderBottomButton()}


                    <CustomDialog
                        visible={this.state.isAlert}
                        title='Alert'
                        desc={this.state.alertMsg}
                        onAccept={this.onAccept.bind(this)}
                        no=''
                        yes='Ok' />

                    <Modal
                        transparent={true}
                        animationType={'none'}
                        visible={this.state.isShowInfoPopup}
                        onRequestClose={() => {
                            //console.log('close modal');
                        }}>
                        <View style={{
                            backgroundColor: '#ffffff',
                            // position: 'relative',
                            flex: 1,
                            justifyContent: 'center',
                        }}>
                            {this.state.selItem.file_type === 1
                                ?
                                (
                                    <View style={{ flex: 1, margin: 10, borderWidth: 0.5, borderColor: '#9c9eb9' }}>
                                        {/*  Some Controls to change PDF resource  */}

                                        <PDFView
                                            style={{ flex: 1 }}
                                            onError={(error) => {
                                                // console.log('onError', error);
                                            }}
                                            onLoad={() => {
                                               // console.log('PDF rendered from file');
                                            }}
                                            // resource={Platform.OS === 'ios' ? 'sample3.pdf' : '/sdcard/Download/sample3.pdf'}
                                            resource={this.state.selItem.pdf_url}
                                            resourceType="url"
                                        />
                                        <TouchableOpacity style={styles.viewTop}
                                            onPress={() => this.setState({ isShowInfoPopup: false, selItem: '' })}>
                                            <Image
                                                source={require('../res/ic_back.png')}
                                                style={styles.popBackImageStyle}
                                            />
                                        </TouchableOpacity>
                                    </View>

                                ) :
                                (
                                    <View style={{ flex: 1, backgroundColor: '#000000', justifyContent: 'center' }}>
                                        <AppIntroSlider
                                            slides={this.state.selItem.report_img}
                                            // onDone={this._onDone}
                                            showSkipButton={false}
                                            showDoneButton={false}
                                            showNextButton={false}
                                            dotStyle={{ backgroundColor: '#ffffff' }}
                                            activeDotStyle={{ backgroundColor: '#8c52ff' }}
                                            renderItem={({ item }) => {
                                                return <FastImage
                                                    style={{ width: wp('90%'), height: hp('80%'), flex: 1, alignSelf: 'center' }}
                                                    source={{
                                                        uri: item.img,
                                                        headers: { Authorization: '12345' },
                                                        priority: FastImage.priority.high,
                                                    }}
                                                    resizeMode={FastImage.resizeMode.contain}
                                                />
                                            }}
                                        />
                                        <TouchableOpacity style={styles.viewTop}
                                            onPress={() => this.setState({ isShowInfoPopup: false, selItem: '' })}>
                                            <Image
                                                source={require('../res/ic_back.png')}
                                                style={styles.popBackImageStyle}
                                            />
                                        </TouchableOpacity>
                                    </View>
                                )
                            }

                        </View>
                    </Modal>
                </View>
            </ImageBackground >
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'center',
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    aroundListStyle: {
        width: wp('95%'),
        backgroundColor: '#ffffff',
        // justifyContent: 'center',
        alignSelf: 'center',
        flexDirection: 'row',
        borderColor: '#ddd',
        // borderRadius: 15,
        borderRadius: 8,
        marginTop: 10,
        marginBottom: 1,
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        padding: 10,
    },
    textWorkName: {
        fontSize: 14,
        fontWeight: '500',
        marginTop: 3,
        marginRight: wp('13%'),
        fontFamily: 'Rubik-Medium',
        color: '#282c37',
    },
    linearGradient: {
        justifyContent: 'center',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderBottomRightRadius: 8,
        borderBottomLeftRadius: 8,
        position: 'relative',
        marginTop: 5,
        padding: 5,
    },
    buttonTuch:
    {
        backgroundColor: 'transparent',
    },
    buttonText: {
        fontSize: 14,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    textTitle: {
        fontSize: 10,
        fontWeight: '400',
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        textAlign: 'left',
        lineHeight: 18,
    },
    viewBlueDot: {
        width: 5,
        height: 5,
        backgroundColor: '#6d819c',
        alignSelf: 'center',
        marginLeft: 5,
        marginRight: 5,
        borderRadius: 5 / 2,
    },
    textNodataTitle: {
        width: wp('85%'),
        fontSize: 16,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
    },
    textNodata: {
        width: wp('85%'),
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
    },
    linearGradient1: {
        width: '90%',
        height: 50,
        bottom: 0,
        borderRadius: 25,
        justifyContent: 'center',
        position: 'absolute',
        alignSelf: 'center',
        marginBottom: 15,
    },
    imgNoSuscribe: {
        width: 270,
        height: 270,
        alignSelf: 'center',
    },
    viewTop: {
        flexDirection: 'column',
        position: 'absolute',
        margin: 15,
        alignItems: 'flex-start',
        padding: 5,
        justifyContent: 'center',
        width: 34,
        height: 34,
        borderRadius: 17,
        backgroundColor: 'white',
        // zIndex: 100,
        top: 1,
    },
    popBackImageStyle: {
        width: 19,
        height: 16,
        tintColor: '#8c52ff',
        alignSelf: 'center',
    },
});

export default MyMedicalRecords;
