/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Image,
    Text,
    ImageBackground,
    TouchableOpacity,
    BackHandler,
    FlatList,
    Platform,
    Modal,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { NoInternet, HomeDialog, Loader } from './common';
import NetInfo from "@react-native-community/netinfo";
import { BASE_URL, SWR, RZP_KEY } from '../actions/types';
import DeviceInfo from 'react-native-device-info';
import RazorpayCheckout from 'react-native-razorpay';
import LogUtils from '../utils/LogUtils.js';
import { allowFunction } from '../utils/ScreenshotUtils.js';
import { plansCheckCountAction } from '../actions/traineeActions'

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

function getNewAmount(val, defVal) {
    var v1 = parseInt(val);
    var value = parseInt(defVal);
    var value1 = parseInt(v1 - value);
    return value1;
}

function getPerMonthAmt(val, defVal, months) {
    var v1 = getNewAmount(val, defVal);
    var value1 = parseInt(v1 / months);

    return value1;
}

class Trainee_BuyPlanModify extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            isAlert: false,
            alertTitle: '',
            alertMsg: '',
            isInternet: false,
            resObj: '',
            arrAllPlans: [],
            noDataMsg: '',
            selSubObj: {},
            subCreateObj: {},
            payRequest: {},
            payResponse: {},
            selPayMethodPop: false,
            arrPaymentMethods: [],
            selPayMethodObj: {},
        };
        this.getAllPlans = this.getAllPlans.bind(this);
        this.getAllPaymentMethods = this.getAllPaymentMethods.bind(this);
    }

    async componentDidMount() {
        try {
            allowFunction();
            NetInfo.fetch().then(state => {
                if (state.isConnected) {
                    this.getAllPlans();
                    this.getAllPaymentMethods();
                }
                else {
                    this.setState({ isInternet: true });
                }
            });
            this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
                this.onBackPressed();
                return true;
            });
        } catch (error) {
            console.log(error);
        }
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        Actions.pop();
    }

    async getAllPlans() {
        try {
            this.setState({ loading: true });
            let token = await AsyncStorage.getItem('token');
            fetch(
                `${BASE_URL}/trainee/modifyplans`,
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${token}`,
                    },
                    body: JSON.stringify({
                        up_id: this.props.myPlanObj.up_id,
                    }),
                },
            )
                .then(processResponse)
                .then(res => {
                    const { statusCode, data } = res;
                    LogUtils.infoLog1('statusCode', statusCode);
                    LogUtils.infoLog1('data ', data);
                    // LogUtils.infoLog1('plans ', data.data.plans);

                    if (statusCode >= 200 && statusCode <= 300) {
                        this.setState({ loading: false, resObj: data.data });
                        if (data.data.plans.length > 0) {
                        }
                        else {
                            this.setState({ noDataMsg: 'There is no modify plans available for you' });
                        }
                    } else {
                        this.setState({ noDataMsg: data.message });
                        if (data.message === 'You are not authenticated!') {
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                        } else {
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                        }
                    }
                })
                .catch(function (error) {
                    this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error), alertTitle: 'Alert' });
                });
        } catch (error) {
            console.log(error);
        }
    }

    async getAllPaymentMethods() {
        try {
            let token = await AsyncStorage.getItem('token');
            fetch(
                `${BASE_URL}/trainee/paymenttypes`,
                {
                    method: 'GET',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${token}`,
                    },
                },
            )
                .then(processResponse)
                .then(res => {
                    const { statusCode, data } = res;
                    LogUtils.infoLog1('statusCode', statusCode);
                    LogUtils.infoLog1('arrPaymentMethods ', data);

                    if (statusCode >= 200 && statusCode <= 300) {
                        this.setState({ arrPaymentMethods: data.data });
                    } else {
                        if (data.message === 'You are not authenticated!') {
                            //this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                        } else {
                            //this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                        }
                    }
                })
                .catch(function (error) {
                    //this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error), alertTitle: 'Alert' });
                });
        } catch (error) {
            console.log(error);
        }
    }


    async getSubscriptionDetails(data) {
        try {
            if (!DeviceInfo.getModel() === 'iPhone 6') {
                this.setState({ loading: true });
            }

            let token = await AsyncStorage.getItem('token');

            LogUtils.infoLog1('sub req ', data);

            fetch(
                `${BASE_URL}/trainee/create_subscription`,
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${token}`,
                    },
                    body: data,
                },
            )
                .then(processResponse)
                .then(res => {
                    const { statusCode, data } = res;
                    LogUtils.infoLog1('statusCode', statusCode);
                    LogUtils.infoLog1('data', data);

                    if (statusCode >= 200 && statusCode <= 300) {
                        this.setState({ loading: false, subCreateObj: data });
                        LogUtils.infoLog(data);

                        var options = {
                            key: RZP_KEY,
                            subscription_id: data.sub_code,
                            // subscription_card_change: 1,
                            name: 'Transform',
                            description: this.state.selSubObj.title,
                            // notes: {
                            //   note_key_1: 'Tea. Earl Grey. Hot',
                            //   note_key_2: 'Make it so.'
                            // },
                            // prefill: {
                            //     email: 'venkatesh@appoids.in',
                            //     contact: '8861512641',
                            //     name: 'Venkatesh A'
                            // },
                            theme: { color: '#8c52ff' }
                        };

                        if (Platform.OS === 'ios') {
                            this.setState({ selPayMethodPop: false });
                        }

                        this.setState({ payRequest: options });
                        LogUtils.infoLog1('Payment Request : ', options);
                        setTimeout(() => {
                            RazorpayCheckout.open(options).then((data) => {
                                // handle success
                                LogUtils.infoLog1('Payment Response : ', data);
                                this.setState({ payResponse: data });
                                // alert(`Success: ${data.razorpay_payment_id}`);
                                this.savePlanSubscription();

                            }).catch((error) => {
                                // handle failure
                                LogUtils.infoLog1('Payment Fail : ', error);
                                this.setState({ isAlert: true, alertMsg: 'Your payment method was declined.Please try again later.', alertTitle: 'Declined!' });
                            });
                        }, 100);

                    } else {
                        if (data.message === 'You are not authenticated!') {
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                        } else {
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                        }
                    }
                })
                .catch(function (error) {
                    this.setState({ loading: false, isAlert: true, alertMsg: SWR, alertTitle: 'Alert' });
                });
        } catch (error) {
            console.log(error);
        }
    }

    async getOrderDetails(data) {
        try {
            if (!DeviceInfo.getModel() === 'iPhone 6') {
                this.setState({ loading: true });
            }
            let token = await AsyncStorage.getItem('token');

            LogUtils.infoLog1('ord req ', data);

            fetch(
                `${BASE_URL}/trainee/create_order`,
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${token}`,
                    },
                    body: data,
                },
            )
                .then(processResponse)
                .then(res => {
                    const { statusCode, data } = res;
                    LogUtils.infoLog1('statusCode', statusCode);
                    LogUtils.infoLog1('data', data);

                    if (statusCode >= 200 && statusCode <= 300) {
                        this.setState({ loading: false, subCreateObj: data });
                        LogUtils.infoLog(data);

                        var options = {
                            description: this.state.selSubObj.title,
                            // image: 'http://fitness.whamzone.com/assets/images/logo.png',
                            // currency: 'INR',
                            key: RZP_KEY,
                            // amount: '2999',
                            name: 'Transform',
                            order_id: data.ord_code,//Replace this with an order_id created using Orders API. Learn more at https://razorpay.com/docs/api/orders.
                            // prefill: {
                            //     email: 'venkatesh@appoids.in',
                            //     contact: '861512641',
                            //     name: 'Venkatesh Annavaram'
                            // },
                            theme: { color: '#8c52ff' }
                        }

                        if (Platform.OS === 'ios') {
                            this.setState({ selPayMethodPop: false });
                        }

                        this.setState({ payRequest: options });
                        LogUtils.infoLog1('Payment Request : ', options);
                        setTimeout(() => {
                            RazorpayCheckout.open(options).then((data) => {

                                // handle success
                                LogUtils.infoLog1('Payment Response : ', data);
                                this.setState({ payResponse: data });
                                // alert(`Success: ${data.razorpay_payment_id}`);
                                this.savePlanSubscription();

                            }).catch((error) => {
                                // handle failure
                                LogUtils.infoLog1('Payment Fail : ', error);
                                LogUtils.firebaseEventLog('click', {
                                    p_id: this.props.selPlan.plan_id,
                                    p_category: 'Plans',
                                    p_name: `${this.props.selPlan.name} - ${this.state.selSubObj.duration} - Payment Fail`,
                                });
                                this.setState({ isAlert: true, alertMsg: 'Your payment method was declined.Please try again later.', alertTitle: 'Declined!' });
                            });
                        }, 100);

                    } else {
                        if (data.message === 'You are not authenticated!') {
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                        } else {
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                        }
                    }
                })
                .catch(function (error) {
                    this.setState({ loading: false, isAlert: true, alertMsg: SWR, alertTitle: 'Alert' });
                });
        } catch (error) {
            console.log(error);
        }
    }

    async savePlanSubscription() {
        try {

            if (!DeviceInfo.getModel() === 'iPhone 6') {
                this.setState({ loading: true });
            }
            let token = await AsyncStorage.getItem('token');
            let data;

            if (this.state.selPayMethodObj.can_subscribe === this.state.selSubObj.can_subscribe) {

                if (this.state.selPayMethodObj.can_subscribe === 1) {
                    data = JSON.stringify({
                        plan_id: this.state.selSubObj.plan_id,
                        pp_id: this.state.selSubObj.pp_id,
                        amount: getNewAmount(this.state.selSubObj.new_total_price, this.state.resObj.credits),
                        rzp_s_id: this.state.subCreateObj.sub_id,
                        payment_id: this.state.payResponse.razorpay_payment_id,
                        subscription_id: this.state.payResponse.razorpay_subscription_id,
                        signature: this.state.payResponse.razorpay_signature,
                        request: this.state.payRequest,
                        // rzp_o_id: '',
                        // order_id: '',
                        modify: 1,
                        up_id: this.props.myPlanObj.up_id,
                        credits: this.state.resObj.credits,
                    });
                }
                else {

                    data = JSON.stringify({
                        plan_id: this.state.selSubObj.plan_id,
                        pp_id: this.state.selSubObj.pp_id,
                        amount: getNewAmount(this.state.selSubObj.new_total_price, this.state.resObj.credits),
                        // rzp_s_id: this.state.subCreateObj.sub_id,
                        payment_id: this.state.payResponse.razorpay_payment_id,
                        // subscription_id: this.state.payResponse.razorpay_subscription_id,
                        signature: this.state.payResponse.razorpay_signature,
                        request: this.state.payRequest,
                        rzp_o_id: this.state.subCreateObj.ord_id,
                        order_id: this.state.subCreateObj.ord_code,
                        modify: 1,
                        up_id: this.props.myPlanObj.up_id,
                        credits: this.state.resObj.credits,
                    });
                }

            }
            else {

                if (this.state.selPayMethodObj.can_subscribe === 1) {
                    data = JSON.stringify({
                        plan_id: this.state.selSubObj.alt_plan_id,
                        pp_id: this.state.selSubObj.alt_pp_id,
                        amount: getNewAmount(this.state.selSubObj.alt_total_price, this.state.resObj.credits),
                        rzp_s_id: this.state.subCreateObj.sub_id,
                        payment_id: this.state.payResponse.razorpay_payment_id,
                        subscription_id: this.state.payResponse.razorpay_subscription_id,
                        signature: this.state.payResponse.razorpay_signature,
                        request: this.state.payRequest,
                        // rzp_o_id: '',
                        // order_id: '',
                        modify: 1,
                        up_id: this.props.myPlanObj.up_id,
                        credits: this.state.resObj.credits,
                    });
                }
                else {
                    data = JSON.stringify({
                        plan_id: this.state.selSubObj.alt_plan_id,
                        pp_id: this.state.selSubObj.alt_pp_id,
                        amount: getNewAmount(this.state.selSubObj.alt_total_price, this.state.resObj.credits),
                        // rzp_s_id: this.state.subCreateObj.sub_id,
                        payment_id: this.state.payResponse.razorpay_payment_id,
                        // subscription_id: this.state.payResponse.razorpay_subscription_id,
                        signature: this.state.payResponse.razorpay_signature,
                        request: this.state.payRequest,
                        rzp_o_id: this.state.subCreateObj.ord_id,
                        order_id: this.state.subCreateObj.ord_code,
                        modify: 1,
                        up_id: this.props.myPlanObj.up_id,
                        credits: this.state.resObj.credits,
                    });
                }
            }

            LogUtils.infoLog1('save Sub req ', data);

            fetch(
                `${BASE_URL}/trainee/save_subscription`,
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${token}`,
                    },
                    body: data,
                },
            )
                .then(processResponse)
                .then(res => {
                    const { statusCode, data } = res;
                    LogUtils.infoLog1('statusCode', statusCode);
                    LogUtils.infoLog1('data', data);

                    if (statusCode >= 200 && statusCode <= 300) {
                        // this.setState({ loading: false, isSuccess: true, sucMsg: data.message, titMsg: data.title });
                        this.setState({ loading: false });

                        Actions.traBuySuccess({ from: 'payment', sucResponse: data });
                        this.setState({ selPayMethodPop: false });
                    } else {
                        if (data.message === 'You are not authenticated!') {
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                        } else {
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                        }
                    }
                })
                .catch(function (error) {
                    this.setState({ loading: false, isAlert: true, alertMsg: SWR, alertTitle: 'Alert' });
                });
        } catch (error) {
            console.log(error);
        }
    }


    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
        this.setState({ isAlert: false, alertMsg: '', alertTitle: '' });
    }

    async onRetry() {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isInternet: false });
                this.getAllPlans();
            }
            else {
                this.setState({ isInternet: true });
            }
        });
    }

    FlatListItemSeparator1 = () => {
        return (
            <View
                style={{
                    height: 10,
                    width: 1,
                    backgroundColor: "transparent",
                }}
            />
        );
    }

    FlatListSeparatorPayMethods = () => {
        return (
            <View
                style={{
                    height: 12,
                    width: 1,
                    backgroundColor: "transparent",
                }}
            />
        );
    }

    renderInstructions() {
        try {
            if (!isEmpty(this.state.resObj)) {
                if (Array.isArray(this.state.resObj.instructions) && this.state.resObj.instructions.length) {
                    let arrInstructions = this.state.resObj.instructions.map((item, i) => {
                        return <View key={i} style={styles.viewListAllInner}>
                            <View style={styles.viewBlueDot}></View>
                            <Text style={styles.textInst}>{item.instruction}</Text>
                        </View>
                    });
                    return (
                        <View style={styles.viewInst}>
                            {arrInstructions}
                        </View>
                    );
                }
            }
        } catch (error) {
            console.log(error);
        }
    }

    renderAllPlansList() {
        try {
            if (this.state.resObj && !isEmpty(this.state.resObj)) {
                if (Array.isArray(this.state.resObj.plans) && this.state.resObj.plans.length) {
                    return (
                        <View style={styles.viewMain}>

                            {this.renderInstructions()}

                            <FlatList
                                contentContainerStyle={{ paddingBottom: hp('20%') }}
                                showsVerticalScrollIndicator={false}
                                data={this.state.resObj.plans}
                                keyExtractor={(item, index) => "plans" + index}
                                ItemSeparatorComponent={this.FlatListItemSeparator1}
                                // ListEmptyComponent={this.ListEmptyView}
                                renderItem={({ item, index }) => {
                                    return <TouchableOpacity key={item.plan_id+item.pp_id+"Plans"} style={styles.aroundListStyle}
                                        onPress={async () => {
                                            this.setState({ selPayMethodPop: true, selSubObj: item, });
                                            // this.getSubscriptionDetails();
                                            let token = await AsyncStorage.getItem('token');
                                            let params = { "p_id": item.plan_id, "pp_id": item.pp_id };
                                            plansCheckCountAction(token, params);
                                        }}>
                                        <View style={{
                                            flexDirection: 'row',
                                            padding: 15,
                                            width: wp('90%'),
                                        }}>

                                            <View style={{ flexDirection: 'column', alignItems: 'flex-start', alignContent: 'center', justifyContent: 'center', }}>

                                                <Text style={styles.txtPlanName}>{`${item.title}`}</Text>
                                                <Text style={styles.txtPlanDesc}>{`${item.description}`}</Text>
                                                <Text style={styles.textPlanMonths} numberOfLines={1}>{`${item.duration}`}</Text>

                                                <View style={styles.nutritionInnerView}>

                                                    <Text style={styles.textNutTitle}>You pay Rs.{getPerMonthAmt(item.new_total_price, this.state.resObj.credits, item.month_duration)}</Text>
                                                    <Text style={styles.textNutDesc}>/{`${item.period}`}</Text>
                                                </View>
                                                <Text style={styles.txtPlanCredits}>{`${this.state.resObj.credits}`} credit applied</Text>

                                            </View>
                                        </View>
                                    </TouchableOpacity >
                                }} />
                        </View>
                    );
                }
                else {
                    return (

                        <View style={{ flexDirection: 'row', flex: 1, }}>
                            <Text style={styles.textNodata}>{this.state.noDataMsg}</Text>
                        </View>
                    );
                }
            }
        } catch (error) {
            console.log(error);
        }
    }

    toggleSelPayMethodModal(visible) {
        this.setState({ selPayMethodPop: visible });
    }

    pressPaymentMethod = (hey) => {
        try {
            this.setState({ selPayMethodObj: hey });

            if (hey.can_subscribe === this.state.selSubObj.can_subscribe) {

                let data = JSON.stringify({
                    plan_id: this.state.selSubObj.plan_id,
                    pp_id: this.state.selSubObj.pp_id,
                    amount: getNewAmount(this.state.selSubObj.new_total_price, this.state.resObj.credits),
                    plan_code: this.state.selSubObj.plan_code,
                    modify: 1,
                });

                if (hey.can_subscribe === 1) {
                    this.getSubscriptionDetails(data);
                }
                else {
                    this.getOrderDetails(data);
                }
            }
            else {

                let data = JSON.stringify({
                    plan_id: this.state.selSubObj.alt_plan_id,
                    pp_id: this.state.selSubObj.alt_pp_id,
                    amount: getNewAmount(this.state.selSubObj.alt_total_price, this.state.resObj.credits),
                    plan_code: this.state.selSubObj.plan_code,
                    modify: 1,
                });

                if (hey.can_subscribe === 1) {
                    this.getSubscriptionDetails(data);
                }
                else {
                    this.getOrderDetails(data);
                }
            }
        } catch (error) {
            console.log(error);
        }
    }

    render() {
        return (
            <ImageBackground source={require('../res/app_bg.png')} style={styles.containerStyle}>
                <View style={styles.containerStyle}>
                    <Loader loading={this.state.loading} />

                    <NoInternet
                        image={require('../res/img_nointernet.png')}
                        loading={this.state.isInternet}
                        onRetry={this.onRetry.bind(this)} />

                    <View style={{
                        flexDirection: 'row',
                        margin: 20,
                    }}>
                        <View style={{ position: 'absolute', zIndex: 111 }}>
                            <TouchableOpacity
                                onPress={() => this.onBackPressed()}>
                                <Image
                                    source={require('../res/ic_back.png')}
                                    style={styles.backImageStyle}
                                />
                            </TouchableOpacity>
                        </View>

                        <Text style={styles.textHeadTitle}>Choose a plan</Text>
                    </View>

                    {this.renderAllPlansList()}

                </View>

                <Modal
                    backdropColor="#B4B3DB"
                    backdropOpacity={0.8}
                    animationIn="zoomInDown"
                    animationOut="zoomOutUp"
                    animationInTiming={600}
                    animationOutTiming={600}
                    backdropTransitionInTiming={600}
                    backdropTransitionOutTiming={600}
                    visible={this.state.selPayMethodPop}
                    onRequestClose={() => { this.toggleSelPayMethodModal(!this.state.selPayMethodPop) }}>

                    <View style={{
                        flex: 1,
                        alignItems: 'center',
                        backgroundColor: '#ffffff',
                        justifyContent: 'center',
                    }}>

                        <TouchableOpacity style={styles.viewTop}
                            onPress={() => { this.toggleSelPayMethodModal(!this.state.selPayMethodPop) }}>
                            <Image
                                source={require('../res/ic_back.png')}
                                style={styles.backImageStyle}
                            />
                        </TouchableOpacity>

                        <Text style={styles.TextHeaderStyle}>Select your payment method</Text>
                        <FlatList
                            contentContainerStyle={{ paddingBottom: hp('10%') }}
                            showsVerticalScrollIndicator={false}
                            data={this.state.arrPaymentMethods}
                            keyExtractor={(item, index) => "arrPaymentMethods" + index}
                            ItemSeparatorComponent={this.FlatListSeparatorPayMethods}
                            renderItem={({ item }) => {
                                return <TouchableOpacity key={item.pt_id} onPress={() => {
                                    // this.toggleSelPayMethodModal(!this.state.selPayMethodPop)
                                    this.pressPaymentMethod(item)
                                }}>
                                    <View style={styles.vPayBg}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            source={{ uri: item.img }}
                                            style={{
                                                width: 30,
                                                height: 30,
                                                alignSelf: 'center',
                                            }}
                                        />

                                        <View style={styles.performancecolumn}>
                                            <Text style={styles.performancetextStyle}>{item.title}</Text>
                                            <Text style={styles.waterdranktextStyle}>{item.descp}</Text>
                                        </View>
                                    </View>

                                </TouchableOpacity>
                            }} />
                    </View>

                </Modal>

                <HomeDialog
                    visible={this.state.isAlert}
                    title={this.state.alertTitle}
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    no=''
                    yes='Ok' />

            </ImageBackground>
        );
    }
}


const styles = StyleSheet.create({
    containerStyle: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    viewMain: {
        flexDirection: 'column',
    },
    viewInst: {
        flexDirection: 'column',
        marginLeft: 25,
        marginRight: 15,
        alignSelf: 'flex-start',
    },
    backImageStyle: {
        width: 19,
        height: 16,
        alignSelf: 'flex-start',
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    textNodata: {
        width: wp('90%'),
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        padding: 10,
        margin: 20,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
    },
    aroundListStyle: {
        width: wp('90%'),
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        alignSelf: 'center',
        flexDirection: 'column',
        borderColor: '#ddd',
        // borderRadius: 15,
        borderRadius: 8,
        position: 'relative',
        marginTop: 10,
        marginBottom: 1,
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    txtPlanName: {
        width: wp('85%'),
        fontSize: 16,
        fontWeight: '500',
        color: '#282c37',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
    },
    textPlanMonths: {
        fontSize: 14,
        fontWeight: '500',
        marginTop: 8,
        fontFamily: 'Rubik-Medium',
        color: '#8c52ff',
        letterSpacing: 0.2,
    },
    txtPlanDesc: {
        width: wp('80%'),
        fontSize: 11,
        fontWeight: '400',
        color: '#282c37',
        marginTop: 5,
        fontFamily: 'Rubik-Regular',
        letterSpacing: 0.5,
    },
    txtPlanCredits: {
        fontSize: 11,
        fontWeight: '400',
        marginTop: 5,
        alignSelf: 'flex-start',
        textAlign: 'left',
        fontFamily: 'Rubik-Regular',
        letterSpacing: 0.5,
        color: '#6d819c',
    },
    nutritionInnerView: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignSelf: 'flex-start',
        marginTop: 5,
    },
    textNutTitle: {
        fontSize: 14,
        fontWeight: '500',
        color: '#2d3142',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
    },
    textNutDesc: {
        fontSize: 11,
        fontWeight: '400',
        color: '#9c9eb9',
        marginTop: 5,
        fontFamily: 'Rubik-Regular',
        letterSpacing: 0.2,
    },
    viewListAllInner: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignSelf: 'flex-start',
        alignItems: 'center',
        marginTop: 5,
        marginBottom: 5,
    },
    viewBlueDot: {
        width: 10,
        height: 10,
        backgroundColor: '#8c52ff',
        alignSelf: 'flex-start',
        marginTop: 2,
        borderRadius: 5,
    },
    textInst: {
        fontSize: 12,
        marginLeft: 10,
        fontWeight: '400',
        color: '#2d3142',
        lineHeight: 15,
        textAlign: 'left',
        letterSpacing: 0.3,
        fontFamily: 'Rubik-Regular',
    },
    TextHeaderStyle: {
        color: '#2d3142',
        fontFamily: 'Rubik-Medium',
        fontSize: 18,
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 20,
        letterSpacing: 0.2,
        alignSelf: 'center',
        textAlign: 'center',
    },
    viewTop: {
        flexDirection: 'column',
        // position: 'absolute',
        margin: 20,
        alignItems: 'center',
        padding: 5,
        justifyContent: 'center',
        alignSelf: 'flex-start',
        width: 34,
        height: 34,
        borderRadius: 17,
        // backgroundColor: 'white',
        // shadowColor: 'rgba(0, 0, 0, 0.08)',
        // shadowOffset: { width: 5, height: 0 },
        // shadowOpacity: 0.25,
        // shadowRadius: 10,
        // elevation: 6,
    },
    vPayBg: {
        width: wp('90%'),
        marginLeft: wp('5%'),
        marginRight: wp('5%'),
        // backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderRadius: 6,
        position: 'relative',
        alignSelf: 'center',
        borderColor: '#b0b0b0',
        borderWidth: 0.3,
        // shadowColor: "#000",
        // shadowOffset: { width: 5, height: 0 },
        // shadowOpacity: 0.25,
        // shadowRadius: 10,
        // elevation: 5,
        // marginTop: 2.5,
        padding: 12,
    },
    performancecolumn: {
        flexDirection: 'column',
        flex: 1,
        alignItems: 'flex-start',
        marginLeft: 12,
    },
    performancetextStyle: {
        fontSize: 14,
        fontWeight: '500',
        color: '#2d3142',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    waterdranktextStyle: {
        fontSize: 12,
        color: '#C3C1CE',
        textAlign: 'left',
        lineHeight: 16,
        marginTop: 3,
        fontFamily: 'Rubik-Regular',
    },
});

export default Trainee_BuyPlanModify;
