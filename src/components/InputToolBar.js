import React from 'react';
import { Image } from 'react-native';
import { InputToolbar, Actions, Composer, Send } from 'react-native-gifted-chat';

export const renderInputToolbar = (props) => (
  <InputToolbar
    {...props}
    containerStyle={{
      // backgroundColor: '#222B45',
      paddingTop: 1,
      paddingBottom:3
    }}
    primaryStyle={{ alignItems: 'center' }}
  />
);

// export const renderActions = (props) => (
//   <Actions
//     {...props}
//     containerStyle={{
//       width: 44,
//       height: 44,
//       alignItems: 'center',
//       justifyContent: 'center',
//       marginLeft: 4,
//       marginRight: 4,
//       marginBottom: 0,
//     }}
//     icon={() => (
//       <Image
//         // style={{ width: 20, height: 20 }}
//         source={require('../res/ic_attachment.png')}
//       />
//     )}
//     options={{
//       'Camera': () => {
//         console.log('Camera');
//       },
//       'Choose From Library': () => {
//         console.log('Choose From Library');
//       },
//       Cancel: () => {
//         console.log('Cancel');
//       },
//     }}
//     optionTintColor="#222B45"
//   />
// );

export const renderComposer = (props) => (
  <Composer
    {...props}
    textInputStyle={{
      color: '#222B45',
      backgroundColor: '#EDF1F7',
      borderWidth: 1,
      borderRadius: 5,
      borderColor: '#E4E9F2',
      paddingTop: 8.5,
      paddingHorizontal: 12,
      marginLeft: 0,
      fontSize: 14,
    }}
  />
);

export const renderSend = (props) => (
  <Send
    {...props}
    disabled={!props.text}
    containerStyle={{
      width: 44,
      height: 44,
      alignItems: 'center',
      justifyContent: 'center',
      marginHorizontal: 4,
    }}
  >
    <Image
      style={{ width: 25, height: 24 }}
      source={require('../res/ic_send.png')}
    />
  </Send>
);