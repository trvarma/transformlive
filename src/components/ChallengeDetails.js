import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    BackHandler,
    ImageBackground,
    ScrollView,
    Dimensions,
    FlatList,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { ProgressBar, Loader, CustomDialog } from './common';
import { withNavigationFocus } from 'react-navigation';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { BASE_URL, SWR } from '../actions/types';
import LinearGradient from 'react-native-linear-gradient';
import ImagePicker from 'react-native-image-picker';
import Video from 'react-native-video';
import NetInfo from "@react-native-community/netinfo";
const screenHeight = Math.round(Dimensions.get('window').height);
const screenWidth = Math.round(Dimensions.get('window').width);
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import DeviceInfo from 'react-native-device-info';
import LogUtils from '../utils/LogUtils.js';
import { allowFunction } from '../utils/ScreenshotUtils.js';

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}
let token = '', userId = '';

const futch = (url, opts = {}, onProgress) => {
    LogUtils.infoLog1(url, opts)
    return new Promise((res, rej) => {
        var xhr = new XMLHttpRequest();
        xhr.open(opts.method || 'get', url);
        for (var k in opts.headers || {})
            xhr.setRequestHeader(k, opts.headers[k]);
        xhr.onload = e => res(e.target);
        xhr.onerror = rej;
        if (xhr.upload && onProgress)
            xhr.upload.onprogress = onProgress; // event.loaded / event.total * 100 ; //event.lengthComputable
        xhr.send(opts.body);
    });
}

const createFormData = (video, body) => {
    const data = new FormData();
    LogUtils.infoLog1("video url", video);
    if (video) {
        data.append('file', {
            uri: video,
            name: video
                .toString()
                .replace(
                    'content://com.fitnes.provider/root/storage/emulated/0/Pictures/images/',
                    '',
                ),
            type: 'video/mp4', // or photo.type
        });
    }
    data.append('data', body);
    return data;
};

class ChallengeDetails extends Component {

    constructor(props) {
        super(props);
        this.state = {
            challenges: [],
            paused: true,
            isSuccess: false,
            sucMsg: '',
            isAlert: false,
            alertMsg: '',
            alertTitle: '',
            loading: false,
            pageNo: 1,
            isTabClicked: 1,
            filepath: {
                data: '',
                uri: '',
            },
            fileData: '',
            fileUri: '',
            fileType: '',
            progress: 0,
            isProgress: false,
            resMyChalObj: '',
            refresh: false,
            challengeDetails: {},
            noData: '',
        };
    }
    async componentDidMount() {
        allowFunction();
        token = await AsyncStorage.getItem('token');
        userId = await AsyncStorage.getItem('userId');

        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ loading: true });
                this.getChallengeDetails();
                // this.getUserMyChallengesVideos();
            }
            else {
                this.setState({ isAlert: true, alertMsg: 'Please enable your internet connection and try again' });
            }
        });

        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        Actions.pop();
    }

    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
        this.setState({ alertMsg: '' });
        this.setState({ isAlert: false });
        this.setState({ fileUri: "" });

        if (this.state.isSuccess) {
            this.setState({ loading: true });
            // this.getUserMyChallengesVideos();
            this.setState({ isSuccess: false });
        }
    }

    async getChallengeDetails() {
        fetch(
            `${BASE_URL}/trainee/challenge?c_id=${this.props.cid}`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1("data", data.data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false, isAlert: false, isSuccess: true, alertMsg: data.message, challengeDetails: data.data });

                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: SWR });
            });
    }

    async getUserMyChallengesVideos() {
        this.setState({ refresh: !this.state.refresh });
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/getmychallengeVd`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    c_id: this.props.cid,
                    page: 1,
                    pagesize: 20,
                    width: screenWidth,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false });
                    this.setState({ ...this.state, resMyChalObj: data.data });
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
            });
    }

    async saveChallengeVideo() {
        futch(`${BASE_URL}/trainee/saveuserchlngvideo`, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
                Authorization: `Bearer ${token}`,
            },
            body: createFormData(this.state.fileUri, JSON.stringify({
                uc_id: this.state.challengeDetails.uc_id,
                c_id: this.state.challengeDetails.c_id,
                comments: this.state.post,
            })),
        }, (progressEvent) => {
            const progress = Math.floor((progressEvent.loaded / progressEvent.total) * 100);
            this.setState({ progress: progress });
        })
            .then(res => {
                this.setState({ isProgress: false })
                const statusCode = res.status;
                LogUtils.infoLog1('statusCode', statusCode);
                const data = res.response;
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false, isAlert: true, isSuccess: true, alertMsg: 'Your Video has been uploaded successfully', alertTitle: 'Congratulations' });
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: 'User Challenge video not saved', alertTitle: 'Error' });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: 'User Challenge video not saved', alertTitle: 'Error' });
                    }
                }
                this.setState({ isProgress: false })
            })
            .catch(function (error) {
                LogUtils.infoLog1('upload error', error);
                feedPostError(dispatch, error);
            });
    }

    renderItem(item) {
        return (
            <View style={{ flexDirection: 'column', justifyContent: 'center' }}>
                <Image
                    progressiveRenderingEnabled={true}
                    resizeMethod="resize"
                    style={styles.image}
                    source={item.thumbnail} />
                <Image
                    progressiveRenderingEnabled={true}
                    resizeMethod="resize"
                    source={require('../res/ic_play.png')}
                    style={styles.playImageStyle}
                />
            </View>
        );
    }

    renderBottom() {
        if (this.state.isTabClicked === 2) {
            if (this.state.challengeDetails.is_expired === 1) {
                return (
                    <View >
                    </View>
                );
            } else {
                return (
                    <View style={styles.bottom}>
                        <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                            <TouchableOpacity
                                onPress={() => {
                                    if (this.state.challengeDetails.is_accept === 1)
                                        this.chooseImage()
                                    else {
                                        this.setState({ loading: false, isAlert: true, alertMsg: 'Accept challenge to share your videos', alertTitle: 'Alert' });
                                    }
                                }}
                                style={styles.buttonTuch}>
                                <Text style={styles.buttonText}>Upload Video</Text>
                            </TouchableOpacity>
                        </LinearGradient>
                    </View>
                );
            }
        }
    }

    renderItem = ({ item, index }) => {
        return (
            <View style={{ felx: 1, flexDirection: 'column', justifyContent: 'center' }}>
                <TouchableOpacity style={{ flexDirection: 'column', justifyContent: 'center' }}
                    onPress={() => {
                        Actions.play({ videoURL: item.video });
                    }}>
                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        style={styles.item}
                        source={{ uri: item.thumbnail }} />
                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={require('../res/ic_play.png')}
                        style={styles.playImageStyle}
                    />
                </TouchableOpacity>
            </View>
        );
    }

    renderTabBar() {
        if (this.state.loading === false) {
            if (this.state.challengeDetails.is_accept === 0) {
                return (
                    <ImageBackground source={require('../res/ic_tab_bg.png')} style={styles.header}>
                        <View style={styles.detailsHeader}>
                            <Text style={styles.textIntro}>DETAILS</Text>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_ver_line.png')}
                                style={styles.detailsheaderSel} />
                        </View>
                    </ImageBackground>
                );
            } else {
                if (this.state.isTabClicked === 1) {
                    return (
                        <ImageBackground source={require('../res/ic_tab_bg.png')} style={styles.header}>
                            <TouchableOpacity onPress={() => this.setState({ isTabClicked: 1 })} style={styles.headerInner}>
                                <Text style={styles.textIntro}>DETAILS</Text>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_ver_line.png')}
                                    style={styles.headerSel} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.setState({ isTabClicked: 2 })} style={styles.headerInner}>
                                <Text style={styles.textPlan}>MY ENTRIES</Text>
                                <View
                                    style={styles.headerSel} />
                            </TouchableOpacity>

                        </ImageBackground>
                    );
                } else if (this.state.isTabClicked === 2) {
                    return (
                        <ImageBackground source={require('../res/ic_tab_bg.png')} style={styles.header}>
                            <TouchableOpacity onPress={() => this.setState({ isTabClicked: 1 })} style={styles.headerInner}>
                                <Text style={styles.textPlan}>DETAILS</Text>
                                <View
                                    style={styles.headerSel} />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.setState({ isTabClicked: 2 })} style={styles.headerInner}>
                                <Text style={styles.textIntro}>MY ENTRIES</Text>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_ver_line.png')}
                                    style={styles.headerSel} />
                            </TouchableOpacity>
                        </ImageBackground>
                    );
                }
            }
        }
    }

    chooseImage = () => {
        let options = {
            title: 'Video Picker',

            takePhotoButtonTitle: 'Record Video...',
            mediaType: 'video',
            videoQuality: 'medium',
            durationLimit: 120,
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.showImagePicker(options, response => {
            if (response.didCancel) {
            } else if (response.error) {
            } else {
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };
                // alert(JSON.stringify(response));s
                LogUtils.infoLog1('response', JSON.stringify(response));
                LogUtils.infoLog1('fileUri', response.uri);
                this.setState({
                    filePath: response,
                    fileData: response.data,
                    fileUri: response.uri,
                    fileType: response.type,
                });

                this.setState({ isProgress: true })
                this.saveChallengeVideo();
            }
        });
    };

    launchCamera = () => {
        let options = {
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.launchCamera(options, response => {
            if (response.didCancel) {
                LogUtils.infoLog('User cancelled image picker');
            } else if (response.error) {
                LogUtils.infoLog1('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                LogUtils.infoLog1('User tapped custom button: ', response.customButton);
            } else {
                LogUtils.infoLog1('launchCamera fileUri', response.uri);
                this.setState({
                    filePath: response,
                    fileData: response.data,
                    fileUri: response.uri,
                    fileType: response.type,
                });
            }
        });
    };

    launchImageLibrary = () => {
        let options = {
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.launchImageLibrary(options, response => {
            // LogUtils.infoLog1('Response = ', response);

            if (response.didCancel) {
                // LogUtils.infoLog('User cancelled image picker');
            } else if (response.error) {
                // LogUtils.infoLog1('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                // LogUtils.infoLog1('User tapped custom button: ', response.customButton);
            } else {
                // LogUtils.infoLog1('launchImageLibrary fileUri', response.uri);
                this.props.videoUri = '';
                this.setState({
                    filePath: response,
                    fileData: response.data,
                    fileUri: response.uri,
                    fileType: response.type,
                });

            }
        });
    };

    renderPlayButton() {
        if (this.state.paused) {
            return (
                <Image
                    progressiveRenderingEnabled={true}
                    resizeMethod="resize"
                    source={require('../res/ic_play_white.png')}
                    style={styles.postPlayImageStyle}
                />
            );
        }
    }

    onEnd = () => this.setState({ paused: true });

    renderVideoData() {
        if (this.state.fileUri) {
            return (
                <TouchableOpacity onPress={() => {
                    this.setState({ paused: !this.state.paused });
                }}>
                    <Video
                        onEnd={this.onEnd}
                        onLoad={this.onLoad}
                        onLoadStart={this.onLoadStart}
                        onProgress={this.onProgress}
                        paused={this.state.paused}
                        ref={videoPlayer => (this.videoPlayer = videoPlayer)}
                        resizeMode={this.state.screenType}
                        source={{ uri: this.state.fileUri }}
                        style={styles.mediaPlayer}
                        onEnd={this.onEnd}
                        volume={10}
                    />
                    <TouchableOpacity
                        style={styles.playButton}
                        onPress={() => {
                            this.setState({ paused: !this.state.paused });
                        }}>
                        {this.renderPlayButton()}
                    </TouchableOpacity>
                </TouchableOpacity>
            );
        }
    }

    renderItemNew = ({ item, index }) => {
        return (
            <View key={item.ucv_id} style={styles.viewBgVideo}>
                <View style={styles.viewbgProfilePic}>
                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        style={styles.avatar}
                        source={{ uri: item.profile_img + '?w=50' }} />
                    <View style={styles.column}>
                        <Text style={styles.textStyle}>{item.crt_date}</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={styles.textStatusTit}>Status : </Text>
                            <Text style={styles.textStatusVal}>{item.video_status}</Text>
                        </View>

                    </View>
                </View>
                <TouchableOpacity style={styles.item}
                    onPress={() => {
                        Actions.play({ videoURL: item.video });
                    }}>
                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        style={styles.image}
                        source={{ uri: item.thumbnail + `?w=${screenWidth}` }} />
                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={require('../res/ic_play.png')}
                        style={styles.playImageStyle}
                    />
                </TouchableOpacity>
                {item.video_comments
                    ? (
                        <View style={{ flexDirection: 'column', padding: 10, marginLeft: 3, marginRight: 3, marginTop: -8 }}>
                            <Text style={styles.textStatusTit}>Comments : </Text>
                            <Text style={styles.textComments}>{item.video_comments}vnsjvnwovns;vnsdklv sd v;ls vsl vsl vls vs;dv sl vsdlv sldv sdl vsdlv sdlv sdlv sdlv sdlv sdlv sdlmv slv </Text>
                        </View>
                    )
                    : (
                        <View>
                        </View>
                    )}
            </View>
        );
    };

    renderVideoListOrNot() {
        if (this.state.resMyChalObj !== null && this.state.resMyChalObj.length > 0) {
            return (
                <FlatList
                    extraData={this.state}
                    contentContainerStyle={{ paddingBottom: hp('20%') }}
                    data={this.state.resMyChalObj}
                    // keyExtractor={item => item.feed_id}
                    renderItem={(this.renderItemNew)} />
            );
        }
        else {
            if (this.state.challengeDetails.is_expired === 1) {
                return (
                    <View style={{ width: wp('100%'), height: hp('90%'), justifyContent: 'center', }}>
                        <Text style={styles.textInputStyleNodata}> This challenge is expired and you haven't uploaded any videos in this challenge !</Text>
                    </View>
                );
            } else {
                return (
                    <View style={{ width: wp('100%'), height: hp('90%'), justifyContent: 'center', }}>
                        <Text style={styles.textInputStyleNodata}> Start uploading video's by tapping Upload Video at the bottom !</Text>
                    </View>
                );
            }
        }
    }

    renderTabData() {
        if (this.state.loading === false) {
            if (this.state.isTabClicked === 1) {
                return (
                    <View style={styles.mainView}>
                        <View style={{ flexDirection: 'column', justifyContent: 'center' }}>

                            <TouchableOpacity
                                activeOpacity={0.8}
                                onPress={() => {
                                    if (this.state.challengeDetails.chlng_video) {
                                        LogUtils.firebaseEventLog('video', {
                                            p_id: this.state.challengeDetails.c_id,
                                            p_category: 'Challenges',
                                            p_name: `Start - ${this.state.challengeDetails.title} Video`,
                                        });

                                        if (Platform.OS === 'android') {
                                            Actions.newvideoplay({ videoURL: this.state.challengeDetails.chlng_video, from: 'challenges', label: `End - ${this.state.challengeDetails.title} Video` });
                                            // Actions.customPlayer({ videoURL: this.state.challengeDetails.chlng_video, from: 'plandetails', label: `End - ${this.state.challengeDetails.title} Video` });
                                        } else {
                                            Actions.afPlayer({ videoURL: this.state.challengeDetails.chlng_video, from: 'plandetails', label: `End - ${this.state.challengeDetails.title} Video` });
                                        }
                                    }
                                }}>
                                {this.state.challengeDetails.image
                                    ? (
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            style={styles.challengeimage}
                                            source={{ uri: this.state.challengeDetails.image }} />
                                    )
                                    : (
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_noimage.png')}
                                            style={styles.challengeimage}
                                        />
                                    )
                                }
                                {this.state.challengeDetails.chlng_video
                                    ?
                                    (
                                        <View style={styles.playContainerProgram}>
                                            <TouchableOpacity
                                                style={{ flexDirection: 'row' }}
                                                activeOpacity={0.8}
                                                onPress={() => {

                                                    LogUtils.firebaseEventLog('video', {
                                                        p_id: this.state.challengeDetails.c_id,
                                                        p_category: 'Challenges',
                                                        p_name: `Start - ${this.state.challengeDetails.title} Video`,
                                                    });

                                                    if (Platform.OS === 'android') {
                                                        Actions.customPlayer({ videoURL: this.state.challengeDetails.chlng_video, from: 'plandetails', label: `End - ${this.state.challengeDetails.title} Video` });
                                                    } else {
                                                        Actions.afPlayer({ videoURL: this.state.challengeDetails.chlng_video, from: 'plandetails', label: `End - ${this.state.challengeDetails.title} Video` });
                                                    }
                                                }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_play_purple.png')}
                                                    style={styles.playIcon} />
                                            </TouchableOpacity>
                                        </View>
                                    )
                                    :
                                    (
                                        <View></View>
                                    )
                                }
                            </TouchableOpacity>

                            <View style={{ margin: 20 }}>
                                <Text style={styles.textWorkName}>{`${this.state.challengeDetails.title}`}</Text>

                                <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                    <Text style={styles.textDate}>{`${this.state.challengeDetails.from_date}`}</Text>
                                    <Image
                                        progressiveRenderingEnabled={true}
                                        resizeMethod="resize"
                                        source={require('../res/ic_arr_right.png')}
                                        style={{
                                            width: 14,
                                            alignSelf: 'center',
                                            marginRight: 10,
                                            marginLeft: 10,
                                            tintColor: '#6d819c',
                                            height: 12,
                                        }} />
                                    <Text style={styles.textDate}>{`${this.state.challengeDetails.to_date}`}</Text>
                                </View>

                                <Text style={styles.textTitle}>Description  </Text>
                                <Text style={styles.textDesc}>{`${this.state.challengeDetails.description}`}</Text>

                                {this.state.challengeDetails.rewards
                                    ?
                                    (
                                        <View>
                                            <Text style={styles.textTitle}>Rewards </Text>
                                            {this.state.challengeDetails.rewards.split('|').map((tipDesc, index) => (
                                                <View style={styles.viewListAllInner}>
                                                    <View style={styles.viewBlackDot}></View>
                                                    <Text style={styles.textDesc}>{tipDesc}</Text>
                                                </View>
                                            ))}
                                        </View>

                                    )
                                    :
                                    (
                                        <View>
                                        </View>
                                    )
                                }

                                {this.state.challengeDetails.remarks
                                    ?
                                    (
                                        <View>
                                            <Text style={styles.textTitle}>Rules </Text>
                                            {this.state.challengeDetails.remarks.split('|').map((tipDesc, index) => (
                                                <View style={styles.viewListAllInner}>
                                                    <View style={styles.viewBlackDot}></View>
                                                    <Text style={styles.textDesc}>{tipDesc}</Text>
                                                </View>
                                            ))}
                                        </View>

                                    )
                                    :
                                    (
                                        <View>
                                        </View>
                                    )
                                }

                                {/* <Text style={styles.textDesc}>{`${this.state.challengeDetails.remarks}`}</Text> */}
                            </View>
                        </View>
                    </View>
                );
            }
            else {
                return (
                    <View style={styles.mainView}>
                        <View style={{
                            paddingBottom: 5, flex: 1,
                            flexDirection: 'column',
                        }}>
                            {this.renderVideoListOrNot()}
                        </View>
                    </View>
                );
            }
        }
    }

    render() {

        return (
            <ImageBackground source={require('../res/app_bg.png')} style={styles.mainContainer}>
                <ProgressBar loading={this.state.isProgress} progress={this.state.progress} />
                <View style={styles.containerStyle}>
                    <Loader loading={this.state.loading} />
                    <View style={{
                        flexDirection: 'row',
                        margin: 20,
                    }}>
                        <View style={{ position: 'absolute', zIndex: 111, }}>
                            <TouchableOpacity
                                style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                                onPress={() => this.onBackPressed()}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_back.png')}
                                    style={styles.backImageStyle}
                                />
                            </TouchableOpacity>
                        </View>

                        <Text style={styles.textHeadTitle}>Challenge Details</Text>
                    </View>

                    {/* {this.renderTabBar()} */}
                    <ScrollView>
                        {this.renderTabData()}
                    </ScrollView>
                    {/* {this.renderBottom()} */}
                </View>

                <CustomDialog
                    visible={this.state.isAlert}
                    title={this.state.alertTitle}
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    no=''
                    yes='Ok' />
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    linearGradient: {
        width: '90%',
        height: 50,
        borderRadius: 25,
        justifyContent: 'center',
        alignSelf: 'center',
    },
    mainContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
    },

    textIntro: {
        color: '#8c52ff',
        fontFamily: 'Rubik-Medium',
        alignSelf: "center",
        fontSize: 12,
        fontWeight: '500',
        lineHeight: 18,
    },
    containerStyle: {
        width: '100%',
        height: '100%',
        flex: 1,
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'center',
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    image: {
        // width: wp('90%'),
        // height: hp('26%'),
        // borderRadius: 10,
        // alignSelf: 'center',

        width: wp('90%'),
        height: undefined,
        justifyContent: 'center',
        alignSelf: 'center',
        borderRadius: 10,
        aspectRatio: 1 / 1.8,
    },
    challengeimage: {
        // width: '95%',
        // height: 220,
        // borderRadius: 10,
        // borderColor: 'black',
        // alignSelf: 'center',

        width: wp('95%'),
        height: undefined,
        justifyContent: 'center',
        alignSelf: 'center',
        borderRadius: 10,
        aspectRatio: 12 / 7,
    },
    mainView: {
        flex: 1,
        flexDirection: 'column',
    },
    header: {
        width: wp('100%'),
        height: 60,
        justifyContent: 'flex-start',
        flexDirection: 'row',
    },
    headerInner: {
        width: wp('50%'),
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
    },
    detailsHeader: {
        width: wp('100%'),
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
    },
    detailsheaderSel: {
        width: wp('100%'),
        height: 3,
        marginTop: 10,
    },
    headerSel: {
        width: wp('50%'),
        height: 3,
        marginTop: 10,
    },
    playImageStyle: {
        width: 30,
        height: 30,
        alignSelf: 'center',
        position: 'absolute',
    },
    postPlayImageStyle: {
        width: 60,
        height: 60,
        justifyContent: 'center',

    },
    textWorkName: {
        fontSize: 16,
        width: wp('85%'),
        fontWeight: '500',
        paddingRight: 10,
        fontFamily: 'Rubik-Medium',
        color: '#282c37',
    },
    textDate: {
        fontSize: 11,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        lineHeight: 18,
    },
    textTitle: {
        width: wp('88%'),
        fontSize: 14,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        color: '#282c37',
        lineHeight: 18,
        marginTop: 15,
    },
    textDesc: {
        // width: wp('88%'),
        fontSize: 12,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        lineHeight: 18,
        marginTop: 2,
    },
    bottom: {
        width: '90%',
        justifyContent: 'center',
        position: 'absolute',
        bottom: 0,
        alignSelf: 'center',
        flexDirection: 'column',
        marginBottom: 10,
    },
    buttonTuch:
    {
        backgroundColor: 'transparent',
    },
    buttonText: {
        fontSize: 14,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    mainContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
        backgroundColor: '#fff',
    },
    textInputStyleNodata: {
        width: wp('100%'),
        fontSize: 15,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        padding: 30,
        marginTop: 15,
        marginBottom: 15,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
        justifyContent: 'center',
    },
    mediaPlayer: {
        width: '100%',
        height: 215,
        position: 'relative',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        borderWidth: .1,
        borderRadius: 10,
        backgroundColor: 'black',
        marginTop: '15%',
    },
    playButton: {
        position: 'absolute',
        alignSelf: 'center',
        bottom: 70,

    },
    textPlan: {
        color: '#6D819C',
        fontFamily: 'Rubik-Medium',
        alignSelf: "center",
        fontSize: 12,
        fontWeight: '500',
        lineHeight: 18,
    },
    item: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        marginTop: 10,// approximate a square
    },
    viewBgVideo: {
        padding: 10,
        color: '#d6d9e0',
        backgroundColor: 'white',
        marginBottom: 10,
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    viewbgProfilePic: {
        flexDirection: 'row',
    },
    avatar: {
        width: 40,
        height: 40,
        aspectRatio: 1,
        backgroundColor: "#D8D8D8",
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: "#979797",
        borderRadius: 20,
        resizeMode: "cover",
        marginRight: 5,
    },
    column: {
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignSelf: 'center',
        marginLeft: 10,
    },
    textStyle: {
        color: '#2d3142',
        fontFamily: 'Rubik-Medium',
        fontSize: RFValue(14, screenHeight),
        fontWeight: '500',
        letterSpacing: 0.23,
    },
    textStatusTit: {
        fontSize: 12,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        lineHeight: 18,
    },
    textStatusVal: {
        fontSize: 12,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        color: '#8c52ff',
        lineHeight: 18,
    },
    textComments: {
        fontSize: 12,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        color: '#282c37',
    },
    playContainerProgram: {
        flexDirection: 'row',
        alignContent: 'center',
        position: 'absolute',
        right: 20,
        bottom: 20,
        alignSelf: 'center',
        alignContent: 'center',
        justifyContent: 'center',
    },
    playIcon: {
        width: 35,
        height: 35,
        alignSelf: 'center',
    },
    viewBlackDot: {
        width: 8,
        height: 8,
        backgroundColor: '#282c37',
        alignSelf: 'flex-start',
        marginTop: 5,
        marginRight: 5,
        borderRadius: 4,
    },
    viewListAllInner: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignSelf: 'flex-start',
        alignItems: 'center',
        marginTop: 2,
        marginBottom: 2,
    },
});

const mapStateToProps = state => {
    return {};
};

export default withNavigationFocus(
    connect(
        mapStateToProps,
        {},
    )(ChallengeDetails),
);
