import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    BackHandler,
    ScrollView,
    ImageBackground,
    FlatList,
    TextInput,
    Platform,
    Modal,
    Animated,
    Easing,
    marqu
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import { NoInternet, HomeDialog, Loader, IAPCustomDialog } from './common';
import LinearGradient from 'react-native-linear-gradient';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import NetInfo from "@react-native-community/netinfo";
import { BASE_URL, SWR, RZP_KEY } from '../actions/types';
import RazorpayCheckout from 'react-native-razorpay';
import DeviceInfo from 'react-native-device-info';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Dialog, {
    DialogContent,
} from 'react-native-popup-dialog';
import LogUtils from '../utils/LogUtils.js';
import RNIap from 'react-native-iap';
let purchaseUpdateSubscription;
let purchaseErrorSubscription;
import { allowFunction } from '../utils/ScreenshotUtils.js';
import Carousel, { Pagination } from 'react-native-snap-carousel';
const sliderWidth = wp('100%');
const itemWidth = wp('40%');
import ImageSlider from 'react-native-image-slider';
import { SliderBox } from "react-native-image-slider-box";
import { WebView } from 'react-native-webview';
import Video from 'react-native-video';
import { plansCheckCountAction } from '../actions/traineeActions'

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();

    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

function is3Months(array) {
    let three = false;
    array.map((item, i) => {
        if (item.is_default === 1) {
            three = true;
        }
    });
    return three;
}

const itemSkus = Platform.select({
    ios: [
        'P4DF01M01999',
        'P4DF03M03899',
        'P4DF06M06000',
        'P4OD01M01299',
        'P4OD03M02699',
        'P4OD06M04799',
        'P4OF01M01299',
        'P4OF03M02699',
        'P4OF06M04799',
    ],
    android: []
});
let selectedItem = {};
let imagesArray = [];

class Trainee_BuyPlanDetailsNew extends Component {

    constructor(props) {
        super(props);
        this.spinValue = new Animated.Value(0)
        this.state = {
            loading: false,
            isAlert: false,
            alertTitle: '',
            alertMsg: '',
            isInternet: false,
            arrAllSubPlans: [],
            noDataMsg: '',
            selSubObj: {},
            subCreateObj: {},
            payRequest: {},
            payResponse: {},
            applyCouponResponse: {},
            modalVisible: false,
            isSuccess: false,
            isFail: false,
            couponCode: '',
            isApplyCoupon: false,
            selPayMethodPop: false,
            arrPaymentMethods: [],
            selPayMethodObj: {},
            is_ios: 0,
            iapSubscriptions: [],
            isIapAlert: false,
            isIapAlertTitle: '',
            isIapAlertMsg: '',
            isIAPOptional: '',
            isIapBtnYes: '',
            isIapBtnNo: '',
            isProceedClicked: false,
            isButtonEnabled: true,
        };
        this.getAllPaymentMethods = this.getAllPaymentMethods.bind(this);
        // this.getAllSubPlanDetails = this.getAllSubPlanDetails.bind(this);
    }

    async componentDidMount() {
        try {
            this.spin();
            allowFunction();
            imagesArray = [];
            if (Platform.OS === 'ios') {
                this.setState({ is_ios: 1 });

            }
            NetInfo.fetch().then(state => {
                if (state.isConnected) {
                    // this.getAllSubPlanDetails();
                    this.getAllPaymentMethods(this.state.is_ios);
                }
                else {
                    this.setState({ isInternet: true });
                }
            });
            this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
                this.onBackPressed();
                return true;
            });
            LogUtils.infoLog1('this.props.selPlan', this.props.selPlan);
            LogUtils.infoLog1('this.props.obj', this.props.obj);

            if (this.props.selPlan.planprice_list.length) {
                // LogUtils.infoLog1(is3Months(this.props.selPlan.planprice_list));
                if (is3Months(this.props.selPlan.planprice_list)) {
                    this.props.selPlan.planprice_list.map((item, i) => {
                        if (item.is_default === 1) {
                            item.check = true;
                            this.setState({ selSubObj: item });
                        }
                        else {
                            item.check = false;
                        }

                    });
                }
                else {
                    this.props.selPlan.planprice_list.map((item, i) => {
                        if (i === 0) {
                            item.check = true;
                            this.setState({ selSubObj: item });
                        }
                        else {
                            item.check = false;
                        }

                    });
                }


                this.setState({ loading: false, arrAllSubPlans: this.props.selPlan.planprice_list });

                const interval = setInterval(() => {
                    if (this._carousel2) {
                        setTimeout(() => {
                            if (this.props.selPlan.planprice_list.length > 1) {
                                this._carousel2.snapToItem(1, true)
                            }
                            else {
                                this._carousel2.snapToItem(0, true)
                            }
                        }, 100)

                        clearInterval(interval);
                    }
                }, 300);

            }
            else {
                this.setState({ loading: false, noDataMsg: 'No data available at this moment' });
            }

            RNIap.initConnection().catch(() => {
                LogUtils.infoLog1('Error connecting to store');
            }).then(async () => {
                console.log(itemSkus,'---itemSkus');
                RNIap.getSubscriptions(itemSkus).catch(() => {
                    LogUtils.infoLog1('Error finding subscriptions');
                }).then((res) => {
                    this.setState({ iapSubscriptions: res });
                    LogUtils.infoLog1('this.state.Subscriptions', this.state.iapSubscriptions)
                })
            })

            purchaseUpdateSubscription = RNIap.purchaseUpdatedListener(async (purchase) => {

                try {
                    // LogUtils.infoLog1("Purchase Entered");
                    // console.log('purchase success', purchase);
                    // if (this.state.isProceedClicked) {
                    //     

                    //     if (purchase) {
                    //         this.setState({isProceedClicked : false});
                    //         const receipt = purchase.transactionReceipt;
                    //         console.log('purchase success', purchase);
                    //         // console.log('Success receipt', receipt);
                    //         this.finishIOSTransaction(purchase.transactionId);
                    //         this.saveIAPSubscription(purchase);
                    //     }

                    // }


                } catch (error) {
                    LogUtils.infoLog1('purchaseUpdatedListener Error', error);
                }

            });

            purchaseErrorSubscription = RNIap.purchaseErrorListener((PurchaseError) => {
                try {
                    this.setState({ isButtonEnabled: true, loading: false });
                    // console.log('purchaseErrorListener', JSON.stringify(PurchaseError));
                    this.saveAppflyerLog(0);
                    this.setState({ isIapAlert: true, isIapAlertMsg: PurchaseError.message, isIapAlertTitle: 'Alert', isIapBtnYes: 'Retry', isIapBtnNo: '', isIAPOptional: 'Ok' });


                    // this.setState({ loading: false, isAlert: true, alertMsg: PurchaseError.message, alertTitle: 'Alert' });
                } catch (error) {
                    LogUtils.infoLog1('purchaseErrorListener Error', JSON.stringify(error));
                }
            });

            // try {
            //     const products = await RNIap.getSubscriptions(itemSkus);
            //     this.setState({ iapSubscriptions: products });
            //     LogUtils.infoLog1('this.state.iapSubscriptions',this.state.iapSubscriptions)
            //   } catch(err) {
            //     console.warn(err); // standardized err.code and err.message available
            //   }

            this.props.obj.media_2.map((item) => {
                imagesArray.push(item.media_url);
            })
        } catch (error) {
            console.log(error);
        }

    }

    spin() {
        this.spinValue.setValue(0)
        Animated.timing(
            this.spinValue,
            {
                toValue: 1,
                duration: 4000,
                easing: Easing.linear
            }
        ).start(() => this.spin())
    }

    getSubscriptions = async () => {
        try {
            const products = await RNIap.getSubscriptions(itemSubs);
            LogUtils.infoLog1('Products', products);
            this.setState({ productList: products });
        } catch (err) {
            console.warn(err.code, err.message);
        }
    };


    componentWillUnmount() {
        this.backHandler.remove();

        if (purchaseUpdateSubscription) {
            purchaseUpdateSubscription.remove();
            purchaseUpdateSubscription = null;
        }
        if (purchaseErrorSubscription) {
            purchaseErrorSubscription.remove();
            purchaseErrorSubscription = null;
        }
        RNIap.endConnection();
    }


    onBackPressed() {
        if (this.state.modalVisible) {
            if (this.state.isSuccess) {
                this.setState({ isSuccess: false, modalVisible: false });
            }
            else if (this.state.isFail) {
                this.setState({ isFail: false });
            }
            else {
                this.setState({ modalVisible: false });
            }
        }
        else {
            Actions.pop();
        }
    }


    async saveAppflyerLog(success) {
        const eventValues = {
            af_content_id: this.props.selPlan.plan_id + '_' + this.state.selSubObj.pp_id,
            af_currency: "INR",
            af_revenue: '' + this.state.selSubObj.new_total_price
        };

        if (success === 1) {
            LogUtils.appsFlyerEventLog('purchase', eventValues);
            LogUtils.appsFlyerEventLog('subscriptionsuccess', eventValues);
        }
        else {

            LogUtils.appsFlyerEventLog('subscriptionfail', {
                desc: 'subscriptionfail',
            });
            LogUtils.appsFlyerEventLog('purchase_fail', {
                desc: 'purchasefail',
            });
        }
    }

    async getAllPaymentMethods(isIOS) {
        try {

            let token = await AsyncStorage.getItem('token');
            LogUtils.infoLog1('url ', `${BASE_URL}/trainee/paymenttypes?is_ios=${isIOS}`);
            fetch(
                `${BASE_URL}/trainee/paymenttypes?is_ios=${isIOS}`,
                {
                    method: 'GET',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${token}`,
                    },
                },
            )
                .then(processResponse)
                .then(res => {
                    const { statusCode, data } = res;
                    LogUtils.infoLog1('statusCode', statusCode);
                    LogUtils.infoLog1('arrPaymentMethods ', data);

                    if (statusCode >= 200 && statusCode <= 300) {
                        this.setState({ loading: false });
                        this.setState({ arrPaymentMethods: data.data });
                        // if (this.state.isApplyCoupon && Platform.OS === 'ios') {
                        //     this.setState({ selPayMethodPop: true });
                        // }
                    } else {
                        if (data.message === 'You are not authenticated!') {
                            //this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                        } else {
                            //this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                        }
                    }
                })
                .catch(function (error) {
                    //this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error), alertTitle: 'Alert' });
                });
        } catch (error) {
            console.log(error);
        }
    }
 
    async getAllSubPlanDetails() {
        try {
            this.setState({ loading: true });
            let token = await AsyncStorage.getItem('token');
            LogUtils.infoLog(JSON.stringify({
                plan_id: this.props.selPlan.plan_id,
            }));
            fetch(
                `${BASE_URL}/trainee/planbenifits`,
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${token}`,
                    },
                    body: JSON.stringify({
                        plan_id: this.props.selPlan.plan_id,
                    }),

                },
            )
                .then(processResponse)
                .then(res => {
                    const { statusCode, data } = res;
                    LogUtils.infoLog1('statusCode', statusCode);
                    LogUtils.infoLog1('data', data);

                    if (statusCode >= 200 && statusCode <= 300) {
                        if (data.data.length) {
                            LogUtils.infoLog1(is3Months(data.data));
                            if (is3Months(data.data)) {
                                data.data.map((item, i) => {
                                    if (item.month_duration === 3) {
                                        item.check = true;
                                        this.setState({ selSubObj: item });
                                    }
                                    else {
                                        item.check = false;
                                    }
                                    return data.data;
                                });
                            }
                            else {
                                data.data.map((item, i) => {
                                    if (i === 0) {
                                        item.check = true;
                                        this.setState({ selSubObj: item });
                                    }
                                    else {
                                        item.check = false;
                                    }
                                    return data.data;
                                });
                            }


                            this.setState({ loading: false, arrAllSubPlans: data.data });
                        }
                        else {
                            this.setState({ loading: false, noDataMsg: 'No data available at this moment' });
                        }

                    } else {
                        if (data.message === 'You are not authenticated!') {
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                        } else {
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                        }
                    }
                })
                .catch(function (error) {
                    this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error), alertTitle: 'Alert' });
                });
        } catch (error) {
            console.log(error);
        }
    }


    async getSubscriptionDetails() {
        try {
            this.setState({ loading: true });
            let token = await AsyncStorage.getItem('token');
            let data;
            if (this.state.isApplyCoupon) {
                data = JSON.stringify({
                    plan_id: this.props.selPlan.plan_id,
                    pp_id: this.state.selSubObj.pp_id,
                    amount: this.state.selSubObj.new_total_price,
                    plan_code: this.state.selSubObj.plan_code,
                    modify: 0,
                    c_id: this.state.applyCouponResponse.c_id,
                    c_disc: this.state.selSubObj.cpn_discount,
                    trial_pp_id: this.state.selSubObj.trial_pp_id,
                });
            }
            else {
                data = JSON.stringify({
                    plan_id: this.props.selPlan.plan_id,
                    pp_id: this.state.selSubObj.pp_id,
                    amount: this.state.selSubObj.new_total_price,
                    plan_code: this.state.selSubObj.plan_code,
                    modify: 0,
                    c_id: 0,
                    c_disc: 0,
                    trial_pp_id: this.state.selSubObj.trial_pp_id,
                });
            }

            // }
            LogUtils.infoLog1(token, data);
            fetch(
                `${BASE_URL}/trainee/create_subscription`,
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${token}`,
                    },
                    body: data,
                },
            )
                .then(processResponse)
                .then(res => {
                    const { statusCode, data } = res;
                    LogUtils.infoLog1('statusCode', statusCode);
                    LogUtils.infoLog1('data', data);

                    if (statusCode >= 200 && statusCode <= 300) {
                        this.setState({ loading: false, subCreateObj: data });
                        LogUtils.infoLog(data);

                        LogUtils.firebaseEventLog('click', {
                            p_id: this.props.selPlan.plan_id,
                            p_category: 'Plans',
                            p_name: `${this.props.selPlan.name} - ${this.state.selSubObj.duration} - Payment Started`,
                        });

                        var options = {
                            key: RZP_KEY,
                            subscription_id: data.sub_code,
                            // subscription_card_change: 1,
                            name: 'Transform',
                            description: this.props.selPlan.title,
                            // notes: {
                            //   note_key_1: 'Tea. Earl Grey. Hot',
                            //   note_key_2: 'Make it so.'
                            // },
                            // prefill: {
                            //     email: 'venkatesh@appoids.in',
                            //     contact: '8861512641',
                            //     name: 'Venkatesh A'
                            // },
                            theme: { color: '#8c52ff' }
                        };
                        if (Platform.OS === 'ios') {
                            this.setState({ selPayMethodPop: false });
                        }

                        this.setState({ payRequest: options });
                        LogUtils.infoLog1('Payment Request : ', options);
                        setTimeout(() => {
                            RazorpayCheckout.open(options).then((data) => {
                                LogUtils.firebaseEventLog('click', {
                                    p_id: this.props.selPlan.plan_id,
                                    p_category: 'Plans',
                                    p_name: `${this.props.selPlan.name} - ${this.state.selSubObj.duration} - Payment Success`,
                                });
                                // handle success
                                LogUtils.infoLog1('Payment Response : ', data);
                                this.setState({ payResponse: data });
                                // alert(`Success: ${data.razorpay_payment_id}`);
                                this.saveAppflyerLog(1);
                                this.savePlanSubscription();

                            }).catch((error) => {
                                // handle failure
                                this.saveAppflyerLog(0);
                                LogUtils.infoLog1('Payment Fail : ', error);
                                LogUtils.firebaseEventLog('click', {
                                    p_id: this.props.selPlan.plan_id,
                                    p_category: 'Plans',
                                    p_name: `${this.props.selPlan.name} - ${this.state.selSubObj.duration} - Payment fail`,
                                });
                                this.setState({ isAlert: true, alertMsg: 'Your payment method was declined.Please try again later.', alertTitle: 'Declined' });
                            });
                        }, 100);

                    } else {
                        if (data.message === 'You are not authenticated!') {
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                        } else {
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                        }
                    }
                })
                .catch(function (error) {
                    this.setState({ loading: false, isAlert: true, alertMsg: SWR, alertTitle: 'Alert' });
                });
        } catch (error) {
            console.log(error);
        }
    }


    async getOrderDetails() {
        try {
            this.setState({ loading: true });
            let token = await AsyncStorage.getItem('token');
            let data;
            if (this.state.isApplyCoupon) {
                // data = JSON.stringify({
                //     plan_id: this.props.selPlan.plan_id,
                //     pp_id: this.state.selSubObj.pp_id,
                //     amount: this.state.selSubObj.new_total_price,
                //     c_id: this.state.applyCouponResponse.c_id,
                //     c_disc: this.state.selSubObj.cpn_discount,
                // });

                data = JSON.stringify({
                    plan_id: this.state.selSubObj.nr_plan_id,
                    pp_id: this.state.selSubObj.nr_pp_id,
                    amount: this.state.selSubObj.nr_total_price,
                    c_id: this.state.applyCouponResponse.c_id,
                    c_disc: this.state.selSubObj.cpn_discount,
                });
            }
            else {
                // data = JSON.stringify({
                //     plan_id: this.props.selPlan.plan_id,
                //     pp_id: this.state.selSubObj.pp_id,
                //     amount: this.state.selSubObj.new_total_price,
                //     c_id: 0,
                //     c_disc: 0,
                // });

                data = JSON.stringify({
                    plan_id: this.state.selSubObj.nr_plan_id,
                    pp_id: this.state.selSubObj.nr_pp_id,
                    amount: this.state.selSubObj.nr_total_price,
                    c_id: 0,
                    c_disc: 0,
                });
            }

            LogUtils.infoLog1(token, data);
            fetch(
                `${BASE_URL}/trainee/create_order`,
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${token}`,
                    },
                    body: data,
                },
            )
                .then(processResponse)
                .then(res => {
                    const { statusCode, data } = res;
                    LogUtils.infoLog1('statusCode', statusCode);
                    LogUtils.infoLog1('data', data);

                    if (statusCode >= 200 && statusCode <= 300) {
                        this.setState({ loading: false, subCreateObj: data });
                        LogUtils.infoLog(data);

                        LogUtils.firebaseEventLog('click', {
                            p_id: this.props.selPlan.plan_id,
                            p_category: 'Plans',
                            p_name: `${this.props.selPlan.name} - ${this.state.selSubObj.duration} - Payment Started`,
                        });

                        var options = {
                            description: this.props.selPlan.title,
                            // image: 'http://fitness.whamzone.com/assets/images/logo.png',
                            // currency: 'INR',
                            key: RZP_KEY,
                            // amount: '2999',
                            name: 'Transform',
                            order_id: data.ord_code,//Replace this with an order_id created using Orders API. Learn more at https://razorpay.com/docs/api/orders.
                            // prefill: {
                            //     email: 'venkatesh@appoids.in',
                            //     contact: '861512641',
                            //     name: 'Venkatesh Annavaram'
                            // },
                            theme: { color: '#8c52ff' }
                        }

                        if (Platform.OS === 'ios') {
                            this.setState({ selPayMethodPop: false });
                        }

                        this.setState({ payRequest: options });
                        LogUtils.infoLog1('Payment Request : ', options);
                        setTimeout(() => {
                            RazorpayCheckout.open(options).then((data) => {
                                LogUtils.firebaseEventLog('click', {
                                    p_id: this.props.selPlan.plan_id,
                                    p_category: 'Plans',
                                    p_name: `${this.props.selPlan.name} - ${this.state.selSubObj.duration} - Payment Success`,
                                });
                                // handle success
                                LogUtils.infoLog1('Payment Response : ', data);
                                this.setState({ payResponse: data });
                                // alert(`Success: ${data.razorpay_payment_id}`);
                                this.saveAppflyerLog(1);
                                this.savePlanSubscription();

                            }).catch((error) => {
                                // handle failure
                                this.saveAppflyerLog(1);
                                LogUtils.infoLog1('Payment Fail : ', error);
                                LogUtils.firebaseEventLog('click', {
                                    p_id: this.props.selPlan.plan_id,
                                    p_category: 'Plans',
                                    p_name: `${this.props.selPlan.name} - ${this.state.selSubObj.duration} - Payment Fail`,
                                });
                                this.setState({ isAlert: true, alertMsg: 'Your payment method was declined.Please try again later.', alertTitle: 'Declined!' });
                            });
                        }, 100);

                    } else {
                        if (data.message === 'You are not authenticated!') {
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                        } else {
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                        }
                    }
                })
                .catch(function (error) {
                    this.setState({ loading: false, isAlert: true, alertMsg: SWR, alertTitle: 'Alert' });
                });
        } catch (error) {
            console.log(error);
        }
    }

    async savePlanSubscription() {
        try {
            if (Platform.OS === 'android') {
                this.setState({ loading: true });
            }
            let token = await AsyncStorage.getItem('token');
            let data;
            if (this.state.selPayMethodObj.can_subscribe === 1) {
                if (this.state.isApplyCoupon) {
                    data = JSON.stringify({
                        plan_id: this.props.selPlan.plan_id,
                        pp_id: this.state.selSubObj.pp_id,
                        amount: this.state.selSubObj.new_total_price,
                        rzp_s_id: this.state.subCreateObj.sub_id,
                        payment_id: this.state.payResponse.razorpay_payment_id,
                        subscription_id: this.state.payResponse.razorpay_subscription_id,
                        signature: this.state.payResponse.razorpay_signature,
                        request: this.state.payRequest,
                        uc_id: this.state.subCreateObj.uc_id,
                        // rzp_o_id: '',
                        // order_id: '',
                        modify: 0,
                        up_id: 0,
                        credits: 0,
                        trial_pp_id: this.state.selSubObj.trial_pp_id,
                    });
                }
                else {
                    data = JSON.stringify({
                        plan_id: this.props.selPlan.plan_id,
                        pp_id: this.state.selSubObj.pp_id,
                        amount: this.state.selSubObj.new_total_price,
                        rzp_s_id: this.state.subCreateObj.sub_id,
                        payment_id: this.state.payResponse.razorpay_payment_id,
                        subscription_id: this.state.payResponse.razorpay_subscription_id,
                        signature: this.state.payResponse.razorpay_signature,
                        request: this.state.payRequest,
                        uc_id: 0,
                        // rzp_o_id: '',
                        // order_id: '',
                        modify: 0,
                        up_id: 0,
                        credits: 0,
                        trial_pp_id: this.state.selSubObj.trial_pp_id,
                    });
                }

            }
            else {
                if (this.state.isApplyCoupon) {
                    data = JSON.stringify({
                        // plan_id: this.props.selPlan.plan_id,
                        // pp_id: this.state.selSubObj.pp_id,
                        // amount: this.state.selSubObj.new_total_price,
                        plan_id: this.state.selSubObj.nr_plan_id,
                        pp_id: this.state.selSubObj.nr_pp_id,
                        amount: this.state.selSubObj.nr_total_price,
                        // rzp_s_id: '',
                        payment_id: this.state.payResponse.razorpay_payment_id,
                        // subscription_id: '',
                        signature: this.state.payResponse.razorpay_signature,
                        request: this.state.payRequest,
                        rzp_o_id: this.state.subCreateObj.ord_id,
                        order_id: this.state.subCreateObj.ord_code,
                        uc_id: this.state.subCreateObj.uc_id,
                        modify: 0,
                        up_id: 0,
                        credits: 0,
                    });
                }
                else {
                    data = JSON.stringify({
                        // plan_id: this.props.selPlan.plan_id,
                        // pp_id: this.state.selSubObj.pp_id,
                        // amount: this.state.selSubObj.new_total_price,
                        plan_id: this.state.selSubObj.nr_plan_id,
                        pp_id: this.state.selSubObj.nr_pp_id,
                        amount: this.state.selSubObj.nr_total_price,
                        // rzp_s_id: '',
                        payment_id: this.state.payResponse.razorpay_payment_id,
                        // subscription_id: '',
                        signature: this.state.payResponse.razorpay_signature,
                        request: this.state.payRequest,
                        rzp_o_id: this.state.subCreateObj.ord_id,
                        order_id: this.state.subCreateObj.ord_code,
                        uc_id: 0,
                        modify: 0,
                        up_id: 0,
                        credits: 0,
                    });
                }

            }
            LogUtils.infoLog1(token, data);

            fetch(
                `${BASE_URL}/trainee/save_subscription`,
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${token}`,
                    },
                    body: data,
                },
            )
                .then(processResponse)
                .then(res => {
                    const { statusCode, data } = res;
                    LogUtils.infoLog1('statusCode', statusCode);
                    LogUtils.infoLog1('data', data);

                    if (statusCode >= 200 && statusCode <= 300) {
                        // this.setState({ loading: false, isSuccess: true, sucMsg: data.message, titMsg: data.title });
                        this.setState({ loading: false });
                        LogUtils.firebaseEventLog('click', {
                            p_id: this.props.selPlan.plan_id,
                            p_category: 'Plans',
                            p_name: `${this.props.selPlan.name} - ${this.state.selSubObj.duration} - Subscribed Successfully`,
                        });
                        Actions.traBuySuccess({ from: 'payment', sucResponse: data });
                        this.setState({ selPayMethodPop: false });

                    } else {
                        if (data.message === 'You are not authenticated!') {
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                        } else {
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                        }
                    }
                })
                .catch(function (error) {
                    this.setState({ loading: false, isAlert: true, alertMsg: SWR, alertTitle: 'Alert' });
                });
        } catch (error) {
            console.log(error);
        }
    }

    async finishIOSTransaction(transactionId) {
        if (Platform.OS === 'ios') {
            await RNIap.finishTransactionIOS(transactionId);
        }
    }


    async saveIAPSubscription(purchase) {
        try {
            if (Platform.OS === 'android') {
                this.setState({ loading: true });
            }
            let token = await AsyncStorage.getItem('token');
            let data;
            data = JSON.stringify({
                plan_id: this.props.selPlan.plan_id,
                pp_id: this.state.selSubObj.pp_id,
                amount: this.state.selSubObj.new_total_price,
                iap_tran: purchase,
            });

            // LogUtils.infoLog1(token, data);

            fetch(
                `${BASE_URL}/trainee/save_iap_subscription`,
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${token}`,
                    },
                    body: data,
                },
            )
                .then(processResponse)
                .then(res => {
                    const { statusCode, data } = res;
                    LogUtils.infoLog1('statusCode', statusCode);
                    LogUtils.infoLog1('data', data);

                    if (statusCode >= 200 && statusCode <= 300) {
                        // this.setState({ loading: false, isSuccess: true, sucMsg: data.message, titMsg: data.title });
                        this.setState({ loading: false });
                        // if (data.transactionId) {
                        //     this.finishIOSTransaction(data.transactionId);
                        // }

                        LogUtils.firebaseEventLog('click', {
                            p_id: this.props.selPlan.plan_id,
                            p_category: 'Plans',
                            p_name: `${this.props.selPlan.name} - ${this.state.selSubObj.duration} - Subscribed Successfully`,
                        });
                        Actions.traBuySuccess({ from: 'payment', sucResponse: data });
                        this.setState({ selPayMethodPop: false });

                    } else {
                        this.setState({ selPayMethodPop: false });
                        if (data.message === 'You are not authenticated!') {
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                        } else {
                            this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                        }
                    }
                })
                .catch(function (error) {
                    this.setState({ loading: false, isAlert: true, alertMsg: SWR, alertTitle: 'Alert' });
                });
        } catch (error) {
            console.log(error);
        }
    }

    async callApplyCouponsService() {
        try {
            if (this.state.couponCode) {
                this.setState({ loading: true });
                let token = await AsyncStorage.getItem('token');
                let data;
                data = JSON.stringify({
                    plan_id: this.props.selPlan.plan_id,
                    // amount: this.state.selSubObj.new_total_price,
                    coupon: this.state.couponCode,
                });
                LogUtils.infoLog1(token, data);
                fetch(
                    `${BASE_URL}/trainee/apply_coupon2`,
                    {
                        method: 'POST',
                        headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                            Authorization: `Bearer ${token}`,
                        },
                        body: data,
                    },
                )
                    .then(processResponse)
                    .then(res => {
                        const { statusCode, data } = res;
                        LogUtils.infoLog1('statusCode', statusCode);
                        LogUtils.infoLog1('data', data);

                        if (statusCode >= 200 && statusCode <= 300) {
                            this.setState({ loading: false, isSuccess: true, isApplyCoupon: true, applyCouponResponse: data });

                            this.isUpdateList();

                        } else {
                            if (data.message === 'You are not authenticated!') {
                                this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                            } else {
                                this.setState({ loading: false, isFail: true, applyCouponResponse: data });
                            }
                        }
                    })
                    .catch(function (error) {
                        this.setState({ loading: false, isAlert: true, alertMsg: SWR, alertTitle: 'Alert' });
                    });
            }
            else {
                this.setState({ isAlert: true, alertMsg: 'Please enter coupon code', alertTitle: 'Alert' });
            }
        } catch (error) {
            console.log(error);
        }

    }

    isUpdateList() {
        this.state.applyCouponResponse.pp_list.map((item, i) => {
            this.state.arrAllSubPlans.map((selItem, j) => {
                if (item.pp_id === selItem.pp_id) {
                    selItem.new_price_text = item.net_price_text;
                    selItem.offer_price_text = item.net_discount_text;
                    selItem.new_total_price = item.tot_price;
                    selItem.cpn_discount = item.cpn_discount * item.months;
                    selItem.price_text = item.price_text;

                    selItem.nr_plan_id = item.alt_plan_id;
                    selItem.nr_pp_id = item.alt_pp_id;
                    selItem.nr_total_price = item.alt_tot_price;

                    selItem.price_text_2 = item.price_text_2;
                    selItem.period_text = item.period_text;
                }
            });
        });

        this.state.arrAllSubPlans.map((item, i) => {
            if (item.check === true) {
                this.setState({ selSubObj: item });
            }
        });
    }




    // async callPaymentMethod() {

    //     var options = {
    //         key: 'rzp_test_3NfxiVvKdFdl3o',
    //         subscription_id: 'sub_Exm4WCuEZyTcAt',
    //         // subscription_card_change: 1,
    //         name: 'Transform',
    //         description: this.props.selPlan.title,
    //         // image: 'http://fitness.whamzone.com/assets/images/logo.png',
    //         // handler: function(response) {
    //         //   alert(response.razorpay_payment_id),
    //         //   alert(response.razorpay_subscription_id),
    //         //   alert(response.razorpay_signature);
    //         // },
    //         notes: {
    //             note_key_1: 'Tea. Earl Grey. Hot',
    //             note_key_2: 'Make it so.'
    //         },
    //         prefill: {
    //             email: 'venkatesh@appoids.in',
    //             contact: '8861512641',
    //             name: 'Venkatesh A'
    //         },
    //         theme: { color: '#8b63e6' }
    //     };

    //     RazorpayCheckout.open(options).then((data) => {
    //         // handle success
    //         LogUtils.infoLog1('Payment Response : ', data);
    //         // alert(`Success: ${data.razorpay_payment_id}`);
    //         this.savePlanSubscription();

    //     }).catch((error) => {
    //         // handle failure
    //         alert(`${error.description}`);
    //     });
    // }

    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
        this.setState({ isAlert: false, alertMsg: '', alertTitle: '' });
    }

    async onIapAccept() {
        try {
            // this.setState({isButtonEnabled : false});
            // RNIap.requestSubscription(selectedItem.ios_iap_product_id);
            this.setState({ loading: true });
            RNIap.requestSubscription(selectedItem.ios_iap_product_id).then((purchase) => {

                LogUtils.infoLog1('Request Sub res', purchase);
                this.setState({ loading: false });
                if (purchase) {

                    this.saveAppflyerLog(1);
                    this.finishIOSTransaction(purchase.transactionId);
                    this.saveIAPSubscription(purchase);
                }
            }).catch(error=>{
                console.log(" Error in onIapAccept requestSubscription ",error);
            });

            this.setState({ isIapAlert: false, isIapAlertMsg: '', isIapAlertTitle: '', isIapBtnYes: '', isIapBtnNo: '', isProceedClicked: true, isIAPOptional: '' });
        } catch (error) {
            console.log(error);
        }
    }

    async onIapDeny() {
        this.setState({ isIapAlert: false, isIapAlertMsg: '', isIapAlertTitle: '', isIapBtnYes: '', isIapBtnNo: '', isIAPOptional: '' });
    }

    async onRetry() {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isInternet: false });
                //this.getAllSubPlanDetails();
            }
            else {
                this.setState({ isInternet: true });
            }
        });
    }

    FlatListItemSeparator1 = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: 1,
                    backgroundColor: "transparent",
                }}
            />
        );
    }

    FlatListItemSeparator2 = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: 2,
                    backgroundColor: "transparent",
                }}
            />
        );
    }

    FlatListSeparatorPayMethods = () => {
        return (
            <View
                style={{
                    height: 12,
                    width: 1,
                    backgroundColor: "transparent",
                }}
            />
        );
    }

    press = (hey, index) => {
        try {
            if (this.state.arrAllSubPlans.length > 1) {
                this.state.arrAllSubPlans.map((item, i) => {
                    if (item.pp_id === hey.pp_id) {
                        item.check = true;
                        this.setState({ selSubObj: item });
                        // this.setState({ selSubObj: item, isApplyCoupon: false, applyCouponResponse: {} });
                    }
                    else {
                        item.check = false;
                    }
                });
                this._carousel2.snapToItem(index, true)
                // LogUtils.infoLog(this.state.applyCouponResponse);
                this.setState({ arrAllSubPlans: this.state.arrAllSubPlans });
            }
        } catch (error) {
            console.log(error)
        }
    }

    renderBenfitsInclude() {
        try {
            if (!isEmpty(this.state.selSubObj)) {

                let arrBenfitIncludes = this.props.selPlan.benefits_list.map((item1, i) => {
                    return <View key={i} style={styles.viewBenInclu}>
                        <View style={styles.viewWhiteDot}></View>
                        <View style={styles.viewBenIncluInner}>
                            {/* <Text style={styles.txt14}>{item1.title}</Text> */}
                            <Text style={styles.txtDesc}>{item1.benefit}</Text>
                        </View>
                    </View>
                });
                return (
                    <View>
                        {arrBenfitIncludes}
                    </View>
                );
            }
        } catch (error) {
            console.log(error);
        }
    }

    renderPriceWhite = (item) => {
        if (item.is_flat === 'y') {
            return (
                <View style={{ flexDirection: 'column', marginTop: 20, }}>
                    <Text style={styles.txtActAmtWhite}>Rs.{`${item.month_price}`} Only</Text>
                    <Text style={styles.txtOffAmtWhite}>@Rs.{`${item.new_price}`} Only</Text>
                </View>
            );
        } else if (item.is_flat === 'n') {
            return (
                <View style={{ flexDirection: 'column', marginTop: 20, }}>
                    <Text style={styles.txtActAmtWhite}>Rs.{`${item.month_price}`}  Only</Text>
                    <Text style={styles.txtOffAmtWhite}>@Rs.{`${item.new_price}`} Only</Text>
                </View>
            );
        }
        else {
            return (
                <View style={{ flexDirection: 'column', marginTop: 20, }}>
                    <Text style={styles.txtOffAmtWhite}>Rs.{`${item.month_price}`} Only</Text>
                </View>
            );
        }
    }

    renderPriceBlack = (item) => {
        if (item.is_flat === 'y') {

            return (
                <View style={{ flexDirection: 'column', marginTop: 20, }}>
                    <Text style={styles.txtActAmtBlack}>Rs.{`${item.month_price}`} Only</Text>
                    <Text style={styles.txtOffAmtBlack}>@Rs.{`${item.new_price}`} Only</Text>
                </View>
            );
        } else if (item.is_flat === 'n') {
            return (
                <View style={{ flexDirection: 'column', marginTop: 20, }}>
                    <Text style={styles.txtActAmtBlack}>Rs.{`${item.month_price}`} Only</Text>
                    <Text style={styles.txtOffAmtBlack}>@Rs.{`${item.new_price}`} Only</Text>
                </View>
            );
        }
        else {
            return (
                <View style={{ flexDirection: 'column', marginTop: 20, }}>
                    <Text style={styles.txtOffAmtBlack}>Rs.{`${item.month_price}`} Only</Text>
                </View>
            );
        }
    }

    renderAllSubPlansList() {
        if (Array.isArray(this.state.arrAllSubPlans) && this.state.arrAllSubPlans.length) {
            return (
                <FlatList
                    styles={{ alignItems: 'center', alignContent: 'center', }}
                    contentContainerStyle={{ justifyContent: 'center' }}
                    data={this.state.arrAllSubPlans}
                    keyExtractor={(item, index) => item.pp_id}
                    showsVerticalScrollIndicator={false}
                    ItemSeparatorComponent={this.FlatListItemSeparator1}
                    renderItem={({ item }) => {
                        return <View key={item.pp_id} >
                            {item.check
                                ? (
                                    <TouchableOpacity style={styles.bgSelect}
                                        onPress={() => {
                                            this.press(item);
                                        }}>
                                        <View style={{
                                            flexDirection: 'column',
                                            flex: 1,
                                            borderRadius: 15,
                                            backgroundColor: '#8c52ff',
                                            margin: 5,
                                            alignSelf: 'center',
                                        }}>
                                            {item.offer_price_text
                                                ? (
                                                    <View style={{
                                                        flexDirection: 'column',
                                                        width: '30%',
                                                        height: 30,
                                                        borderRadius: 8,
                                                        backgroundColor: '#7ba6e7',
                                                        alignSelf: 'center',
                                                        position: 'absolute',
                                                        right: 10,
                                                        top: -15,
                                                        justifyContent: 'center',
                                                    }}>
                                                        <Text numberOfLines={2} style={styles.txtOffPerBlack}>{`${item.offer_price_text}`}</Text>

                                                    </View>
                                                )
                                                :
                                                (
                                                    <View>
                                                    </View>
                                                )}


                                            <View style={{
                                                padding: 15,
                                                justifyContent: 'center',
                                            }}>
                                                <Text style={styles.txtActAmtWhite}>{`${item.price_text}`}</Text>
                                                <Text style={styles.txtBilledWhite}>{`${item.new_price_text}`}</Text>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                )
                                : (
                                    <TouchableOpacity style={styles.bgUnSelect}
                                        onPress={() => {
                                            this.press(item);
                                        }}>
                                        <View style={{
                                            padding: 15,
                                        }}>
                                            <Text style={styles.txtPriceBlack}>{`${item.price_text}`}</Text>
                                            <Text style={styles.txtBilledBlack}>{`${item.new_price_text}`}</Text>
                                        </View>

                                        {item.offer_price_text
                                            ? (
                                                <View style={{
                                                    flexDirection: 'column',
                                                    width: '30%',
                                                    height: 30,
                                                    borderRadius: 8,
                                                    backgroundColor: '#7ba6e7',
                                                    position: 'absolute',
                                                    right: 10,
                                                    top: -15,
                                                    justifyContent: 'center',
                                                }}>
                                                    <Text numberOfLines={2} style={styles.txtOffPerBlack}>{`${item.offer_price_text}`}</Text>

                                                </View>
                                            )
                                            :
                                            (
                                                <View>
                                                </View>
                                            )}


                                    </TouchableOpacity>
                                )}
                        </View>
                    }} />

            );
        }
        else {
            return (

                <View style={{ flexDirection: 'row', flex: 1, }}>
                    <Text style={styles.textNodata}>{this.state.noDataMsg}</Text>
                </View>
            );
        }

    }

    changeIndex = async (currentIndex) => {
        try {
            //console.log('index', currentIndex)
            let token = await AsyncStorage.getItem('token');
            this.state.arrAllSubPlans.map((item, index) => {
                if (index === currentIndex) {
                    item.check = true;
                    this.setState({ selSubObj: item });
                    let params = { "p_id": this.props.selPlan.plan_id, "pp_id": item.pp_id };
                    plansCheckCountAction(token, params);
                    // this.setState({ selSubObj: item, isApplyCoupon: false, applyCouponResponse: {} });
                }
                else {
                    item.check = false;
                }
            });
            // LogUtils.infoLog(this.state.applyCouponResponse);
            this.setState({ arrAllSubPlans: this.state.arrAllSubPlans });
            // this._carousel2.snapToItem(currentIndex + 1, true);
            // this.setState({ currentIndex });
            // this.setState({ viewIndex: currentIndex + 1 });
        } catch (error) {
            console.log(error);
        }
    }

    renderAllSubPlansList_New() {
        if (Array.isArray(this.state.arrAllSubPlans) && this.state.arrAllSubPlans.length) {
            return (
                <Carousel style={{ width: wp('90%'), }}
                    // currentIndex={1}
                    // currentScrollPosition={2}
                    // activeSlideOffset={1}
                    activeSlideAlignment={'center'}
                    // animatedTension={10}
                    // activeAnimationType={'timing'}
                    data={this.state.arrAllSubPlans}
                    renderItem={({ item, index }) => {
                        return <TouchableOpacity
                            activeOpacity={1}
                            key={item.pp_id}
                            style={{
                                // backgroundColor: '#4075cd',
                                flexDirection: 'column',
                                alignSelf: 'center',
                                // shadowColor: 'rgba(0, 0, 0, 0.08)',
                                // shadowOffset: { width: 5, height: 0 },
                                // shadowOpacity: 0.25,
                                // shadowRadius: 5,
                                // elevation: 2,
                                // padding: 5,
                            }}

                            onPress={() => { this.press(item, index); }}>
                            {item.check
                                ? (
                                    <View>
                                        <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.bgPlanSelect}>

                                            <View style={{
                                                padding: 10,
                                                width: wp('40%'),
                                                backgroundColor: 'transparent',
                                            }}>

                                                <Text style={styles.txtOfferWhite} numberOfLines={1}>{`${item.duration}`}</Text>

                                                <View style={{ flexDirection: 'row' }}>

                                                    {item.org_price_text
                                                        ? (
                                                            <Text style={[styles.txtActAmtWhite1, {
                                                                textDecorationLine: 'line-through',
                                                                textDecorationStyle: 'solid',
                                                            }]}>{`${item.org_price_text}`} yy</Text>
                                                        )
                                                        :
                                                        (
                                                            <Text style={styles.txtActAmtWhite1}></Text>
                                                        )}
                                                    <View style={{ flex: 1 }}></View>
                                                    {item.offer_price_text
                                                        ? (
                                                            <Text style={styles.txtCouponApllied}>{`${item.offer_price_text}`}</Text>
                                                        )
                                                        :
                                                        (
                                                            <Text style={[styles.txtCouponApllied, { backgroundColor: 'transparent', }]}></Text>
                                                        )}

                                                </View>

                                                {/* <Text style={{
                                                fontSize: 20,
                                                fontWeight: '500',
                                                color: '#ffffff',
                                                fontFamily: 'Rubik-Medium',
                                                letterSpacing: 0.2,
                                                textDecorationStyle: 'solid',
                                                marginTop: 3
                                            }}>{`${item.price_text}`}</Text> */}

                                                <View style={styles.nutritionInnerView}>
                                                    <Text style={styles.textNutTitle}>{`${item.price_text_2}`}</Text>
                                                    <Text style={styles.textNutDesc}>{item.period_text}</Text>
                                                </View>
                                                <Text style={styles.txtBilledWhite1}>{`${item.new_price_text}`}</Text>
                                                {/* {item.offer_price_text
                                                ? (
                                                    <Text style={styles.txtCouponApllied}>{`${item.offer_price_text}`}</Text>
                                                )
                                                :
                                                (
                                                    <Text style={[styles.txtCouponApllied, { backgroundColor: 'transparent', }]}></Text>
                                                )} */}
                                            </View>
                                            {Platform.OS === 'android'
                                                ? (
                                                    <View style={{
                                                        flexDirection: 'column',
                                                        borderRadius: 8,
                                                        position: 'absolute',
                                                        right: 0,
                                                        top: -30,
                                                        justifyContent: 'center',
                                                        zIndex: 1111,
                                                        overflow: 'visible'
                                                    }}>
                                                        {item.img
                                                            ? (
                                                                <View >
                                                                    <Image
                                                                        progressiveRenderingEnabled={true}
                                                                        resizeMethod="resize"
                                                                        source={{ uri: item.img }}
                                                                        style={styles.imgOffer}
                                                                    />
                                                                </View>
                                                            )
                                                            :
                                                            (
                                                                <View>
                                                                </View>
                                                            )}
                                                    </View>
                                                )
                                                :
                                                (
                                                    <View>
                                                    </View>
                                                )}

                                        </LinearGradient>
                                        {Platform.OS === 'ios'
                                            ? (
                                                <View style={{
                                                    flexDirection: 'column',
                                                    borderRadius: 8,
                                                    position: 'absolute',
                                                    right: 0,
                                                    top: 0,
                                                    justifyContent: 'center',
                                                    zIndex: 1111,
                                                }}>
                                                    {item.img
                                                        ? (
                                                            <View >
                                                                <Image
                                                                    progressiveRenderingEnabled={true}
                                                                    resizeMethod="resize"
                                                                    source={{ uri: item.img }}
                                                                    style={styles.imgOffer}
                                                                />
                                                            </View>
                                                        )
                                                        :
                                                        (
                                                            <View>
                                                            </View>
                                                        )}
                                                </View>
                                            )
                                            :
                                            (
                                                <View>
                                                </View>
                                            )}
                                    </View>
                                )
                                : (
                                    <View>
                                        <LinearGradient colors={['#e6e6e6', '#ffffff']} style={styles.bgPlanUnSelect}>

                                            <View style={{
                                                padding: 10,
                                                width: wp('40%'),
                                                backgroundColor: 'transparent',
                                            }}>
                                                <Text style={styles.txtOfferBlack} numberOfLines={1}>{`${item.duration}`}</Text>
                                                <View style={{ flexDirection: 'row' }}>

                                                    {item.org_price_text
                                                        ? (
                                                            <Text style={[styles.txtPriceBlack1, {
                                                                textDecorationLine: 'line-through',
                                                                textDecorationStyle: 'solid',
                                                            }]}>{`${item.org_price_text}`}</Text>
                                                        )
                                                        :
                                                        (
                                                            <Text style={styles.txtPriceBlack1}></Text>
                                                        )}
                                                    <View style={{ flex: 1 }}></View>
                                                    {item.offer_price_text
                                                        ? (
                                                            <Text style={styles.txtCouponApllied}>{`${item.offer_price_text}`}</Text>
                                                        )
                                                        :
                                                        (
                                                            <Text style={[styles.txtCouponApllied, { backgroundColor: 'transparent', }]}></Text>
                                                        )}
                                                </View>


                                                <View style={styles.nutritionInnerView}>
                                                    <Text style={styles.textNutTitle1}>{`${item.price_text_2}`}</Text>
                                                    <Text style={styles.textNutDesc1}>{item.period_text}</Text>
                                                </View>
                                                {/* <Text style={styles.txtPriceBlack1}>{`${item.price_text}`}</Text> */}
                                                <Text style={styles.txtBilledBlack1}>{`${item.new_price_text}`}</Text>

                                                {/* {item.offer_price_text
                                                    ? (
                                                        <Text style={styles.txtCouponApllied}>{`${item.offer_price_text}`}</Text>
                                                    )
                                                    :
                                                    (
                                                        <Text style={[styles.txtCouponApllied, { backgroundColor: 'transparent', }]}></Text>
                                                    )} */}
                                            </View>

                                            {Platform.OS === 'android'
                                                ? (
                                                    <View style={{
                                                        flexDirection: 'column',
                                                        borderRadius: 8,
                                                        position: 'absolute',
                                                        right: 0,
                                                        top: -30,
                                                        justifyContent: 'center',
                                                        zIndex: 1111,
                                                        overflow: 'visible'
                                                    }}>
                                                        {item.img
                                                            ? (
                                                                <View >
                                                                    <Image
                                                                        progressiveRenderingEnabled={true}
                                                                        resizeMethod="resize"
                                                                        source={{ uri: item.img }}
                                                                        style={styles.imgOffer}
                                                                    />
                                                                </View>
                                                            )
                                                            :
                                                            (
                                                                <View>
                                                                </View>
                                                            )}
                                                    </View>
                                                )
                                                :
                                                (
                                                    <View>
                                                    </View>
                                                )}


                                        </LinearGradient>
                                        {Platform.OS === 'ios'
                                            ? (
                                                <View style={{
                                                    flexDirection: 'column',
                                                    borderRadius: 8,
                                                    position: 'absolute',
                                                    right: 0,
                                                    top: 0,
                                                    justifyContent: 'center',
                                                    zIndex: 1111,
                                                }}>
                                                    {item.img
                                                        ? (
                                                            <View >
                                                                <Image
                                                                    progressiveRenderingEnabled={true}
                                                                    resizeMethod="resize"
                                                                    source={{ uri: item.img }}
                                                                    style={styles.imgOffer}
                                                                />
                                                            </View>
                                                        )
                                                        :
                                                        (
                                                            <View>
                                                            </View>
                                                        )}
                                                </View>
                                            )
                                            :
                                            (
                                                <View>
                                                </View>
                                            )}
                                    </View>
                                )}
                        </TouchableOpacity>
                    }}
                    itemWidth={itemWidth}
                    sliderWidth={sliderWidth}
                    onSnapToItem={this.changeIndex.bind(this)}
                    separatorWidth={1}
                    // inActiveScale={0.8}
                    ref={(c_plans) => {
                        this._carousel2 = c_plans;
                    }}
                    inactiveSlideScale={0.7}
                    inactiveSlideOpacity={1}
                //pagingEnable={false}
                />

                // <FlatList
                //     horizontal
                //     styles={{ alignItems: 'center', alignContent: 'center', }}
                //     contentContainerStyle={{ padding: 20, paddingRight: 20, paddingBottom: 10, }}
                //     showsHorizontalScrollIndicator={false}
                //     data={this.state.arrAllSubPlans}
                //     keyExtractor={(item, index) => index.toString()}
                //     ItemSeparatorComponent={this.FlatListItemSeparator2}
                //     renderItem={({ item }) => {
                //         return <TouchableOpacity
                //             activeOpacity={0.9}
                //             key={item.pp_id}
                //             style={{
                //                 flexDirection: 'column',
                //                 alignSelf: 'center',paddingTop: 10,
                //             }}
                //             onPress={() => {
                //                 this.press(item);
                //             }}>
                //             {item.check
                //                 ? (
                //                     <LinearGradient colors={['#8b63e6','#8c52ff']} style={styles.bgPlanSelect}>

                //                         <View style={{
                //                             padding: 10,
                //                             width: wp('45%'),
                //                             backgroundColor: 'transparent',
                //                         }}>
                //                             <Text style={styles.txtOfferWhite} numberOfLines={1}>{`${item.duration}`}</Text>
                //                             {item.org_price_text
                //                                 ? (
                //                                     <Text style={[styles.txtActAmtWhite1, {
                //                                         textDecorationLine: 'line-through',
                //                                         textDecorationStyle: 'solid',
                //                                     }]}>{`${item.org_price_text}`}</Text>
                //                                 )
                //                                 :
                //                                 (
                //                                     <Text style={styles.txtActAmtWhite1}></Text>
                //                                 )}

                //                             <Text style={styles.txtActAmtWhite1}>{`${item.price_text}`}</Text>
                //                             <Text style={styles.txtBilledWhite1}>{`${item.new_price_text}`}</Text>
                //                             {item.offer_price_text
                //                                 ? (
                //                                     <Text style={styles.txtCouponApllied}>{`${item.offer_price_text}`}</Text>
                //                                 )
                //                                 :
                //                                 (
                //                                     <Text style={[styles.txtCouponApllied, { backgroundColor: 'transparent', }]}></Text>
                //                                 )}
                //                         </View>

                //                         {item.img
                //                             ? (
                //                                 <View style={{
                //                                     flexDirection: 'column',
                //                                     borderRadius: 8,
                //                                     position: 'absolute',
                //                                     right: 5,
                //                                     top: -30,
                //                                     justifyContent: 'center',
                //                                 }}>
                //                                     <Image
                //                                         progressiveRenderingEnabled={true}
                //                                         resizeMethod="resize"
                //                                         source={{ uri: item.img }}
                //                                         style={styles.imgOffer}
                //                                     />
                //                                 </View>
                //                             )
                //                             :
                //                             (
                //                                 <View>
                //                                 </View>
                //                             )}

                //                     </LinearGradient>
                //                 )
                //                 : (
                //                     <LinearGradient colors={['#e6e6e6', '#ffffff']} style={styles.bgPlanUnSelect}>

                //                         <View style={{
                //                             padding: 10,
                //                             width: wp('43%'),
                //                             backgroundColor: 'transparent',
                //                         }}>
                //                             <Text style={styles.txtOfferBlack} numberOfLines={1}>{`${item.duration}`}</Text>
                //                             {item.org_price_text
                //                                 ? (
                //                                     <Text style={[styles.txtPriceBlack1, {
                //                                         textDecorationLine: 'line-through',
                //                                         textDecorationStyle: 'solid',
                //                                     }]}>{`${item.org_price_text}`}</Text>
                //                                 )
                //                                 :
                //                                 (
                //                                     <Text style={styles.txtPriceBlack1}></Text>
                //                                 )}


                //                             <Text style={styles.txtPriceBlack1}>{`${item.price_text}`}</Text>
                //                             <Text style={styles.txtBilledBlack1}>{`${item.new_price_text}`}</Text>
                //                             {item.offer_price_text
                //                                 ? (
                //                                     <Text style={styles.txtCouponApllied}>{`${item.offer_price_text}`}</Text>
                //                                 )
                //                                 :
                //                                 (
                //                                     <Text style={[styles.txtCouponApllied, { backgroundColor: 'transparent', }]}></Text>
                //                                 )}
                //                         </View>

                //                         {item.img
                //                             ? (
                //                                 <View style={{
                //                                     flexDirection: 'column',
                //                                     borderRadius: 8,
                //                                     position: 'absolute',
                //                                     right: 5,
                //                                     top: -30,
                //                                     backgroundColor: 'transparent',
                //                                     justifyContent: 'center',
                //                                 }}>
                //                                     <Image
                //                                         progressiveRenderingEnabled={true}
                //                                         resizeMethod="resize"
                //                                         source={{ uri: item.img }}
                //                                         style={styles.imgOffer}
                //                                     />
                //                                 </View>
                //                             )
                //                             :
                //                             (
                //                                 <View>
                //                                 </View>
                //                             )}

                //                     </LinearGradient>
                //                 )}
                //         </TouchableOpacity>
                //     }} />

            );
        }
        else {
            return (
                <View style={{ flexDirection: 'row', flex: 1, }}>
                    <Text style={styles.textNodata}>{this.state.noDataMsg}</Text>
                </View>
            );
        }

    }

    renderIaApplyCoupons() {
        // if (this.props.selPlan.trial_info) {
        //     return (
        //         <View style={{ marginTop: 20, marginBottom: 20, paddingLeft: 30, paddingRight: 30 }}>
        //             <Text style={styles.calDetail}>{this.props.selPlan.trial_info}</Text>
        //         </View>
        //     );
        // }
        // else {
        if (this.state.selSubObj.is_coupon === 1) {
            if (this.state.isApplyCoupon) {
                return (
                    <View style={{ marginTop: 10, }}>
                        <View style={styles.calViewDetails}>
                            <View style={styles.viewApplyCoupon1}>
                                <Text style={styles.calDetail1}>{this.state.applyCouponResponse.coupon} coupon applied</Text>
                            </View>
                        </View>

                        {this.props.selPlan.trial_info
                            ?
                            (
                                <View style={{ marginTop: 20, marginBottom: 20, paddingLeft: 30, paddingRight: 30 }}>
                                    <Text style={styles.calDetail}>{this.props.selPlan.trial_info}</Text>
                                </View>
                            )
                            :
                            (
                                <View>
                                </View>
                            )
                        }
                    </View>

                );
            }
            else {
                return (
                    <View style={{ marginTop: 10, }}>
                        <TouchableOpacity style={styles.calViewDetails}
                            onPress={() => {
                                this.setState({ modalVisible: true, });
                            }}>
                            <View style={styles.viewApplyCoupon1}>

                                <Text style={styles.calDetail}>Apply Coupon</Text>
                            </View>
                        </TouchableOpacity>

                        {
                            this.props.selPlan.trial_info
                                ?
                                (
                                    <View style={{ marginTop: 20, marginBottom: 20, paddingLeft: 30, paddingRight: 30 }}>
                                        <Text style={styles.calDetail}>{this.props.selPlan.trial_info}</Text>
                                    </View>
                                )
                                :
                                (
                                    <View>
                                    </View>
                                )
                        }
                    </View>
                );
            }

        }
        else {
            return (
                <View style={{ height: 30 }}>
                </View>
            );
        }
        // }
    }

    toggleModal(visible) {
        this.setState({ modalVisible: visible });
    }

    toggleSelPayMethodModal(visible) {
        this.setState({ selPayMethodPop: visible });
    }

    pressPaymentMethod = (hey) => {
        try {
            this.setState({ selPayMethodObj: hey });
            if (Platform.OS === 'ios' && hey.can_subscribe === 1 && hey.is_ios_iap === 1) {
                // this.setState({ selPayMethodPop: false });
                if (hey.alert_msg != '') {

                    // this.setState({ selPayMethodPop: false });
                    this.setState({ isIapAlert: true, isIapAlertMsg: hey.alert_msg, isIapAlertTitle: 'Alert', isIapBtnYes: 'Proceed', isIapBtnNo: 'Change Payment Mode', isIAPOptional: '' });

                } else {
                    // RNIap.requestSubscription(selectedItem.ios_iap_product_id);
                    RNIap.requestSubscription(selectedItem.ios_iap_product_id).then((purchase) => {

                        // LogUtils.infoLog1('Request Sub res', purchase);
                        if (purchase) {
                            this.saveAppflyerLog(1);
                            this.finishIOSTransaction(purchase.transactionId);
                            this.saveIAPSubscription(purchase);
                        }
                    }).catch(error=>{
                        console.log(" Error in pressPaymentMethod requestSubscription ",error);
                    });
                }


                // Actions.inApp();

            } else {
                if (hey.can_subscribe === 1) {
                    this.getSubscriptionDetails();
                }
                else {
                    this.getOrderDetails();
                }
            }
        } catch (error) {
            console.log(error);
        }

    }

    render() {
        const spin = this.spinValue.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '360deg']
        })
        return (
            <KeyboardAwareScrollView
                resetScrollToCoords={{ x: 0, y: 0 }}
                contentContainerStyle={styles.containerStyle}
                enableOnAndroid={false}
                scrollEnabled={false}>
                <ImageBackground source={require('../res/app_bg.png')} style={styles.containerStyle}>
                    <Loader loading={this.state.loading} />

                    <NoInternet
                        image={require('../res/img_nointernet.png')}
                        loading={this.state.isInternet}
                        onRetry={this.onRetry.bind(this)} />

                    <View style={{
                        flexDirection: 'row',
                        margin: 20,
                    }}>
                        <View style={{ position: 'absolute', zIndex: 111 }}>
                            <TouchableOpacity
                                onPress={() => this.onBackPressed()}>
                                <Image
                                    resizeMethod="resize"
                                    source={require('../res/ic_back.png')}
                                    style={styles.backImageStyle}
                                />
                            </TouchableOpacity>
                        </View>

                        <Text style={styles.textHeadTitle}>{this.props.obj.user_reviews}</Text>


                    </View>
                    <ScrollView contentContainerStyle={{ paddingBottom: hp('3%') }}>
                        <View style={styles.containerStyle}>
                            {Array.isArray(imagesArray) && imagesArray.length
                                ? (

                                    // <ImageSlider
                                    //     loopBothSides
                                    //     loop
                                    //     autoPlayWithInterval={2000}
                                    //     images={this.props.obj.media_2}
                                    //     // titleStyle={styles.sliderDate}
                                    //     indicatorSize={0}
                                    //     customSlide={({ index, item, style, width }) => (
                                    //         <View key={index} style={[style, styles.customSlide]}>
                                    //             {item.media_type === 1
                                    //                 ?
                                    //                 (
                                    //                     <Image
                                    //                         progressiveRenderingEnabled={true}
                                    //                         resizeMethod="resize"
                                    //                         source={{ uri: item.media_url }}
                                    //                         style={{
                                    //                             width: wp('100%'),
                                    //                             height: undefined,
                                    //                             justifyContent: 'center',
                                    //                             alignSelf: 'center',
                                    //                             aspectRatio: 16 / 9,
                                    //                             // transform: [{rotate: spin}] 
                                    //                         }}
                                    //                     />
                                    //                 )
                                    //                 : (
                                    //                     <View style={{ justifyContent: 'center' }}>
                                    //                         <Image
                                    //                             progressiveRenderingEnabled={true}
                                    //                             resizeMethod="resize"
                                    //                             source={{ uri: item.thumbnail_url }}
                                    //                             style={{
                                    //                                 width: wp('100%'),
                                    //                                 height: undefined,
                                    //                                 justifyContent: 'center',
                                    //                                 alignSelf: 'center',
                                    //                                 aspectRatio: 16 / 9,
                                    //                                 // transform: [{rotate: spin}] 
                                    //                             }}
                                    //                         />

                                    //                         <View style={styles.playContainer1}>


                                    //                             <TouchableOpacity
                                    //                                 activeOpacity={0.7}
                                    //                                 style={{ flexDirection: 'row', justifyContent: 'center' }}
                                    //                                 onPress={() => {

                                    //                                     Actions.newvideoplay({ videoURL: item.media_url });
                                    //                                     // this.renderProgramVideoPlay(this.state.resObj.current_day[0]);

                                    //                                 }}>
                                    //                                 <Image
                                    //                                     progressiveRenderingEnabled={true}
                                    //                                     resizeMethod="resize"
                                    //                                     source={require('../res/ic_play_purple.png')}
                                    //                                     style={styles.playIcon} />
                                    //                             </TouchableOpacity>


                                    //                         </View>
                                    //                     </View>


                                    //                 )}



                                    //         </View>
                                    //     )}
                                    // />
                                    <SliderBox
                                        images={imagesArray}
                                        sliderBoxHeight={250}
                                        onCurrentImagePressed={index => console.warn(`image ${index} pressed`)}
                                        dotColor="#8c52ff"
                                        inactiveDotColor="#90A4AE"
                                        // paginationBoxVerticalPadding={20}
                                        autoplay
                                        circleLoop
                                        resizeMethod={'resize'}
                                        resizeMode={'cover'}
                                        paginationBoxStyle={{
                                            position: "absolute",
                                            bottom: 0,
                                            padding: 0,
                                            alignItems: "center",
                                            alignSelf: "center",
                                            justifyContent: "center",
                                        }}
                                        dotStyle={{
                                            width: 0,
                                            height: 0,
                                            borderRadius: 5,
                                            marginHorizontal: 0,
                                            padding: 0,
                                            margin: 0,
                                            backgroundColor: "rgba(128, 128, 128, 0.92)"
                                        }}
                                        ImageComponentStyle={{
                                            width: wp('100%'),
                                            height: undefined,
                                            justifyContent: 'center',
                                            alignSelf: 'center',
                                            aspectRatio: 16 / 9,
                                        }}
                                        imageLoadingColor="#2196F3"
                                    />

                                ) : (

                                    <View style={{ justifyContent: 'center' }}>
                                        <View style={{
                                            width: wp('100%'),
                                            height: hp('26%'),
                                        }}>

                                            {Platform.OS === 'android'
                                                ? (
                                                    <WebView
                                                        javaScriptEnabled={true}
                                                        domStorageEnabled={true}
                                                        style={{
                                                            flex: 1,
                                                            width: wp('100%'),
                                                            // position: 'absolute', top: 0, bottom: 0, right: 0, left: 0,
                                                            backgroundColor: 'black',
                                                        }}
                                                        source={{
                                                            uri: this.props.obj.video_url_2
                                                        }}
                                                    />

                                                ) : (
                                                    <Video
                                                        ref={videoPlayer => { this.videoPlayer = videoPlayer }}
                                                        source={{ uri: this.props.obj.video_url_2 }}
                                                        controls={true}
                                                        style={{
                                                            flex: 1,
                                                            width: wp('100%'),
                                                            // position: 'absolute', top: 0, bottom: 0, right: 0, left: 0,
                                                            backgroundColor: 'black',
                                                        }}

                                                    />

                                                )

                                            }

                                        </View>
                                        {/* //     <Image
                                    //         progressiveRenderingEnabled={true}
                                    //         resizeMethod="resize"
                                    //         source={{ uri: this.props.obj.thumbnail_url_2 }}
                                    //         style={{
                                    //             width: wp('100%'),
                                    //             height: undefined,
                                    //             justifyContent: 'center',
                                    //             alignSelf: 'center',
                                    //             aspectRatio: 16 / 9,
                                    //             // transform: [{rotate: spin}] 
                                    //         }}
                                    //     />

                                    //     <View style={styles.playContainer1}>


                                    //         <TouchableOpacity
                                    //             activeOpacity={0.7}
                                    //             style={{ flexDirection: 'row', justifyContent: 'center' }}
                                    //             onPress={() => {

                                    //                 Actions.newvideoplay({ videoURL: this.props.obj.video_url_2 });
                                    //                 // this.renderProgramVideoPlay(this.state.resObj.current_day[0]);

                                    //             }}>
                                    //             <Image
                                    //                 progressiveRenderingEnabled={true}
                                    //                 resizeMethod="resize"
                                    //                 source={require('../res/ic_play_purple.png')}
                                    //                 style={styles.playIcon} />
                                    //         </TouchableOpacity>


                                    //     </View> */}

                                    </View>

                                )}


                            <View
                                style={{ width: '100%', height: '1%', backgroundColor: '#eeeeee', marginTop: 15 }}>

                            </View>

                            <View style={styles.mainView}>
                                <Text style={styles.txtPlanName}>{this.props.selPlan.title}</Text>
                                {/* <Text style={styles.txtPlanName}>Personal Dietician and Diet plan</Text> */}
                                {/* <Text style={styles.txtPlanDesc}>{this.props.selPlan.description}</Text> */}

                                {/* <Text style={styles.txtBenfitIncludes}>Benfits includes:</Text> */}

                                {this.renderBenfitsInclude()}


                            </View>
                            {/*  sub plans */}
                            <View style={{}}></View>
                            {/* {this.renderAllSubPlansList()} */}
                            {this.renderAllSubPlansList_New()}

                            {this.renderIaApplyCoupons()}
                            {this.state.isButtonEnabled
                                ?
                                (
                                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                                        <TouchableOpacity
                                            onPress={() => {
                                                // LogUtils.infoLog(this.state.selSubObj);
                                                LogUtils.firebaseEventLog('click', {
                                                    p_id: this.props.selPlan.plan_id,
                                                    p_category: 'Plans',
                                                    p_name: `${this.props.selPlan.name} - ${this.state.selSubObj.duration}`,
                                                });
                                                selectedItem = {};
                                                this.props.selPlan.planprice_list.map((item, i) => {
                                                    if (item.check) {
                                                        selectedItem = item;
                                                    }
                                                });
                                                
                                                if (this.props.active_plan === 1) {

                                                    this.setState({ isAlert: true, alertMsg: this.props.active_plan_msg, alertTitle: 'Alert' });

                                                } else {
                                                    // if (Platform.OS === 'ios') {
                                                    //     if (this.state.isApplyCoupon || selectedItem.ios_iap_product_id === '') {
                                                    //         this.setState({ is_ios: 0 });
                                                    //         NetInfo.fetch().then(state => {
                                                    //             if (state.isConnected) {
                                                    //                 // this.getAllSubPlanDetails();
                                                    //                 this.setState({ loading: true });
                                                    //                 this.getAllPaymentMethods(this.state.is_ios);
                                                    //             }
                                                    //             else {
                                                    //                 this.setState({ isInternet: true });
                                                    //             }
                                                    //         });
                                                    //     } else {
                                                    //         this.setState({ selPayMethodPop: true });
                                                    //     }

                                                    // } else {
                                                    //     this.setState({ selPayMethodPop: true });
                                                    // }
                                                    if (Platform.OS === 'ios' && this.props.direct_iap === 1) {
                                                        
                                                        this.setState({ isButtonEnabled: false, loading: true });
                                                        RNIap.requestSubscription(selectedItem.ios_iap_product_id).then((purchase) => {

                                                            // LogUtils.infoLog1('Request Sub res', purchase);
                                                            this.setState({ isButtonEnabled: true, loading: false });
                                                            if (purchase) {
                                                                this.saveAppflyerLog(1);
                                                                this.finishIOSTransaction(purchase.transactionId);
                                                                this.saveIAPSubscription(purchase);
                                                            }
                                                        }).catch(error=>{
                                                            console.log(" Error in requestSubscription ",error);
                                                        });

                                                    } else {
                                                        this.setState({ selPayMethodPop: true });
                                                    }
                                                }
                                                // if (this.state.isApplyCoupon && Platform.OS === 'ios' && selectedItem.ios_iap_product_id === '') {
                                                //     this.setState({ is_ios: 0 });
                                                //     NetInfo.fetch().then(state => {
                                                //         if (state.isConnected) {
                                                //             // this.getAllSubPlanDetails();
                                                //             this.setState({ loading: true });
                                                //             this.getAllPaymentMethods(this.state.is_ios);
                                                //         }
                                                //         else {
                                                //             this.setState({ isInternet: true });
                                                //         }
                                                //     });

                                                // } else {
                                                //     this.setState({ selPayMethodPop: true });
                                                // }
                                                // if (this.state.selSubObj.can_subscribe === 1) {
                                                //     this.getSubscriptionDetails();
                                                // }
                                                // else {
                                                //     this.getOrderDetails();
                                                // }

                                            }}
                                            style={styles.buttonTuch}>
                                            <Text style={styles.buttonText}>Buy Now</Text>
                                        </TouchableOpacity>
                                    </LinearGradient>
                                ) :
                                (
                                    <View></View>
                                )
                            }

                        </View>

                    </ScrollView>

                    {/* <View style={styles.bottom}>
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                        <TouchableOpacity
                            onPress={() => {
                                // LogUtils.infoLog(this.state.selSubObj);

                                LogUtils.firebaseEventLog('click', {
                                    p_id: this.props.selPlan.plan_id,
                                    p_category: 'Plans',
                                    p_name: `${this.props.selPlan.name} - ${this.state.selSubObj.duration}`,
                                });

                                if (this.state.selSubObj.can_subscribe === 1) {
                                    this.getSubscriptionDetails();
                                }
                                else {
                                    this.getOrderDetails();
                                }
                            }}
                            style={styles.buttonTuch}>
                            <Text style={styles.buttonText}>Buy Now</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                </View> */}

                    <Dialog
                        onDismiss={() => {
                            this.setState({ modalVisible: false });
                        }}
                        onTouchOutside={() => {
                            this.setState({ modalVisible: false });
                        }}
                        // width={0.9}
                        // height={0.9}
                        dialogStyle={{ backgroundColor: '#ffffff', }}
                        visible={this.state.modalVisible}>
                        <DialogContent
                            style={{
                                // backgroundColor: '#b0b0b0',
                                flex: 1,

                            }}>
                            <KeyboardAwareScrollView
                                resetScrollToCoords={{ x: 0, y: 0 }}
                                contentContainerStyle={styles.containerStyle}
                                enableOnAndroid={false}
                                scrollEnabled={false}>
                                <View style={{ flexDirection: 'column', backgroundColor: '#ffffff', width: wp('100%'), }}>

                                    <View style={{
                                        flexDirection: 'row',
                                        margin: 20,
                                    }}>
                                        <View style={{ position: 'absolute', zIndex: 111 }}>
                                            <TouchableOpacity
                                                style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                                                onPress={() => this.toggleModal(!this.state.modalVisible)}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_back.png')}
                                                    style={styles.backImageStyle1}
                                                />
                                            </TouchableOpacity>
                                        </View>

                                        <Text style={styles.textHeadTitle}>Apply Coupon</Text>
                                    </View>

                                    <View style={{ flexDirection: 'column', alignSelf: 'center', alignItems: 'center', justifyContent: 'flex-start' }}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/img_apply_coupon.png')}
                                            style={styles.imgNoSuscribe}
                                        />
                                        <Text style={styles.textNodataTitle}>{this.props.selPlan.coupon_msg}</Text>
                                    </View>
                                </View>
                                <View style={styles.bottom}>

                                    <View style={styles.containerMobileStyle}>
                                        <TextInput
                                            ref={input => { this.addressTextInput = input; }}
                                            style={styles.textInputStyle}
                                            placeholder="Enter Coupon Code"
                                            maxLength={20}
                                            placeholderTextColor="grey"
                                            value={this.state.couponCode}
                                            autoCapitalize='words'
                                            returnKeyType='done'
                                            onChangeText={text => {
                                                if (text) {
                                                    this.setState({ couponCode: text });
                                                }
                                                else {
                                                    this.setState({ couponCode: '' });
                                                }
                                            }}
                                        />
                                    </View>

                                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                                        <TouchableOpacity
                                            onPress={() => {
                                                this.callApplyCouponsService();
                                            }}
                                            style={styles.buttonTuch}>
                                            <Text style={styles.buttonText}>Submit</Text>
                                        </TouchableOpacity>

                                    </LinearGradient>
                                </View>
                            </KeyboardAwareScrollView>
                        </DialogContent>
                    </Dialog>

                    <Dialog
                        onDismiss={() => {
                            this.setState({ isSuccess: false, modalVisible: false });
                        }}
                        onTouchOutside={() => {
                            this.setState({ isSuccess: false, modalVisible: false });
                        }}
                        width={0.75}
                        visible={this.state.isSuccess}
                    >
                        <DialogContent
                            style={{
                                backgroundColor: '#ffffff'
                            }}>
                            <View style={{ flexDirection: 'column', }}>
                                <View style={{ flexDirection: 'column', padding: 10 }}>

                                    <View style={{ alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'column' }}>

                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/img_apply_coupon_success.png')}
                                            style={styles.imgpopup}
                                        />
                                        <Text style={styles.textPopTitle}>{this.state.applyCouponResponse.title}</Text>
                                        <Text style={styles.textPopDesc}>{this.state.applyCouponResponse.message}</Text>
                                        <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                                            <TouchableOpacity onPress={() => {
                                                this.setState({ isSuccess: false, modalVisible: false });
                                                // this.cancelSubscriptions();
                                            }}>
                                                <Text style={styles.textSave}>OK</Text>
                                            </TouchableOpacity>
                                        </LinearGradient>

                                    </View>

                                </View>

                            </View>

                        </DialogContent>
                    </Dialog>

                    <Dialog
                        onDismiss={() => {
                            this.setState({ isFail: false });
                        }}
                        onTouchOutside={() => {
                            this.setState({ isFail: false });
                        }}
                        width={0.75}
                        visible={this.state.isFail}
                    >
                        <DialogContent
                            style={{
                                backgroundColor: '#ffffff'
                            }}>
                            <View style={{ flexDirection: 'column', }}>
                                <View style={{ flexDirection: 'column', padding: 10 }}>

                                    <View style={{ alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'column' }}>

                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/img_apply_coupon_fail.png')}
                                            style={styles.imgpopup}
                                        />
                                        <Text style={styles.textPopTitle}>{this.state.applyCouponResponse.title}</Text>
                                        <Text style={styles.textPopDesc}>{this.state.applyCouponResponse.message}</Text>
                                        <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                                            <TouchableOpacity onPress={() => {
                                                this.setState({ isFail: false });
                                            }}>
                                                <Text style={styles.textSave}>OK</Text>
                                            </TouchableOpacity>
                                        </LinearGradient>

                                    </View>

                                </View>

                            </View>

                        </DialogContent>
                    </Dialog>



                    <Modal
                        backdropColor="#B4B3DB"
                        backdropOpacity={0.8}
                        animationIn="zoomInDown"
                        animationOut="zoomOutUp"
                        animationInTiming={600}
                        animationOutTiming={600}
                        backdropTransitionInTiming={600}
                        backdropTransitionOutTiming={600}
                        visible={this.state.selPayMethodPop}
                        onRequestClose={() => { this.toggleSelPayMethodModal(!this.state.selPayMethodPop) }}>

                        <View style={{
                            flex: 1,
                            alignItems: 'center',
                            backgroundColor: '#ffffff',
                            justifyContent: 'center',
                        }}>

                            <TouchableOpacity style={styles.viewTop}
                                onPress={() => { this.toggleSelPayMethodModal(!this.state.selPayMethodPop) }}>
                                <Image
                                    source={require('../res/ic_back.png')}
                                    style={styles.backImageStyle}
                                />
                            </TouchableOpacity>

                            <Text style={styles.TextHeaderStyle}>Select your payment method</Text>
                            <FlatList
                                contentContainerStyle={{ paddingBottom: hp('10%') }}
                                showsVerticalScrollIndicator={false}
                                data={this.state.arrPaymentMethods}
                                keyExtractor={item => item.pt_id.toString()}
                                ItemSeparatorComponent={this.FlatListSeparatorPayMethods}
                                renderItem={({ item }) => {
                                    return <TouchableOpacity key={item.pt_id} onPress={() => {
                                        //this.toggleSelPayMethodModal(!this.state.selPayMethodPop)
                                        this.pressPaymentMethod(item)
                                    }}>
                                        <View style={styles.vPayBg}>
                                            <Image
                                                progressiveRenderingEnabled={true}
                                                source={{ uri: item.img }}
                                                style={{
                                                    width: 30,
                                                    height: 30,
                                                    alignSelf: 'center',
                                                }}
                                            />

                                            <View style={styles.performancecolumn}>
                                                <Text style={styles.performancetextStyle}>{item.title}</Text>
                                                <Text style={styles.waterdranktextStyle}>{item.descp}</Text>
                                            </View>
                                        </View>

                                    </TouchableOpacity>
                                }} />
                        </View>

                        <IAPCustomDialog
                            visible={this.state.isIapAlert}
                            title={this.state.isIapAlertTitle}
                            desc={this.state.isIapAlertMsg}
                            optional={this.state.isIAPOptional}
                            onAccept={this.onIapAccept.bind(this)}
                            onDecline={this.onIapDeny.bind(this)}
                            onOptional={this.onIapDeny.bind(this)}
                            no={this.state.isIapBtnNo}
                            yes={this.state.isIapBtnYes} />

                    </Modal>

                    <HomeDialog
                        visible={this.state.isAlert}
                        title={this.state.alertTitle}
                        desc={this.state.alertMsg}
                        onAccept={this.onAccept.bind(this)}
                        no=''
                        yes='Ok' />

                    <IAPCustomDialog
                        visible={this.state.isIapAlert}
                        title={this.state.isIapAlertTitle}
                        desc={this.state.isIapAlertMsg}
                        optional={this.state.isIAPOptional}
                        onAccept={this.onIapAccept.bind(this)}
                        onDecline={this.onIapDeny.bind(this)}
                        onOptional={this.onIapDeny.bind(this)}
                        no={this.state.isIapBtnNo}
                        yes={this.state.isIapBtnYes} />


                </ImageBackground>
            </KeyboardAwareScrollView>
        )
    }
}

const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        height: '100%',
        flex: 1,
    },
    backImageStyle: {
        width: 19,
        height: 16,
        alignSelf: 'flex-start',
    },
    backImageStyle1: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'center',
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    mainView: {
        flexDirection: 'column',
        margin: 20,
    },
    txtPlanName: {
        fontSize: 20,
        fontWeight: '500',
        color: '#282c37',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
        textAlign: 'center',
    },
    txtPlanDesc: {
        fontSize: 12,
        fontWeight: '400',
        color: '#6D819C',
        marginTop: 15,
        fontFamily: 'Rubik-Regular',
        letterSpacing: 0.2,
    },
    txtBenfitIncludes: {
        fontSize: 12,
        fontWeight: '400',
        color: '#8c52ff',
        marginTop: 20,
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.4,
    },
    viewBenInclu: {
        flexDirection: 'row',
        marginTop: 20,
    },
    viewBenIncluInner: {
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignSelf: 'center',
        marginLeft: 15,
    },
    txt14: {
        fontSize: 16,
        fontWeight: '500',
        color: '#282c37',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
    },
    txtDesc: {
        fontSize: 12,
        fontWeight: '400',
        color: '#282c37',
        marginRight: 30,
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
    },

    txtBenifit: {
        height: 70,
        fontSize: 12,
        fontWeight: '400',
        color: '#6D819C',
        marginTop: 15,
        fontFamily: 'Rubik-Regular',
        letterSpacing: 0.2,
        textAlign: 'center',
        lineHeight: 20,
    },
    bgSelect: {
        width: wp('90%'),
        // height: 80,
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        flexDirection: 'row',
        borderRadius: 15,
        marginBottom: 5,
        position: 'relative',
        alignSelf: 'center',
        shadowColor: 'rgba(0, 0, 0, 0.08)',
        shadowOffset: { width: 5, height: 0 },
        shadowOpacity: 0.25,
        shadowRadius: 10,
        elevation: 2,
        marginTop: 10,
        zIndex: 111,
        borderWidth: .5,
        borderColor: '#6D819C',
    },
    bgUnSelect: {
        width: wp('85%'),
        backgroundColor: '#e6e6e6',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderRadius: 15,
        marginBottom: 5,
        // position: 'relative',
        alignSelf: 'center',
        // alignContent: 'center',
        alignItems: 'center',
        // shadowColor: 'rgba(0, 0, 0, 0.08)',
        // shadowOffset: {width: 5, height: 0 },
        // shadowOpacity: 0.25,
        // shadowRadius: 5,
        // elevation: 2,
        marginTop: 15,
        zIndex: 111,
    },
    bgPlanSelect: {
        width: wp('40%'),
        height: undefined,
        // backgroundColor: '#8c52ff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderRadius: 15,
        marginBottom: 5,
        marginLeft: 2,
        marginRight: 2,
        marginTop: 30,
        // position: 'relative',
        alignSelf: 'center',
        // alignContent: 'center',
        alignItems: 'center',
        shadowColor: 'rgba(0, 0, 0, 0.08)',
        shadowOffset: { width: 5, height: 0 },
        shadowOpacity: 0.25,
        shadowRadius: 5,
        elevation: 2,
        // zIndex: 111,
        aspectRatio: 1,
        // overflow: 'visible',
    },
    bgPlanUnSelect: {
        width: wp('40%'),
        height: undefined,
        // backgroundColor: '#e6e6e6',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderRadius: 15,
        marginBottom: 5,
        marginLeft: 2,
        marginRight: 2,
        marginTop: 30,
        // position: 'relative',
        alignSelf: 'center',
        // alignContent: 'center',
        alignItems: 'center',
        shadowColor: 'rgba(0, 0, 0, 0.08)',
        shadowOffset: { width: 5, height: 0 },
        shadowOpacity: 0.25,
        shadowRadius: 5,
        elevation: 2,
        // zIndex: 111,
        aspectRatio: 1,
    },
    txtOfferWhite: {
        fontSize: 18,
        fontWeight: '500',
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
        alignSelf: 'flex-start',
        textAlign: 'left',
        marginBottom: 10,
    },
    txtOfferBlack: {
        fontSize: 18,
        fontWeight: '500',
        color: '#2d3142',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
        alignSelf: 'flex-start',
        textAlign: 'left',
        marginBottom: 5,
    },
    txtActAmtWhite: {
        fontSize: 14,
        fontWeight: '500',
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
        textDecorationStyle: 'solid',
        marginTop: 3
    },
    txtActAmtWhite1: {
        fontSize: 11,
        fontWeight: '500',
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
        textDecorationStyle: 'solid',
        marginTop: 3
    },
    txtBilledWhite: {
        fontSize: 12,
        fontWeight: '400',
        color: '#ffffff',
        fontFamily: 'Rubik-Regular',
        letterSpacing: 0.2,
        textDecorationStyle: 'solid',
        marginTop: 5
    },
    txtBilledWhite1: {
        fontSize: 9,
        fontWeight: '400',
        color: '#ffffff',
        fontFamily: 'Rubik-Regular',
        letterSpacing: 0.2,
        textDecorationStyle: 'solid',
        marginTop: 5
    },
    txtBilledBlack: {
        fontSize: 12,
        fontWeight: '400',
        color: '#6d819c',
        fontFamily: 'Rubik-Regular',
        letterSpacing: 0.2,
        textDecorationStyle: 'solid',
        marginTop: 5
    },
    txtBilledBlack1: {
        fontSize: 9,
        fontWeight: '400',
        color: '#6d819c',
        fontFamily: 'Rubik-Regular',
        letterSpacing: 0.2,
        textDecorationStyle: 'solid',
        marginTop: 5
    },
    txtActAmtBlack: {
        fontSize: 10,
        fontWeight: '500',
        color: '#282c37',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
        textDecorationLine: 'line-through',
        textDecorationStyle: 'solid',
    },
    txtPriceBlack: {
        fontSize: 14,
        fontWeight: '500',
        color: '#282c37',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
        textDecorationStyle: 'solid',
        marginTop: 3
    },
    txtPriceBlack1: {
        fontSize: 11,
        fontWeight: '500',
        color: '#282c37',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
        textDecorationStyle: 'solid',
        marginTop: 3
    },
    txtOffAmtWhite: {
        fontSize: 10,
        fontWeight: '700',
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
    },
    txtOffAmtBlack: {
        fontSize: 10,
        fontWeight: '700',
        color: '#282c37',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
    },
    txtOffPerBlack: {
        fontSize: 10,
        fontWeight: '500',
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
        textAlign: 'center',
        // marginTop: 5,
        paddingLeft: 7,
        paddingRight: 7,
        alignSelf: 'center',
        paddingTop: 3,
        paddingBottom: 3,
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
    },
    txtCouponApllied: {
        // fontSize: 8,
        // fontWeight: '500',
        // color: '#ffffff',
        // fontFamily: 'Rubik-Medium',
        // backgroundColor: '#7ba6e7',
        // borderRadius: 8,
        // textAlign: 'left',
        // padding: 5,
        // marginTop: 10,
        // alignSelf: 'flex-start',
        // borderTopRightRadius: 10,
        // borderBottomRightRadius: 10,

        fontSize: 8,
        fontWeight: '500',
        color: '#000000',
        fontFamily: 'Rubik-Medium',
        backgroundColor: '#ffbf00',
        borderTopLeftRadius: 8,
        borderBottomLeftRadius: 8,
        textAlign: 'left',
        padding: 5,
        marginRight: -10,
        alignSelf: 'flex-end',
    },
    txtSaveAmtBlack: {
        fontSize: 9,
        fontWeight: '500',
        color: '#282c37',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
        marginTop: 15,
        marginRight: 30,
        backgroundColor: '#FAD541',
        padding: 3,
        borderRadius: 3,
    },
    bottom: {
        width: '90%',
        justifyContent: 'center',
        position: 'absolute',
        bottom: 0,
        alignSelf: 'center',
        marginBottom: 20,
        flexDirection: 'column',
    },
    linearGradient: {
        width: '90%',
        height: 50,
        borderRadius: 25,
        justifyContent: 'center',
        alignSelf: 'center',
    },
    buttonTuch:
    {
        backgroundColor: 'transparent',
    },
    buttonText: {
        fontSize: 14,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    viewApplyCoupon: {
        margin: 25,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    txtApplyCoupon: {
        fontSize: 10,
        fontWeight: '500',
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 3,
        marginLeft: 3,
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
        paddingBottom: 3,
        backgroundColor: '#8c52ff',
        letterSpacing: 0.3,
    },
    modal: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#ffffff',
        justifyContent: 'center',
    },
    imgNoSuscribe: {
        width: 270,
        height: 270,
        marginTop: 50,
        alignSelf: 'center',
    },
    textNodataTitle: {
        width: wp('80%'),
        fontSize: 14,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        padding: 15,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
    },
    textNodata: {
        width: wp('80%'),
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
    },
    containerMobileStyle: {
        width: '90%',
        borderBottomWidth: 1,
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 15,
        position: 'relative',
        alignSelf: 'center',
        padding: 8,
        marginBottom: 50,
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    textInputStyle: {
        height: 40,
        flex: 1,
        fontSize: 13,
        marginLeft: 10,
        fontFamily: 'Rubik-Regular',
        color: '#2d3142',
    },
    imgpopup: {
        width: 200,
        height: 200,
        alignSelf: 'center',
    },
    textPopTitle: {
        fontSize: 16,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
    },
    textPopDesc: {
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
        padding: 5,
    },
    textSave: {
        width: wp('40%'),
        fontSize: 14,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        borderRadius: 15,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 8,
        paddingBottom: 8,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#ffffff',
    },
    linearGradientEnroll: {
        borderRadius: 15,
        justifyContent: 'center',
        alignSelf: 'center',
        alignSelf: "center",
        alignItems: 'center',
        marginTop: 25,
    },

    slide1: {
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 40,
        marginRight: 40,
    },
    text: {
        fontSize: 14,
        fontWeight: '400',
        color: '#6D819C',
        fontFamily: 'Rubik-Regular',
        letterSpacing: 0.2,
        textAlign: 'center',
        lineHeight: 20,
    },
    viewWhiteDot: {
        width: 10,
        height: 10,
        backgroundColor: '#8c52ff',
        alignSelf: 'flex-start',
        marginTop: 3,
        borderRadius: 5,
    },
    calViewDetails: {
        // // width: 60,
        borderRadius: 25,
        borderColor: '#e9e9e9',
        // // borderStyle: 'solid',
        borderWidth: 1,
        alignSelf: 'flex-end',
        marginRight: wp('10%'),
        marginTop: 10,
        marginBottom: 25,
        justifyContent: 'flex-end',
        // backgroundColor: '#ffffff',
    },
    viewApplyCoupon1: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 5,
    },
    calDetail: {
        fontSize: 12,
        fontWeight: '400',
        color: '#282c37',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
        lineHeight: 15,
    },
    calDetail1: {
        color: '#8c52ff',
        fontFamily: 'Rubik-Medium',
        fontSize: 11,
        alignSelf: 'center',
        fontWeight: '500',
    },
    TextHeaderStyle: {
        color: '#2d3142',
        fontFamily: 'Rubik-Medium',
        fontSize: 18,
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 20,
        letterSpacing: 0.2,
        alignSelf: 'center',
        textAlign: 'center',
    },
    viewTop: {
        flexDirection: 'column',
        // position: 'absolute',
        margin: 20,
        alignItems: 'center',
        padding: 5,
        justifyContent: 'center',
        alignSelf: 'flex-start',
        width: 34,
        height: 34,
        borderRadius: 17,
        // backgroundColor: 'white',
        // shadowColor: 'rgba(0, 0, 0, 0.08)',
        // shadowOffset: {width: 5, height: 0 },
        // shadowOpacity: 0.25,
        // shadowRadius: 10,
        // elevation: 6,
    },
    vPayBg: {
        width: wp('90%'),
        marginLeft: wp('5%'),
        marginRight: wp('5%'),
        // backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderRadius: 6,
        position: 'relative',
        alignSelf: 'center',
        borderColor: '#b0b0b0',
        borderWidth: 0.3,
        // shadowColor: "#000",
        // shadowOffset: {width: 5, height: 0 },
        // shadowOpacity: 0.25,
        // shadowRadius: 10,
        // elevation: 5,
        // marginTop: 2.5,
        padding: 12,
    },
    performancecolumn: {
        flexDirection: 'column',
        flex: 1,
        alignItems: 'flex-start',
        marginLeft: 12,
    },
    performancetextStyle: {
        fontSize: 14,
        fontWeight: '500',
        color: '#2d3142',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    waterdranktextStyle: {
        fontSize: 12,
        color: '#C3C1CE',
        textAlign: 'left',
        lineHeight: 16,
        marginTop: 3,
        fontFamily: 'Rubik-Regular',
    },
    imgOffer: {
        width: 60,
        height: 60,
        alignSelf: 'center',
        aspectRatio: 1,
    },
    nutritionInnerView: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignSelf: 'flex-start',
        marginTop: 5,
    },
    textNutTitle: {
        fontSize: 25,
        fontWeight: '500',
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
    },
    textNutDesc: {
        fontSize: 11,
        fontWeight: '400',
        color: '#ffffff',
        marginTop: 5,
        alignSelf: 'center',
        fontFamily: 'Rubik-Regular',
        letterSpacing: 0.2,
    },
    textNutTitle1: {
        fontSize: 25,
        fontWeight: '500',
        color: '#2d3142',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
    },
    textNutDesc1: {
        fontSize: 11,
        fontWeight: '400',
        color: '#2d3142',
        marginTop: 5,
        alignSelf: 'center',
        fontFamily: 'Rubik-Regular',
        letterSpacing: 0.2,
    },
    customSlide: {
        backgroundColor: '#1f1f1f',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    playContainer1: {
        flexDirection: 'row',
        alignContent: 'center',
        position: 'absolute',
        alignSelf: 'center',
        justifyContent: 'center',
    },
    playIcon: {
        width: 35,
        height: 35,
        alignSelf: 'center',
        justifyContent: 'center',
    },
});

const mapStateToProps = state => {
    // const {goals, trainerCodes} = state.procre;
    // const loading = state.procre.loading;
    // const error = state.procre.error;
    return {};
};

export default Trainee_BuyPlanDetailsNew;
