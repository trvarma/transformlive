import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getActiveLevels } from '../actions';
import { CustomDialog, Loader, NoInternet } from './common';
import { withNavigationFocus } from 'react-navigation';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  BackHandler,
  ImageBackground,
  KeyboardAvoidingView,
  StatusBar,
  FlatList,
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage';
import LogUtils from '../utils/LogUtils.js';
import NetInfo from "@react-native-community/netinfo";
import DeviceInfo from 'react-native-device-info';
import { allowFunction } from '../utils/ScreenshotUtils.js';



class ActivityLevel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeId: 0,
      fakeContact: [],
      SelectedFakeContactList: [],
      isAlert: false,
      alertMsg: '',
      isInternet: false,
    };
  }

  async componentDidMount() {
    allowFunction();
    StatusBar.setHidden(true);

    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.props.getActiveLevels();
      }
      else {
        this.setState({ isInternet: true });
      }
    });

    

    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      if (this.props.isFocused) {
        Actions.pop();
      } else {
        this.props.navigation.goBack(null);
      }
      return true;
    });
  }


  componentWillUnmount() {
    this.backHandler.remove();
  }

  onBackPressed() {
    Actions.pop();
  }

  async onButtonPressed() {
    try {
      if (this.state.activeId !== 0) {

        await AsyncStorage.setItem('activeId', this.state.activeId.toString());
        actLevel = await AsyncStorage.getItem('activeId');

        LogUtils.firebaseEventLog('click', {
          p_id: 107,
          p_category: 'Registration',
          p_name: 'ActiveLevel',
        });
        Actions.calorieCounterSetup();
      }
      else {
        this.setState({ isAlert: true, alertMsg: 'Please select your level of activity' });
      }
    } catch (error) {
      console.log("Error in Activity level onButtonPressed ",error);
    }
  }

  renderButton() {
    return (
      <View style={styles.containerSubmitStyle}>
        <Text style={styles.textSubmitStyle}>
          {'request otp'.toUpperCase()}
        </Text>
      </View>
    );
  }

  FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 2,
          width: "100%",
          backgroundColor: "transparent",
        }}
      />
    );
  }

  async onAccept() {
    this.setState({ isAlert: false,alertMsg: ''  });
  }

  async onRetry() {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        this.setState({ isInternet: false });
        this.props.getActiveLevels();
      }
      else {
        this.setState({ isInternet: true });
      }
    });
  }

  render() {
    return (
      <KeyboardAvoidingView keyboardVerticalOffset={Platform.select({ ios: 0, android: -20 })} style={styles.containerStyle} behavior="padding" enabled>
        <ImageBackground source={require('../res/app_bg.png')} style={styles.img}>
          <Loader loading={this.props.loading} />
          <NoInternet
            image={require('../res/img_nointernet.png')}
            loading={this.state.isInternet}
            onRetry={this.onRetry.bind(this)} />
          <View style={{
            flexDirection: 'row',
            margin: 20,
          }}>
            <View style={{ position: 'absolute', zIndex: 111 }}>
              <TouchableOpacity
                style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                onPress={() => this.onBackPressed()}>
                <Image
                  source={require('../res/ic_back.png')}
                  style={styles.backImageStyle}
                />
              </TouchableOpacity>
            </View>

            <Text style={styles.textIndicator}>4 / 4</Text>
          </View>

          <View style={styles.optionInnerContainer}>
            <Image
              source={require('../res/ic_app.png')}
              style={styles.appImageStyle}
            />
          </View>

          <View style={styles.containericonStyle}>

            <Text style={styles.subtextOneStyle}>How active are you?</Text>
            <Text style={styles.desc}>We need the below information to personalise your experience</Text>


            <FlatList
              contentContainerStyle={{ paddingBottom: hp('45%') }}
              showsVerticalScrollIndicator={false}
              data={this.props.activeLevels}
              keyExtractor={(item, index) => index + "activeLevels"}
              ItemSeparatorComponent={this.FlatListItemSeparator}
              renderItem={({ item }) => {
                return <TouchableOpacity key={item.id} style={styles.containerMaleStyle} onPress={() => {
                  this.press(item)
                }}>
                  <View >
                    <Text style={styles.countryText}>{`${item.name}`}</Text>
                    <Text style={[styles.countryText, { fontSize: 11,paddingVertical:6 }]}>{item.sub_title}</Text>
                  </View>
                  <View style={styles.mobileImageStyle}>
                    {item.check
                      ? (
                        <TouchableOpacity onPress={() => {
                          this.press(item)
                        }}>
                          <Image source={require('../res/check.png')} style={styles.mobileImageStyle} />
                        </TouchableOpacity>
                      )
                      : (
                        <TouchableOpacity onPress={() => {
                          this.press(item)
                        }}>
                          <Image source={require('../res/uncheck.png')} style={styles.mobileImageStyle} />
                        </TouchableOpacity>
                      )}
                  </View>
                </TouchableOpacity>
              }} />
          </View>

          <View style={styles.viewBottom}>
            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
              <TouchableOpacity
                onPress={() => this.onButtonPressed()}>
                <Text style={styles.buttonText}>Next</Text>
              </TouchableOpacity>
            </LinearGradient>
          </View>

          <CustomDialog
            visible={this.state.isAlert}
            title='Alert'
            desc={this.state.alertMsg}
            onAccept={this.onAccept.bind(this)}
            no=''
            yes='Ok' />

        </ImageBackground>
      </KeyboardAvoidingView>
    );
  }

  press = (hey) => {
    this.props.activeLevels.map((item) => {
      if (item.id === hey.id) {
        item.check = true;
        this.setState({ activeId: item.id });
      }
      else {
        item.check = false;
      }
    })
  }
}


const styles = StyleSheet.create({
  containerStyle: {
    width: '100%',
    height: '100%',
    flex: 1,
  },
  img: {
    width: '100%',
    height: '100%',
    flex: 1,
  },
  containericonStyle: {
    flexDirection: 'column',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  containerMaleStyle: {
    width: wp('90%'),
    height: hp('9%'),
    marginTop: hp('1%'),
    backgroundColor: '#ffffff',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: '#ddd',
    borderRadius: 10,
    position: 'relative',
    alignSelf: 'center',
    marginLeft: 15,
    marginRight: 15,
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: '#4075cd',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  containerSubmitStyle: {
    width: '93%',
    backgroundColor: '#06a283',
    flexDirection: 'row',
    borderRadius: 5,
    justifyContent: 'center',
    alignSelf: 'center',
    padding: 15,
    marginTop: 5,
  },
  subtextOneStyle: {
    fontSize: 20,
    marginTop: hp('3%'),
    marginLeft: 40,
    marginRight: 40,
    fontWeight: '500',
    color: '#2d3142',
    lineHeight: 30,
    textAlign: 'center',
    fontFamily: 'Rubik-Medium',
  },
  desc: {
    fontSize: 14,
    marginTop: 10,
    marginLeft: 20,
    marginRight: 20,
    fontWeight: '400',
    color: '#9c9eb9',
    lineHeight: 24,
    textAlign: 'center',
    fontFamily: 'Rubik-Regular',
    marginTop: hp('2%'),
    marginBottom: hp('2%'),
  },
  textSubmitStyle: { fontSize: 14, color: '#ffffff', fontWeight: 'bold' },
  mobileImageStyle: { width: 25, height: 25, position: 'relative', alignSelf: 'center', marginRight: 10 },
  linearGradient: {
    width: '95%',
    height: 50,
    borderRadius: 25,
    justifyContent: 'center',
    alignSelf: 'center',
    backgroundColor: 'transparent',
    marginBottom: 15,
  },
  buttonText: {
    fontSize: 16,
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    fontFamily: 'Rubik-Medium',
    fontWeight: '500',
  },
  countryText: {
    width: wp('75%'),
    alignSelf: 'center',
    fontSize: 14,
    fontWeight: '400',
    marginLeft: wp('2%'),
    color: '#2d3142',
    letterSpacing: 0.23,
    fontFamily: 'Rubik-Regular',
  },
  backImageStyle: {
    width: 19,
    height: 16,
    marginTop: 10,
    alignSelf: 'center',
  },
  appImageStyle: {
    width: 200,
    height: 60,
    marginTop: 40,
  },
  optionInnerContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  viewBottom: {
    width: wp('93%'),
    alignContent: 'center',
    alignSelf: 'center',
    position: 'absolute',
    bottom: 0,
    backgroundColor: '#fff',
  },
  textIndicator: {
    fontSize: 14,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    flex: 1,
    textAlign: 'center',
    alignSelf: 'center',
    color: '#2d3142',
    // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
    paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
  },
});

const mapStateToProps = state => {
 
  const { activeLevels } = state.masters;
  const loading = state.masters.loading;
  const error = state.masters.error;
 
  return { 
    loading, 
    error, 
    activeLevels,
  };
};

export default withNavigationFocus(
  connect(
    mapStateToProps,
    { getActiveLevels},
  )(ActivityLevel),
);
