import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    BackHandler,
    ScrollView,
    ImageBackground,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import { withNavigationFocus } from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import LogUtils from '../utils/LogUtils.js';
import DeviceInfo from 'react-native-device-info';
import { BASE_URL, VINFO_ANDROID, VINFO_IOS } from '../actions/types';
import { allowFunction } from '../utils/ScreenshotUtils.js'; 

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
      statusCode: res[0],
      data: res[1],
    }));
  }

class Trainee_GoalCompleted extends Component {

    constructor(props) {
        super(props);
        this.state = {
            fill: 100,
            userName: '',
            proImg: '',
            present_weight: '',
            sucessmsg: '',
            goal_code: '',
            goal_name: '',
            weight_target: '',
            calperday: '',
            complete_date: '',
            goal_text: '',
            quotes: '',
            author: '',
        };
    }

    async componentDidMount() {

        allowFunction();

        this.setState({ proImg: await AsyncStorage.getItem('profileImg') });
        this.setState({ goal_code: await AsyncStorage.getItem('goal_code') });
        this.setState({ goal_name: await AsyncStorage.getItem('goalName') });
        this.setState({ userName: await AsyncStorage.getItem('userName') });
        this.setState({ present_weight: await AsyncStorage.getItem('present_weight') });
        this.setState({ weight_target: await AsyncStorage.getItem('weight_target') });
        this.setState({ calperday: await AsyncStorage.getItem('calperday') });
        this.setState({ complete_date: await AsyncStorage.getItem('complete_date') });
        this.setState({ sucessmsg: await AsyncStorage.getItem('sucessmsg') });
        this.setState({ goal_text: await AsyncStorage.getItem('goal_text') });
        this.setState({ quotes: await AsyncStorage.getItem('quotes') });
        this.setState({ author: await AsyncStorage.getItem('author') });

        this.checkVersion();

        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onButtonPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    async checkVersion() {
        let token = await AsyncStorage.getItem('token');
        let version = DeviceInfo.getVersion();
        let data;

        if (Platform.OS === 'android') {
            data = JSON.stringify({
                os_id: 1,
                curnt_vrsn: version,
                vrsn_info: VINFO_ANDROID,
            });
        }
        else if (Platform.OS === 'ios') {
            data = JSON.stringify({
                os_id: 2,
                curnt_vrsn: version,
                vrsn_info: VINFO_IOS,
            });
        }

        LogUtils.infoLog(data);

        fetch(
            `${BASE_URL}/trainee/appversion`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: data,
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('version statusCode', statusCode);
                LogUtils.infoLog1('version data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                } else {
                    // if (data.message === 'You are not authenticated!') {
                    //   this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                    // } else {
                    //   this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                    // }
                }
            })
            .catch(function (error) {
                // this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error), alertTitle: 'Alert' });
            });
    }

    async onButtonPressed() {
        LogUtils.firebaseEventLog('click', {
            p_id: 115,
            p_category: 'Registration',
            p_name: 'UserGoalDetails',
        });
        Actions.thome({ type: 'reset' });
    }

    renderList() {
        if (this.state.goal_text) {
            let whatDiet;
            whatDiet = this.state.goal_text.split('||').map(function (address, index) {
                return <View key={index} style={styles.viewListAllInner}>
                    <View style={styles.viewBlueDot}></View>
                    <Text style={styles.desc}>{address}</Text>
                </View>
            });
            return (
                <View style={styles.viewListBack}>
                    {whatDiet}
                </View>
            );
        }
    }

    renderWeightData() {
        if (this.state.goal_code === 'WL' || this.state.goal_code === 'PP') {
            return (
                <View style={styles.viewCal}>
                    <View style={styles.column}>
                        <Text style={styles.mltextStyle}>
                            {this.state.present_weight}
                        </Text>
                        <Text style={styles.waterdranktextStyle}>
                            Present Weight
                                </Text>

                    </View>
                    <View style={styles.verLineView}>
                    </View>
                    <View style={styles.column}>
                        <Text style={styles.mltextStyle}>
                            {this.state.weight_target}
                        </Text>
                        <Text style={styles.waterdranktextStyle}>
                            Target Weight
                        </Text>
                    </View>

                </View>
            )
        }
        else if (this.state.goal_code === 'WG') {
            return (
                <View style={styles.viewCal}>
                    <View style={styles.column}>
                        <Text style={styles.mltextStyle}>
                            {this.state.present_weight}
                        </Text>
                        <Text style={styles.waterdranktextStyle}>
                            Present Weight
                                </Text>

                    </View>
                    <View style={styles.verLineView}>
                    </View>
                    <View style={styles.column}>
                        <Text style={styles.mltextStyle}>
                            {this.state.weight_target}
                        </Text>
                        <Text style={styles.waterdranktextStyle}>
                            Desired Weight
                        </Text>
                    </View>

                </View>
            )
        }
        else {
            return (
                <View style={styles.viewCal}>
                    <View style={styles.column}>
                        <Text style={styles.mltextStyle}>
                            {this.state.present_weight}
                        </Text>
                        <Text style={styles.waterdranktextStyle}>
                            Present Weight
                                </Text>

                    </View>
                    <View style={styles.verLineView}>
                    </View>
                    <View style={styles.column}>
                        <Text style={styles.textMaintain}>
                            Maintain Weight
                        </Text>
                    </View>
                </View>
            )
        }

    }

    render() {
        return (
            <ImageBackground
                resizeMode='stretch'
                source={require('../res/img_goal_success.png')} style={styles.containerStyle}>

                <ScrollView contentContainerStyle={{ paddingBottom: hp('10%') }}>

                    {/* arc chat and details    */}
                    <View style={styles.viewWalkChart}>

                        <Text style={{
                            padding: 15, textAlign: 'center',
                            // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0, 
                            paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
                        }}>
                            <Text style={styles.textQuotes}>{this.state.quotes}</Text>
                            {this.state.author
                                ? (
                                    <Text style={styles.textAuthor}> - {this.state.author}</Text>
                                )
                                : (
                                    <Text style={styles.textAuthor}></Text>
                                )}

                        </Text>
                        <Text style={styles.textStyle}>{this.state.sucessmsg}</Text>
                        <View style={styles.viewCircle}>
                            <View style={styles.viewProfile}>

                                {this.state.proImg
                                    ? (
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={{ uri: this.state.proImg }}
                                            style={styles.profileImage} />
                                    )
                                    : (
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_profile_gray.png')}
                                            style={styles.profileImage} />
                                    )}


                                <Text style={styles.textName}>{this.state.userName}</Text>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_app.png')}
                                    style={styles.appImageStyle}
                                />

                            </View>
                            <View style={{ height: 1, width: wp('80%'), backgroundColor: '#e9e9e9', }}></View>
                            <Text style={styles.calText}>
                                Goal - {this.state.goal_name}
                            </Text>
                            <Text style={styles.calText}>
                                Your daily calories goal is {this.state.calperday} calories per day
                                </Text>


                            <View style={styles.calViewDet}>
                                <View style={{
                                    width: 130, height: 130,
                                    position: 'absolute',
                                    top: 15,
                                    borderWidth: 3,
                                    borderRadius: 65,
                                    borderColor: '#e9e9e9',
                                    borderStyle: 'dotted',
                                }}>

                                </View>
                                <AnimatedCircularProgress
                                    size={160}
                                    width={7}
                                    fill={this.state.fill}
                                    tintColor="#8c52ff"
                                    arcSweepAngle={290}
                                    rotation={215}
                                    lineCap="round"
                                    backgroundColor="#e9e9e9">
                                    {
                                        (fill) => (
                                            <View>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_workout_black.png')}
                                                    style={{
                                                        width: 20,
                                                        height: 20,
                                                        alignSelf: 'center',
                                                    }}
                                                />
                                                <Text style={styles.calTitleBig}>{this.state.calperday}</Text>
                                                <Text style={styles.calDesc}>Cal of daily{'\n'} goals</Text>
                                            </View>

                                        )
                                    }
                                </AnimatedCircularProgress>

                                <TouchableOpacity style={styles.calViewDetails}>
                                    <Text style={styles.calDetail}></Text>
                                </TouchableOpacity>
                            </View>

                            {this.renderWeightData()}

                            {/* <View style={styles.viewCal}>
                                <View style={styles.column}>
                                    <Text style={styles.mltextStyle}>
                                        {this.state.present_weight}
                                    </Text>
                                    <Text style={styles.waterdranktextStyle}>
                                        Present Weight
                                </Text>

                                </View>
                                <View style={styles.verLineView}>
                                </View>

                                {this.state.goal_code === 'WL' || this.state.goal_code === 'PP'
                                    ? (
                                        <View style={styles.column}>
                                            <Text style={styles.mltextStyle}>
                                                {this.state.weight_target}
                                            </Text>
                                            <Text style={styles.waterdranktextStyle}>
                                                Target Weight
                                            </Text>
                                        </View>
                                    )
                                    : (
                                        <View style={styles.column}>
                                            <Text style={styles.textMaintain}>
                                                Maintain Weight
                                </Text>
                                        </View>
                                    )}

                            </View> */}

                            <View style={{ height: 20, }}>

                            </View>

                        </View>

                        <View style={{ height: 20, }}>
                        </View>
                        {this.renderList()}

                    </View>

                </ScrollView>

                <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                    <TouchableOpacity
                        onPress={() => this.onButtonPressed()}>
                        <Text style={styles.buttonText}>Next</Text>
                    </TouchableOpacity>
                </LinearGradient>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        height: '100%',
    },
    viewWalkChart: {
        width: '100%',
        flexDirection: 'column',
        padding: 20,
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 15,
        tintColor: '#ffffff',
        alignSelf: 'flex-start',
    },
    row: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'center',
    },
    column: {
        flexDirection: 'column',
        flex: 1,
        justifyContent: 'center',
    },
    textStyle: {
        fontSize: 13,
        fontWeight: '500',
        color: '#ffffff',
        marginTop: 5,
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 20,
        textAlign: 'center',
        fontFamily: 'Rubik-Regular',
    },
    textQuotes: {
        fontSize: 14,
        fontWeight: '500',
        color: '#ffffff',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    textAuthor: {
        fontSize: 13,
        fontWeight: '500',
        color: '#ffffff',
        textAlign: 'center',
        fontFamily: 'Rubik-Regular',
    },
    viewCircle: {
        flexDirection: 'column',
        width: wp('86%'),
        alignItems: 'center',
        alignSelf: 'center',
        borderRadius: 15,
        backgroundColor: '#ffffff',
        justifyContent: 'center'
    },
    viewProfile: {
        justifyContent: 'flex-start',
        flexDirection: 'row',
        alignItems: 'center',
        padding: 10,
    },
    profileImage: {
        width: 35,
        height: 35,
        aspectRatio: 1,
        backgroundColor: "#D8D8D8",
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: "#979797",
        borderRadius: 35 / 2,
        resizeMode: "cover",
        alignSelf: 'flex-start'
    },
    textName: {
        fontSize: 15,
        fontWeight: '500',
        marginLeft: 10,
        marginRight: 10,
        flex: 1,
        fontFamily: 'Rubik-Medium',
        color: '#282c37',
    },
    appImageStyle: {
        width: 70,
        height: 21,
    },
    calViewDet: {
        alignItems: 'center',
        alignSelf: 'center',
        marginTop: 20,
        marginBottom: 20,
        justifyContent: 'center'
    },
    calTitle: {
        fontSize: 20,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        textAlign: 'center',
        color: '#282c37',
    },
    calDesc: {
        fontSize: 12,
        fontWeight: '400',
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        textAlign: 'center',
    },
    calViewDetails: {
        width: 65,
        borderRadius: 100,
        borderStyle: 'solid',
        padding: 5,
        marginTop: -35,
        backgroundColor: '#ffffff',
    },
    calTitleBig: {
        fontSize: 28,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        textAlign: 'center',
        color: '#282c37',
    },
    viewCal: {
        flexDirection: 'row',
        justifyContent: 'center',
        margin: 15,
    },
    mltextStyle: {
        fontSize: 16,
        fontWeight: '500',
        color: '#2d3142',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    waterdranktextStyle: {
        fontSize: 12,
        fontWeight: '500',
        color: '#C3C1CE',
        textAlign: 'center',
        fontFamily: 'Rubik-Regular',
    },
    textMaintain: {
        fontSize: 12,
        fontWeight: '500',
        color: '#2d3142',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    calText: {
        fontSize: 14,
        fontWeight: '500',
        color: '#2d3142',
        marginTop: 10,
        marginLeft: 30,
        marginRight: 30,
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    verLineView: {
        borderWidth: 1,
        borderColor: '#ddd',
    },
    container: {
        width: wp('90%'),
        height: 150,
        borderColor: '#000000',
        alignSelf: 'center',
        backgroundColor: '#000000',
        justifyContent: 'center'
    },
    linearGradient: {
        width: '95%',
        height: 50,
        borderRadius: 25,
        justifyContent: 'center',
        alignSelf: 'center',
        backgroundColor: 'transparent',
        marginBottom: 25,
    },
    buttonText: {
        fontSize: 16,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    viewListBack: {
        flexDirection: 'column',
        width: wp('86%'),
        alignItems: 'center',
        alignSelf: 'center',
        borderRadius: 15,
        padding: 15,
        backgroundColor: '#ffffff',
        justifyContent: 'center'
    },
    viewListAllInner: {
        flexDirection: 'row',
        marginLeft: wp('2%'),
        justifyContent: 'flex-start',
        alignSelf: 'flex-start',
        alignItems: 'center',
        marginTop: 10,
    },
    desc: {
        fontSize: 12,
        marginLeft: 5,
        marginRight: 10,
        fontWeight: '400',
        color: '#2d3142',
        lineHeight: 15,
        textAlign: 'left',
        fontFamily: 'Rubik-Medium',
    },
    viewBlueDot: {
        width: 10,
        height: 10,
        backgroundColor: '#8c52ff',
        alignSelf: 'flex-start',
        marginTop: 3,
        borderRadius: 5,
    },


});

const mapStateToProps = state => {

    //   const { goals, trainerCodes } = state.procre;
    const loading = state.procre.loading;
    const error = state.procre.error;
    return { loading, error, };

};

export default withNavigationFocus(
    connect(
        mapStateToProps,
        {},
    )(Trainee_GoalCompleted),
);

// export default ProfileCreation;
