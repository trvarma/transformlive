import React, { Component } from 'react';
import { View, Image, TouchableOpacity, StyleSheet, Animated, BackHandler } from 'react-native';
import Video from 'react-native-video';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import MediaControls, { PLAYER_STATES } from 'react-native-media-controls';
import Icon from "react-native-vector-icons/FontAwesome";
import Orientation from 'react-native-orientation';
export default class VideoPlayerCard extends Component {
  videoPlayer;
  state =
    {
      videopaused: true,
      buffering: true,
      animated: new Animated.Value(0),
      currentTime: 0,
      duration: 0,
      isFullScreen: true,
      isLoading: true,
      paused: false,
      playerState: PLAYER_STATES.PLAYING,
      screenType: 'contain',
    };

  handleLoadStart = () => {
    this.triggerBufferAnimation();
  };

  handleFullscreen() {
    state.fullscreen
      ? Orientation.unlockAllOrientations()
      : Orientation.lockToLandscapeLeft();
  }

  triggerBufferAnimation = () => {
    this.loopingAnimation && this.loopingAnimation.stopAnimation();
    this.loopingAnimation = Animated.loop(
      Animated.timing(this.state.animated, {
        toValue: 1,
        duration: 350,
      })
    ).start();
  };

  handleBuffer = meta => {
    meta.isBuffering && this.triggerBufferAnimation()

    if (this.loopingAnimation && !meta.isBuffering) {
      this.loopingAnimation.stopAnimation();
    }
    this.setState({
      buffering: meta.isBuffering,
    });
  };

  videoLoadCalling = (meta) => {
    this.setState({ duration: meta.duration, isLoading: false })

    meta.isBuffering && this.triggerBufferAnimation()

    if (this.loopingAnimation && !meta.isBuffering) {
      this.loopingAnimation.stopAnimation();
    }
    this.setState({
      buffering: meta.isBuffering,
    });
  }

  onSeek = seek => {
    //Handler for change in seekbar
    this.videoPlayer.seek(seek);
  };

  onPaused = playerState => {
    //Handler for Video Pause
    this.setState({
      paused: !this.state.paused,
      playerState,
    });
  };

  onReplay = () => {
    //Handler for Replay
    this.setState({ playerState: PLAYER_STATES.PLAYING });
    this.videoPlayer.seek(0);
  };

  onProgress = (data) => {
    const { isLoading, playerState } = this.state;
    // Video Player will continue progress even if the video already ended
    if (!isLoading && playerState !== PLAYER_STATES.ENDED) {
      this.setState({ currentTime: data.currentTime });
    }
  };

  onLoadStart = data => this.setState({ isLoading: true });

  onEnd = () => {this.setState({videopaused: true}); this.setState({ playerState: PLAYER_STATES.PLAYING });
  this.videoPlayer.seek(0);}

  onError = () => alert('Oh! ', error);

  exitFullScreen = () => {
    alert('Exit full screen');
  };

  enterFullScreen = () => { };

  onFullScreen = () => {
    if (this.state.screenType == 'content')
      this.setState({ screenType: 'cover' });
    else this.setState({ screenType: 'content' });
  };
  renderToolbar = () => (
    <View>
      <Text> toolbar </Text>
    </View>
  );
  onSeeking = currentTime => this.setState({ currentTime });

  renderVideoOrThumbnail = () => {
    if (this.props.currentIndex === this.props.index) {
      if (!this.state.videopaused) {
        return (
          <View >
            <Video
                ref={videoPlayer => { this.videoPlayer = videoPlayer }}
                source={{ uri: this.props.videoURL }}
                rate={1.0}
                volume={1.0}
                isMuted={false}
                paused={this.state.paused}
                playInBackground={false}
                playWhenInactive={false}
                resizeMode="cover"
                onLoad = {this.videoLoadCalling.bind(this)}
                style={styles.container}
                onLoadStart={this.handleLoadStart}
                onBuffer={this.handleBuffer}
                onProgress={this.onProgress}
                onEnd={this.onEnd}
              />
              <MediaControls
                style={styles.container}
                duration={this.state.duration}
                isLoading={this.state.buffering}
                mainColor="grey"
                onPaused={this.onPaused}
                onReplay={this.onReplay}
                onSeek={this.onSeek}
                onSeeking={this.onSeeking}
                playerState={this.state.playerState}
                progress={this.state.currentTime}
              //   toolbar={this.renderToolbar()}
              />
              {/* <View style={styles.videoCover}>
              {buffering &&
                <Animated.View style={rotateStyle}>
                  <Icon name="circle-o-notch" size={30} color="white" />
                </Animated.View>}
            </View> */}
          </View>
        );
      }
      else {
        return (
          <View style={styles.playContainer}>
            {this.props.thumbnailURL
              ? (
                <Image
                  source={{ uri: this.props.thumbnailURL }}
                  style={styles.thumbNail}
                />
              )
              : (
                <Image
                  source={require('../res/ic_noimage.png')}
                  style={styles.thumbNail}
                />
              )
            }
            <Image source={require('../res/ic_play_white.png')}
              style={styles.playIcon} />
          </View>
        );
      }
    }
    else {
      return (
        <View style={styles.playContainer}>
          {this.props.thumbnailURL
            ? (
              <Image
                source={{ uri: this.props.thumbnailURL }}
                style={styles.thumbNail}
              />
            )
            : (
              <Image
                source={require('../res/ic_noimage.png')}
                style={styles.thumbNail}
              />
            )
          }
          <Image source={require('../res/ic_play_white.png')}
            style={styles.playIcon} />
        </View>
      );
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={() => this.setState({ videopaused: !this.state.videopaused })}>
          {this.renderVideoOrThumbnail()}
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    width: wp('90%'),
    height: hp('33%'),
    borderRadius: 10,
    backgroundColor: 'grey',
    overflow: 'hidden'
  },
  thumbNail: {
    width: wp('90%'),
    height: hp('33%'),
    borderRadius: 10,
    backgroundColor: 'black',
  },
  playIcon: {
    width: 40,
    height: 40,
    position: 'absolute',
    alignSelf: 'center',
    alignContent: 'center'
  },
  playContainer: {
    height: hp('33%'),
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  videoCover: {
    alignItems: "center",
    justifyContent: "center",
    position: "absolute",
    left: 0,
    top: 0,
    right: 0,
    bottom: 0,
    backgroundColor: "transparent",
  },
  buffering: {
    backgroundColor: "#000",
  },
  mediaPlayer: {
    width: '100%',
    height:'100%',
  position: 'absolute',
  top: 0,
  left: 0,
  bottom: 0,
  right: 0,
  backgroundColor: 'black',
},
});