import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    Image,
    TouchableOpacity,
    BackHandler,
    ActivityIndicator,
    Text,
} from 'react-native'
import { WebView } from 'react-native-webview';
import { Actions } from 'react-native-router-flux';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { allowFunction } from '../utils/ScreenshotUtils.js';
import LinearGradient from 'react-native-linear-gradient';
import RNExitApp from 'react-native-exit-app';
import { HomeDialog } from './common';

export default class Webview extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isSuccess: false,
            sucMsg: '',
            titMsg: '',
        };
    }

    async componentDidMount() {
        allowFunction();
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        if (this.props.flag && this.props.flag === 'newreg') {
            // RNExitApp.exitApp();
            this.setState({ isSuccess: true, titMsg: 'Are you sure!', sucMsg: 'Do you really want to exit?' });
        } else {
            Actions.pop();
        }
    }

    goBack = () => {
        this.webview.goBack();
    };
    
    ActivityIndicatorLoadingView() {
        //making a view to show to while loading the webpage
        return (
            <ActivityIndicator
                color="#8c52ff"
                size="large"
                style={styles.ActivityIndicatorStyle}
            />
        );
    }

    onButtonPressed() {

        Actions.profile({ type: 'reset', url : this.props.url });

    }

    async onDeny() {
        this.setState({ isSuccess: false, sucMsg: '', titMsg: '' });
    }

    async onSuccess() {

        RNExitApp.exitApp();
        this.setState({ isSuccess: false, sucMsg: '', titMsg: '' });

    }

    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity style={styles.viewTop}
                    onPress={() => this.onBackPressed()}>
                    <Image
                        source={require('../res/ic_back.png')}
                        style={styles.backImageStyle}
                    />
                </TouchableOpacity>

                <WebView
                    style={{ width: wp('100%'), height: hp('100%'), position: 'absolute', top: 0 }}
                    javaScriptEnabled={true}
                    domStorageEnabled={true}
                    onNavigationStateChange={this.log}
                    renderLoading={this.ActivityIndicatorLoadingView}
                    startInLoadingState={true}
                    source={{ uri: this.props.url }} />

                {
                    this.props.flag && this.props.flag === 'newreg' ?
                        (
                            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                                <TouchableOpacity
                                    onPress={() => this.onButtonPressed()}>
                                    <Text style={styles.buttonText}>Continue</Text>
                                </TouchableOpacity>
                            </LinearGradient>
                        )
                        : (
                            <View />
                        )
                }

                <HomeDialog
                    visible={this.state.isSuccess}
                    title={this.state.titMsg}
                    desc={this.state.sucMsg}
                    onAccept={this.onSuccess.bind(this)}
                    onDecline={this.onDeny.bind(this)}
                    no='No'
                    yes='Yes' />


            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
        flex: 1,
        backgroundColor: '#ffffff',
    },
    viewTop: {
        flexDirection: 'column',
        position: 'absolute',
        marginTop: wp('12%'),
        marginLeft: 20,
        alignItems: 'center',
        padding: 5,
        justifyContent: 'center',
        width: 35,
        height: 35,
        borderRadius: 35 / 2,
        backgroundColor: 'white',
        zIndex: 100,
        shadowColor: 'rgba(0, 0, 0, 0.08)',
        shadowOffset: { width: 5, height: 0 },
        shadowOpacity: 0.25,
        shadowRadius: 10,
        elevation: 6,
    },
    backImageStyle: {
        width: 19,
        height: 16,
        alignSelf: 'center',
    },
    view: {
        width: "100%",
        padding: 20,
        backgroundColor: 'white',

    },
    ActivityIndicatorStyle: {
        flex: 1,
        justifyContent: 'center',
    },
    linearGradient: {
        width: wp('90%'),
        height: 50,
        borderRadius: 25,
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: 20,
        marginBottom: 15,
        position: 'absolute', bottom: 0
    },
    buttonText: {
        fontSize: 16,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
})