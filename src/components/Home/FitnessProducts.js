import React from 'react';
import { Text, View, FlatList, TouchableOpacity, Image, StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';

const FitnessProducts = (props) => {
    
    const renderEcommerce = (data) => {
        try {
            if (data.products.length > 0) {
                return (
                    <View>
                        <View style={styles.vHeaderTitMore}>
                            <Text style={styles.textPlan}>{'Top Sellers'.toUpperCase()}</Text>
                            <TouchableOpacity
                                onPress={() => {
                                    Actions.webview({ url: data.shoopingURL });
                                }
                                }>
                                <Text style={styles.textAdd}>{'+ MORE'.toUpperCase()}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                );
            }
        } catch (error) {
            console.log("Error in renderEcommerce : ", error);
        }
    };

    const FlatListItemSeparatorWorkouts = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: 10,
                    backgroundColor: "transparent",
                }}
            />
        );
    }

    const renderEcommerceList = (data) => {
        try {
            if (data.products.length > 0) {
                return (
                    <FlatList
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        data={data.products}
                        // keyExtractor={item => item.pg_id}
                        keyExtractor={(item, index) => "products" + index}
                        contentContainerStyle={{ margin: 5, paddingRight: 20 }}
                        ItemSeparatorComponent={FlatListItemSeparatorWorkouts}
                        renderItem={({ item }) => {
                            return (<TouchableOpacity key={item.id} style={styles.containerFoodStyle} onPress={() => {
                                // LogUtils.firebaseEventLog('click', {
                                //   p_id: 215,
                                //   p_category: 'Home',
                                //   p_name: `Home->TopSellersView-${item.title}`,
                                // });
                                Actions.webview({ url: item.product_url })
                            }}>
                                <View style={{ flexDirection: 'column', }}>
                                    <Image
                                        progressiveRenderingEnabled={true}
                                        resizeMethod="resize"
                                        source={{ uri: item.image_url }}
                                        // source={`${item.image}`}
                                        style={styles.imgVeg}
                                    />
                                    <View style={{ padding: 5, height: 50 }}>
                                        <Text numberOfLines={2} ellipsizeMode='tail' style={styles.vegText}>{`${item.title}`}</Text>
                                        <Text style={styles.priceText}>Rs {`${item.price}`}</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>)
                        }} />
                );
            }
        } catch (error) {
            console.log("Error in renderEcommerceList", error);
        }
    }

    return (
        <View>
            {renderEcommerce(props)}
            {renderEcommerceList(props)}
        </View>
    );
}

const styles = StyleSheet.create({
    vHeaderTitMore: {
        width: wp('94%'),
        flexDirection: 'row',
        paddingTop: 15,
        paddingBottom: 15,
    },
    textPlan: {
        lineHeight: 18,
        color: '#282c37',
        fontFamily: 'Rubik-Medium',
        fontSize: 14,
        flex: 1,
        alignSelf: "center",
        fontWeight: '500',
    },
    textAdd: {
        color: '#8c52ff',//color: '#8c52ff',
        fontFamily: 'Rubik-Medium',
        alignSelf: "flex-end",
        fontSize: 11,
        fontWeight: '500',
        lineHeight: 18,
    },
    containerFoodStyle: {
        width: wp('27%'),
        marginTop: hp('1%'),
        marginBottom: hp('2%'),
        backgroundColor: '#ffffff',
        flexDirection: 'row',
        // borderColor: '#ddd',
        borderRadius: 5,
        position: 'relative',
        alignSelf: 'center',
        // shadowColor: "#ddd",
        // shadowOffset: {
        //   width: 0,
        //   height: 2,
        // },
        //shadowOpacity: 0.25,
        //shadowRadius: 3.84,
        elevation: 3,
        marginLeft: 0.5,
        marginRight: 0.5,
    },
    imgVeg: {
        width: wp('27%'),
        height: 100,
        alignSelf: 'center',
        position: 'relative',
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5
    },
    vegText: {
        fontSize: 10,
        fontWeight: '500',
        color: '#2d3142',
        letterSpacing: 0.23,
        fontFamily: 'Rubik-Regular',
    },
    priceText: {
        fontSize: 11,
        fontWeight: '400',
        color: '#2d3142',
        letterSpacing: 0.23,
        fontFamily: 'Rubik-Medium',
    },
})

export default FitnessProducts;