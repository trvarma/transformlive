import React from 'react';
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';

const NewAppointments = (props) => {

    const renderAppointmentsNew = (data) => {
        try {
            if (data.isShowAppt === 1) {
                if (data.appointments.piad_arr && data.appointments.piad_arr.length) {
                    let arrpaidApps = data.appointments.piad_arr.map((item, i) => {
                        return <TouchableOpacity key={item.ua_id} style={{ flexDirection: 'column', flex: 1, }}
                            onPress={() => Actions.appointments({
                                plantype: data.appointments.plan_type,
                                premiumuser: data.appointments.premium_user,
                                homeObj: data.freeTrialInitial
                            })}>
                            <View style={{ flexDirection: 'column', padding: 15 }}>

                                <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center', }}>
                                    <View style={styles.viewBlueDot}></View>
                                    <Text style={styles.textChalName}>{item.trainer_name}</Text>
                                </View>

                                <View style={{ flexDirection: 'column', marginTop: 1 }}>
                                    <Text style={styles.textTitle}>{item.sch_date}, {item.sch_time}</Text>
                                    <Text style={styles.textCertification}>{item.certification}</Text>
                                </View>

                            </View>
                            <View style={{ height: 0.7, backgroundColor: '#e9e9e9', }}></View>
                        </TouchableOpacity>
                    });

                    if (data.appointments.premium_user === 1) {
                        return (
                            <View style={{ flexDirection: 'column', }}>

                                <View style={styles.vHeaderTitMore}>
                                    <Text style={styles.textPlan}>{'YOUR APPOINTMENTS'.toUpperCase()}</Text>
                                    <TouchableOpacity
                                        onPress={() => Actions.appointments({
                                            plantype: data.appointments.plan_type,
                                            premiumuser: data.appointments.premium_user,
                                            freeTrial: data.appointments.show_free_trial,
                                            homeObj: data.freeTrialInitial
                                        })}>
                                        <Text style={styles.textAdd}>{'+ BOOK'.toUpperCase()}</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ height: 0.7, backgroundColor: '#e9e9e9' }}></View>
                                {arrpaidApps}
                            </View>
                        );
                    } else {
                        return (
                            <View style={{ flexDirection: 'column', }}>
                                <View style={styles.vHeaderTitMore}>
                                    <Text style={styles.textPlan}>{'APPOINTMENTS'.toUpperCase()}</Text>
                                </View>
                                <View style={{ height: 0.7, backgroundColor: '#e9e9e9' }}></View>
                                {arrpaidApps}
                            </View>
                        );
                    }
                } else {
                    // if (data.appointments.premium_user === 1) {
                    //   return (
                    //     <View style={{ flexDirection: 'column', }}>
                    //       <View style={styles.vHeaderTitMore}>
                    //         <Text style={styles.textPlan}>{'Appointments'.toUpperCase()}</Text>
                    //         <TouchableOpacity
                    //           onPress={() => Actions.appointments({ plantype: data.appointments.plan_type, premiumuser: data.appointments.premium_user, freeTrial: data.appointments.show_free_trial, homeObj: this.state.freeTrialInitialObj })}>
                    //           <Text style={styles.textAdd}>{'+ Book'.toUpperCase()}</Text>
                    //         </TouchableOpacity>
                    //       </View>
                    //       <View style={{ height: 1, backgroundColor: '#e9e9e9', }}></View>
                    //       <View style={{ flexDirection: 'row' }}>
                    //         <Text style={styles.textNodata}>{data.appointments.appoint_msg}</Text>
                    //       </View>
                    //     </View>
                    //   );

                    // } else {

                    //   return (
                    //     <View style={{ flexDirection: 'column', }}>
                    //       <View style={styles.vHeaderTitMore}>
                    //         <Text style={styles.textPlan}>{'BOOK AN APPOINTMENT'.toUpperCase()}</Text>
                    //         <TouchableOpacity
                    //           onPress={() => {
                    //             Actions.myOrders({ from: 'premiumday', appText: 'You are not subscribed to any premium plans. Please subscribe to book appointments.' });
                    //           }}>
                    //           <Text style={styles.textAdd}>{'+ ADD'.toUpperCase()}</Text>
                    //         </TouchableOpacity>
                    //       </View>
                    //       <View style={{ height: 1, backgroundColor: '#e9e9e9', }}></View>
                    //       <View style={{ flexDirection: 'row' }}>
                    //         <Text style={styles.textNodata}>{data.appointments.appoint_msg}</Text>
                    //       </View>
                    //     </View>
                    //   );

                    // }
                    return (
                        <View />
                    );
                }
            }
        } catch (error) {
            console.log(error);
        }
    }

    return (
        <View>
            {renderAppointmentsNew(props)}
        </View>
    );
}

const styles = StyleSheet.create({
    vHeaderTitMore: {
        width: wp('94%'),
        flexDirection: 'row',
        paddingTop: 15,
        paddingBottom: 15,
    },
    textPlan: {
        lineHeight: 18,
        color: '#282c37',
        fontFamily: 'Rubik-Medium',
        fontSize: 14,
        flex: 1,
        alignSelf: "center",
        fontWeight: '500',
    },
    textAdd: {
        color: '#8c52ff',//color: '#8c52ff',
        fontFamily: 'Rubik-Medium',
        alignSelf: "flex-end",
        fontSize: 11,
        fontWeight: '500',
        lineHeight: 18,
    },
    viewBlueDot: {
        width: 10,
        height: 10,
        backgroundColor: '#8c52ff',
        alignSelf: 'flex-start',
        marginRight: 10,
        marginTop: 5,
        borderRadius: 5,
    },
    textChalName: {
        fontSize: 14,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        color: '#282c37',
        marginRight: 15,
    },
    textTitle: {
        fontSize: 11,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        lineHeight: 18,
    },
    textCertification: {
        fontSize: 12,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        color: '#282c37',
        lineHeight: 18,
    },
})

export default NewAppointments;