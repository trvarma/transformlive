import React from 'react';
import { Text, View, FlatList, TouchableOpacity, Image, StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import LogUtils from '../../utils/LogUtils';

function isEmpty(obj) {
    try {
        for (var key in obj) {
            if (obj.hasOwnProperty(key))
                return false;
        }
        return true;
    } catch (error) {
        console.log(error)
    }
}

const Challenges = (props) => {
    const renderChallengesHeader = (data) => {
        try {
            if (!isEmpty(data.trChnlg)) {
                if (data.trChnlg.challenge && data.trChnlg.challenge.length) {
                    return (
                        <View>
                            <View style={styles.vHeaderTitMore}>
                                <Text style={styles.textPlan}>{'Challenges'.toUpperCase()}</Text>
                                <TouchableOpacity
                                    onPress={() => {
                                        LogUtils.firebaseEventLog('click', {
                                            p_id: 209,
                                            p_category: 'Home',
                                            p_name: 'Home->Challenges',
                                        });
                                        Actions.traChallenges();
                                    }}>
                                    <Text style={styles.textAdd}>{'+ MORE'.toUpperCase()}</Text>
                                </TouchableOpacity>
                            </View>
                            {/* <View style={{ marginBottom: 10, height: 1, backgroundColor: '#e9e9e9' }}></View> */}
                        </View>
                    );
                }
                else {
                    return (
                        <View>
                            <View style={styles.vHeaderTitMore}>

                                <Text style={styles.textPlan1}>
                                    {'TAKE A CHALLENGE'.toUpperCase()}
                                </Text>

                                <View style={{ flex: 1 }}></View>
                                <TouchableOpacity
                                    onPress={() => {
                                        LogUtils.firebaseEventLog('click', {
                                            p_id: 209,
                                            p_category: 'Home',
                                            p_name: 'Home->Challenges',
                                        });
                                        Actions.traChallenges();
                                    }}>
                                    <Text style={styles.textAdd}>{'+ MORE'.toUpperCase()}</Text>
                                </TouchableOpacity>
                            </View>
                            {/* <View style={{ marginBottom: 10, height: 1, backgroundColor: '#e9e9e9' }}></View> */}
                        </View>
                    );
                }
            }

        } catch (error) {
            console.log(error)
        }
    }

    const renderChallengesList = (data) => {
        try {
            //if (data.loading === false && data.refreshing === false) {
                if (!isEmpty(data.trChnlg)) {
                    if (data.trChnlg.challenge && data.trChnlg.challenge.length) {
                        return (
                            <FlatList
                                data={data.trChnlg.challenge}
                                //extraData={this.state}
                                //style={{}}
                                // keyExtractor={item => item.c_id}
                                keyExtractor={(item, index) => "challenge" + index}
                                showsVerticalScrollIndicator={false}
                                scrollEnabled={false}
                                contentContainerStyle={{ paddingLeft: 5, paddingRight: 5, paddingBottom: 5, }}
                                ItemSeparatorComponent={FlatListItemSeparatorCallenges}
                                renderItem={({ item }) => {
                                    return <TouchableOpacity key={item.c_id} style={{ flexDirection: 'column', height: 110, }} onPress={() => {
                                        if (data.open) {
                                            this.props.closeChallenge(false);
                                        }
                                        else {
                                            // Actions.appSuccess();
                                            // if (item.is_accept === 1) {
                                            //   Actions.accChallenge({ cid: item.c_id, uc_id: item.uc_id })
                                            // } else {
                                            //   Actions.traChallenges({ cid: item.c_id })
                                            //   // Actions.challengedetails({ cid: item.c_id })
                                            // }
                                            Actions.traChallenges({ cid: item.c_id });
                                        }

                                    }}>
                                        <View style={{ flexDirection: 'row', flex: 1, }}>
                                            <Image
                                                progressiveRenderingEnabled={true}
                                                resizeMethod="resize"
                                                source={{ uri: item.home_image }}
                                                style={{
                                                    width: 110,
                                                    height: 110,
                                                    borderRadius: 6,
                                                    alignSelf: 'center',
                                                    alignContent: 'center',
                                                }} />
                                            <View style={{ flexDirection: 'column', flex: 1, padding: 15 }}>
                                                <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                                                <View style={styles.viewBlueDot}></View>
                                                    {/* <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        source={require('../../res/ic_blue_dot.png')}
                                                        style={{
                                                            width: 10,
                                                            height: 10,
                                                            marginRight: 10,
                                                            alignSelf: 'center',
                                                        }}
                                                    /> */}
                                                    <Text numberOfLines={1} style={styles.textChalName}>{item.title}</Text>
                                                </View>

                                                <View style={{ flexDirection: 'row', marginTop: 1 }}>
                                                    <Text style={styles.textTitle}>{item.from_date}</Text>
                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        source={require('../../res/ic_arr_right.png')}
                                                        style={{
                                                            width: 14,
                                                            alignSelf: 'center',
                                                            marginRight: 10,
                                                            marginLeft: 10,
                                                            tintColor: '#6d819c',
                                                            height: 12,
                                                        }} />
                                                    <Text style={styles.textTitle}>{item.to_date}</Text>
                                                </View>

                                                {renderChallengesProfiles(item)}

                                            </View>
                                        </View>

                                    </TouchableOpacity>
                                }} />
                        );
                    }
                    else {
                        if (Array.isArray(data.trChnlg.top_challenge) && data.trChnlg.top_challenge.length) {

                            return (
                                <FlatList
                                    data={data.trChnlg.top_challenge}
                                    //extraData={this.state}
                                    style={{}}
                                    // keyExtractor={item => item.c_id}
                                    keyExtractor={(item, index) => "top_challenge" + index}
                                    showsVerticalScrollIndicator={false}
                                    scrollEnabled={false}
                                    contentContainerStyle={{ paddingLeft: 5, paddingRight: 5, paddingBottom: 5, }}
                                    ItemSeparatorComponent={FlatListItemSeparatorCallenges}
                                    renderItem={({ item }) => {
                                        return <TouchableOpacity key={item.c_id} style={{ flexDirection: 'column', height: 110, }} onPress={() => {
                                            if (data.open) {
                                                this.props.closeChallenge(false);
                                            }
                                            else {
                                                Actions.traChallenges({ cid: item.c_id })
                                            }
                                        }}>
                                            <View style={{ flexDirection: 'row', flex: 1, }}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={{ uri: item.home_image }}
                                                    style={{
                                                        width: 110,
                                                        height: 110,
                                                        borderRadius: 6,
                                                        alignSelf: 'center',
                                                        alignContent: 'center',
                                                    }} />
                                                <View style={{ flexDirection: 'column', flex: 1, padding: 15 }}>
                                                    <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                                                        <Image
                                                            progressiveRenderingEnabled={true}
                                                            resizeMethod="resize"
                                                            source={require('../../res/ic_blue_dot.png')}
                                                            style={{
                                                                width: 10,
                                                                height: 10,
                                                                marginRight: 10,
                                                                alignSelf: 'center',
                                                            }}
                                                        />
                                                        <Text numberOfLines={1} style={styles.textChalName}>{item.title}</Text>
                                                    </View>

                                                    <View style={{ flexDirection: 'row', marginTop: 1 }}>
                                                        <Text style={styles.textTitle}>{item.from_date}</Text>
                                                        <Image
                                                            progressiveRenderingEnabled={true}
                                                            resizeMethod="resize"
                                                            source={require('../../res/ic_arr_right.png')}
                                                            style={{
                                                                width: 14,
                                                                alignSelf: 'center',
                                                                marginRight: 10,
                                                                marginLeft: 10,
                                                                tintColor: '#6d819c',
                                                                height: 12,
                                                            }} />
                                                        <Text style={styles.textTitle}>{item.to_date}</Text>
                                                    </View>

                                                    {renderChallengesProfiles(item)}

                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                    }} />
                            );
                        } else {
                            return (
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={styles.textNodata}>{data.trChnlg.challenge_nodata_msg}</Text>
                                </View>
                            );
                        }
                    }
                }
           // }
        } catch (error) {
            console.log(error)
        }
    }

    const FlatListItemSeparatorCallenges = () => {
        return (
            <View style={{ height: 3, }}></View>
        );
    }

    const renderChallengesProfiles = (item) => {
        try {
            if (Array.isArray(item.user_videos) && item.user_videos.length) {
                if (item.user_videos.length === 1) {
                    let arrProfiles = item.user_videos.map((item, i) => {
                        return <View key={i} style={{ flexDirection: 'row', marginTop: 7, alignItems: 'center' }}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={{ uri: item.profile_img }}
                                style={styles.chaProfile} />
                        </View>
                    });
                    return (
                        <View>
                            {arrProfiles}
                        </View>
                    );
                }
                else if (item.user_videos.length === 2) {
                    let arrProfiles = item.user_videos.map((item, i) => {
                        return <View key={i} style={{ flexDirection: 'row', marginTop: 7, alignItems: 'center' }}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={{ uri: item.profile_img }}
                                style={styles.chaProfile} />
                        </View>
                    });
                    return (
                        <View style={{ flexDirection: 'row', marginTop: 7, alignItems: 'center' }}>
                            {arrProfiles}
                            {item.chlng_cnt !== 0
                                ?
                                (
                                    <View style={styles.chaMore}>
                                        <Text style={styles.chaMoreText}>+{`${item.chlng_cnt}`}</Text>
                                    </View>
                                )
                                :
                                (
                                    <View />
                                )
                            }
                        </View>
                    );
                }
            }
            else {
                return (
                    <View />
                );
            }
        } catch (error) {
            console.log(error)
        }
    }

    return (
        <View>
            {renderChallengesHeader(props)}
            {renderChallengesList(props)}
        </View>
    );
}

const styles = StyleSheet.create({
    vHeaderTitMore: {
        width: wp('94%'),
        flexDirection: 'row',
        paddingTop: 15,
        paddingBottom: 15,
    },
    textPlan: {
        lineHeight: 18,
        color: '#282c37',
        fontFamily: 'Rubik-Medium',
        fontSize: 14,
        flex: 1,
        alignSelf: "center",
        fontWeight: '500',
    },
    textAdd: {
        color: '#8c52ff', //color: '#8c52ff',
        fontFamily: 'Rubik-Medium',
        alignSelf: "flex-end",
        fontSize: 11,
        fontWeight: '500',
        lineHeight: 18,
    },
    containerFoodStyle: {
        width: wp('27%'),
        marginTop: hp('1%'),
        marginBottom: hp('2%'),
        backgroundColor: '#ffffff',
        flexDirection: 'row',
        // borderColor: '#ddd',
        borderRadius: 5,
        position: 'relative',
        alignSelf: 'center',
        // shadowColor: "#ddd",
        // shadowOffset: {
        //   width: 0,
        //   height: 2,
        // },
        //shadowOpacity: 0.25,
        //shadowRadius: 3.84,
        elevation: 3,
        marginLeft: 0.5,
        marginRight: 0.5,
    },
    imgVeg: {
        width: wp('27%'),
        height: 100,
        alignSelf: 'center',
        position: 'relative',
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5
    },
    vegText: {
        fontSize: 10,
        fontWeight: '500',
        color: '#2d3142',
        letterSpacing: 0.23,
        fontFamily: 'Rubik-Regular',
    },
    priceText: {
        fontSize: 11,
        fontWeight: '400',
        color: '#2d3142',
        letterSpacing: 0.23,
        fontFamily: 'Rubik-Medium',
    },
    textPlan1: {
        lineHeight: 18,
        color: '#282c37',
        fontFamily: 'Rubik-Medium',
        fontSize: 14,
        textAlign: 'left',
        alignSelf: "center",
        fontWeight: '500',
    },
    textChalName: {
        fontSize: 14,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        color: '#282c37',
        marginRight: 15,
    },
    textNodata: {
        width: wp('90%'),
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        padding: 10,
        marginTop: 10,
        marginBottom: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
    },
    viewBlueDot: {
        width: 10,
        height: 10,
        backgroundColor: '#8c52ff',
        alignSelf: 'flex-start',
        marginRight: 10,
        marginTop: 5,
        borderRadius: 5,
    },
    chaProfile: {
        width: 23,
        height: 23,
        aspectRatio: 1,
        backgroundColor: "#D8D8D8",
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: "#979797",
        borderRadius: 23 / 2,
        marginRight: 4,
        resizeMode: "cover",
        justifyContent: 'center',
    },
    chaMore: {
        flexDirection: 'row',
        backgroundColor: '#979797',
        borderRadius: 15,
        paddingLeft: 8,
        paddingTop: 4,
        marginTop: 5,
        alignSelf: 'center',
        paddingBottom: 4,
        paddingRight: 8,
    },
    chaMoreText: {
        color: '#ffffff',
        fontSize: 12,
        textAlign: 'center',
        alignSelf: 'center',
        fontFamily: 'Rubik-Regular',
    },
    textTitle: {
        fontSize: 11,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        lineHeight: 18,
    },
})

export default Challenges;