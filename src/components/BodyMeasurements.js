
import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    ImageBackground,
    StatusBar,
    FlatList,
    Dimensions,
    Image,
    BackHandler,
    ScrollView
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage';
const GAUGE_WIDTH = Math.floor(Dimensions.get('window').width - 25);
import NetInfo from "@react-native-community/netinfo";
import { BASE_URL } from '../actions/types';
import LogUtils from '../utils/LogUtils.js';
import LineGauge from './LineGuage.js';
import { Loader, CustomDialog, NoInternet } from './common';
let subCatIds = '', subcatVals = '';
import { allowFunction } from '../utils/ScreenshotUtils.js';

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}


class BodyMeasurements extends Component {
    constructor(props) {
        super(props);

        this.state = {
            sliderValue: 0,
            maxSliderValue: 0,
            arrSubCatList: [],
            noDataMsg: '',
            loading: false,
            isAlert: false,
            alertMsg: '',
            goalSubId: 0,
            tarWeight: '',
            goalCode: '',
            goalName: '',
            mBMTitle: 'What are your body measurements?',
            measurementvalue: 0,
            isSuccess: false,
            sucTit: '',
            sucMsg: '',
            isNoInternet: false,
            bodyMsgImgUrl: '',
        };
    }

    async componentDidMount() {
        allowFunction();
        StatusBar.setHidden(true);

        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isNoInternet: false });
                this.getBodyMeasurements();
            }
            else {
                this.setState({ isNoInternet: true });
            }
        });
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        Actions.pop();
    }



    async getBodyMeasurements() {
        this.setState({loading: true});
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/master/bodymesurements`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                // body: JSON.stringify({
                //     g_id: this.props.goalId,
                // }),

            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {

                    if (data.data) {
                        data.data.map((item) => {
                            item.check = false;
                        })
                        this.setState({ loading: false, arrSubCatList: data.data, bodyMsgImgUrl: data.body_img });
                    }
                    else {
                        this.setState({ loading: false, noDataMsg: 'No data available' });
                    }
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
            });
    }



    FlatListItemSeparator = () => {
        return (
            <View
                style={{
                    height: 0.5,
                    width: wp('100%'),
                    backgroundColor: '#dddddd',
                }}
            />
        );
    }

    async onAccept() {
        this.setState({ alertMsg: '' });
        this.setState({ isAlert: false });
    }

    async onButtonPressed() {

        if (this.state.arrSubCatList) {
            subCatIds = '';
            subcatVals = '';
            this.state.arrSubCatList.map((item) => {

                if (subCatIds.length === 0) {
                    subCatIds = subCatIds + item.id;
                }
                else {
                    subCatIds = subCatIds + ',' + item.id;
                }

                if (subcatVals.length === 0) {
                    subcatVals = subcatVals + item.avg_val;
                }
                else {
                    subcatVals = subcatVals + ',' + item.avg_val;
                }
            })
            LogUtils.infoLog1('subCatIds', subCatIds);
            LogUtils.infoLog1('subcatVals', subcatVals);

            this.saveBodyMeasurements();


        }


    }

    async saveBodyMeasurements() {
        this.setState({ loading: false });
        let token = await AsyncStorage.getItem('token');
        LogUtils.infoLog(JSON.stringify({

            mg_id: subCatIds,
            mg_val: subcatVals,
        }));
        fetch(
            `${BASE_URL}/user/savebodymesurement`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({

                    mg_id: subCatIds,
                    mg_val: subcatVals,

                }),

            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {

                    this.setState({ loading: false, isSuccess: true, sucTit: data.title, sucMsg: data.message });

                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
            });
    }

    press = (hey) => {
        this.state.arrSubCatList.map((item) => {
            if (item.id === hey.id) {
                item.check = true;
                this.setState({ goalSubId: item.id });
            }
            else {
                item.check = false;
            }
        })

    }

    pressBodyMeasurement = (hey) => {
        this.state.arrSubCatList.map((item) => {
            if (item.id === hey.id) {
                item.check = true;
                this.setState({ measurementvalue: item.avg_val });
            }
            else {
                item.check = false;
            }
        })

    }

    pressBodyMsnt = (hey, isPlusMinus) => {
        const tempArray = [...this.state.arrSubCatList];

        tempArray.map((item) => {
            if (item.id === hey.id) {
                if (isPlusMinus === 'plus') {
                    item.avg_val = item.avg_val + 1;
                }
                else {
                    if (item.avg_val !== 0) {
                        item.avg_val = item.avg_val - 1;
                    }
                }
            }
        });

        this.setState(() => {
            return {
                arrSubCatList: tempArray,
            };
        });

    }

    handleOnchange = (hey, value) => {
        LogUtils.infoLog1('Value', value);
        const tempArray = [...this.state.arrSubCatList];

        tempArray.map((item) => {
            if (item.id === hey.id) {
                item.avg_val = value;

            }
        });


        this.setState(() => {
            return {
                arrSubCatList: tempArray,
            };
        });


    }

    renderListAndDet() {
        return (
            <FlatList
                contentContainerStyle={{ paddingBottom: hp('20%') }}
                data={this.state.arrSubCatList}
                keyExtractor={item => item.id}
                extraData={this.state}
                showsVerticalScrollIndicator={false}
                ItemSeparatorComponent={this.FlatListItemSeparator}
                renderItem={({ item }) => {
                    return <View key={item.id} >
                        {item.check
                            ? (
                                <View style={{ flexDirection: 'column', alignItems: 'center', padding: 10, backgroundColor: '#F9F9F9' }}>
                                    <Image
                                        progressiveRenderingEnabled={true}
                                        resizeMethod="resize"
                                        source={{ uri: item.image }}
                                        style={styles.profileImage}
                                    />
                                    <Text style={{
                                        alignSelf: 'center',
                                        fontSize: 12,
                                        fontWeight: '400',
                                        color: '#2d3142',
                                        letterSpacing: 0.23,
                                        fontFamily: 'Rubik-Regular',
                                        marginBottom: 10,
                                        marginTop: 10,

                                    }}>{`${item.name}`}</Text>
                                    <View style={{ flexDirection: 'row', alignSelf: 'center', marginLeft: 25 }}>
                                        <Text style={styles.txtAvgVal}>{`${item.avg_val}`}</Text>
                                        <Text style={styles.txtMeasurement}>CM</Text>
                                    </View>
                                    <LineGauge min={0} max={200} value={`${item.avg_val}`} onChange={(value) =>
                                        //  this.setState({measurementvalue : value})
                                        //  LogUtils.infoLog1('value',value)
                                        this.handleOnchange(item, value)
                                    } />
                                </View>


                            ) : (

                                <TouchableOpacity style={styles.containeMuscleGain}
                                    onPress={() => {
                                        this.pressBodyMeasurement(item)
                                    }}>
                                    <View style={{ width: wp('68%'), flexDirection: 'row', alignItems: 'center' }}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={{ uri: item.image }}
                                            style={styles.profileImage}
                                        />
                                        <View >
                                            <Text style={styles.countryText1}>{`${item.name}`}</Text>
                                        </View>
                                    </View>


                                    <View style={styles.viewPlusMinus}>

                                        <Text style={styles.txtAvgVal}>{`${item.avg_val}`}</Text>
                                        <Text style={styles.txtMeasurement}>CM</Text>

                                    </View>
                                </TouchableOpacity>

                            )
                        }
                    </View>

                }} />


        )
    }


    async onSuccess() {
        this.setState({ isSuccess: false, sucTit: '', sucMsg: '' });
        this.getBodyMeasurements();
        //Actions.myProfile();
    }

    async onRetry() {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isNoInternet: false });
                this.getBodyMeasurements();
            }
            else {
                this.setState({ isNoInternet: true });
            }
        });
    }

    render() {

        return (
            <ImageBackground source={require('../res/app_bg.png')} style={styles.containerStyle}>
                <ScrollView style={styles.containerStyle}>
                    <Loader loading={this.state.loading} />
                    <NoInternet
                        image={require('../res/img_nointernet.png')}
                        loading={this.state.isNoInternet}
                        onRetry={this.onRetry.bind(this)} />
                    <View style={{ position: 'absolute', zIndex: 111, right: 0 }}>
                        <TouchableOpacity
                            style={{ width: 40, height: 40, marginTop: 20 }}
                            onPress={() => Actions.bodymeasureHome()}>
                            <Image
                                source={require('../res/ic_info_blueround.png')}
                                style={styles.chartImageStyle}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={{ position: 'absolute', zIndex: 111, }}>
                        <TouchableOpacity
                            style={{ width: 40, height: 40, marginTop: 20 }}
                            onPress={() => this.onBackPressed()}>
                            <Image
                                source={require('../res/ic_back.png')}
                                style={styles.backImageStyle}
                            />
                        </TouchableOpacity>
                    </View>

                    <View style={styles.optionInnerContainer}>
                        <Image
                            progressiveRenderingEnabled={true}
                            resizeMethod="resize"
                            source={require('../res/ic_app.png')}
                            style={styles.appImageStyle}
                        />
                    </View>

                    <View style={styles.containericonStyle}>

                        <Text style={styles.subtextOneStyle}>{this.state.mBMTitle}</Text>
                        {this.state.bodyMsgImgUrl
                            ?
                            (
                                <Image
                                    source={{ uri: this.state.bodyMsgImgUrl }}
                                    style={styles.personImageStyle} />
                            )
                            :
                            (
                                <View></View>
                            )
                        }

                        {this.renderListAndDet()}

                    </View>

                    <CustomDialog
                        visible={this.state.isAlert}
                        title='Alert'
                        desc={this.state.alertMsg}
                        onAccept={this.onAccept.bind(this)}
                        no=''
                        yes='Ok' />

                    <CustomDialog
                        visible={this.state.isSuccess}
                        title={this.state.sucTit}
                        desc={this.state.sucMsg}
                        onAccept={this.onSuccess.bind(this)}
                        no=''
                        yes='Ok' />
                </ScrollView>
                <View style={styles.viewBottom}>
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                        <TouchableOpacity
                            onPress={() => this.onButtonPressed()}>
                            <Text style={styles.buttonText}>Submit</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                </View>
            </ImageBackground>
        );
    }
}


const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        height: '100%',
        flex: 1,
    },
    containeMuscleGain: {
        width: wp('100%'),
        height: hp('12%'),
        backgroundColor: '#ffffff',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderWidth: .2,
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        marginBottom: -2,

    },
    linearGradient: {
        width: '95%',
        height: 50,
        borderRadius: 25,
        justifyContent: 'center',
        backgroundColor: 'transparent',
        alignSelf: 'center',
        marginBottom: 15
    },
    buttonText: {
        fontSize: 16,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    container: {
        height: 40,
        width: GAUGE_WIDTH,
        marginVertical: 8,
        alignSelf: 'center',
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        transform: [{ rotate: '90deg' }],
        top: 0,
    },
    optionInnerContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: hp('15%'),
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: hp('1%'),
        marginLeft: 20,
        marginBottom: 20,
        alignSelf: 'flex-start',
    },
    chartImageStyle: {
        width: 25,
        height: 25,
        alignSelf: 'flex-start',
    },
    appImageStyle: {
        width: 200,
        height: 60,
        marginTop: -45,
    },
    textStyle: {
        fontSize: 18,
        marginTop: hp('2%'),
        marginLeft: 40,
        marginRight: 40,
        fontWeight: '500',
        color: '#2d3142',
        lineHeight: 30,
        textAlign: 'center',
        fontFamily: 'Rubik-Regular',
    },
    txtMeasurement: {
        alignSelf: 'flex-end',
        fontSize: 12,
        fontWeight: '400',
        color: '#9c9eb9',
        letterSpacing: 0.23,
        marginLeft: -3,
        marginRight: 5,
        fontFamily: 'Rubik-Regular',
        marginBottom: 2,
    },
    viewBottom: {
        width: wp('93%'),
        alignContent: 'center',
        alignSelf: 'center',
        position: 'absolute',
        bottom: 0,
        backgroundColor: '#fff',
    },
    subtextOneStyle: {
        fontSize: 20,
        marginTop: hp('3%'),
        marginLeft: 40,
        marginRight: 40,
        fontWeight: '500',
        color: '#2d3142',
        lineHeight: 30,
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    countryText: {
        width: wp('75%'),
        alignSelf: 'center',
        fontSize: 14,
        fontWeight: '400',
        marginLeft: wp('2%'),
        color: '#2d3142',
        letterSpacing: 0.23,
        fontFamily: 'Rubik-Regular',
    },
    containericonStyle: {
        flexDirection: 'column',
        alignSelf: 'center',
        justifyContent: 'center',
    },
    viewPlusMinus: {
        width: wp('25%'),
        flexDirection: 'row',
        alignSelf: 'center',
        justifyContent: 'center',
    },
    countryText1: {
        alignSelf: 'center',
        fontSize: 14,
        fontWeight: '400',
        marginLeft: wp('2%'),
        color: '#2d3142',
        letterSpacing: 0.23,
        fontFamily: 'Rubik-Regular',
    },
    txtAvgVal: {
        alignSelf: 'center',
        fontSize: 20,
        fontWeight: '400',
        color: '#2d3142',
        letterSpacing: 0.23,
        marginLeft: -5,
        marginRight: 5,
        fontFamily: 'Rubik-Medium',
    },
    profileImage: {
        width: 60,
        height: 60,
        aspectRatio: 1,
        backgroundColor: "#ffffff",
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: "#979797",
        borderRadius: 60 / 2,
        resizeMode: "cover",
        alignSelf: 'center',
    },
    personImageStyle: {
        alignSelf: 'center',
        width: 180,
        height: 255,
        marginTop: 10,
        marginBottom: 10,
    },
});

export default BodyMeasurements;