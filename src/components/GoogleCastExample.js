import React, { Component } from 'react';
import { } from 'react-native';
import GoogleCast, { CastButton } from 'react-native-google-cast'

class GoogleCastExample extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };    
    }

    async componentDidMount() {
       
    }

    componentWillUnmount() {
        
    }

    render() {
        return (
            <CastButton style={{ width: 24, height: 24 }} />
        );
    }
}

const mapStateToProps = state => {
    return {};
};

export default connect(
    mapStateToProps,
    {
    },
)(GoogleCastExample);
