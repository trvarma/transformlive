"use strict"
import React, { Component } from "react";
import {
  ActivityIndicator,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { RNCamera } from "react-native-camera";
import { Actions } from "react-native-router-flux";
import { videoUriChanged } from '../actions';
import { withNavigationFocus } from 'react-navigation';
import { connect } from 'react-redux';
import CountDown from 'react-native-countdown-component';
import { allowFunction } from '../utils/ScreenshotUtils.js';
class RecordVideo extends Component {
  constructor() {
    super();

    this.state = {
      recording: false,
      processing: false,
      recorded: false,
      videoUri: '',
      timer: null,
      minutes_Counter: '00',
      seconds_Counter: '00',
      startDisable: false,
      isCountDown: false,
    };
  }
  componentDidMount() {
    allowFunction();
  }
  componentWillUnmount() {
    clearInterval(this.state.timer);
  }

  renderCountDown() {
    if (this.state.isCountDown) {
      return <CountDown
        style={styles.counter}
        until={120}
        size={14}
        onFinish={() => this.stopRecording()}
        digitStyle={{ backgroundColor: null }}
        digitTxtStyle={{ color: '#FFFFFF', fontSize: 12 }}
        timeLabelStyle={{ color: 'red', fontWeight: 'bold' }}
        separatorStyle={{ color: '#FFFFFF', fontWeight: 'bold', fontSize: 12 }}
        timeToShow={['M', 'S']}
        timeLabels={{ m: null, s: null }}
        showSeparator
      />
    }
    return null;
  }

  render() {
    const { recording, processing, recorded, videoUri } = this.state;

    let button = (
      <TouchableOpacity
        onPress={this.startRecording.bind(this)}
        style={styles.capture}
      >
        <Text style={{ fontSize: 14, color: '#FFFFFF' }}> RECORD </Text>
      </TouchableOpacity>
    );

    if (recording) {
      button = (
        <TouchableOpacity
          onPress={this.stopRecording.bind(this)}
          style={styles.capture}
        >
          <Text style={{ fontSize: 14, color: '#FFFFFF' }}> STOP </Text>
        </TouchableOpacity>
      );
    }

    if (processing) {
      button = (
        <View style={styles.capture}>
          <ActivityIndicator animating size={18} />
        </View>
      );
    }

    return (
      <View style={styles.container}>
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={styles.preview}
          type={RNCamera.Constants.Type.back}
          // flashMode={RNCamera.Constants.FlashMode.on}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          androidRecordAudioPermissionOptions={{
            title: 'Permission to use audio recording',
            message: 'We need your permission to use your audio',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
        />
        <View
          style={{ flex: 0, flexDirection: "row", justifyContent: "center", alignSelf: "center", }}>
          {button}
          {this.renderCountDown()}
        </View>


      </View>
    );
  }

  captureVideoUnit(camera) {
    setInterval(console.log('Start of interval'), 20000);
    setInterval(this.takeVideo(camera), 20000);
    if (camera) {
      //console.log('about to stop');
      camera.stopRecording();
    }
  }

  onButtonClear = () => {
    this.setState({
      timer: null,
      minutes_Counter: '00',
      seconds_Counter: '00',
    });
  }



  async startRecording() {
    let timer = setInterval(() => {

      var num = (Number(this.state.seconds_Counter) + 1).toString(),
        count = this.state.minutes_Counter;

      if (Number(this.state.seconds_Counter) == 120) {

        this.stopRecording();

      }

      this.setState({
        minutes_Counter: count.length == 1 ? '0' + count : count,
        seconds_Counter: num.length == 1 ? '0' + num : num
      });
    }, 1000);
    this.setState({ timer });

    this.setState({ startDisable: true })
    this.setState({ isCountDown: true })
    this.setState({ recording: true });
    // default to mp4 for android as codec is not set
    // const options = { maxDuration: 10 }
    const options = { quality: RNCamera.Constants.VideoQuality['4:3'] };
    // const options = {  fixOrientation:true, forceUpOrientation:true, quality: 0.2};
    // const options = { quality: 0.5, base64: true };
    const { uri, codec = "mp4" } = await this.camera.recordAsync(options);
    this.setState({ recording: false, processing: true });
    const type = `video/${codec}`;
    // this.setState({videoUri: uri})
    // quality will be "low", "medium" or "high"
    // RNCompress.compressVideo(uri, 'low');
    this.setState({ videoUri: uri })
    this.props.videoUriChanged(uri);
    //console.log("URI:", uri);
    this.setState({ processing: false });
  }

  stopRecording() {
    clearInterval(this.state.timer);
    this.setState({ startDisable: false })
    this.setState({ isCountDown: false })
    this.camera.stopRecording();
    Actions.pop();
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "black"
  },
  preview: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center"
  },
  capture: {
    flex: 0,
    backgroundColor: "#1aba9a",
    borderRadius: 5,
    padding: 10,
    paddingHorizontal: 20,
    alignSelf: "center",
    margin: 10,
  },
  counter: {
    alignSelf: 'center',
    position: 'relative',
  }

});
const mapStateToProps = state => {
  return {
    videoUri: state.catcre.videoUri,
    error: state.catcre.error,
    loading: state.catcre.loading,
  };
};
export default withNavigationFocus(
  connect(
    mapStateToProps,
    { videoUriChanged },
  )(RecordVideo),
);