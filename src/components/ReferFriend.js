import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    BackHandler,
    KeyboardAvoidingView,
    Dimensions,
    Platform,
    TextInput
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { CustomDialog, Loader, NoInternet } from './common';
let token = '', userId = '';
import { withNavigationFocus } from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { RFValue } from "react-native-responsive-fontsize";
const screenHeight = Math.round(Dimensions.get('window').height);
import DeviceInfo from 'react-native-device-info';
import Share1 from 'react-native-share';
import RNFetchBlob from 'rn-fetch-blob';
import { BASE_URL, SWR } from '../actions/types';
import LogUtils from '../utils/LogUtils.js';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import NetInfo from "@react-native-community/netinfo";
import { allowFunction } from '../utils/ScreenshotUtils.js'; 
function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}


class ReferFriend extends Component {

    constructor(props) {
        super(props);
        this.state = {
            firstName: '',
            lastName: '',
            email: '',
            isSuccess: false,
            sucMsg: '',
            isAlert: false,
            alertMsg: '',
            alertTitle: '',
            loading: false,
            referData: {},
            mobileNumber: '',
            isInternet: false,
            isShowAgeNext:true,
        };
    }
    async componentDidMount() {
        allowFunction();
        token = await AsyncStorage.getItem('token');
        userId = await AsyncStorage.getItem('userId');

        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.getReferData();
            }
            else {
                this.setState({ isInternet: true });
            }
        });

        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    onBackPressed() {
        Actions.pop();
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }


    async onRetry() {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isInternet: false });
            }
            else {
                this.setState({ isInternet: true });
            }
        });
    }

    mobilevalidate(text) {
        const reg = /^[0]?[6789]\d{9}$/;
        if (reg.test(text) === false) {
            this.setState({
                mobilevalidate: false,
                telephone: text,
            });
            return false;
        } else {
            this.setState({
                mobilevalidate: true,
                telephone: text,
                message: '',
            });
            return true;
        }
    }

    onButtonPressed() {
        if (this.state.mobileNumber) {
            if (this.mobilevalidate(this.state.mobileNumber)) {

                NetInfo.fetch().then(state => {
                    if (state.isConnected) {
                        this.saveReferData();
                    }
                    else {
                        this.setState({ isInternet: true });
                    }
                });

            }
            else {
                this.setState({ isAlert: true, alertTitle: 'Alert', alertMsg: 'Please enter a valid mobile number' });
            }
        }
        else {
            this.setState({ isAlert: true, alertTitle: 'Alert', alertMsg: 'Please enter mobile number' });
        }


    }



    async getReferData() {
        this.setState({ loading: true });

        let body = '';

        if (Platform.OS === 'android') {
            body = JSON.stringify({
                device_type: 1,
            });

        }
        else if (Platform.OS === 'ios') {
            body = JSON.stringify({
                device_type: 2,
            });
        }
        LogUtils.infoLog1('request', body);

        fetch(
            `${BASE_URL}/trainee/referalmaster`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: body,
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('code', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false, isAlert: false, isSuccess: true, alertMsg: data.message, referData: data.data });
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertTitle: data.title, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertTitle: data.title, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: SWR });
            });
    }

    shareImage = (img_url, message) => {

        const fs = RNFetchBlob.fs;
        let imagePath = null;
        RNFetchBlob.config({
            fileCache: true
        })
            .fetch("GET", img_url)
            // the image is now dowloaded to device's storage
            .then(resp => {
                // the image path you can use it directly with Image component
                imagePath = resp.path();
                return resp.readFile("base64");
            })
            .then(base64Data => {
                // here's base64 encoded image
                console.log(base64Data);
                Share1.open({
                    title: 'Share Via',
                    message: message,
                    // social: Share1.Social.WHATSAPP,
                    url: `data:image/png;base64,${base64Data}`,
                    // url: "file://" + img_url,

                })
                    .then(res => {
                        LogUtils.infoLog(res);
                    })
                    .catch(err => {
                        err && console.log(err);
                    });
                // remove the file from storage
                return fs.unlink(imagePath);
            });

        // Share1.open({
        //     title: 'Share Via',
        //     message: message,
        //     // social: Share1.Social.WHATSAPP,
        //     // url: `data:image/png;base64,${base64Data}`,
        //     // url: "file://" + img_url,

        // })
        //     .then(res => {
        //         LogUtils.infoLog(res);
        //     })
        //     .catch(err => {
        //         err && console.log(err);
        //     });



        // RNFetchBlob.fetch('GET', img_url)
        //     .then(resp => {
        //         LogUtils.infoLog1('response : ', resp);
        //         LogUtils.infoLog(resp.data);
        //         let base64image = resp.data;
        //         var base64Data = `data:image/png;base64,` + base64image;
        //         Share1.open({
        //             title: 'Share Via',
        //             message: message,
        //             // social: Share1.Social.WHATSAPP,
        //             // url: base64Data,
        //         })
        //             .then(res => {
        //                 LogUtils.infoLog(res);
        //             })
        //             .catch(err => {
        //                 err && console.log(err);
        //             });
        //         // share('data:image/png;base64,' + base64image);
        //     })
        //     .catch((errorMessage, statusCode) => {
        //         // error handling
        //     })

        // Share1.open({
        //     title: 'Share Via',
        //     message: message,
        //     // social: Share1.Social.WHATSAPP,
        //     url: app_img,
        // })
        //     .then(res => {
        //         LogUtils.infoLog(res);
        //     })
        //     .catch(err => {
        //         err && LogUtils.infoLog(err);
        //     });

    };

    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
        this.setState({ isAlert: false, alertTitle: '', alertMsg: '' });

        if (this.state.isSuccess) {
            this.setState({ isSuccess: false, mobileNumber: '' });
        }
    }


    onChanged(text) {
        let newText = '';
        let numbers = '0123456789';

        for (var i = 0; i < text.length; i++) {
            if (numbers.indexOf(text[i]) > -1) {
                newText = newText + text[i];
            }
            else {
                this.setState({ isAlert: true, alertTitle: 'Alert', alertMsg: 'Please enter numbers only' });

            }
        }
        this.setState({ mobileNumber: newText });
    }


    async saveReferData() {
        this.setState({ loading: true });
        let body = '';
        body = JSON.stringify({
            mobileno: this.state.mobileNumber,
        });
        LogUtils.infoLog1('request', body);

        fetch(
            `${BASE_URL}/trainee/referfriend`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: body,
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('code', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false, isAlert: true, isSuccess: true, alertMsg: data.message, alertTitle: data.title });
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertTitle: data.title, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertTitle: data.title, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertTitle: data.title, alertMsg: SWR });
            });
    }

    render() {
        return (
            <KeyboardAwareScrollView
                resetScrollToCoords={{ x: 0, y: 0 }}
                contentContainerStyle={styles.containerStyle}
                enableOnAndroid={false}
                onKeyboardDidShow={(frames) => this.setState({ isShowAgeNext: false })}
        onKeyboardDidHide={(frames) => this.setState({ isShowAgeNext: true })}
                scrollEnabled={false}>
                <Loader loading={this.state.loading} />
                <NoInternet
                    image={require('../res/img_nointernet.png')}
                    loading={this.state.isInternet}
                    onRetry={this.onRetry.bind(this)} />
                <View style={{ width: '100%', height: '100%', backgroundColor: '#ffffff', }}>

                    <View style={{
                        flexDirection: 'row',
                        margin: 20,
                    }}>
                        <View style={{ position: 'absolute', zIndex: 111 }}>
                            <TouchableOpacity
                                style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                                onPress={() => this.onBackPressed()}>
                                <Image
                                    source={require('../res/ic_back.png')}
                                    style={styles.backImageStyle}
                                />
                            </TouchableOpacity>
                        </View>

                        <Text style={styles.textHeadTitle}>Refer a Friend</Text>
                    </View>

                    <View style={{ width: wp('100%'), height: hp('85%'), flexDirection: 'column', alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>

                        <Image
                            source={require('../res/ic_referfriend.png')}
                            style={{
                                width: 170,
                                height: 168,
                                alignSelf: 'center',
                            }}
                        />


                        <View style={{ marginTop: 25 }}>
                            <Text style={styles.headerTextStyle}>
                                {`${this.state.referData.title}`}
                            </Text>

                        </View>

                        <Text style={styles.refDesctextStyle}>
                            {`${this.state.referData.sub_title}`}
                        </Text>


                        <View style={styles.containerMobileStyle}>
                            {/* <Image
                                source={require('../res/india_flag.png')}
                                style={styles.mobileImageStyle}
                            />
                            <Text style={styles.countryText}> +91 </Text>
                            <Image
                                source={require('../res/ic_dropdown.png')}
                                style={styles.countryImageDropStyle}
                            /> */}
                            <TextInput
                                style={styles.textInputStyle}
                                keyboardType="numeric"
                                placeholder="Mobile Number"
                                placeholderTextColor="grey"
                                maxLength={10}
                                value={this.state.mobileNumber}
                                returnKeyType='done'
                                onChangeText={(text) => this.onChanged(text)}
                            />
                        </View>

                        {/* <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 20 }}>
                            <Image
                                source={require('../res/ic_referalcode_bg.png')}
                                style={styles.copyImageStyle} />

                            <Text style={styles.linkTextStyle}>
                                {`${this.state.referData.code}`}
                            </Text>

                        </View> */}

                    </View>

                    {
                            this.state.isShowAgeNext &&
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                        <TouchableOpacity
                            onPress={() => this.onButtonPressed()}
                            style={styles.buttonTuch}>
                            <Text style={styles.buttonText}>REFER</Text>
                        </TouchableOpacity>
                    </LinearGradient>
    }


                </View>
                <CustomDialog
                    visible={this.state.isAlert}
                    title={this.state.alertTitle}
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    no=''
                    yes='Ok' />
            </KeyboardAwareScrollView>
        )
    }

}


const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        height: '100%',
        backgroundColor: '#fff',
        flex: 1,
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'center',
    },
    container: {
        flex: 1,
        justifyContent: "flex-start"
    },
    row: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'center',
    },
    column: {
        flexDirection: 'column',
        flex: 1,
        justifyContent: 'center',
    },
    headerTextStyle: {
        fontSize: RFValue(16, screenHeight),
        fontWeight: '500',
        color: '#2d3142',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
        marginTop: 10,
        lineHeight: 24,
    },
    refDesctextStyle: {
        fontSize: RFValue(14, screenHeight),
        fontWeight: '500',
        color: '#C3C1CE',
        textAlign: 'center',
        fontFamily: 'Rubik-Regular',
        lineHeight: 16,
        marginTop: 5,
        marginLeft: 35,
        marginRight: 35,
    },
    linkTextStyle: {
        fontSize: RFValue(16, screenHeight),
        fontWeight: '500',
        color: '#ffffff',
        textAlign: 'center',
        fontFamily: 'Rubik-Regular',
        position: 'absolute',
        alignSelf: 'center',
        lineHeight: 20,
        letterSpacing: 5,
    },
    copyImageStyle: {
        width: '60%',
        height: 50,
        alignSelf: 'center',
    },
    linearGradient: {
        width: '90%',
        height: 50,
        bottom: 0,
        borderRadius: 25,
        justifyContent: 'center',
        position: 'absolute',
        alignSelf: 'center',
        marginBottom: 15,
    },
    buttonTuch:
    {
        backgroundColor: 'transparent',
    },
    buttonText: {
        fontSize: RFValue(15, screenHeight),
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    containerMobileStyle: {
        backgroundColor: '#ffffff',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 15,
        position: 'relative',
        alignItems: 'center',
        marginLeft: 30,
        marginRight: 30,
        marginTop: 40,
        alignSelf: 'center',
        height: 50,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    textInputStyle: {
        height: 40,
        flex: 1,
        marginLeft: 10,
        alignSelf: 'center',
        fontSize: 14,
        fontWeight: '400',
        color: '#2d3142',
        letterSpacing: .5,
        fontFamily: 'Rubik-Regular',
    },

});

const mapStateToProps = state => {

    //   const { goals, trainerCodes } = state.procre;
    const loading = state.procre.loading;
    const error = state.procre.error;
    return { loading, error, };

};

export default withNavigationFocus(
    connect(
        mapStateToProps,
        {},
    )(ReferFriend),
);