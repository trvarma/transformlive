import React, { Component } from 'react';
import {
    LayoutAnimation,
    UIManager,
    Platform,
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    BackHandler,
    ScrollView,
    ImageBackground,
    Dimensions,
    Modal,
    Animated,
    ActivityIndicator,
    TextInput,
    FlatList
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { CustomDialog, Loader, NoInternet } from './common';
import { withNavigationFocus } from 'react-navigation';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import NetInfo from "@react-native-community/netinfo";
import { BASE_URL, SWR } from '../actions/types';
import LinearGradient from 'react-native-linear-gradient';
import moment from "moment";
import FastImage from 'react-native-fast-image';
import Dialog, {
    DialogContent,
} from 'react-native-popup-dialog';
import LogUtils from '../utils/LogUtils.js';
import DeviceInfo from 'react-native-device-info';
const sliderWidth = Dimensions.get('window').width;
const itemWidth = wp('85%');
import Carousel, { Pagination } from 'react-native-snap-carousel';
import _ from 'lodash';
import { WebView } from 'react-native-webview';

import RBSheet from "react-native-raw-bottom-sheet";
let screenHeight = Math.round(Dimensions.get('window').height);
let popupHeight = screenHeight / 2;
import { allowFunction } from '../utils/ScreenshotUtils.js';

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

const FOOD = [
    { id: 1, name: 'Breakfast', image: require('../res/ic_breakfast_blue.png') },
    { id: 2, name: 'Lunch', image: require('../res/ic_lunch_blue.png') },
    { id: 3, name: 'Dinner', image: require('../res/ic_dinner_blue.png') },
    { id: 4, name: 'Snacks', image: require('../res/ic_snacks_blue.png') },
];



class ExpandableItemComponent extends Component {
    //Custom Component for the Expandable List
    constructor() {
        super();
        // if (Platform.OS === 'android' && UIManager.setLayoutAnimationEnabledExperimental) {
        //     UIManager.setLayoutAnimationEnabledExperimental(true);
        // }
        this.state = {
            layoutHeight: 0,
        };
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.item.isExpanded) {
            this.setState(() => {
                return {
                    layoutHeight: null,
                };
            });
        } else {
            this.setState(() => {
                return {
                    layoutHeight: 0,
                };
            });
        }
    }
    shouldComponentUpdate(nextProps, nextState) {
        if (this.state.layoutHeight !== nextState.layoutHeight) {
            return true;
        }
        return false;
    }

    render() {
        return (
            <View key={this.props.item.f_id}>

                {this.props.item.option
                    ?
                    (
                        <Text style={styles.textOption}>{`${this.props.item.option}`}</Text>
                    )
                    :
                    (
                        <View>
                        </View>
                    )
                }

                <View style={styles.vFooditemList}>
                    <View style={{ padding: 10, flexDirection: 'row' }}>
                        <View style={{ flex: 1, flexDirection: 'column', alignItems: 'flex-start', alignContent: 'flex-start', }}>
                            <Text style={styles.textfoodItemName}>{`${this.props.item.food_name}`}</Text>
                            <View>
                                <View style={{ flexDirection: 'row', paddingTop: 5, paddingBottom: 5, }}>
                                    <Text style={styles.textPer}>{`${this.props.item.qnty_text}`}</Text>
                                    {this.props.item.img_url
                                        ?
                                        (
                                            <TouchableOpacity
                                                onPress={this.props.onClickShowImage}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    source={require('../res/ic_eye.png')}
                                                    style={{
                                                        width: 18,
                                                        height: 18,
                                                        alignSelf: 'center',
                                                        marginLeft: 5,
                                                        tintColor: '#8c52ff'
                                                    }} />
                                            </TouchableOpacity>
                                        )
                                        :
                                        (
                                            <View>
                                            </View>
                                        )}

                                </View>


                                {this.props.item.goodsource
                                    ?
                                    (
                                        <View style={styles.nutLevelBack}>
                                            <Text style={styles.nutLevel}>{this.props.item.goodsource}</Text>
                                        </View>
                                    )
                                    :
                                    (
                                        <View>
                                        </View>
                                    )
                                }

                            </View>
                            <View style={{ flexDirection: 'row', alignItems: 'center', paddingTop: 5, alignContent: 'center', justifyContent: 'center' }}>
                                <View style={styles.viewCarBack}>
                                    <Text style={styles.textCarBack}>C</Text>
                                </View>
                                <Text style={styles.textTitle}>{this.props.item.carbs} g</Text>
                                <View style={{ width: 10 }}></View>
                                <View style={styles.viewCarBack}>
                                    <Text style={styles.textCarBack}>P</Text>
                                </View>
                                <Text style={styles.textTitle}>{this.props.item.protein} g</Text>
                                <View style={{ width: 10 }}></View>
                                <View style={styles.viewCarBack}>
                                    <Text style={styles.textCarBack}>F</Text>
                                </View>
                                <Text style={styles.textTitle}>{this.props.item.fat} g</Text>
                                <View style={{ width: 10 }}></View>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/ic_cal.png')}
                                    style={{ width: 15, height: 15, alignSelf: 'center' }}
                                />
                                <Text style={styles.textTitle}>{this.props.item.calories} cal</Text>
                            </View>
                        </View>

                        <View style={{
                            flexDirection: 'column',
                            justifyContent: 'center',
                            alignContent: 'center',
                            alignSelf: 'center',
                            padding: 5,
                        }}>
                            {this.props.item.instructions_url
                                ?
                                (
                                    <View>
                                        <TouchableOpacity
                                            onPress={this.props.onClickShowInst}>
                                            <Image
                                                progressiveRenderingEnabled={true}
                                                source={require('../res/ic_info_blueround.png')}
                                                style={styles.imgAdd} />

                                        </TouchableOpacity>
                                        <View style={{ height: 10 }}></View>
                                    </View>

                                )
                                :
                                (
                                    <View></View>
                                )

                            }

                            <TouchableOpacity
                                onPress={this.props.onClickAddFood}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    source={require('../res/ic_add_feed.png')}
                                    style={styles.imgAdd} />

                            </TouchableOpacity>
                            <View style={{ height: 10 }}></View>
                            <TouchableOpacity
                                onPress={this.props.onClickFunction}>
                                {this.props.item.isExpanded
                                    ? (
                                        <Image
                                            source={require('../res/ic_top_arrow.png')}
                                            style={styles.imgDropdown1}
                                        />
                                    )
                                    : (
                                        <Image
                                            source={require('../res/ic_top_arrow.png')}
                                            style={styles.imgDropdown}
                                        />
                                    )}

                            </TouchableOpacity>
                        </View>

                    </View>
                    <View
                        style={{
                            height: this.state.layoutHeight,
                            overflow: 'hidden',
                            flexDirection: 'column',
                        }}>
                        <View style={{ height: 1, backgroundColor: '#e9e9e9', }}></View>
                        <Text style={styles.textIngredients}>Ingredients</Text>
                        {/*Content under the header of the Expandable List Item*/}
                        {this.props.item.ingredients.map((item, key) => (
                            <View key={key} style={styles.nutrition1}>
                                <View style={styles.viewItemBlueDot}></View>
                                <View style={styles.nutritionInnerView}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text style={styles.textNutTitle}>{item.name} ({item.energy} cal/gram)</Text>
                                        <Text style={styles.textNutTitle1}>{item.qty_msr} {item.unit}</Text>
                                    </View>
                                </View>
                            </View>
                        ))}
                    </View>
                </View>

            </View>
        );
    }
}

class Trainee_DietHome1 extends Component {

    constructor(props) {
        super(props);
        super();

        this.state = {
            fill1: 0,
            fill2: 0,
            fill3: 0,
            loading: false,
            isAlert: false,
            alertTitle: '',
            alertMsg: '',
            resGuideLinesObj: {},
            noPrefCarb: '',
            tab: 1,
            foodType: 1,
            noData: '',

            headerTitle: 'How does it work',
            mainOption: 1,
            searchText: '',
            arrDietFoodHungryItems: [],
            isPopVisible: false,
            arrDietFoodCatgs: [],
            isSubListVisible: false,
            modalVisible: false,
            isPopCallDiet: false,
            selFoodItemObj: '',
            selFoodTitle: '',
            isHungryInfo: 'visible',
            showhungry: true,
            isTipsPopVisible: false,
            isPopItemInst: false,
            sectionTipTitle: '',
            sectionTipDescription: '',
            tipTitle: '',
            activeSlide1: 0,
            activeSlide2: 0,
            activeSlide3: 0,
            activeSlide4: 0,
            activeSlide5: 0,
            DATA: [
                {
                    id: '1',
                    name: 'I cant eat this much',
                    check: false,
                },
                {
                    id: '2',
                    name: 'Im allergic to some of these foods',
                    check: false,
                },
                {
                    id: '3',
                    name: 'I dont feel like eating this right now',
                    check: false,
                },
                {
                    id: '4',
                    name: 'I dont have time to prepare this meal',
                    check: false,
                },
                {
                    id: '5',
                    name: 'This is an odd combination!',
                    check: false,
                },
                {
                    id: '6',
                    name: 'I dont want to eat some of these foods',
                    check: false,
                },
                {
                    id: '7',
                    name: 'Other',
                    check: false,
                },
                {
                    id: '8',
                    name: 'Im avoiding non-vegetarian foods right now',
                    check: false,
                },

            ],

            SelectedfoodLikesArray: [],
            foodLikesarray: [],
            newDietArray: [],
            isInternet: false,
            arrAllPlans: [],
            itemImg: '',
            isShowItemImage: false,
            showFreeTrail: 0,
            dietId: '0',

            bounceValue: new Animated.Value(1000),  //This is the initial position of the subview
            confirmSuccess: false,
            freeTrialObj: {},
            buttonState: '',
            freeTrialSuccessTitle: '',
            freeTrialSuccessMessage: '',
            onConfirmClick: false,
            freeTrialConfirmImg: '',
            freeTrialConfirmTitle: '',
            freeTrialConfirmText: '',
            popuploading: false,

            email: '',
            freeTrailProcess: 1,
            emailOtp: '',
            emailSuccessTitle: '',
            emailSuccessDesc: '',

            diet_list: [],
            isDietListPopup: false,
            dietName: '',
            dietDuration: '',

        };

    }
    async componentDidMount() {
        allowFunction();
        if (popupHeight < 370) {
            popupHeight = 370;
        }
        this.setState({ isHungryInfo: await AsyncStorage.getItem('isHungryInfo') });
        let diet_id = await AsyncStorage.getItem('diet_id');
        this.setState({ dietId: diet_id });
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isInternet: false });
                this.getDietPlanDetails();
            }
            else {
                this.setState({ isInternet: true });
            }
        });
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        if (this.state.isPopItemInst) {
            this.setState({ isPopItemInst: false });
        }
        else if (this.state.isPopVisible) {
            this.setState({ isPopVisible: false });
        } else if (this.state.isTipsPopVisible) {
            this.setState({ isTipsPopVisible: false });
        } else if (this.state.isDietListPopup) {
            LogUtils('Pressed');
            this.setState({ isDietListPopup: false });
        }
        else {
            if (this.props.from === 'rewards' || this.props.from === 'feed') {
                Actions.pop();
            }
            else if (this.props.isRefresh === 'yes') {
                Actions.traineeHome();
            }
            else {
                Actions.popTo('traineeHome');
            }
        }
    }

    updateLayout = (mainIndex, index) => {
        // console.log('mainIndex', mainIndex);
        // console.log('index', index);
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        const array = [...this.state.newDietArray];
        // console.log('selArray', array[mainIndex].foodarr);
        array[mainIndex].foodarr.map((value, placeindex) =>
            placeindex === index
                ? (array[mainIndex].foodarr[placeindex]['isExpanded'] = !array[mainIndex].foodarr[placeindex]['isExpanded'])
                : (array[mainIndex].foodarr[placeindex]['isExpanded'] = false)
        );
        this.setState(() => {
            return {
                newDietArray: array,
            };
        });
        // console.log('sel Array', array[mainIndex].foodarr);
    };

    updateSelFooditem = (mainIndex, index) => {
        const item = { ...this.state.newDietArray[mainIndex].foodarr[index] }
        const item1 = { ...this.state.newDietArray[mainIndex] }
        this.setState({ selFoodItemObj: item, selFoodTitle: item1.foodtype });
        this.getFoodDetails({ selItem: item });
    };

    instructionsOfFooditem = (mainIndex, index) => {
        const item = { ...this.state.newDietArray[mainIndex].foodarr[index] }
        this.setState({ modalVisible: true, selFoodItemObj: item });

    };

    showImageOfFooditem = (mainIndex, index) => {
        const item = { ...this.state.newDietArray[mainIndex].foodarr[index] }
        this.setState({ isShowItemImage: true, selFoodItemObj: item, itemImg: item.img_url });


    };

    async saveDietIdAsync(dietid) {
        await AsyncStorage.setItem('diet_id', dietid);
    }

    async getDietPlanDetails() {
        this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/dietprofilenew`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    LogUtils.infoLog1('data', data.data.food);
                    LogUtils.infoLog1('diet_id', JSON.stringify(data.data.diet_id));
                    if (data.data.diet_id > 0) {
                        if (this.state.dietId !== JSON.stringify(data.data.diet_id)) {
                            this.setState({ isTipsPopVisible: true });
                            this.saveDietIdAsync(JSON.stringify(data.data.diet_id));
                        }
                    }
                    this.setState({ loading: false, resGuideLinesObj: data.data, newDietArray: data.data.food, diet_list: data.data.diet_list });
                    this.setState({ email: data.data.email, dietName: data.data.diet_name, dietDuration: data.data.diet_duration });
                    LogUtils.infoLog1('diet_list', this.state.diet_list);

                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertTitle: data.title, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertTitle: data.title, alertMsg: data.message, noData: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertTitle: 'Alert', alertMsg: JSON.stringify(error) });
            });
    }

    async getFoodDetails({ selItem }) {
        this.setState({ loading: true, });
        let token = await AsyncStorage.getItem('token');
        LogUtils.infoLog1(token, JSON.stringify({
            food_id: selItem.food_id,
            fs_id: selItem.fs_id,
            is_update: 0,
        }));
        fetch(
            `${BASE_URL}/trainee/getfooditemdetail`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    food_id: selItem.food_id,
                    fs_id: selItem.fs_id,
                    is_update: 0,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false });
                    this.goToFoodDetails({ response: data.data });
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error) });
            });
    }


    _toggleSubview() {

        Animated.timing(
            this.state.bounceValue,
            {
                toValue: 0,
                duration: 1000
            }
        ).start();

    }

    async saveFreeTrial() {
        let token = await AsyncStorage.getItem('token');
        let urlType = '';
        if (this.state.resGuideLinesObj.show_free_call === 1) {
            urlType = 'save_freetrial2';
        }
        else {
            urlType = 'save_freetrial';
        }

        LogUtils.infoLog(`Url : ${BASE_URL}/trainee/${urlType}`);

        fetch(
            `${BASE_URL}/trainee/${urlType}`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    otp: this.state.emailOtp,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this._toggleSubview();
                    this.setState({ popuploading: false, freeTrialObj: data, confirmSuccess: true, freeTrialSuccessTitle: data.title, freeTrialSuccessMessage: data.message, freeTrailProcess: 4 });
                    if (this.state.resGuideLinesObj.show_free_call === 1) {
                        LogUtils.appsFlyerEventLog('freeconsultationstarted', {
                            desc: 'Free Consultation Started',
                        });
                    }
                    else {
                        LogUtils.appsFlyerEventLog('freetrialstarted', {
                            desc: 'Free Trial Started',
                        });
                    }
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ popuploading: false, isAlert: true, alertMsg: data.message, onConfirmClick: false });
                    } else {
                        this.setState({ popuploading: false, isAlert: true, alertMsg: data.message, onConfirmClick: false });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ popuploading: false, isAlert: true, alertMsg: JSON.stringify(error), onConfirmClick: false });
            });
    }

    async verifyEmailOtp() {
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/verifyemailotp`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    email: this.state.email,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this._toggleSubview();
                    this.setState({ popuploading: false, confirmSuccess: true, emailSuccessTitle: data.title, emailSuccessDesc: data.message, freeTrailProcess: 3, onConfirmClick: false });

                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ popuploading: false, isAlert: true, alertMsg: data.message, onConfirmClick: false });
                    } else {
                        this.setState({ popuploading: false, isAlert: true, alertMsg: data.message, onConfirmClick: false });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ popuploading: false, isAlert: true, alertMsg: JSON.stringify(error), onConfirmClick: false });
            });
    }


    goToFoodDetails({ response }) {
        Actions.traAddFood({ title: this.state.selFoodTitle, foodId: this.state.selFoodItemObj.food_id, fType: this.state.selFoodItemObj.meal_type, mdate: moment(new Date()).format("YYYY-MM-DD"), fsId: this.state.selFoodItemObj.fs_id, mFrom: 'diet', mainObject: response });
    }

    async saveFood({ selItem }) {
        LogUtils.infoLog1('sel object  ', selItem);
        LogUtils.infoLog1('req object  ', JSON.stringify({
            mt_id: selItem.meal_type,
            name: selItem.food_name,
            qnty: selItem.qnty,
            measurement: selItem.mesurement,
            cal: selItem.calories,
            carb: selItem.carbs,
            protein: selItem.protein,
            fat: selItem.fat,
            food_id: selItem.food_id,
            uf_id: 0,
            sel_date: moment(new Date()).format("YYYY-MM-DD"),
            fs_id: selItem.fs_id,
        }));

        this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/fooddet`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    mt_id: selItem.meal_type,
                    name: selItem.food_name,
                    qnty: selItem.qnty,
                    measurement: selItem.mesurement,
                    cal: selItem.calories,
                    carb: selItem.carbs,
                    protein: selItem.protein,
                    fat: selItem.fat,
                    food_id: selItem.food_id,
                    uf_id: 0,
                    sel_date: moment(new Date()).format("YYYY-MM-DD"),
                    fs_id: selItem.fs_id,
                }),

            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {

                    this.setState({ selFoodItemObj: '', loading: false, isAlert: true, alertTitle: data.title, alertMsg: data.message });

                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertTitle: data.title, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertTitle: data.title, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertTitle: 'Alert', alertMsg: JSON.stringify(error) });
            });
    }

    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }

        this.setState({ isAlert: false, alertTitle: '', alertMsg: '' });

    }

    press = (hey) => {

        this.state.DATA.map((item) => {
            if (item.id === hey.id) {
                item.check = !item.check
                if (item.check === true) {
                    this.state.SelectedfoodLikesArray.push(item);
                } else if (item.check === false) {
                    const i = this.state.SelectedfoodLikesArray.indexOf(item)
                    if (1 != -1) {
                        this.state.SelectedfoodLikesArray.splice(i, 1)
                        return this.state.SelectedfoodLikesArray
                    }
                }
            }
        })
        this.setState({ foodLikesarray: this.state.DATA })

    }

    toggleModal(visible) {
        this.setState({ modalVisible: visible });
    }


    get pagination1() {
        // const { entries, activeSlide } = this.state;
        return (
            <Pagination
                dotsLength={this.state.newDietArray.length}
                activeDotIndex={this.state.activeSlide1}
                containerStyle={{ marginTop: 5, paddingVertical: 10, }}
                dotContainerStyle={{
                    // backgroundColor: '#ffffff',
                    width: 10,
                    height: 10,
                    // marginTop: -20,
                }}
                dotStyle={{
                    width: 10,
                    height: 10,
                    borderRadius: 5,
                    // marginHorizontal: 2,
                    backgroundColor: '#8c52ff'
                }}
                inactiveDotStyle={{
                    // Define styles for inactive dots here
                    width: 10,
                    height: 10,
                    borderRadius: 5,
                    // marginHorizontal: 2,
                    backgroundColor: '#b0b0b0',
                }}
                inactiveDotOpacity={1}
                inactiveDotScale={0.8}
            />
        );
    }

    FlatListItemSeparator = () => {
        return (
            <View
                style={{
                    height: 5,
                    width: "100%",
                    backgroundColor: "transparent",
                }}
            />
        );
    }

    FlatListItemSeparator1 = () => {
        return (
            <View
                style={{
                    height: 0,
                    width: 0,
                    backgroundColor: "transparent",
                }}
            />
        );
    }

    renderFoodType = (item) => {
        if (item.food_type) {
            if (item.food_type.length > 20) {
                return (
                    <View>
                        <Text numberOfLines={1} style={styles.textFoodType}>{`${item.food_type}`}</Text>
                    </View>
                );
            }
            else {
                return (
                    <View>
                        <Text numberOfLines={1} style={styles.textFoodType1}>{`${item.food_type}`}</Text>
                    </View>
                );
            }
        } else {
            return (
                <View>
                </View>
            )
        }
    }

    renderFoodTypesPOP() {
        let arrBreakfastInner = FOOD.map((inItem, j) => {
            return <LinearGradient key={j} colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientPopup}>
                <TouchableOpacity onPress={() => {
                    this.setState({ isPopVisible: false });
                    this.saveFood({ mealType: inItem.id })
                }}>
                    <Text style={styles.textAddBreakOpt}>{inItem.name}</Text>
                </TouchableOpacity>
            </LinearGradient>
        });
        return (
            <View>
                {arrBreakfastInner}
            </View>
        );
    }

    // renderHungryItemsList() {
    //     if (Array.isArray(this.state.arrDietFoodHungryItems) && this.state.arrDietFoodHungryItems.length) {
    //         return (
    //             <FlatList
    //                 style={{ marginTop: 10, }}
    //                 contentContainerStyle={{ paddingBottom: hp('1%') }}
    //                 showsVerticalScrollIndicator={false}
    //                 data={this.state.arrDietFoodHungryItems}
    //                 // extraData={this.state}
    //                 keyExtractor={item => item.food_id}
    //                 ItemSeparatorComponent={this.FlatListItemSeparator}
    //                 renderItem={({ item }) => {
    //                     return <TouchableOpacity
    //                         key={item.food_id}
    //                         onPress={() => {
    //                             this.setState({ isPopVisible: true, selFoodItemObj: item });
    //                         }}
    //                         style={styles.containerListStyle}>

    //                         <View style={{ flexDirection: 'column', alignItems: 'flex-start', alignContent: 'flex-start', }}>
    //                             <Text style={styles.textWorkName}>{`${item.food_name} - ${item.full_serving}`}</Text>
    //                             <View style={{ flexDirection: 'row', alignItems: 'center', paddingTop: 5, alignContent: 'center', justifyContent: 'center' }}>
    //                                 <View style={styles.viewCarBack}>
    //                                     <Text style={styles.textCarBack}>C</Text>
    //                                 </View>
    //                                 <Text style={styles.textTitle}>{`${item.carbohydrate}`}g</Text>
    //                                 <View style={{ width: 15 }}></View>
    //                                 <View style={styles.viewCarBack}>
    //                                     <Text style={styles.textCarBack}>P</Text>
    //                                 </View>
    //                                 <Text style={styles.textTitle}>{`${item.protein}`}g</Text>
    //                                 <View style={{ width: 15 }}></View>
    //                                 <View style={styles.viewCarBack}>
    //                                     <Text style={styles.textCarBack}>F</Text>
    //                                 </View>
    //                                 <Text style={styles.textTitle}>{`${item.fat}`}g</Text>
    //                                 <View style={{ width: 15 }}></View>
    //                                 <Image
    //                                     progressiveRenderingEnabled={true}
    //                                     resizeMethod="resize"
    //                                     source={require('../res/ic_cal.png')}
    //                                     style={{ width: 15, height: 15, alignSelf: 'center' }}
    //                                 />
    //                                 <Text style={styles.textTitle}>{`${item.calories}`} cal</Text>

    //                             </View>

    //                         </View>

    //                         <Image
    //                             progressiveRenderingEnabled={true}
    //                             resizeMethod="resize"
    //                             source={require('../res/ic_add_feed.png')}
    //                             style={styles.imgAdd} />

    //                     </TouchableOpacity>
    //                 }}
    //             />
    //         )
    //     }
    //     else {
    //         return (
    //             <Text style={styles.textNoPrefData}>
    //             </Text>
    //         );
    //     }
    // }

    async onButtonStart() {
        this.setState({ isHungryInfo: 'done' });
        await AsyncStorage.setItem('isHungryInfo', 'done');
    }

    renderAboutInfo() {
        if (!isEmpty(this.state.resGuideLinesObj)) {
            if (this.state.resGuideLinesObj.diet_about) {
                return (
                    <View style={{ position: 'absolute', zIndex: 111, right: -20 }}>
                        <TouchableOpacity
                            style={{ width: 40, height: 40, marginLeft: 10 }}
                            onPress={() => {
                                if (this.state.resGuideLinesObj.diet_about) {
                                    this.setState({ isTipsPopVisible: true });
                                }
                                else {
                                    this.setState({ isAlert: true, alertMsg: 'No data available', alertTitle: 'Alert' });
                                }
                            }
                            }>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_info_blueround.png')}
                                style={styles.infoAppBlue}
                            />
                        </TouchableOpacity>
                    </View>
                )

            }
        }
    }


    renderAllData() {
        if (!isEmpty(this.state.resGuideLinesObj)) {
            if (this.state.resGuideLinesObj.is_paiduser === 1) {
                if (this.state.resGuideLinesObj.diet_name) {
                    return (
                        <View>
                            <View style={{
                                flexDirection: 'row',
                                marginTop: 20,
                                marginLeft: 20,
                                marginRight: 20,
                            }}>
                                <View style={{ position: 'absolute', zIndex: 111, }}>
                                    <TouchableOpacity
                                        style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                                        onPress={() => this.onBackPressed()}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_back.png')}
                                            style={styles.backImageStyle}
                                        />
                                    </TouchableOpacity>

                                </View>
                                {/* <View style={{
                                    flexDirection: 'column', flex: 1, textAlign: 'center',
                                    alignSelf: 'center', marginRight: 50,
                                    marginLeft: 50,
                                    paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
                                }}> */}
                                <Text style={styles.textHeadTitle}>{`${this.state.dietName} \n ${this.state.dietDuration}`} </Text>
                                {/* <Text style={styles.textHeadTitle}>{this.state.resGuideLinesObj.diet_name}</Text> */}
                                {/* </View> */}

                                {this.renderAboutInfo()}



                            </View>
                            {/* <View style={styles.tipsBg}>
                                <ScrollView
                                    showsVerticalScrollIndicator={true}
                                    alwaysBounceVertical={true}
                                    contentContainerStyle={{ paddingBottom: hp('3%') }}>
                                    {this.state.resGuideLinesObj.diet_tips.split('|').map((tipDesc, index) => (
                                        <View style={styles.viewListAllInner}>
                                            <View style={styles.viewBlueDot}></View>
                                            <Text style={styles.textTipsDesc}>{tipDesc}</Text>
                                        </View>
                                    ))}
                                </ScrollView>

                            </View> */}


                            <Carousel
                                // layout={'default'}
                                data={this.state.newDietArray}
                                renderItem={({ item, index }) => {
                                    return <ImageBackground source={require('../res/ic_diet_bg1.jpg')}
                                        resizeMode='cover'
                                        style={{
                                            //flex: 1,
                                            width: wp('83%'),
                                            height: hp('80%'),
                                            borderRadius: 15,
                                            marginTop: 15.
                                        }}
                                        imageStyle={{ borderRadius: 15 }}>
                                        <View style={styles.vCarouselListItemInner}>
                                            <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{ paddingBottom: hp('10%') }}>
                                                <Text style={styles.textTips}>{item.foodtype.toUpperCase()}</Text>

                                                {item.tips
                                                    ?
                                                    (
                                                        <View>
                                                            {item.tips.split('|').map((tipDesc, index) => (
                                                                <View style={styles.viewListAllInner}>
                                                                    <View style={styles.viewWhiteDot}></View>
                                                                    <Text style={styles.textTipsDesc}>{tipDesc}</Text>
                                                                </View>
                                                            ))}
                                                        </View>

                                                    )
                                                    :
                                                    (
                                                        <View>
                                                        </View>
                                                    )
                                                }


                                                {item.foodarr.map((item, key) => (
                                                    <ExpandableItemComponent
                                                        key={item.f_id}
                                                        onClickFunction={this.updateLayout.bind(this, index, key)}
                                                        onClickAddFood={this.updateSelFooditem.bind(this, index, key)}
                                                        onClickShowInst={this.instructionsOfFooditem.bind(this, index, key)}
                                                        onClickShowImage={this.showImageOfFooditem.bind(this, index, key)}
                                                        item={item}
                                                    />
                                                ))}
                                            </ScrollView>
                                        </View>
                                        <TouchableOpacity
                                            style={{
                                                width: 40,
                                                height: 40,
                                                position: 'absolute',
                                                top: 10,
                                                right: 20,
                                                alignSelf: 'center',
                                                justifyContent: 'center',
                                            }}
                                            onPress={() => { this.setState({ isDietListPopup: true }) }}>
                                            <Image
                                                source={require('../res/ic_diet_list.png')}
                                                style={{
                                                    width: 40,
                                                    height: 40,
                                                    alignSelf: 'center',

                                                }} />
                                        </TouchableOpacity>
                                    </ImageBackground>

                                }}
                                onSnapToItem={(index) => this.setState({ activeSlide1: index })}
                                sliderWidth={sliderWidth}
                                itemWidth={itemWidth}
                            />

                            {this.pagination1}

                        </View>

                    )

                }
                else {
                    return (

                        <View style={{ flexDirection: 'column', flex: 1, alignSelf: 'center', justifyContent: 'center' }}>
                            <View style={{
                                flexDirection: 'row',
                                marginTop: 30,
                            }}>
                                <View style={{ position: 'absolute', zIndex: 111 }}>
                                    <TouchableOpacity
                                        style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                                        onPress={() => this.onBackPressed()}>
                                        <Image
                                            source={require('../res/ic_back.png')}
                                            style={styles.backImageStyle}
                                        />
                                    </TouchableOpacity>
                                </View>

                            </View>
                            <View style={{ flexDirection: 'column', flex: 1, alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/img_diet_notready.png')}
                                    style={styles.imgNoSuscribe}
                                />
                                <Text style={styles.textNodataTitle}>Working on it.</Text>

                                <Text style={styles.textNodata}>{this.state.resGuideLinesObj.diet_msg}</Text>
                            </View>
                        </View>


                    )
                }
            } else {
                return (

                    <View style={{ flexDirection: 'column', flex: 1, alignSelf: 'center', justifyContent: 'center' }}>
                        <View style={{
                            flexDirection: 'row',
                            marginTop: 30,
                        }}>
                            <View style={{ position: 'absolute', zIndex: 111 }}>
                                <TouchableOpacity
                                    style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                                    onPress={() => this.onBackPressed()}>
                                    <Image
                                        source={require('../res/ic_back.png')}
                                        style={styles.backImageStyle}
                                    />
                                </TouchableOpacity>
                            </View>

                        </View>
                        <View style={{ flexDirection: 'column', flex: 1, alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/img_no_subscription.png')}
                                style={styles.imgNoSuscribe}
                            />
                            <Text style={styles.textNodataTitle}>Please Subscribe.</Text>

                            {this.state.resGuideLinesObj.diet_work_video
                                ?
                                (
                                    <TouchableOpacity
                                        activeOpacity={0.7}
                                        onPress={() => {
                                            if (Platform.OS === 'android') {
                                                Actions.newvideoplay({ videoURL: this.state.resGuideLinesObj.diet_work_video, from: 'diet video', label: `End - Diet Video` });
                                            } else {
                                                Actions.afPlayer({ videoURL: this.state.resGuideLinesObj.diet_work_video, from: 'diet video', label: `End - Diet Video` });
                                            }
                                        }}>

                                        <Text style={styles.textNodata}>
                                            <Text style={styles.textNodata}>{this.state.resGuideLinesObj.non_sub_msg_new}</Text>
                                            <Text style={styles.textClickHere}>Click Here</Text>
                                        </Text>
                                    </TouchableOpacity>

                                    // <Text style={styles.textNodata}>{this.state.resGuideLinesObj.non_sub_msg_new}</Text>
                                )
                                :
                                (
                                    <Text style={styles.textNodata}>{this.state.resGuideLinesObj.non_sub_msg}</Text>
                                )
                            }

                        </View>
                    </View>

                )
            }
        }

    }

    async onViewPlansClicked() {
        // let plan_cnt = await AsyncStorage.getItem('plan_cnt');
        // if (plan_cnt == '1') {
        //     this.getAllPlans();
        // }
        // else {
        Actions.traAllBuyPlans1({screen:"Trainee Diet"});
        // }
    }


    renderViewPlans() {
        if (!isEmpty(this.state.resGuideLinesObj)) {
            if (this.state.resGuideLinesObj.show_free_call === 1) {
                return (
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                        <TouchableOpacity
                            onPress={() => {
                                this.bottompopup.open();
                            }}
                            style={styles.buttonTuch}>
                            <Text style={styles.buttonText}>{this.state.resGuideLinesObj.fcc_btn}</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                );
            }
            else if (this.state.resGuideLinesObj.show_free_trial === 1) {
                return (
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                        <TouchableOpacity
                            onPress={() => {
                                this.bottompopup.open();
                            }}
                            style={styles.buttonTuch}>
                            <Text style={styles.buttonText}>{this.state.resGuideLinesObj.ftc_btn}</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                );
            }
            else if (this.state.resGuideLinesObj.is_paiduser === 0) {
                return (
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
                        <TouchableOpacity
                            onPress={() => Actions.traAllBuyPlans1({screen:"Trainee Diet"})}
                            style={styles.buttonTuch}>
                            <Text style={styles.buttonText}>VIEW PLANS</Text>
                        </TouchableOpacity>
                    </LinearGradient>

                );
            }
            else {
                return (
                    <View>
                    </View>
                );
            }

        }

    }

    async onRetry() {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isInternet: false });
                this.getDietPlanDetails();
                //this.getDietfoodCategories();
            }
            else {
                this.setState({ isInternet: true });
            }
        });
    }

    renderFreeTrailProcess() {

        if (this.state.freeTrailProcess === 1) {

            return (
                <View style={{ flexDirection: 'column', margin: 15, flex: 1, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>

                    {this.state.resGuideLinesObj.show_free_call === 1 ?
                        (
                            <View>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={{ uri: this.state.resGuideLinesObj.fcc_image }}
                                    style={{
                                        width: 150,
                                        height: 150,
                                        alignSelf: 'center',
                                    }}
                                />
                                <Text style={styles.freeTrialPopupTitle}>{this.state.resGuideLinesObj.fcc_title}</Text>
                                <Text style={styles.freeTrialPopupText}>{this.state.resGuideLinesObj.fcc_text}</Text>
                            </View>
                        )
                        :
                        (
                            <View>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={{ uri: this.state.resGuideLinesObj.ftc_image }}
                                    style={{
                                        width: 150,
                                        height: 150,
                                        alignSelf: 'center',
                                    }}
                                />
                                <Text style={styles.freeTrialPopupTitle}>{this.state.resGuideLinesObj.ftc_title}</Text>
                                <Text style={styles.freeTrialPopupText}>{this.state.resGuideLinesObj.ftc_text}</Text>
                            </View>
                        )
                    }

                    {this.state.popuploading
                        ?
                        (
                            <ActivityIndicator size="large" color="#000000" animating={this.state.popuploading} />
                        )
                        :
                        (

                            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                                <TouchableOpacity onPress={() => {

                                    if (this.state.resGuideLinesObj.email_verified === 1) {
                                        this.setState({ popuploading: true, onConfirmClick: true });
                                        this.saveFreeTrial();

                                    } else {
                                        this._toggleSubview();
                                        this.setState({ freeTrailProcess: 2 });
                                    }


                                }}>
                                    <Text style={styles.textSave}>Confirm</Text>
                                </TouchableOpacity>
                            </LinearGradient>

                        )
                    }


                </View>
            );

        } else if (this.state.freeTrailProcess === 2) {

            return (

                <View style={{ flexDirection: 'column', margin: 15, flex: 1, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>

                    <Animated.View style={{ transform: [{ translateX: this.state.bounceValue }] }}>

                        <Text style={{
                            width: wp('85%'),
                            fontSize: 14,
                            fontWeight: '500',
                            letterSpacing: 0.2,
                            fontFamily: 'Rubik-Medium',
                            textAlign: 'center',
                            alignSelf: 'center',
                            color: '#282c37',
                            lineHeight: 18,
                            marginBottom: 10
                        }}>{this.state.resGuideLinesObj.ev_title}</Text>

                        <Text style={{
                            fontSize: 12,
                            fontWeight: '400',
                            letterSpacing: 0.2,
                            fontFamily: 'Rubik-Regular',
                            textAlign: 'center',
                            alignSelf: 'center',
                            color: '#6d819c',
                            lineHeight: 18,
                            marginTop: 5,
                            marginLeft: 5,
                            marginRight: 5,
                            marginBottom: 25,
                        }}> {this.state.resGuideLinesObj.ev_descp} </Text>
                    </Animated.View>



                    <View style={styles.containerMobileStyle}>
                        <TextInput
                            ref={input => { this.emailTextInput = input; }}
                            style={styles.textInputStyle}
                            placeholder="Enter Email"
                            placeholderTextColor="grey"
                            keyboardType="email-address"
                            maxLength={100}
                            value={this.state.email}
                            returnKeyType='done'
                            onChangeText={text => this.setState({ email: text })}
                        />
                    </View>



                    {this.state.popuploading
                        ?
                        (
                            <ActivityIndicator size="large" color="#000000" animating={this.state.popuploading} style={{ marginTop: 40 }} />
                        )
                        :
                        (

                            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                                <TouchableOpacity onPress={() => {

                                    this.validateEmail(this.state.email);

                                }}>
                                    <Text style={styles.textSave}>Continue</Text>
                                </TouchableOpacity>
                            </LinearGradient>

                        )
                    }

                </View>

            );

        } else if (this.state.freeTrailProcess === 3) {

            return (

                <View style={{ flexDirection: 'column', margin: 15, flex: 1, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>

                    <Animated.View style={{ transform: [{ translateX: this.state.bounceValue }] }}>

                        <Text style={{
                            width: wp('85%'),
                            fontSize: 14,
                            fontWeight: '500',
                            letterSpacing: 0.2,
                            fontFamily: 'Rubik-Medium',
                            textAlign: 'center',
                            alignSelf: 'center',
                            color: '#282c37',
                            lineHeight: 18,
                            marginBottom: 10
                        }}> {this.state.emailSuccessTitle} </Text>

                        <Text style={{
                            fontSize: 12,
                            fontWeight: '400',
                            letterSpacing: 0.2,
                            fontFamily: 'Rubik-Regular',
                            textAlign: 'center',
                            alignSelf: 'center',
                            color: '#6d819c',
                            lineHeight: 18,
                            marginTop: 5,
                            marginLeft: 20,
                            marginRight: 20,
                            marginBottom: 25,
                        }}> {this.state.emailSuccessDesc} </Text>
                    </Animated.View>



                    <View style={styles.containerMobileStyle}>
                        <TextInput
                            ref={input => { this.emailTextInput = input; }}
                            style={styles.textInputStyle}
                            placeholder="Enter code"
                            placeholderTextColor="grey"
                            keyboardType="number-pad"
                            maxLength={4}
                            value={this.state.emailOtp}
                            returnKeyType='done'
                            onChangeText={text => this.setState({ emailOtp: text })}
                        />
                    </View>



                    {this.state.popuploading
                        ?
                        (
                            <ActivityIndicator size="large" color="#000000" animating={this.state.popuploading} style={{ marginTop: 40 }} />
                        )
                        :
                        (

                            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                                <TouchableOpacity onPress={() => {

                                    if (this.state.emailOtp) {
                                        this.setState({ popuploading: true, onConfirmClick: true });
                                        this.saveFreeTrial();
                                    } else {
                                        this.setState({ isAlert: true, alertMsg: 'Please enter otp sent to your email id' });
                                    }


                                    // setTimeout(() => {
                                    //   this._toggleSubview();
                                    //   this.setState({ popuploading: false, confirmSuccess: true, freeTrialSuccessTitle: 'Congratulations', freeTrialSuccessMessage: 'Dear User, as part of the free trial you shall have access to all the fitness programs and a sample diet plan for the next 7 days. Hope you have fun.', });
                                    // }, 5000)



                                }}>
                                    <Text style={styles.textSave}>Continue</Text>
                                </TouchableOpacity>
                            </LinearGradient>

                        )
                    }

                </View>

            );

        } else if (this.state.freeTrailProcess === 4) {

            return (

                <View style={{ flexDirection: 'column', margin: 15, flex: 1, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>

                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={{ uri: this.state.freeTrialObj.image }}
                        style={{
                            width: 150,
                            height: 150,
                            alignSelf: 'center',
                        }}
                    />
                    <Animated.View style={{ transform: [{ translateX: this.state.bounceValue }] }}>

                        <Text style={styles.freeTrialPopupTitle}>{this.state.freeTrialSuccessTitle}</Text>

                        <Text style={styles.freeTrialPopupText}>{this.state.freeTrialSuccessMessage}</Text>
                    </Animated.View>
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                        <TouchableOpacity onPress={() => {
                            this.bottompopup.close();
                            // Actions.traBuySuccess({ from: 'payment', sucResponse: this.state.freeTrialObj });

                            if (this.state.freeTrialObj.qst_type !== 0) {
                                if (this.state.freeTrialObj.qst_type === 1) {
                                    if (this.state.freeTrialObj.diet_imp_confirm === 0) {
                                        Actions.aidietques();
                                    } else {
                                        Actions.dietHome1({ isRefresh: 'yes' });
                                    }

                                } else if (this.state.freeTrialObj.qst_type === 2 || this.state.freeTrialObj.qst_type === 3) {
                                    if (this.state.freeTrialObj.workout_imp_confirm === 0) {
                                        Actions.traDietWorkQues({ qst_type: this.state.freeTrialObj.qst_type });
                                    } else if (this.state.freeTrialObj.diet_imp_confirm === 0) {
                                        Actions.traDietWorkQues({ qst_type: this.state.freeTrialObj.qst_type });
                                    } else {
                                        Actions.traineeWorkoutsHome({ isRefresh: 'yes' });
                                    }

                                }
                            }

                        }}>
                            <Text style={styles.textSave}>Ok. Thanks</Text>
                        </TouchableOpacity>
                    </LinearGradient>


                </View>

            );




        }
    }


    async validateEmail(email) {
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        if (!email) {
            this.setState({ isAlert: true, alertMsg: 'Please enter an email ID' });
        }
        else if (reg.test(email.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '')) === false) {
            this.setState({ isAlert: true, alertMsg: 'Please enter a valid email ID' });
        }
        else {

            this.setState({ popuploading: true, onConfirmClick: true });
            this.verifyEmailOtp();

        }
    }

    render() {
        return (
            <View
                // source={require('../res/app_bg.png')} 
                style={styles.containerStyle}>
                <View style={styles.containerStyle}>
                    <Loader loading={this.state.loading} />
                    <NoInternet
                        image={require('../res/img_nointernet.png')}
                        loading={this.state.isInternet}
                        onRetry={this.onRetry.bind(this)} />

                    {/* {this.state.resGuideLinesObj.is_paiduser === 1 && this.state.resGuideLinesObj.diet_ready === 1
                        ? (
                            
                        )
                        : (
                            <View>
                            </View>
                        )
                    } */}

                    {this.renderAllData()}
                    {this.renderViewPlans()}

                    <CustomDialog
                        visible={this.state.isAlert}
                        title={this.state.alertTitle}
                        desc={this.state.alertMsg}
                        onAccept={this.onAccept.bind(this)}
                        no=''
                        yes='Ok' />
                </View>

                <Dialog
                    onDismiss={() => {
                        this.setState({ isPopVisible: false });
                    }}
                    onTouchOutside={() => {
                        this.setState({ isPopVisible: false });
                    }}
                    width={0.7}
                    // height={0.5}
                    visible={this.state.isPopVisible}>
                    <DialogContent
                        style={{
                            backgroundColor: '#ffffff'
                        }}>
                        <View style={{ flexDirection: 'column', padding: 15 }}>
                            <View style={{ alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'column' }}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/img_pop_addfood.png')}
                                    style={styles.imgpopup}
                                />

                                <View style={{ marginTop: 10, }}></View>
                                <Text style={styles.textNodataTitle}>Add to</Text>
                                {this.renderFoodTypesPOP()}
                            </View>
                        </View>

                    </DialogContent>
                </Dialog>

                {/* call diet popup */}
                {/* <Dialog
                    onDismiss={() => {
                        this.setState({ isPopCallDiet: false });
                    }}
                    onTouchOutside={() => {
                        this.setState({ isPopCallDiet: false });
                    }}
                    width={0.7}
                    // height={0.5}
                    visible={this.state.isPopCallDiet}
                >
                    <DialogContent
                        style={{
                            backgroundColor: '#ffffff',
                        }}>
                        <View style={{ flexDirection: 'row', padding: 15, alignItems: 'center', alignSelf: 'center' }}>
                            <View style={{ alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'column' }}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={require('../res/img_call_diet_cys.png')}
                                    style={styles.imgpopup}
                                />

                                <View style={{ marginTop: 20, }}></View>
                                <Text style={styles.textNodataTitle}>Please Select</Text>

                                <View style={styles.viewCallDiet}>
                                    <TouchableOpacity
                                        onPress={() => this.setState({ isPopCallDiet: false })}>
                                        <View style={{
                                            width: wp('34%'),
                                            position: 'relative',
                                            alignItems: 'center',
                                            alignSelf: 'center',
                                            justifyContent: 'center',
                                            flexDirection: 'column'
                                        }}>

                                            <Image
                                                progressiveRenderingEnabled={true}
                                                resizeMethod="resize"
                                                source={require('../res/call_dietician.png')}
                                                style={styles.imgCallDiet}
                                            />
                                            <Text style={styles.txtCallDiet}>Call Dietician</Text>
                                        </View>

                                    </TouchableOpacity>

                                    <TouchableOpacity
                                        onPress={() => this.setState({ isPopCallDiet: false })}>

                                        <View style={{
                                            width: wp('34%'),
                                            position: 'relative',
                                            alignItems: 'center',
                                            alignSelf: 'center',
                                            justifyContent: 'center',
                                            flexDirection: 'column'
                                        }}>

                                            <Image
                                                progressiveRenderingEnabled={true}
                                                resizeMethod="resize"
                                                source={require('../res/change_yourself.png')}
                                                style={styles.imgCallDiet}
                                            />
                                            <Text style={styles.txtCallDiet}>Change by yourself</Text>
                                        </View>
                                    </TouchableOpacity>

                                </View>

                            </View>
                        </View>

                    </DialogContent>
                </Dialog> */}


                <Modal
                    backdropColor="#B4B3DB"
                    backdropOpacity={0.8}
                    animationIn="zoomInDown"
                    animationOut="zoomOutUp"
                    animationInTiming={600}
                    animationOutTiming={600}
                    backdropTransitionInTiming={600}
                    backdropTransitionOutTiming={600}
                    visible={this.state.modalVisible}
                    onRequestClose={() => { this.toggleModal(!this.state.modalVisible) }}>

                    <View style={styles.modal}>

                        <WebView
                            style={{ width: wp('100%'), height: hp('100%') }}
                            onNavigationStateChange={this.log}
                            source={{ uri: this.state.selFoodItemObj.instructions_url }} />
                    </View>

                    <TouchableOpacity style={{
                        flexDirection: 'column',
                        position: 'absolute',
                        margin: 20,
                        alignItems: 'center',
                        padding: 5,
                        justifyContent: 'center',
                        width: 34,
                        height: 34,
                        borderRadius: 17,
                        backgroundColor: 'white',
                        zIndex: 100,
                        shadowColor: 'rgba(0, 0, 0, 0.08)',
                        shadowOffset: { width: 5, height: 0 },
                        shadowOpacity: 0.25,
                        shadowRadius: 10,
                        elevation: 6,
                    }}
                        onPress={() => { this.toggleModal(!this.state.modalVisible) }}>
                        <Image
                            style={{
                                width: 19,
                                height: 16,
                                tintColor: '#8c52ff',
                                alignSelf: 'center',
                            }}
                            source={require('../res/ic_back.png')}
                        />
                    </TouchableOpacity>
                </Modal>

                <Modal
                    transparent
                    animationType={'fade'}
                    visible={this.state.isTipsPopVisible}
                    onRequestClose={() => {
                        this.setState({ isTipsPopVisible: false });
                    }}>

                    <View style={{
                        backgroundColor: 'rgba(0,0,0,0.5)',
                        flex: 1,
                        justifyContent: 'center',
                    }}>
                        <Image
                            source={require('../res/img_about_diet.jpg')}
                            style={{
                                width: wp('90%'),
                                height: hp('95%'),
                                borderRadius: 15,
                                alignSelf: 'center',
                                resizeMode: 'cover',
                            }} />

                        <View style={{
                            flexDirection: 'column', alignSelf: 'center', width: wp('90%'),
                            height: hp('95%'), position: 'absolute'
                        }}>

                            <Text style={styles.textPopTitle}>About Diet</Text>

                            <Image
                                source={require('../res/img_about_diet_info.png')}
                                style={{
                                    width: 200,
                                    height: 200,
                                    marginTop: 20,
                                    marginBottom: 20,
                                    alignSelf: 'center',
                                }} />


                            {this.state.resGuideLinesObj.diet_about
                                ?
                                (
                                    <ScrollView
                                        style={{ marginBottom: 100, }}
                                        showsVerticalScrollIndicator={true}
                                        contentContainerStyle={{
                                            paddingBottom: hp('10%')
                                        }}>
                                        {this.state.resGuideLinesObj.diet_about.split('|').map((tipDesc, index) => (
                                            <View style={styles.viewListAllInner1}>
                                                <View style={styles.viewBlueDot}></View>
                                                <Text style={styles.textPopDesc1}>{tipDesc}</Text>
                                            </View>
                                        ))}
                                    </ScrollView>
                                )
                                :
                                (
                                    <View>
                                    </View>
                                )}
                        </View>

                        <LinearGradient colors={['#8c52ff', '#8c52ff']} style={{
                            position: 'absolute',
                            bottom: 40,
                            width: 100,
                            alignItems: 'center',
                            padding: 5,
                            justifyContent: 'center',
                            alignSelf: 'center',
                            margin: 25,
                            borderRadius: 25,
                            zIndex: 100,
                        }}>
                            <TouchableOpacity onPress={() => {
                                this.setState({ isTipsPopVisible: false });
                            }}>
                                <Text style={styles.textClose}>Close</Text>
                            </TouchableOpacity>
                        </LinearGradient>
                    </View>


                </Modal>

                <Modal
                    transparent={true}
                    animationType={'none'}
                    visible={this.state.isShowItemImage}
                    onRequestClose={() => {
                        this.setState({ isShowItemImage: false });
                    }}>
                    <View style={{
                        backgroundColor: 'rgba(0,0,0,0.5)',
                        flex: 1,
                        justifyContent: 'center',
                    }}>
                        <View style={{ flex: 1, backgroundColor: '#000000', justifyContent: 'center' }}>
                            {this.state.itemImg
                                ?
                                (
                                    <FastImage
                                        style={{ width: wp('99%'), height: hp('99%'), flex: 1, alignSelf: 'center' }}
                                        source={{
                                            uri: this.state.itemImg,
                                            headers: { Authorization: '12345' },
                                            priority: FastImage.priority.high,
                                        }}
                                        resizeMode={FastImage.resizeMode.contain}
                                    />
                                )
                                :
                                (
                                    <Image
                                        style={{ width: wp('95%'), height: wp('98%'), alignSelf: 'center' }}
                                        source={require('../res/ic_noimage.png')}

                                    />
                                )

                            }

                            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={{
                                position: 'absolute',
                                width: 100,
                                bottom: 40,
                                // left: 0,
                                alignItems: 'center',
                                padding: 5,
                                justifyContent: 'center',
                                alignSelf: 'center',
                                margin: 25,
                                borderRadius: 25,
                                zIndex: 100,
                            }}>
                                <TouchableOpacity onPress={() => {
                                    this.setState({ isShowItemImage: false });
                                }}>
                                    <Text style={styles.textClose}>Close</Text>
                                </TouchableOpacity>
                            </LinearGradient>

                        </View>

                    </View>
                </Modal>

                <Modal
                    transparent
                    animationType={'fade'}
                    visible={this.state.isDietListPopup}
                    onRequestClose={() => {
                        this.setState({ isDietListPopup: false });
                    }}>

                    <View style={{
                        backgroundColor: 'rgba(0,0,0,0.5)',
                        flex: 1,
                        justifyContent: 'center',
                    }}>
                        <View style={styles.content}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                style={{
                                    width: wp('80%'),
                                    height: undefined,
                                    justifyContent: 'center',
                                    alignSelf: 'center',
                                    borderTopLeftRadius: 10,
                                    borderTopRightRadius: 10,
                                    aspectRatio: 2 / 1,
                                }}
                                source={{ uri: this.state.resGuideLinesObj.dietplan_image }}
                            />

                            <FlatList
                                style={{ marginTop: 10, marginBottom: 10 }}
                                contentContainerStyle={{ paddingBottom: hp('1%') }}
                                showsVerticalScrollIndicator={false}
                                data={this.state.diet_list}
                                // extraData={this.state}
                                keyExtractor={(item,index) => "diet_list"+index}
                                ItemSeparatorComponent={this.FlatListItemSeparator}
                                renderItem={({ item }) => {
                                    return <View style={{ flexDirection: 'column', alignItems: 'center', alignSelf: 'center', justifyContent: 'center' }}>
                                        <Text style={styles.textWorkName}>{`${item.diet_category}`}</Text>
                                        {item.frud_id != 0
                                            ? (
                                                <TouchableOpacity
                                                    key={item.frud_id}
                                                    style={styles.containerListStyle}
                                                    onPress={() => {
                                                        if (item.frud_id != 0) {
                                                            this.setState({ isDietListPopup: false });
                                                            this.setState({ newDietArray: item.food, dietName: item.diet_category, dietDuration: item.diet_duration });
                                                        }

                                                    }}>

                                                    <View style={{ flexDirection: 'column', alignItems: 'center', alignContent: 'center' }}>

                                                        <Text style={styles.txtDietPlanName}>{`${item.diet_duration}`}</Text>

                                                    </View>

                                                </TouchableOpacity>
                                            ) : (
                                                <View
                                                    key={item.frud_id}
                                                    style={styles.containerListStyle}>

                                                    <View style={{ flexDirection: 'column', alignItems: 'center', alignContent: 'center' }}>

                                                        <Text style={styles.txtDietPlanName}>{`${item.diet_duration}`}</Text>

                                                    </View>

                                                </View>

                                            )}

                                    </View>
                                }}
                            />

                            <View style={styles.viewButtons}>

                                <TouchableOpacity
                                    style={{ width: '100%', backgroundColor: '#8c52ff', borderBottomLeftRadius: 10, borderBottomRightRadius: 10, padding: 2 }}
                                    onPress={() => {
                                        this.setState({ isDietListPopup: false });
                                    }}>
                                    <Text style={styles.buttonText}>Close</Text>
                                </TouchableOpacity>


                            </View>
                        </View>
                    </View>

                    {/* <View style={{
                        backgroundColor: 'rgba(0,0,0,0.5)',
                        flex: 1,
                        justifyContent: 'center',
                    }}>
                        <Image
                            source={require('../res/img_about_diet.jpg')}
                            style={{
                                width: wp('90%'),
                                height: hp('95%'),
                                borderRadius: 15,
                                alignSelf: 'center',
                                resizeMode: 'cover',
                            }} />

                        <View style={{
                            flexDirection: 'column', alignSelf: 'center', width: wp('90%'),
                            height: hp('95%'), position: 'absolute'
                        }}>

                            <Text style={styles.textPopTitle}>Diet Information</Text>

                            <Image
                                source={require('../res/img_about_diet_info.png')}
                                style={{
                                    width: 200,
                                    height: 200,
                                    marginTop: 20,
                                    marginBottom: 20,
                                    alignSelf: 'center',
                                }} />


                            <FlatList
                                style={{ marginTop: 10, }}
                                contentContainerStyle={{ paddingBottom: hp('1%') }}
                                showsVerticalScrollIndicator={false}
                                data={this.state.diet_list}
                                // extraData={this.state}
                                keyExtractor={item => item.frud_id}
                                ItemSeparatorComponent={this.FlatListItemSeparator}
                                renderItem={({ item }) => {
                                    return <View >
                                        <Text style={styles.textWorkName}>{`${item.diet_category}`}</Text>
                                        <View
                                            key={item.frud_id}
                                            style={styles.containerListStyle}>

                                            <View style={{ flexDirection: 'column', alignItems: 'flex-start', alignContent: 'flex-start', }}>

                                                <Text style={styles.txtDietPlanName}>{`${item.diet_name}`}</Text>
                                                <Text style={styles.txtDietPlanName}>{`${item.diet_duration}`}</Text>

                                            </View>

                                        </View>
                                    </View>
                                }}
                            />
                        </View>

                        <LinearGradient colors={['#8c52ff', '#8c52ff']} style={{
                            position: 'absolute',
                            bottom: 40,
                            width: 100,
                            alignItems: 'center',
                            padding: 5,
                            justifyContent: 'center',
                            alignSelf: 'center',
                            margin: 25,
                            borderRadius: 25,
                            zIndex: 100,
                        }}>
                            <TouchableOpacity onPress={() => {
                                this.setState({ isDietListPopup: false });
                            }}>
                                <Text style={styles.textClose}>Close</Text>
                            </TouchableOpacity>
                        </LinearGradient>
                    </View> */}


                </Modal>

                <RBSheet
                    ref={ref => {
                        this.bottompopup = ref;
                    }}
                    height={popupHeight}
                    closeOnDragDown={false}
                    closeOnPressMask={false}
                    closeOnPressBack={!this.state.onConfirmClick}
                    onClose={() => {
                        this.setState({ freeTrailProcess: 1 })
                    }}
                    customStyles={{
                        container: {
                            borderTopLeftRadius: 10,
                            borderTopRightRadius: 10
                        }
                    }}
                >

                    {this.state.onConfirmClick
                        ?
                        (
                            <View></View>
                        )
                        :
                        (
                            <TouchableOpacity style={{ position: 'absolute', right: 10, top: 10, padding: 10, zIndex: 1 }}
                                onPress={() => { this.bottompopup.close() }}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    style={{
                                        width: 19,
                                        height: 19,
                                        alignSelf: 'flex-start',
                                        tintColor: 'black',
                                    }}
                                    source={require('../res/ic_cross_black.png')}
                                />
                            </TouchableOpacity>

                        )}

                    {this.renderFreeTrailProcess()}

                    <CustomDialog
                        visible={this.state.isAlert}
                        title='Alert'
                        desc={this.state.alertMsg}
                        onAccept={this.onAccept.bind(this)}
                        no=''
                        yes='Ok' />

                </RBSheet>

            </View >
        )
    }
}


const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        height: '100%',
        flex: 1,
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'center',
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        // marginRight: 50,
        // marginLeft: 50,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    viewCalDetails: {
        flexDirection: 'column',
        marginTop: 20,
    },
    textAdd: {
        color: '#8c52ff',
        fontFamily: 'Rubik-Medium',
        alignSelf: "flex-end",
        fontSize: 14,
        flex: 1,
        textAlign: 'right',
        fontWeight: '500',
    },
    nutrition: {
        flexDirection: 'column',
        marginTop: 5,
    },
    textNutTitle: {
        fontSize: 12,
        fontWeight: '500',
        color: '#2d3142',
        flex: 1,
        fontFamily: 'Rubik-Regular',
        letterSpacing: 0.23,
    },
    textIngredients: {
        fontSize: 14,
        fontWeight: '500',
        color: '#2d3142',
        margin: 10,
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.23,
    },
    textNutTitle1: {
        fontSize: 12,
        fontWeight: '500',
        color: '#2d3142',
        textAlign: 'right',
        fontFamily: 'Rubik-Medium',
        marginLeft: 8,
        letterSpacing: 0.23,
    },
    foodImg: {
        width: 40,
        height: 40,
        alignSelf: 'center',
    },
    foodImg1: {
        width: 40,
        height: 40,
        alignSelf: 'center',
    },
    textNoPrefData: {
        fontSize: 11,
        fontWeight: '500',
        fontFamily: 'Rubik-Regular',
        textAlign: 'center',
        alignSelf: 'center',
        marginLeft: 50,
        marginTop: 20,
        marginBottom: 20,
        marginRight: 50,
        color: '#6d819c',
    },
    linearGradient: {
        width: '90%',
        height: 50,
        bottom: 0,
        borderRadius: 25,
        justifyContent: 'center',
        position: 'absolute',
        alignSelf: 'center',
        marginBottom: 15,
    },
    buttonTuch:
    {
        backgroundColor: 'transparent',
    },
    buttonText: {
        fontSize: 15,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    textNodata: {
        width: wp('85%'),
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
    },
    textNodataTitle: {
        width: wp('85%'),
        fontSize: 16,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
    },
    imgNoSuscribe: {
        width: 270,
        height: 270,
        alignSelf: 'center',
    },
    bottom: {
        flex: 1,
        justifyContent: 'flex-end',
        backgroundColor: '#ffffff',
    },
    bottomHome: {
        width: wp('100%'),
        height: hp('10%'),
        justifyContent: 'flex-end',
        backgroundColor: '#ffffff',
    },
    bottomHome1: {
        width: wp('100%'),
        height: hp('10%'),
        backgroundColor: '#ffffff',
    },
    imgBottomMenu: {
        width: 23,
        height: 23,
        alignSelf: 'center',
    },
    viewListAllInner: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignSelf: 'flex-start',
        alignItems: 'center',
        marginTop: 3,
        marginBottom: 3,
    },
    viewListAllInner1: {
        width: wp('80%'),
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignSelf: 'center',
        alignItems: 'center',
        marginTop: 3,
        marginBottom: 3,
    },
    desc: {
        fontSize: 12,
        marginLeft: 10,
        marginRight: 10,
        fontWeight: '400',
        color: '#2d3142',
        lineHeight: 15,
        textAlign: 'left',
        fontFamily: 'Rubik-Regular',
    },
    viewBlueDot: {
        width: 10,
        height: 10,
        backgroundColor: '#8c52ff',
        alignSelf: 'flex-start',
        marginTop: 3,
        borderRadius: 5,
    },
    containerListStyle: {
        width: wp('70%'),
        marginTop: hp('1%'),
        backgroundColor: '#8c52ff',
        flexDirection: 'row',
        borderColor: '#ddd',
        // borderRadius: 15,
        borderRadius: 6,
        padding: 5,
        position: 'relative',
        marginLeft: wp('5%'),
        marginRight: wp('5%'),
        marginBottom: hp('1%'),
        alignItems: 'center',
        justifyContent: 'center',
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    textWorkName: {
        fontSize: 16,
        width: wp('80%'),
        fontWeight: '500',
        // paddingRight: 20,
        fontFamily: 'Rubik-Medium',
        color: '#282c37',
        alignSelf: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        marginTop: 5,
        letterSpacing: 1,
    },
    textFoodType: {
        fontSize: 10,
        width: wp('25%'),
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        lineHeight: 18,
    },
    textFoodType1: {
        fontSize: 10,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        lineHeight: 18,
    },
    textPer: {
        fontSize: 10,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        alignSelf: 'center',
        textAlign: 'left',
        // lineHeight: 18,
    },
    textTitle: {
        fontSize: 10,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        lineHeight: 18,
        marginLeft: 3,
    },
    imgAdd: { width: 25, height: 25, alignSelf: 'center', },
    modal: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#ffffff',
        justifyContent: 'center',
    },
    viewCallDiet: {
        flexDirection: 'row',
        alignSelf: 'center',
        justifyContent: 'center',
        marginTop: 10,
        alignItems: 'center',
    },
    imgCallDiet: {
        height: 60,
        width: 60,
        alignSelf: 'center',
    },
    txtCallDiet: {
        alignSelf: 'center',
        fontSize: 12,
        fontWeight: '400',
        color: '#2d3142',
        padding: 10,
        textAlign: 'center',
        letterSpacing: 0.26,
        fontFamily: 'Rubik-Medium',
    },
    txtDietPlanName: {
        fontSize: 12,
        fontWeight: '400',
        color: '#ffffff',
        padding: 5,
        letterSpacing: 0.26,
        fontFamily: 'Rubik-Regular',
        textAlign: 'center',
        letterSpacing: 0.2,
    },
    imgpopup: {
        width: 180,
        height: 180,
        alignSelf: 'center',
    },
    imgPopBL: {
        width: 40,
        height: 40,
        alignSelf: 'center',
    },
    linearGradientPopup: {
        borderRadius: 15,
        justifyContent: 'center',
        alignSelf: 'center',
        alignSelf: "center",
        alignItems: 'center',
        marginTop: 10,
    },
    textAddBreakOpt: {
        width: wp('40%'),
        fontSize: 13,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        borderRadius: 15,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 8,
        paddingBottom: 8,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#ffffff',
    },
    viewCarBack: {
        width: 16,
        height: 16,
        borderRadius: 8,
        borderColor: '#6d819c',
        borderWidth: 0.5,
        alignSelf: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    textCarBack: {
        fontSize: 10,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
    },
    vCarouselListItem: {
        width: wp('82%'),
        justifyContent: 'flex-start',
        flexDirection: 'column',
        alignSelf: 'flex-start',
    },
    vCarouselListItemInner: {
        width: wp('83%'),
        height: hp('85%'),
        justifyContent: 'flex-start',
        flexDirection: 'column',
        borderRadius: 15,
        padding: 10,
        position: 'relative',
    },
    textTips: {
        fontSize: 14,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        textAlign: 'left',
        marginTop: 10,
        marginBottom: 10,
        alignSelf: 'flex-start',
        color: '#282c37',
    },
    viewWhiteDot: {
        width: 10,
        height: 10,
        backgroundColor: '#282c37',
        alignSelf: 'flex-start',
        marginTop: 3,
        borderRadius: 5,
    },
    textTipsDesc: {
        fontSize: 11,
        marginLeft: 10,
        marginRight: 10,
        fontWeight: '400',
        color: '#282c37',
        lineHeight: 15,
        textAlign: 'left',
        fontFamily: 'Rubik-Regular',
    },
    vFooditemList: {
        width: wp('78%'),
        marginTop: hp('1%'),
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'column',
        borderColor: '#ddd',
        borderRadius: 6,
        alignItems: 'center',
        justifyContent: 'flex-start',
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    textfoodItemName: {
        fontSize: 14,
        // width: wp('70%'),
        fontWeight: '500',
        paddingRight: 20,
        fontFamily: 'Rubik-Medium',
        color: '#282c37',
    },
    imgDropdown: {
        width: 25,
        height: 13,
        alignSelf: 'center',
        tintColor: '#8c52ff',
        transform: [{ rotate: '180deg' }]
    },
    imgDropdown1: {
        width: 25,
        height: 13,
        alignSelf: 'center',
        tintColor: '#8c52ff',
    },
    nutrition1: {
        flexDirection: 'row',
        alignSelf: 'flex-start',
        paddingLeft: 10,
        paddingRight: 10,
        paddingBottom: 10,
    },
    viewItemBlueDot: {
        width: 10,
        height: 10,
        alignSelf: 'flex-start',
        backgroundColor: '#8c52ff',
        borderRadius: 10 / 2,
    },
    nutritionInnerView: {
        flexDirection: 'column',
        marginLeft: wp('3%'),
        justifyContent: 'flex-start',
        alignSelf: 'flex-start',
        flex: 1,
    },

    // Expandable list
    container: {
        flex: 1,
        paddingTop: 30,
        backgroundColor: '#F5FCFF',
    },
    header: {
        backgroundColor: '#F5FCFF',
        padding: 16,
    },
    separator: {
        height: 0.5,
        backgroundColor: '#808080',
        width: '95%',
        marginLeft: 16,
        marginRight: 16,
    },
    text: {
        fontSize: 16,
        color: '#606070',
        padding: 10,
    },
    content: {
        backgroundColor: 'white',
        // padding: 20,
        width: wp('80%'),
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        position: 'relative',
        alignContent: 'center',
        borderRadius: 10,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },
    textPopTitle: {
        fontSize: 16,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
        marginTop: 5,
    },
    textPopDesc: {
        fontSize: 11,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        alignSelf: 'center',
        color: '#6d819c',
        flex: 1,
        lineHeight: 16,
        marginLeft: 8,
    },
    textPopDesc1: {
        fontSize: 11,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        alignSelf: 'center',
        color: '#2d3142',
        lineHeight: 16,
        marginLeft: 8,
    },
    nutLevelBack: {
        borderRadius: 2,
        alignSelf: 'flex-start',
        padding: 5,
        backgroundColor: '#e1ddf5',
        justifyContent: 'flex-start',
    },
    nutLevel: {
        fontSize: 10,
        fontWeight: '500',
        color: '#8c52ff',
        textAlign: 'left',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.17,
        alignSelf: 'center',
    },
    textOption: {
        width: wp('78%'),
        marginTop: hp('1%'),
        fontSize: 14,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        textAlign: 'left',
        alignSelf: 'flex-start',
        color: '#282c37',
    },
    tipsBg: {
        width: wp('90%'),
        height: hp('15%'),
        marginLeft: wp('5%'),
        marginRight: wp('5%'),
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderRadius: 15,
        marginTop: 15,
        padding: 10,
        position: 'relative',
        alignSelf: 'center',
        shadowColor: 'rgba(0, 0, 0, 0.08)',
        shadowOffset: { width: 5, height: 0 },
        shadowOpacity: 0.25,
        shadowRadius: 10,
        elevation: 6,
    },
    infoAppBlue: {
        width: 25,
        height: 25,
        marginRight: 10,
        alignSelf: 'center',
        tintColor: '#8c52ff',
    },
    textClose: {
        fontSize: 10,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 5,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#ffffff',
    },
    textClickHere: {
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#8c52ff',
        lineHeight: 18,
    },
    linearGradientEnroll: {
        borderRadius: 15,
        justifyContent: 'center',
        alignSelf: 'center',
        alignSelf: "center",
        alignItems: 'center',
        marginTop: 20,
        // position:'absolute',
        // bottom:0,
    },
    textSave: {
        width: wp('40%'),
        fontSize: 14,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        borderRadius: 15,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 8,
        paddingBottom: 8,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#ffffff',
    },
    freeTrialPopupTitle: {
        width: wp('85%'),
        fontSize: 14,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
        marginTop: 10
    },
    freeTrialPopupText: {
        fontSize: 12,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
        marginTop: 5,
        marginLeft: 5,
        marginRight: 5,
        marginBottom: 15,
    },
    containerMobileStyle: {
        borderBottomWidth: 1,
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 15,
        position: 'relative',
        alignSelf: 'center',
        marginLeft: 20,
        marginRight: 20,
        marginTop: 10,
        marginBottom: 20,
        padding: 8,
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    textInputStyle: {
        height: 40,
        flex: 1,
        fontSize: 13,
        marginLeft: 10,
        fontFamily: 'Rubik-Regular',
        color: '#2d3142',
    },
    viewButtons: {
        width: '100%',
        flexDirection: 'row',
        alignSelf: 'center',
        borderBottomLeftRadius: 10, borderBottomRightRadius: 10
    },
});

const mapStateToProps = state => {

    return {};
};

export default withNavigationFocus(
    connect(
        mapStateToProps,
        {},
    )(Trainee_DietHome1),
);