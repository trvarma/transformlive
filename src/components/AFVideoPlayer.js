
import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  BackHandler,
  Dimensions,
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux';
import Orientation from 'react-native-orientation';
import Video from 'react-native-video';
import LogUtils from '../utils/LogUtils.js';
import { forbidFunction } from '../utils/ScreenshotUtils.js';
const theme = {
  title: '#FFF',
  more: '#446984',
  center: '#7B8F99',
  fullscreen: '#446984',
  volume: '#A5957B',
  scrubberThumb: '#234458',
  scrubberBar: '#DBD5C7',
  seconds: '#DBD5C7',
  duration: '#DBD5C7',
  progress: '#446984',
  loading: '#DBD5C7'
}
const initial = Orientation.getInitialOrientation();
let orientationchange = 'PORTRAIT';

// let url = '';

class AFVideoPlayer extends Component {
  constructor(props) {
    super(props);
    // this.player && this.player.presentFullscreenPlayer()
    this.state = {
      currentTime: 0,
      duration: 0,
      isFullScreen: false,
      isLoading: true,
      paused: false,
      screenType: 'contain',
      volume: 1,
      isBuffering: true,
      isKey: 1,
      url: '',
    };
  }

  async onBackPressed() {
    Orientation.lockToPortrait();
    //console.log('Orientation', orientationchange);
    if (orientationchange === 'PORTRAIT') {
      Actions.pop();
    } else {
      Orientation.lockToPortrait();
    }
  }

  async componentDidMount() {
    forbidFunction();
    Orientation.unlockAllOrientations();
    Orientation.addOrientationListener(this._orientationDidChange);
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      this.onBackPressed();
      return true;
    });

    if (this.props.item) {
      if (this.props.from === 'challengedetails') {
        this.setState({ url: this.props.item.my_video });
        // url = this.props.item.my_video;
      } else if (this.props.from === 'feed') {
        this.setState({ url: this.props.item.file_url });
        // url = this.props.item.file_url;
      }
    } else if (this.props.videoURL) {
      this.setState({ url: this.props.videoURL });
      // url = this.props.videoURL;
    }
  }

  componentWillMount() {
    if (initial === 'PORTRAIT') {
      // do something
    } else {
      // do something else
    }
  }

  _orientationDidChange = (orientation) => {
    initial === orientation;
    orientationchange === orientation;
    if (orientation === 'LANDSCAPE') {
      // do something with landscape layout
      Orientation.lockToLandscape();
    } else {
      // do something with portrait layout
      Orientation.lockToPortrait();
    }
  }

  _onFullscreenPlayerWillDismiss() {
    Orientation.lockToPortrait();
    Actions.pop();
  }

  _onFullscreenPlayerWillPresent() {
    this.player && this.player.presentFullscreenPlayer();
  }

  _orientationRemove = (orientation) => {
    orientation.lockToPortrait();
  }

  componentWillUnmount() {
    this.backHandler.remove();
    Orientation.getOrientation((err, orientation) => {
      //console.log(`Current Device Orientation: ${orientation}`);
    });


    // Remember to remove listener
    Orientation.removeOrientationListener(this._orientationRemove);
  }

  onFullScreen(status) {
    this.setState({ isFullScreen: true });
  }

  onMorePress() {
    Actions.pop();
  }
  handleFullScreenToPortrait = () => {
    this.player.presentFullscreenPlayer();
  };

  handleFullScreenDismiss = () => {
    Orientation.lockToPortrait();
    Actions.pop();
  };

  onEnd = () => {
    if (this.props.label) {
      LogUtils.firebaseEventLog('programclick', {
        p_id: 1245,
        p_category: 'Programs',
        p_name: this.props.label,
      });
    }
  }


  onBuffer = () => {
    this.setState({ isBuffering: true });
  }

  videoError = () => {
    if (this.state.isKey === 1) {
      this.setState({ isKey: 2, url: this.props.item.my_tmp_video });
    } else if (this.state.isKey === 2) {
      this.setState({ isKey: 1 });
    }
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={styles.container}>
          <Video
            ref={ref => this.player = ref}
            // key={this.state.isKey}
            source={{ uri: this.state.url }}
            style={styles.video}
            onEnd={this.onEnd}
            volume={this.state.volume}
            controls={false}
            resizeMode="contain"
            fullscreen={true}
            ignoreSilentSwitch={"ignore"}
            onLoadStart={this.handleFullScreenToPortrait}
            onFullscreenPlayerDidDismiss={() =>
              this.handleFullScreenDismiss()
            }
          // onError={this.videoError}
          // onFullscreenPlayerWillPresent={this._onFullscreenPlayerWillPresent()}
          // onFullscreenPlayerWillDismiss={this._onFullscreenPlayerWillDismiss()}
          // onFullscreenPlayerDidDismiss={this._onFullscreenPlayerWillDismiss()}
          />

          {/* <Video source={{ uri: url }}   // Can be a URL or a local file.
            ref={(ref) => {
              this.player = ref
            }}
            key={this.state.isKey}                                   // Store reference
            onBuffer={this.onBuffer}              // Callback when remote video is buffering
            onError={this.videoError}
            paused={this.state.paused} 
            minLoadRetryCount={10}              // Callback when video cannot be loaded
            style={styles.backgroundVideo} /> */}
        </View>
      </View >
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#000000'
  },
  toolbar: {
    marginTop: 30,
    backgroundColor: 'white',
    padding: 10,
    borderRadius: 5,
  },
  mediaPlayer: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: 'black',
  },
  containerTop: {
    flexDirection: 'column',
    position: 'absolute',
    top: 0,
  },
  backImageStyle: {
    width: 20,
    height: 16,
    margin: hp('3%'),
    alignSelf: 'flex-start',
    tintColor: 'white',
  },
  video: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').width * (9 / 16),
    backgroundColor: 'black',
  },
  backgroundVideo: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
});

export default AFVideoPlayer;