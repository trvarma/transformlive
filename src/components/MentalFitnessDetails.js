
import React, { Component } from 'react';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    BackHandler,
    ScrollView,
    FlatList,
    Dimensions,
    KeyboardAvoidingView, StatusBar,
    Animated,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import { CustomDialog, Loader } from './common';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import NetInfo from "@react-native-community/netinfo";
import { BASE_URL, SWR } from '../actions/types';
const sliderheight = Dimensions.get('window').height;
import DeviceInfo from 'react-native-device-info';
import LogUtils from '../utils/LogUtils.js';
import _ from 'lodash';
let screenHeight = Math.round(Dimensions.get('window').height);
let popupHeight = screenHeight / 2;
import { allowFunction } from '../utils/ScreenshotUtils.js';

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}
function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

const initialLayout = { width: Dimensions.get('window').width };
let colors = ['#CFD7E2', '#E0CBC8', '#DEE2D3', '#D4D3DB'];//, '#EADACB'

class MentalFitnessDetails extends Component {
    constructor(props) {
        super(props);
        LogUtils.showSlowLog(this);
        this.state = {
            index: 0,
            setIndex: 0,
            routes: [
                { key: 'first', title: 'TRAINER' },
                { key: 'second', title: 'INTRODUCTION' },
                { key: 'third', title: 'PLAN DETAIL' },
            ],
            tab: 1,
            isAlert: false,
            loading: false,
            isSubTrue: false,
            alertMsg: '',
            alertTitle: '',
            resObj: '',
            width: Dimensions.get('window').width,
            height: Dimensions.get('window').height,
            arrAllPlans: [],
            isWeekPopup: false,
            selSubWeekObj: {},

            bounceValue: new Animated.Value(1000),  //This is the initial position of the subview
            confirmSuccess: false,
            freeTrialObj: {},
            buttonState: '',
            freeTrialSuccessTitle: '',
            freeTrialSuccessMessage: '',
            onConfirmClick: false,
            freeTrialConfirmImg: '',
            freeTrialConfirmTitle: '',
            freeTrialConfirmText: '',
            popuploading: false,

        };

        this.onLayout = this.onLayout.bind(this);
        this.getTraineeAndPlanDetails = this.getTraineeAndPlanDetails.bind(this);
    }

    onLayout(e) {
        this.setState({
            width: Dimensions.get('window').width,
            height: Dimensions.get('window').height,
        });
    }

    async componentDidMount() {

        allowFunction();

        if (popupHeight < 370) {
            popupHeight = 370;
        }

        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ loading: true });
                LogUtils.infoLog1('pId', this.props.pId)

                this.getTraineeAndPlanDetails();
            }
            else {
                this.setState({ isAlert: true, alertMsg: 'Please enable your internet connection and try again' });
            }
        });

        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    async getTraineeAndPlanDetails() {
        let token = await AsyncStorage.getItem('token');
        LogUtils.infoLog1('token', token);
        LogUtils.infoLog1('Body', JSON.stringify({
            pg_id: this.props.pId,
        }))
        fetch(
            `${BASE_URL}/trainee/mentalpgmdet`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    pg_id: this.props.pId,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('ProgramDetails data', data);
                // LogUtils.infoLog1('Curr Day ', data.data.current_day);

                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false, resObj: data.data });

                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title, });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title, });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: SWR, alertTitle: data.title, });
            });
    }

    async onBackPressed() {
        Actions.pop();
    }

    FlatListItemSeparator = () => {
        return (
            <View
                style={{
                    height: 2,
                    width: "100%",
                    backgroundColor: "transparent",
                }}
            />
        );
    }

    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }

        this.setState({ isAlert: false, alertMsg: '' });
        if (this.state.isSubTrue) {
            this.setState({ isSubTrue: false, loading: true });
            this.getTraineeAndPlanDetails();
        }
    }

    renderProgramVideoPlay(selItem) {
        if (selItem.is_restday === 0) {
            if (this.state.resObj.is_paiduser === 1) {
                if (this.state.resObj.is_subscribe === 1) {
                    LogUtils.firebaseEventLog('video', {
                        p_id: this.props.pId,
                        p_category: 'Programs',
                        p_name: `Start - ${this.state.resObj.programname}-${this.state.resObj.current_day[0].title}`,
                    });
                    Actions.newvideoplay({ pdObj: this.state.resObj.current_day[0], planObj: this.state.resObj.current_day[0], from: 'details', p_id: this.state.resObj.p_id, label: `End - ${this.state.resObj.programname}-${this.state.resObj.current_day[0].title}` });
                }
                else {
                    this.setState({ isAlert: true, alertMsg: 'Please enroll the fitness program to start', alertTitle: 'Alert' });
                }
            }
            else {
                if (selItem.is_buy.toLowerCase() === 'free') {
                    LogUtils.firebaseEventLog('video', {
                        p_id: this.props.pId,
                        p_category: 'Programs',
                        p_name: `Start - ${this.state.resObj.programname}-${this.state.resObj.current_day[0].title}`,
                    });
                    Actions.newvideoplay({ pdObj: this.state.resObj.current_day[0], planObj: this.state.resObj.current_day[0], from: 'details', p_id: this.state.resObj.p_id, label: `End - ${this.state.resObj.programname}-${this.state.resObj.current_day[0].title}` });
                }
                else {
                    Actions.myOrders({ from: 'premiumday' });
                }
            }
        }
    }

    callPlanDayIntro(selItem) {
        // if (selItem.is_restday === 0) {
        // if (this.state.resObj.is_paiduser === 1) {
        // if (this.state.resObj.is_subscribe === 1) {

        if (selItem.can_play === 1) {
            if (Platform.OS === 'android') {
                Actions.newvideoplay({ videoURL: selItem.cld_video, from: 'plandetails', label: `End - ${selItem.title}` });
            } else {
                Actions.afPlayer({ videoURL: selItem.cld_video, from: 'plandetails', label: `End - ${selItem.title}` });
            }
        }
        else {
            Actions.myOrders({ from: 'premiumday' });
        }
        // }
        // else {
        //     this.setState({ isAlert: true, alertMsg: 'Please enroll the fitness program to start', alertTitle: 'Alert' });
        // }

        // }
        // else {
        //     if (selItem.is_buy.toLowerCase() === 'free') {
        //         Actions.traPlanIntro({ pdObj: selItem, from: 'details', p_id: this.state.resObj.p_id });
        //     }
        //     else {
        //         Actions.myOrders({ from: 'premiumday' });
        //     }
        // }
        // }
    }

    renderTodayWorkout() {
        if (this.state.resObj && !isEmpty(this.state.resObj)) {

            return (
                <TouchableOpacity activeOpacity={0.9} style={styles.containerListStyle}
                    onPress={() => {
                        // this.renderProgramVideoPlay(this.state.resObj.current_day[0]);
                    }}>

                    {this.state.resObj.img
                        ? (
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={{ uri: this.state.resObj.img }}
                                style={styles.todayImage}
                            />
                        )
                        : (
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_noimage.png')}
                                style={styles.todayImage}
                            />
                        )
                    }

                    <Text style={styles.todayText}>{this.state.resObj.tr_name}</Text>

                </TouchableOpacity>
            );
        }
    }

    ThirdRoute = () => (
        <ScrollView contentContainerStyle={{ paddingBottom: hp('10%') }}>
            <View style={styles.planDuration}>
                <Text style={styles.textWorkPlan}>{this.state.resObj.name}</Text>
                <View style={{ flexDirection: 'row', marginTop: 2, }}>
                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={require('../res/ic_alarm.png')}
                        style={{ width: 12, height: 12, marginRight: 7, alignSelf: 'center', tintColor: '#ffffff' }}
                    />
                    <Text style={styles.textTitleWhite}>{this.state.resObj.duration}</Text>
                </View>
            </View>

            {this.renderTodayWorkout()}
            {/* {this.renderWeekDropDown()} */}

            <View style={{ height: 20 }}></View>
            <View style={{ marginLeft: wp('5%') }}>
                <Text style={styles.textHeight}>PROGRAM SCHEDULE</Text>
            </View>

            <FlatList
                style={{ marginTop: 5 }}
                contentContainerStyle={{ paddingBottom: hp('5%') }}
                data={this.state.resObj.workouts}
                keyExtractor={(item, index) => "workouts"+index}
                showsVerticalScrollIndicator={false}
                scrollEnabled={false}
                ItemSeparatorComponent={this.FlatListItemSeparator}
                renderItem={({ item, index }) => {
                    return <View>
                        <TouchableOpacity activeOpacity={0.8} style={styles.containerListStyle} onPress={() => {
                            this.callPlanDayIntro(item);
                        }}>
                            <View style={{ flexDirection: 'row', alignItems: 'flex-start', margin: 5, }}>
                                {item.img
                                    ? (
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={{ uri: item.img }}
                                            style={styles.profileImage}
                                        />
                                    )
                                    : (
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_noimage.png')}
                                            style={styles.profileImage}
                                        />
                                    )
                                }
                                <View style={{ flexDirection: 'column', flex: 1, alignItems: 'flex-start', alignContent: 'flex-start', justifyContent: 'center' }}>
                                    <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
                                        <Text style={styles.countryText}>{`${item.name.toUpperCase()}`}</Text>
                                        <Text style={styles.isfree}>{`${item.is_buy.toUpperCase()}`}</Text>
                                    </View>
                                    <Text style={styles.textWorkName}>{`${item.title}`}</Text>

                                    <View style={{ flexDirection: 'row', paddingLeft: 10, paddingTop: 2, paddingBottom: 10, }}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_alarm.png')}
                                            style={{ width: 10, height: 10, marginRight: 4, alignSelf: 'center' }}
                                        />
                                        <Text style={styles.textTitle}>{`${item.duration}`} mins</Text>
                                        {/* <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_cal.png')}
                                            style={{ width: 12, height: 12, marginRight: 4, marginLeft: 10, alignSelf: 'center' }}
                                        />
                                        <Text style={styles.textTitle}>{`${item.cal_burn}`} cal</Text> */}
                                    </View>

                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                }} />
        </ScrollView>
    );

    _renderTabBar = props => {
        const inputRange = props.navigationState.routes.map((x, i) => i);
        return (
            <TabBar
                style={{ backgroundColor: '#FFFFFF', elevation: 0, borderColor: '#fff', borderBottomWidth: 1, }}
                labelStyle={styles.textPlan}
                {...props}
                indicatorStyle={{ backgroundColor: '#8c52ff', height: 3 }}
                renderLabel={({ route }) => (
                    <View>
                        <Text style={{
                            fontFamily: 'Rubik-Medium',
                            alignSelf: "center",
                            fontSize: 12,
                            fontWeight: '500',
                            lineHeight: 18,
                            color: route.key === props.navigationState.routes[props.navigationState.index].key ?
                                '#8c52ff' : '#6D819C'
                        }}>
                            {route.title}
                        </Text>
                    </View>
                )}
            />
        );
    };

    render() {
        return (
            <KeyboardAvoidingView onLayout={this.onLayout} keyboardVerticalOffset={Platform.select({ ios: 0, android: -20 })} style={styles.containerStyle} behavior="padding" enabled>
                <Loader loading={this.state.loading} />

                <View style={{
                    flexDirection: 'row',
                    margin: 20,
                }}>
                    <View style={{ position: 'absolute', zIndex: 111 }}>
                        <TouchableOpacity
                            style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                            onPress={() => this.onBackPressed()}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_back.png')}
                                style={styles.backImageStyle}
                            />
                        </TouchableOpacity>
                    </View>

                    <Text style={styles.textHeadTitle}>Program Details</Text>
                </View>
                {this.ThirdRoute()}

                {/* {this.renderBottom()} */}

                <CustomDialog
                    visible={this.state.isAlert}
                    title={this.state.alertTitle}
                    desc={this.state.alertMsg}
                    onAccept={this.onAccept.bind(this)}
                    no=''
                    yes='Ok' />
            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: StatusBar.currentHeight,
        backgroundColor: '#fff'
    },
    scene: {
    },
    playContainer: {
        flexDirection: 'row',
        alignContent: 'center',
        position: 'absolute',
        right: 20,
        bottom: 30,
        alignSelf: 'center',
        alignContent: 'center',
        justifyContent: 'center',
    },
    containerStyle: {
        width: '100%',
        height: sliderheight,
        backgroundColor: '#ffffff',
        flex: 1,
    },
    container: {
        flex: 1,
        justifyContent: "flex-start"
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'center',
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    textPlan: {
        color: '#6D819C',
        fontFamily: 'Rubik-Medium',
        alignSelf: 'center',
        textAlign: 'center',
        fontSize: 11,
        fontWeight: '500',
    },
    textTitle: {
        fontSize: 11,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        lineHeight: 18,
    },
    textTitleWhite: {
        fontSize: 10,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        color: '#ffffff',
        lineHeight: 18,
    },
    play: {
        width: 60,
        height: 60,
        alignSelf: "center",
        position: 'absolute',
    },
    textDesc: {
        fontSize: 13,
        fontWeight: '400',
        fontFamily: 'Rubik-Regular',
        marginLeft: wp('5%'),
        marginRight: wp('1%'),
        lineHeight: 18,
        color: '#282c37',
    },
    planDuration: {
        justifyContent: 'flex-start',
        flexDirection: 'column',
        backgroundColor: '#8c52ff',
        padding: 15,
    },
    textWorkPlan: {
        fontSize: 16,
        lineHeight: 18,
        color: '#ffffff',
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
    },
    containerListStyle: {
        width: wp('92%'),
        marginTop: hp('1%'),
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 6,
        position: 'relative',
        marginLeft: 15,
        alignSelf: 'center',
        marginRight: 15,
        alignItems: 'center',
        justifyContent: 'flex-start',
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    countryText: {
        color: '#282c37',
        fontFamily: 'Rubik-Medium',
        fontSize: 10,
        padding: 10,
        flex: 1,
        fontWeight: '500',
    },
    isfree: {
        color: '#8c52ff',
        fontFamily: 'Rubik-Medium',
        fontSize: 10,
        padding: 10,
        fontWeight: '500',
    },
    textWorkName: {
        fontSize: 14,
        width: wp('73%'),
        fontWeight: '500',
        paddingLeft: 10,
        // paddingTop: 10,
        paddingRight: 10,
        fontFamily: 'Rubik-Medium',
        color: '#282c37',
    },
    linearGradient: {
        width: '90%',
        height: 50,
        bottom: 0,
        borderRadius: 25,
        justifyContent: 'center',
        position: 'absolute',
        alignSelf: 'center',
        marginBottom: 15,
    },
    buttonTuch:
    {
        backgroundColor: 'transparent',
    },
    textHeight: {
        fontSize: 10,
        fontWeight: '500',
        fontFamily: 'Rubik-Regular',
        textAlign: 'left',
        color: '#6d819c',
    },
    wrapper: { flexDirection: 'row' },
    title: { flex: 1, backgroundColor: '#f6f8fa' },
    row: { height: 60 },
    text: { textAlign: 'center', fontSize: 8, padding: 5 },
    profileImage: {
        width: 40,
        height: 70,
        backgroundColor: "#D8D8D8",
        borderColor: "#979797",
        borderRadius: 5,
        alignSelf: 'center',
        resizeMode: "contain",
        justifyContent: 'center',
    },
    todayImage: {
        width: wp('92%'),
        height: undefined,
        alignSelf: 'center',
        resizeMode: 'cover',
        borderRadius: 6,
        aspectRatio: 16 / 9,
    },
    todayText: {
        position: 'absolute',
        left: 10,
        bottom: 5,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        textAlign: 'left',
        alignSelf: 'center',
        backgroundColor: '#8c52ff',
        padding: 5,
        borderRadius: 15,
        color: '#ffffff',
        fontSize: 10,
    },
    textWeekTit: {
        fontSize: 16,
        fontWeight: '500',
        marginLeft: 20,
        marginTop: 10,
        fontFamily: 'Rubik-Medium',
        color: '#282c37',
    },
    viewSelectWeek: {
        width: wp('92%'),
        borderRadius: 25,
        borderColor: '#8c52ff',
        // // borderStyle: 'solid',
        borderWidth: 0.5,
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: 15,
        // backgroundColor: '#ffffff',
    },
    viewSelectWeek1: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        alignContent: 'center',
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 10,
        paddingBottom: 10,
    },
});

export default (MentalFitnessDetails);
