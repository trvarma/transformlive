import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    BackHandler,
    ScrollView,
    ImageBackground,
    FlatList,
    TextInput,
    Platform,
    Modal,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import { NoInternet, HomeDialog, Loader } from './common';
import LinearGradient from 'react-native-linear-gradient';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import NetInfo from "@react-native-community/netinfo";
import { BASE_URL, SWR, RZP_KEY } from '../actions/types';
import RazorpayCheckout from 'react-native-razorpay';
import DeviceInfo from 'react-native-device-info';
import Dialog, {
    DialogContent,
} from 'react-native-popup-dialog';
import LogUtils from '../utils/LogUtils.js';
import { allowFunction } from '../utils/ScreenshotUtils.js'; 

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();

    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
}

class Trainee_BuyPlanPayMethod extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            isAlert: false,
            alertTitle: '',
            alertMsg: '',
            isInternet: false,
            arrPaymentMethods: [],
            selPayMethodObj: {},
        };
        this.getAllPaymentMethods = this.getAllPaymentMethods.bind(this);
    }

    async componentDidMount() {
        allowFunction();
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.getAllPaymentMethods();
            }
            else {
                this.setState({ isInternet: true });
            }
        });
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }


    onBackPressed() {
        Actions.pop();
    }

    async getAllPaymentMethods() {
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/paymenttypes`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('arrPaymentMethods ', data);

                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ arrPaymentMethods: data.data });
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: SWR, alertTitle: 'Alert' });
            });
    }

    pressPaymentMethod = (hey) => {
        this.setState({ selPayMethodObj: hey });
        if (hey.can_subscribe === 1) {
            this.getSubscriptionDetails();
        }
        else {
            this.getOrderDetails();
        }
    }

    renderAllPaymentMethods() {
        if (this.state.loading === false) {
            if (Array.isArray(this.state.arrPaymentMethods) && this.state.arrPaymentMethods.length) {
                return (
                    <View style={{
                        flex: 1,
                        alignItems: 'center',
                        backgroundColor: '#ffffff',
                        justifyContent: 'center',
                    }}>

                        <TouchableOpacity style={styles.viewTop}
                            onPress={() => { this.toggleSelPayMethodModal(!this.state.selPayMethodPop) }}>
                            <Image
                                source={require('../res/ic_back.png')}
                                style={styles.backImageStyle}
                            />
                        </TouchableOpacity>

                        <Text style={styles.TextHeaderStyle}>Select your payment method</Text>
                        <FlatList
                            contentContainerStyle={{ paddingBottom: hp('10%') }}
                            showsVerticalScrollIndicator={false}
                            data={this.state.arrPaymentMethods}
                            keyExtractor={item => item.pt_id}
                            ItemSeparatorComponent={this.FlatListSeparatorPayMethods}
                            renderItem={({ item }) => {
                                return <TouchableOpacity key={item.pt_id} onPress={() => {
                                    this.pressPaymentMethod(item)
                                }}>
                                    <View style={styles.vPayBg}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            source={{ uri: item.img }}
                                            style={{
                                                width: 30,
                                                height: 30,
                                                alignSelf: 'center',
                                            }}
                                        />

                                        <View style={styles.performancecolumn}>
                                            <Text style={styles.performancetextStyle}>{item.title}</Text>
                                            <Text style={styles.waterdranktextStyle}>{item.descp}</Text>
                                        </View>
                                    </View>

                                </TouchableOpacity>
                            }} />
                    </View>
                )
            }
            else {
                return (
                    <View></View>
                )
            }
        }
    }

    async onRetry() {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isInternet: false });
                this.getAllPaymentMethods();
            }
            else {
                this.setState({ isInternet: true });
            }
        });
    }

    FlatListSeparatorPayMethods = () => {
        return (
            <View
                style={{
                    height: 12,
                    width: 1,
                    backgroundColor: "transparent",
                }}
            />
        );
    }

    render() {
        return (
            <ImageBackground source={require('../res/app_bg.png')} style={styles.containerStyle}>

                <Loader loading={this.state.loading} />

                <NoInternet
                    image={require('../res/img_nointernet.png')}
                    loading={this.state.isInternet}
                    onRetry={this.onRetry.bind(this)} />

                {this.renderAllPaymentMethods()}

            </ImageBackground>
        )
    }
}


const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        height: '100%',
        flex: 1,
    },
    backImageStyle: {
        width: 19,
        height: 16,
        alignSelf: 'flex-start',
    },
    TextHeaderStyle: {
        color: '#2d3142',
        fontFamily: 'Rubik-Medium',
        fontSize: 18,
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 20,
        letterSpacing: 0.2,
        alignSelf: 'center',
        textAlign: 'center',
    },
    viewTop: {
        flexDirection: 'column',
        // position: 'absolute',
        margin: 20,
        alignItems: 'center',
        padding: 5,
        justifyContent: 'center',
        alignSelf: 'flex-start',
        width: 34,
        height: 34,
        borderRadius: 17,
        // backgroundColor: 'white',
        // shadowColor: 'rgba(0, 0, 0, 0.08)',
        // shadowOffset: { width: 5, height: 0 },
        // shadowOpacity: 0.25,
        // shadowRadius: 10,
        // elevation: 6,
    },
    vPayBg: {
        width: wp('90%'),
        marginLeft: wp('5%'),
        marginRight: wp('5%'),
        // backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderRadius: 6,
        position: 'relative',
        alignSelf: 'center',
        borderColor: '#b0b0b0',
        borderWidth: 0.3,
        // shadowColor: "#000",
        // shadowOffset: { width: 5, height: 0 },
        // shadowOpacity: 0.25,
        // shadowRadius: 10,
        // elevation: 5,
        // marginTop: 2.5,
        padding: 12,
    },
    performancecolumn: {
        flexDirection: 'column',
        flex: 1,
        alignItems: 'flex-start',
        marginLeft: 12,
    },
    performancetextStyle: {
        fontSize: 14,
        fontWeight: '500',
        color: '#2d3142',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    waterdranktextStyle: {
        fontSize: 12,
        color: '#C3C1CE',
        textAlign: 'left',
        lineHeight: 16,
        marginTop: 3,
        fontFamily: 'Rubik-Regular',
    },
});

export default Trainee_BuyPlanPayMethod;