import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    FlatList,
    Image,
    TouchableOpacity,
    BackHandler,
    Text,
    ImageBackground,
    Dimensions,
    Animated,
    ActivityIndicator,
    TextInput,
} from 'react-native';
import { HomeDialog, CustomDialog, Loader, NoInternet } from './common';
import AsyncStorage from '@react-native-community/async-storage';
import { Actions } from 'react-native-router-flux';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import LinearGradient from 'react-native-linear-gradient';
import NetInfo from "@react-native-community/netinfo";
import { BASE_URL, SWR } from '../actions/types';
import DeviceInfo from 'react-native-device-info';
import Dialog, {
    DialogContent,
} from 'react-native-popup-dialog';
import LogUtils from '../utils/LogUtils.js';
import _ from 'lodash';
import { allowFunction } from '../utils/ScreenshotUtils.js';

import RBSheet from "react-native-raw-bottom-sheet";
let screenHeight = Math.round(Dimensions.get('window').height);
let popupHeight = screenHeight / 2;

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

class MyOrders extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isAlert: false,
            loading: false,
            alertMsg: '',
            arrAllPlans: [],
            programsArray: [],
            noData: '',
            pageNo: 1,
            isNoInternet: false,
            isSuccess: false,
            sucMsg: '',
            titMsg: '',
            selUpId: 0,
            isPopupVisible: false,
            canSubDesc: '',
            isPopModify: false,
            myPlanObj: {},
            buttonText: 'View Plans',
            modifyPlanObj: {},
            showFreeTrail: 0,

            bounceValue: new Animated.Value(1000),  //This is the initial position of the subview
            confirmSuccess: false,
            freeTrialObj: {},
            buttonState: '',
            freeTrialSuccessTitle: '',
            freeTrialSuccessMessage: '',
            onConfirmClick: false,
            freeTrialConfirmImg: '',
            freeTrialConfirmTitle: '',
            freeTrialConfirmText: '',
            popuploading: false,

            isCancelInfo: false,
            cancelDescription: '',
            cancelTitle: '',

            email: '',
            freeTrailProcess: 1,
            emailOtp: '',
            emailSuccessTitle: '',
            emailSuccessDesc: '',
            resObj: {},


        };
        this.getTraineeSubscriptions = this.getTraineeSubscriptions.bind(this);
    }

    async componentDidMount() {
        allowFunction();

        if (popupHeight < 370) {
            popupHeight = 370;
        }

        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isNoInternet: false });
                this.getTraineeSubscriptions();
            }
            else {
                this.setState({ isNoInternet: true });
            }
        });
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }


    async getTraineeSubscriptions() {
        this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/getuserplan`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('statusmessage', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ resObj: data });
                    this.setState({ email: data.email });
                    // LogUtils.infoLog1('this.state.resObj', this.state.resObj);
                    if (data.data.length > 0) {
                        // if (this.props.from === 'premiumday') {
                        //     this.setState({ buttonText: 'Modify' });
                        // }
                        setTimeout(() => {
                            this.setState({ programsArray: data.data });
                            // this.setState({ email: data.email });
                            data.data.map((item) => {
                                if (item.can_modify === 1) {
                                    this.setState({ modifyPlanObj: item })
                                }
                            })

                            this.setState({ loading: false });
                        }, 100);
                    }
                    else {
                        this.setState({ loading: false, programsArray: [] });
                    }
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, });
                    } else {
                        this.setState({ loading: false, noData: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: SWR, });
            });
    }

    async cancelSubscriptions() {
        this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/unsubscribeplan`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    up_id: this.state.selUpId,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('statusmessage', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false, programsArray: [] });
                    Actions.traBuySuccess({ from: 'cancel', sucResponse: data });
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message, });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: JSON.stringify(error), });
            });
    }

    async saveFreeTrial() {
        let token = await AsyncStorage.getItem('token');
        let urlType = '';
        if (this.state.resObj.show_free_call === 1) {
            urlType = 'save_freetrial2';
        }
        else {
            urlType = 'save_freetrial';
        }

        LogUtils.infoLog(`Url : ${BASE_URL}/trainee/${urlType}`);

        fetch(
            `${BASE_URL}/trainee/${urlType}`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    otp: this.state.emailOtp,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this._toggleSubview();
                    this.setState({ popuploading: false, freeTrialObj: data, confirmSuccess: true, freeTrialSuccessTitle: data.title, freeTrialSuccessMessage: data.message, freeTrailProcess: 4 });
                    if (this.state.resObj.show_free_call === 1) {
                        LogUtils.appsFlyerEventLog('freeconsultationstarted', {
                            desc: 'Free Consultation Started',
                        });
                    }
                    else {
                        LogUtils.appsFlyerEventLog('freetrialstarted', {
                            desc: 'Free Trial Started',
                        });
                    }
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ popuploading: false, isAlert: true, alertMsg: data.message, onConfirmClick: false });
                    } else {
                        this.setState({ popuploading: false, isAlert: true, alertMsg: data.message, onConfirmClick: false });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ popuploading: false, isAlert: true, alertMsg: JSON.stringify(error), onConfirmClick: false });
            });
    }

    async verifyEmailOtp() {
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/verifyemailotp`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify({
                    email: this.state.email,
                }),
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this._toggleSubview();
                    this.setState({ popuploading: false, confirmSuccess: true, emailSuccessTitle: data.title, emailSuccessDesc: data.message, freeTrailProcess: 3, onConfirmClick: false });
                    // this.setState({ popuploading: false, freeTrialObj: data.data,confirmSuccess: true, freeTrialSuccessTitle:  data.data.title, freeTrialSuccessMessage:  data.data.message, });

                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ popuploading: false, isAlert: true, alertMsg: data.message, onConfirmClick: false });
                    } else {
                        this.setState({ popuploading: false, isAlert: true, alertMsg: data.message, onConfirmClick: false });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ popuploading: false, isAlert: true, alertMsg: JSON.stringify(error), onConfirmClick: false });
            });
    }

    async onAccept() {

        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
        this.setState({ isAlert: false, alertMsg: '' });

    }

    navigateCancelSub() {
        Actions.traBuySuccess({ from: 'cancel', sucResponse: data });
    }

    FlatListItemSeparator1 = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: 1,
                    backgroundColor: "transparent",
                }}
            />
        );
    }

    onBackPressed() {
        if (this.state.isPopupVisible) {
            this.setState({ isPopupVisible: false });
        }
        else if (this.state.isPopModify) {
            this.setState({ isPopModify: false });
        }
        else {
            if (this.props.from === 'premiumday') {
                // Actions.traineeWorkoutsHome();
                Actions.pop();
            } else {
                Actions.popTo('traineeHome');
            }
        }
    }

    async onDeny() {
        this.setState({ isSuccess: false, sucMsg: '', titMsg: '' });
    }

    async onSuccess() {
        this.setState({ isSuccess: false, sucMsg: '', titMsg: '' });
        this.cancelSubscriptions();

    }

    renderCancelModify(item) {
        if (item.can_cancel === 1 && item.can_modify === 1) {
            return (
                <View style={{
                    flexDirection: 'row',
                    marginTop: 5,
                }}>
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.lgCancel}>
                        <TouchableOpacity onPress={() => {
                            this.setState({ selUpId: item.up_id });

                            this.setState({ isPopupVisible: true, canSubDesc: item.cancel_msg });

                        }}
                            style={styles.buttonTuch}>
                            <Text style={styles.buttonText}>Cancel</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                    <View style={{ width: 1 }}></View>
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.lgModify}>
                        <TouchableOpacity onPress={() => {
                            this.setState({ isPopModify: true, myPlanObj: item });

                        }}
                            style={styles.buttonTuch}>
                            <Text style={styles.buttonText}>Modify</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                </View>
            );
        }
        else if (item.can_cancel === 1 && item.can_modify === 0) {
            return (
                <View style={{
                    flexDirection: 'row',
                    marginTop: 5,
                }}>
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.lgCancel1}>
                        <TouchableOpacity onPress={() => {
                            this.setState({ selUpId: item.up_id });
                            this.setState({ isPopupVisible: true, canSubDesc: item.cancel_msg });
                        }}
                            style={styles.buttonTuch}>
                            <Text style={styles.buttonText}>Cancel</Text>
                        </TouchableOpacity>
                    </LinearGradient>

                </View>
            );
        }
        else if (item.can_cancel === 0 && item.can_modify === 1) {
            return (
                <View style={{
                    flexDirection: 'row',
                    marginTop: 5,
                }}>
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.lgModify1}>
                        <TouchableOpacity onPress={() => {
                            this.setState({ isPopModify: true, myPlanObj: item });

                        }}
                            style={styles.buttonTuch}>
                            <Text style={styles.buttonText}>Modify</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                </View>
            );
        }
    }
    renderExpire(item) {
        if (item.remain_days > 0) {
            return (
                <View style={{ flexDirection: 'row', marginTop: 5, justifyContent: 'flex-start', alignItems: 'center', alignContent: 'center', }}>
                    <Image
                        source={require('../res/ic_alarm.png')}
                        style={{ width: 12, height: 12, marginRight: 5, alignSelf: 'center' }}
                    />
                    <Text style={styles.textTitle}>Expires in {`${item.remain_days}`} days</Text>
                </View>
            );
        }
    }

    renderAllSuscriprionsList() {

        if (this.state.loading === false) {
            if (!isEmpty(this.state.resObj) && this.props.from === 'premiumday') {
                return (
                    <View style={{ flexDirection: 'column', flex: 1, alignSelf: 'center', justifyContent: 'center' }}>
                        <View style={{
                            flexDirection: 'row',
                            marginTop: 30,
                        }}>
                            <View style={{ position: 'absolute', zIndex: 111 }}>
                                <TouchableOpacity
                                    style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                                    onPress={() => this.onBackPressed()}>
                                    <Image
                                        source={require('../res/ic_back.png')}
                                        style={styles.backImageStyle}
                                    />
                                </TouchableOpacity>
                            </View>

                        </View>
                        <View style={{ flexDirection: 'column', flex: 1, alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>


                            <Image
                                source={require('../res/img_no_subscription.png')}
                                style={styles.imgNoSuscribe}
                            />
                            <Text style={styles.textNodataTitle}>{this.state.resObj.no_plan_title1}</Text>
                            {this.props.appText
                                ?
                                (
                                    <Text style={styles.textNodata}> {this.props.appText}</Text>
                                )
                                :
                                (
                                    <Text style={styles.textNodata}>{this.state.resObj.no_plan_text2}</Text>
                                )
                            }
                        </View>
                    </View>
                );
            }
            else if (!isEmpty(this.state.resObj)) {
                if (Array.isArray(this.state.programsArray) && this.state.programsArray.length) {
                    return (
                        <View>
                            <View style={{
                                flexDirection: 'row',
                                margin: 20,
                            }}>
                                <View style={{ position: 'absolute', zIndex: 111 }}>
                                    <TouchableOpacity
                                        style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                                        onPress={() => this.onBackPressed()}>
                                        <Image
                                            source={require('../res/ic_back.png')}
                                            style={styles.backImageStyle}
                                        />
                                    </TouchableOpacity>
                                </View>

                                <Text style={styles.textHeadTitle}>My Subscriptions</Text>
                            </View>

                            <FlatList
                                contentContainerStyle={{ paddingBottom: hp('30%') }}
                                showsVerticalScrollIndicator={false}
                                data={this.state.programsArray}
                                keyExtractor={item => "programsArray"+item.up_id}
                                ItemSeparatorComponent={this.FlatListItemSeparator1}
                                renderItem={({ item }) => {
                                    return <View key={item.up_id} style={styles.aroundListStyle}>
                                        <View style={{ width: wp('90%'), flexDirection: 'column', padding: 15, alignItems: 'flex-start', alignContent: 'center', justifyContent: 'center', }}>
                                            <Text style={styles.textWorkName}>{`${item.pc_name.toUpperCase()}`}</Text>
                                            <Text style={styles.textSubPlan}>{`${item.psc_name}`}</Text>
                                            <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                                <Text style={styles.textDates}>{item.start_date}</Text>
                                                <Image
                                                    source={require('../res/ic_arr_right.png')}
                                                    style={{
                                                        width: 14,
                                                        alignSelf: 'center',
                                                        marginRight: 10,
                                                        marginLeft: 10,
                                                        tintColor: '#6d819c',
                                                        height: 12,
                                                    }} />
                                                <Text style={styles.textDates}>{item.end_date}</Text>
                                            </View>
                                            <View style={styles.nutritionInnerView}>
                                                <Text style={styles.textNutTitle}>Rs.{`${item.amount}`}</Text>
                                                {/* {item.can_subscribe === 0
                                                    ? (
                                                        <Text style={styles.textNutDesc}>/One Call</Text>
                                                    )
                                                    : ( */}
                                                <Text style={styles.textNutDesc}>/{item.period}</Text>
                                                {/* )
                                                } */}


                                            </View>
                                            {this.renderExpire(item)}

                                        </View>
                                        {this.renderCancelModify(item)}
                                        {item.is_ios_iap === 1
                                            ? (
                                                <TouchableOpacity
                                                    onPress={() =>

                                                        this.setState({ isCancelInfo: true, cancelDescription: item.cancel_msg, cancelTitle: item.cancel_title })
                                                    }
                                                    style={{
                                                        flexDirection: 'row', alignItems: 'center', position: 'absolute',
                                                        right: 10,
                                                        top: 10,
                                                    }}>
                                                    <Image
                                                        progressiveRenderingEnabled={true}
                                                        resizeMethod="resize"
                                                        source={require('../res/ic_info_blueround.png')}
                                                        style={{
                                                            width: 25,
                                                            height: 25,

                                                        }}
                                                    />
                                                </TouchableOpacity>

                                            ) : (
                                                <View></View>

                                            )}
                                    </View>
                                }} />
                        </View>

                    );

                }
                else {
                    return (
                        <View style={{ flexDirection: 'column', flex: 1, alignSelf: 'center', justifyContent: 'center' }}>
                            <View style={{
                                flexDirection: 'row',
                                marginTop: 30,
                            }}>
                                <View style={{ position: 'absolute', zIndex: 111 }}>
                                    <TouchableOpacity
                                        style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                                        onPress={() => this.onBackPressed()}>
                                        <Image
                                            source={require('../res/ic_back.png')}
                                            style={styles.backImageStyle}
                                        />
                                    </TouchableOpacity>
                                </View>

                            </View>
                            <View style={{ flexDirection: 'column', flex: 1, alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>


                                <Image
                                    source={require('../res/img_no_subscription.png')}
                                    style={styles.imgNoSuscribe}
                                />
                                <Text style={styles.textNodataTitle}>{this.state.resObj.no_plan_title1}</Text>


                                <Text style={styles.textNodata}>{this.state.resObj.no_plan_text1}</Text>


                            </View>
                        </View>

                    );
                }
            }
            else {
                return (
                    <View></View>
                );
            }
        }
        else {
            return (
                <View></View>
            );
        }
    }

    // async getAllPlans() {
    //     this.setState({ loading: true });
    //     let token = await AsyncStorage.getItem('token');

    //     fetch(
    //         `${BASE_URL}/trainee/plans2`,
    //         {
    //             method: 'GET',
    //             headers: {
    //                 Accept: 'application/json',
    //                 'Content-Type': 'application/json',
    //                 Authorization: `Bearer ${token}`,
    //             },
    //         },
    //     )
    //         .then(processResponse)
    //         .then(res => {
    //             const { statusCode, data } = res;
    //             LogUtils.infoLog1('statusCode', statusCode);
    //             LogUtils.infoLog1('data', data);

    //             if (statusCode >= 200 && statusCode <= 300) {
    //                 if (data.data.length) {
    //                     this.setState({ loading: false, arrAllPlans: data.data });
    //                     var mySelItem = _.cloneDeep(this.state.arrAllPlans[0].data[0]);
    //                     Actions.traBuyPlanDetNew({ selPlan: mySelItem });
    //                 }
    //                 else {
    //                     this.setState({ loading: false, noDataMsg: 'No data available at this moment' });
    //                 }

    //             } else {
    //                 if (data.message === 'You are not authenticated!') {
    //                     this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
    //                 } else {
    //                     this.setState({ loading: false, isAlert: true, alertMsg: data.message, alertTitle: data.title });
    //                 }
    //             }
    //         })
    //         .catch(function (error) {
    //             this.setState({ loading: false, isAlert: true, alertMsg: SWR, alertTitle: 'Alert' });
    //         });
    // }

    async onButtonClicked() {
        // let plan_cnt = await AsyncStorage.getItem('plan_cnt');

        if (this.state.buttonText === 'View Plans') {
            // if (plan_cnt == '1') {
            //     this.getAllPlans();
            // }
            // else {
            Actions.traAllBuyPlans1({screen:"My Subscriptions"});
            // }

        } else {
            Actions.traBuyPlanModify({ myPlanObj: this.state.modifyPlanObj});
        }
        // if (this.props.from) {
        //     if (this.props.from === 'home') {
        //         Actions.traBuyPlanModify({ myPlanObj: this.state.programsArray[0] });
        //     } else if (this.props.from === 'premiumday') {
        //         Actions.traAllBuyPlans();
        //     }
        // } else {
        //     Actions.traAllBuyPlans();
        // }
    }

    renderBottomButton() {

        if (!isEmpty(this.state.resObj)) {
            if (Array.isArray(this.state.programsArray) && this.state.programsArray.length) {
                // if (plan_cnt === '1') {
                //     return (
                //         <View></View>
                //     );
                // }
                // else {
                return (
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient1}>
                        <TouchableOpacity onPress={() => {
                            this.onButtonClicked();

                        }}
                            style={styles.buttonTuch}>
                            <Text style={styles.buttonText}>{this.state.buttonText}</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                );
                // }
            }
            else if (this.state.resObj.show_free_call === 1) {
                return (
                    <View>
                        <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient1}>
                            <TouchableOpacity onPress={() => {
                                this.bottompopup.open();
                            }}
                                style={styles.buttonTuch}>
                                <Text style={styles.buttonText}>{this.state.resObj.fcc_btn}</Text>
                            </TouchableOpacity>
                        </LinearGradient>
                    </View>
                );
            }
            else if (this.state.resObj.show_free_trial === 1) {
                return (
                    <View>
                        <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient1}>
                            <TouchableOpacity onPress={() => {
                                this.bottompopup.open();
                            }}
                                style={styles.buttonTuch}>
                                <Text style={styles.buttonText}>{this.state.resObj.ftc_btn}</Text>
                            </TouchableOpacity>
                        </LinearGradient>
                    </View>
                );
            }
            else {
                return (
                    <View>
                        <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient1}>
                            <TouchableOpacity onPress={() => {
                                this.onButtonClicked();

                            }}
                                style={styles.buttonTuch}>
                                <Text style={styles.buttonText}>{this.state.buttonText}</Text>
                            </TouchableOpacity>
                        </LinearGradient>
                    </View>
                );
            }
        }
        else {
            return (
                <View></View>
            );
        }
    }

    async onRetry() {
        NetInfo.fetch().then(state => {
            if (state.isConnected) {
                this.setState({ isNoInternet: false });
                this.getTraineeSubscriptions();
            }
            else {
                this.setState({ isNoInternet: true });
            }
        });
    }

    _toggleSubview() {
        Animated.timing(
            this.state.bounceValue,
            {
                toValue: 0,
                duration: 1000
            }
        ).start();
    }

    renderFreeTrailProcess() {

        if (this.state.freeTrailProcess === 1) {

            return (
                <View style={{ flexDirection: 'column', margin: 15, flex: 1, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>

                    {this.state.resObj.show_free_call === 1 ?
                        (
                            <View>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={{ uri: this.state.resObj.fcc_image }}
                                    style={{
                                        width: 150,
                                        height: 150,
                                        alignSelf: 'center',
                                    }}
                                />
                                <Text style={styles.freeTrialPopupTitle}>{this.state.resObj.fcc_title}</Text>
                                <Text style={styles.freeTrialPopupText}>{this.state.resObj.fcc_text}</Text>
                            </View>
                        )
                        :
                        (
                            <View>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={{ uri: this.state.resObj.ftc_image }}
                                    style={{
                                        width: 150,
                                        height: 150,
                                        alignSelf: 'center',
                                    }}
                                />
                                <Text style={styles.freeTrialPopupTitle}>{this.state.resObj.ftc_title}</Text>
                                <Text style={styles.freeTrialPopupText}>{this.state.resObj.ftc_text}</Text>
                            </View>
                        )
                    }

                    {this.state.popuploading
                        ?
                        (
                            <ActivityIndicator size="large" color="#000000" animating={this.state.popuploading} />
                        )
                        :
                        (

                            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                                <TouchableOpacity onPress={() => {

                                    if (this.state.resObj.email_verified === 1) {
                                        this.setState({ popuploading: true, onConfirmClick: true });
                                        this.saveFreeTrial();

                                    } else {
                                        this._toggleSubview();
                                        this.setState({ freeTrailProcess: 2 });
                                    }


                                }}>
                                    <Text style={styles.textSave}>Confirm</Text>
                                </TouchableOpacity>
                            </LinearGradient>

                        )
                    }
                </View>
            );

        } else if (this.state.freeTrailProcess === 2) {

            return (

                <View style={{ flexDirection: 'column', margin: 15, flex: 1, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>

                    <Animated.View style={{ transform: [{ translateX: this.state.bounceValue }] }}>

                        <Text style={{
                            width: wp('85%'),
                            fontSize: 14,
                            fontWeight: '500',
                            letterSpacing: 0.2,
                            fontFamily: 'Rubik-Medium',
                            textAlign: 'center',
                            alignSelf: 'center',
                            color: '#282c37',
                            lineHeight: 18,
                            marginBottom: 10
                        }}>{this.state.resObj.ev_title}</Text>

                        <Text style={{
                            fontSize: 12,
                            fontWeight: '400',
                            letterSpacing: 0.2,
                            fontFamily: 'Rubik-Regular',
                            textAlign: 'center',
                            alignSelf: 'center',
                            color: '#6d819c',
                            lineHeight: 18,
                            marginTop: 5,
                            marginLeft: 5,
                            marginRight: 5,
                            marginBottom: 25,
                        }}> {this.state.resObj.ev_descp} </Text>
                    </Animated.View>

                    <View style={styles.containerMobileStyle}>
                        <TextInput
                            ref={input => { this.emailTextInput = input; }}
                            style={styles.textInputStyle}
                            placeholder="Enter Email"
                            placeholderTextColor="grey"
                            keyboardType="email-address"
                            maxLength={100}
                            value={this.state.email}
                            returnKeyType='done'
                            onChangeText={text => this.setState({ email: text })}
                        />
                    </View>

                    {this.state.popuploading
                        ?
                        (
                            <ActivityIndicator size="large" color="#000000" animating={this.state.popuploading} style={{ marginTop: 40 }} />
                        )
                        :
                        (

                            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                                <TouchableOpacity onPress={() => {

                                    this.validateEmail(this.state.email);

                                }}>
                                    <Text style={styles.textSave}>Continue</Text>
                                </TouchableOpacity>
                            </LinearGradient>

                        )
                    }

                </View>

            );

        } else if (this.state.freeTrailProcess === 3) {

            return (

                <View style={{ flexDirection: 'column', margin: 15, flex: 1, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>

                    <Animated.View style={{ transform: [{ translateX: this.state.bounceValue }] }}>

                        <Text style={{
                            width: wp('85%'),
                            fontSize: 14,
                            fontWeight: '500',
                            letterSpacing: 0.2,
                            fontFamily: 'Rubik-Medium',
                            textAlign: 'center',
                            alignSelf: 'center',
                            color: '#282c37',
                            lineHeight: 18,
                            marginBottom: 10
                        }}> {this.state.emailSuccessTitle} </Text>

                        <Text style={{
                            fontSize: 12,
                            fontWeight: '400',
                            letterSpacing: 0.2,
                            fontFamily: 'Rubik-Regular',
                            textAlign: 'center',
                            alignSelf: 'center',
                            color: '#6d819c',
                            lineHeight: 18,
                            marginTop: 5,
                            marginLeft: 20,
                            marginRight: 20,
                            marginBottom: 25,
                        }}> {this.state.emailSuccessDesc} </Text>
                    </Animated.View>



                    <View style={styles.containerMobileStyle}>
                        <TextInput
                            ref={input => { this.emailTextInput = input; }}
                            style={styles.textInputStyle}
                            placeholder="Enter code"
                            placeholderTextColor="grey"
                            keyboardType="number-pad"
                            maxLength={4}
                            value={this.state.emailOtp}
                            returnKeyType='done'
                            onChangeText={text => this.setState({ emailOtp: text })}
                        />
                    </View>



                    {this.state.popuploading
                        ?
                        (
                            <ActivityIndicator size="large" color="#000000" animating={this.state.popuploading} style={{ marginTop: 40 }} />
                        )
                        :
                        (

                            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                                <TouchableOpacity onPress={() => {

                                    if (this.state.emailOtp) {
                                        this.setState({ popuploading: true, onConfirmClick: true });
                                        this.saveFreeTrial();
                                    } else {
                                        this.setState({ isAlert: true, alertMsg: 'Please enter otp sent to your email id' });
                                    }

                                }}>
                                    <Text style={styles.textSave}>Continue</Text>
                                </TouchableOpacity>
                            </LinearGradient>
                        )
                    }

                </View>

            );

        } else if (this.state.freeTrailProcess === 4) {

            return (

                <View style={{ flexDirection: 'column', margin: 15, flex: 1, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>

                    <Image
                        progressiveRenderingEnabled={true}
                        resizeMethod="resize"
                        source={{ uri: this.state.freeTrialObj.image }}
                        style={{
                            width: 150,
                            height: 150,
                            alignSelf: 'center',
                        }}
                    />
                    <Animated.View style={{ transform: [{ translateX: this.state.bounceValue }] }}>

                        <Text style={styles.freeTrialPopupTitle}>{this.state.freeTrialSuccessTitle}</Text>

                        <Text style={styles.freeTrialPopupText}>{this.state.freeTrialSuccessMessage}</Text>
                    </Animated.View>
                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                        <TouchableOpacity onPress={() => {
                            this.bottompopup.close();

                            if (this.state.freeTrialObj.qst_type !== 0) {
                                if (this.state.freeTrialObj.qst_type === 1) {
                                    if (this.state.freeTrialObj.diet_imp_confirm === 0) {
                                        Actions.aidietques();
                                    } else {
                                        Actions.dietHome1({ isRefresh: 'yes' });
                                    }

                                } else if (this.state.freeTrialObj.qst_type === 2 || this.state.freeTrialObj.qst_type === 3) {
                                    if (this.state.freeTrialObj.workout_imp_confirm === 0) {
                                        Actions.traDietWorkQues({ qst_type: this.state.freeTrialObj.qst_type });
                                    } else if (this.state.freeTrialObj.diet_imp_confirm === 0) {
                                        Actions.traDietWorkQues({ qst_type: this.state.freeTrialObj.qst_type });
                                    } else {
                                        Actions.traineeWorkoutsHome({ isRefresh: 'yes' });
                                    }

                                }
                            }

                        }}>
                            <Text style={styles.textSave}>Ok. Thanks</Text>
                        </TouchableOpacity>
                    </LinearGradient>
                </View>
            );
        }
    }


    async validateEmail(email) {
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        if (!email) {
            this.setState({ isAlert: true, alertMsg: 'Please enter an email ID' });
        }
        else if (reg.test(email.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '')) === false) {
            this.setState({ isAlert: true, alertMsg: 'Please enter a valid email ID' });
        }
        else {

            this.setState({ popuploading: true, onConfirmClick: true });
            this.verifyEmailOtp();

        }
    }

    render() {
        return (
            <ImageBackground source={require('../res/app_bg.png')} style={styles.mainContainer}>
                <View style={styles.mainContainer}>
                    <Loader loading={this.state.loading} />
                    <NoInternet
                        image={require('../res/img_nointernet.png')}
                        loading={this.state.isNoInternet}
                        onRetry={this.onRetry.bind(this)} />

                    {this.renderAllSuscriprionsList()}

                    {this.renderBottomButton()}


                    <CustomDialog
                        visible={this.state.isAlert}
                        title='Alert'
                        desc={this.state.alertMsg}
                        onAccept={this.onAccept.bind(this)}
                        no=''
                        yes='Ok' />

                    <HomeDialog
                        visible={this.state.isSuccess}
                        title={this.state.titMsg}
                        desc={this.state.sucMsg}
                        onAccept={this.onSuccess.bind(this)}
                        onDecline={this.onDeny.bind(this)}
                        no='No'
                        yes='Yes' />
                </View>

                <Dialog
                    onDismiss={() => {
                        this.setState({ isCancelInfo: false });
                    }}
                    onTouchOutside={() => {
                        this.setState({ isCancelInfo: false });
                    }}
                    width={0.75}
                    visible={this.state.isCancelInfo}
                >
                    <DialogContent
                        style={{
                            backgroundColor: '#ffffff'
                        }}>
                        <View style={{ flexDirection: 'column', }}>
                            <View style={{ flexDirection: 'column', }}>

                                <View style={{ alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'column' }}>
                                    <Text style={styles.textPopTitle}>{this.state.cancelTitle}</Text>

                                    {this.state.cancelDescription
                                        ?
                                        (
                                            <View>
                                                {this.state.cancelDescription.split('|').map((tipDesc, index) => (
                                                    <View style={styles.viewListAllInner}>
                                                        <View style={styles.viewBlueDot}></View>
                                                        <Text style={styles.textPopDesc}>{tipDesc}</Text>
                                                    </View>
                                                ))}
                                            </View>
                                        )
                                        :
                                        (
                                            <View style={styles.viewListAllInner}>
                                                <View style={styles.viewBlueDot}></View>
                                                <Text style={styles.textPopDesc}>No data available</Text>
                                            </View>
                                        )}


                                    {/* <Text style={styles.textPopDesc}>{this.state.cancelDescription}</Text> */}
                                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientTaskInfo}>
                                        <TouchableOpacity onPress={() => {
                                            this.setState({ isCancelInfo: false });
                                        }}>
                                            <Text style={styles.textTaskInfoSave}>OK</Text>
                                        </TouchableOpacity>
                                    </LinearGradient>

                                </View>

                            </View>

                        </View>

                    </DialogContent>
                </Dialog>

                <Dialog
                    onDismiss={() => {
                        this.setState({ isPopupVisible: false });
                    }}
                    onTouchOutside={() => {
                        this.setState({ isPopupVisible: false });
                    }}
                    width={0.75}
                    visible={this.state.isPopupVisible}
                >
                    <DialogContent
                        style={{
                            backgroundColor: '#ffffff'
                        }}>
                        <View style={{ flexDirection: 'column', }}>
                            <View style={{ flexDirection: 'column', padding: 10 }}>

                                <View style={{ alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'column' }}>

                                    <Image
                                        source={require('../res/img_pop_cancel_subscription.png')}
                                        style={styles.imgpopup}
                                    />
                                    <Text style={styles.textPopTitle}>Are you sure ?</Text>
                                    <Text style={styles.textPopDesc}>{this.state.canSubDesc}</Text>
                                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                                        <TouchableOpacity onPress={() => {
                                            this.setState({ isPopupVisible: false });
                                            this.cancelSubscriptions();
                                        }}>
                                            <Text style={styles.textSave}>CANCEL</Text>
                                        </TouchableOpacity>
                                    </LinearGradient>
                                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                                        <TouchableOpacity onPress={() => {
                                            this.setState({ isPopupVisible: false });
                                        }}>
                                            <Text style={styles.textSave}>BACK</Text>
                                        </TouchableOpacity>
                                    </LinearGradient>

                                </View>

                            </View>

                        </View>

                    </DialogContent>
                </Dialog>

                <Dialog
                    onDismiss={() => {
                        this.setState({ isPopModify: false });
                    }}
                    onTouchOutside={() => {
                        this.setState({ isPopModify: false });
                    }}
                    width={0.75}
                    visible={this.state.isPopModify}
                >
                    <DialogContent
                        style={{
                            backgroundColor: '#ffffff'
                        }}>
                        <View style={{ flexDirection: 'column', }}>
                            <View style={{ flexDirection: 'column', padding: 10 }}>

                                <View style={{ alignItems: 'center', alignContent: 'center', alignSelf: 'center', flexDirection: 'column' }}>

                                    <Image
                                        source={require('../res/img_pop_cancel_subscription.png')}
                                        style={styles.imgpopup}
                                    />
                                    <Text style={styles.textPopTitle}>Are you sure ?</Text>
                                    <Text style={styles.textPopDesc}>You can earn credits from present subscription and choose another plan.</Text>
                                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                                        <TouchableOpacity onPress={() => {
                                            this.setState({ isPopModify: false });
                                            Actions.traBuyPlanModify({ myPlanObj: this.state.myPlanObj });
                                        }}>
                                            <Text style={styles.textSave}>MODIFY</Text>
                                        </TouchableOpacity>
                                    </LinearGradient>
                                    <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                                        <TouchableOpacity onPress={() => {
                                            this.setState({ isPopModify: false });
                                        }}>
                                            <Text style={styles.textSave}>BACK</Text>
                                        </TouchableOpacity>
                                    </LinearGradient>

                                </View>

                            </View>

                        </View>

                    </DialogContent>
                </Dialog>

                <RBSheet
                    ref={ref => {
                        this.bottompopup = ref;
                    }}
                    height={popupHeight}
                    closeOnDragDown={false}
                    closeOnPressMask={false}
                    closeOnPressBack={!this.state.onConfirmClick}
                    onClose={() => {
                        this.setState({ freeTrailProcess: 1 })
                    }}
                    customStyles={{
                        container: {
                            borderTopLeftRadius: 10,
                            borderTopRightRadius: 10
                        }
                    }}
                >


                    {this.state.onConfirmClick
                        ?
                        (
                            <View></View>
                        )
                        :
                        (
                            <TouchableOpacity style={{ position: 'absolute', right: 10, top: 10, padding: 10, zIndex: 1 }}
                                onPress={() => { this.bottompopup.close() }}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    style={{
                                        width: 19,
                                        height: 19,
                                        alignSelf: 'flex-start',
                                        tintColor: 'black',
                                    }}
                                    source={require('../res/ic_cross_black.png')}
                                />
                            </TouchableOpacity>

                        )}

                    {this.renderFreeTrailProcess()}

                    {/* {this.state.confirmSuccess
                        ?
                        (

                            <View style={{ flexDirection: 'column', margin: 15, flex: 1, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={{ uri: this.state.freeTrialObj.image }}
                                    style={{
                                        width: 150,
                                        height: 150,
                                        alignSelf: 'center',
                                    }}
                                />
                                <Animated.View style={{ transform: [{ translateX: this.state.bounceValue }] }}>

                                    <Text style={styles.freeTrialPopupTitle}>{this.state.freeTrialSuccessTitle}</Text>

                                    <Text style={styles.freeTrialPopupText}>{this.state.freeTrialSuccessMessage}</Text>
                                </Animated.View>
                                <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                                    <TouchableOpacity onPress={() => {
                                        this.bottompopup.close();
                                        // Actions.traBuySuccess({ from: 'payment', sucResponse: this.state.freeTrialObj });

                                        if (this.state.freeTrialObj.qst_type !== 0) {
                                            if (this.state.freeTrialObj.qst_type === 1) {
                                                if (this.state.freeTrialObj.diet_imp_confirm === 0) {
                                                    Actions.aidietques();
                                                } else {
                                                    Actions.dietHome1({ isRefresh: 'yes' });
                                                }

                                            } else if (this.state.freeTrialObj.qst_type === 2 || this.state.freeTrialObj.qst_type === 3) {
                                                if (this.state.freeTrialObj.workout_imp_confirm === 0) {
                                                    Actions.traDietWorkQues({ qst_type: this.state.freeTrialObj.qst_type });
                                                } else if (this.state.freeTrialObj.diet_imp_confirm === 0) {
                                                    Actions.traDietWorkQues({ qst_type: this.state.freeTrialObj.qst_type });
                                                } else {
                                                    Actions.traineeWorkoutsHome({ isRefresh: 'yes' });
                                                }

                                            }
                                        }


                                    }}>
                                        <Text style={styles.textSave}>Ok. Thanks</Text>
                                    </TouchableOpacity>
                                </LinearGradient>


                            </View>

                        )
                        :
                        (
                            <View style={{ flexDirection: 'column', margin: 15, flex: 1, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>
                                <Image
                                    progressiveRenderingEnabled={true}
                                    resizeMethod="resize"
                                    source={{ uri: this.state.freeTrialConfirmImg }}
                                    style={{
                                        width: 150,
                                        height: 150,
                                        alignSelf: 'center',
                                    }}
                                />


                                <Text style={styles.freeTrialPopupTitle}>{this.state.freeTrialConfirmTitle}</Text>

                                <Text style={styles.freeTrialPopupText}>{this.state.freeTrialConfirmText}</Text>

                                {this.state.popuploading
                                    ?
                                    (
                                        <ActivityIndicator size="large" color="#000000" animating={this.state.popuploading} />
                                    )
                                    :
                                    (

                                        <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradientEnroll}>
                                            <TouchableOpacity onPress={() => {

                                                this.setState({ popuploading: true, onConfirmClick: true });
                                                this.saveFreeTrial();

                                                // setTimeout(() => {
                                                //   this._toggleSubview();
                                                //   this.setState({ popuploading: false, confirmSuccess: true, freeTrialSuccessTitle: 'Congratulations', freeTrialSuccessMessage: 'Dear User, as part of the free trial you shall have access to all the fitness programs and a sample diet plan for the next 7 days. Hope you have fun.', });
                                                // }, 5000)



                                            }}>
                                                <Text style={styles.textSave}>Confirm</Text>
                                            </TouchableOpacity>
                                        </LinearGradient>

                                    )
                                }


                            </View>
                        )
                    } */}

                    <CustomDialog
                        visible={this.state.isAlert}
                        title='Alert'
                        desc={this.state.alertMsg}
                        onAccept={this.onAccept.bind(this)}
                        no=''
                        yes='Ok' />

                </RBSheet>

            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'center',
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    aroundListStyle: {
        width: wp('90%'),
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        alignSelf: 'center',
        flexDirection: 'column',
        borderColor: '#ddd',
        // borderRadius: 15,
        borderRadius: 8,
        position: 'relative',
        marginTop: 10,
        marginBottom: 1,
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    textWorkName: {
        fontSize: 14,
        fontWeight: '500',
        marginTop: 3,
        marginRight: wp('13%'),
        fontFamily: 'Rubik-Medium',
        color: '#282c37',
    },
    textSubPlan: {
        fontSize: 14,
        fontWeight: '500',
        marginTop: 2,
        fontFamily: 'Rubik-Medium',
        color: '#8c52ff',
    },
    linearGradient: {
        justifyContent: 'center',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderBottomRightRadius: 8,
        borderBottomLeftRadius: 8,
        position: 'relative',
        marginTop: 5,
        padding: 5,
    },
    buttonTuch:
    {
        backgroundColor: 'transparent',
    },
    buttonText: {
        fontSize: 14,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    textTitle: {
        fontSize: 11,
        fontWeight: '400',
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        lineHeight: 18,
    },
    textNodataTitle: {
        width: wp('85%'),
        fontSize: 16,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
    },
    textNodata: {
        width: wp('85%'),
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
    },
    textDates: {
        fontSize: 10,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        color: '#6d819c',
        lineHeight: 18,
    },
    nutritionInnerView: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignSelf: 'flex-start',
        marginTop: 5,
    },
    textNutTitle: {
        fontSize: 16,
        fontWeight: '500',
        color: '#2d3142',
        fontFamily: 'Rubik-Medium',
        letterSpacing: 0.2,
    },
    textNutDesc: {
        fontSize: 11,
        fontWeight: '400',
        color: '#9c9eb9',
        marginTop: 5,
        fontFamily: 'Rubik-Regular',
        letterSpacing: 0.2,
    },
    linearGradient1: {
        width: '90%',
        height: 50,
        bottom: 0,
        borderRadius: 25,
        justifyContent: 'center',
        position: 'absolute',
        alignSelf: 'center',
        marginBottom: 15,
    },
    lgSevenDays: {
        width: wp('70%'),
        height: 40,
        borderRadius: 25,
        justifyContent: 'center',
        alignSelf: 'center',
    },
    imgNoSuscribe: {
        width: 270,
        height: 270,
        alignSelf: 'center',
    },
    imgpopup: {
        width: 200,
        height: 200,
        alignSelf: 'center',
    },
    textPopTitle: {
        fontSize: 16,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        padding: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
    },
    textPopDesc: {
        fontSize: 13,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
        padding: 5,
    },
    textSave: {
        width: wp('40%'),
        fontSize: 14,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        borderRadius: 15,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 8,
        paddingBottom: 8,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#ffffff',
    },
    linearGradientEnroll: {
        borderRadius: 15,
        justifyContent: 'center',
        alignSelf: 'center',
        alignSelf: "center",
        alignItems: 'center',
        marginTop: 20,
    },
    lgCancel: {
        justifyContent: 'center',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderBottomLeftRadius: 8,
        position: 'relative',
        flex: 1,
    },
    lgCancel1: {
        justifyContent: 'center',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8,
        position: 'relative',
        flex: 1,
    },
    lgModify: {
        justifyContent: 'center',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderBottomRightRadius: 8,
        position: 'relative',
        flex: 1,
    },
    lgModify1: {
        justifyContent: 'center',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8,
        position: 'relative',
        flex: 1,
    },
    linearGradientEnroll: {
        borderRadius: 15,
        justifyContent: 'center',
        alignSelf: 'center',
        alignSelf: "center",
        alignItems: 'center',
        marginTop: 20,
        // position:'absolute',
        // bottom:0,
    },
    textSave: {
        width: wp('40%'),
        fontSize: 14,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        borderRadius: 15,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 8,
        paddingBottom: 8,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#ffffff',
    },
    freeTrialPopupTitle: {
        width: wp('85%'),
        fontSize: 14,
        fontWeight: '500',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Medium',
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        lineHeight: 18,
        marginTop: 10
    },
    freeTrialPopupText: {
        fontSize: 12,
        fontWeight: '400',
        letterSpacing: 0.2,
        fontFamily: 'Rubik-Regular',
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        lineHeight: 18,
        marginTop: 5,
        marginLeft: 5,
        marginRight: 5,
        marginBottom: 15,
    },
    viewListAllInner: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignSelf: 'flex-start',
        alignItems: 'center',
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 15,
    },
    viewBlueDot: {
        width: 10,
        height: 10,
        backgroundColor: '#8c52ff',
        alignSelf: 'flex-start',
        marginTop: 5,
        borderRadius: 5,
    },
    textTaskInfoSave: {
        width: wp('75%'),
        fontSize: 14,
        fontWeight: '500',
        fontFamily: 'Rubik-Regular',
        borderRadius: 15,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 10,
        paddingBottom: 10,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#ffffff',
    },
    linearGradientTaskInfo: {
        width: '75%',
        borderBottomLeftRadius: 9,
        borderBottomRightRadius: 9,
        justifyContent: 'center',
        alignSelf: "center",
        alignItems: 'center',
        marginTop: 20,
        marginBottom: -25,
    },

    containerMobileStyle: {
        borderBottomWidth: 1,
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: '#ddd',
        borderRadius: 15,
        position: 'relative',
        alignSelf: 'center',
        marginLeft: 20,
        marginRight: 20,
        marginTop: 10,
        marginBottom: 20,
        padding: 8,
        shadowColor: '#4075cd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    textInputStyle: {
        height: 40,
        flex: 1,
        fontSize: 13,
        marginLeft: 10,
        fontFamily: 'Rubik-Regular',
        color: '#2d3142',
    },
});

export default MyOrders;
