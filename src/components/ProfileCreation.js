import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  BackHandler,
  ScrollView,
  TextInput,
  ImageBackground,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { getTraineeGoals, getTrainerCodes, uploadProfile } from '../actions';
import { Loader, CustomDialog } from './common';
import { withNavigationFocus } from 'react-navigation';
import LinearGradient from 'react-native-linear-gradient';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import LogUtils from '../utils/LogUtils.js';
import DeviceInfo from 'react-native-device-info';
import { allowFunction } from '../utils/ScreenshotUtils.js';
import RNExitApp from 'react-native-exit-app';

class ProfileCreation extends Component {

  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      age: '',
      isMaleFemale: -1,
      isAlert: false,
      isExit: false,
      alertMsg: '',

      isShowAgeNext:true,

    };
  }

  async componentDidMount() {
    allowFunction();
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      this.onBackPressed();
      return true;
    });
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }


  onBackPressed() {
    //Actions.web({ url: this.props.url, flag: 'newreg' });
    this.setState({ loading: false, isAlert: false, isExit: true, alertMsg: 'Are you sure! You want to exit from profile creation?' });
    //Actions.login();
  }

  async onAccept() {
    this.setState({ isAlert: false, isExit: false ,alertMsg: ''});
  }

  async onYesClicked() {
    //this.setState({ alertMsg: '' });
    this.setState({ isExit: false, isAlert: false,alertMsg: '' });
    RNExitApp.exitApp();
    // BackHandler.exitApp()
  }

  renderGenderMaleFemale() {
    try {
      if (this.state.isMaleFemale === -1) {
        return (
          <View style={styles.containerGender}>
            <TouchableOpacity
              onPress={() => this.setState({ isMaleFemale: 1 })}>
              <View style={styles.containerMaleStyle}>
                <Image
                  progressiveRenderingEnabled={true}
                  resizeMethod="resize"
                  source={require('../res/uncheck.png')}
                  style={styles.mobileImageStyle}
                />
                <Image
                  progressiveRenderingEnabled={true}
                  resizeMethod="resize"
                  source={require('../res/male.png')}
                  style={styles.genImg}
                />
                <Text style={styles.countryText}>Male</Text>
              </View>

            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => this.setState({ isMaleFemale: 2 })}>

              <View style={styles.containerFemaleStyle}>
                <Image
                  progressiveRenderingEnabled={true}
                  resizeMethod="resize"
                  source={require('../res/uncheck.png')}
                  style={styles.mobileImageStyle}
                />
                <Image
                  progressiveRenderingEnabled={true}
                  resizeMethod="resize"
                  source={require('../res/female.png')}
                  style={styles.genImg}
                />
                <Text style={styles.countryText}>Female</Text>
              </View>
            </TouchableOpacity>

          </View>
        );
      } else if (this.state.isMaleFemale === 1) {
        return (
          <View style={styles.containerGender}>
            <TouchableOpacity
              onPress={() => this.setState({ isMaleFemale: 1 })}>
              <View style={styles.containerMaleStyle}>
                <Image
                  progressiveRenderingEnabled={true}
                  resizeMethod="resize"
                  source={require('../res/check.png')}
                  style={styles.mobileImageStyle}
                />
                <Image
                  progressiveRenderingEnabled={true}
                  resizeMethod="resize"
                  source={require('../res/male.png')}
                  style={styles.genImg}
                />
                <Text style={styles.countryText}>Male</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => this.setState({ isMaleFemale: 2 })}>
              <View style={styles.containerFemaleStyle}>
                <Image
                  progressiveRenderingEnabled={true}
                  resizeMethod="resize"
                  source={require('../res/uncheck.png')}
                  style={styles.mobileImageStyle}
                />
                <Image
                  progressiveRenderingEnabled={true}
                  resizeMethod="resize"
                  source={require('../res/female.png')}
                  style={styles.genImg}
                />
                <Text style={styles.countryText}>Female</Text>
              </View>
            </TouchableOpacity>
          </View>
        );
      } else if (this.state.isMaleFemale === 2) {
        return (
          <View style={styles.containerGender}>
            <TouchableOpacity
              onPress={() => this.setState({ isMaleFemale: 1 })}>
              <View style={styles.containerMaleStyle}>
                <Image
                  progressiveRenderingEnabled={true}
                  resizeMethod="resize"
                  source={require('../res/uncheck.png')}
                  style={styles.mobileImageStyle}
                />
                <Image
                  progressiveRenderingEnabled={true}
                  resizeMethod="resize"
                  source={require('../res/male.png')}
                  style={styles.genImg}
                />
                <Text style={styles.countryText}>Male</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => this.setState({ isMaleFemale: 2 })}>

              <View style={styles.containerFemaleStyle}>
                <Image
                  progressiveRenderingEnabled={true}
                  resizeMethod="resize"
                  source={require('../res/check.png')}
                  style={styles.mobileImageStyle}
                />
                <Image
                  progressiveRenderingEnabled={true}
                  resizeMethod="resize"
                  source={require('../res/female.png')}
                  style={styles.genImg}
                />
                <Text style={styles.countryText}>Female</Text>
              </View>
            </TouchableOpacity>

          </View>
        );
      }
    } catch (error) {
      console.log(error);
    }
  }

  async profileUpload(firstName, lastName, email, age) {
    try {
      let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      //console.log(firstName, lastName, email);
      if (!firstName) {
        this.setState({ isAlert: true, alertMsg: 'Please enter first name' });
      } else if (!lastName) {
        this.setState({ isAlert: true, alertMsg: 'Please enter last name' });
      } else if (!email) {
        this.setState({ isAlert: true, alertMsg: 'Please enter an email ID' });
      } else if (reg.test(email.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '')) === false) {
        this.setState({ isAlert: true, alertMsg: 'Please enter a valid email ID' });
      } else if (!age) {
        this.setState({ isAlert: true, alertMsg: 'Please enter your age' });
      } else if ((age < 15) || (age > 80)) {
        this.setState({ isAlert: true, alertMsg: 'Persons with age between 15 and 80 can only register', age: '' });
      } else if (this.state.isMaleFemale === -1) {
        this.setState({ isAlert: true, alertMsg: 'Please select your gender' });
      } else {

        LogUtils.firebaseEventLog('click', {
          p_id: 101,
          p_category: 'Registration',
          p_name: 'Profile Details',
        });

        await AsyncStorage.setItem('firstName', firstName);
        await AsyncStorage.setItem('lastName', lastName);
        await AsyncStorage.setItem('email', email);
        await AsyncStorage.setItem('age', this.state.age.toString());
        await AsyncStorage.setItem('genderId', this.state.isMaleFemale.toString());
        //Actions.age();
        Actions.heightnew();
        //Actions.gender();
        
      }

    } catch (error) {
      console.log(error);
    }
  }

  render() {
    return (
      <KeyboardAwareScrollView
        resetScrollToCoords={{ x: 0, y: 0 }}
        contentContainerStyle={styles.containerStyle}
        enableOnAndroid={false}
        scrollEnabled={false}
        onKeyboardDidShow={(frames)=>this.setState({isShowAgeNext:false})}
        onKeyboardDidHide={(frames)=>this.setState({isShowAgeNext:true})}
        >
        <ImageBackground source={require('../res/app_bg.png')} style={{ width: '100%', height: '100%' }}>
          <Loader loading={this.props.loading} />
         

          <View style={{
            flexDirection: 'row',
            margin: 20,
          }}>
            <View style={{ position: 'absolute', zIndex: 111 }}>
              <TouchableOpacity
                style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                onPress={() => this.onBackPressed()}>
                <Image
                  source={require('../res/ic_back.png')}
                  style={styles.backImageStyle}
                />
              </TouchableOpacity>
            </View>

            <Text style={styles.textIndicator}>1 / 3</Text>
          </View>

            <ScrollView contentContainerStyle={{ paddingBottom: hp('15%') }}>

            <View style={styles.optionInnerContainer}>
              <Image
                source={require('../res/ic_app.png')}
                style={styles.appImageStyle}
              />
            </View>

            <Text style={styles.textStyle}>Create an account with us</Text>

            <Text style={styles.desc}>We need the below information to personalise your experience</Text>

            <View style={styles.containerMobileStyle}>
              <TextInput
                style={styles.textInputStyle}
                placeholder="Enter First Name"
                maxLength={50}
                placeholderTextColor="grey"
                value={this.state.firstName}
                autoCapitalize='words'
                returnKeyType='next'
                onSubmitEditing={() => { this.secondTextInput.focus(); }}
                onChangeText={text => {
                  if (text) {
                    let newText = '';
                    let numbers = 'abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                    for (var i = 0; i < text.length; i++) {
                      if (numbers.indexOf(text[i]) > -1) {
                        newText = newText + text[i];
                      }
                      else {
                        this.setState({ alertMsg: 'First Name does not contain special characters and numbers' });
                        this.setState({ isAlert: true });
                      }
                    }

                    if (newText) {
                      this.setState({ firstName: newText });
                    }

                  }
                  else {
                    this.setState({ firstName: '' });
                  }

                }}
              />
            </View>

            <View style={styles.containerMobileStyle}>
              <TextInput
                ref={input => { this.secondTextInput = input; }}
                style={styles.textInputStyle}
                placeholder="Enter Last Name"
                placeholderTextColor="grey"
                maxLength={50}
                value={this.state.lastName}
                returnKeyType='next'
                onSubmitEditing={() => { this.emailTextInput.focus(); }}
                onChangeText={text => {
                  if (text) {
                    let newText = '';
                    let numbers = 'abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                    for (var i = 0; i < text.length; i++) {
                      if (numbers.indexOf(text[i]) > -1) {
                        newText = newText + text[i];
                      }
                      else {
                        this.setState({ alertMsg: 'Last Name does not contain special characters and numbers' });
                        this.setState({ isAlert: true });
                      }
                    }

                    if (newText) {
                      this.setState({ lastName: newText });
                    }

                  }
                  else {
                    this.setState({ lastName: '' });
                  }
                }
                }
              />
            </View>

            <View style={styles.containerMobileStyle}>
              <TextInput
                ref={input => { this.emailTextInput = input; }}
                style={styles.textInputStyle}
                placeholder="Enter Email"
                placeholderTextColor="grey"
                keyboardType="email-address"
                maxLength={100}
                value={this.state.email}
                returnKeyType='done'
                onChangeText={text => this.setState({ email: text })}
              />
            </View>

            <View style={styles.containerMobileStyle}>
              <TextInput
                ref={input => { this.ageTextInput = input; }}
                style={styles.textInputStyle}
                placeholder="Enter your age"
                maxLength={3}
                placeholderTextColor="grey"
                keyboardType="numeric"
                returnKeyType='done'
                value={this.state.age}
                onChangeText={text => {
                  if (text) {
                    let newText = '';
                    let numbers = '0123456789';
                    for (var i = 0; i < text.length; i++) {
                      if (numbers.indexOf(text[i]) > -1) {
                        newText = newText + text[i];
                      }
                      else {
                        this.setState({ alertMsg: 'Please enter numbers only' });
                        this.setState({ isAlert: true });
                      }
                    }
                    if (newText) {
                      this.setState({ age: newText });
                    }
                  }
                  else {
                    this.setState({ age: '' });
                  }
                }}
              />
            </View>

            <View style={styles.containericonStyle}>
                {/* To give you a better experience we need to know your gender */}
              
              {this.renderGenderMaleFemale()}


            </View>

          </ScrollView>

          {
            this.state.isShowAgeNext &&
            <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearGradient}>
              <TouchableOpacity
                style={styles.buttonTuch}
                onPress={() => {
                  this.profileUpload(
                    this.state.firstName,
                    this.state.lastName,
                    this.state.email,
                    this.state.age
                  );
                }}>
                <Text style={styles.buttonText}>Next</Text>
              </TouchableOpacity>
            </LinearGradient>
          }

          <CustomDialog
            visible={this.state.isAlert}
            title='Alert'
            desc={this.state.alertMsg}
            onAccept={this.onAccept.bind(this)}
            no=''
            yes='Ok' />

          <CustomDialog
            visible={this.state.isExit}
            title='Alert'
            desc={this.state.alertMsg}
            onAccept={this.onYesClicked.bind(this)}
            onDecline={this.onAccept.bind(this)}
            no='Cancel'
            yes='Ok' />

        </ImageBackground>
      </KeyboardAwareScrollView>
    )
  }

  
}


const styles = StyleSheet.create({
  containerStyle: {
    width: '100%',
    height: '100%',
    flex: 1,
  },
  container: {
    flex: 1,
    justifyContent: "flex-start"
  },
  bottom: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  buttonText: {
    fontSize: 16,
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    fontFamily: 'Rubik-Medium',
    fontWeight: '500',
  },
  buttonTuch:
  {
    backgroundColor: 'transparent',
  },
  backImageStyle: {
    width: 19,
    height: 16,
    marginTop: 10,
    alignSelf: 'center',
  },
  appImageStyle: {
    width: 200,
    height: 60,
    marginTop: 20,

  },
  container: {
    flex: 1,
    marginTop: 20,
  },
  textInputStyle: {
    height: 40,
    flex: 1,
    fontSize: 13,
    marginLeft: 10,
    fontFamily: 'Rubik-Regular',
    color: '#2d3142',
  },
  optionInnerContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  innerContainer: {
    flexDirection: 'row',
    alignItems: 'stretch',
  },
  text: {
    fontSize: 14,
    color: '#000',
  },
  containerMobileStyle: {
    borderBottomWidth: 1,
    backgroundColor: '#ffffff',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: '#ddd',
    borderRadius: 15,
    position: 'relative',
    alignSelf: 'center',
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10,
    padding: 8,
    shadowColor: '#4075cd',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  containericonStyle: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignSelf: 'center',
  },
  textStyle: {
    fontSize: 20,
    marginTop: hp('3%'),
    marginLeft: 40,
    marginRight: 40,
    //marginBottom: hp('1%'),
    marginBottom: 0,
    fontWeight: '500',
    color: '#2d3142',
    lineHeight: 30,
    textAlign: 'center',
    fontFamily: 'Rubik-Medium',
  },
  linearGradient: {
    width: '90%',
    height: 50,
    bottom: 0,
    borderRadius: 25,
    justifyContent: 'center',
    position: 'absolute',
    alignSelf: 'center',
    marginBottom: 15,
  },
  textIndicator: {
    fontSize: 14,
    fontWeight: '500',
    fontFamily: 'Rubik-Medium',
    flex: 1,
    textAlign: 'center',
    alignSelf: 'center',
    color: '#2d3142',
    // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
    paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
  },
  containerGender: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'center',
    marginTop: hp('3%'),
    //backgroundColor:'red'
  },
  containerMaleStyle: {
    width: wp('41%'),
    height: hp('20%'),
    right: wp('2%'),
    backgroundColor: '#ffffff',
    flexDirection: 'column',
    borderColor: '#ddd',
    borderRadius: 10,
    position: 'relative',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  containerFemaleStyle: {
    width: wp('41%'),
    height: hp('20%'),
    left: wp('2%'),
    backgroundColor: '#ffffff',
    flexDirection: 'column',
    borderColor: '#ddd',
    borderRadius: 10,
    position: 'relative',
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 2,
  },
  desc: {
    fontSize: 14,
    marginTop: 10,
    marginLeft: 40,
    marginRight: 40,
    fontWeight: '400',
    color: '#9c9eb9',
    lineHeight: 24,
    textAlign: 'center',
    fontFamily: 'Rubik-Regular',
    marginTop: hp('1%'),
    marginBottom: hp('1%'),
  },
  mobileImageStyle: {
    width: 25,
    height: 25,
    position: 'absolute',
    top: 20,
    right: 0,
    marginRight: 20
  },
  genImg: {
    height: 40,
    width: 35,
    alignSelf: 'center',
    marginTop: hp('3%'),
  },
  countryText: {
    alignSelf: 'center',
    fontSize: 14,
    marginTop: hp('2%'),
    fontWeight: '400',
    color: '#2d3142',
    letterSpacing: 0.26,
    fontFamily: 'Rubik-Medium',
  },
});

const mapStateToProps = state => {

  const { goals, trainerCodes } = state.procre;
  const loading = state.procre.loading;
  const error = state.procre.error;
  return { loading, error, goals, trainerCodes };

};

export default withNavigationFocus(
  connect(
    mapStateToProps,
    { getTraineeGoals, getTrainerCodes, uploadProfile },
  )(ProfileCreation),
);

// export default ProfileCreation;
