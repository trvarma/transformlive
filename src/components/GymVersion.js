import React, { Component } from 'react';
import {
    TouchableWithoutFeedback,
    TouchableHighlight,
    ImageBackground,
    PanResponder,
    StyleSheet,
    Animated,
    SafeAreaView,
    Easing,
    Image,
    View,
    Text,
    KeyboardAvoidingView,
    StatusBar, Dimensions, BackHandler, Platform, TouchableOpacity, TextInput, 
    ScrollView, FlatList,Keyboard
} from 'react-native';
import { CustomDialog, Loader, DataInput, WorkoutCustomDialog,HomeDialog } from './common';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import AppIntroSlider from 'react-native-app-intro-slider';
import Orientation from 'react-native-orientation';
import { Actions } from 'react-native-router-flux';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import LogUtils from '../utils/LogUtils.js';
import { AirbnbRating } from 'react-native-ratings';
import AsyncStorage from '@react-native-community/async-storage';
import ShortVideoPlayer from '../components/ShortVideoPlayer';
import LinearGradient from 'react-native-linear-gradient';
import { BASE_URL, SWR } from '../actions/types';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const initial = Orientation.getInitialOrientation();

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
      statusCode: res[0],
      data: res[1],
    }));
  }

export default  class GymVersion extends Component {

    static defaultProps = {
        toggleResizeModeOnFullscreen: true,
        controlAnimationTiming: 500,
        doubleTapTime: 130,
        playInBackground: true,
        playWhenInactive: false,
        resizeMode: 'contain',
        isFullscreen: false,
        showOnStart: true,
        paused: false,
        repeat: false,
        muted: false,
        volume: 10,
        title: '',
        rate: 1,
        //video_key:"",
    };

    constructor(props) {
        super(props);
        this.state = {
            // Video
            resizeMode: this.props.resizeMode,
            paused: this.props.paused,
            muted: this.props.muted,
            volume: this.props.volume,
            rate: this.props.rate,
            // Controls
            isFullscreen:
                this.props.isFullScreen || this.props.resizeMode === 'cover' || false,
            showTimeRemaining: true,
            volumeTrackWidth: 0,
            volumeFillWidth: 0,
            seekerFillWidth: 0,
            showControls: this.props.showOnStart,
            volumePosition: 0,
            seekerPosition: 0,
            volumeOffset: 0,
            seekerOffset: 0,
            seeking: false,
            originallyPaused: false,
            scrubbing: false,
            loading: false,
            currentTime: 0,
            error: false,
            duration: 0,

            isEndWorkout: false,
            isEndWithOutFeedback: false,
            post: '',
            rating: 5,
            isFeedbackSubmit: false,
            isBackPressed: false,
            fullscreen: false,
            isAlert: false,
            alertMsg: '',
            resObj: '',
            isSuccess: false,
            ignoreSilentSwitch: 'ignore',
            isCompleted: 0,

            isKey: 1,
            url: '',

            logError: {},
            bufferLoader: false,

            gymVersionObj: this.props.gymVersionObj,
            gymVersionObjTemp: this.props.gymVersionObj,
            
            isShow: false,
            slideindex: 0,

            timer: null,
            hours_Counter: '00',
            minutes_Counter: '00',
            seconds_Counter: '00',

            isVisible: false,
            pd_id: 0,
            pdse_id: 0,
            pdses_id: 0,
            setValue: 0,
            setNum: 0,
            setReps: 0,
            alertTitle: "",
            setValueType: '',

            viewIndex: 0,
            video_url: "",
            video_key: 0,

            isShowAlert: false, 
            isShowAlertMsg: ""

        }

        this.setsAndRepsArr=[];
    }

    componentDidMount() {
        try {
            Orientation.lockToPortrait();
            this.setState({
                isShow: true,
            })
            let temp = this.state.gymVersionObj;
            temp.forEach((element, index) => {
                if (index == 0) {
                    element.pause = false;
                } else {
                    element.pause = true;
                }
            });
            //setTimeout(() => {
                this.setState({ gymVersionObj: temp, });
            //}, 1000);

            this.startInterval();
            this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
                this.onBackPressed();
                return true;
            });
        } catch (error) {

        }
    }


    async onBackPressed() {
        //if (!this.state.isEndWorkout) {
            Orientation.lockToPortrait();
            // Actions.pop();
            let temp = this.state.gymVersionObj;
            temp.forEach((element, i) => {
                element.pause = true
            });

            this.setState({ 
                loading: false, 
                isBackPressed: true,
                gymVersionObj: temp
             });
        //}
    }

    //Used to show video recording timer on screen
    startInterval() {
        try {
            if (!this.state.isShow) {
                this.timer = null
                this.timer = setInterval(() => {

                    var num = (Number(this.state.seconds_Counter) + 1).toString()
                    var count = this.state.minutes_Counter;
                    //var hcount = this.state.hours_Counter;

                    if (Number(this.state.seconds_Counter) == 59) {
                        count = (Number(this.state.minutes_Counter) + 1).toString();
                        num = '00';
                    }

                    // if (Number(this.state.minutes_Counter) == 60) {
                    //     hcount = (Number(this.state.hours_Counter) + 1).toString();
                    //     count = '00';
                    //     num = '00';
                    // }

                    this.setState({
                        //hours_Counter: hcount.length == 1 ? '0' + hcount : hcount,
                        minutes_Counter: count.length == 1 ? '0' + count : count,
                        seconds_Counter: num.length == 1 ? '0' + num : num
                    });
                }, 1000);
                this.setState({ timer: this.timer })
            }
        } catch (error) {
            console.warn(error)
        }
    }

    onChangeText(text, pdses_id, type) {
        try {
            let temp = this.state.gymVersionObj;
            temp.forEach(element => {
                element.pdse_arr.forEach((pdse, pdseID) => {
                    pdse.pdses_arr.forEach((pdses, pdsesID) => {
                        if (pdses.pdses_id == pdses_id) {
                            if (type == 'w' || type == 'm') {
                                pdses.set_val = text
                            } else if (type == 'r') {
                                pdses.reps = text
                            }
                        }
                    })
                })
            });

            this.setState({ gymVersionObj: temp });

        } catch (error) {
            console.log(error);
        }
    }

    checkSet(pds_id,pdses_id, is_default, title, type) {
        try {
            this.setState({ pdses_id: pdses_id });
            let temp = this.state.gymVersionObj;
            temp.forEach(element => {
                if (element.pds_id == pds_id) {
                    element.pdse_arr[0].pdses_arr.map(pdses => {
                        if (pdses.pdses_id == pdses_id && pdses.is_default == 0) {
                            if (pdses.set_val == "--" || pdses.set_val == "" || pdses.reps == "" || pdses.set_val == 0 || pdses.reps == 0) {
                                var Atitle = title != "" ? title + " : Set - " + pdses.set : " Set -" + pdses.set;
                                this.setState({
                                    setValue: pdses.set_val,
                                    setReps: pdses.reps,
                                    isAlert: true,
                                    alertMsg: type == 'w' ? "Enter weight and reps to complete the set." : "Enter seconds and reps to complete the set.",
                                    alertTitle: Atitle,
                                    setValueType: type,
                                });

                            } else {
                                pdses.is_default = 1;
                                this.createAndUpdatesetsrepsArr(pds_id, pdses.pdses_id, pdses.set, pdses.set_val, pdses.reps, "check");
                            }
                        } else if (pdses.pdses_id == pdses_id && pdses.is_default == 1) {
                            pdses.is_default = 0;
                            this.createAndUpdatesetsrepsArr(pds_id, pdses.pdses_id, pdses.set, pdses.set_val, pdses.reps, "uncheck");
                        }
                    });
                }
            });
          
            this.setState({ gymVersionObj: temp });
        } catch (error) {
            console.log(error)
        }
    }

    onSubmitEditing(pdses_id,pds_id){
        try{
            this.setState({ pdses_id: pdses_id });
            let temp = this.state.gymVersionObj;
            temp.forEach(element => {
                if (element.pds_id == pds_id) {
                    element.pdse_arr[0].pdses_arr.map(pdses => {
                        if (pdses.pdses_id == pdses_id && pdses.is_default == 0 ) {
                          
                            if(pdses.set_val > 0 && pdses.reps > 0 ){
                                pdses.is_default = 1;
                                this.createAndUpdatesetsrepsArr(pds_id,pdses.pdses_id,pdses.set,pdses.set_val,pdses.reps,"check");
                            }
                            // else if((pdses.set_val == "" || pdses.reps == "")){
                            //     pdses.is_default = 0;
                            //     pdses.set_val="--";
                            //     // pdses.reps=15;   
                            //     this.createAndUpdatesetsrepsArr(pds_id,pdses.pdses_id,pdses.set,pdses.set_val,pdses.reps,"uncheck");
                            // }
                        } else if(pdses.pdses_id == pdses_id && pdses.is_default == 1) {

                            if((pdses.set_val == "" || pdses.reps == "")){
                                pdses.is_default = 0;
                                //pdses.set_val = "--";
                                this.createAndUpdatesetsrepsArr(pds_id,pdses.pdses_id,pdses.set,pdses.set_val,pdses.reps,"uncheck");
                               
                            }else{
                                pdses.is_default = 1;// pdses.reps = 15;
                                this.createAndUpdatesetsrepsArr(pds_id,pdses.pdses_id,pdses.set,pdses.set_val,pdses.reps,"check");
                            }

                        }
                    })
                }
            })
            this.setState({ gymVersionObj: temp });
        }catch(error){
            console.log(error);
        }
    }

    async createAndUpdatesetsrepsArr(pds_id, pdses_id, set, setVal, repVal, checkType) {
        try {
            if (checkType == "check") {
                let params = {
                    "pd_id": this.props.pdObj.pd_id,
                    "pds_id": pds_id,
                    "pdses_id": pdses_id,
                    "sets": set,
                    "reps": repVal,
                    "val": setVal
                }
                
                this.setsAndRepsArr.push(params);
                //this.setState({setsAndRepsArr: this.setsAndRepsArr});
               
            } else {
                this.setsAndRepsArr= this.setsAndRepsArr.filter(ele => ele.pdses_id != pdses_id) 
                //this.setState({ setsAndRepsArr: this.setsAndRepsArr.filter(ele => ele.pdses_id != pdses_id) });
            }
        } catch (error) {
            console.log(error);
        }
    }

    async onAccept() {
        try{
            this.setState({ isAlert: false, alertMsg: '' });
            let temp = this.state.gymVersionObj;
            temp.forEach(element => {
                element.pdse_arr.forEach((pdse, pdseID) => {
                    pdse.pdses_arr.forEach((pdses, pdsesID) => { 
                        if (pdses.pdses_id == this.state.pdses_id) {
                                pdses.is_default = 1;
                                pdses.set_val = this.state.setValue;
                                pdses.reps = this.state.setReps;
                                this.createAndUpdatesetsrepsArr(element.pds_id,pdses.pdses_id,pdses.set,this.state.setValue,this.state.setReps,"check");
                        }
                    })
                })
            });
            this.setState({ gymVersionObj: temp });
        }catch(error){
            console.log(error);
        }
    }

    async onDecline() {
        this.setState({ isAlert: false, alertMsg: '' });
    }

    async onChangeWeight(value) {
        this.setState({
            setValue: value
        })
    }

    async onChangeReps(value) {
        this.setState({
            setReps: value
        })
    }

    onAlertAccept() {
        if (this.state.isShowAlertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }

        this.setState({ isShowAlert: false, isShowAlertMsg: '' });

        if (this.state.isEndWithOutFeedback) {
            this.setState({ isEndWithOutFeedback: false })
            if (this.props.from === 'home') {
                Actions.popTo('traineeHome');
            } else if (this.props.from === 'workouts') {
                Actions.WorkoutsHome();
            } else {
                // Actions.traineeWorkoutsHome();
                Actions.traineePlanNew({ pId: this.props.p_id, from: 'endWork' });
            }
        }
        
    }

    showSetsAndReps(item) {
        try {
            var setAndReps = [];
            item.pdse_arr.map((ele, index) => {
                setAndReps.push(
                    <View key={ele.pdse_id + "pdse_arr"} style={{ flexDirection: 'column', marginTop: 5, }}>
                        {
                            ele.title != "" &&
                            (<View style={{ backgroundColor: '#8c52ff',  justifyContent: 'center', borderTopRightRadius: 4, borderTopLeftRadius: 4 }}>
                                <Text style={{ textAlign: 'center', fontFamily: 'Rubik-Regular', fontSize: 14, color: '#ffffff',paddingTop:5 }}>{ele.title.toUpperCase()}</Text>
                            </View>)
                        }
                        <View style={{
                            flexDirection: 'row',
                            backgroundColor: '#8c52ff',
                            borderTopRightRadius: ele.title != "" ? 0 : 4,
                            borderTopLeftRadius: ele.title != "" ? 0 : 4,
                        }}>
                            <View style={{ flex: 0.1, alignItems: 'center' }}><Text></Text></View>
                            <View style={{ flex: 0.3, alignItems: 'center' ,justifyContent:'center'}}><Text style={{ fontFamily: 'Rubik-Regular', color: '#ffffff' }}>{'Sets'.toLowerCase()}</Text></View>
                            <View style={{ flex: 0.3, alignItems: 'center' ,justifyContent:'center'}}><Text style={{ fontFamily: 'Rubik-Regular', color: '#ffffff' }}>{ele.pdses_arr[index].type == 'w' ? "Kg".toLowerCase() : 'Time(sec)'.toLowerCase()}</Text></View>
                            <View style={{ flex: 0.3, alignItems: 'center',justifyContent:'center' }}><Text style={{ fontFamily: 'Rubik-Regular', color: '#ffffff' }}>{ele.pdses_arr[index].type == 'w' ? "reps".toLowerCase() : 'Minutes'.toLowerCase()}</Text></View>
                        </View>
                        {
                            ele.pdses_arr.map((pdses, setID) => {
                             
                                return (
                                    <View key={pdses.pdses_id + "pdses"} style={styles.containerListStyle}>

                                        <View style={{ flex: 0.1 }}>
                                            {
                                                pdses.is_default ? (
                                                    <TouchableOpacity onPress={() => this.checkSet(item.pds_id,pdses.pdses_id, pdses.is_default, ele.title,pdses.type)}>
                                                        <Image
                                                            progressiveRenderingEnabled={true}
                                                            resizeMethod="resize"
                                                            source={require('../res/check.png')}
                                                            style={styles.profileImage}
                                                        />
                                                    </TouchableOpacity>

                                                ) : (
                                                    <TouchableOpacity onPress={() => this.checkSet(item.pds_id,pdses.pdses_id, pdses.is_default, ele.title, pdses.type)}>
                                                        <Image
                                                            progressiveRenderingEnabled={true}
                                                            resizeMethod="resize"
                                                            source={require('../res/uncheck.png')}
                                                            style={styles.profileImage}
                                                        />
                                                    </TouchableOpacity>
                                                )
                                            }
                                        </View>

                                        <View style={{ flex: 0.3, alignItems: 'center' }}>
                                            <Text style={{ fontFamily: 'Rubik-Regular', }}>{pdses.set}</Text>
                                        </View>

                                        {
                                            pdses.type == 'w' ? (
                                                <View style={{ flex: 0.3, alignItems: 'center' }}>
                                                    <TextInput
                                                        ref={input => { this.set_val = input; }}
                                                        style={[styles.textInputStyle, { marginLeft: 25, }]}
                                                        placeholder="Kg"
                                                        placeholderTextColor="grey"
                                                        keyboardType="numeric"
                                                        maxLength={3}
                                                        value={pdses.set_val.toString()}
                                                        returnKeyType='done'
                                                        onChangeText={text => {
                                                            if (parseFloat(text) > 0) {
                                                                this.onChangeText(text.replace(/[^0-9.]/g, ''), pdses.pdses_id, "w")
                                                            }else{
                                                                this.onChangeText("", pdses.pdses_id, "w")
                                                            }
                                                        }
                                                        }
                                                        onSubmitEditing={() => { 
                                                            Keyboard.dismiss,
                                                            this.onSubmitEditing(pdses.pdses_id,item.pds_id)
                                                            //this.checkSet(item.pds_id,pdses.pdses_id, pdses.is_default, ele.title, pdses.type)
                                                        }}
                                                        onEndEditing={() => { Keyboard.dismiss,this.onSubmitEditing(pdses.pdses_id, item.pds_id)}}
                                                    />
                                                </View>
                                            ) : (
                                                <View style={{ flex: 0.3, alignItems: 'center' }}>
                                                    <TextInput
                                                        ref={input => { this.set_val = input; }}
                                                        style={[styles.textInputStyle, { marginLeft: 25, }]}
                                                        placeholder="Sec"
                                                        placeholderTextColor="grey"
                                                        keyboardType="numeric"
                                                        maxLength={3}
                                                        value={pdses.set_val.toString()}
                                                        returnKeyType='done'
                                                        //onChangeText={text => this.onChangeText(text.replace(/[^0-9]/g, ''), pdses.pdses_id, "m")}
                                                        onChangeText={text => {
                                                            if (parseFloat(text) > 0) {
                                                                this.onChangeText(text.replace(/[^0-9]/g, ''), pdses.pdses_id, "m")
                                                            }else{
                                                                this.onChangeText("", pdses.pdses_id, "m")
                                                            }
                                                        }
                                                        }
                                                        onSubmitEditing={() => { 
                                                            Keyboard.dismiss,
                                                            this.onSubmitEditing(pdses.pdses_id, "m",item.pds_id)
                                                            //this.checkSet(item.pds_id,pdses.pdses_id, pdses.is_default, ele.title, pdses.type)
                                                        }}
                                                        onEndEditing={() => { Keyboard.dismiss,this.onSubmitEditing(pdses.pdses_id, item.pds_id)}}
                                                    />

                                                </View>
                                            )
                                        }
                                        
                                        {
                                            pdses.type == 'w' ? (
                                                <View style={{ flex: 0.3, alignItems: 'center' }}>
                                                    <TextInput
                                                        ref={input => { this.reps = input; }}
                                                        style={[styles.textInputStyle, { marginLeft: 25, }]}
                                                        placeholder="Reps"
                                                        placeholderTextColor="grey"
                                                        keyboardType="numeric"
                                                        maxLength={3}
                                                        value={pdses.reps.toString()}
                                                        returnKeyType='done'
                                                        //onChangeText={text => this.onChangeText(text.replace(/[^0-9]/g, ''), pdses.pdses_id, "r")}
                                                        onChangeText={text => {
                                                            if (parseFloat(text) > 0) {
                                                                this.onChangeText(text.replace(/[^0-9]/g, ''), pdses.pdses_id, "r")
                                                            } else {
                                                                this.onChangeText("", pdses.pdses_id, "r")
                                                            }
                                                        }
                                                        }
                                                        onSubmitEditing={() => {
                                                            Keyboard.dismiss,
                                                                this.onSubmitEditing(pdses.pdses_id, "r", item.pds_id)
                                                            //this.checkSet(item.pds_id,pdses.pdses_id, pdses.is_default, ele.title, pdses.type)
                                                        }}
                                                        onEndEditing={() => { Keyboard.dismiss, this.onSubmitEditing(pdses.pdses_id, item.pds_id) }}
                                                    />
                                                </View>
                                            ) : (
                                                <View style={{ flex: 0.3, alignItems: 'center' }}>
                                                    <Text style={[ { marginLeft: 25, fontFamily: 'Rubik-Regular',}]}>{pdses.reps}</Text>
                                                    
                                                </View>
                                            )
                                        }

                                    </View>
                                )
                            })
                        }
                    </View>
                )
            })
            return (
                <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
                    <View>
                        {setAndReps}
                    </View>
                </TouchableWithoutFeedback>
            )
        } catch (error) {
            console.log(error);
        }
    }

    get pagination1() {
        // const { entries, activeSlide } = this.state;
        return (
            <Pagination
                dotsLength={this.state.gymVersionObj.length}
                activeDotIndex={this.state.viewIndex}
                containerStyle={{ paddingVertical: 10 }}
                dotContainerStyle={{
                    // backgroundColor: '#ffffff',
                    width: 10,
                    height: 10,
                    // marginTop: -20,
                    // marginBottom: -20
                }}
                dotStyle={{
                    width: 10,
                    height: 10,
                    borderRadius: 5,
                    // marginHorizontal: 2,
                    backgroundColor: '#8c52ff',
                    //marginRight:30
                }}
                inactiveDotStyle={{
                    // Define styles for inactive dots here
                    width: 10,
                    height: 10,
                    borderRadius: 5,
                    // marginHorizontal: 2,
                    backgroundColor: '#b0b0b0',
                }}
                inactiveDotOpacity={1}
                inactiveDotScale={0.8}
            />
        );
    }

    async onExitWorkout() {
        try {
            this.setState({
                isBackPressed: false,
                //isEndWorkout:true ,
                isCompleted: 0, 
            });
 
            if(this.setsAndRepsArr.length > 0){
                this.savePDSetsList();
            }
            
            
            if (this.props.planObj.fb_done === 0){              
                this.setState({isEndWorkout:true ,});
            }else{
                if (this.props.from === 'home') {
                    Actions.popTo('traineeHome');
                } else if (this.props.from === 'workouts') {
                    Actions.WorkoutsHome();
                } else {
                    // Actions.traineeWorkoutsHome();
                    Actions.traineePlanNew({ pId: this.props.p_id, from: 'endWork' });
                }
            }  
     
        } catch (error) {
            console.log(error);
        }
    }

    async onCompleteWorkout() {
        this.setState({ isCompleted: 1, isBackPressed: false });
        if(this.setsAndRepsArr.length > 0){
            this.savePDSetsList();
        }

        this.saveEndWorkOut();
    }

    async savePDSetsList() {
        try {

            const uniqueSetsAndReps = Array.from(new Set(this.setsAndRepsArr.map(a => a.pdses_id))).map(pdses_id => {
                return this.setsAndRepsArr.find(a => a.pdses_id === pdses_id)
            });

            //this.setState({ loading: true });
            let token = await AsyncStorage.getItem('token');
            fetch(
                `${BASE_URL}/trainee/savepdsetslist`,
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${token}`,
                    },
                    body: JSON.stringify({
                        sets_arr: uniqueSetsAndReps,
                    }),
                },
            )
                .then(processResponse)
                .then(res => {
                    const { statusCode, data } = res;
                    LogUtils.infoLog1('statusCode', statusCode);
                    LogUtils.infoLog1('data', data);
                    if (statusCode >= 200 && statusCode <= 300) {
                        console.log("--savepd set list");
                    } else {
                        // if (data.message === 'You are not authenticated!') {
                        //     this.setState({ loading: false, isShowAlert: true, isShowAlertMsg: data.message });

                        // } else {
                        //     this.setState({ loading: false, isShowAlert: true, isShowAlertMsg: data.message });
                        // }
                    }
                })
                .catch(function (error) {
                    //this.setState({ loading: false, isShowAlert: true, isShowAlertMsg: SWR });
                });
        } catch (error) {
            console.log(error);
        }
    }

    async saveEndWorkOut() {
        try {
            this.setState({ loading: true });
            let token = await AsyncStorage.getItem('token');
            fetch(
                `${BASE_URL}/trainee/endworkout`,
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${token}`,
                    },
                    body: JSON.stringify({
                        pd_id: this.props.pdObj.pd_id,
                        vrsn : "G" 
                    }),
                },
            )
                .then(processResponse)
                .then(res => {
                    const { statusCode, data } = res;
                    LogUtils.infoLog1('statusCode', statusCode);
                    LogUtils.infoLog1('data', data);
                    if (statusCode >= 200 && statusCode <= 300) {

                        if (this.state.isCompleted === 0) {
                            this.setState({ loading: false, isEndWithOutFeedback: true, isShowAlert: true, isShowAlertMsg: data.message, });

                        } else if (this.state.isCompleted === 1) {
                            if (this.props.planObj.fb_done === 0)
                                this.setState({ loading: false, isEndWorkout: true });
                            else
                                this.setState({ loading: false, isEndWithOutFeedback: true, isShowAlert: true, isShowAlertMsg: data.message, });
                        }

                    } else {
                        if (data.message === 'You are not authenticated!') {
                            this.setState({ loading: false, isShowAlert: true, isShowAlertMsg: data.message });

                        } else {
                            this.setState({ loading: false, isShowAlert: true, isShowAlertMsg: data.message });
                        }
                    }
                })
                .catch(function (error) {
                    this.setState({ loading: false, isShowAlert: true, isShowAlertMsg: SWR });
                });
        } catch (error) {
            console.log(error);
        }
    }

    renderProfileImg() {
        try{

            if (this.props.planObj.tr_img) {
                return (
                    <Image
                        source={{ uri: this.props.planObj.tr_img }}
                        style={styles.profileImage1}
                    />
                );
            }
            else {
                return (
                    <Image
                        source={require('../res/ic_profile_gray.png')}
                        style={styles.profileImage1}
                    />
                );
            }
        }catch(error){
            console.log(error)
        }
    }

    ratingCompleted(rating) {
        try{
        this.setState({ rating: rating })
        switch (rating) {
            case 1:
                this.setState({ ratingText: 'Bad experience !' })
                break;
            case 2:
                this.setState({ ratingText: 'Not that great !' })
                break;
            case 3:
                this.setState({ ratingText: 'It was good !' })
                break;
            case 4:
                this.setState({ ratingText: 'Had a great experience !' })
                break;
            case 5:
                this.setState({ ratingText: 'Absolutely superb !' })
                break;
            default:
                this.setState({ ratingText: 'Give rating !' })
                break
        }
         }catch(error){
            console.log(error)
        }
    }

    async saveFeebackWorkOut() {
        try {
            this.setState({ loading: true });
            let token = await AsyncStorage.getItem('token');
            fetch(
                `${BASE_URL}/trainee/savedayfdback`,
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${token}`,
                    },
                    body: JSON.stringify({
                        pd_id: this.props.pdObj.pd_id,
                        rating: this.state.rating,
                        comments: this.state.post,
                    }),
                },
            )
                .then(processResponse)
                .then(res => {
                    const { statusCode, data } = res;
                    LogUtils.infoLog1('statusCode', statusCode);
                    LogUtils.infoLog1('data', data);
                    if (statusCode >= 200 && statusCode <= 300) {
                        this.setState({ loading: false, isFeedbackSubmit: true })
                    } else {
                        if (data.message === 'You are not authenticated!') {
                            this.setState({ loading: false, isShowAlert: true, isShowAlertMsg: data.message });

                        } else {
                            this.setState({ loading: false, isFeedBackError: true, isShowAlertMsg: data.message });
                        }
                    }
                })
                .catch(function (error) {
                    this.setState({ loading: false, isFeedBackError: true, isShowAlertMsg: SWR });
                });
        } catch (error) {
            console.log(error);
        }
    }

    async onFeedbackSubmit() {
        try {
            if (this.state.isShowAlertMsg === 'You are not authenticated!') {
                AsyncStorage.clear().then(() => {
                    Actions.auth({ type: 'reset' });
                });
            }
            this.setState({ isFeedbackSubmit: false, isShowAlertMsg: '' });

            Actions.traineePlanNew({ pId: this.props.p_id, from: 'endWork' });

        } catch (error) {
            console.log(error);
        }
    }

    _renderItem = ({ item, index }) => {
        return (
            <KeyboardAwareScrollView
                resetScrollToCoords={{ x: 0, y: 0 }}
                contentContainerStyle={styles.mainContainer}
                enableOnAndroid={false}
                scrollEnabled={false}>
                <View key={item.title + index.toString()} style={styles.containerStyle}>

                    <View style={styles.mainView}>

                        {/* touch back */}
                        <View style={styles.viewTitleCal}>
                            <View style={{
                                flexDirection: 'row',
                            }}>
                                <View style={{ position: 'absolute', zIndex: 111 }}>
                                    <TouchableOpacity
                                        style={{ width: 40, height: 40, marginTop: -10, marginLeft: -10 }}
                                        onPress={() => this.onBackPressed()}>
                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_back.png')}
                                            style={styles.backImageStyle}
                                        />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            {/* <View style={{ marginTop: 5, }} /> */}
                        </View>

                        <View style={[styles.viewTitleCal]}>
                            {/* title */}
                            <View style={{ justifyContent: 'center' }}>
                                <View>
                                    <Text style={{ fontSize: 15, fontFamily: 'Rubik-Medium', textAlign: 'center' }} > {item.title}</Text>
                                </View>
                                <View>
                                    <Text style={{ fontSize: 13, fontFamily: 'Rubik-Medium', textAlign: 'center', marginBottom: 5 }} > Exercise {index + 1} of {this.state.gymVersionObj.length} </Text>
                                </View>
                            </View>
                        </View>

                        {/* Video player */}
                        <View style={[styles.viewTitleCal, {
                            marginTop: 0,
                            //height: hp("40%")
                            //backgroundColor:'red'
                        }]}
                        >
                            <ShortVideoPlayer
                                video_url={item.video_url}
                                video_key={item.pds_id}
                                pause={item.pause}
                            />
                        </View>


                        {/* <View style={{marginTop:10}}></View> */}

                        {/* {this.pagination1} */}

                        {/* show sets and reps */}
                        <View style={[styles.viewTitleCal, {
                            marginTop: 5,
                            height: hp("41%")
                        }]}
                        >
                            <View style={{}}>
                                <ScrollView
                                    key={index + "gymVersion"}
                                    ref={ref => this.scrollView = ref}
                                    showsVerticalScrollIndicator={false}
                                    contentContainerStyle={{ paddingBottom: hp('1%') }}
                                    keyboardShouldPersistTaps='handled'
                                    keyboardDismissMode={'on-drag'}
                                >
                                    {this.showSetsAndReps(item)}
                                </ScrollView>
                            </View>
                        </View>

                        {/* timer */}
                        <View style={[styles.viewTitleCal, { height: hp("4%"), justifyContent: 'center', marginTop: 3 }]}>
                            <View style={{
                                alignSelf: 'center',
                                justifyContent: 'center',
                                //backgroundColor:'#8c52ff',
                                padding: 5,
                            }}>
                                <Text style={{ fontSize: 15, fontFamily: 'Rubik-Medium', fontWeight: '200' }}> {`${this.state.minutes_Counter} : ${this.state.seconds_Counter}  `}

                                    <Image
                                        progressiveRenderingEnabled={true}
                                        resizeMethod="resize"
                                        source={require('../res/clock.png')}
                                        style={[styles.backImageStyle, { height: 15, width: 15 }]}
                                    />
                                </Text>
                            </View>
                        </View>
                        {/* timer end  */}

                        <HomeDialog
                            visible={this.state.isShowAlert}
                            title="Success"
                            desc={this.state.isShowAlertMsg}
                            onAccept={this.onAlertAccept.bind(this)}
                            no=''
                            yes='Ok' />

                        <DataInput
                            visible={this.state.isAlert}
                            title={this.state.alertTitle}
                            desc={this.state.alertMsg}
                            onAccept={this.onAccept.bind(this)}
                            onDecline={this.onDecline.bind(this)}
                            no='CANCEL'
                            yes='SAVE'
                            weight={this.state.setValue}//this.state.setValue
                            reps={this.state.setReps}//this.state.setReps
                            setValueType={this.state.setValueType}
                            onChangeWeight={this.onChangeWeight.bind(this)}
                            onChangeReps={this.onChangeReps.bind(this)}
                        />

                        <WorkoutCustomDialog
                            visible={this.state.isBackPressed}
                            title=''
                            desc={'Are you sure you want to exit this workout?'}
                            onAccept={this.onExitWorkout.bind(this)}
                            onDecline={() => { this.setState({ isBackPressed: false, paused: false }) }}
                            onComplete={this.onCompleteWorkout.bind(this)}
                            no='CANCEL'
                            yes='EXIT WORKOUT'
                            third='MY WORKOUT IS COMPLETED'
                        />

                    </View>
                </View>
            </KeyboardAwareScrollView>
        );
    };

    callbk(){
        Keyboard.dismiss();
    }

    slideChange = (index, lastIndex) => {
        try {
            this.callbk();
            this.setState({
                //loading: true,
                viewIndex: index,
            });
            setTimeout(() => {
                let temp = this.state.gymVersionObj;
                temp.forEach((element, i) => {
                    if (index == i) {
                        element.pause = false
                    } else {
                        element.pause = true
                    }
                });

                this.setState({ 
                    //loading: false, 
                    gymVersionObj: temp
                 });
            }, 100);

        } catch (error) {
            console.log(error);
        }
    }

    render() {
        if (!this.state.isEndWorkout) {
            return (
                <View style={styles.maincontainerStyle}>
                    <Loader loading={this.state.loading} />
                    {this.state.isShow
                        ? (

                            <AppIntroSlider
                                ref={ref => this.AppIntroSlider = ref}
                                style={{ width: wp('100%'), alignSelf: 'center', }}
                                slides={this.state.gymVersionObj}
                                showSkipButton={false}
                                showDoneButton={false}
                                showNextButton={false}
                                hidePagination={false}
                                bottomButton={true}
                                dotStyle={{ backgroundColor: 'transparent' }}
                                activeDotStyle={{ backgroundColor: 'transparent' }}
                                extraData={this.state}
                                onSlideChange={this.slideChange}
                                renderItem={this._renderItem}
                                keyExtractor={(item, index) => item.pds_id + item.title}
                            />
                        )
                        : (
                            <View />
                        )
                    }
                </View>
            )
        } else {
            return (
                <KeyboardAwareScrollView
                    resetScrollToCoords={{ x: 0, y: 0 }}
                    contentContainerStyle={styles.mainContainer}
                    enableOnAndroid={false}
                    scrollEnabled={false}>
                <ImageBackground source={require('../res/post_back.png')} style={styles.mainImageContainer}>
                    <View style={styles.postContents}>
                        <Text style={styles.createPostStyle}>RATE YOUR WORKOUT</Text>
                        {this.renderProfileImg()}
                        <Text style={styles.feedbackNameStyle}>{this.props.planObj.tr_name}</Text>
                        <Text style={styles.feedbackSubStyle}>{this.props.planObj.title}</Text>
                        <AirbnbRating
                            // showRating
                            type='star'
                            ratingCount={5}
                            reviews={['Bad experience !', 'Not that great !', 'It was good !', 'Had a great experience !', 'Absolutely superb !']}
                            imageSize={30}
                            startingValue={this.state.rating}
                            readonly={false}
                            defaultRating={5}
                            isDisabled={false}
                            reviewSize={15}
                            onFinishRating={this.ratingCompleted.bind(this)}
                            style={{ marginTop: hp('5%'), alignSelf: 'center', paddingVertical: 5 }}
                        />
                        <View style={styles.inputRow}>
                            <TextInput
                                style={styles.textInputStyle1}
                                placeholder="Type your review..."
                                placeholderTextColor="#9c9eb9"
                                value={this.state.post}
                                numberOfLines={2}
                                multiline={true}
                                onChangeText={text => this.setState({ post: text })}
                            />
                        </View>
                        <LinearGradient colors={['#8c52ff', '#8c52ff']} style={styles.linearSubmitGradient}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.saveFeebackWorkOut();
                                }}
                                style={styles.buttonTuch}>
                                <Text style={styles.buttonText}>SUBMIT</Text>
                            </TouchableOpacity>
                        </LinearGradient>
                    </View>
                    <CustomDialog
                            visible={this.state.isFeedbackSubmit}
                            title='Success'
                            desc={'Feedback submitted successfully.'}
                            onAccept={this.onFeedbackSubmit.bind(this)}
                            no=''
                            yes='Ok' />
                </ImageBackground>
                </KeyboardAwareScrollView>
            );
        }
    }
}

const styles = {
    player: StyleSheet.create({
        container: {
            overflow: 'hidden',
            backgroundColor: '#000',
            flex: 1,
            alignSelf: 'stretch',
            justifyContent: 'space-between',
            borderRadius: 15,
            height: 280,
            //width:wp("90%")
        },
        video: {
            overflow: 'hidden',
            position: 'absolute',
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
        },
    }),
    error: StyleSheet.create({
        container: {
            backgroundColor: 'rgba( 0, 0, 0, 0.5 )',
            position: 'absolute',
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
            justifyContent: 'center',
            alignItems: 'center',
        },
        icon: {
            marginBottom: 16,
            width: 50,
            height: 50,
        },
        text: {
            backgroundColor: 'transparent',
            color: '#8c52ff',//#8c52ff #f27474
            fontFamily: 'Rubik-Medium',
            justifyContent: 'center',
            textAlign: 'center'
        },
    }),
    loader: StyleSheet.create({
        container: {
            position: 'absolute',
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
            alignItems: 'center',
            justifyContent: 'center',
        },

        centerContainer: {
            // flex: 30,
            marginTop: 60,
            height: '60%',
            alignItems: 'center',
            justifyContent: 'center',
            alignSelf: 'center',
        },
    }),
    controls: StyleSheet.create({
        backImageStyle: {
            width: 12,
            height: 19,
            alignSelf: 'center',
            marginLeft: 5,
        },

        backImageContainer: {
            width: 12,
            height: 19,
            alignSelf: 'center',
            padding: 10,
        },
        row: {
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            height: null,
            width: null,
        },
        centerRow: {
            flexDirection: 'row',
            alignItems: 'center',
            height: null,
            width: null,
            alignSelf: 'center',
        },
        column: {
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'space-between',
            height: null,
            width: null,
        },
        vignette: {
            resizeMode: 'stretch',
        },
        control: {
            padding: 16,
        },
        text: {
            backgroundColor: 'transparent',
            color: '#FFF',
            fontSize: 14,
            textAlign: 'center',
        },
        pullRight: {
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
        },
        top: {
            // flex: 1,
            alignItems: 'stretch',
            justifyContent: 'flex-start',
            height: '10%',
        },
        center: {
            flex: 1,
            alignItems: 'stretch',
            justifyContent: 'center',
        },
        bottom: {
            alignItems: 'stretch',
            // flex: 2,
            justifyContent: 'flex-end',
            height: '20%',
            marginTop: -20
        },
        topControlGroup: {
            alignSelf: 'stretch',
            alignItems: 'center',
            justifyContent: 'space-between',
            flexDirection: 'row',
            width: null,
            margin: 12,
            marginBottom: 18,
        },
        bottomControlGroup: {
            alignSelf: 'stretch',
            alignItems: 'center',
            justifyContent: 'space-between',
            marginLeft: 12,
            marginRight: 12,
            marginBottom: 0,
        },
        centerControlGroup: {
            alignSelf: 'center',
            alignItems: 'center',
            marginLeft: 12,
            marginRight: 12,
            marginBottom: 0,
        },
        volume: {
            flexDirection: 'row',
        },
        fullscreen: {
            flexDirection: 'row',
        },
        playPause: {
            // position: 'relative',
            justifyContent: 'center',
            alignSelf: 'center',
            tintColor: '#ffffff'
        },
        title: {
            alignItems: 'center',
            flex: 0.6,
            flexDirection: 'column',
            padding: 0,
        },
        titleText: {
            textAlign: 'center',
        },
        timer: {
            width: 80,

        },
        timerText: {
            backgroundColor: 'transparent',
            color: '#FFF',
            fontSize: 11,
            textAlign: 'left',
            marginTop: -20,
            marginLeft: -7,
        },
        durationText: {
            backgroundColor: 'transparent',
            color: '#FFF',
            fontSize: 11,
            textAlign: 'right',
            marginTop: -20,
            marginRight: -7,
        },
    }),
    volume: StyleSheet.create({
        container: {
            alignItems: 'center',
            justifyContent: 'flex-start',
            flexDirection: 'row',
            height: 1,
            marginLeft: 20,
            marginRight: 20,
            width: 150,
        },
        track: {
            backgroundColor: '#333',
            height: 1,
            marginLeft: 7,
        },
        fill: {
            backgroundColor: '#FFF',
            height: 1,
        },
        handle: {
            position: 'absolute',
            marginTop: -24,
            marginLeft: -24,
            padding: 16,
        },
        icon: {
            marginLeft: 7,
        },
    }),
    seekbar: StyleSheet.create({
        container: {
            alignSelf: 'stretch',
            height: 28,
            marginLeft: 20,
            marginRight: 20,
        },
        track: {
            backgroundColor: '#333',
            height: 1,
            position: 'relative',
            top: 14,
            width: '100%',
        },
        fill: {
            backgroundColor: '#FFF',
            height: 1,
            width: '100%',
        },
        handle: {
            position: 'absolute',
            marginLeft: -7,
            height: 28,
            width: 28,
        },
        circle: {
            borderRadius: 12,
            position: 'relative',
            top: 8,
            left: 8,
            height: 12,
            width: 12,
        },
    }),
    containerStyle: {
        width: '100%',
        height: '100%',
        backgroundColor: '#ffffff',
        flex: 1,
        //backgroundColor: 'red'
    },
    maincontainerStyle: {
        width: '100%',
        height: '100%',
        backgroundColor: '#ffffff',
        flex: 1,
    },
    mainView: {
        flexDirection: 'column',
    },
    viewTitleCal: {
        flexDirection: 'column',
        marginLeft: 20,
        marginTop: 10,
        marginRight: 20,
    },
    textHeadTitle: {
        fontSize: 16,
        fontWeight: '500',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#282c37',
        // paddingTop: (DeviceInfo.getModel() === 'iPhone 11' || 'iPhone 11 Pro Max' || 'iPhone XS' || 'iPhone X' || 'iPhone XS Max' || 'iPhone Xc' || 'iPhone XR') ? 30 : 0,
        // paddingTop: (DeviceInfo.hasNotch() === true) ? 30 : 0,
    },
    todayCalView: {
        flexDirection: 'row',
        marginLeft: 40,
        alignSelf: 'center',
        marginRight: 40,
    },
    textCal: {
        fontSize: 14,
        //fontWeight: '500',
        //color: '#6d819c',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
        padding: 4
        //marginBottom:15
    },
    // container: {
    //     flex: 1,
    //     justifyContent: "flex-start"
    // },
    profileImage: {
        width: 25,
        height: 25,
        resizeMode: 'cover',
        marginLeft:15
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'center',
    },
    textIndicator1: {
        fontSize: 12,
        fontWeight: '400',
        fontFamily: 'Rubik-Medium',
        flex: 1,
        textAlign: 'center',
        alignSelf: 'center',
        color: '#6d819c',
        marginTop: 5
    },
    containerListStyle: {
        width: wp('90%'),
        //marginTop: hp('1%'),
        backgroundColor: '#ffffff',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderBottomColor: '#ddd',
        borderBottomWidth: 1,
        borderRadius:4,
        //position: 'relative',
        alignSelf: 'center',
        marginLeft: 15,
        //marginTop: 5,
        marginRight: 15,
        alignItems: 'center',
        // shadowColor: '#4075cd',
        // shadowOffset: { width: 0, height: 2 },
        // shadowOpacity: 0.25,
        // shadowRadius: 3.84,
        // elevation: 5
    },
    textInputStyle: {
        height: 40,
        flex: 1,
        fontSize: 13,
        marginLeft: 5,
        fontFamily: 'Rubik-Regular',
        color: '#2d3142',
        width: 50,
        //backgroundColor:'red'
    },
    overlay: {
        flex: 1,
        position: 'absolute',
        left: 0,
        top: 0,
        opacity: 0.5,
        backgroundColor: 'rgba( 0, 0, 0, 0.5 )',
        width: '100%',
        height: '100%'
    },
    container: {
        flex: 1,
        marginTop: 10,
    },
    item: {
        backgroundColor: '#f9c2ff',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
    },
    title: {
        fontSize: 32,
    },
    mainContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
     mainImageContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
        justifyContent: 'center',
    },
     postContents: {
        width: '90%',
        alignSelf: 'center',
        backgroundColor: '#fff',
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12,
        alignContent: 'center',
        justifyContent: 'center',
        padding: 23,
        marginTop: 10,
    },
    createPostStyle: {
        height: 30,
        alignSelf: 'center',
        color: '#5d5d5d',
        fontFamily: 'Rubik-Medium',
        fontSize: 14,
        fontWeight: '500',
        lineHeight: 30,
    },
    profileImage1: {
        width: 150,
        height: 150,
        aspectRatio: 1,
        alignSelf: 'center',
        backgroundColor: "#D8D8D8",
        marginTop: hp('2%'),
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: "#979797",
        borderRadius: 150 / 2,
        resizeMode: "cover",
        justifyContent: 'flex-end',
    },
    feedbackNameStyle: {
        height: 30,
        alignSelf: 'center',
        marginTop: hp('2%'),
        color: '#5a5a5a',
        fontFamily: 'Rubik-Medium',
        fontSize: 14,
        fontWeight: '500',
        lineHeight: 30,
    },
    feedbackSubStyle: {
        alignSelf: 'center',
        color: '#5d5d5d',
        fontFamily: 'Rubik-Medium',
        fontSize: 10,
        fontWeight: '500',
    },
    inputRow: {
        flexDirection: 'row',
        alignItems: "flex-start",
        height: '15%',
        marginTop: hp('2%'),
        marginBottom: hp('3%'),
        borderRadius: 10,
        backgroundColor: '#f3f4f6',
    },
    linearSubmitGradient: {
        width: '90%',
        height: 50,
        // bottom: 10,
        borderRadius: 15,
        justifyContent: 'center',
        // position: 'absolute',
        alignSelf: 'center',
    },
    buttonTuch:
    {
        backgroundColor: 'transparent',
    },
    buttonText: {
        fontSize: 14,
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        fontFamily: 'Rubik-Medium',
        fontWeight: '500',
    },
    textInputStyle1: {
        width: wp('75%'),
        color: '#2d3142',
        marginLeft: 5,
        fontFamily: 'Rubik-Regular',
        fontSize: 12,
        fontWeight: '400',
        letterSpacing: 0.23,
    },
    
}
