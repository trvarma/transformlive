import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image,
    BackHandler,
    ScrollView,
    KeyboardAvoidingView,
    FlatList,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { CustomDialog, Loader } from './common';
import { withNavigationFocus } from 'react-navigation';
import { BASE_URL, SWR } from '../actions/types';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import LogUtils from '../utils/LogUtils.js';
import { allowFunction } from '../utils/ScreenshotUtils.js'; 

function processResponse(response) {
    const statusCode = response.status;
    const data = response.json();
    return Promise.all([statusCode, data]).then(res => ({
        statusCode: res[0],
        data: res[1],
    }));
}

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

class Trainee_WaterDetails extends Component {

    constructor(props) {
        super(props);
        this.state = {
            firstName: '',
            lastName: '',
            email: '',
            isSuccess: false,
            sucMsg: '',
            sucTitle: '',
            isAlert: false,
            alertMsg: '',
            glsArray: [],
            resObj: {},
            loading: false,
        };
        this.callService = this.callService.bind(this);
    }
    async componentDidMount() {
        allowFunction();
        this.callService();
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBackPressed();
            return true;
        });
        this.storeNotiId('');
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    onBackPressed() {
        // Actions.popTo('traineeHome');
        Actions.trackershome();
    }

    async storeNotiId(notiId) {
        await AsyncStorage.setItem('notiId', notiId);
        LogUtils.infoLog1("NotificationId", await AsyncStorage.getItem('notiId'));
    }

    async callService() {
        this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/waterdet`,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false, resObj: data.data });
                    if (this.state.resObj) {
                        var myloop = [];
                        for (let i = 0; i < this.state.resObj.water_goal; i++) {
                            if (i < this.state.resObj.glass_drink) {
                                myloop.push({ id: i + 1, isFill: true });
                            }
                            else {
                                myloop.push({ id: i + 1, isFill: false });
                            }
                        }
                        this.setState({ glsArray: myloop })
                    }

                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: SWR });
            });
    }

    async saveWater() {
        this.setState({ loading: true });
        let token = await AsyncStorage.getItem('token');
        fetch(
            `${BASE_URL}/trainee/waterdet`,
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            },
        )
            .then(processResponse)
            .then(res => {
                const { statusCode, data } = res;
                LogUtils.infoLog1('statusCode', statusCode);
                LogUtils.infoLog1('data', data);
                if (statusCode >= 200 && statusCode <= 300) {
                    this.setState({ loading: false, isSuccess: true, sucMsg: data.message, sucTitle: data.title });
                } else {
                    if (data.message === 'You are not authenticated!') {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    } else {
                        this.setState({ loading: false, isAlert: true, alertMsg: data.message });
                    }
                }
            })
            .catch(function (error) {
                this.setState({ loading: false, isAlert: true, alertMsg: SWR });
            });
    }


    FlatListItemSeparator = () => {
        return (
            <View
                style={{
                    height: 0,
                    width: 0,
                    backgroundColor: "transparent",
                }}
            />
        );
    }

    async onAccept() {
        if (this.state.alertMsg === 'You are not authenticated!') {
            AsyncStorage.clear().then(() => {
                Actions.auth({ type: 'reset' });
            });
        }
        this.setState({ alertMsg: '' });
        this.setState({ isAlert: false });
    }

    async onSuccess() {
        // const interval = setInterval(() => {
        this.setState({ isSuccess: false, sucMsg: '', sucTitle: '' });
        this.callService();
        // clearInterval(interval);
        // }, 100);

    }

    jewelStyle = function (height) {
        if (height) {
            var val = 4.3 * height;
            return {
                borderRadius: 8, borderWidth: 5, borderColor: '#8c52ff', height: val, position: 'absolute', bottom: 0,
            }
        }
        else {
            return {
                borderRadius: 8, borderWidth: 5, borderColor: '#ddd', height: 0, position: 'absolute', bottom: 0,
            }
        }

    }

    renderWaterRemainder() {
        if (!isEmpty(this.state.resObj)) {
            if (this.state.resObj.water_status) {
                return (
                    <View style={styles.remainderViewStyle}>
                        <Text style={styles.remindertextStyle}>
                            {this.state.resObj.water_status}
                        </Text>
                    </View>
                );
            }
            else {
                return (
                    <View style={{ width: wp('100%'), height: 1, marginTop: 20, backgroundColor: '#ddd' }}>
                    </View>
                );
            }

        }

    }

    render() {
        return (
            <KeyboardAvoidingView keyboardVerticalOffset={Platform.select({ ios: 0, android: -70 })} style={styles.containerStyle} behavior="padding" enabled>
                <View style={{ width: '100%', height: '100%', backgroundColor: '#ffffff', }}>
                    <Loader loading={this.state.loading} />
                    <ScrollView contentContainerStyle={{ paddingBottom: hp('5%') }}>

                        <TouchableOpacity
                            style={{
                                width: 40,
                                height: 40,
                                marginTop: 20,
                                marginLeft: 20,
                                // marginBottom: 20,
                            }}
                            onPress={() => this.onBackPressed()}>
                            <Image
                                progressiveRenderingEnabled={true}
                                resizeMethod="resize"
                                source={require('../res/ic_back.png')}
                                style={styles.backImageStyle}
                            />
                        </TouchableOpacity>
                        <Text style={styles.textStyle}>
                            You have drank
                        </Text>
                        <View style={styles.row}>
                            <Text style={styles.textColorStyle}>
                                {this.state.resObj.glass_drink} glasses
                            </Text>
                            <Text style={styles.textStyle}>
                                today
                            </Text>
                        </View>

                        <FlatList
                            data={this.state.glsArray}
                            numColumns={4}
                            extraData={this.state}
                            style={{ marginTop: hp('2%'), marginLeft: wp('6%'), marginRight: wp('6%') }}
                            keyExtractor={(item, index) => index.toString()}
                            showsVerticalScrollIndicator={false}
                            ItemSeparatorComponent={this.FlatListItemSeparator}
                            renderItem={({ item, index }) => {
                                return <View key={index} style={styles.viewGlasses}>
                                    {item.isFill
                                        ? (
                                            <Image
                                                progressiveRenderingEnabled={true}
                                                resizeMethod="resize"
                                                source={require('../res/ic_glass_det_fill.png')}
                                                style={{
                                                    width: 35,
                                                    height: 45,
                                                    alignSelf: 'center',
                                                }}
                                            />
                                        )
                                        : (
                                            <TouchableOpacity onPress={() => this.saveWater()}>
                                                <Image
                                                    progressiveRenderingEnabled={true}
                                                    resizeMethod="resize"
                                                    source={require('../res/ic_glass_add.png')}
                                                    style={{
                                                        width: 35,
                                                        height: 45,
                                                        alignSelf: 'center',
                                                    }}
                                                />
                                            </TouchableOpacity>
                                        )}
                                </View>
                            }} />


                        <View style={styles.glassesRow}>
                            <View style={styles.column}>
                                <Text style={styles.mltextStyle}>
                                    {this.state.resObj.water_drank}
                                </Text>
                                <Text style={styles.waterdranktextStyle}>
                                    Water Drank
                                </Text>

                            </View>
                            <View style={styles.verLineView}>
                            </View>
                            <View style={styles.column}>
                                <Text style={styles.mltextStyle}>
                                    {this.state.resObj.water_goal} glasses (2 ltr)
                                </Text>
                                <Text style={styles.waterdranktextStyle}>
                                    Daily Goal
                                </Text>
                            </View>
                        </View>
                        {/* water remainder */}
                        {this.renderWaterRemainder()}


                        <Text style={styles.insighttextStyle}>
                            Daily Average
                        </Text>

                        <FlatList
                            data={this.state.resObj.water_arr}
                            numColumns={7}
                            extraData={this.state}
                            keyExtractor={(item, index) => index.toString()}
                            style={{ marginTop: hp('2%'), marginLeft: wp('6%'), marginRight: wp('6%') }}
                            showsVerticalScrollIndicator={false}
                            ItemSeparatorComponent={this.FlatListItemSeparator}
                            renderItem={({ item, index }) => {
                                return <View key={index} style={{ flexDirection: 'column', width: wp('12%') }}>
                                    <View style={styles.insightViewStyle}>
                                        <View style={styles.insightInsideViewStyle}>

                                        </View>
                                        <View style={this.jewelStyle(item.glass_cnt)}>

                                        </View>
                                    </View>
                                    <Text style={styles.MtextStyle}>
                                        {item.day_name}
                                    </Text>
                                </View>
                            }} />

                        <Text style={styles.insighttextStyle}>
                            Daily Intake
                        </Text>
                        <View style={styles.horLineView}>
                        </View>
                        <FlatList
                            data={this.state.resObj.lw_water_arr}
                            extraData={this.state}
                            keyExtractor={(item, index) => index.toString()}
                            // style={{ marginTop: hp('2%'), marginLeft: wp('4%'), marginRight: wp('6%') }}
                            showsVerticalScrollIndicator={false}
                            scrollEnabled={false}
                            ItemSeparatorComponent={this.FlatListItemSeparator}
                            renderItem={({ item, index }) => {
                                return <View key={index}>
                                    <View style={styles.performanceRow}>

                                        <Image
                                            progressiveRenderingEnabled={true}
                                            resizeMethod="resize"
                                            source={require('../res/ic_blue_dot.png')}
                                            style={{
                                                width: 10,
                                                height: 10,
                                                marginLeft: 10,
                                                marginTop: 5,
                                                alignSelf: 'flex-start',
                                            }}
                                        />
                                        <View style={styles.performancecolumn}>
                                            <Text style={styles.performancetextStyle}>
                                                {item.cdate}
                                            </Text>
                                            <Text style={styles.waterdranktextStyle}>
                                                {item.full_day_name}
                                            </Text>
                                        </View>
                                        <Text style={styles.performancePercenttextStyle}>
                                            {item.ml_drink}
                                        </Text>
                                    </View>
                                    <View style={styles.horLineView}>
                                    </View>
                                </View>
                            }} />

                    </ScrollView>

                    <CustomDialog
                        visible={this.state.isAlert}
                        title='Alert'
                        desc={this.state.alertMsg}
                        onAccept={this.onAccept.bind(this)}
                        no=''
                        yes='Ok' />

                    <CustomDialog
                        visible={this.state.isSuccess}
                        title={this.state.sucTitle}
                        desc={this.state.sucMsg}
                        onAccept={this.onSuccess.bind(this)}
                        no=''
                        yes='Ok' />

                </View>
            </KeyboardAvoidingView>
        )
    }

}

const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        height: '100%',
        backgroundColor: '#fff',
        flex: 1,
    },
    backImageStyle: {
        width: 19,
        height: 16,
        marginTop: 10,
        alignSelf: 'flex-start',
    },
    container: {
        flex: 1,
        justifyContent: "flex-start"
    },
    row: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'center',
    },
    column: {
        flexDirection: 'column',
        flex: 1,
        justifyContent: 'center',
    },
    textStyle: {
        fontSize: 22,
        fontWeight: '500',
        color: '#2d3142',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    textColorStyle: {
        fontSize: 22,
        marginRight: 5,
        fontWeight: '500',
        color: '#8c52ff',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    viewGlasses: {
        flexDirection: 'row',
        flex: 1,
        width: wp('20%'),
        alignSelf: 'center',
        marginTop: hp('5%'),
        justifyContent: 'center',
    },
    glassesRow: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'center',
        marginTop: hp('6%'),
        paddingLeft: 10,
        paddingRight: 10,
    },
    mltextStyle: {
        fontSize: 14,
        marginTop: 5,
        fontWeight: '500',
        color: '#2d3142',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    waterdranktextStyle: {
        fontSize: 14,
        marginTop: 1,
        fontWeight: '500',
        color: '#C3C1CE',
        textAlign: 'center',
        fontFamily: 'Rubik-Regular',
    },
    verLineView: {
        borderWidth: 1,
        borderColor: '#ddd',
    },
    remainderViewStyle: {
        backgroundColor: '#F4DCDC',
        marginTop: 20,
        flexDirection: 'row',
        padding: 15,
        justifyContent: 'flex-start',
    },
    remindertextStyle: {
        fontSize: 12,
        width: wp('100%'),
        fontWeight: '500',
        color: '#E4858A',
        textAlign: 'center',
        alignSelf: 'center',
        fontFamily: 'Rubik-Medium',
    },
    insighttextStyle: {
        fontSize: 18,
        marginTop: 5,
        fontWeight: '500',
        color: '#2d3142',
        padding: 20,
        fontFamily: 'Rubik-Medium',
    },
    insightViewStyle: {
        flexDirection: 'column',
        alignContent: 'center',
        alignItems: 'center',
    },
    insightInsideViewStyle: {
        flexDirection: 'column',
        borderWidth: 5,
        borderColor: '#ddd',
        height: 35,
        borderRadius: 8,
    },
    MtextStyle: {
        fontSize: 14,
        marginTop: 10,
        fontWeight: '500',
        color: '#C3C1CE',
        textAlign: 'center',
        alignSelf: 'center',
        fontFamily: 'Rubik-Regular',
        width: 20,
    },
    performanceRow: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'flex-start',
        padding: 15,
    },
    performancecolumn: {
        flexDirection: 'column',
        flex: 1,
        alignItems: 'flex-start',
        marginLeft: 10,
    },
    performancetextStyle: {
        fontSize: 14,
        fontWeight: '500',
        color: '#2d3142',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
    },
    performancePercenttextStyle: {
        fontSize: 14,
        fontWeight: '500',
        color: '#2d3142',
        textAlign: 'center',
        fontFamily: 'Rubik-Medium',
        marginRight: 30,
    },
    horLineView: {
        borderWidth: .5,
        borderColor: '#ddd',
    },
});

const mapStateToProps = state => {

    //   const { goals, trainerCodes } = state.procre;
    const loading = state.procre.loading;
    const error = state.procre.error;
    return { loading, error, };

};

export default withNavigationFocus(
    connect(
        mapStateToProps,
        {},
    )(Trainee_WaterDetails),
);

// export default ProfileCreation;