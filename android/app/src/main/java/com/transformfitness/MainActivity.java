package com.transformfitness;

import com.facebook.react.ReactActivity;
import com.facebook.react.ReactActivityDelegate;
import com.facebook.react.ReactRootView;
import com.swmansion.gesturehandler.react.RNGestureHandlerEnabledRootView;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.WindowManager;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class MainActivity extends ReactActivity {

  /**
   * Returns the name of the main component registered from JavaScript. This is used to schedule
   * rendering of the component.
   */


  @Override
  protected String getMainComponentName() {
    return "fitnes";
  }

  @Override
  protected ReactActivityDelegate createReactActivityDelegate() {
    return new ReactActivityDelegate(this, getMainComponentName()) {
      @Override
      protected ReactRootView createRootView() {
        return new RNGestureHandlerEnabledRootView(MainActivity.this);
      }
    };
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
    Thread.setDefaultUncaughtExceptionHandler(new UnCaughtException(MainActivity.this));

    // hash key code
//    PackageInfo info;
//    try {
//      info = getPackageManager().getPackageInfo("com.transformfitness", PackageManager.GET_SIGNATURES);
//      for (Signature signature : info.signatures) {
//        MessageDigest md;
//        md = MessageDigest.getInstance("SHA");
//        md.update(signature.toByteArray());
//        String something = new String(Base64.encode(md.digest(), 0));
//        //String something = new String(Base64.encodeBytes(md.digest()));
//        Log.e("hash key", something);
//      }
//    } catch (PackageManager.NameNotFoundException e1) {
//      Log.e("name not found", e1.toString());
//    } catch (NoSuchAlgorithmException e) {
//      Log.e("no such an algorithm", e.toString());
//    } catch (Exception e) {
//      Log.e("exception", e.toString());
//    }
  }
}